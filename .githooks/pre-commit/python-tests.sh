#!/usr/bin/env bash
for pyfile in $(git diff --cached --diff-filter=ACMR --name-only | grep -P "\.py$")
do
    nosetests --config "$ROOT_DIR/etc/nose/nose.cfg" "$pyfile"
done
