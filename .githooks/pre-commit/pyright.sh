#!/usr/bin/env bash
exit_code=0

on_exit(){
    if [ $exit_code -ne 0 ]; then
        echo "Please fix the issues raised by pyright before committing."
    fi
    exit $exit_code
}

trap on_exit EXIT

on_error(){
    ret=$?
    if [ $ret -ne 0 ]; then
        exit_code=$ret
    fi
}

trap on_error ERR

get_changed_files() {
    git diff --cached --diff-filter=ACMR --name-only | grep -P "\.py$" | while read -r file; do
        if ! grep -xq "$file" "$ROOT_DIR/etc/pyright-erroring-modules"; then
            echo "$file"
        fi
    done
}

mapfile -t changed_files < <(get_changed_files)

if [ ${#changed_files[@]} -eq 0 ]; then
    exit 0  # exit early if no relevant files changed
fi

pyright --project "etc/pyrightconfig.json" "${changed_files[@]}"
