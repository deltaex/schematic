#!/usr/bin/env bash
exit_code=0

on_exit() {
    if [ $exit_code -ne 0 ]; then
        echo "You can dismiss a lint error by adding \`# noqa: {lint_rule_code}\` to the violating line"
        echo "or use \`# noqa\` to suppress all errors from a line (not recommended)"
        echo "or (highly unrecommended) to ignore all violations across a file \`# ruff: noqa\`"
    fi
    exit $exit_code
}

on_error() {
    ret=$?
    if [ $ret -ne 0 ]; then
        exit_code=$ret
    fi
}

trap on_exit EXIT
trap on_error ERR

for pyfile in $(git diff --cached --diff-filter=ACMR --name-only | grep -P "\.py$")
do
    ruff check "$pyfile"
done