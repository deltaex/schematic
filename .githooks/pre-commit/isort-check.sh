#!/usr/bin/env bash
for pyfile in $(git diff --cached --diff-filter=ACMR --name-only | grep -P "\.py$")
do
    case $pyfile in
       src/mixrank/q5/cli.py)
           echo "Ignoring $pyfile";;
        *)
            # let's automatically isort imports when changing python files to remove a manual step
            # be careful to use the staged copy of the file instead of the current version
            cp "$pyfile" /tmp/isort-check.sh-tmp-crorofitdyni  # backup the unstaged version
            git show :"$pyfile" > "$pyfile"  # write the staged version of the file
            isort --settings-path "$ROOT_DIR/etc/.isort.cfg" "$pyfile" # isort the staged copy
            git add "$pyfile" # stage the isorted version
            cp /tmp/isort-check.sh-tmp-crorofitdyni "$pyfile"  # restore the unstaged version
            isort --settings-path "$ROOT_DIR/etc/.isort.cfg" "$pyfile" # also isort the unstaged version so that it matches the changes to the commited one
            ;;
    esac
done
