#!/usr/bin/env bash
# uses shellcheck, a haskell program that does static analysis on shell scripts to raise errors and lint warnings
# https://github.com/koalaman/shellcheck
for shfile in $(git diff --cached --diff-filter=ACMR --name-only)
do
    (echo "$shfile" | grep -P "\.sh$") || \
        (head -n1 "$shfile" | grep "\#! ?*/usr/bin/env ?*bash") || \
        continue
    shellcheck "$shfile"
done
