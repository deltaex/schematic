#!/usr/bin/env bash
for pyfile in $(git diff --cached --diff-filter=ACMR --name-only | grep -P "\.py$")
do
    pylint --rcfile="$ROOT_DIR/etc/pylint/default.rc" -E "$pyfile"
    python py/schematic/pylint_diff.py "$pyfile"
done
