# Client authentication using SSL
PostgreSQL supports using the TLS protocol (previously called SSL) for encrypting the communication between servers and clients. Below are the steps to setup PostgreSQL client authentication for a Schematic database:
1. Unzip the certificates zip file, it should contain directories named after the users of the database and look something like this:
```sh
database-S16JAGM5ANYDEL1S-certificates
├── user1
│  ├── postgresql.crt
│  ├── postgresql.key
│  └── root.crt
└── user2
   ├── postgresql.crt
   ├── postgresql.key
   └── root.crt
```
   Each folder contains the SSL credentials required for authentication as a specific user.

2. Check to see if you have any existing PostgreSQL SSL certificates, by default they are stored in a `root.crt` file located at `~/.postgresql` on Linux and `%APPDATA%\\postgresql` on WIndows [^1], this can be done by checking the output of running:
```sh
# Linux
ls ~/.postgresql

# Windows (PowerShell)
ls %APPDATA%\\postgresql
```
  __Note:__ If the directory doesn't exist, create it like so:
```sh
# Linux
mkdir ~/.postgresql

# Windows (PowerShell)
mkdir %APPDATA%\\postgresql
```

If the directory is empty you can simply copy the contents of the directory named after the user you wish to authenticates as into `~/.postgresql`, like so:
```sh
# Linux
mv user1/* ~/.postgresql  # Linux

# Windows (PowerShell)
mv  .\\user1\\* %APPDATA%\\postgresql\\root.crt
```

If the directory is not empty and contains an existing `root.crt` file, the process is a bit more complicated as the existing `root.crt` might contain existing certificates which we have to be careful not to overwrite.
- First we switch to directory containing the user we wish to authenticate with:
```sh
# Linux
cd user1       # Note: This is assuming you wish to use the credentials of user1

# Windows (PowerShell)
cd .\\user1\\    # Note: This is assuming you wish to use the credentials of user1
```
- Then we append the contents of the user's `root.crt` file to the existing `root.crt` file:
```sh
# Linux
cat root.crt >> ~/.postgresql/root.crt   # Note the use of >> (append) over > (overwrite)

# Windows (PowerShell)
cat .\\root.crt | Add-Content -Path %APPDATA%\\postgresql\\root.crt
```
- Move the remaining files:
```sh
# Linux
mv postgresql.crt ~/.postgresql
mv postgresql.key ~/.postgresql

# Windows (PowerShell)
mv .\\postgresql.crt,.\\postgresql.key %APPDATA%\\postgresql\\root.crt
```
3. Connect to the database:
```
psql "postgresql://your_username:your_password@your_hostname:port/database_name?sslmode=verify-ca"
```
If everything went well you should see a prompt starting with:
```sh
psql (16.4)
SSL connection (protocol: TLSv1.3, cipher: TLS_AES_256_GCM_SHA384, compression: off)
```

[^1]: https://www.postgresql.org/docs/current/libpq-ssl.html#LIBPQ-SSL-CLIENTCERT
