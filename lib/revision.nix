stdargs @ { scm, pkgs, rsync, ... }:
args @ {
    guid, name, upgrade_sql, basefiles ? null, add_meta_revision ? true, autocommit ? false,
    dependencies ? [], preBuildInputs ? null,
    ...
}:

rec {
    scm_type = "revision";
    inherit guid name dependencies upgrade_sql preBuildInputs basefiles add_meta_revision autocommit;
    depends = scm.deps dependencies;
    transDepGuids = scm.getTransDepGuids depends;
    apply = { server }: scm.effect {
        inherit (server) postgresql pguri port basedir SCM_UPGRADE_MODE SCM_VERBOSE SCM_PG_UPGRADE SCM_ISTEMP;
        inherit guid transDepGuids upgrade_sql name scm_type add_meta_revision basefiles autocommit;
        SCM_DERIVATIVE_REF_BASEDIR = builtins.getEnv "SCM_DERIVATIVE_REF_BASEDIR";
        SCM_EFFECT_PROG = ../py/schematic/build_revision.py;
        dependencies = map (a: a.guid) (builtins.filter (a: a ? guid) depends);
        # revisions have an implied dependency on revision to log that they've been applied
        buildInputs = [ rsync ] ++ (scm.buildDeps {
          inherit server preBuildInputs;
          dependencies = dependencies ++ (if add_meta_revision then [ <meta.revision-ADTUKDUCMEEMEGPI> ] else []);
        });
    };
}
