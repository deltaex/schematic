stdargs @ { scm, pkgs, ... }:

let
    inherit (builtins) getEnv filter getAttr;
    reverseString = str: with pkgs.lib; concatStrings (reverseList (stringToCharacters str));
    scm_sandbox_modules = filter (x: x != "") (pkgs.lib.splitString ":" (getEnv "SCM_SANDBOX_MODULES"));
    modules = map scm.pkg-from-path scm_sandbox_modules;
    dbobj = pkgs.lib.findFirst (p: p.scm_type == "database") null modules; # maybe an scm object
    othermods = map scm.normalize-path (filter (m: (scm.pkg-from-path m).scm_type != "database") scm_sandbox_modules); # must be a list of paths
    postgresql_version = (getEnv "SCM_POSTGRESQL_VERSION");
    db = scm.database rec {
        guid = reverseString server.guid;
        name = (if dbobj != null then ''${dbobj.name}-sandbox'' else "sandbox"); 
        server = scm.server rec {
            postgresql = (
                    if postgresql_version != null && postgresql_version != "" then (getAttr ("postgresql_" + postgresql_version) pkgs)
                    else (if dbobj != null then dbobj.server.postgresql else pkgs.postgresql));
            guid = getEnv "SCM_GUID";
            name = (if dbobj != null then ''${dbobj.server.name}-sandbox'' else "sandbox");
            dbname = (if dbobj != null then dbobj.server.dbname else (pkgs.lib.maybeEnv "SCM_DBNAME" "sandbox"));
            port = getEnv "SCM_PORT";
            user = (if dbobj != null then dbobj.server.user else (pkgs.lib.maybeEnv "SCM_USER" "root"));
            password = (if dbobj != null then dbobj.server.password else (pkgs.lib.maybeEnv "SCM_PASS" "pass"));
            SCM_ISTEMP = pkgs.lib.maybeEnv "SCM_ISTEMP" true;
            SCM_VERBOSE = pkgs.lib.maybeEnv "SCM_VERBOSE" false;
            SCM_UPGRADE_MODE = "declarative";
        };
        # don't depend directly on a database, because then we'll depend on another server instance instead of just our sandbox server
        dependencies = (if dbobj != null then dbobj.dependencies else []) ++ othermods;
        preBuildInputs = if dbobj != null then dbobj.preBuildInputs else null;
    };
in db
