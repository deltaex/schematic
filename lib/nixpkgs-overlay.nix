super: self:
let
  pglib = import ./postgresql-lib.nix { pkgs=self; };
in
{
  tmux = (self.callPackage ./tmux {});
} // pglib.all_releases
