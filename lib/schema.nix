stdargs @ { scm, pkgs, rsync, ... }:
args @ {
    guid, name, upgrade_sql, basefiles ? null, autocommit ? false,
    dependencies ? [], preBuildInputs ? null,
    ...
}:

rec {
    scm_type = "schema";
    inherit guid name dependencies upgrade_sql preBuildInputs basefiles autocommit;
    depends = scm.deps dependencies;
    transDepGuids = scm.getTransDepGuids depends;
    apply = { server }: scm.effect rec {
        inherit (server) postgresql pguri port basedir SCM_UPGRADE_MODE SCM_VERBOSE SCM_PG_UPGRADE SCM_ISTEMP;
        inherit guid transDepGuids upgrade_sql name scm_type basefiles autocommit;
        SCM_DERIVATIVE_REF_BASEDIR = builtins.getEnv "SCM_DERIVATIVE_REF_BASEDIR";
        SCM_EFFECT_PROG = ../py/schematic/build_schema.py;
        buildInputs = [ rsync ] ++ (scm.buildDeps { inherit server dependencies preBuildInputs; });
    };
}
