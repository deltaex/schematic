#! /usr/bin/env python3
'''
Refresh the file lib/postgresql/postgresql-releases.json using the index of official PostgreSQL releases.

Use this when new PostgreSQL releases are available to make them available in Schematic.
'''
import base64
import hashlib
import json
import os
import re
import sys
import time
from datetime import datetime
from urllib.parse import urljoin

import requests

from schematic.build_util import split_postgresql_version

baseurl = 'https://ftp.postgresql.org/pub/source/'
json_output = os.path.join(os.path.dirname(os.path.realpath(__file__)), 'postgresql-releases.json')

def calculate_sha256(content):
    hash_sha256 = hashlib.sha256()
    for chunk in content.iter_content(chunk_size=10000000):
        hash_sha256.update(chunk)
    return hash_sha256.hexdigest()

def request_manual(url, attempts=3):
    ''' When the hash isn't provided, download the release file and calculate the hash.
        PostgreSQL 9.2.4 and earlier provide md5 hash instead of sha256, which isn't usable for nix derivations.
    '''
    for attempt in range(1, attempts + 1):
        try:
            fr = requests.get(url, timeout=300, stream=True)
            fr.raise_for_status()
            return base64.b64encode(bytes.fromhex(calculate_sha256(fr))).decode()
        except requests.exceptions.HTTPError as e:
            if e.response.status_code == 404:
                return
        except requests.exceptions.RequestException:
            if attempt == attempts:
                raise
            time.sleep(attempt)

def get_cached_sha256(ver, _cache={}):  # pylint: disable=dangerous-default-value
    if 'releases' not in _cache:
        with open(json_output) as fp:
            _cache['releases'] = json.load(fp)
    for release in _cache['releases']:
        if release['version'] == ver:
            return release['hash']

def fetch_hash(ver, attempts=3):
    hfile = 'postgresql-{}.tar'.format(ver[1:-1])
    if ver[1:-1] == '8.0':
        hfile = 'postgresql-{}.tar'.format('8.0.0')
    elif ver[1:-1] == '6.0':
        hfile = 'postgresql-{}.tar'.format('v6.0') # v6.0 instead of 6.0
    elif ver[1:-1] == '6.3':
        hfile = 'postgresql-{}.tar'.format('6.3.2')
    elif ver[1:-1] in ['1.09', '1.08']:
        hfile = 'postgres95-{}.tar'.format(ver[1:-1])
    elif ver[1:-1] == '6.4':
        hfile = 'postgresql-{}.tar'.format('6.4.2')
    elif ver[1:-1] == '7.0.1':
        hfile = 'postgresql.{}.tar'.format(ver[1:-1]) # uses dot instead of - after postgresql
    base_ver_url =  urljoin(baseurl, ver)
    gz_url = f'{base_ver_url}{hfile}.gz'
    bz2_url = f'{base_ver_url}{hfile}.bz2'
    hash_url = f'{bz2_url}.sha256'
    clean_version = ver.strip('/').strip('v')
    if split_postgresql_version(clean_version) <= [9, 2, 4]:
        response = None  # hash file isn't available for 9.2.4 and earlier, don't bother trying
    else:
        for attempt in range(1, attempts + 1):
            try:
                response = requests.get(hash_url, timeout=300)
                response.raise_for_status()
                break
            except (requests.exceptions.HTTPError, requests.exceptions.RequestException) as e:
                if e.response.status_code == 404:
                    response = None
                    break
                if attempt == attempts:
                    raise
                time.sleep(attempt)
    hash_content = response.text if response else ''
    if matches := re.findall(r'[a-fA-F0-9]{64}', hash_content):
        hashstr = base64.b64encode(bytes.fromhex(matches[0])).decode()
    else:
        hashstr = (
            get_cached_sha256(clean_version) or
            request_manual(bz2_url, attempts=attempts) or
            request_manual(gz_url, attempts=attempts))
    print(f'{ver[:-1]} -> {hashstr if hashstr else "ERROR!"}', file=sys.stderr)
    return hashstr

def fetch_date(url):
    content = requests.get(url, timeout=300).text
    dates = re.findall(r'\d{2}-\w{3}-\d{4}', content)
    if not dates:
        raise ValueError('Invalid release date: ', content)
    # format back to YYYY-MM-DD
    dates = [datetime.strptime(date, '%d-%b-%Y').strftime('%Y-%m-%d') for date in dates]
    release_date = min(dates)
    return release_date

def parse_version(version_string):
    matches = re.search(r'(?P<major>\d+)\.(?P<minor>\d+)(\.(?P<patch>\d+))?', version_string)
    if not matches:
        raise ValueError('Invalid version string: ', version_string)
    return tuple(int(matches.group(g) or 0) for g in ['major', 'minor', 'patch'])

def main():
    indexcontent = requests.get(baseurl, timeout=300).text
    releases = re.findall(r'href="((?:v)[^"]+)"', indexcontent)
    # filter out v<major>beta<minor>
    releases = [s for s in releases if not re.search(r'\d+(beta|rc)\d', s)]
    releases.sort(key=parse_version, reverse=True)
    versions_data = []
    for version in releases:
        release_date = fetch_date(urljoin('https://ftp.postgresql.org/pub/source/', version))
        versions_data.append({'version': version[1:-1], 'hash': fetch_hash(version), 'date': release_date})
    with open(json_output, 'w') as json_file:
        json.dump(versions_data, json_file, indent=4)
    print('Written to ' + json_output, file=sys.stderr)

if __name__ == '__main__':
    main()
