{
  lib,
  stdenv,
  buildPythonPackage,
  fetchFromGitHub,
  fetchurl,
  pythonOlder,
  substituteAll,

  # build
  postgresql,
  setuptools,

  # propagates
  backports-zoneinfo,
  typing-extensions,

  # psycopg-c
  cython,
  tomli,

  # docs
  furo,
  shapely,
  sphinxHook,
  sphinx-autodoc-typehints,

  # tests
  anyio,
  pproxy,
  pytest-randomly,
  pytestCheckHook,
  postgresqlTestHook,
}:

let
  pname = "psycopg";
  version = "3.2.3";

  src = fetchFromGitHub {
    owner = "psycopg";
    repo = pname;
    rev = "refs/tags/${version}";
    hash = "sha256-vcUZvQeD5MnEM02phk73I9dpf0Eug95V7Rspi0s6S2M=";
  };

  patches = [
    (substituteAll {
      src = ./ctypes.patch;
      libpq = "${postgresql.out}/lib/libpq${stdenv.hostPlatform.extensions.sharedLibrary}";
      libc = "${stdenv.cc.libc}/lib/libc.so.6";
    })
  ];

  baseMeta = {
    changelog = "https://github.com/psycopg/psycopg/blob/${version}/docs/news.rst#current-release";
    homepage = "https://github.com/psycopg/psycopg";
    license = lib.licenses.lgpl3Plus;
    maintainers = with lib.maintainers; [ hexa ];
  };

in

buildPythonPackage rec {
  inherit pname version src;
  format = "pyproject";

  disabled = pythonOlder "3.7";

  outputs = [
    "out"
    "doc"
  ];

  sphinxRoot = "../docs";

  # Introduce this file necessary for the docs build via environment var
  LIBPQ_DOCS_FILE = fetchurl {
    url = "https://raw.githubusercontent.com/postgres/postgres/496a1dc44bf1261053da9b3f7e430769754298b4/doc/src/sgml/libpq.sgml";
    hash = "sha256-JwtCngkoi9pb0pqIdNgukY8GbG5pUDZvrGAHZqjFOw4";
  };

  inherit patches;

  # only move to sourceRoot after patching, makes patching easier
  postPatch = ''
    cd psycopg
  '';

  nativeBuildInputs = [
    furo
    setuptools
    shapely
    sphinx-autodoc-typehints
    sphinxHook
  ];

  propagatedBuildInputs = [
    typing-extensions
  ] ++ lib.optionals (pythonOlder "3.9") [ backports-zoneinfo ];

  pythonImportsCheck = [
    "psycopg"
  ];

  nativeCheckInputs =
    [
      anyio
      pproxy
      pytest-randomly
      pytestCheckHook
      postgresql
    ]
    ++ lib.optional (stdenv.hostPlatform.isLinux) postgresqlTestHook;

  env = {
    postgresqlEnableTCP = 1;
    PGUSER = "psycopg";
    PGDATABASE = "psycopg";
  };

  preCheck =
    ''
      cd ..
    ''
    + lib.optionalString (stdenv.hostPlatform.isLinux) ''
      export PSYCOPG_TEST_DSN="host=/build/run/postgresql user=$PGUSER"
    '';

  disabledTests = [
    # don't depend on mypy for tests
    "test_version"
    "test_package_version"
  ];
  doCheck=false;

  disabledTestPaths = [
    # Network access
    "tests/test_dns.py"
    "tests/test_dns_srv.py"
    # Mypy typing test
    "tests/test_typing.py"
    "tests/crdb/test_typing.py"
    # https://github.com/psycopg/psycopg/pull/915
    "tests/test_notify.py"
    "tests/test_notify_async.py"
    # Add pool-related test paths
    "tests/pool/test_pool.py"
    "tests/pool/test_pool_common_async.py"
    "tests/pool/test_pool_async_noasyncio.py"
  ];

  pytestFlagsArray = [
    "-o"
    "cache_dir=$TMPDIR"
    "-m"
    "'not refcount and not timing and not flakey'"
    # pytest.PytestRemovedIn9Warning: Marks applied to fixtures have no effect
    "-W"
    "ignore::pytest.PytestRemovedIn8Warning"
  ];

  postCheck = ''
    cd ${pname}
  '';

  meta = baseMeta // {
    description = "PostgreSQL database adapter for Python";
  };
}
