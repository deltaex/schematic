{ pkgs, stdenv }:

with rec {
    py3 = pkgs.python310.override {
        packageOverrides = final: prev: {
            autodebug = (final.callPackage ./autodebug.nix {});
            bugsnag = (final.callPackage ./bugsnag.nix {});
            locust = (final.callPackage ./locust.nix {});
            psycopg2 = (final.callPackage ./psycopg2.nix {});
            pylint_quotes = (final.callPackage ./pylint_quotes.nix {});
            schemainspect = (final.callPackage ./schemainspect.nix {});
            sqlbag = (final.callPackage ./sqlbag {});
            psycopg = (final.callPackage ./psycopg.nix {});
        };
    };
}; py3.buildEnv.override {
    ignoreCollisions = true;
    extraLibs = with py3.pkgs; [
        (if stdenv.isLinux then autodebug else null)
        (if stdenv.isLinux then pylint else null)
        Fabric
        boto3
        boto3-stubs
        mypy-boto3-s3
        bugsnag
        click
        clint
        gevent
        orjson
        ipython
        isort
        locust
        lz4
        nose
        pglast
        psutil
        psycopg2
        psycopg
        pudb
        pylint_quotes
        pytz
        requests
        schemainspect
        setproctitle
        sh
        sqlalchemy
        sqlbag
        tkinter
        toolz
    ];
}
