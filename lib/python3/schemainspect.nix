{ pkgs
, stdenv
, buildPythonPackage
, six
, sqlalchemy
, sqlbag
}: buildPythonPackage rec {
    name = "schemainspect-0.1.1610933302";
    src = pkgs.fetchurl {
        url = "https://files.pythonhosted.org/packages/ff/37/ebc43691c8be47fb6aab320b8141f84c1753a5d0817f7120fc5fa91c72a1/schemainspect-3.1.1658648837.tar.gz";
        sha256 = "sha256-YB6fpfwkPLZFIB6gBk0vKZMCPufJf6CNnS/TbcgG3fw=";
    };
    propagatedBuildInputs = [
        six
        sqlalchemy
        sqlbag
    ];
}
