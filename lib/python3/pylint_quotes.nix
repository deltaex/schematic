{ lib
, buildPythonPackage
, fetchPypi
, pylint
, six
, webob
}:

buildPythonPackage rec {
  pname = "pylint-quotes";
  version = "0.2.3";

  src = fetchPypi {
    inherit pname version;
    sha256 = "sha256-LWuz+ooaha86+KDKh1pxmsW823NcRXVihGmdgJwQnJU=";
  };

  propagatedBuildInputs = [ pylint ];

  # # no tests
  # doCheck = false;

  meta = with lib; {
    description = "Pylint plugin for checking the consistency of string quotes.";
    homepage = "https://github.com/edaniszewski/pylint-quotes";
    license = licenses.mit;
  };
}
