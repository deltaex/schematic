{ pkgs
, stdenv
, buildPythonPackage
, six
, sqlalchemy
}: buildPythonPackage rec {
    name = "sqlbag-0.1.1579049654";
    src = pkgs.fetchurl {
        url = "https://files.pythonhosted.org/packages/00/2a/39299296352f561cfa20ccd579cfafaf9bdb3df0626959a57c2c2b7042d4/sqlbag-0.1.1579049654.tar.gz";
        sha256 = "0hlg55fiij4a4dw43hg063c4pg08i2mg9dkssdhyzc32l09vl6mz";
    };
    doCheck = false;
    patches = [
        ./sqlbag-remove-pathlib.diff
    ];
    propagatedBuildInputs = [
        six
        sqlalchemy
    ];
}
