stdargs @ { scm, pkgs, python3, rsync, openssl, ... }:
args @ {
    guid, name, server, basefiles ? null,
    dependencies ? [], preBuildInputs ? null,
    ...
}:

rec {
    scm_type = "database";
    inherit guid name dependencies server preBuildInputs basefiles;
    depends = scm.deps dependencies;
    transDepGuids = scm.getTransDepGuids depends;
    apply = scm.effect {
        inherit (server) postgresql pguri basedir port name user SCM_UPGRADE_MODE SCM_VERBOSE SCM_PG_UPGRADE SCM_ISTEMP;
        inherit  guid scm_type transDepGuids basefiles;
        SCM_EFFECT_PROG = ../py/schematic/build_database.py;
        buildInputs = [ rsync openssl ] ++ (scm.buildDeps { inherit server dependencies preBuildInputs; });
    };
}
