stdargs @ { scm, pkgs, stdenv, postgresql, python3, rsync, ... }:
args @ {
    guid, name, location,
    seq_page_cost ? null, random_page_cost ? null, effective_io_concurrency ? null, maintenance_io_concurrency ? null,
    comment ? null,
    dependencies ? [], preBuildInputs ? null,
    ...
}:

rec {
    scm_type = "tablespace";
    inherit guid name dependencies preBuildInputs location seq_page_cost random_page_cost effective_io_concurrency maintenance_io_concurrency comment;
    depends = scm.deps dependencies;
    transDepGuids = scm.getTransDepGuids depends;
    apply = { server }: scm.effect {
        inherit (server) postgresql pguri port basedir SCM_UPGRADE_MODE SCM_VERBOSE SCM_PG_UPGRADE SCM_ISTEMP;
        inherit guid name scm_type transDepGuids location seq_page_cost random_page_cost effective_io_concurrency maintenance_io_concurrency comment;
        SCM_INSTALL_TABLESPACES = pkgs.lib.maybeEnv "SCM_INSTALL_TABLESPACES" "1";
        SCM_EFFECT_PROG = ../py/schematic/build_tablespace.py;
        buildInputs = [ rsync ] ++ (scm.buildDeps { inherit server dependencies preBuildInputs; });
    };
}
