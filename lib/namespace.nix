stdargs @ { scm, pkgs, stdenv, postgresql, python3, rsync, ... }:
args @ {
    guid, name, comment ? null,
    dependencies ? [], preBuildInputs ? null,
    ...
}:

rec {
    scm_type = "namespace";
    inherit guid name dependencies preBuildInputs comment;
    depends = scm.deps dependencies;
    transDepGuids = scm.getTransDepGuids depends;
    apply = { server }: scm.effect {
        inherit (server) postgresql pguri port basedir SCM_UPGRADE_MODE SCM_VERBOSE SCM_PG_UPGRADE SCM_ISTEMP;
        inherit guid name scm_type transDepGuids comment;
        SCM_EFFECT_PROG = ../py/schematic/build_namespace.py;
        buildInputs = [ rsync ] ++ (scm.buildDeps { inherit server dependencies preBuildInputs; });
    };
}
