let
  inherit (builtins) foldl' filter nixPath pathExists concatMap attrNames readDir concatLists;
  pkgs = import ./nixpkgs.nix {};
  callPackage = pkgs.lib.callPackageWith (pkgs // {
    inherit scm;
    python3 = callPackage ./python3 {};
  });
  # An attribute set of "${name}-${guid}" -> pkg for each package that can be found
  # by traversing all paths in the `nixPath`. Some of the attributes aren't guaranteed
  # to be valid packages, but all valid packages should appear in this attrset.
  packages = let
    valid-paths = filter (p:
      let
        path = /${p.path};
        parent = dirOf path;
        basename = baseNameOf path;
      in pathExists parent && ((readDir parent).${basename} or null) == "directory"
    ) nixPath;
    add-path = acc: p: let
      files-in-dir = readDir p.path;
      add-package = acc2: pkg: let
        pkg-path = /${p.path}/${pkg};
      in if pathExists /${pkg-path}/default.nix
        # given we're doing a left fold, the leftmost string in the nixPath will end up as the first element
        # but that'd make it the highest priority, and would override any path with the same name that comes later
        # thus, we must override the current path if there's one already in the accumulator,
        # as it will have higher priority
         then { ${toString pkg-path} = callPackage pkg-path {}; } // acc2
         else acc2;
    in
      foldl' add-package acc (attrNames files-in-dir);
  in
    foldl' add-path {} valid-paths;
  scm = rec {
    inherit callPackage packages;
    effect = callPackage ./effect.nix {};
    server = callPackage ./server.nix {};
    replica = callPackage ./replica.nix {};
    remoteServer = callPackage ./remote-server.nix {};
    database = callPackage ./database.nix {};
    revision = callPackage ./revision.nix {};
    schema = callPackage ./schema.nix {};
    extension = callPackage ./extension.nix {};
    tablespace = callPackage ./tablespace.nix {};
    namespace = callPackage ./namespace.nix {};
    pgconf = callPackage ./pgconf.nix {};
    pghba = callPackage ./pghba.nix {};
    sandbox = callPackage ./sandbox.nix {};
    derivative = callPackage ./derivative.nix {};
    # utility functions
    normalize-path = path: toString (if baseNameOf path == "default.nix" then dirOf path else path);
    pkg-from-path = path: packages.${normalize-path path};
    deps = dependencies: map pkg-from-path dependencies;
    getTransDepGuids = depends: pkgs.lib.unique (concatMap (d: if !(isNull d) then d.transDepGuids ++ [ d.guid ] else []) depends);  # guids of all transitive dependencies
    buildDeps = { server, dependencies, preBuildInputs } : let
      deps = map (p: server.scm-pkgs.${toString p}) dependencies;
      pre-build = if isNull preBuildInputs then [] else preBuildInputs { inherit (server) pkgs; };
    in
      concatLists [[server.apply] deps pre-build];
  };
in scm
