{
  url ? "https://github.com/NixOS/nixpkgs/archive/057f9aecfb71c4437d2b27d3323df7f93c010b7e.tar.gz",
  sha256 ? "sha256:1ndiv385w1qyb3b18vw13991fzb9wg4cl21wglk89grsfsnra41k",
  overlays ? []
}:
import (builtins.fetchTarball { inherit url sha256; }) {
  overlays = [
    (import ./nixpkgs-overlay.nix)
  ] ++ overlays;
}
