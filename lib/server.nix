stdargs @ { scm, python3, rsync, openssl, lsof, ... }:
args @ {
    postgresql,
    guid, name, dbname, port, user, password,
    SCM_ISTEMP ? false,
    SCM_VERBOSE ? false,
    SCM_UPGRADE_MODE ? null,
    SCM_PG_UPGRADE ? "0",
    ...
}: let
  inherit (builtins) mapAttrs getEnv;
  server = rec {
    scm_type = "server";
    dependencies = [];
    depends = scm.deps dependencies;
    transDepGuids = scm.getTransDepGuids depends;
    SCM_UPGRADE_MODE = if args ? SCM_UPGRADE_MODE && args.SCM_UPGRADE_MODE != null then args.SCM_UPGRADE_MODE else (pkgs.lib.maybeEnv "SCM_UPGRADE_MODE" null);
    inherit postgresql guid name dbname port user password SCM_ISTEMP SCM_VERBOSE;
    SCM_PG = getEnv "SCM_PG";
    SCM_PG_UPGRADE = getEnv "SCM_PG_UPGRADE";
    host = "localhost";
    basedir = with (import ./nixpkgs.nix {}); (
        if SCM_PG_UPGRADE != "1"
        then "${SCM_PG}/${name}-${guid}"
        else "${SCM_PG}/${name}-${guid}-upgrade"
    );
    datadir = "${basedir}/data";
    pguri = "postgresql://${user}:${password}@${host}:${port}/${dbname}";
    pguri_postgres = "postgresql://${user}:${password}@${host}:${port}/postgres";
    pkgs =  (import ./nixpkgs.nix {
        overlays = [
            (self: super: {
                inherit postgresql;
            })
        ];
    });
    scm-pkgs = mapAttrs (_name: pkg: pkg.apply { inherit server; }) scm.packages;
    apply = scm.effect rec {
        inherit postgresql dbname port user password SCM_ISTEMP SCM_VERBOSE SCM_UPGRADE_MODE SCM_PG_UPGRADE;
        inherit guid name scm_type transDepGuids SCM_PG host basedir datadir;
        inherit pguri pguri_postgres;
        SCM_EFFECT_PROG = ../py/schematic/build_server.py;
        buildInputs = [ rsync openssl lsof ];
        propagatedBuildInputs = [
            postgresql
            python3
        ];
    };
  }; in
  server
