stdargs @ { scm, pkgs, stdenv, postgresql, python3, rsync, ... }:
args @ {
    guid, name, rules, comment ? null, basefiles ? null,
    dependencies ? [], preBuildInputs ? null,
    ...
}:

rec {
    scm_type = "pghba";
    inherit guid name dependencies preBuildInputs basefiles comment rules;
    depends = scm.deps dependencies;
    transDepGuids = scm.getTransDepGuids depends;
    apply = { server }: scm.effect {
        inherit (server) postgresql pguri port basedir SCM_UPGRADE_MODE SCM_VERBOSE SCM_PG_UPGRADE SCM_ISTEMP;
        inherit guid name scm_type transDepGuids comment basefiles;
        rules = builtins.toJSON rules;
        SCM_EFFECT_PROG = ../py/schematic/build_pghba.py;
        buildInputs = [ rsync ] ++ (scm.buildDeps { inherit server dependencies preBuildInputs; });
    };
}
