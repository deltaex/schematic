{ modulepath }: let
  scm = import ./default.nix;
  database = scm.callPackage modulepath { };
in database.apply
