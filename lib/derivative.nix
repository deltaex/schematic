stdargs @ { scm, pkgs, ... }:

let
    dbpath = (/. + (builtins.getEnv "SCM_DERIVATIVE_TARGET"));
    dbobj = scm.pkg-from-path dbpath; 
in (scm.database {
    guid = builtins.getEnv "SCM_GUID";
    name = ''${dbobj.name}-derivative'';
    server = scm.server rec {
        postgresql = dbobj.server.postgresql;
        guid = builtins.getEnv "SCM_GUID";
        name = ''${dbobj.server.name}-derivative'';
        dbname = dbobj.server.dbname;
        port = builtins.getEnv "SCM_PORT";
        user = dbobj.server.user;
        password = dbobj.server.password;
        SCM_ISTEMP = true;
        SCM_VERBOSE = pkgs.lib.maybeEnv "SCM_VERBOSE" false;
        SCM_UPGRADE_MODE = "declarative";
    };
    dependencies = dbobj.dependencies;
    preBuildInputs = dbobj.preBuildInputs;
})
