{ pkgs }:
rec {
  buildPostgreSQL = (import ./postgresql);

  postgresql_releases = builtins.fromJSON (builtins.readFile ./postgresql/postgresql-releases.json);

  get_icu = date: version:
    if date > "2020-11-09" then
      pkgs.icu
    else
      # cf.
      # https://www.postgresql.org/message-id/CA%2BhUKG%2BXnrPeVJaSE2cscA3ouvxeNzwTGU-cHpAKd-Jfzew8uA%40mail.gmail.com
      # https://github.com/unicode-org/icu/commit/c3fe7e09d844
      pkgs.icu67;

  to_derivation = release: pkgs.callPackage buildPostgreSQL rec {
    version = release.version;
    sha256 = release.hash;
    date = release.date;
    icu = get_icu date version;
  };

  new_attribute = release: {
      name = "postgresql_${(builtins.replaceStrings [ "." ] [ "_" ] release.version)}";
      value = to_derivation release;
  };

  filtered_derivations = builtins.map (v: (new_attribute v).value) (pkgs.lib.filter (value: value.date >= "2021-09-27") postgresql_releases);

  full_shell = pkgs.mkShell { name = "fullshell"; buildInputs = filtered_derivations; };

  postgresql_attrs = builtins.listToAttrs (builtins.map new_attribute postgresql_releases);

  getConcatVer =
    ver: pkgs.lib.toInt (pkgs.lib.strings.concatStrings (pkgs.lib.versions.splitVersion ver));

  major_version_groups = builtins.groupBy (release: pkgs.lib.versions.major release.version) postgresql_releases;

  pvergrp = builtins.mapAttrs (k: v: builtins.map (e: e.version) v) major_version_groups;

  major_version_derivations = builtins.mapAttrs (k: v: builtins.map to_derivation v) major_version_groups;

  major_version_shells = builtins.mapAttrs
                           (k: v: pkgs.mkShell { name = "postgresql_test_shell_${k}"; buildInputs = v; })
                           major_version_derivations;

  largeeach = builtins.mapAttrs (
    n: v: builtins.elemAt (builtins.sort (p: q: (getConcatVer p) > (getConcatVer q)) v) 0
  ) pvergrp;

  default_releases = (
    pkgs.lib.attrsets.mapAttrs' (
      n: v:
      pkgs.lib.attrsets.nameValuePair "postgresql_${n}"
        postgresql_attrs."postgresql_${(builtins.concatStringsSep "_" (pkgs.lib.versions.splitVersion v))}"
    ) largeeach
  );

  all_releases = postgresql_attrs // default_releases // {
    postgresql = default_releases.postgresql_16;
  };
}
