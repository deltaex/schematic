stdargs @ { scm, pkgs, rsync, ... }:
args @ {
    guid, name, upgrade_sql, settings, basefiles ? null, autocommit ? false,
    dependencies ? [], preBuildInputs ? null,
    ...
}:

rec {
    scm_type = "extension";
    inherit guid name dependencies upgrade_sql preBuildInputs basefiles autocommit settings;
    depends = scm.deps dependencies;
    transDepGuids = scm.getTransDepGuids depends;
    apply = { server }: scm.effect {
        inherit (server) postgresql pguri port basedir SCM_UPGRADE_MODE SCM_VERBOSE SCM_PG_UPGRADE SCM_ISTEMP;
        inherit guid transDepGuids upgrade_sql name scm_type basefiles autocommit ;
        settings = builtins.toJSON settings;
        SCM_EFFECT_PROG = ../py/schematic/build_extension.py;
        buildInputs = [ rsync ] ++ (scm.buildDeps { inherit server dependencies preBuildInputs; });
    };
}
