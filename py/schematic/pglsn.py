import re

lsn_regex = re.compile(r'^[0-9A-Fa-f]{1,8}/[0-9A-Fa-f]{1,8}$')

def is_lsn(text: str) -> bool:
    return bool(re.match(lsn_regex, text))

def lsn_to_hex16(lsn):
    ''' Convert a textual LSN to an hex string.
    >>> lsn_to_hex16('0/17E2C48')
    '00000000017E2C48'
    >>> lsn_to_hex16('8925/38C3A4C0')
    '0000892538C3A4C0'
    >>> lsn_to_hex16('A/01234567')
    '0000000A01234567'
    >>> lsn_to_hex16('A/1234567')
    '0000000A01234567'
    >>> lsn_to_hex16(None)
    '''
    return ''.join(map(lambda x: x.rjust(8, '0'), lsn.split('/'))) if lsn is not None else None

def lsn_to_int(lsn, cursor):  # noqa: ARG001
    ''' Convert a textual LSN to an integer.
    >>> lsn_to_int('0/17E2C48', None)
    25046088
    >>> lsn_to_int('8925/38C3A4C0', None)
    150792959141056
    >>> lsn_to_int('A/01234567', None)
    42968761703
    >>> lsn_to_int('A/1234567', None)
    42968761703
    >>> lsn_to_int(52636680, None)
    52636680
    >>> lsn_to_int(None, None)
    '''
    if isinstance(lsn, int):
        return lsn
    return int(lsn_to_hex16(lsn), base=16) if lsn is not None else None

def lsn_to_tsn(lsn):
    ''' Encode a textual LSN to 16 digit hex value TSN.
    >>> lsn_to_tsn('0/17E2C48')
    '00000000017E2C48'
    >>> lsn_to_tsn('8925/38C3A4C0')
    '0000892538C3A4C0'
    >>> lsn_to_tsn('A/01234567')
    '0000000A01234567'
    >>> lsn_to_tsn('A/1234567')
    '0000000A01234567'
    >>> lsn_to_tsn(None)
    '''
    return ''.join(map(lambda x: x.rjust(8, '0'), lsn.split('/'))) if lsn is not None else None

def tsn_to_lsn(tsn):
    ''' Encode a 16 digit hex value TSN to a textual LSN.
    >>> tsn_to_lsn('00000000017E2C48')
    '00000000/017E2C48'
    >>> tsn_to_lsn('0000892538C3A4C0')
    '00008925/38C3A4C0'
    >>> tsn_to_lsn('0000000A01234567')
    '0000000A/01234567'
    >>> tsn_to_lsn('0000000A01234567')
    '0000000A/01234567'
    >>> tsn_to_lsn(None)
    '''
    return f'{tsn[0:8]}/{tsn[8:16]}' if tsn is not None else None

def encode_tsn(lsn):
    ''' Encode an transaction sequence number ("tsn") from a postgresql lsn.
        :lsn: postgresql log sequence number

    >>> encode_tsn('A/1234567')
    '0000000A01234567'
    >>> encode_tsn('A/01234567')
    '0000000A01234567'
    >>> encode_tsn('A/01234567')
    '0000000A01234567'
    >>> encode_tsn('A/01234567')
    '0000000A01234567'
    >>> encode_tsn('A/01234567')
    '0000000A01234567'
    >>> encode_tsn(42968761703)
    '0000000A01234567'
    '''
    return '%0.16X' % lsn_to_int(lsn, None) if lsn is not None else None  # pylint: disable=consider-using-f-string
