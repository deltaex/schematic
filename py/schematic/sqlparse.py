import json
import re
import sys
from dataclasses import dataclass

import pglast


@dataclass
class SqlStatement:
    text: str
    data: dict

def txt2stmt(text):
    return SqlStatement(text, parse_dict(text))

def parse_statements(text):
    '''Parse a sequence of sql statements into SqlStatement objects. Also see pglast.split. '''
    for stmt in parse_dict(text)['stmts']:
        offset = stmt.get('stmt_location', 0)
        length = stmt.get('stmt_len')
        stmt_text = text[offset:offset + length]
        yield SqlStatement(stmt_text, stmt)

def parse_create_table_names(text):
    # TODO: replace this implementation with pglast  # pylint: disable=fixme
    for stmt in parse_statements(text):
        match = re.match(r'CREATE( UNLOGGED)? TABLE (?P<tablename>(\w+\.)\w+)\W', stmt.text)
        if match:
            yield match.group('tablename')

def find_statement_match(src_text, statement):
    if statement in src_text:
        return statement
    statement = re.sub(r'\s+', ' ', statement).rstrip(' ')
    if statement in src_text:
        return statement

def schemaname(tablename):
    '''
    >>> schemaname('public.foo')
    'foo'
    >>> schemaname('bar.baz')
    'bar.baz'
    '''
    nameparts = tablename.split('.')
    if len(nameparts) == 2 and nameparts[0] == 'public':
        return nameparts[-1]
    return tablename

def safe_get(data, *keys):
    '''
    Access @*keys in nested @data dictionary and return `None` instead of `KeyError`.

    >>> safe_get(None, 'x')
    >>> safe_get({}, 'x')
    >>> safe_get({'x': 1}, 'x')
    1
    >>> safe_get({'x': {'y': 2}}, 'x', 'y')
    2
    >>> safe_get({'x': {'y': ''}}, 'x', 'y')
    ''
    >>> safe_get({'x': [{'y': 'z'}]}, 'x', 0, 'y')
    'z'
    >>> safe_get({'x': [{'y': 'z'}]}, 'x', '0', 'y')
    >>> safe_get(['a'], 1)
    '''
    val = data
    for key in keys:
        if val is None:
            return
        try:
            val = val[key]
        except (TypeError, IndexError, KeyError):
            val = None
    return val

def parse_dict(text):
    return json.loads(pglast.parser.parse_sql_json(text))

def parse_fk_ref_table(statement):
    '''
    >>> parse_fk_ref_table('ALTER TABLE ONLY public.f34 ADD CONSTRAINT sdf FOREIGN KEY (f34256) REFERENCES public.fj349_r435h(id) NOT VALID;')
    'public.fj349_r435h'
    >>> parse_fk_ref_table('ALTER TABLE ONLY f34 ADD CONSTRAINT SDF FOREIGN KEY (g456) REFERENCES foo.gh456_ert(id) NOT VALID;')
    'foo.gh456_ert'
    '''
    p = parse_dict(statement)
    ref = safe_get(p, 'stmts', 0, 'stmt', 'AlterTableStmt', 'cmds', 0, 'AlterTableCmd', 'def', 'Constraint', 'pktable')
    if ref:
        if 'schemaname' in ref:
            return '%s.%s' % (ref['schemaname'], ref['relname'])  # noqa: UP031
        return ref['relname']

def is_create_trigger(statement):
    '''
    >>> is_create_trigger('CREATE TRIGGER w AFTER INSERT ON q.z FOR EACH ROW EXECUTE FUNCTION x.y();')
    True
    '''
    p = parse_dict(statement)
    return bool(safe_get(p, 'stmts', 0, 'stmt', 'CreateTrigStmt'))

def is_create_index(statement):
    '''
    >>> is_create_index('CREATE INDEX revision_date_idx ON meta.revision USING btree (date);')
    True
    '''
    p = parse_dict(statement)
    return bool(safe_get(p, 'stmts', 0, 'stmt', 'IndexStmt'))

def is_add_constraint(statement):
    '''
    >>> is_add_constraint('ALTER TABLE country ADD CONSTRAINT country_id_upper CHECK (id = upper(id));')
    True
    >>> is_add_constraint('ALTER TABLE country ADD CONSTRAINT country_id_pkey PRIMARY KEY (id);')
    True
    >>> is_add_constraint('CREATE INDEX county_id_ix ON public.country USING btree(id);')
    False
    >>> is_add_constraint('ALTER TABLE country DROP COLUMN id;')
    False
    '''
    p = parse_dict(statement)
    for cmd in safe_get(p, 'stmts', 0, 'stmt', 'AlterTableStmt', 'cmds') or []:
        if safe_get(cmd, 'AlterTableCmd', 'def', 'Constraint'):
            return True
    return False

def eq_pglast_name(name, struct):
    '''
    >>> eq_pglast_name('public.wombat', [{'String': {'sval': 'public'}}, {'String': {'sval': 'wombat'}}])
    True
    >>> eq_pglast_name('wombat', [{'String': {'sval': 'public'}}, {'String': {'sval': 'wombat'}}])
    True
    >>> eq_pglast_name('foo.wombat', [{'String': {'sval': 'public'}}, {'String': {'sval': 'wombat'}}])
    False
    >>> eq_pglast_name('foo.wombat', [{'String': {'sval': 'wombat'}}])
    True
    '''
    np1 = name.split('.')[::-1]
    np2 = [safe_get(x, 'String', 'sval') for x in struct or []][::-1]
    return all(x == y for (x, y) in zip(np1, np2))

def is_create_func_table_arg(statement, table):
    '''
    >>> is_create_func_table_arg('CREATE FUNCTION public.squawk(r public.wombat) RETURNS text LANGUAGE sql AS $_$ SELECT 1; $_$;', 'public.wombat')  # pylint: disable=line-too-long
    True
    >>> is_create_func_table_arg('CREATE FUNCTION public.squawk(public.wombat) RETURNS text LANGUAGE sql AS $_$ SELECT 1; $_$;', 'public.wombat')
    True
    >>> is_create_func_table_arg('CREATE FUNCTION public.squawk(public.wombat, foo) RETURNS text LANGUAGE sql AS $_$ SELECT 1; $_$;', 'public.wombat')
    True
    >>> is_create_func_table_arg('CREATE FUNCTION public.squawk(x public.wombat, foo) RETURNS text LANGUAGE sql AS $_$ SELECT 1; $_$;', 'public.wombat')
    True
    >>> is_create_func_table_arg('CREATE FUNCTION public.squawk(foo, public.wombat) RETURNS text LANGUAGE sql AS $_$ SELECT 1; $_$;', 'public.wombat')
    True
    >>> is_create_func_table_arg('CREATE FUNCTION public.squawk(foo, x public.wombat) RETURNS text LANGUAGE sql AS $_$ SELECT 1; $_$;', 'public.wombat')
    True
    >>> is_create_func_table_arg('CREATE FUNCTION public.squawk(integer) RETURNS text LANGUAGE sql AS $_$ SELECT 1; $_$;', 'public.wombat')
    False
    '''
    try:
        p = parse_dict(statement)
    except pglast.parser.ParseError:
        # TODO: fix parser in pglast; happens when $ is used inside a function body to refer to a positional argument  # pylint: disable=fixme
        print(('failed to parse %s' % (statement.replace('\n', '')))[:120] + '', file=sys.stderr)
        return
    for param in safe_get(p, 'stmts', 0, 'stmt', 'CreateFunctionStmt', 'parameters') or []:
        if eq_pglast_name(table, safe_get(param, 'FunctionParameter', 'argType', 'names')):
            return True
    return False

def is_create_func_table_ret(statement, table):
    '''
    >>> is_create_func_table_ret('CREATE FUNCTION public.squawk() RETURNS public.wombat LANGUAGE sql AS $_$ SELECT 1; $_$;', 'public.wombat')
    True
    >>> is_create_func_table_ret('CREATE FUNCTION public.squawk(integer) RETURNS text LANGUAGE sql AS $_$ SELECT 1; $_$;', 'public.wombat')
    False
    >>> is_create_func_table_ret('CREATE FUNCTION public.squawk() RETURNS integer LANGUAGE sql AS $_$ SELECT 1; $_$;', 'public.wombat')
    False
    >>> is_create_func_table_ret('CREATE FUNCTION public.squawk(integer) RETURNS wombat LANGUAGE sql AS $_$ SELECT 1; $_$;', 'public.wombat')
    True
    '''
    try:
        p = parse_dict(statement)
    except pglast.parser.ParseError:
        # TODO: fix parser in pglast; happens when $ is used inside a function body to refer to a positional argument  # pylint: disable=fixme
        print(('failed to parse %s' % (statement.replace('\n', '')))[:120] + '', file=sys.stderr)
        return
    return eq_pglast_name(table, safe_get(p, 'stmts', 0, 'stmt', 'CreateFunctionStmt', 'returnType', 'names'))

def find_table_funcs(src_text, table):
    '''Return create function statements whose type signature depends on the given table.'''
    for stmt in parse_statements(src_text):
        if is_create_func_table_arg(stmt.txt, table) or is_create_func_table_ret(stmt.txt, table):
            yield stmt.txt

def get_rename_statement(stmt):
    '''
    >>> assert get_rename_statement(txt2stmt('ALTER SEQUENCE noexist RENAME TO newseq;'))
    >>> assert not get_rename_statement(txt2stmt('CREATE TABLE public.tbl1 ();'))
    '''
    return safe_get(stmt.data, 'stmts', 0, 'stmt', 'RenameStmt')

def get_drop_statement(stmt):
    '''
    >>> assert get_drop_statement(txt2stmt('DROP ACCESS METHOD noexist;'))
    >>> assert get_drop_statement(txt2stmt('DROP GROUP noexist;'))
    '''
    for kind in ('DropStmt', 'DropRoleStmt', 'DropOwnedStmt', 'DropSubscriptionStmt', 'DropTableSpaceStmt',
                 'DropUserMappingStmt', 'DropdbStmt'):
        r = safe_get(stmt.data, 'stmts', 0, 'stmt', kind)
        if r:
            return r

def is_compound_drop_statement(stmt):
    '''
    >>> assert not is_compound_drop_statement(txt2stmt('DROP GROUP noexist;'))
    >>> assert is_compound_drop_statement(txt2stmt('DROP OPERATOR + (int, int), ^ (int, int);'))
    '''
    drop = get_drop_statement(stmt)
    return len(safe_get(drop, 'objects') or []) > 1

def is_compound_rename_statement(stmt):
    '''
    >>> assert not is_compound_rename_statement(txt2stmt('ALTER SEQUENCE noexist RENAME TO newseq;'))
    >>> assert not is_compound_rename_statement(txt2stmt('ALTER TABLE public.tbl1 rename COLUMN a TO b;'))
    '''
    return (
        safe_get(stmt.data, 'stmts', 0, 'stmt', 'RenameStmt') and
        safe_get(stmt.data, 'stmts', 1, 'stmt', 'RenameStmt'))

def get_alter_schema_statement(stmt):
    ''' # noqa: D200
    >>> assert get_alter_schema_statement(txt2stmt('ALTER SEQUENCE nsp0.seq1 SET SCHEMA public;'))
    '''
    return safe_get(stmt.data, 'stmts', 0, 'stmt', 'AlterObjectSchemaStmt')

def is_compound_alter_schema_statement(stmt):
    ''' # noqa: D200
    >>> assert not is_compound_alter_schema_statement(txt2stmt('ALTER SEQUENCE nsp0.seq1 SET SCHEMA public;'))
    '''
    return (
        safe_get(stmt.data, 'stmts', 0, 'stmt', 'AlterObjectSchemaStmt') and
        safe_get(stmt.data, 'stmts', 1, 'stmt', 'AlterObjectSchemaStmt'))

def get_alter_sequence(stmt):
    '''
    >>> assert get_alter_sequence(txt2stmt('ALTER SEQUENCE noexist RESTART 123;'))
    >>> assert not get_alter_sequence(txt2stmt('CREATE TABLE public.tbl1 ();'))
    '''
    return safe_get(stmt.data, 'stmts', 0, 'stmt', 'AlterSeqStmt')

def get_alter_table(stmt):
    '''
    >>> assert get_alter_table(txt2stmt('ALTER TABLE public.tbl1 DROP COLUMN a;'))
    >>> assert not get_alter_table(txt2stmt('CREATE TABLE public.tbl1 ();'))
    '''
    return safe_get(stmt.data, 'stmts', 0, 'stmt', 'AlterTableStmt')

def is_compound_alter_table(stmt):
    '''
    >>> assert not is_compound_alter_table(txt2stmt('ALTER TABLE public.tbl1 DROP COLUMN a;'))
    >>> assert is_compound_alter_table(txt2stmt('ALTER TABLE public.tbl1 DROP COLUMN a, DROP COLUMN b;'))
    '''
    alter_table = safe_get(stmt.data, 'stmts', 0, 'stmt', 'AlterTableStmt')
    cmds = safe_get(alter_table, 'cmds')
    return alter_table and len(cmds) > 1

def parse_alter_table_drop_column(stmt):
    '''
    >>> parse_alter_table_drop_column(txt2stmt('ALTER TABLE public.tbl1 DROP COLUMN a;'))
    ['public', 'tbl1', 'a']
    >>> parse_alter_table_drop_column(txt2stmt('ALTER TABLE tbl1 DROP COLUMN a;'))
    [None, 'tbl1', 'a']
    '''
    alter_table = safe_get(stmt.data, 'stmts', 0, 'stmt', 'AlterTableStmt')
    cmds = safe_get(alter_table, 'cmds')
    if not alter_table or not cmds:
        return False
    assert len(cmds) == 1, 'expected single cmd, got %r' % cmds
    cmd = cmds[0]['AlterTableCmd']
    if cmd['subtype'] != 'AT_DropColumn':
        return False
    relation = alter_table['relation']
    return [relation.get('schemaname'), relation['relname'], cmd['name']]

def get_alter_domain(stmt):
    ''' # noqa: D200
    >>> assert get_alter_domain(txt2stmt('ALTER DOMAIN nodomain VALIDATE CONSTRAINT noconstraint;'))
    '''
    return safe_get(stmt.data, 'stmts', 0, 'stmt', 'AlterDomainStmt')

def get_alter_enum(stmt):
    ''' # noqa: D200
    >>> assert get_alter_enum(txt2stmt("ALTER TYPE noexist RENAME VALUE 'existing_enum_value' TO 'new_enum_value';"))
    '''
    return safe_get(stmt.data, 'stmts', 0, 'stmt', 'AlterEnumStmt')
