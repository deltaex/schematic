'''
Apply revision statements, gracefully handling idempotent cases in the presence of declarative schemas, like dropping
a column that doesn't exist in the declarative schema.
'''
import pglast
import psycopg2

from schematic import slog, sqlparse
from schematic.build_util import apply_sql_text, log_msg_upgrade_sql_text, onelinesql


def safe_apply_statements(pg, content, log_statements=1, log=slog.error):
    pg.execute('SAVEPOINT prevstate;')
    for stmt_text in pglast.split(content, with_parser=False):
        try:
            log_msg_upgrade_sql_text(stmt_text) if log_statements else 0
            apply_sql_text(pg, stmt_text)
            pg.execute('SAVEPOINT prevstate;')
        except psycopg2errs as ex:  # pylint: disable=catching-non-exception
            pg.execute('ROLLBACK TO SAVEPOINT prevstate;')
            if not is_error_ok(ex, stmt_text, log=log):
                raise

def print_error(ex, log, stmt, headline, detail):
    log(f'ERROR: {headline}')
    log(f'    detail: {detail}')
    log(f'    statement: {onelinesql(stmt.text)}')
    log(f'    error: {ex.pgcode} {type(ex).__name__} {ex}')

psycopg2errs = tuple( # see psycopg/sqlstate_errors.h, https://www.postgresql.org/docs/current/errcodes-appendix.html
    getattr(psycopg2.errors, name) for name in (
        'DuplicateAlias', 'DuplicateColumn', 'DuplicateDatabase', 'DuplicateFunction', 'DuplicateObject',
        'DuplicateSchema', 'DuplicateTable', 'UndefinedColumn', 'UndefinedFunction', 'UndefinedObject',
        'UndefinedParameter', 'UndefinedTable', 'InvalidSchemaName', 'InvalidParameterValue',
        'InvalidColumnReference', 'InvalidDatabaseDefinition', 'InvalidFunctionDefinition', 'InvalidSchemaDefinition',
        'InvalidTableDefinition', 'InvalidObjectDefinition',))

def is_error_ok(ex, stmt_text, log):
    stmt = sqlparse.txt2stmt(stmt_text)
    if sqlparse.is_compound_drop_statement(stmt):
        print_error(ex, log, stmt,
                    'must split the compound drop statement into individual statements',
                    'got an error, but unsure which part the error applies to')
        return False
    if sqlparse.get_drop_statement(stmt):
        if isinstance(ex, (psycopg2.errors.UndefinedTable, psycopg2.errors.UndefinedColumn,  # pylint: disable=no-member
                           psycopg2.errors.UndefinedObject, psycopg2.errors.InvalidSchemaName,  # pylint: disable=no-member
                           psycopg2.errors.UndefinedFunction)):  # pylint: disable=no-member
            return True
    if sqlparse.get_alter_table(stmt) or sqlparse.get_alter_sequence(stmt):
        if isinstance(ex, psycopg2.errors.UndefinedTable):  # pylint: disable=no-member
            return True
    if sqlparse.is_compound_alter_table(stmt):
        print_error(ex, log, stmt,
                    'must split the compound statement into individual statements',
                    'got an error, but unsure which part the error applies to')
        return False
    if sqlparse.parse_alter_table_drop_column(stmt):
        if isinstance(ex, psycopg2.errors.UndefinedColumn):  # pylint: disable=no-member
            return True
    if sqlparse.is_compound_rename_statement(stmt):
        print_error(ex, log, stmt,
                    'must split the rename statement into individual statements',
                    'got an error, but unsure which part the error applies to')
        return False
    if sqlparse.get_rename_statement(stmt):
        if isinstance(ex, (psycopg2.errors.UndefinedTable, psycopg2.errors.UndefinedColumn,  # pylint: disable=no-member
                           psycopg2.errors.UndefinedObject, psycopg2.errors.InvalidSchemaName,  # pylint: disable=no-member
                           psycopg2.errors.UndefinedFunction)):  # pylint: disable=no-member
            return True
    if sqlparse.is_compound_alter_schema_statement(stmt):
        print_error(ex, log, stmt,
                    'must split the schema rename statement into individual statements',
                    'got an error, but unsure which part the error applies to')
        return False
    if sqlparse.get_alter_schema_statement(stmt):
        if isinstance(ex, (psycopg2.errors.UndefinedTable, psycopg2.errors.UndefinedColumn,  # pylint: disable=no-member
                           psycopg2.errors.UndefinedObject, psycopg2.errors.InvalidSchemaName,  # pylint: disable=no-member
                           psycopg2.errors.UndefinedFunction)):  # pylint: disable=no-member
            return True
    if sqlparse.get_alter_domain(stmt):
        if isinstance(ex, psycopg2.errors.UndefinedObject):  # pylint: disable=no-member
            return True
    if sqlparse.get_alter_enum(stmt):
        if isinstance(ex, psycopg2.errors.UndefinedObject):  # pylint: disable=no-member
            return True
        if isinstance(ex, psycopg2.errors.InvalidParameterValue):  # pylint: disable=no-member
            return True  # happens when renaming an enum label that doesn't exist
    print_error(ex, log, stmt,
                'a revision statement caused an unhandled error',
                'check that the statement is correct and raise a bug report if you believe it should be handled by schematic')  # pylint: disable=line-too-long
    return False
