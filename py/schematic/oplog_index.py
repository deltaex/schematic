# pyright: strict
from dataclasses import dataclass
from typing import Iterator, List, Optional, Tuple

from schematic.oplog import Operation
from schematic.oplog_s3 import OplogReference, extend_reference


@dataclass(slots=True)
class Transaction:
    identifier: int
    first_tsn: str
    commit_tsn: str
    operations: OplogReference

def group_transactions(stream: Iterator[Tuple[Operation, OplogReference]]) -> Iterator[Transaction]:
    item = next(stream, None)
    while item is not None:
        op, reference = item
        ret = Transaction(op['txn'], op['tsn'], op['tsn'], reference)
        while (item := next(stream, None)) is not None:
            op, reference = item
            if op['txn'] != ret.identifier:
                break
            ret.commit_tsn = op['tsn']
            extend_reference(ret.operations, reference)
        yield ret

@dataclass(slots=True)
class IdCommit:
    identifier: int
    commit_tsn: str

def pop_concurrent(
    commit_tsn: str, transactions: List[Transaction]
) -> Optional[Transaction]:
    if transactions and transactions[-1].first_tsn <= commit_tsn:
        return transactions.pop()

def indexed_oplog(
    stream: Iterator[Tuple[Operation, OplogReference]]
) -> Tuple[List[IdCommit], List[Transaction]]:
    transactions: List[Transaction] = list(group_transactions(stream))
    by_commit = [IdCommit(transaction.identifier, transaction.commit_tsn) for transaction in transactions]
    def tsn_acessor(t: Transaction) -> str:
        return t.first_tsn
    transactions.sort(key=tsn_acessor, reverse=True)
    return (by_commit, transactions)
