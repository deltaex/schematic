# This module will be used to store functions and variables used by multiple commands in the old
# `scm_cli.py`

import codecs
import contextlib
import datetime
import functools
import glob
import importlib.util
import io
import json
import multiprocessing
import os
import re
import shutil
import signal
import socket
import subprocess
import sys
import tempfile
import time
import traceback
from collections import OrderedDict, defaultdict

import psutil
import psycopg2
from clint.textui import colored
from toolz import dicttoolz

from schematic import env, fs, guids, pgcn, prompt_yesno, slog
from schematic.build_revision import revision_exists
from schematic.build_util import (
    alter_system_set, ensure_pg_running, get_pid, mkdir, pg_ctl_fn, pg_is_ready,
    server_needs_major_upgrade, show_pg_param)
from schematic.pguri import pguri_from_basedir

nix_store_path: str = os.environ.get('NIX_STORE', '/nix/store/')
scm_var_path: str = env.get_str('SCM_VAR', os.path.expanduser('~/var')) # type: ignore reportAssignmentType

def parse_derivation_line(line, prefix=nix_store_path):
    ''' Parse output of nix shell commands referencing a .drv output.

    >>> parse_derivation_line('  /nix/store/i96g1m88fi3xhara7q9n86fp3f0l1i0d-country.drv')
    '/nix/store/i96g1m88fi3xhara7q9n86fp3f0l1i0d-country.drv'
    >>> parse_derivation_line('/nix/store/i96g1m88fi3xhara7q9n86fp3f0l1i0d-country.drv\\n')
    '/nix/store/i96g1m88fi3xhara7q9n86fp3f0l1i0d-country.drv'
    >>> parse_derivation_line(' blah')
    >>> parse_derivation_line(None)
    '''
    match = re.match(r'\s*(?P<drv>[^\s]+\.drv)\s*', line) if line else None
    drv = match.group('drv') if match else ''
    return drv if drv.startswith(prefix) else None

def parse_nix_version(text):
    '''
    >>> parse_nix_version('1.0')
    (1, 0)
    >>> parse_nix_version('2.4')
    (2, 4)
    >>> parse_nix_version('2.11')
    (2, 11)
    >>> parse_nix_version('nix (Nix) 2.6.0')
    (2, 6, 0)
    '''
    if match := re.search(r'[\d\.]+', text):
        return tuple(map(int, match.group(0).split('.')))
    assert 'failed to parse nix version string %r' % text

def get_nix_version():
    return parse_nix_version(subprocess.check_output(['nix', '--version']).decode())

def clear_environment():
    ''' Delete all environmental variables. '''
    for key in os.environ:
        del os.environ[key]

def same_python_exe(exe, this_exe=sys.executable):
    ''' Check if the argument refers to the same python executable.
    >>> assert same_python_exe('/nix/store/0lwl5nihn8qj62l5j2rlmq56glyq4djb-python3-3.8.1-env/bin/python3.8',\
        '/nix/store/0lwl5nihn8qj62l5j2rlmq56glyq4djb-python3-3.8.1-env/bin/python3')
    >>> assert same_python_exe('/nix/store/0lwl5nihn8qj62l5j2rlmq56glyq4djb-python3-3.8.1-env/bin/python3.8',\
        '/nix/store/0lwl5nihn8qj62l5j2rlmq56glyq4djb-python3-3.8.1-env/bin/python3.8')
    >>> assert same_python_exe('/nix/store/0lwl5nihn8qj62l5j2rlmq56glyq4djb-python3-3.8.1-env/bin/python3.8',\
        '/nix/store/0lwl5nihn8qj62l5j2rlmq56glyq4djb-python3-3.8.1-env/bin/python3.8')
    >>> assert same_python_exe('/nix/store/0lwl5nihn8qj62l5j2rlmq56glyq4djb-python3-3.8.1-env/bin/python3.8',\
        '/nix/store/0lwl5nihn8qj62l5j2rlmq56glyq4djb-python3-3.8.1-env/bin/python')
    >>> assert not same_python_exe('/nix/store/99999999999999l5j2rlmq56glyq4djb-python3-3.8.1-env/bin/python3.8',\
        '/nix/store/0lwl5nihn8qj62l5j2rlmq56glyq4djb-python3-3.8.1-env/bin/python')
    '''
    if not exe:
        return
    exe = os.path.realpath(exe)
    this_exe = os.path.realpath(this_exe)
    if exe == this_exe:
        return True
    if exe.startswith(nix_store_path) and this_exe.startswith(nix_store_path):
        if exe.startswith(this_exe) or this_exe.startswith(exe):
            return True
    return False

def parse_drv(drv, recursive=False):
    ''' Parse a nix .drv file using 'nix show derivation'. '''
    nix_version = get_nix_version() or ()
    if nix_version >= (2, 15):
        cmd = ['nix', 'derivation', 'show', '%s^out' % drv, '--extra-experimental-features', 'nix-command']
    else:
        cmd = ['nix', 'show-derivation', drv]
        if nix_version >= (2, 4):
            cmd += ['--extra-experimental-features', 'nix-command']
    if recursive:
        cmd += ['-r']
    ret = json.loads(subprocess.check_output(cmd))
    for depdrv, depobj in ret.items():
        depobj['drv_path'] = depdrv
    return ret

def get_relative_nix_mod(path, scm_path=env.get_str('SCM_PATH')):
    return os.path.join(scm_path, path) # type: ignore reportAttributeAccessIssue

def unquote_bash(text):
    r'''
    >>> unquote_bash('hello \\"world\\"')
    'hello "world"'
    >>> unquote_bash('hello \\nworld')
    'hello \nworld'
    '''
    (ret, _) = codecs.escape_decode(text)
    return ret.decode('utf8') # type: ignore reportAttributeAccessIssue

def parse_shell_env_line(line):
    ''' Parse a shell line generated from `export -p'.
    >>> parse_shell_env_line('declare -x STRINGS="strings"')
    ('STRINGS', 'strings')
    >>> parse_shell_env_line('')
    (None, None)
    >>> parse_shell_env_line('declare -x rules="[{\"addr\":\"127.0.0.0/24\",\"auth\":\"md5\"}]"')
    ('rules', '[{"addr":"127.0.0.0/24","auth":"md5"}]')
    '''
    if match := re.match(r'declare (\-x )(?P<key>[a-zA-Z_]+[a-zA-Z0-9_]*)="(?P<value>.*)"', (line or '').strip()):
        return match.group('key'), unquote_bash(match.group('value').replace('\\$', '$'))
    return (None, None)

def parse_shell_env(path):
    ''' Parse ouput of shell 'export -p'. '''
    ret = {}
    with open(path) as fp:
        for line in fp:
            (key, value) = parse_shell_env_line(line)
            if key:
                ret[key] = value
    return ret

class FakeTTY(io.StringIO):
    def isatty(self):  # useful to get clint.textui.colored to include colors
        return True

def apply_effect(drv_obj, build_guid, environ=None, verbose=False):
    ''' Execute a schematic effect. Expects to be called in a subprocess since it mutates interpreter state. '''
    env = drv_obj['env']
    effect_exec = env.get('SCM_EFFECT_EXEC')
    effect_prog = env.get('SCM_EFFECT_PROG')
    def write_debug_logs(stdout, stderr, stacktrace=None):
        # pylint: disable=consider-using-with
        build_dir = os.path.join(scm_var_path, '.scm-builds', build_guid) # type: ignore reportAttributeAccessIssue
        mkdir(build_dir)
        open(os.path.join(build_dir, 'stdout.log'), 'w').write(colored.clean(stdout)) \
            if stdout.strip() else 0
        open(os.path.join(build_dir, 'stderr.log'), 'w').write(colored.clean(stderr)) \
            if stderr and stderr.strip() else 0
        open(os.path.join(build_dir, 'stacktrace.log'), 'w').write(stacktrace) \
            if stacktrace and stacktrace.strip() else 0
    if same_python_exe(effect_exec):  # optimization: save python startup cost by using this process
        sys.argv[:] = [effect_exec, effect_prog]
        clear_environment()
        os.environ.update(environ) if environ else None
        os.environ.update(parse_shell_env(os.path.join(env['out'], 'transitive.env')))
        real_stderr, real_stdout = sys.stderr, sys.stdout
        fake_stderr, fake_stdout = sys.stderr, sys.stdout = FakeTTY(), FakeTTY()
        spec = importlib.util.spec_from_file_location('effect_prog', effect_prog)
        effect_mod = importlib.util.module_from_spec(spec) # type: ignore reportAttributeAccessIssue
        spec.loader.exec_module(effect_mod) # type: ignore reportAttributeAccessIssue
        try:
            effect_mod.main()
            if verbose:
                fake_stderr.seek(0)
                fake_stdout.seek(0)
                real_stderr.write(fake_stderr.read())
                real_stdout.write(fake_stdout.read())
        except BaseException:
            fake_stderr.seek(0)
            fake_stdout.seek(0)
            stdout_content = fake_stdout.read()
            stderr_content = fake_stderr.read()
            real_stdout.write(stdout_content) if stdout_content.strip() else 0
            real_stderr.write(stderr_content) if stderr_content.strip() else 0
            stacktrace = traceback.format_exc()
            real_stderr.write(stacktrace)
            write_debug_logs(stdout_content, stderr_content, stacktrace=stacktrace)
            raise
    else:
        scm_effect = os.path.join(drv_obj['outputs']['out']['path'], 'scm-effect')
        cmd = [scm_effect]
        with subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, text=True, env=environ) as p:
            (stdout, stderr) = p.communicate()
            if p.returncode != 0:
                write_debug_logs(stdout, stderr)
                raise subprocess.CalledProcessError(p.returncode, cmd, output=stdout, stderr=stderr)
            if verbose:
                sys.stderr.write(stderr) if stderr else None
                sys.stdout.write(stdout) if stdout else None

def instantiate_module(nix_module, environ=None, verbose=False):
    ''' Build a schematic object. '''
    parallelism = env.get_str('SCM_PARALLELISM', 'auto')
    cmd = [
        'nix-instantiate', get_relative_nix_mod('lib/eval.nix'), '--arg', 'modulepath', nix_module,
        '--show-trace', '--max-jobs', parallelism]
    build_environ = dicttoolz.merge(os.environ, environ or {})
    drv_paths = []
    lines = []
    with subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, text=True, env=build_environ) as p:
        while True:
            line = p.stdout.readline() # type: ignore reportAttributeAccessIssue
            lines.append(line)
            if not line:
                break
            if verbose and not line.startswith('''warning: you did not specify '--add-root';'''):
                print(line, file=sys.stderr, end='')
            drv_path = parse_derivation_line(line)
            if drv_path:
                drv_paths.append(drv_path)
        p.wait()
        if p.returncode != 0:
            error_module = environ.get('SCM_SANDBOX_MODULES') or nix_module # type: ignore reportAttributeAccessIssue
            print(f'Error constructing {error_module}', file=sys.stderr)
            print(''.join(lines), file=sys.stderr)
            raise subprocess.CalledProcessError(p.returncode, cmd)
    assert len(drv_paths) == 1, f'expected exactly one drv path: {drv_paths}'
    return drv_paths[0]

def get_applied_revision_state(basedir, port, pguri):
    ''' Return a set of revision guids that have been applied against a database and flag indicating binary replica. '''
    if pg_is_ready(basedir, port):
        with pgcn.connect(pguri) as pg:
            guids = set(
              pg.execute('SELECT array_agg(guid) FROM meta.revision;').scalar() or []
            ) if revision_exists(pg) else None
            is_replica = pg.execute('SELECT pg_is_in_recovery();').scalar()
            return guids, is_replica
    return None, None

def get_max_effect_procs(parallelism=env.get_str('SCM_PARALLELISM', 'auto'), cpu_count=None, limit=50):
    ''' Get number of processes to use for applying effects.
        Limit is defined so that postgresql max_connections parameter isn't exceeded on computers with high core count.
    >>> get_max_effect_procs(parallelism=1)
    1
    >>> get_max_effect_procs(parallelism=7)
    7
    >>> get_max_effect_procs(parallelism=0, cpu_count=5)
    10
    >>> get_max_effect_procs(parallelism='auto', cpu_count=5)
    10
    >>> get_max_effect_procs(parallelism='', cpu_count=5)
    10
    >>> get_max_effect_procs(parallelism='', cpu_count=999)
    50
    '''
    if isinstance(parallelism, int) and parallelism > 0:
        return min(parallelism, limit)
    cpu_count = cpu_count or multiprocessing.cpu_count()
    return min(cpu_count * 2, limit)

class EvalException(Exception):

    def __init__(self, proc, exitcode, main_drv, main_obj):
        super().__init__()
        self.proc = proc
        self.exitcode = exitcode
        self.main_drv = main_drv
        self.main_obj = main_obj

def debug_effect_error(main_drv, main_obj, effect_obj, proc):
    ''' Debug output for an error generated from an effect. '''
    effect_drv = effect_obj['drv_path']
    procenv = effect_obj['env']
    print(f'Error: Process {proc.pid} exited with code {proc.exitcode}', file=sys.stderr)
    print(f'    drv: {effect_drv}', file=sys.stderr)
    for key in ('scm_type', 'guid', 'name', 'upgrade_sql', 'out'):
        if procenv.get(key):
            print(f'    {key}: {procenv[key]}', file=sys.stderr)
    if procenv['scm_type'] == 'tablespace' and proc and proc.exitcode == 59:
        print('    hint: Use --tablespaces=0 to skip tablespaces for testing', file=sys.stderr)
    build_guid = effect_obj.get('_build_guid')
    if build_guid:
        build_dir = os.path.join(scm_var_path, '.scm-builds', build_guid) # type: ignore[reportGeneralTypeIssues]
        def print_log_file(name, label):
            logpath = os.path.join(build_dir, name)
            if os.path.exists(logpath):
                print(f'    {label}: {logpath}', file=sys.stderr)
        print_log_file('stdout.log', 'stdout')
        print_log_file('stderr.log', 'stderr')
        print_log_file('stacktrace.log', 'stacktrace')
    raise EvalException(proc, proc.exitcode, main_drv, main_obj)

def realize_derivation(derivation, environ=None, verbose=False):
    parallelism = env.get_str('SCM_PARALLELISM', 'auto')
    cmd = [
        'nix-store', '--realise', derivation, '--max-jobs', parallelism,
        '--option', 'sandbox', 'false']  # sandbox off isn't necessary, but improves performance up to ~70%
    build_environ = dicttoolz.merge(os.environ, environ) if environ else None
    with subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, text=True, env=build_environ) as p:
        while True:
            line = p.stdout.readline() # type: ignore reportAttributeAccessIssue
            if not line:
                break
            if verbose:
                print(line, file=sys.stderr, end='')
        p.wait()
        if p.returncode != 0:
            raise subprocess.CalledProcessError(p.returncode, cmd)

def stop_server_and_wait(basedir, wait_time=None, verbose=False):
    ''' Stop the possibly running postgres server and wait until it fully
    stops.  If after `wait_time` passes the server has not stopped, return
    False signaling a failure to stop the database, otherwise return True.
    '''
    wait_time = wait_time or 60*60*2
    pg_ctl_fn(basedir, 'stop', shutdown_mode='fast', quiet=not verbose)
    pidfile = os.path.join(basedir, 'data/postmaster.pid')
    pid = get_pid(pidfile)
    if not pid:
        return True
    start = time.time()
    while (time.time() - start) <= wait_time:
        if not get_postgres_process(pid, pidfile_to_remove=pidfile):
            return True  # Server stopped
        time.sleep(5)
    return False  # Waited too long and it hasn't stopped

def get_replication_slots_lag(pg, use_pg_last_commit_lsn=False, all_slots=False):
    '''Query for slots that are still lagging and by how much.

    If the function pg_last_commit_lsn() is available, it is used to compute
    the lag more precisely. This function is added via patch, see:

     lib/postgresql/default.nix
     lib/postgresql/patches/*-expose-lsn-of-last-commit-via-pg_last_committed_x.patch
     https://www.postgresql.org/message-id/flat/CAAaqYe9QBiAu%2Bj8rBun_JKBRe-3HeKLUhfVVsYfsxQG0VqLXsA%40mail.gmail.com
    '''
    if use_pg_last_commit_lsn:
        lag_snippet = '''
            (pg_last_commit_lsn() - confirmed_flush_lsn) AS lag_bytes,
            pg_size_pretty(pg_last_commit_lsn() - confirmed_flush_lsn) AS lag_pretty,
            pg_size_pretty(pg_current_wal_lsn() - confirmed_flush_lsn) AS current_wal_lag_pretty
        '''
        lag_cols = 'lag_pretty, current_wal_lag_pretty'
    else:
        lag_snippet = '''
            (pg_current_wal_lsn() - confirmed_flush_lsn) AS lag_bytes,
            pg_size_pretty(pg_current_wal_lsn() - confirmed_flush_lsn) AS lag_pretty
        '''
        lag_cols = 'lag_pretty'
    filter_clause = '' if all_slots else 'WHERE lag_bytes IS NOT NULL AND abs(lag_bytes) > 0'
    return pg.execute(f'''
    WITH lag AS (
        SELECT
            slot_name,
            {lag_snippet}
        FROM pg_replication_slots
        WHERE slot_type = 'logical'
         AND temporary IS false
        ORDER BY 2
    )
    SELECT slot_name, {lag_cols} FROM lag {filter_clause};
    ''').fetchall()

def tabulate(headers, data):
    '''Arrange data in table form. Data must be two-dimensional.'''
    assert headers
    headers = list(map(str, headers))
    str_data = [[str(row[i]) for i in range(min(len(headers), len(row)))] for row in data]
    max_len_per_col = [max(len(str_data[i][j]) for i in range(len(str_data))) for j in range(len(headers))]
    max_len_per_col = [max(l, len(h)) for l, h in zip(max_len_per_col, headers)]
    table = ' | '.join([h.ljust(l) for h, l in zip(headers, max_len_per_col)])
    table += '\n' + '-' * len(table) + '\n'
    table += '\n'.join([' | '.join([v.ljust(l) for v, l in zip(row, max_len_per_col)]) for row in str_data])
    return table + '\n'

def poll_replication_slots_lag(pg, refresh_period=2):
    '''Poll logical replication slots for their lag and return once every all are caught up.'''
    use_pg_last_commit_lsn = pg.execute('''
        SELECT true
        FROM pg_catalog.pg_proc AS p LEFT JOIN pg_catalog.pg_namespace AS n ON n.oid = p.pronamespace
        WHERE n.nspname = 'pg_catalog' AND p.proname = 'pg_last_commit_lsn';
    ''').scalar()
    headers = ('Slot name', 'Lag')
    if use_pg_last_commit_lsn:
        headers += ('Current WAL Lag',)
    while lag := get_replication_slots_lag(pg, use_pg_last_commit_lsn=use_pg_last_commit_lsn):
        msg = tabulate(headers, lag)
        sys.stdout.write(msg)
        sys.stdout.flush()
        time.sleep(refresh_period)
        lag = get_replication_slots_lag(pg)
        for _ in range(msg.count('\n')):
            sys.stdout.write('\033[2K\033[1G') # delete line and move to the beginning
            sys.stdout.write('\x1b[1A') # move cursor up
    # output all slots for extra final check
    slots = get_replication_slots_lag(pg, use_pg_last_commit_lsn=use_pg_last_commit_lsn, all_slots=True)
    sys.stdout.write('\n' + tabulate(headers, slots))
    sys.stdout.flush()

class UnsupportedReplicationSlot(Exception):
    '''Exception raised when we encounter a replication slot that we cannot migrate.'''

def extract_replication_slots(pguri, slots_no_wait=False, permission='no', log=None):
    '''Retrieve replication slot data from old cluster to restore in the new one.'''
    log = log or slog.nolog
    if not slots_no_wait:
        with pgcn.connect(pguri, autocommit=True) as pg:
            inactive_slots = pg.execute(
                'SELECT array_agg(slot_name) FROM pg_replication_slots WHERE active_pid IS NULL;').scalar()
            not_walsender_slots = pg.execute('''
                SELECT array_agg(prs.slot_name)
                FROM pg_replication_slots AS prs
                JOIN pg_stat_activity AS psa ON psa.pid = prs.active_pid
                WHERE prs.active_pid IS NOT NULL
                    AND psa.backend_type != 'walsender';
            ''').scalar()
            err_msg = ''
            if inactive_slots:
                err_msg += (
                    '''\nThe following replication slots are not active, please delete them or get them running, '''
                    '''then try again:\n\n    %s''' % ',\n    '.join(inactive_slots))
            if not_walsender_slots:
                err_msg += (
                    '''\n\nThe following replication slots are not being used by a walsender process and cannot be '''
                    '''migrated, please delete them and try again:\n\n    %s''' % ',\n    '.join(not_walsender_slots))
            if inactive_slots or not_walsender_slots:
                slog.error2(err_msg.strip())
                raise UnsupportedReplicationSlot
            slots_lag = get_replication_slots_lag(pg)
        if not slots_lag:
            return
    msg = 'To speed up replication slots catching up, all new transactions will be set to read-only.'
    if (permission == 'no'
            or (permission == 'ask' and not prompt_yesno(msg + ' Do you want to continue?', default=False))):
        sys.exit(0)
    with pgcn.connect(pguri, autocommit=True) as pg:
        if not slots_no_wait:
            alter_system_set(pg, 'default_transaction_read_only', 'on')
            log('default_transaction_read_only set to "on"')
            log('Monitoring lagging logical replication slots...')
            poll_replication_slots_lag(pg)
            log('All logical replication slots are caught up!')
            alter_system_set(pg, 'default_transaction_read_only', 'off')
        slots = pg.execute('''
            SELECT slot_name, plugin
            FROM pg_catalog.pg_replication_slots
            WHERE slot_type = 'logical' AND temporary IS false;
        ''').fetchall()
    return '\n'.join([
        f'SELECT * FROM pg_catalog.pg_create_logical_replication_slot(\'{slot[0]}\', \'{slot[1]}\', false);'
        for slot in slots])

def extract_replication_origins(pguri):
    '''Retrieve replication origin data from old cluster to restore in the new one.'''
    with pgcn.connect(pguri, autocommit=True) as pg:
        origins = pg.execute('SELECT external_id, remote_lsn FROM pg_replication_origin_status;').fetchall()
    return '\n'.join([
        (f'SELECT * FROM pg_catalog.pg_replication_origin_create(\'{origin[0]}\');\n'
         f'SELECT pg_replication_origin_advance(\'{origin[0]}\', \'{origin[1]}\');')
        for origin in origins])

def get_new_wal_filename(pguri):
    with pgcn.connect(pguri, autocommit=True) as pg:
        if pg.execute('SELECT pg_is_in_recovery();').scalar():
            return  # WAL related functions cannot run in recovery mode
        lsn = pg.execute('SELECT pg_walfile_name(pg_current_wal_insert_lsn());').scalar()
        # Add a buffer to the lsn to account for unexpected increments during the
        # upgrade process
        return f'{int(lsn, 16) + 0xF000000000:024x}'.upper()

def upgrade_to_new_version(
        main_obj, upgrade_obj, tmpdir, hardlink=False, slots_no_wait=False,
        check_only=True, permission='no', verbose=False):
    '''
    Execute pg_upgrade with approppriate flags to migrate to the new Postgres
    version. Pass check_only=True to only perform cluster compability checks.

    Returns true if successful, false otherwise.
    '''
    old_basedir = main_obj['env']['basedir']
    old_port = main_obj['env']['port']
    old_pguri = main_obj['env']['pguri']
    new_basedir = upgrade_obj['env']['basedir']
    new_port = upgrade_obj['env']['port']
    new_pguri = upgrade_obj['env']['pguri']
    user = main_obj['env']['user']
    runcmd = functools.partial(
        subprocess.run, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, text=True, check=True, cwd=tmpdir)
    base_cmd = [
        os.path.join(new_basedir, 'bin/pg_upgrade'),
        '--old-bindir', os.path.join(old_basedir, 'bin'),
        '--old-datadir', os.path.join(old_basedir, 'data'),
        '--new-bindir', os.path.join(new_basedir, 'bin'),
        '--new-datadir', os.path.join(new_basedir, 'data'),
        '--username', user,
        '--jobs', str(get_max_effect_procs())]
    base_cmd += ['--link'] if hardlink else []
    base_cmd += ['--verbose'] if verbose else []
    log = slog.info2 if verbose else slog.info
    if not stop_server_and_wait(new_basedir, verbose=verbose):
        log('New server still running. Skipping Postgres version upgrade.')
        return False
    if check_only:
        # Replace port for a random one different from the current port and
        # 50432 (used by pg_upgrade) to run pg_upgrade --check
        new_port = str(get_free_port())
        while new_port in (old_port, '50432'):
            new_port = str(get_free_port())
        completed_process = runcmd([*base_cmd, '--old-port', old_port, '--new-port', new_port, '--check'])
        log(completed_process.stdout)
        if completed_process.returncode != 0:
            log('Error while doing checks for Postgres version upgrade')
            return False
        return True
    assert old_port == new_port, 'Ports must be the same when running the actual upgrade'
    try:
        slots_script = extract_replication_slots(old_pguri, slots_no_wait=slots_no_wait, permission=permission, log=log)
    except UnsupportedReplicationSlot:
        return False
    origins_script = extract_replication_origins(old_pguri)
    new_wal_filename = get_new_wal_filename(old_pguri)
    if any([slots_script, origins_script]):
        # Dumping data into a file in the old and new basedirs in case an
        # unexpected error is encountered. Also dumping into tmpdir so it can
        # be later executed along with other post-upgrade scripts below.
        for basedir in (old_basedir, new_basedir, tmpdir):
            if slots_script:
                path = os.path.join(basedir, 'recreate_slots.sql')
                fs.write(path, slots_script, mode='w+')
                log('Slot recreation script saved at: %s', path)
            if origins_script:
                path = os.path.join(basedir, 'recreate_origins.sql')
                fs.write(path, origins_script, mode='w+')
                log('Origins recreation script saved at: %s', path)
            path = os.path.join(basedir, 'new_wal_filename')
            fs.write(path, new_wal_filename, mode='w+')
            log('New WAL file name saved at: %s', path)
    if not stop_server_and_wait(old_basedir, verbose=verbose):
        log('Old server still running. Skipping Postgres version upgrade.')
        return False
    completed_process = runcmd([*base_cmd, '--old-port', old_port, '--new-port', new_port])
    if completed_process.returncode != 0:
        log('Error while running pg_upgrade. Restoring old database.')
        log(completed_process.stdout)
        # Restore old database
        if hardlink:
            pg_control_file = os.path.join(old_basedir, 'data/global/pg_control')
            try:
                os.rename(pg_control_file + '.old', pg_control_file)
            except FileNotFoundError:
                pass
        ensure_pg_running(old_basedir, old_port)
        # Revert read-only from replication slots catching up
        with pgcn.connect(old_pguri, autocommit=True) as pg:
            alter_system_set(pg, 'default_transaction_read_only', 'off')
        return False
    # Random port different from new_port or old_port to make sure that
    # replication slots are created BEFORE receiving any connections
    while (tmp_port := str(get_free_port())) == new_port:
        pass
    tmp_pguri = new_pguri.replace(new_port, tmp_port)
    log('Temporary port: %s. Temporary PostgreSQL connection URI: %s', tmp_port, tmp_pguri)
    new_config_path = os.path.join(new_basedir, 'data/postgresql.conf')
    with open(new_config_path, 'r+', encoding='utf-8') as fpointer:
        original_config = fpointer.read()
        tmp_config = f'\nport = {tmp_port}\n'
        tmp_config += '\nwal_level = logical\n'  # Needed to execute pg_create_logical_replication_slot()
        num_slots = slots_script.count('\n') + 10 if slots_script else 10
        tmp_config += f'\nmax_replication_slots = {num_slots}\n'  # Needed to create all slots
        fpointer.write(tmp_config)
    if new_wal_filename:
        # We advance the LSN to beyond the LSN of the pre-upgrade database. This allows LSNs to monotonically
        # increase across postgresql version upgrades. This fixes issues with client applications that use LSNs
        # for synchronization, in particular with users of replication slots.
        subprocess.run(
            [os.path.join(new_basedir, 'bin/pg_resetwal'),
             '--pgdata', os.path.join(new_basedir, 'data'),
            '--next-wal-file', new_wal_filename],
            check=True)
        log('WAL file name reset to %s' % new_wal_filename)
    ensure_pg_running(new_basedir, tmp_port)
    with pgcn.connect(tmp_pguri, autocommit=True) as pg:
        alter_system_set(pg, 'default_transaction_read_only', 'off')  # Revert read-only from slots catching up
        max_connections = show_pg_param(pg, 'max_connections')
        # Execute post-upgrade scripts
        log('Executing post-upgrade scripts...')
        for filepath in glob.glob(os.path.join(tmpdir, '*.sql')):
            with open(filepath, encoding='utf8') as fp:
                script = fp.read()
            script = re.sub(r'\\.*', '', script).strip()  # remove psql commands
            if script:
                log('Executing script %s\n%s', filepath, script)
                pg.execute(script)
    log('Vacuum the cluster (may take a long time depending on the size of the cluster)...')
    log(subprocess.check_output(
        [os.path.join(new_basedir, 'bin/vacuumdb'),
         '-U', user,
         '--port', tmp_port,
         '--all',
         '--analyze-in-stages',
         '--skip-locked',
         # vacuumdb can fail if jobs > max_connections (postgresql.conf)
         '--jobs', str(min(get_max_effect_procs(), int(max_connections)))],
        stderr=subprocess.STDOUT,
        text=True))
    log('Vacuum finished')
    # WARNING: remember that the directory name can only be changed when
    # the database is stopped
    log('Stopping the new cluster...')
    stop_server_and_wait(new_basedir)
    log('New cluster stopped')
    # Reverting temp port. We must add the original port back as the config
    # file has default contents and upcoming connection attempts assume the
    # cluster is already started with its intended port number, otherwise they
    # will hang forever.
    #
    # We must also set wal_level to logical or postgres will complain because
    # of the already created slot.
    fs.write(new_config_path, original_config + f'\nport = {new_port}\nwal_level = logical\n')
    new_ssl_directory = os.path.join(new_basedir, 'data/ssl')
    old_ssl_directory = os.path.join(old_basedir, 'data/ssl')
    if os.path.exists(new_ssl_directory):
        subprocess.check_call(['chmod', '-R', 'u+w', old_ssl_directory])
    if os.path.exists(old_ssl_directory):
        log('Copying over SSL config...')
        subprocess.check_call(['cp', '-r', old_ssl_directory, new_ssl_directory])
        log('SSL config copied')
    if hardlink:
        # At this point there's already no coming back, so we can delete the old files
        log('Deleting old installation...')
        subprocess.check_call(['chmod', '-R', '0700', old_basedir])
        shutil.rmtree(old_basedir)
        log('Old installation deleted')
    else:
        old_dir = f'{old_basedir}-{guids.get_old_server_guid()}'
        os.rename(old_basedir, old_dir)
        log('Old database files now reside in %s. Delete these when the backup is no longer needed.', old_dir)
    log('Renaming new cluster\'s directory: \'%s\' -> \'%s\'', new_basedir, old_basedir)
    os.rename(new_basedir, old_basedir)
    return True

def warn_blocking_locks(obj, procs_active):
    '''Warn the user about locks that are blocking the completion of an upgrade.'''
    def _print_locks(locks):
        for lock in locks:
            (waiting_pid, active_pid, waiting_app_name, active_app_name,
            waiting_query, active_query) = lock
            width = shutil.get_terminal_size().columns or 152
            one_line_waiting_sql = re.sub(r'\s+', ' ', f'Blocked: {waiting_query}')[:width]
            one_line_active_sql = re.sub(r'\s+', ' ', f'Active: {active_query}')[:width]
            print(f'''\
{one_line_waiting_sql}
    {waiting_pid}: {waiting_app_name}
{one_line_active_sql}
    {active_pid}: {active_app_name}''')
    objects = list(procs_active.values())
    application_names = [f'<{obj["env"].get("name")}-{obj["env"].get("guid")}>' for obj in objects]
    basedir = obj['env']['basedir']
    port = obj['env']['port']
    if pg_is_ready(basedir, port):
        pguri = str(pguri_from_basedir(basedir))
        try:
            with pgcn.connect3(pguri) as pg:
                locks = pg.execute('''
                    SELECT
                        p.pid AS waiting_pid,
                        active.pid AS active_pid,
                        p.application_name AS waiting_app_name,
                        active.application_name AS active_app_name,
                        p.query AS waiting_query,
                        active.query AS active_query
                    FROM pg_stat_activity p
                    JOIN pg_stat_activity active ON active.pid = any(pg_blocking_pids(p.pid))
                    WHERE p.application_name = ANY (%(application_names)s)
                        AND p.wait_event_type = 'Lock';
                ''', {'application_names': application_names}).fetchall()
            _print_locks(locks)
        # connection errors can happen because the DB is still booting
        except (psycopg2.errors.ConnectionException, psycopg2.errors.SqlclientUnableToEstablishSqlconnection, # pylint: disable=no-member
                psycopg2.errors.ConnectionDoesNotExist, psycopg2.errors.SqlserverRejectedEstablishmentOfSqlconnection, # pylint: disable=no-member
                    psycopg2.errors.ConnectionFailure): # pylint: disable=no-member
            pass

def build_main_drv_deps(main_drv, main_obj, all_deps, environ=None, verbose=False, interval=30):
    ''' Build all dependencies for the main derivation.'''
    pguri = main_obj['env']['pguri']
    basedir = main_obj['env']['basedir']
    port = main_obj['env']['port']
    applied_revs, is_replica = get_applied_revision_state(basedir, port, pguri)
    effect_deps = {}
    dependees = defaultdict(lambda: {})
    for effect_drv, effect_obj in all_deps.items():
        if 'SCM_EFFECT_EXEC' not in effect_obj['env']:
            continue
        effect_deps[effect_drv] = effect_obj
    for effect_drv, effect_obj in effect_deps.items():
        effect_obj['_dependencies'] = {drv: effect_deps[drv] for drv in effect_obj['inputDrvs'] if drv in effect_deps}
        for dependency_drv in effect_obj['_dependencies']:
            dependees[dependency_drv][effect_drv] = effect_obj
    max_procs = get_max_effect_procs()
    procs_active = OrderedDict()
    procs_scheduled = OrderedDict()
    procs_done = OrderedDict()
    def spawn_proc(effect_obj):
        effect_drv = effect_obj['drv_path']
        effect_obj['_build_guid'] = build_guid = guids.get_build_guid()
        proc = multiprocessing.Process(
            target=apply_effect, name=effect_drv,
            args=(effect_obj, build_guid), kwargs=dict(environ=environ, verbose=verbose))  # pylint: disable=use-dict-literal
        proc.start()
        effect_obj['_proc'] = proc
        procs_active[effect_drv] = effect_obj
    def pop_scheduled():
        ''' Move tasks from scheduled to active. '''
        if procs_scheduled and len(procs_active) < max_procs:
            for drv, obj in list(procs_scheduled.items())[:max_procs - len(procs_active)]:
                del procs_scheduled[drv]
                if obj['env'].get('guid') in (applied_revs or []):  # optimization: already executed, don't bother
                    # .. but not if basefiles exist, fixes missing basedir files on replicas
                    if is_replica and obj['env'].get('basefiles'):
                        procs_done[drv] = obj
                        continue
                spawn_proc(obj)
    def schedule_proc(effect_obj):
        drv = effect_obj['drv_path']
        if drv not in procs_done and drv not in procs_active and drv not in procs_scheduled:
            procs_scheduled[drv] = effect_obj
    for effect_obj in (v for v in effect_deps.values() if not v['_dependencies']):
        schedule_proc(effect_obj)
    last_run = 0
    while procs_active or procs_scheduled:
        pop_scheduled()
        # check if this process is waiting on a lock before trying to join
        now = time.time()
        if now - last_run >= interval:
            warn_blocking_locks(main_obj, procs_active)
            last_run = now
        for effect_drv, effect_obj in list(procs_active.items()):
            proc = effect_obj['_proc']
            proc.join(0.01)   # alternatively: multiprocessing.connection.wait([proc.sentinel])
            if not proc.is_alive():
                if proc.exitcode != 0:
                    for term_obj in list(procs_active.values()):  # sigkill siblings to prevent noisy error output
                        term_obj['_proc'].terminate() if term_obj['_proc'].exitcode is None else None
                    debug_effect_error(main_drv, main_obj, effect_obj, proc)
                    raise EvalException(proc, proc.exitcode, main_drv, main_obj)
                del procs_active[effect_drv]
                proc.close()
                procs_done[effect_drv] = effect_obj
                for dependee_obj in dependees[effect_drv].values():  # schedule the unlocked dependees
                    dependee_deps = dependee_obj['_dependencies']
                    if effect_drv in dependee_deps:
                        del dependee_deps[effect_drv]
                        if not dependee_deps:
                            schedule_proc(dependee_obj)
                pop_scheduled()
    return main_obj

def get_nix_module_dir(module) -> str | None:
    path = get_nix_module(module)
    if path:
        return path if os.path.isdir(path) else os.path.dirname(path)

@contextlib.contextmanager
def temp_nix_module(nix_module):
    ''' Create a new temporary nix module for the new Postgres server to migrate to.'''
    module_dir = get_nix_module_dir(nix_module)
    default_nix_path = os.path.join(module_dir, 'default.nix') # type: ignore reportAttributeAccessIssue
    temp_nix_module_path = tempfile.mkdtemp()
    temp_default_nix_path = os.path.join(temp_nix_module_path, 'default.nix')
    shutil.rmtree(temp_nix_module_path, ignore_errors=True)  # Remove stale directory if any
    mkdir(temp_nix_module_path)
    shutil.copyfile(default_nix_path, temp_default_nix_path)
    try:
        yield temp_nix_module_path
    finally:
        shutil.rmtree(temp_nix_module_path, ignore_errors=True)

def enough_space_for_migration(basedir, verbose=False):
    '''
    Determine if there is enough space for copying the data in the
    <basedir>/data directory and any tablespaces in <basedir>/tablespaces.
    '''
    tblspaces = list(glob.glob(os.path.join(basedir, 'tablespaces/*')))
    tblspaces_to_check = [os.path.join(basedir, 'data'), *tblspaces]
    tblspaces_by_fs = defaultdict(list)
    usage_by_fs = defaultdict(int)
    avail_by_fs = {}
    cmd = ['df', '-k', '--output=file,source,avail', *tblspaces_to_check]
    for line in subprocess.check_output(cmd, text=True).splitlines()[1:]:
        tblspace, filesystem, free_space = line.split()
        usage = subprocess.check_output(
            ['du', '-k', '--summarize', tblspace], text=True).strip().split()[0]
        usage = int(usage)
        tblspaces_by_fs[filesystem].append((tblspace, usage))
        usage_by_fs[filesystem] += usage
        avail_by_fs[filesystem] = int(free_space)
    enough_space = True
    log = slog.info2 if verbose else slog.info
    for fsys in tblspaces_by_fs:
        if usage_by_fs[fsys] > avail_by_fs[fsys] * 0.99:  # 1% safe zone to avoid filling the filesystem completely
            enough_space = False
            log(('Not enough space in filesystem %s to perform the upgrade.'
                ' Tablespaces use %dK in total while available space is %dK'),
                fsys, usage_by_fs[fsys], avail_by_fs[fsys])
            for tblspace, usage in tblspaces_by_fs[fsys]:
                log('Usage of tablespace %s is %dK', tblspace, usage)
    return enough_space

def recover_pg_upgrade_logs(basedir):
    ''' Log pg_upgrade messages to schematic log file.'''
    base_out = 'data/pg_upgrade_output.d/*'
    paths = glob.glob(os.path.join(basedir, base_out, 'log/pg_upgrade*.log'))
    paths += glob.glob(os.path.join(basedir, base_out, 'loadable_libraries.txt'))
    paths += [os.path.join(basedir, 'data/pg_ctl.log')]
    for path in sorted(paths):
        log_file = os.path.abspath(path)
        try:
            with open(log_file, encoding='utf-8') as fp:
                content = fp.read().strip()
                if content:
                    slog.info('Dumping contents of %s:\n%s', log_file, content)
        except OSError:
            pass

def upgrade_postgres_major_version(
        nix_module, main_obj, main_drv, hardlink=False, slots_no_wait=False,
        environ=None, permission='no', verbose=False):
    '''
    Upgrade a Postgres server by instantiating and building a fresh set of
    binaries in the target version.
    '''
    if not hardlink and not enough_space_for_migration(main_obj['env']['basedir'], verbose=verbose):
        log = slog.info2 if verbose else slog.info
        log('As an alternative, back up the data and use the option \'--pg-upgrade-hardlink=1\''
            ' to hardlink the data instead of copying it')
        return False
    with temp_nix_module(nix_module) as temp_nix_module_path:
        upgrade_environ = dicttoolz.merge(environ or {}, {'SCM_PG_UPGRADE': '1'})
        upgrade_drv = instantiate_module(temp_nix_module_path, environ=upgrade_environ, verbose=verbose)
        upgrade_deps = parse_drv(upgrade_drv, recursive=True)
        upgrade_obj = upgrade_deps[upgrade_drv]
        realize_derivation(upgrade_drv, environ=upgrade_environ, verbose=verbose)
        build_main_drv_deps(main_drv, upgrade_obj, upgrade_deps, environ=upgrade_environ, verbose=verbose)
        upgrade = functools.partial(
            upgrade_to_new_version, main_obj, upgrade_obj, temp_nix_module_path, hardlink=hardlink,
            slots_no_wait=slots_no_wait, permission=permission, verbose=verbose)
        new_basedir = upgrade_obj['env']['basedir']
        passed_checks = upgrade(check_only=True)
        upgrade_success = passed_checks and upgrade(check_only=False)
        # Remove partial installations if hardlink was used or if checks
        # failed.
        #
        # If the data was copied instead of hardlinked, there exists the risk
        # of deleting it, so we don't remove the installation directory.
        #
        # If the upgrade was successful, the new version directory becomes the
        # new home of the database, so of course we don't remove it.
        if not passed_checks or (passed_checks and hardlink and not upgrade_success):
            recover_pg_upgrade_logs(new_basedir)
            pg_ctl_fn(new_basedir, 'stop', shutdown_mode='immediate')
            subprocess.check_call(['chmod', '-R', '0700', new_basedir])
            shutil.rmtree(new_basedir)
    return upgrade_success

def is_replica(all_deps):
    ''' Determine if a replica is being built.'''
    for drv in all_deps.values():
        if 'build_replica.py' in drv.get('env', {}).get('SCM_EFFECT_PROG', ''):
            return True

def eval_module(path, pg_upgrade='no', hardlink=False, slots_no_wait=False, environ=None, verbose=False):
    ''' Build a schematic object.
        First, we execute a pure nix build. This builds system dependencies, and the effect scripts.
        Next, we build a dependency DAG of all IO effects to apply, and then apply them.
    '''
    nix_module = validate_nix_module(path)
    main_drv = instantiate_module(nix_module, environ=environ, verbose=verbose)
    all_deps = parse_drv(main_drv, recursive=True)
    main_obj = all_deps[main_drv]
    match = re.search(r'(postgresql-\d+(\.\d+)?(\.\d+)?(alpha\d*)?(beta\d*)?(rc\d*)?)$', main_obj['env']['postgresql'])
    if not match:
        raise ValueError(f'Unrecognized PostgreSQL directory path: {main_obj["env"]["postgresql"]}')
    postgresql_drv = next(k for k in main_obj['inputDrvs'] if match.group(1) in k)
    if not os.path.isdir(main_obj['env']['postgresql']):
        # Build Postgres if it doesn't exist to guarantee the existence of
        # binaries needed for properly checking version strings
        realize_derivation(postgresql_drv, environ=environ, verbose=verbose)
    needs_major_upgrade = server_needs_major_upgrade(
        main_obj['env']['basedir'], main_obj['env']['postgresql'], permission=pg_upgrade)
    building_replica = is_replica(all_deps)
    upgrade_success = False
    if needs_major_upgrade and not building_replica:
        upgrade_success = upgrade_postgres_major_version(
            nix_module, main_obj, main_drv, hardlink=hardlink, slots_no_wait=slots_no_wait,
            environ=environ, permission=pg_upgrade, verbose=verbose)
    elif needs_major_upgrade and building_replica:
        basedir = main_obj['env']['basedir']
        module = nix_module.removesuffix('/default.nix').rstrip('/')
        default_nix_path = module + '/default.nix'
        print('\n', f'''
Automatically upgrading replicas to a newer major Postgres version is not
supported. If you still need to upgrade to a newer major version, you can
perform the following steps:

    1. Upgrade the primary cluster.

        scm upgrade --pg-upgrade [yes|no|ask] [--pg-upgrade-hardlink [0|1]] [--pg-upgrade-slots-no-wait [0|1]] <path_to_primary_cluster_module>

    2. Remove the replica.

        scm gc-basedir --force [--stop] {basedir}

    3. Change the Postgres version of the replica within the default.nix
       definition file to match that of the primary cluster.

        {default_nix_path}

    4. Recreate the replica with the new version.

        scm upgrade {module}
              '''.strip(), '\n', sep='', file=sys.stderr)
        sys.exit(1)
    realize_derivation(main_drv, environ=environ, verbose=verbose)
    build_main_drv_deps(main_drv, main_obj, all_deps, environ=environ, verbose=verbose)
    if upgrade_success:
        pg_ctl_fn(main_obj['env']['basedir'], 'start', quiet=(not verbose))
        with pgcn.connect(main_obj['env']['pguri'], autocommit=True) as pg:
            cursor = pg.execute('SELECT count(*) FROM pg_settings WHERE pending_restart;')
            pending_restart = cursor.fetchone()[0] > 0
        if pending_restart:
            log = slog.info2 if verbose else slog.info
            log('Pending restart reported by the server. Restarting it now...')
            pg_ctl_fn(main_obj['env']['basedir'], 'restart', quiet=(not verbose))
    return main_obj

def psql_from_basedir(basedir):
    metajson = os.path.join(basedir, 'meta.json')
    if not os.path.exists(metajson):
        sys.stderr.write(f'Could not find meta.json in {basedir}')
        sys.exit(1)
    metadata = json.loads(fs.try_read(metajson)) # type: ignore reportAttributeAccessIssue
    if not sys.stderr.isatty() or not sys.stdout.isatty():
        sys.stderr.write(f'refusing to open psql for {basedir} because shell is not a tty\n')
        sys.exit(5)
    ensure_pg_running(basedir, metadata['port'])
    signal.signal(signal.SIGINT, signal.SIG_IGN)
    psql_cmd = [
        os.path.join(basedir, 'bin/psql'), '-p', str(metadata['port']), '-U', metadata['user'], '-h', 'localhost',
        metadata['dbname']]
    with subprocess.Popen(psql_cmd) as proc:
        proc.communicate()

def pretty_basedir(basedir):
    if os.environ.get('HOME'):
        rel_path = os.path.relpath(basedir, os.environ['HOME'])
        return f'~/{rel_path}'
    return basedir

def maybe_open_psql(basedir, no_psql):
    ''' Open psql on basedir, or print the command for user to do so. '''
    if no_psql:
        print('open psql shell: scm psql %s' % pretty_basedir(basedir), file=sys.stderr)
    elif sys.stderr.isatty():
        psql_from_basedir(basedir)

def ls_basedirs(store=os.environ['SCM_PG'], sort=lambda x: x[1].st_ctime, reverse=False):
    bds = glob.glob(os.path.join(store, '*'))
    return sorted(((bd, os.stat(bd)) for bd in bds), key=sort, reverse=reverse)

def ls_bd_dirs():
    return [bdir for bdir, _ in ls_basedirs() if os.path.isdir(bdir)]

def resolve_basedirs(basedirs, default=ls_bd_dirs):
    if basedirs:
        return list(filter(None, [resolve_basedir(path) for path in basedirs]))
    return default()

def resolve_basedir(path):
    '''
    Resolves a path of the form srv/*/default.nix to its corresponding database path in $SCM_PG.
    If the path doesn't point to a server nix definition, returns `path` unchanged.
    '''
    if not path:
        return
    path = os.path.expanduser(path)
    if os.path.isdir(path) and os.path.exists(os.path.join(path, 'meta.json')):
        return path
    nixmod = get_nix_module(path)
    if nixmod and os.path.exists(nixmod):
        path = nixmod
    if os.path.isdir(path) and os.path.exists(os.path.join(path, 'default.nix')):
        path = os.path.join(path, 'default.nix')
    if os.path.isfile(path) and not path.endswith('nix'):
        print(f'expected directory, got {path!r}', file=sys.stderr)
        return
    if not os.path.isfile(path):
        print(f'does not exist {path!r}', file=sys.stderr)
        return
    content = fs.try_read(path, default='')
    def find_attr(attr):
        m = re.search(rf'scm\.(server|replica).*?{attr}\s*=\s*"(.*?)"',\
            content, re.DOTALL) # type: ignore reportAttributeAccessIssue
        if m:
            return m.group(2)
    if (name := find_attr('name')) and (guid := find_attr('guid')):
        basedir = os.path.join(os.path.join(os.environ['SCM_PG'], f'{name}-{guid}'))
        if os.path.isdir(basedir):
            return basedir

def _derivative_nix(nix_module, verbose=False):
    ''' Create a sandbox database intended to be used as the reference database for diffing to discover changes to
        apply to the target database.
    '''
    environ = {
        'SCM_PORT': env.get_str('SCM_PORT') or str(get_free_port()),
        'SCM_GUID': guids.get_server_guid(),
        'SCM_INSTALL_TABLESPACES': '0',
        'SCM_VERBOSE': str(verbose),
        'SCM_ALLOW_RESTART': 'yes',
        'SCM_DERIVATIVE_TARGET': nix_module,
    }
    return eval_module(get_relative_nix_mod('lib/derivative.nix'), environ=environ, verbose=verbose)

def _sandbox(paths: list[str], verbose: bool =False, scm_install_tablespaces: int =0, version: str | None =None):
    ''' Create a temporary sandbox database given a schema, database, or sql file. '''
    environ = {
        'SCM_PORT': env.get_str('SCM_PORT') or str(get_free_port()),
        'SCM_GUID': env.get_str('SCM_GUID') or guids.get_server_guid(),
        'SCM_INSTALL_TABLESPACES': str(scm_install_tablespaces),
        'SCM_VERBOSE': str(verbose),
        'SCM_POSTGRESQL_VERSION': (version or '').replace('.', '_'),
        'SCM_ALLOW_RESTART': 'yes',
    }
    nix_modules = []
    for path in paths:
        nix_module = validate_nix_module(path)
        nix_modules.append(nix_module)
    if not (environ['SCM_GUID'].startswith('S1') and len(environ['SCM_GUID']) == 16):
        print('invalid SCM_GUID: %s it should start with S1 and have length 16' % environ['SCM_GUID'], file=sys.stderr)
        sys.exit(29)
    environ['SCM_SANDBOX_MODULES'] = ':'.join(os.path.abspath(x) for x in nix_modules)
    return eval_module(get_relative_nix_mod('lib/sandbox.nix'), environ=environ, verbose=verbose)

def _inspect(path, include_all=False):
    nix_module = validate_nix_module(path)
    environ = {
        'SCM_SANDBOX_MODULES': os.path.abspath(nix_module),
    }
    main_drv = instantiate_module(get_relative_nix_mod('lib/sandbox.nix'), environ=environ)
    all_drvs = parse_drv(main_drv, recursive=True)
    if not include_all:
        all_drvs = {drv: obj for drv, obj in all_drvs.items() if obj['env'].get('scm_type')}
    return {'meta': {'drv_path': main_drv, 'drv_obj': all_drvs[main_drv]}, 'dependencies': all_drvs}

def drvobj_to_nixref(obj):
    ''' Return a "<name-guid>" string from a derivation object. '''
    env = obj['env']
    if not env.get('guid'):
        return  # sandbox database objects don't have a guid
    return f'<{env["name"]}-{env["guid"]}>'

def parse_anglepath(path):
    '''
    >>> parse_anglepath('<pg_repack>')
    'pg_repack'
    '''
    match = re.match(r'\<(?P<name>.+)\>', path) if path else None
    if match:
        return match.group('name')

def get_nix_module(path, incl_default_nix=False, nix_paths=os.environ.get('NIX_PATH')) -> str | None:
    '''
    >>> root_dir = env.get_str('ROOT_DIR')
    >>> scm_path = env.get_str('SCM_PATH')
    >>> pkg = os.path.join(root_dir, 'pkg')
    >>> repack = 'pg_repack-E0U844YU7D8AI2MI'
    >>> assert get_nix_module('pkg/%s' % repack) == os.path.join(pkg, repack), \
        (get_nix_module('pkg/%s' % repack), os.path.join(pkg, repack))
    >>> assert get_nix_module('pkg/%s' % repack, incl_default_nix=True) == os.path.join(pkg, '%s/default.nix' % repack)
    >>> assert get_nix_module('<pg_repack-E0U844YU7D8AI2MI>') == os.path.join(os.path.join(scm_path, 'pkg'), repack), \
        (get_nix_module('<pg_repack-E0U844YU7D8AI2MI>'), os.path.join(os.path.join(scm_path, 'pkg'), repack))
    >>> assert get_nix_module('<foo/pg_repack-E0U844YU7D8AI2MI>', nix_paths='foo=%s' % pkg) == os.path.join(pkg, repack)
    '''
    anglepath = parse_anglepath(path)
    if anglepath:
        for nix_path in nix_paths.split(':'): # type: ignore reportAttributeAccessIssue
            if '=' in nix_path:
                prefix_str, _, sub = nix_path.partition('=')
                prefix_list = prefix_str.split(os.path.sep)
                path_list = anglepath.split(os.path.sep)
                if prefix_list == path_list[:len(prefix_list)]:
                    path = os.path.join(sub, *path_list[len(prefix_list):])
                    break
            else:
                _path = os.path.join(nix_path, anglepath)
                if os.path.exists(_path):
                    path = _path
                    break
    path = os.path.expanduser(os.path.normpath(path))
    if not os.path.isabs(path):
        path = os.path.abspath(path)
    default_nix_path = os.path.join(path, 'default.nix')
    if path.endswith('.nix') and os.path.exists(path):
        return path
    if os.path.isdir(path) and os.path.exists(default_nix_path):
        if incl_default_nix:
            return default_nix_path
        return path.rstrip('/')

def validate_nix_module(path):
    if nix_module := get_nix_module(path):
        return nix_module
    print('invalid nix module: %s' % path, file=sys.stderr)
    sys.exit(2)

def get_free_port():
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.bind(('', 0))
    addr = s.getsockname()
    s.close()
    return addr[1]

def unexpand_user(path, home=os.environ['HOME']):
    '''
    >>> unexpand_user('/home/scott/var/pg', home='/home/scott')
    '~/var/pg'
    '''
    return re.sub('^%s/' % home.rstrip('/'), '~/', path) if path else path

def validate_name(name):
    if not re.match(r'^[a-z][a-z0-9._-]*$', name):
        print('error: please use a name that includes only letters, numbers, dash, dot or underscore', file=sys.stderr)
        sys.exit(1)

def get_postgres_process(pid, pidfile_to_remove=None):
    try:
        proc = psutil.Process(pid) if pid and pid > 0 else None
    except psutil.NoSuchProcess:
        proc = None
    if not proc or proc.name() != 'postgres':
        # PID is not associated to a postgres process anymore
        if pidfile_to_remove:
            fs.try_unlink(pidfile_to_remove)
        proc = None
    return proc

def get_basedir(path):
    basedir = os.path.normpath(os.path.expanduser(path))
    if basedir.endswith('/data'):
        basedir = '/'.join(basedir.split('/')[:-1])
    return basedir

def has_pidfile(basedir):
    return os.path.exists(os.path.join(basedir, 'data/postmaster.pid'))

def _gc_basedir(basedir, stop=False, force=False, verbose=False, raise_exc=False):
    if not basedir:
        return
    metajson = os.path.join(basedir, 'meta.json')
    metadata = None
    try:
        metadata = json.loads(fs.try_read(metajson, default='')) # type: ignore reportAttributeAccessIssue
    except (json.JSONDecodeError, KeyError):
        print('invalid %s' % unexpand_user(metajson), file=sys.stderr) if verbose else None
        if raise_exc:
            raise
        return
    except FileNotFoundError:
        print('missing %s' % unexpand_user(metajson), file=sys.stderr) if verbose else None
        if raise_exc:
            raise
        return
    if not metadata['istemp'] and not force:
        print('not a temp datababase: %s (hint: use `scm gc-basedir --force` to insist on deleting)' % \
              unexpand_user(basedir), file=sys.stderr) if verbose else None
        return
    is_running = has_pidfile(basedir)
    if not metadata['istemp'] and (stop or not is_running):
        consent = prompt_yesno('remove database %s?' % unexpand_user(basedir))
        if not consent:
            return
    if is_running:
        if not stop:
            print(
                'won\'t gc a running server: %s  (hint: use `scm gc-basedir --stop` to insist on stopping)' %
                unexpand_user(basedir), file=sys.stdout) if verbose else None
            if raise_exc:
                raise RuntimeError
            return
        pg_ctl_fn(basedir, 'stop', shutdown_mode='immediate', quiet=(not verbose))
    if verbose:
        print('removing %s' % unexpand_user(basedir), file=sys.stderr)
    subprocess.check_call(['chmod', '-R', '0700', basedir])
    shutil.rmtree(basedir)

def create_schema(name):
    validate_name(name)
    guid = guids.get_schema_guid()
    dirname = f'{name}-{guid}'
    schema_path = os.path.abspath(
        os.path.join(env.get_str('ROOT_DIR'), 'pkg', dirname) # type: ignore reportAttributeAccessIssue
    )
    mkdir(schema_path)
    nixout = os.path.join(schema_path, 'default.nix')
    if os.path.exists(nixout):
        print('error, file exists: %s' % nixout, file=sys.stderr)
        sys.exit(1)
    with open(nixout, 'w') as out:
        out.write('''
stdargs @ { scm, ... }:

scm.schema {
    guid = "%s";
    name = "%s";
    upgrade_sql = ./upgrade.sql;
    dependencies = [

    ];
}\n'''.lstrip() % (guid, name))
    sqlout = os.path.join(schema_path, 'upgrade.sql')
    if os.path.exists(sqlout):
        print('error, file exists: %s' % sqlout, file=sys.stderr)
        sys.exit(1)
    with open(sqlout, 'w') as out:
        out.write('\n')
    print('created "<%s>"' % dirname)
    print('    %s' % os.path.relpath(nixout))
    print('    %s' % os.path.relpath(sqlout))
    return '<%s>' % dirname

def escape_double_quote(text):
    r'''
    >>> escape_double_quote('hello')
    'hello'
    >>> escape_double_quote('hello "world"')
    'hello \\"world\\"'
    '''
    return text.replace('"', r'\"')

def path_relative(path, relativeto):
    '''Turn an absolute path reference into a path to a reference path.'''
    relpath = os.path.relpath(path, relativeto)
    if not relpath.startswith('/') and not relpath.startswith('.'):
        relpath = './%s' % relpath
    return relpath

def path_nixpath(path, nix_paths=os.environ.get('NIX_PATH')):
    '''Turn an absolute path reference into a path relative to NIX_PATH.'''
    if not nix_paths:
        return
    abs_path = os.path.normpath(os.path.expanduser(path))
    basename = os.path.basename(path)
    for nix_path in nix_paths.split(':'):
        if '=' in nix_path:
            prefix_str, _, sub = nix_path.partition('=')
            sub_list = sub.split(os.path.sep)
            path_list = abs_path.split(os.path.sep)
            if sub_list == path_list[:len(sub_list)]:
                subpath = os.path.sep.join(path_list[len(sub_list):])
                return f'<{prefix_str}/{subpath}>'
        elif os.path.relpath(abs_path, nix_path) == basename:
            return f'<{basename}>'

def path_norm(path, relativeto=None, nix_paths=os.environ.get('NIX_PATH')):
    '''
    Turn an absolute path reference into a path relative to NIX_PATH or relative to a reference path.

    >>> root_dir = env.get_str('ROOT_DIR')
    >>> rev = os.path.join(root_dir, 'rev/2020-11-16-country-R000GHY2HI7C7675')
    >>> pkg = os.path.join(root_dir, 'pkg/country-S0Y2F1PPW4X10VTW')
    >>> srv = os.path.join(root_dir, 'srv/world-D0J6PLZEYV46LZA8')
    >>> path_norm(srv, None, os.path.join(root_dir, 'srv'))
    '<world-D0J6PLZEYV46LZA8>'
    >>> path_norm(srv, None, 'srv=%s' % os.path.join(root_dir, 'srv'))
    '<srv/world-D0J6PLZEYV46LZA8>'
    >>> path_norm(rev, None, os.path.join(root_dir, 'rev'))
    '<2020-11-16-country-R000GHY2HI7C7675>'
    >>> path_norm(rev, None, 'rev=%s' % os.path.join(root_dir, 'rev'))
    '<rev/2020-11-16-country-R000GHY2HI7C7675>'
    >>> path_norm(pkg, None, os.path.join(root_dir, 'pkg'))
    '<country-S0Y2F1PPW4X10VTW>'
    >>> path_norm(pkg, None, 'pkg=%s' % os.path.join(root_dir, 'pkg'))
    '<pkg/country-S0Y2F1PPW4X10VTW>'
    >>> path_norm(srv, pkg, None)
    '../../srv/world-D0J6PLZEYV46LZA8'
    >>> path_norm(rev, pkg, None)
    '../../rev/2020-11-16-country-R000GHY2HI7C7675'
    >>> path_norm(pkg, srv, None)
    '../../pkg/country-S0Y2F1PPW4X10VTW'
    '''
    return path_nixpath(path, nix_paths) or path_relative(path, relativeto)

def update_schema_src(orig_src: str, dependencies: set[str]):
    r'''
    >>> update_schema_src(
    ...     'dependencies = [\n    <schema_1>\n    <schema_2>\n];',
    ...     set(['<schema_1>', '<schema_2>', '<schema_3>'])
    ... )
    'dependencies = [\n        <schema_3>\n        <schema_2>\n        <schema_1>\n    ];'
    >>> update_schema_src(
    ...     'dependencies = [\n    <schema_1>\n    <schema_2>\n    # <schema_ignore>\n];',
    ...     set(['<schema_1>', '<schema_2>', '<schema_3>'])
    ... )
    'dependencies = [\n        <schema_3>\n        <schema_2>\n        <schema_1>\n    ];'
    '''
    def replacement(match):  # noqa: ARG001
        return 'dependencies = [%s\n    ];' % ''.join(['\n        %s' % d for d in sorted(dependencies, reverse=True)])
    updated_src: str = re.sub(
        r'dependencies\s*=\s*\[\s*([\s\S]*?)\s*\];',
        replacement,
        orig_src
    )
    return updated_src

def update_schema_deps(schema_path: str, dependencies: list[str]):
    '''Replace the 'dependencies' attribute in a schema source file.'''
    schema_nix: str = get_nix_module(schema_path, incl_default_nix=True) # type: ignore reportAssignmentType
    orig_src: str = fs.try_read(schema_nix, default='') # type: ignore reportAssignmentType
    norm_deps: set[str] = set(filter(None, dependencies))
    updated_src = update_schema_src(orig_src, norm_deps)
    with open(schema_nix, 'w') as out:
        out.write(updated_src)
    updated_schema_metadata = _inspect(schema_nix)
    updated_schema_obj = next(get_immediate_deps(updated_schema_metadata, scm_types=('schema',)))
    updated_norm_deps: set[str] = set(filter(None, [
        drvobj_to_nixref(d) for d in
        get_immediate_deps(updated_schema_metadata, cursor=updated_schema_obj['drv_path'])]))

    if norm_deps == updated_norm_deps:
        print('updated %s' % os.path.relpath(schema_nix))
    else:
        print('failed updating %s - must update dependencies by hand' % os.path.relpath(schema_nix), file=sys.stderr)
        print('Missing dependencies:')
        for dep in norm_deps.difference(updated_norm_deps):
            print(f'    - "{dep}"')
        with open(schema_nix, 'w') as out:
            out.write(orig_src)

def get_transitive_dependencies(inspect_metadata, cursor=None):
    cursor = cursor or inspect_metadata['meta']['drv_path']
    cursor_obj = inspect_metadata['dependencies'][cursor]
    for sub_drv in cursor_obj['inputDrvs']:
        if sub_drv not in inspect_metadata['dependencies']:
            continue  # pure software dependency
        sub_obj = inspect_metadata['dependencies'][sub_drv]
        yield sub_obj
        yield from get_transitive_dependencies(inspect_metadata, cursor=sub_drv)

def get_transitive_revision_heads(inspect_metadata, cursor=None):
    cursor = cursor or inspect_metadata['meta']['drv_path']
    cursor_obj = inspect_metadata['dependencies'][cursor]
    for sub_drv in cursor_obj['inputDrvs']:
        if sub_drv not in inspect_metadata['dependencies']:
            continue  # pure software dependency
        sub_obj = inspect_metadata['dependencies'][sub_drv]
        if sub_obj['env']['scm_type'] in ('revision', 'tablespace', 'namespace', 'pghba'):
            yield sub_obj
        elif sub_obj['env']['scm_type'] == 'schema':
            yield from get_transitive_revision_heads(inspect_metadata, cursor=sub_drv)

def get_minimal_revision_heads(inspect_metadata):
    ''' Get the mininum set of revisions dependended on (eliminating redundant transitive dependencies). '''
    suppress = set()
    deps = list(get_transitive_revision_heads(inspect_metadata))
    for dep in deps:
        for trans_dep in get_transitive_dependencies(inspect_metadata, cursor=dep['drv_path']):
            nixref = drvobj_to_nixref(trans_dep)
            suppress.add(nixref) if nixref else None
    for dep in deps:
        nixref = drvobj_to_nixref(dep)
        if nixref and nixref not in suppress:
            yield dep

def get_immediate_deps(inspect_metadata, cursor=None, scm_types=None):
    cursor = cursor or inspect_metadata['meta']['drv_path']
    cursor_obj = inspect_metadata['dependencies'][cursor]
    for sub_drv in cursor_obj['inputDrvs']:
        if sub_drv not in inspect_metadata['dependencies']:
            continue  # pure software dependency
        sub_obj = inspect_metadata['dependencies'][sub_drv]
        if not scm_types or sub_obj['env']['scm_type'] in scm_types:
            yield sub_obj

def add_basefiles_attribute(schema_path, schema_drv_obj):
    ''' Add `basefiles = ./basefiles;` attribute to a schema object if it doesn't already exist. '''
    if schema_drv_obj['env']['basefiles']:
        return  # basefiles attribute already defined
    schema_relpath = os.path.relpath(schema_path)
    with open(os.path.join(schema_path, 'default.nix'), 'r+') as fp:
        schema_content = fp.read()
        fp.seek(0)
        for line in schema_content.split('\n'):
            print(line, file=fp)
            if re.search(r'\Wupgrade_sql\s*=', line):
                print('    basefiles = ./basefiles;', file=fp)
                print('added basefiles attribute to %s' % schema_relpath, file=sys.stderr)

def _revision_diff_filesystem(rev_basefiles, obj_basedir, sbj_basedir, verbose=False):
    '''
    If the schema has basefiles defined, diff the declarative against the imperative basedir to discover basefiles
    that have diverged.

    This is useful for discovering basefiles that have been edited in a schema so that they can be included in a
    generated revision.

    Returns list of relative files names that have been changed.
    '''
    ret = []
    for path in walk_dir_rel(rev_basefiles):
        rev_path = os.path.join(rev_basefiles, path)
        if os.path.isdir(rev_path):
            if not os.listdir(rev_path):
                os.rmdir(rev_path)
            continue
        obj_path = os.path.join(obj_basedir, path) if obj_basedir else None
        sbj_path = os.path.join(sbj_basedir, path) if sbj_basedir else None
        if same_file_content(obj_path, sbj_path):
            print('same %s' % path, file=sys.stderr) if verbose else None
            os.unlink(rev_path)
        else:
            ret.append(path)
            print('diff %s' % path, file=sys.stderr) if verbose else None
    if not os.listdir(rev_basefiles):
        os.rmdir(rev_basefiles)
    return ret

def create_revision(path, verbose=False):
    nix_module = validate_nix_module(path)
    schema_path: str = get_nix_module_dir(nix_module) # type: ignore reportAssignmentType
    schema_metadata = _inspect(schema_path)
    schema_drv_obj = next(get_immediate_deps(schema_metadata, scm_types=('schema',)))
    schema_name = schema_drv_obj['env']['name']
    if os.path.isdir(os.path.join(schema_path, 'basefiles')):
        # if ./basefiles directory exists, ensure it's defined as an attribute on the schema otherwise it will be
        # ignored, causing bad UX
        add_basefiles_attribute(schema_path, schema_drv_obj)
    guid = guids.get_revision_guid()
    now_iso = datetime.datetime.now().date().isoformat()
    name = f'{now_iso}-{schema_name}'
    dirname = f'{name}-{guid}'
    root_dir: str = env.get_str('ROOT_DIR') # type: ignore reportAssignmentType
    revision_dir = os.path.join(root_dir, 'rev', dirname)
    mkdir(revision_dir)
    print(f'created "<{dirname}>"')
    revision_heads = list(get_minimal_revision_heads(schema_metadata))
    dependencies_str = '\n        '.join(sorted(
        filter(None, list({drvobj_to_nixref(r) for r in revision_heads})), reverse=True))
    revision_nix = os.path.join(revision_dir, 'default.nix')
    obj_basedir = sbj_basedir = None
    schema_basefiles = os.path.join(schema_path, 'basefiles')
    rev_basefiles = os.path.join(revision_dir, 'basefiles')
    include_basefiles = False
    if os.path.isdir(schema_basefiles):
        include_basefiles = True
        subprocess.check_call(['rsync', '-rlc', '%s/' % schema_basefiles, rev_basefiles])
        if obj_basedir and sbj_basedir:
            include_basefiles = bool(
                _revision_diff_filesystem(rev_basefiles, obj_basedir, sbj_basedir, verbose))
    revision_nix_content = '''
stdargs @ { scm, ... }:

scm.revision {
    guid = "%(guid)s";
    name = "%(name)s";
    basefiles = ./basefiles;
    upgrade_sql = ./upgrade.sql;
    dependencies = [
        %(dependencies)s
    ];
}\n'''.lstrip() % {'guid': guid, 'name': name, 'dependencies': dependencies_str}
    if not include_basefiles:
        revision_nix_content = revision_nix_content.replace('''
    basefiles = ./basefiles;''', '')
    with open(revision_nix, 'w') as out:
        out.write(revision_nix_content)
    print('    %s' % os.path.relpath(revision_nix))
    upgrade_sql = os.path.join(revision_dir, 'upgrade.sql')
    with open(upgrade_sql, 'w') as out:
        out.write('\n')
    print('    %s' % os.path.relpath(upgrade_sql))
    schema_new_deps = list(filter(None, [
        drvobj_to_nixref(obj) for obj in get_immediate_deps(schema_metadata, cursor=schema_drv_obj['drv_path'])
        if obj['env'] and obj['env']['scm_type'] and obj['env']['scm_type'] != 'revision'] + [path_norm(revision_dir)]))
    update_schema_deps(schema_path, schema_new_deps)
    _gc_basedir(obj_basedir, force=True, stop=True, verbose=verbose)
    _gc_basedir(sbj_basedir, force=True, stop=True, verbose=verbose)

def file_content(filename):
    '''Return file mode bits and file content in tuple (for equivalence comparison).'''
    try:
        return os.stat(filename).st_mode, open(filename).read()
    except FileNotFoundError:
        return (None, None)

def same_file_content(file1, file2):
    '''Check if the two filenames contain contain the same content and have the same file mode bits.'''
    data1 = file_content(file1)
    data2 = file_content(file2)
    return data1 == data2

def walk_dir_rel(prefix):
    '''Generate relative sub-paths of @prefix from bottom up.'''
    for dirpath, dirnames, filenames in os.walk(prefix, topdown=False):
        for path in dirnames + filenames:
            yield os.path.relpath(os.path.join(dirpath, path), prefix)

def file_search(pattern, paths, extension, case_sensitive=True):
    paths = list(set(paths))
    cmd = ['grep', '-Erl', '--include', '*.%s' % extension]
    if not case_sensitive:
        cmd = [*cmd, '-i']
    try:
        output = subprocess.check_output([*cmd, pattern, *paths], text=True)
    except subprocess.CalledProcessError as ex:
        if ex.returncode != 1:
            raise
        return []
    for line in output.split('\n'):
        if os.path.isfile(line):
            yield line
