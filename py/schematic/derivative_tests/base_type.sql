--# AFTER
CREATE TYPE mybool;
CREATE FUNCTION mybool_in(cstring) RETURNS mybool AS 'boolin' LANGUAGE internal IMMUTABLE STRICT;
CREATE FUNCTION mybool_out(mybool) RETURNS cstring AS 'boolout' LANGUAGE internal IMMUTABLE STRICT;
CREATE TYPE mybool (
    INPUT = mybool_in,
    OUTPUT = mybool_out,
    STORAGE = plain,
    INTERNALLENGTH = 4,
    ALIGNMENT = int4,
    PASSEDBYVALUE = true,
    CATEGORY = 'B',
    PREFERRED = true,
    DEFAULT = 't',
    DELIMITER = ';',
    COLLATABLE = true
);
COMMENT ON TYPE mybool IS 'hi';

--# BEFORE empty

--# CHECK
SELECT
    array[n.nspname, t.typname] AS qualname, t.typlen, t.typbyval, t.typcategory, t.typispreferred, t.typdefault, t.typisdefined,
    t.typdelim, t.typalign, t.typstorage, t.typbasetype,
    (SELECT x.rolname FROM pg_catalog.pg_authid x WHERE x.oid = t.typowner) AS rolname,
    (SELECT array[y.nspname, x.proname] FROM pg_catalog.pg_proc x JOIN pg_catalog.pg_namespace y ON x.pronamespace = y.oid WHERE x.oid = t.typinput) AS input_func,
    (SELECT array[y.nspname, x.proname] FROM pg_catalog.pg_proc x JOIN pg_catalog.pg_namespace y ON x.pronamespace = y.oid WHERE x.oid = t.typoutput) AS output_func,
    (SELECT array[y.nspname, x.proname] FROM pg_catalog.pg_proc x JOIN pg_catalog.pg_namespace y ON x.pronamespace = y.oid WHERE x.oid = t.typreceive) AS receive_func,
    (SELECT array[y.nspname, x.proname] FROM pg_catalog.pg_proc x JOIN pg_catalog.pg_namespace y ON x.pronamespace = y.oid WHERE x.oid = t.typsend) AS send_func,
    (SELECT array[y.nspname, x.proname] FROM pg_catalog.pg_proc x JOIN pg_catalog.pg_namespace y ON x.pronamespace = y.oid WHERE x.oid = t.typmodin) AS modin_func,
    (SELECT array[y.nspname, x.proname] FROM pg_catalog.pg_proc x JOIN pg_catalog.pg_namespace y ON x.pronamespace = y.oid WHERE x.oid = t.typmodout) AS modout_func,
    (SELECT array[y.nspname, x.proname] FROM pg_catalog.pg_proc x JOIN pg_catalog.pg_namespace y ON x.pronamespace = y.oid WHERE x.oid = t.typanalyze) AS analyze_func,
    (SELECT array[y.nspname, x.proname] FROM pg_catalog.pg_proc x JOIN pg_catalog.pg_namespace y ON x.pronamespace = y.oid WHERE x.oid = nullif(nullif(pg_temp.safe_catalog_column('pg_type', 'typsubscript', 'oid = ' || t.oid, '0'), '-'), '0')::oid) AS subscript_func,
    (SELECT array[y.nspname, pg_catalog.format_type(x.oid, x.typtypmod)] FROM pg_catalog.pg_type x JOIN pg_catalog.pg_namespace y ON x.typnamespace = y.oid WHERE x.oid = t.typelem) AS subscript_type,
    (SELECT x.collname FROM pg_collation x WHERE x.oid = t.typcollation) AS collname,
    pg_catalog.obj_description(t.oid) AS objcomment
FROM pg_catalog.pg_type t
JOIN pg_catalog.pg_namespace n ON n.oid = t.typnamespace
WHERE n.nspname != 'information_schema' AND n.nspname !~ '^pg_'
AND t.typtype IN ('b')
AND NOT EXISTS ( -- postgresql automatically creates an array type for other user types, don't try to create that base type ourselves
    SELECT 1
    FROM pg_catalog.pg_type elemtype
    WHERE elemtype.oid = t.typelem
    AND elemtype.typtype IN ('c', 'd', 'e', 'r', 'm', 'b')
    AND t.typname LIKE '_%')
ORDER BY 1;
