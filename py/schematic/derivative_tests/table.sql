--# AFTER
CREATE TABLE public.bar (id integer unique, DOB date, code text, bar2_fk integer, start_date DATE, end_date DATE);
CREATE TABLE public.bar2 (id integer unique);
ALTER TABLE public.bar ADD CONSTRAINT name_code_chk CHECK (DOB >= '1970-01-01'::date) NO INHERIT;
ALTER TABLE public.bar ADD PRIMARY KEY (id);
ALTER TABLE public.bar ADD CONSTRAINT bar_dob_code_uniq UNIQUE (DOB, code);
ALTER TABLE public.bar ADD CONSTRAINT bar_fk FOREIGN KEY (bar2_fk) REFERENCES bar2 (id) NOT VALID;
ALTER TABLE public.bar ADD CONSTRAINT bar_esc EXCLUDE USING gist (daterange(start_date, end_date, '[]') WITH &&);
ALTER TABLE public.bar FORCE ROW LEVEL SECURITY;

COMMENT ON TABLE public.bar IS 'this';

CREATE TABLE public.baz ();
CREATE TABLE public.foo0 () INHERITS ( public.bar, public.baz );
ALTER TABLE public.foo0 ENABLE ROW LEVEL SECURITY;

CREATE TABLE public.foo1 (a int, b smallint) PARTITION BY RANGE ( a int4_ops, b int2_ops );
ALTER TABLE public.foo0 DISABLE ROW LEVEL SECURITY;

CREATE TABLE public.foo2 (name text NOT NULL) PARTITION BY RANGE ( name COLLATE "zu-ZA-x-icu" text_ops);
-- ALTER TABLE public.foo2 ADD PRIMARY KEY (name);
CREATE TABLE public.foo2_name_a_d (name text NOT NULL);
ALTER TABLE public.foo2 ATTACH PARTITION foo2_name_a_d FOR VALUES FROM ('a') TO ('d');

CREATE TABLE public.foo4 (id integer) PARTITION BY LIST (id);

CREATE TABLE public.foo4_part ();
ALTER TABLE public.foo4 FORCE ROW LEVEL SECURITY;

CREATE TABLE public.foo5 () WITH ( toast_tuple_target=4123, parallel_workers=6 );

CREATE TABLE public.foo6 (a text, b int) USING heap;
CREATE FUNCTION fn_foo6_insert_trg() RETURNS TRIGGER
    LANGUAGE plpgsql AS $$ BEGIN
        INSERT INTO foo6 (a) VALUES (NEW.a);
        RETURN NEW;
    END $$;
CREATE RULE notify_foo6 AS ON UPDATE TO foo6 DO ALSO NOTIFY foo6;
ALTER TABLE public.foo6 ENABLE RULE notify_foo6;
ALTER TABLE public.foo6 DISABLE RULE notify_foo6;

CREATE TRIGGER foo6_trigger AFTER INSERT ON foo6 FOR EACH ROW EXECUTE PROCEDURE fn_foo6_insert_trg();
ALTER TABLE public.foo6 ENABLE TRIGGER foo6_trigger;

CREATE TYPE my_table_type AS (a bigint, b smallint, c text collate "C.utf8");
CREATE UNLOGGED TABLE my_typed_table OF my_table_type ( a, b, c) PARTITION BY RANGE ( b int2_ops);

CREATE TABLE public.foo7 (a text, b int) PARTITION BY RANGE ( a COLLATE "zu-ZA-x-icu" text_ops, (a || b::text) COLLATE "C.utf8" text_ops, (hashtext(a)::text || b) COLLATE "ucs_basic" text_ops );
CREATE ROLE me;
ALTER TABLE public.foo7 OWNER TO  me;

CREATE TABLE public.foo8 ( a text not null);
CREATE RULE notify_foo8 AS ON UPDATE TO foo8 DO ALSO NOTIFY foo8;
ALTER TABLE public.foo8 ENABLE RULE notify_foo8;
CREATE UNIQUE INDEX foo8_idx ON public.foo8 USING btree (a);
ALTER TABLE public.foo8 CLUSTER ON foo8_idx;
ALTER TABLE public.foo8 REPLICA IDENTITY USING INDEX foo8_idx;


CREATE TABLE public.bar_parent(id integer);
CREATE TABLE public.baz_parent(name text);
CREATE TABLE public.foo_parent(update_at timestamp);
CREATE TABLE public.table_check_inherits (update_at timestamp) INHERITS ( public.bar_parent, public.baz_parent );


CREATE TABLE public.foo_test_cluster (id integer, total_spent bigint );
CREATE INDEX ix_test_table_cluster ON public.foo_test_cluster USING btree ( total_spent );
ALTER TABLE public.foo_test_cluster CLUSTER ON ix_test_table_cluster;

--# BEFORE
CREATE TABLE public.foo8 ( a text );
CREATE RULE notify_foo8 AS ON UPDATE TO foo8 DO ALSO NOTIFY foo8;

CREATE TABLE public.foo5 () WITH ( fillfactor=90, toast_tuple_target=3000 );
COMMENT ON TABLE public.foo5 IS 'this';

CREATE TABLE public.foo6 (a text, b int);
CREATE RULE notify_foo6 AS ON UPDATE TO foo6 DO ALSO NOTIFY foo6;
ALTER TABLE public.foo6 ENABLE RULE notify_foo6;

-- we don't drop constraints for now
-- ALTER TABLE public.foo6 ADD CONSTRAINT foo1_pkey UNIQUE (a, b);

CREATE TYPE my_table_type AS (a bigint, b smallint, c text collate "C.utf8");

CREATE TABLE public.bar_parent(id integer);
CREATE TABLE public.baz_parent(name text);
CREATE TABLE public.foo_parent(update_at timestamp);
-- Quick explanation about why I did not leave this test active. Here, we should have a test case
-- for when we want to alter a table to `NO INHERIT`,
-- but the `NO INHERIT` command does not drop columns, and we'd still have the columns
-- from the parent retained, but the relationship severed. Since we do not support automatic dropping of columns, and I couldn't think of a way
-- to game the `AFTER` table (by introducing the column manually to it as it causes a failure with the attribute property `attislocal`)
-- I decided to leave the test out.
-- CREATE TABLE public.table_check_inherits () INHERITS ( public.foo_parent );

-- Another test left out is this:
-- CREATE TABLE public.table_check_inherits (id integer, name text);
-- This is the case of an existing table being added as a child to another table (notice that this test says the table exists, and we only have to make it a child of another)
-- The problem is that `attlocal` will always be True for columns in the target table, even if it's not true for the ref table.


--# CHECK tables
SELECT
    array[n.nspname, c.relname] AS qualname, c.relpersistence, pg_catalog.pg_get_expr(c.relpartbound, c.oid) AS part_bound,
    inherits.inherits, partitions.partitions, constraints.constraints,
    partition_parent.parent_table, triggers.triggers, ty.ty, rules.rules,
    c.relrowsecurity, c.relforcerowsecurity, c.relreplident, c.reloptions, c.relispartition, c.relacl,
    pt.partstrat, pt.partnatts, pt.partattrs, pt.partclass, pt.partcollation,
    partcols.partcols, pg_catalog.pg_get_expr(pt.partexprs, pt.partrelid) AS partexpressions,
    am.amname, tblspc.spcname,
    pg_catalog.pg_get_partkeydef(c.oid) AS partdef,
    pg_catalog.pg_get_userbyid(c.relowner) AS rolname,
    (SELECT array[n.nspname, relty.typname] FROM pg_catalog.pg_type relty join pg_catalog.pg_namespace n ON n.oid = relty.typnamespace WHERE relty.oid = c.reloftype) AS of_type,
    (
        SELECT array[ns.nspname, cl.relname]
        FROM pg_catalog.pg_index i
        JOIN pg_class cl ON cl.oid = i.indexrelid
        JOIN pg_catalog.pg_namespace ns ON ns.oid = cl.relnamespace
        WHERE i.indrelid = c.oid AND i.indisreplident
    ) AS replica_index,
    (
        SELECT array[n.nspname, cl.relname]
        FROM pg_catalog.pg_index i
        JOIN pg_catalog.pg_class cl ON i.indexrelid = cl.oid
        JOIN pg_catalog.pg_namespace n ON n.oid = cl.relnamespace
        WHERE i.indrelid = c.oid AND i.indisclustered
    ) AS cluster_index,
    pg_catalog.obj_description(c.oid) AS objcomment
FROM pg_catalog.pg_class c
JOIN pg_catalog.pg_namespace n ON n.oid = c.relnamespace
LEFT JOIN LATERAL ( -- parent tables this table inherits from
    SELECT array_agg(
        array[pn.nspname, p.relname]
        ORDER BY p.relname, pn.nspname) AS inherits
    FROM pg_catalog.pg_inherits i
    JOIN pg_catalog.pg_class p ON i.inhparent = p.oid
    JOIN pg_catalog.pg_namespace pn ON pn.oid = p.relnamespace
    WHERE i.inhrelid = c.oid
) AS inherits ON true
LEFT JOIN LATERAL ( -- this table's parent. it's `PARTITION OF` table
    SELECT array[pn.nspname, p.relname] AS parent_table
    FROM pg_catalog.pg_inherits i
    JOIN pg_catalog.pg_class p ON i.inhparent = p.oid
    JOIN pg_catalog.pg_namespace pn ON pn.oid = p.relnamespace
    WHERE i.inhrelid = c.oid AND c.relispartition is true
) AS partition_parent ON true
LEFT JOIN LATERAL ( -- existing partitions of this table
    SELECT
        json_agg(json_build_object(
            'part_name', array[pn.nspname, p.relname],
            'bound', pg_catalog.pg_get_expr(p.relpartbound, p.oid)
        ) ORDER BY p.oid) AS partitions
    FROM pg_catalog.pg_inherits i
    JOIN pg_catalog.pg_class p ON i.inhrelid = p.oid
    JOIN pg_catalog.pg_namespace pn ON pn.oid = p.relnamespace
    WHERE i.inhparent = c.oid AND p.relispartition is true
) AS partitions ON true
LEFT JOIN LATERAL ( -- constraints on this table
    SELECT
        json_agg(json_build_object(
            'conname', n.nspname || '.' || con.conname,
            'constraint_def', pg_catalog.pg_get_constraintdef(con.oid),
            'index_name', ind.relname,
            'indisvalid', i.indisvalid,
            'convalidated', con.convalidated,
            'contype', con.contype,
            'connoinherit', con.connoinherit,
            'condeferrable', con.condeferrable,
            'condeferred', con.condeferred,
            'indkind', ind.relkind
        ) ORDER BY con.conname) AS constraints
    FROM pg_catalog.pg_constraint con
    JOIN pg_catalog.pg_namespace n ON n.oid = con.connamespace
    LEFT JOIN pg_catalog.pg_class ind ON ind.oid = con.conindid
    LEFT JOIN pg_catalog.pg_index i ON i.indexrelid = ind.oid
    WHERE con.conrelid = c.oid
) AS constraints ON true
LEFT JOIN LATERAL ( -- triggers associated with this table
    SELECT
        json_object_agg(t.tgname, t.tgenabled) AS triggers
    FROM pg_catalog.pg_trigger t
    WHERE t.tgrelid = c.oid AND NOT t.tgisinternal
) AS triggers ON true
LEFT JOIN LATERAL ( -- rules associated with this table
    SELECT
        json_object_agg(r.rulename, r.ev_enabled) AS rules
    FROM pg_catalog.pg_rewrite r
    WHERE r.ev_class = c.oid
) AS rules ON true
LEFT JOIN LATERAL ( -- the underlying composite type of the table
    SELECT
        json_build_object(
            'typname', array[n.nspname, t.typname]
        ) AS ty
    FROM pg_catalog.pg_type t
    JOIN pg_catalog.pg_namespace n ON n.oid = t.typnamespace
    WHERE t.oid = c.reloftype
) AS ty ON true
LEFT JOIN pg_catalog.pg_partitioned_table pt ON pt.partrelid = c.oid
LEFT JOIN LATERAL ( -- columns in the partition key
    WITH cols AS (
        SELECT
            unnest(pt.partattrs) AS colid,
            unnest(pt.partclass) AS opclassid,
            unnest(pt.partcollation) AS collationid
    )
    SELECT array_agg(array[att.attname, opclass.opcname, collat.collname]) AS partcols
    FROM cols
    LEFT JOIN pg_catalog.pg_attribute att ON att.attrelid = c.oid AND att.attnum = cols.colid
    JOIN pg_catalog.pg_opclass opclass ON opclass.oid = cols.opclassid
    LEFT JOIN pg_catalog.pg_collation collat ON collat.oid = cols.collationid
) AS partcols ON true
LEFT JOIN pg_catalog.pg_am am ON am.oid = c.relam
LEFT JOIN pg_catalog.pg_tablespace tblspc ON tblspc.oid = c.reltablespace
WHERE n.nspname != 'information_schema' AND n.nspname !~ '^pg_'
AND c.relkind IN ('r', 'p')
ORDER BY 1;

--# CHECK columns
SELECT
    array[n.nspname, c.relname, a.attname] AS qualname,
    pg_catalog.format_type(t.oid, t.typtypmod) AS typname,
    a.attstattarget, a.attstorage,
    pg_temp.safe_catalog_column('pg_attribute', 'attcompression', 'attrelid = ' || a.attrelid, '')::char AS attcompression,
    a.attnotnull, a.atthasmissing, a.attisdropped,
    a.attislocal,
    a.attinhcount, collat.collname, a.attacl, a.attoptions, a.attfdwoptions,
    a.attidentity, a.attgenerated, a.atthasdef,
    pg_catalog.pg_get_expr(attrdef.adbin, attrdef.adrelid) AS attdef,
    pg_catalog.col_description(c.oid, a.attnum) AS objcomment
FROM pg_catalog.pg_class c
JOIN pg_catalog.pg_namespace n ON n.oid = c.relnamespace
JOIN pg_catalog.pg_attribute a ON a.attrelid = c.oid AND a.attnum > 0
LEFT JOIN pg_catalog.pg_type t ON t.oid = a.atttypid
LEFT JOIN pg_catalog.pg_collation collat ON collat.oid = a.attcollation
LEFT JOIN pg_catalog.pg_attrdef attrdef ON attrdef.adrelid = a.attrelid AND attrdef.adnum = a.attnum
WHERE n.nspname != 'information_schema' AND n.nspname !~ '^pg_'
AND c.relkind IN ('r', 'p', 'f')
ORDER BY 1, a.attnum;
