--# AFTER
CREATE COLLATION german_phonebook (provider = icu, locale = 'de-u-co-phonebk');

CREATE COLLATION german FROM "german_phonebook";

CREATE COLLATION custom (provider = icu, locale = 'und');

CREATE ROLE me;
ALTER COLLATION "custom" OWNER TO me;

COMMENT ON COLLATION custom IS 'this';

--# BEFORE
CREATE ROLE me;
CREATE COLLATION custom (provider = icu, locale = 'und');

COMMENT ON COLLATION custom IS 'that';

--# CHECK
SELECT
    ARRAY [n.nspname, c.collname] as qualname,
    json_build_object(
        'LOCALE', pg_temp.safe_catalog_column('pg_collation', 'colliculocale', 'oid = ' || c.oid, '')::text,
        'LC_COLLATE', c.collcollate,
        'LC_CTYPE', c.collctype,
        'PROVIDER', c.collprovider,
        'DETERMINISTIC', c.collisdeterministic,
        'RULES',  pg_temp.safe_catalog_column('pg_collation', 'collicurules', 'oid = ' || c.oid, '')::text,
        'VERSION', c.collversion
    ) AS opts,
    pg_catalog.pg_get_userbyid(c.collowner) AS rolname,
    pg_catalog.obj_description(c.oid, 'pg_collation') AS objcomment
FROM pg_catalog.pg_collation c
JOIN pg_catalog.pg_namespace n ON n.oid = c.collnamespace
WHERE n.nspname !~ 'pg_.*'
ORDER BY 1;
