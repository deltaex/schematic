--# AFTER
CREATE DOMAIN ctext AS text COLLATE "C.utf8" DEFAULT 'hello' NOT NULL;
COMMENT ON DOMAIN ctext IS 'hi';
CREATE DOMAIN dood2 text COLLATE "C.utf8" CONSTRAINT dood2conname1 CHECK (char_length(VALUE) BETWEEN 10 AND 20);
CREATE DOMAIN dood3 text COLLATE "C.utf8" CONSTRAINT dood3conname1 CHECK (char_length(VALUE) BETWEEN 10 AND 20) CONSTRAINT dood3conname2 CHECK (VALUE ~ 'hellodood3') DEFAULT '321';
CREATE DOMAIN text9 AS character varying(9);
CREATE DOMAIN texta9 AS character varying(9)[];
CREATE DOMAIN ctext1 AS text[] COLLATE "C.utf8" DEFAULT '{}' NOT NULL;
CREATE DOMAIN ctext2 AS text[][] COLLATE "C.utf8" DEFAULT '{}' NOT NULL;
CREATE DOMAIN ctext3 AS ctext1[] COLLATE "C.utf8" DEFAULT '{}' NOT NULL;
CREATE DOMAIN ctext4 AS ctext2[] COLLATE "C.utf8" DEFAULT '{}' NOT NULL;
CREATE DOMAIN ctext5 AS ctext3[] COLLATE "C.utf8" DEFAULT '{}' NOT NULL;
CREATE DOMAIN dchar1 AS "char";


--# BEFORE empty

--# BEFORE has constraint
CREATE DOMAIN dood3 text COLLATE "C.utf8" CONSTRAINT dood3conname1 CHECK (char_length(VALUE) BETWEEN 10 AND 20)

--# BEFORE change constraint
CREATE DOMAIN dood3 text COLLATE "C.utf8" CONSTRAINT dood3conname1 CHECK (char_length(VALUE) BETWEEN 11 AND 21)

--# BEFORE options
CREATE DOMAIN dood3 text COLLATE "C.utf8" CONSTRAINT dood3conname_to_remove NOT NULL DEFAULT 123::text;

--# CHECK
SELECT
    array[n.nspname, t.typname] AS qualname, t.typnotnull, t.typndims, t.typtypmod,
    pg_catalog.pg_get_expr(t.typdefaultbin, 0) AS ddefaultexp,
    -- format_type only adds up to one set of array brackets, but additionals are needed for types like text[][]
    pg_catalog.format_type(t.typbasetype, t.typtypmod) || repeat('[]', t.typndims - 1) AS domaintype,
    (SELECT x.rolname FROM pg_catalog.pg_authid x WHERE x.oid = t.typowner) AS rolname,
    (SELECT x.collname FROM pg_catalog.pg_collation x WHERE x.oid = t.typcollation) AS collname,
    constraints.domainconstraints,
    pg_catalog.obj_description(t.oid) AS objcomment
FROM pg_catalog.pg_type t
JOIN pg_catalog.pg_namespace n ON n.oid = t.typnamespace
LEFT JOIN LATERAL (
    SELECT array_agg(array[x.conname, pg_catalog.pg_get_constraintdef(x.oid, true)] ORDER BY x.oid) AS domainconstraints
    FROM pg_catalog.pg_constraint x
    WHERE t.oid = x.contypid
) AS constraints ON true
WHERE n.nspname != 'information_schema' AND n.nspname !~ '^pg_'
AND t.typtype = 'd'
ORDER BY 1;
