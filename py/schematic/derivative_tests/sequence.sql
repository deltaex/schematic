--# AFTER
CREATE UNLOGGED SEQUENCE public.hello_id_seq START 50 MAXVALUE 99 AS int2 RESTART 66;
-- CREATE UNLOGGED SEQUENCE public.hello_id_seq MAXVALUE 99 AS int2;
COMMENT ON SEQUENCE public.hello_id_seq IS 'hi';

--# AFTER null comment
CREATE SEQUENCE public.hello_id_seq;
COMMENT ON SEQUENCE public.hello_id_seq IS null;

--# AFTER
CREATE TABLE public.bar (id int);
CREATE SEQUENCE public.hello_id_seq CYCLE OWNED BY public.bar.id;

--# AFTER restart
CREATE SEQUENCE public.hello_id_seq;
CREATE SEQUENCE public.seqrestart RESTART 99;

--# BEFORE from default
CREATE SEQUENCE public.hello_id_seq;
COMMENT ON SEQUENCE public.hello_id_seq IS 'wat';

--# BEFORE empty

--# CHECK
SELECT
    array[n.nspname, c.relname] AS qualname,
    s.seqstart, s.seqincrement, s.seqmax, s.seqmin, s.seqcache, s.seqcycle, c.relpersistence,
    owned_by.owned_by, pg_catalog.format_type(t.oid, t.typtypmod) AS typname,
    pg_temp.get_sequence_last_value(n.nspname, c.relname) AS last_value,
    pg_catalog.pg_get_userbyid(c.relowner) AS rolname,
    pg_catalog.obj_description(c.oid, 'pg_class') AS objcomment
FROM pg_catalog.pg_class c
JOIN pg_catalog.pg_namespace n ON n.oid = c.relnamespace
JOIN pg_catalog.pg_sequence s ON s.seqrelid = c.oid
JOIN pg_catalog.pg_type t ON t.oid = s.seqtypid
LEFT JOIN LATERAL ( -- which column owns this sequence
    SELECT array[tbl_nsp.nspname, tbl.relname, a.attname] AS owned_by
    FROM pg_catalog.pg_depend d
    JOIN pg_catalog.pg_class deptype ON deptype.oid = d.classid AND deptype.relname = 'pg_class'
    JOIN pg_catalog.pg_namespace depnsp ON depnsp.oid = deptype.relnamespace AND depnsp.nspname = 'pg_catalog'
    JOIN pg_catalog.pg_class seq ON seq.oid = d.objid
    JOIN pg_catalog.pg_namespace seq_nsp ON seq_nsp.oid = seq.relnamespace
    JOIN pg_catalog.pg_class tbl ON tbl.oid = d.refobjid
    JOIN pg_catalog.pg_namespace tbl_nsp ON tbl_nsp.oid = tbl.relnamespace
    JOIN pg_catalog.pg_attribute a ON a.attrelid = tbl.oid AND a.attnum = d.refobjsubid
    WHERE seq_nsp.nspname = n.nspname AND seq.relname = c.relname
) AS owned_by ON true
WHERE n.nspname != 'information_schema' AND n.nspname !~ '^pg_'
AND c.relkind = 'S'
ORDER BY 1;
