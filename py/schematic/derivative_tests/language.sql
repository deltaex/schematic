--# AFTER
CREATE LANGUAGE plsample
    HANDLER plpgsql_call_handler;

CREATE ROLE pubrole;

CREATE LANGUAGE plsample2
    HANDLER plpgsql_call_handler;

ALTER LANGUAGE plsample2 OWNER TO pubrole;

COMMENT ON LANGUAGE plsample2 IS 'com';
--# BEFORE

CREATE ROLE pubrole;

CREATE LANGUAGE plsample2
    HANDLER plpgsql_call_handler;

--# CHECK
SELECT
    ARRAY[l.lanname] AS qualname,
    (SELECT rolname FROM pg_catalog.pg_authid a WHERE l.lanowner= a.oid) AS rolname,
    lanpltrusted,
    (
        SELECT ARRAY[n.nspname, p.proname]
        FROM pg_catalog.pg_proc p
        JOIN pg_catalog.pg_namespace n ON n.oid = p.pronamespace
        WHERE p.oid = l.lanplcallfoid
    ) AS callproc,
    (
        SELECT ARRAY[n.nspname, p.proname]
        FROM pg_catalog.pg_proc p
        JOIN pg_catalog.pg_namespace n ON n.oid = p.pronamespace
        WHERE p.oid = l.laninline
    ) AS inlineproc,
    (
        SELECT ARRAY[n.nspname, p.proname]
        FROM pg_catalog.pg_proc p
        JOIN pg_catalog.pg_namespace n ON n.oid = p.pronamespace
        WHERE p.oid = l.lanvalidator
    ) AS valproc,
    pg_catalog.obj_description(l.oid, 'pg_language') AS objcomment
FROM pg_catalog.pg_language l
WHERE l.lanispl
ORDER BY 1;
