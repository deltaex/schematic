--# AFTER
CREATE TEXT SEARCH PARSER public.my_text_parser (
    START = pg_catalog.prsd_start,
    GETTOKEN = pg_catalog.prsd_nexttoken,
    END = pg_catalog.prsd_end,
    LEXTYPES = pg_catalog.prsd_lextype,
    HEADLINE = pg_catalog.prsd_headline
);

CREATE TEXT SEARCH PARSER public.my_text_parser_no_headline (
    START = pg_catalog.prsd_start,
    GETTOKEN = pg_catalog.prsd_nexttoken,
    END = pg_catalog.prsd_end,
    LEXTYPES = pg_catalog.prsd_lextype
);

--# BEFORE

--# CHECK
SELECT
    ARRAY[n.nspname, tp.prsname] AS qualname,
    (
        SELECT ARRAY[np.nspname, p.proname]
        FROM pg_catalog.pg_proc p
        JOIN pg_catalog.pg_namespace np ON np.oid = p.pronamespace
        WHERE p.oid = tp.prsstart
    ) AS prsstart,
    (
        SELECT ARRAY[np.nspname, p.proname]
        FROM pg_catalog.pg_proc p
        JOIN pg_catalog.pg_namespace np ON np.oid = p.pronamespace
        WHERE p.oid = tp.prstoken
    ) AS prstoken,
    (
        SELECT ARRAY[np.nspname, p.proname]
        FROM pg_catalog.pg_proc p
        JOIN pg_catalog.pg_namespace np ON np.oid = p.pronamespace
        WHERE p.oid = tp.prsend
    ) AS prsend,
    (
        SELECT ARRAY[np.nspname, p.proname]
        FROM pg_catalog.pg_proc p
        JOIN pg_catalog.pg_namespace np ON np.oid = p.pronamespace
        WHERE p.oid = tp.prsheadline
    ) AS prsheadline,
    (
        SELECT ARRAY[np.nspname, p.proname]
        FROM pg_catalog.pg_proc p
        JOIN pg_catalog.pg_namespace np ON np.oid = p.pronamespace
        WHERE p.oid = tp.prslextype
    ) AS prslextype,
    pg_catalog.obj_description(tp.oid, 'pg_ts_parser') AS objcomment
FROM pg_catalog.pg_ts_parser tp
JOIN pg_catalog.pg_namespace n ON n.oid = tp.prsnamespace
WHERE n.nspname !~ '^pg_'
ORDER BY 1;
