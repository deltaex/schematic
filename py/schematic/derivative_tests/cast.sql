--# AFTER
CREATE TYPE myint AS (field integer);
CREATE TYPE myfloat AS (field decimal);

CREATE OR REPLACE FUNCTION public.into_text(value public.myint)
RETURNS text
AS
$func$
BEGIN
    RETURN value::text;
END;
$func$
LANGUAGE plpgsql;

CREATE CAST (public.myint AS pg_catalog.text) WITH FUNCTION public.into_text(public.myint) AS ASSIGNMENT;

CREATE CAST (public.myint AS pg_catalog.int2) WITH INOUT AS IMPLICIT;
CREATE CAST (public.myint AS pg_catalog.int4) WITH INOUT AS IMPLICIT;

CREATE CAST (public.myint AS pg_catalog.int8) WITH INOUT AS IMPLICIT;
COMMENT ON CAST (public.myint AS pg_catalog.int8) IS 'hi';

CREATE TYPE public.aid AS (
    type smallint,
    mod smallint,
    id integer
);
CREATE OR REPLACE FUNCTION public.bigint_to_aid(bigint) RETURNS public.aid
LANGUAGE sql IMMUTABLE STRICT LEAKPROOF PARALLEL SAFE AS $_$
    SELECT (
        (($1::bit(64))::int::smallint),
        ((($1::bit(64) << 16)::bit(16))::int::smallint),
        ((($1::bit(64) << 32)::bit(32))::int)
    )::public.aid;
$_$;
CREATE CAST (bigint AS public.aid) WITH FUNCTION public.bigint_to_aid(bigint) AS IMPLICIT;

CREATE OR REPLACE FUNCTION public.smallint_to_bit(smallint) RETURNS bit
    LANGUAGE sql IMMUTABLE
    AS $_$
            SELECT ($1::smallint)::int::bit(16);
        $_$;

CREATE CAST (smallint AS bit) WITH FUNCTION public.smallint_to_bit(smallint) AS IMPLICIT;

--# BEFORE

CREATE OR REPLACE FUNCTION public.smallint_to_bit(smallint) RETURNS bit
    LANGUAGE sql IMMUTABLE
    AS $_$
            SELECT ($1::smallint)::int::bit(16);
        $_$;

CREATE TYPE public.aid AS (
    type smallint,
    mod smallint,
    id integer
);
CREATE OR REPLACE FUNCTION public.bigint_to_aid(bigint) RETURNS public.aid
LANGUAGE sql IMMUTABLE STRICT LEAKPROOF PARALLEL SAFE AS $_$
    SELECT (
        (($1::bit(64))::int::smallint),
        ((($1::bit(64) << 16)::bit(16))::int::smallint),
        ((($1::bit(64) << 32)::bit(32))::int)
    )::public.aid;
$_$;


CREATE TYPE myint AS (field integer);
CREATE CAST (public.myint AS pg_catalog.int2) WITH INOUT AS IMPLICIT;

CREATE CAST (public.myint AS pg_catalog.int8) WITH INOUT AS IMPLICIT;
COMMENT ON CAST (public.myint AS pg_catalog.int8) IS 'hey';

--# CHECK
SELECT
    ARRAY[CONCAT(ns.nspname, sty.typname, nt.nspname, tty.typname)] AS castdef,
    c.castcontext,
    c.castmethod,
    ARRAY[ns.nspname, sty.typname] AS source_type,
    ARRAY[nt.nspname, tty.typname] AS tgt_type,
    ARRAY[np.nspname, p.proname] AS funcname,
    pg_catalog.pg_get_function_identity_arguments(p.oid) AS funcargs,
    pg_catalog.obj_description(c.oid, 'pg_cast') AS objcomment
FROM pg_catalog.pg_cast c
JOIN pg_catalog.pg_type sty on c.castsource = sty.oid
JOIN pg_catalog.pg_namespace ns on ns.oid = sty.typnamespace
JOIN pg_catalog.pg_type tty on c.casttarget = tty.oid
JOIN pg_catalog.pg_namespace nt on nt.oid = tty.typnamespace
LEFT JOIN pg_catalog.pg_proc p ON p.oid = c.castfunc
LEFT JOIN pg_catalog.pg_namespace np ON np.oid = p.pronamespace
WHERE np.nspname !~ 'pg_.*'
ORDER BY 1;
