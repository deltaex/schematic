--# AFTER
CREATE ROLE myrole;
CREATE TEXT SEARCH CONFIGURATION public.my_config (
    PARSER = pg_catalog.default
);

ALTER TEXT SEARCH CONFIGURATION public.my_config ADD MAPPING FOR asciiword WITH pg_catalog.arabic_stem, pg_catalog.danish_stem;
ALTER TEXT SEARCH CONFIGURATION public.my_config ADD MAPPING FOR word WITH pg_catalog.arabic_stem;
ALTER TEXT SEARCH CONFIGURATION public.my_config ADD MAPPING FOR numword WITH pg_catalog.arabic_stem;
ALTER TEXT SEARCH CONFIGURATION public.my_config ADD MAPPING FOR email WITH pg_catalog.arabic_stem;

CREATE TEXT SEARCH CONFIGURATION public.my_other_config (
    PARSER = pg_catalog.default
);

ALTER TEXT SEARCH CONFIGURATION public.my_other_config ADD MAPPING FOR asciiword WITH pg_catalog.arabic_stem, pg_catalog.danish_stem;
ALTER TEXT SEARCH CONFIGURATION public.my_other_config ADD MAPPING FOR numword WITH pg_catalog.arabic_stem;
ALTER TEXT SEARCH CONFIGURATION public.my_other_config ADD MAPPING FOR email WITH pg_catalog.arabic_stem;

ALTER TEXT SEARCH CONFIGURATION my_other_config OWNER TO myrole;

--# BEFORE
CREATE ROLE myrole;
CREATE TEXT SEARCH CONFIGURATION public.my_other_config (
    PARSER = pg_catalog.default
);

ALTER TEXT SEARCH CONFIGURATION public.my_other_config ADD MAPPING FOR asciiword WITH pg_catalog.arabic_stem, pg_catalog.danish_stem;
ALTER TEXT SEARCH CONFIGURATION public.my_other_config ADD MAPPING FOR word WITH pg_catalog.danish_stem;
ALTER TEXT SEARCH CONFIGURATION public.my_other_config ADD MAPPING FOR email WITH pg_catalog.english_stem;

--# CHECK
SELECT
    ARRAY[n.nspname, tc.cfgname] AS qualname,
    (SELECT rolname FROM pg_catalog.pg_authid a WHERE a.oid = tc.cfgowner) AS rolname,
    ARRAY[np.nspname, p.prsname] as parser,
    (
        WITH x AS (
            SELECT 
                (SELECT alias FROM pg_catalog.ts_token_type(tc.cfgparser) AS t WHERE t.tokid = m.maptokentype) AS token,
                array_agg(ARRAY[np.nspname, d.dictname] ORDER BY m.mapseqno) AS dicts
            FROM pg_catalog.pg_ts_config_map m
            JOIN pg_catalog.pg_ts_dict d ON d.oid = m.mapdict
            JOIN pg_catalog.pg_namespace np ON np.oid = d.dictnamespace
            WHERE m.mapcfg = tc.oid
            GROUP BY m.maptokentype
        )
        SELECT json_object_agg(x.token, x.dicts) FROM x
    ) AS mappings,
    pg_catalog.obj_description(tc.oid, 'pg_ts_config') AS objcomment
FROM pg_catalog.pg_ts_config tc
JOIN pg_catalog.pg_namespace n ON n.oid = tc.cfgnamespace
JOIN pg_catalog.pg_ts_parser p ON p.oid = tc.cfgparser
JOIN pg_catalog.pg_namespace np ON np.oid = p.prsnamespace
WHERE n.nspname !~ '^pg_'
ORDER BY 1;
