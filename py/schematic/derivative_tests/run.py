#! /usr/bin/env python3
'''
Run tests for derivative schema upgrade logic (ie declarative schema that automatically upgrades).

$ ./py/schematic/derivative_tests/run.py

The tests are defined as sql files with three types of sections: AFTER, BEFORE, and CHECK. An AFTER section defines
the target state the database should end up in. A BEFORE section defines the state a database starts in. A CHECK section
defines a query that should return the same result between the AFTER and BEFORE database. Before running the CHECK
sections, the BEFORE database is reconciled to match the AFTER database using the derivative logic.

The intention with the CHECK section is to query postgresql catalog tables for the relevant descriptions of schema to
ensure they match.

There can be multiple statements in each AFTER and BEFORE section. There can also be multiple statements in a CHECK
section, but only the result of the last one is used. There can be multiple instances of each section type; this causes
it to check every possible combination of the three section types together. When using multiple of a given type, give
them a name by putting some text after the section label, otherwise a nondescript name will be given.

See examples for formatting. Notice that sections are delimited with `--# `. Blank lines and comments are ignored.

For CHECK queries, take inspiration from py/schematic/pg_catalog.py queries which are used to detect changes to schema.

Test with postgresql 12 (oldest supported version) with `git apply py/schematic/derivative_tests/test_pg12.patch`.
'''

import glob
import itertools
import os
import re
import sys
import traceback
from collections import OrderedDict
from dataclasses import dataclass
from sys import stderr

import path
from clint.textui import colored
from nose.tools import nottest

from schematic import derivative, pgcn
from schematic.build_util import is_empty_sql
from schematic.scm_cli_utils import _gc_basedir, _sandbox


def parse_section_head(line):
    if match := re.match(r'^--# (?P<name>[\w ]+)\s*$', line):
        return match.group('name')

def is_section_head(line):
    return line.startswith('--# ')

@nottest
def parse_testfile(filename):
    sections = OrderedDict()
    name = None
    seq = 1
    with open(filename, encoding='utf8') as fp:
        for line in fp:
            if is_section_head(line):
                name = parse_section_head(line)
                if name in sections:
                    name = f'{name} {seq}'
                    seq += 1
                sections[name] = ''
            elif name:
                sections[name] += line
    return sections

@dataclass
class TestModule:
    testfile: str
    sections: OrderedDict

    @property
    def filename(self):
        return os.path.basename(self.testfile)

@nottest
def get_test_modules(glob_pattern):
    for testfile in glob.glob(path.Path(os.path.dirname(__file__)) / glob_pattern):
        yield TestModule(testfile=testfile, sections=parse_testfile(testfile))

def get_mod_prefix(testmod, prefix):
    for name, content in testmod.sections.items():
        if name.startswith(prefix):
            yield name, content

PASS = colored.green('PASS')
FAIL = colored.red('FAIL')

def print_row_diff(bef_result, aft_result):
    show_fields = [ # show first few fields (identifiers) or ones that are different
        k for (i, k, b, a) in zip(range(len(bef_result)), bef_result._fields, bef_result, aft_result)
        if b != a or i < 2]
    def printrec(label, rec):
        print(f'''    {label} {', '.join('%s=%r' % (f, getattr(rec, f)) for f in show_fields)}''', file=stderr)  # noqa: UP031
    printrec('BEF', bef_result)
    printrec('AFT', aft_result)

def main():
    aft_basedir = bef_basedir = None
    passes = fails = 0
    glob_pattern = None  # first position arg is a glob pattern to select sql files to test
    for arg in sys.argv[1:]:
        if arg.startswith('-'):
            continue
        glob_pattern = arg
        break
    glob_pattern = glob_pattern or '*.sql'
    action = derivative.action_logexec if '-v' in sys.argv else derivative.action_exec
    try:
        aft_drv = _sandbox([])
        bef_drv = _sandbox([])
        aft_basedir = aft_drv['env']['basedir']
        bef_basedir = bef_drv['env']['basedir']
        with pgcn.connect_dir(aft_basedir) as pg_aft, pgcn.connect_dir(bef_basedir) as pg_bef:
            for testmod in get_test_modules(glob_pattern):
                if fails and '--break' in sys.argv:
                    break
                for aft_name, aft_content in get_mod_prefix(testmod, 'AFTER'):
                    if fails and '--break' in sys.argv:
                        break
                    pg_aft.execute(aft_content) if not is_empty_sql(aft_content) else 0
                    for bef_name, bef_content in get_mod_prefix(testmod, 'BEFORE'):
                        try:
                            pg_bef.execute(bef_content) if not is_empty_sql(bef_content) else 0
                        except Exception as ex:  # noqa: BLE001
                            print(f'{FAIL} {testmod.filename} {bef_name} {ex} (while initializing)', file=stderr)
                            traceback.print_exc(file=stderr)
                            pg_bef.rollback()
                            fails += 1
                            continue
                        try:
                            derivative.add_all(pg_aft, pg_bef, action=action, allow_commit=False)
                        except Exception:  # noqa: BLE001
                            print(f'{FAIL} {testmod.filename} {aft_name} vs {bef_name} (while deriving)', file=stderr)
                            traceback.print_exc(file=stderr)
                            pg_bef.rollback()
                            fails += 1
                            continue
                        for chk_name, chk_content in get_mod_prefix(testmod, 'CHECK'):
                            chkaft_results = pg_aft.execute(chk_content).fetchall()
                            chkbef_results = pg_bef.execute(chk_content).fetchall()
                            if len(chkaft_results) != len(chkbef_results):
                                print(f'{FAIL} {testmod.filename} {aft_name} vs {bef_name} : {chk_name}', file=stderr)
                                print(f'    BEF {len(chkbef_results)} results', file=stderr)
                                print(f'    AFT {len(chkaft_results)} results', file=stderr)
                                fails += 1
                                continue
                            anyfail = False
                            for aft_result, bef_result in itertools.zip_longest(chkaft_results, chkbef_results):
                                if aft_result == bef_result:
                                    passes += 1
                                else:
                                    print(
                                        f'{FAIL} {testmod.filename} {aft_name} vs {bef_name} : {chk_name}', file=stderr)
                                    print_row_diff(bef_result, aft_result)
                                    fails += 1
                                    anyfail = True
                            if not anyfail:
                                print(f'{PASS} {testmod.filename} {aft_name} vs {bef_name} : {chk_name}', file=stderr)
                        pg_bef.rollback()
                    pg_aft.rollback()
        print(f'{PASS} {passes} {FAIL} {fails}') if fails else print(f'{PASS} {passes}')
    finally:
        _gc_basedir(aft_basedir, force=True, stop=True) if aft_basedir else 0
        _gc_basedir(bef_basedir, force=True, stop=True) if bef_basedir else 0

if __name__ == '__main__':
    main()
