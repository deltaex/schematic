--# AFTER
CREATE TABLE public.t1 (
    a   int,
    b   int
);

CREATE TABLE public.t2 (
    a   int,
    b   int
);

CREATE RULE notify_me AS ON UPDATE TO public.t1 DO ALSO NOTIFY t1;

CREATE RULE notify_me2 AS ON DELETE TO public.t2 DO ALSO NOTIFY t2;
COMMENT ON RULE "notify_me2" ON public.t2 IS 'this';
--# BEFORE

CREATE TABLE public.t1 (
    a   int,
    b   int
);

CREATE TABLE public.t2 (
    a   int,
    b   int
);
CREATE RULE notify_me2 AS ON DELETE TO public.t2 DO ALSO NOTIFY t2;

--# CHECK
SELECT
    array[n.nspname, r.rulename, ns.nspname || '.' || c.relname] as qualname,
    pg_get_ruledef(r.oid) AS definition,
    pg_catalog.obj_description(r.oid) AS objcomment
FROM (
    pg_rewrite r
    JOIN pg_catalog.pg_class c ON (c.oid = r.ev_class)
    LEFT JOIN pg_catalog.pg_namespace ns ON ns.oid = c.relnamespace)
LEFT JOIN pg_catalog.pg_namespace n ON (n.oid = c.relnamespace)
WHERE n.nspname != 'information_schema' AND n.nspname !~ '^pg_'
-- there is no difference between a "_RETURN" rule ON SELECT and a postgres view
AND r.rulename != '_RETURN'
ORDER BY 1;
