--# AFTER
CREATE SCHEMA foospc;
COMMENT ON SCHEMA foospc IS 'hi';

--# BEFORE

--# CHECK
SELECT
    array[n.nspname] AS qualname, n.nspacl,
    (SELECT x.rolname FROM pg_catalog.pg_authid x WHERE x.oid = n.nspowner) AS rolname,
    pg_catalog.obj_description(n.oid) AS objcomment
FROM pg_catalog.pg_namespace n
WHERE n.nspname != 'information_schema' AND n.nspname !~ '^pg_'
ORDER BY 1;
