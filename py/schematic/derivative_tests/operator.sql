--# AFTER

CREATE ROLE me;

CREATE OR REPLACE FUNCTION public.eq(var1 anyelement, var2 anyelement) RETURNS bool
LANGUAGE sql CALLED ON NULL INPUT IMMUTABLE PARALLEL SAFE LEAKPROOF AS $function$
    SELECT $1 IS NOT DISTINCT FROM $2;
$function$;

CREATE OR REPLACE FUNCTION public.neq(var1 anyelement, var2 anyelement) RETURNS bool
LANGUAGE sql CALLED ON NULL INPUT IMMUTABLE PARALLEL SAFE LEAKPROOF AS $function$
    SELECT $1 IS DISTINCT FROM $2;
$function$;

CREATE OPERATOR public.#= (
    LEFTARG = anyelement,
    RIGHTARG = anyelement,
    FUNCTION = public.eq,
    NEGATOR = OPERATOR(public.#!=)
);

CREATE OPERATOR public.#!= (
    LEFTARG = anyelement,
    RIGHTARG = anyelement,
    FUNCTION = public.neq,
    NEGATOR = OPERATOR(public.#=)
);

ALTER OPERATOR public.#!= (anyelement, anyelement) SET ( RESTRICT =  neqsel);
ALTER OPERATOR public.#!= (anyelement, anyelement) OWNER TO me;

COMMENT ON OPERATOR public.#!= (anyelement, anyelement) IS 'this'; 

--# BEFORE

CREATE OR REPLACE FUNCTION public.eq(var1 anyelement, var2 anyelement) RETURNS bool
LANGUAGE sql CALLED ON NULL INPUT IMMUTABLE PARALLEL SAFE LEAKPROOF AS $function$
    SELECT $1 IS NOT DISTINCT FROM $2;
$function$;

CREATE OR REPLACE FUNCTION public.neq(var1 anyelement, var2 anyelement) RETURNS bool
LANGUAGE sql CALLED ON NULL INPUT IMMUTABLE PARALLEL SAFE LEAKPROOF AS $function$
    SELECT $1 IS DISTINCT FROM $2;
$function$;

CREATE ROLE me;

CREATE OPERATOR public.#!= (
    LEFTARG = anyelement,
    RIGHTARG = anyelement,
    FUNCTION = public.neq
);

COMMENT ON OPERATOR public.#!=  (anyelement, anyelement) IS 'that'; 

--# CHECK
SELECT
        ARRAY [n.nspname, o.oprname] as qualname,
        (SELECT rolname FROM pg_catalog.pg_authid a WHERE a.oid = o.oprowner) AS rolname,
        o.oprkind,
        o.oprcanmerge,
        o.oprcanhash,
        (
            SELECT ARRAY[np.nspname, t.typname]
            FROM pg_catalog.pg_type t JOIN pg_catalog.pg_namespace np ON np.oid = t.typnamespace
            WHERE t.oid = o.oprleft
        ) AS oprleft,
        (
            SELECT ARRAY[np.nspname, t.typname]
            FROM pg_catalog.pg_type t JOIN pg_catalog.pg_namespace np ON np.oid = t.typnamespace
            WHERE t.oid = o.oprright
        ) AS oprright,
        (
            SELECT ARRAY[np.nspname, op.oprname]
            FROM pg_catalog.pg_operator op JOIN pg_catalog.pg_namespace np ON np.oid = op.oprnamespace
            WHERE op.oid = o.oprcom
        ) AS oprcom,
        (
            SELECT ARRAY[np.nspname, op.oprname]
            FROM pg_catalog.pg_operator op JOIN pg_catalog.pg_namespace np ON np.oid = op.oprnamespace
            WHERE op.oid = o.oprnegate
        ) AS oprnegate,
        (
            SELECT ARRAY[np.nspname, p.proname]
            FROM pg_catalog.pg_proc p JOIN pg_catalog.pg_namespace np ON np.oid = p.pronamespace
            WHERE p.oid = o.oprcode
        ) AS oprcode,
        (
            SELECT ARRAY[np.nspname, p.proname]
            FROM pg_catalog.pg_proc p JOIN pg_catalog.pg_namespace np ON np.oid = p.pronamespace
            WHERE p.oid = o.oprrest
        ) AS oprrest,
        (
            SELECT ARRAY[np.nspname, p.proname]
            FROM pg_catalog.pg_proc p JOIN pg_catalog.pg_namespace np ON np.oid = p.pronamespace
            WHERE p.oid = o.oprjoin
        ) AS oprjoin,
        pg_catalog.obj_description(o.oid, 'pg_operator') AS objcomment
    FROM pg_catalog.pg_operator o
    JOIN pg_catalog.pg_namespace n ON n.oid = o.oprnamespace
    WHERE n.nspname !~ 'pg_.*'
    ORDER BY 1;
