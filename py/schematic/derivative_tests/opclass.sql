--# AFTER
CREATE TYPE mytext AS (a text);

CREATE OR REPLACE FUNCTION function_less_than(mytext, mytext)
RETURNS boolean AS
$$
BEGIN
    RETURN $1 :: text < $2 :: text;
END;
$$ LANGUAGE 'plpgsql' IMMUTABLE;

CREATE OR REPLACE FUNCTION function_equals(mytext, mytext)
RETURNS boolean AS
$$
BEGIN
    RETURN $1 = $2;
END;
$$ LANGUAGE 'plpgsql' IMMUTABLE;

CREATE OR REPLACE FUNCTION dumb_fn(mytext, mytext)
RETURNS integer AS
$$
BEGIN
    RETURN 0;
END;
$$ LANGUAGE 'plpgsql' IMMUTABLE;


CREATE OPERATOR <# (
    LEFTARG = mytext,
    RIGHTARG = mytext,
    PROCEDURE = function_less_than
);

CREATE OPERATOR ==# (
    LEFTARG = mytext,
    RIGHTARG = mytext,
    PROCEDURE = function_equals
);


CREATE OPERATOR FAMILY mytextops USING GiST;

CREATE TYPE myt AS ( t mytext);

CREATE OPERATOR CLASS mytextopsclass
DEFAULT FOR TYPE mytext USING GiST FAMILY mytextops AS
    OPERATOR        2       <#,
    OPERATOR        4       ==#,
    FUNCTION        1       dumb_fn(mytext, mytext),
    STORAGE myt;

-- NB: no need for this: the above statement makes this an unnecessary repetition leading to an error
-- ALTER OPERATOR FAMILY mytextops USING GiST ADD
--     OPERATOR 1 <# (mytext, mytext) ,
--     OPERATOR 3 ==# (mytext, mytext);

--# BEFORE none

--# CHECK
SELECT
    array[n.nspname, o.opcname] as qualname,
    array[npo.nspname, ofam.opfname] AS opcfamily,
    array[npt1.nspname, t1.typname] AS opcintype,
    array[npt2.nspname, t1.typname] AS opckeytype,
    o.opcdefault,
    am.amname,
    ops.ops,
    procs.procs,
    pg_catalog.obj_description(o.oid, 'pg_opclass') AS objcomment,
    (
        SELECT array[n.nspname, t.typname]
        FROM pg_type t
        JOIN pg_catalog.pg_namespace n ON n.oid = t.typnamespace
        WHERE t.oid = o.opckeytype
    ) AS opcstorage,
    pg_catalog.pg_get_userbyid(o.opcowner) AS rolname
FROM pg_catalog.pg_opclass o
JOIN pg_catalog.pg_namespace n ON n.oid = o.opcnamespace
LEFT JOIN pg_catalog.pg_am am on am.oid = o.opcmethod
LEFT JOIN pg_catalog.pg_opfamily ofam ON ofam.oid = o.opcfamily
LEFT JOIN pg_catalog.pg_namespace npo ON npo.oid = ofam.opfnamespace
JOIN pg_catalog.pg_type t1 ON t1.oid = o.opcintype
JOIN pg_catalog.pg_namespace npt1 ON npt1.oid = t1.typnamespace
LEFT JOIN pg_catalog.pg_type t2 ON t2.oid = o.opckeytype
LEFT JOIN pg_catalog.pg_namespace npt2 ON npt2.oid = t2.typnamespace
LEFT JOIN LATERAL (
    SELECT array_agg(json_build_object(
        'oprname', np.nspname || '.' || op.oprname,
        'amopstrategy', a.amopstrategy,
        'lefttype', npt1.nspname || '.' || t1.typname,
        'righttype', npt2.nspname || '.' || t2.typname,
        'amoppurpose', a.amoppurpose,
        'amopsortfamily', (
            SELECT name.nspname || '.' || ofam.opfname
            FROM pg_catalog.pg_opfamily ofam
            JOIN pg_catalog.pg_namespace name ON name.oid = ofam.opfnamespace
            WHERE ofam.oid = a.amopsortfamily
        )
    ) ORDER BY np.nspname, op.oprname) AS ops
    FROM pg_catalog.pg_amop a
    JOIN pg_catalog.pg_operator op ON op.oid = a.amopopr
    JOIN pg_catalog.pg_namespace np ON op.oprnamespace = np.oid
    LEFT JOIN pg_catalog.pg_type t1 ON t1.oid = a.amoplefttype
    LEFT JOIN pg_catalog.pg_namespace npt1 ON npt1.oid = t1.typnamespace
    LEFT JOIN pg_catalog.pg_type t2 ON t2.oid = a.amoprighttype
    LEFT JOIN pg_catalog.pg_namespace npt2 ON npt2.oid = t2.typnamespace
    WHERE a.amopmethod = o.opcmethod AND a.amoplefttype = o.opcintype AND a.amopfamily = o.opcfamily
    ) AS ops ON TRUE
LEFT JOIN LATERAL (
    SELECT array_agg(
        json_build_object(
            'proname', array[np.nspname, f.proname],
            'proargs', pg_catalog.pg_get_function_identity_arguments(f.oid),
            'lefttype', npt1.nspname || '.' || t1.typname,
            'righttype', npt2.nspname || '.' || t2.typname,
            'amprocnum', p.amprocnum,
            'amprocfamily', (
                SELECT name.nspname || '.' || ofam.opfname
                FROM pg_catalog.pg_opfamily ofam
                JOIN pg_catalog.pg_namespace name ON name.oid = ofam.opfnamespace
                WHERE ofam.oid = p.amprocfamily
            )
    ) ORDER BY np.nspname, f.proname) AS procs
    FROM pg_catalog.pg_amproc p
    JOIN pg_catalog.pg_proc f ON f.oid = p.amproc
    JOIN pg_catalog.pg_namespace np ON f.pronamespace = np.oid
    LEFT JOIN pg_catalog.pg_type t1 ON t1.oid = p.amproclefttype
    LEFT JOIN pg_catalog.pg_namespace npt1 ON npt1.oid = t1.typnamespace
    LEFT JOIN pg_catalog.pg_type t2 ON t2.oid = p.amprocrighttype
    LEFT JOIN pg_catalog.pg_namespace npt2 ON npt2.oid = t2.typnamespace
    WHERE p.amproclefttype = o.opcintype AND p.amprocfamily = o.opcfamily
) AS procs ON TRUE
WHERE n.nspname != 'information_schema' AND n.nspname !~ '^pg_'
ORDER BY 1;
