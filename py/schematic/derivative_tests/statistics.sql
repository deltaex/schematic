--# AFTER
CREATE TABLE t1 (
    a   int,
    b   int
);
CREATE STATISTICS sx1 (dependencies) ON a, b FROM t1;

CREATE ROLE me;
CREATE TABLE t2 (
    a   int,
    b   int
);
CREATE STATISTICS sx2 (mcv) ON a, b FROM t2;
ALTER STATISTICS sx2 OWNER TO me;
ALTER STATISTICS sx2 SET STATISTICS 5;

CREATE TABLE t3 (
    a   timestamp
);
CREATE STATISTICS sx3 (ndistinct) ON date_trunc('month', a), date_trunc('day', a) FROM t3;


CREATE STATISTICS sx4 (ndistinct, dependencies, mcv) ON a, (a + 7) FROM t2;


--# BEFORE
CREATE ROLE me;

CREATE TABLE t1 (
    a   int,
    b   int
);

CREATE TABLE t2 (
    a   int,
    b   int
);
CREATE STATISTICS sx2 (mcv) ON a, b FROM t2;

CREATE TABLE t3 (
    a   timestamp
);


--# CHECK
SELECT
    array[n.nspname, s.stxname] as qualname,
    pg_temp.safe_catalog_column('pg_statistic_ext', 'stxstattarget', 'oid = ' || s.oid, null)::int AS stxstattarget,
    s.stxkind,
    pg_catalog.pg_get_expr(pg_temp.safe_catalog_column_pg_node_tree('pg_statistic_ext', 'stxexprs', 'oid = ' || s.oid, null), s.stxrelid) AS stxexprs,
    (
        SELECT array[n.nspname, c.relname]
        FROM pg_catalog.pg_class c
        JOIN pg_catalog.pg_namespace n ON n.oid = c.relnamespace
        WHERE c.oid = s.stxrelid
    ) AS tablequal,
    (
        SELECT array_agg(a.attname ORDER BY a.attname)
        FROM pg_attribute a
        WHERE a.attrelid = s.stxrelid AND a.attnum = any(s.stxkeys)
    ) AS cols,
    pg_catalog.pg_get_userbyid(s.stxowner) AS rolname,
    pg_catalog.obj_description(s.oid, 'pg_statistic_ext') AS objcomment
FROM pg_catalog.pg_statistic_ext s
JOIN pg_catalog.pg_namespace n ON n.oid = s.stxnamespace
WHERE n.nspname !~ '^pg_'
ORDER BY 1;
