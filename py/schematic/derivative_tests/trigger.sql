--# AFTER
CREATE EXTENSION pg_stat_statements;
CREATE EXTENSION intarray;
CREATE TABLE public.bb ( id bigint NOT NULL, name text, cc_id bigint );
CREATE TABLE public.cc ( id bigint NOT NULL, num integer );

CREATE OR REPLACE FUNCTION fn_notify()
RETURNS TRIGGER AS $$
BEGIN
    RAISE NOTICE 'Inserted row: name = %', NEW.name;
    RETURN NEW;
END;
$$ LANGUAGE plpgsql;

CREATE TRIGGER trg_notify_insert
BEFORE INSERT ON public.bb FOR EACH ROW EXECUTE FUNCTION fn_notify();

ALTER TRIGGER trg_notify_insert ON public.bb DEPENDS ON EXTENSION pg_stat_statements;

CREATE OR REPLACE FUNCTION fn_update_cc() RETURNS TRIGGER AS $$
BEGIN
    UPDATE public.cc SET cc.num = cc.num + 1 WHERE id = NEW.cc_id;
    RETURN NEW;
END; $$ LANGUAGE plpgsql;

CREATE TRIGGER trg_update_cc
AFTER INSERT ON public.bb FOR EACH ROW EXECUTE FUNCTION fn_update_cc();

CREATE TRIGGER trg_check_insert_or_update
AFTER INSERT OR UPDATE ON public.bb FOR EACH ROW EXECUTE FUNCTION fn_notify();

CREATE TRIGGER trg_check_for_each_statement
AFTER INSERT OR UPDATE ON public.bb FOR EACH STATEMENT EXECUTE FUNCTION fn_notify();

CREATE TRIGGER trg_check_when
AFTER INSERT OR UPDATE ON public.bb FOR EACH ROW WHEN (NEW.name = 'testing') EXECUTE FUNCTION fn_notify();
COMMENT ON TRIGGER trg_check_when ON public.bb IS 'hi';

-- `instead of` triggers cannot be associated with tables
CREATE VIEW bb_view AS
SELECT name
FROM public.bb
WHERE bb.name IS NOT NULL;

CREATE TRIGGER trg_check_instead_of_on_view
INSTEAD OF INSERT ON public.bb_view FOR EACH ROW EXECUTE FUNCTION fn_notify();

--# BEFORE
CREATE EXTENSION pg_stat_statements;
CREATE EXTENSION intarray;
CREATE TABLE public.bb ( id bigint NOT NULL, name text, cc_id bigint );
CREATE TABLE public.cc ( id bigint NOT NULL, num integer );

CREATE OR REPLACE FUNCTION fn_notify() RETURNS TRIGGER AS $$
BEGIN
    RAISE NOTICE 'Inserted row: name = %', NEW.name;
    RETURN NEW;
END; $$ LANGUAGE plpgsql;

CREATE TRIGGER trg_notify_insert
BEFORE INSERT ON public.bb FOR EACH ROW EXECUTE FUNCTION fn_notify();

ALTER TRIGGER trg_notify_insert ON public.bb DEPENDS ON EXTENSION intarray;

CREATE OR REPLACE FUNCTION fn_update_cc() RETURNS TRIGGER AS $$
BEGIN
    UPDATE public.cc SET cc.num = cc.num + 1 WHERE id = NEW.cc_id;
    RETURN NEW;
END; $$ LANGUAGE plpgsql;

CREATE VIEW bb_view AS
SELECT name
FROM public.bb
WHERE bb.name IS NOT NULL;

--# CHECK
SELECT
    array[n.nspname, t.tgname] AS qualname,
    array[n.nspname, c.relname] AS tablename,
    t.tgisinternal,
    t.tgdeferrable,
    t.tginitdeferred,
    t.tgnargs,
    t.tgattr,
    t.tgargs, t.tgoldtable, t.tgnewtable,
    (SELECT array_agg(e.extname) FROM pg_extension e  WHERE e.oid = depends.refobjid) AS extension_names,
    array[n.nspname, f.proname] as triggerfunc_qualname,
    pg_catalog.pg_get_functiondef(f.oid) AS triggerfuncdef,
    pg_catalog.pg_get_triggerdef(t.oid) AS triggerdef,
    pg_catalog.obj_description(t.oid) AS objcomment
FROM pg_catalog.pg_trigger t
JOIN pg_catalog.pg_class c ON c.oid = t.tgrelid
JOIN pg_catalog.pg_namespace n ON n.oid = c.relnamespace
JOIN pg_catalog.pg_proc f ON f.oid = t.tgfoid
LEFT JOIN LATERAL (
    SELECT refobjid FROM pg_catalog.pg_depend d WHERE t.oid = d.objid AND d.refclassid =
    (SELECT oid FROM pg_catalog.pg_class WHERE relname = 'pg_extension')
) AS depends ON true
WHERE n.nspname != 'information_schema' AND n.nspname !~ '^pg_'
AND not t.tgisinternal
ORDER BY 1;
