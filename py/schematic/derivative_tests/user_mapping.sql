--# AFTER
CREATE ROLE bob;
CREATE EXTENSION IF NOT EXISTS postgres_fdw;

CREATE SERVER foo FOREIGN DATA WRAPPER postgres_fdw;
CREATE USER MAPPING FOR bob SERVER foo OPTIONS (user 'bob', password 'secret');

--  check alter
CREATE ROLE bob_2;
CREATE USER MAPPING FOR bob_2 SERVER foo OPTIONS (user 'bob', password 'secret');

--# BEFORE
CREATE EXTENSION IF NOT EXISTS postgres_fdw;
CREATE ROLE bob;
CREATE ROLE bob_2;
CREATE SERVER foo FOREIGN DATA WRAPPER postgres_fdw;

CREATE USER MAPPING FOR bob_2 SERVER foo OPTIONS (user 'me');

--# CHECK
SELECT
    array[
        (SELECT a.rolname FROM pg_catalog.pg_authid a WHERE a.oid = u.umuser)] AS qualname,
    (SELECT s.srvname FROM pg_catalog.pg_foreign_server s WHERE s.oid = u.umserver) AS umserver,
    u.umoptions
FROM pg_catalog.pg_user_mapping u
ORDER BY 1;
