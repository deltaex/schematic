--# AFTER

CREATE EXTENSION IF NOT EXISTS postgres_fdw;

CREATE SERVER foreigndb_fdw FOREIGN DATA WRAPPER postgres_fdw OPTIONS (host '127.0.0.1', port '5432', dbname 'foreigndb');

CREATE ROLE me;

CREATE SERVER foreigndb_fdw2 FOREIGN DATA WRAPPER postgres_fdw OPTIONS (host '127.0.0.1', dbname 'foreigndb');
ALTER SERVER foreigndb_fdw2 OWNER TO me;

CREATE SERVER pfd_server TYPE 'postgres_fdw' FOREIGN DATA WRAPPER postgres_fdw;

--# BEFORE
CREATE EXTENSION IF NOT EXISTS postgres_fdw;

CREATE ROLE me;
CREATE SERVER foreigndb_fdw2 FOREIGN DATA WRAPPER postgres_fdw OPTIONS (host '127.0.0.2', port '5432');


--# CHECK
SELECT
    ARRAY[s.srvname] AS qualname,
    s.srvtype,
    s.srvversion,
    s.srvacl,
    s.srvoptions,
    fdw.fdwname,
    pg_catalog.obj_description(s.oid, 'pg_foreign_server') AS objcomment,
    pg_catalog.pg_get_userbyid(s.srvowner) AS rolname
FROM pg_catalog.pg_foreign_server s
LEFT JOIN pg_catalog. pg_foreign_data_wrapper fdw ON s.srvfdw = fdw.oid 
ORDER BY 1;
