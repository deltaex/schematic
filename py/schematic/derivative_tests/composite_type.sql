--# AFTER
CREATE TYPE t0 AS (a int, b bigint, c text collate "C", d text[], e int[], f timestamp, g text[][], h char(3)[][], i char, j "char");
COMMENT ON TYPE t0 IS 'hi';
COMMENT ON COLUMN t0.a IS 'abc';

--# BEFORE empty

--# BEFORE types
CREATE TYPE t0 AS (a bigint, b smallint, c text collate "C.utf8");

--# CHECK type
SELECT
    array[n.nspname, c.relname] AS qualname, (SELECT x.rolname FROM pg_catalog.pg_authid x WHERE x.oid = t.typowner) AS rolname,
    pg_catalog.obj_description(t.oid) AS objcomment
FROM pg_catalog.pg_class c
JOIN pg_catalog.pg_namespace n ON n.oid = c.relnamespace
LEFT JOIN pg_catalog.pg_type t ON t.oid = c.reltype
WHERE c.relkind = 'c'
AND n.nspname NOT IN ('pg_toast', 'pg_catalog', 'information_schema')
ORDER BY 1;

--# CHECK attributes
SELECT
    array[n.nspname, c.relname, a.attname] AS qualname,
    pg_catalog.format_type(coltype.oid, coltype.typtypmod) AS typname,
    a.attstorage, a.attisdropped, collat.collname,
    a.attacl, a.attoptions,
    pg_catalog.col_description(c.oid, a.attnum) AS objcomment
FROM pg_catalog.pg_class c
JOIN pg_catalog.pg_namespace n ON n.oid = c.relnamespace
LEFT JOIN pg_catalog.pg_type t ON t.oid = c.reltype
JOIN pg_catalog.pg_attribute a ON a.attrelid = c.oid AND a.attnum > 0
LEFT JOIN pg_catalog.pg_type coltype ON coltype.oid = a.atttypid
LEFT JOIN pg_catalog.pg_collation collat ON collat.oid = a.attcollation
WHERE n.nspname != 'information_schema' AND n.nspname !~ '^pg_'
AND c.relkind = 'c'
ORDER BY 1, a.attnum;
