--# AFTER
CREATE ROLE my_role WITH SUPERUSER;
-- GRANT ALL PRIVILEGES ON SCHEMA public TO my_role;

CREATE OR REPLACE FUNCTION notify_any_command()
RETURNS event_trigger
LANGUAGE plpgsql
AS $$
BEGIN
    RAISE NOTICE 'DDL command executed: %', tg_tag;
END;
$$;

CREATE EVENT TRIGGER notif_ddl_start ON ddl_command_start EXECUTE FUNCTION notify_any_command();

CREATE EVENT TRIGGER notif_ddl_end ON ddl_command_end EXECUTE FUNCTION notify_any_command();

ALTER EVENT TRIGGER notif_ddl_end OWNER TO my_role;
ALTER EVENT TRIGGER notif_ddl_end ENABLE REPLICA;
COMMENT ON EVENT TRIGGER notif_ddl_end IS 'changed';

--# BEFORE
CREATE ROLE my_role WITH SUPERUSER;
-- GRANT ALL PRIVILEGES ON SCHEMA public TO my_role;

CREATE OR REPLACE FUNCTION notify_any_command()
RETURNS event_trigger
LANGUAGE plpgsql
AS $$
BEGIN
    RAISE NOTICE 'DDL command executed: %', tg_tag;
END;
$$;

CREATE EVENT TRIGGER notif_ddl_end ON ddl_command_end EXECUTE FUNCTION notify_any_command();

ALTER EVENT TRIGGER notif_ddl_end DISABLE;
COMMENT ON EVENT TRIGGER notif_ddl_end IS 'initial';

--# CHECK
SELECT
    e.evtname AS qualname,
    e.evtevent,
    e.evtenabled,
    array[np.nspname, p.proname] AS funcname,
    p.prokind AS functype,
    r.rolname AS rolname,
    pg_catalog.obj_description(e.oid, 'pg_event_trigger') AS objcomment
FROM pg_catalog.pg_event_trigger e
JOIN pg_catalog.pg_authid r on r.oid = e.evtowner
JOIN pg_catalog.pg_proc p on p.oid = e.evtfoid
JOIN pg_catalog.pg_namespace np on np.oid = p.pronamespace
WHERE np.nspname !~ 'pg_.*'
ORDER BY 1;
