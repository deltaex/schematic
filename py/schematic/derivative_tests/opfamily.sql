--# AFTER
CREATE ROLE me;

CREATE OPERATOR FAMILY mybtreefam USING btree;

CREATE OPERATOR FAMILY mygistfam USING gist;
ALTER OPERATOR FAMILY mygistfam USING gist OWNER TO me;


CREATE TYPE mytext2 AS (a text);

CREATE OR REPLACE FUNCTION function_less_than(mytext2, mytext2)
RETURNS boolean AS
$$
BEGIN
    RETURN $1 :: text < $2 :: text;
END;
$$ LANGUAGE 'plpgsql' IMMUTABLE;

CREATE OR REPLACE FUNCTION function_equals(mytext2, mytext2)
RETURNS boolean AS
$$
BEGIN
    RETURN $1 = $2;
END;
$$ LANGUAGE 'plpgsql' IMMUTABLE;

CREATE OR REPLACE FUNCTION dumb_fn(mytext2, mytext2)
RETURNS integer AS
$$
BEGIN
    RETURN 0;
END;
$$ LANGUAGE 'plpgsql' IMMUTABLE;


CREATE OPERATOR <# (
    LEFTARG = mytext2,
    RIGHTARG = mytext2,
    PROCEDURE = function_less_than
);

CREATE OPERATOR =# (
    LEFTARG = mytext2,
    RIGHTARG = mytext2,
    PROCEDURE = function_equals
);

CREATE OPERATOR FAMILY mytext2ops USING btree;

COMMENT ON OPERATOR FAMILY mytext2ops USING btree IS 'this';

CREATE OPERATOR CLASS mytext2ops
DEFAULT FOR TYPE mytext2 USING btree FAMILY mytext2ops AS
    OPERATOR        1       <#,
    OPERATOR        3       =#,
    FUNCTION        1       dumb_fn(mytext2, mytext2);



-- check ALTER
CREATE OR REPLACE FUNCTION function_less_less(int4, int2)
RETURNS boolean AS
$$
BEGIN
    RETURN $1 < $2;
END;
$$ LANGUAGE 'plpgsql' IMMUTABLE;

CREATE OR REPLACE FUNCTION function_greater_greater(int4, int2)
RETURNS boolean AS
$$
BEGIN
    RETURN $1 > $2;
END;
$$ LANGUAGE 'plpgsql' IMMUTABLE;

CREATE OPERATOR <=== (
    LEFTARG = int4,
    RIGHTARG = int2,
    PROCEDURE = function_less_less
);

CREATE OPERATOR >=== (
    LEFTARG = int4,
    RIGHTARG = int2,
    PROCEDURE = function_greater_greater
);

CREATE OPERATOR FAMILY myintops USING btree;

ALTER OPERATOR FAMILY myintops USING btree ADD
    OPERATOR 1 <=== (int4, int2) ,
    OPERATOR 2 >=== (int4, int2),
    FUNCTION 1 dumb_fn(mytext2, mytext2);

--# BEFORE

CREATE ROLE me;

CREATE OPERATOR FAMILY mygistfam USING gist;

CREATE TYPE mytext2 AS (a text);

CREATE OR REPLACE FUNCTION function_equals(mytext2, mytext2)
RETURNS boolean AS
$$
BEGIN
    RETURN $1 = $2;
END;
$$ LANGUAGE 'plpgsql' IMMUTABLE;

CREATE OR REPLACE FUNCTION function_less_than(mytext2, mytext2)
RETURNS boolean AS
$$
BEGIN
    RETURN $1 :: text < $2 :: text;
END;
$$ LANGUAGE 'plpgsql' IMMUTABLE;

CREATE OR REPLACE FUNCTION dumb_fn(mytext2, mytext2)
RETURNS integer AS
$$
BEGIN
    RETURN 0;
END;
$$ LANGUAGE 'plpgsql' IMMUTABLE;


CREATE OPERATOR <# (
    LEFTARG = mytext2,
    RIGHTARG = mytext2,
    PROCEDURE = function_less_than
);

CREATE OPERATOR =# (
    LEFTARG = mytext2,
    RIGHTARG = mytext2,
    PROCEDURE = function_equals
);

CREATE OPERATOR FAMILY mytext2ops USING btree;

CREATE OPERATOR CLASS mytext2ops
DEFAULT FOR TYPE mytext2 USING btree FAMILY mytext2ops AS
    OPERATOR        3       =#;

COMMENT ON OPERATOR FAMILY mytext2ops USING btree IS 'that';

--# CHECK
SELECT
    ARRAY [n.nspname, o.opfname] as qualname,
    am.amname,
    ops.ops,
    procs.procs,
    pg_catalog.obj_description(o.oid, 'pg_opfamily') AS objcomment,
    pg_catalog.pg_get_userbyid(o.opfowner) AS rolname
FROM pg_catalog.pg_opfamily o
JOIN pg_catalog.pg_namespace n ON n.oid = o.opfnamespace
JOIN pg_catalog.pg_am am ON am.oid = o.opfmethod
LEFT JOIN LATERAL (
    SELECT json_object_agg (
        op.oprname,
        json_build_object(
            'amopstrategy', a.amopstrategy,
            'lefttype', npt1.nspname || '.' || t1.typname,
            'righttype', npt2.nspname || '.' || t2.typname,
            'amoppurpose', a.amoppurpose,
            'amopsortfamily', (
                SELECT name.nspname || '.' || ofam.opfname
                FROM pg_catalog.pg_opfamily ofam
                JOIN pg_catalog.pg_namespace name ON name.oid = ofam.opfnamespace
                WHERE ofam.oid = a.amopsortfamily
            ))
    ORDER BY a.oid) AS ops
    FROM pg_catalog.pg_amop a
    JOIN pg_catalog.pg_operator op ON op.oid = a.amopopr
    LEFT JOIN pg_catalog.pg_type t1 ON t1.oid = a.amoplefttype
    LEFT JOIN pg_catalog.pg_namespace npt1 ON npt1.oid = t1.typnamespace
    LEFT JOIN pg_catalog.pg_type t2 ON t2.oid = a.amoprighttype
    LEFT JOIN pg_catalog.pg_namespace npt2 ON npt2.oid = t2.typnamespace
    WHERE a.amopmethod = o.opfmethod AND a.amopfamily = o.oid
    ) AS ops ON TRUE
LEFT JOIN LATERAL (
    SELECT json_object_agg (
        f.proname,
        json_build_object(
            'proargs', pg_catalog.pg_get_function_identity_arguments(f.oid),
            'lefttype', npt1.nspname || '.' || t1.typname,
            'righttype', npt2.nspname || '.' || t2.typname,
            'amprocnum', p.amprocnum,
            'amprocfamily', (
                SELECT name.nspname || '.' || ofam.opfname
                FROM pg_catalog.pg_opfamily ofam
                JOIN pg_catalog.pg_namespace name ON name.oid = ofam.opfnamespace
                WHERE ofam.oid = p.amprocfamily
            ))
    ORDER BY p.oid) AS procs
    FROM pg_catalog.pg_amproc p
        JOIN pg_catalog.pg_proc f ON f.oid = p.amproc
        LEFT JOIN pg_catalog.pg_type t1 ON t1.oid = p.amproclefttype
        LEFT JOIN pg_catalog.pg_namespace npt1 ON npt1.oid = t1.typnamespace
        LEFT JOIN pg_catalog.pg_type t2 ON t2.oid = p.amprocrighttype
        LEFT JOIN pg_catalog.pg_namespace npt2 ON npt2.oid = t2.typnamespace
        WHERE p.amprocfamily = o.oid
    ) AS procs ON TRUE
WHERE n.nspname !~ 'pg_.*'
ORDER BY 1;
