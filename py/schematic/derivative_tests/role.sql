--# AFTER
CREATE USER myuser;
ALTER USER myuser WITH SUPERUSER CREATEDB INHERIT LOGIN REPLICATION BYPASSRLS CONNECTION LIMIT 9;
ALTER USER myuser WITH PASSWORD '123'; -- not implemented yet
COMMENT ON ROLE myuser IS 'only comment';

CREATE ROLE myrole;
CREATE GROUP mygroup;



CREATE ROLE my_role_alter;
ALTER ROLE my_role_alter WITH NOREPLICATION;
ALTER ROLE my_role_alter WITH CONNECTION LIMIT 4;
ALTER ROLE my_role_alter WITH PASSWORD 'xyz';
ALTER ROLE my_role_alter WITH VALID UNTIL '2023-01-01 12:30:00';
ALTER ROLE my_role_alter SET datestyle TO postgres, dmy;
ALTER ROLE my_role_alter SET search_path TO my_schema, public;
ALTER ROLE my_role_alter IN DATABASE postgres SET search_path TO my_schema, public;
ALTER ROLE my_role_alter IN DATABASE postgres SET TIME ZONE 'pst8pdt';
COMMENT ON ROLE my_role_alter IS 'final comment';

CREATE GROUP my_group_alter;
CREATE GROUP my_other_group_alter;
GRANT my_group_alter TO my_role_alter;


--# BEFORE
CREATE ROLE my_role_alter;
ALTER ROLE my_role_alter WITH SUPERUSER CREATEDB INHERIT LOGIN REPLICATION BYPASSRLS;
ALTER ROLE my_role_alter WITH PASSWORD 'abc';
ALTER ROLE my_role_alter WITH CONNECTION LIMIT 5;
ALTER ROLE my_role_alter WITH VALID UNTIL '2023-01-05 02:45:00';
ALTER ROLE my_role_alter SET TIME ZONE 'PST8PDT';
ALTER ROLE my_role_alter IN DATABASE postgres SET TIME ZONE 'Europe/Rome';
COMMENT ON ROLE my_role_alter IS 'first comment';

CREATE GROUP my_group_alter;
CREATE GROUP my_other_group_alter;
GRANT my_other_group_alter TO my_role_alter;

--# CHECK
SELECT
    ARRAY[r.rolname] AS qualname,
    pg_catalog.obj_description(r.oid, 'pg_authid') AS objcomment,
    json_build_object(
        'SUPERUSER', r.rolsuper,
        'PASSWORD', r.rolpassword,
        'INHERIT', r.rolinherit,
        'CREATEROLE', r.rolcreaterole,
        'CREATEDB', r.rolcreatedb,
        'LOGIN', r.rolcanlogin,
        'REPLICATION', r.rolreplication,
        'BYPASSRLS', r.rolbypassrls,
        'CONNECTION LIMIT', r.rolconnlimit,
        'VALID UNTIL', r.rolvaliduntil
    ) AS options,
    (
        SELECT array_agg(a.rolname ORDER BY a.rolname) FROM pg_catalog.pg_authid a WHERE a.oid = any(members.oids)
    ) AS rolmembers,
    default_db_configs.c AS db_configs,
    default_all_db_configs.c AS all_configs
FROM pg_catalog.pg_authid r
LEFT JOIN LATERAL (
    SELECT
        array_agg(json_build_object(
            d.datname,
            (
                SELECT array_agg(sorted_defaults)
                FROM (SELECT unnest(rs.setconfig) AS sorted_defaults ORDER BY sorted_defaults) AS subquery
            )
    ) ORDER BY d.datname, rs.setconfig) AS c
    FROM pg_catalog.pg_db_role_setting rs
    JOIN pg_catalog.pg_database d ON d.oid = rs.setdatabase
    WHERE rs.setrole = r.oid
) AS default_db_configs ON true
LEFT JOIN LATERAL (
    SELECT
        rs.setconfig AS c
    FROM pg_catalog.pg_db_role_setting rs
    WHERE rs.setrole = r.oid AND rs.setdatabase = 0
) AS default_all_db_configs ON true
LEFT JOIN LATERAL (
    SELECT array_agg(m.member) AS oids from pg_catalog.pg_auth_members m WHERE r.oid = m.roleid
) AS members ON true
WHERE r.rolname !~ 'pg_.*'
ORDER BY 1;
