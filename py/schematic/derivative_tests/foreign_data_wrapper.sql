--# AFTER
CREATE FOREIGN DATA WRAPPER dummy OPTIONS (foo '1', bar '2');

CREATE FOREIGN DATA WRAPPER mywrapper
    OPTIONS (debug 'true');

--# BEFORE
CREATE FOREIGN DATA WRAPPER dummy;

--# CHECK
SELECT
    ARRAY[fdw.fdwname] AS qualname,
    (
        SELECT array[n.nspname, f.proname]
        FROM pg_catalog.pg_proc f
        JOIN pg_catalog.pg_namespace n ON f.pronamespace = n.oid
        WHERE f.oid = fdw.fdwhandler
    ) AS fdwhandler,
    (
        SELECT array[n.nspname, f.proname]
        FROM pg_catalog.pg_proc f
        JOIN pg_catalog.pg_namespace n ON f.pronamespace = n.oid
        WHERE f.oid = fdw.fdwvalidator
    ) AS fdwvalidator,
    fdw.fdwacl,
    fdw.fdwoptions,
    pg_catalog.obj_description(fdw.oid, 'pg_foreign_data_wrapper') AS objcomment,
    pg_catalog.pg_get_userbyid(fdw.fdwowner) AS rolname
FROM pg_catalog.pg_foreign_data_wrapper fdw
ORDER BY 1;
