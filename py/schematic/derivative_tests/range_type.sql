--# AFTER
CREATE TYPE float8_range1 AS RANGE (subtype = float8);

CREATE TYPE float8_range2 AS RANGE (subtype = float8, subtype_diff = float8mi);

CREATE TYPE int4_range2 AS RANGE (subtype = float8, subtype_diff = float8mi);
COMMENT ON TYPE int4_range2 IS 'THIS';

CREATE TYPE mydate AS RANGE ( subtype = date, SUBTYPE_OPCLASS = pg_catalog.date_ops);

CREATE TYPE mytextrange AS RANGE ( subtype = text, COLLATION = 'C' );

CREATE TYPE myint4_with_can;

CREATE OR REPLACE FUNCTION pg_catalog.myint4range_canonical(myint4_with_can)
    RETURNS myint4_with_can
    LANGUAGE internal
    IMMUTABLE PARALLEL SAFE STRICT
AS $function$int4range_canonical$function$;

CREATE TYPE myint4_with_can AS RANGE ( subtype = int4, CANONICAL = pg_catalog.myint4range_canonical );

CREATE TYPE myint4_ AS RANGE ( subtype = int4, MULTIRANGE_TYPE_NAME = myint4_multirange );



--# BEFORE
CREATE TYPE int4_range2 AS RANGE (subtype = float8, subtype_diff = float8mi);
COMMENT ON TYPE int4_range2 IS 'THAT';

CREATE ROLE myrole;
ALTER TYPE int4_range2 OWNER TO myrole;

--# CHECK
SELECT
    array[n.nspname, t.typname] AS qualname,
    pg_catalog.pg_get_userbyid(t.typowner) AS rolname,
    (SELECT array[n.nspname, col.collname] FROM pg_catalog.pg_collation col JOIN pg_catalog.pg_namespace n ON n.oid = col.collnamespace WHERE col.oid = r.rngcollation) AS collation,
    (SELECT array[n.nspname, t.typname] FROM pg_catalog.pg_type t JOIN pg_catalog.pg_namespace n ON n.oid = t.typnamespace WHERE t.oid = r.rngsubtype) AS subtype,
    (SELECT array[n.nspname, x.typname] FROM pg_catalog.pg_type x JOIN pg_catalog.pg_namespace n ON n.oid = t.typnamespace WHERE x.oid = (pg_temp.safe_catalog_column('pg_range', 'rngmultitypid', 'rngtypid = ' || t.oid::text, NULL))::oid) AS multirange_type_name,
    (SELECT array[n.nspname, oc.opcname] FROM pg_catalog.pg_opclass oc JOIN pg_catalog.pg_namespace n ON oc.opcnamespace = n.oid WHERE oc.oid = r.rngsubopc) AS subtype_operator_class,
    (SELECT
        json_build_object(
            'name', array[n.nspname, p.proname],
            'def', pg_catalog.pg_get_functiondef(p.oid)
        ) FROM pg_catalog.pg_proc p JOIN pg_catalog.pg_namespace n ON p.pronamespace = n.oid WHERE p.oid = r.rngcanonical) AS canonical_function,
    (SELECT array[n.nspname, p.proname] FROM pg_catalog.pg_proc p JOIN pg_catalog.pg_namespace n ON p.pronamespace = n.oid WHERE p.oid = r.rngsubdiff) AS subtype_diff_function,
    pg_catalog.obj_description(t.oid, 'pg_type') AS objcomment
FROM pg_catalog.pg_type t
JOIN pg_catalog.pg_range r ON r.rngtypid = t.oid
JOIN pg_catalog.pg_namespace n ON n.oid = t.typnamespace
WHERE n.nspname != 'information_schema' AND n.nspname !~ '^pg_'
AND t.typtype = 'r'
ORDER BY 1;
