--# AFTER
CREATE CONVERSION myconv FOR 'MULE_INTERNAL' TO 'KOI8R' FROM pg_catalog.mic_to_koi8r;

CREATE CONVERSION second_conv FOR 'BIG5' TO 'MULE_INTERNAL' FROM pg_catalog.big5_to_mic;

--# BEFORE
CREATE ROLE joe;

CREATE CONVERSION second_conv FOR 'BIG5' TO 'MULE_INTERNAL' FROM pg_catalog.big5_to_mic;

ALTER CONVERSION second_conv OWNER TO joe;


--# CHECK
SELECT
    ARRAY [n.nspname, c.conname] as qualname,
    c.condefault,
    pg_catalog.pg_encoding_to_char(c.conforencoding) AS source_encoding,
    pg_catalog.pg_encoding_to_char(c.contoencoding) AS dest_encoding,
    ARRAY[np.nspname, p.proname] AS function_name,
    pg_catalog.pg_get_userbyid(c.conowner) AS rolname,
    pg_catalog.obj_description(c.oid, 'pg_conversion') AS objcomment
FROM pg_catalog.pg_conversion c
JOIN pg_catalog.pg_namespace n ON n.oid = c.connamespace
LEFT JOIN pg_catalog.pg_proc p ON p.oid = c.conproc
LEFT JOIN pg_catalog.pg_namespace np ON np.oid = p.pronamespace
WHERE n.nspname !~ 'pg_.*'
ORDER BY 1;
