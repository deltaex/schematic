--# AFTER
CREATE TYPE enum0 AS ENUM ('enum1', 'enum2', 'enum3');
COMMENT ON TYPE enum0 IS 'hi';

--# BEFORE empty

--# BEFORE missing end
CREATE TYPE enum0 AS ENUM ('enum1', 'enum2');

--# BEFORE missing middle
CREATE TYPE enum0 AS ENUM ('enum1', 'enum3');

--# BEFORE missing start
CREATE TYPE enum0 AS ENUM ('enum2', 'enum3');

--# CHECK
SELECT
    array[n.nspname, t.typname] AS qualname, (SELECT x.rolname FROM pg_catalog.pg_authid x WHERE x.oid = t.typowner) AS rolname,
    (SELECT array_agg(e.enumlabel ORDER BY enumsortorder) FROM pg_catalog.pg_enum e WHERE e.enumtypid = t.oid) AS labels,
    pg_catalog.obj_description(t.oid) AS objcomment
FROM pg_catalog.pg_type t
JOIN pg_catalog.pg_namespace n ON n.oid = t.typnamespace
WHERE n.nspname != 'information_schema' AND n.nspname !~ '^pg_'
AND t.typtype = 'e'
ORDER BY 1;
