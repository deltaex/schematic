--# AFTER domain constraint function
CREATE OR REPLACE FUNCTION func01(x bytea) RETURNS boolean
LANGUAGE sql IMMUTABLE STRICT LEAKPROOF PARALLEL SAFE AS $_$
    SELECT true;
$_$;

CREATE DOMAIN domain01 AS bytea CONSTRAINT chk01 CHECK (func01(VALUE));

--# AFTER column generated function
CREATE OR REPLACE FUNCTION gen01(integer)
RETURNS integer LANGUAGE sql IMMUTABLE AS $function$
    SELECT 123;
$function$;
CREATE OR REPLACE FUNCTION gen02(integer)
RETURNS integer LANGUAGE sql IMMUTABLE AS $function$
    SELECT 123;
$function$;

CREATE TABLE tbl01 (
    col1 integer PRIMARY KEY,
    col2 integer GENERATED ALWAYS AS (gen01(col1)) STORED NOT NULL,
    col3 integer GENERATED ALWAYS AS (gen01(gen02(col1))) STORED NOT NULL
);

--# AFTER table enum

CREATE TYPE enum01 AS ENUM ('val01' );

CREATE TABLE tbl01 (
    col1 int NOT NULL,
    col2 enum01 NOT NULL
);

--# AFTER table depends on composite type
CREATE TYPE composite01 AS ( foo text, bar text );

CREATE TABLE tbl01 (
    col1 integer NOT NULL,
    col2 composite01[]
);

--# AFTER composite type depends on table

CREATE TABLE tbl01 (
    foo text,
    bar text
);

CREATE TYPE composite01 AS ( col1 tbl01[]  );

--# AFTER function depends on view

CREATE OR REPLACE VIEW view01 AS SELECT 1::integer AS col1;

CREATE OR REPLACE FUNCTION func01() RETURNS view01
LANGUAGE sql STRICT AS $_$
    SELECT * FROM view01;
$_$;

--# AFTER operator constraint

CREATE OR REPLACE FUNCTION func01(var1 anyelement, var2 anyelement) RETURNS bool
LANGUAGE sql CALLED ON NULL INPUT IMMUTABLE PARALLEL SAFE LEAKPROOF AS $function$
    SELECT $1 IS NOT DISTINCT FROM $2;
$function$;

CREATE OPERATOR == (
    LEFTARG = anyelement,
    RIGHTARG = anyelement,
    FUNCTION = func01,
    COMMUTATOR = OPERATOR(==)
);

CREATE OR REPLACE FUNCTION func01(var1 text, var2 text) RETURNS bool
LANGUAGE sql CALLED ON NULL INPUT IMMUTABLE PARALLEL SAFE LEAKPROOF AS $function$
    SELECT $1 IS NOT DISTINCT FROM $2;
$function$;

CREATE OPERATOR == (
    LEFTARG = text,
    RIGHTARG = text,
    FUNCTION = func01,
    COMMUTATOR = OPERATOR(==)
);
CREATE TABLE tbl01 (
    col1 integer,
    col2 integer[]
);
ALTER TABLE tbl01 ADD CONSTRAINT chk_tbl01_col2_sorted CHECK (col2 == col2);



--# BEFORE empty

--# CHECK
SELECT 1; -- we're just testing to see if it gets the dependency order right and there's no error
