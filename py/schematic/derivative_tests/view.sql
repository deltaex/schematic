--# AFTER
CREATE ROLE test_role;
GRANT ALL PRIVILEGES ON SCHEMA public TO test_role;
GRANT ALL PRIVILEGES ON pg_catalog.pg_authid TO test_role;

CREATE TABLE public.bar (
    id integer,
    name text,
    description text
);

CREATE VIEW public.test_view AS
SELECT id, name, description
FROM public.bar;

CREATE VIEW public.test_view_replace AS
SELECT id, name, description
FROM public.bar;

CREATE VIEW public.test_view_alter AS
SELECT name
FROM public.bar;

ALTER VIEW public.test_view_alter ALTER COLUMN name SET DEFAULT 'testing';
ALTER VIEW public.test_view_alter SET ( check_option = CASCADED, security_barrier = true );
ALTER VIEW public.test_view_alter OWNER TO test_role;
COMMENT ON VIEW public.test_view_alter IS 'hi';

--# BEFORE
CREATE ROLE test_role;
GRANT ALL PRIVILEGES ON SCHEMA public TO test_role;
GRANT ALL PRIVILEGES ON pg_catalog.pg_authid TO test_role;

CREATE TABLE public.bar (
    id integer,
    name text,
    description text
);

CREATE VIEW public.test_view_replace AS
SELECT id, name
FROM public.bar;

CREATE VIEW public.test_view_alter AS
SELECT id, name
FROM public.bar;

ALTER VIEW public.test_view_alter ALTER COLUMN id SET DEFAULT 7;
ALTER VIEW public.test_view_alter SET ( check_option = LOCAL, security_invoker = True);

--# CHECK
SELECT
    array[n.nspname, c.relname] AS qualname,
    c.relpersistence,
    c.reloptions,
    pg_catalog.pg_get_viewdef(c.oid) AS viewdef,
    pg_catalog.pg_get_userbyid(c.relowner) AS rolname,
    columns.viewcolumns,
    pg_catalog.obj_description(c.oid) AS objcomment
FROM pg_catalog.pg_class c
JOIN pg_catalog.pg_namespace n ON n.oid = c.relnamespace
LEFT JOIN LATERAL (
    SELECT
        array_agg(array[a.attname,
            pg_catalog.format_type(columntype.oid, columntype.typtypmod),
            a.atthasdef::text,
            pg_catalog.pg_get_expr(attrdef.adbin, attrdef.adrelid
        )] ORDER BY a.attnum ASC ) AS viewcolumns
    FROM pg_catalog.pg_attribute a
    JOIN pg_catalog.pg_type columntype ON columntype.oid = a.atttypid
    LEFT JOIN pg_catalog.pg_attrdef attrdef ON attrdef.adrelid = a.attrelid AND attrdef.adnum = a.attnum
    WHERE a.attrelid = c.oid
) AS columns ON true
WHERE n.nspname != 'information_schema' AND n.nspname !~ '^pg_'
AND c.relkind = 'v'
AND NOT c.relhasrules
ORDER BY 1;
