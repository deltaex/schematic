--# AFTER

CREATE OR REPLACE FUNCTION concat_with_delimiter (state text, value text, delimiter text)
RETURNS text LANGUAGE plpgsql AS $$
BEGIN
  IF state IS NULL THEN
    RETURN value;
  END IF;
  RETURN state || delimiter || value;
END;
$$;

CREATE AGGREGATE text_concat2 ( text, text ORDER BY foo123 text, bar234 text) (
  sfunc = concat_with_delimiter,
  stype = text,
  initcond = NULL
);

CREATE AGGREGATE user_avg (float8)
(
    sfunc = float8_accum,
    stype = float8[],
    finalfunc = float8_avg,
    initcond = '{0,0,0}'
);

COMMENT ON AGGREGATE user_avg(float8) IS 'comment here';

CREATE AGGREGATE user_unsafe_sum (float8)
(
    stype = float8,
    sfunc = float8pl,
    mstype = float8,
    msfunc = float8pl,
    minvfunc = float8mi,
    initcond = '0'
);

COMMENT ON AGGREGATE user_unsafe_sum(float8) IS NULL;

--# BEFORE
CREATE ROLE my_agg_role;

CREATE AGGREGATE user_avg (float8)
(
    sfunc = float8_accum,
    stype = float8[],
    finalfunc = float8_avg,
    initcond = '{0,0,0}'
);

ALTER AGGREGATE user_avg (float8) OWNER TO my_agg_role; 

COMMENT ON FUNCTION user_avg IS 'no comment';

--# CHECK
SELECT
    ARRAY[np.nspname, pr.proname] AS qualname,
    pg_catalog.pg_get_function_identity_arguments(pr.oid) AS argtype,
    au.rolname AS owner,
    a.aggkind,
    a.aggnumdirectargs,
    pg_catalog.obj_description(pr.oid) AS objcomment,
    pr.proparallel,
    (SELECT ARRAY[n.nspname, p.proname] FROM pg_catalog.pg_proc p JOIN pg_catalog.pg_namespace n on p.pronamespace = n.oid WHERE p.oid = a.aggtransfn) AS aggtransfn,
    (SELECT ARRAY[n.nspname, t.typname] FROM pg_catalog.pg_type t JOIN pg_catalog.pg_namespace n ON t.typnamespace = n.oid WHERE t.oid = a.aggtranstype) AS aggtranstype,
    json_build_object(
        'INITCOND', a.agginitval,
        'MINITCOND', a.aggminitval,
        'FINALFUNC_EXTRA', a.aggfinalextra,
        'MFINALFUNC_EXTRA', a.aggmfinalextra,
        'FINALFUNC_MODIFY', a.aggfinalmodify,
        'MFINALFUNC_MODIFY', a.aggmfinalmodify,
        'SSPACE', a.aggtransspace,
        'MSSPACE', a.aggmtransspace,
        'FINALFUNC', (SELECT ARRAY[n.nspname, p.proname] FROM pg_catalog.pg_proc p JOIN pg_catalog.pg_namespace n on p.pronamespace = n.oid WHERE p.oid = a.aggfinalfn),
        'COMBINEFUNC', (SELECT ARRAY[n.nspname, p.proname] FROM pg_catalog.pg_proc p JOIN pg_catalog.pg_namespace n on p.pronamespace = n.oid WHERE p.oid = a.aggcombinefn),
        'SERIALFUNC', (SELECT ARRAY[n.nspname, p.proname] FROM pg_catalog.pg_proc p JOIN pg_catalog.pg_namespace n on p.pronamespace = n.oid WHERE p.oid = a.aggserialfn),
        'DESERIALFUNC', (SELECT ARRAY[n.nspname, p.proname] FROM pg_catalog.pg_proc p JOIN pg_catalog.pg_namespace n on p.pronamespace = n.oid WHERE p.oid = a.aggdeserialfn),
        'MSFUNC', (SELECT ARRAY[n.nspname, p.proname] FROM pg_catalog.pg_proc p JOIN pg_catalog.pg_namespace n on p.pronamespace = n.oid WHERE p.oid = a.aggmtransfn),
        'MINVFUNC', (SELECT ARRAY[n.nspname, p.proname] FROM pg_catalog.pg_proc p JOIN pg_catalog.pg_namespace n on p.pronamespace = n.oid WHERE p.oid = a.aggminvtransfn),
        'MFINALFUNC', (SELECT ARRAY[n.nspname, p.proname] FROM pg_catalog.pg_proc p JOIN pg_catalog.pg_namespace n on p.pronamespace = n.oid WHERE p.oid = a.aggmfinalfn),
        'SORTOP', (SELECT ARRAY[n.nspname, o.oprname] FROM pg_catalog.pg_operator o LEFT JOIN pg_catalog.pg_namespace n ON o.oprnamespace = n.oid WHERE o.oid = a.aggsortop),
        'MSTYPE', (SELECT ARRAY[n.nspname, t.typname] FROM pg_catalog.pg_type t JOIN pg_catalog.pg_namespace n ON t.typnamespace = n.oid WHERE t.oid = a.aggmtranstype)
    ) AS opts
FROM pg_catalog.pg_aggregate a
JOIN pg_catalog.pg_proc pr ON a.aggfnoid = pr.oid
JOIN pg_catalog.pg_namespace np ON pr.pronamespace = np.oid AND np.nspname !~ 'pg_.*'
JOIN pg_catalog.pg_authid au ON pr.proowner = au.oid
LEFT JOIN pg_catalog.pg_operator op ON op.oid = a.aggsortop
ORDER BY 1;
