--# AFTER
CREATE ROLE myrole;

CREATE TEXT SEARCH DICTIONARY my_dict (
    template = snowball,
    language = russian,
    stopwords = 'french'
);

CREATE TEXT SEARCH DICTIONARY my_dict2 (
    template = snowball,
    language = 'finnish',
    stopwords = 'french'
);
COMMENT ON TEXT SEARCH DICTIONARY my_dict2 IS 'THIS';
ALTER TEXT SEARCH DICTIONARY my_dict2 OWNER TO myrole;

--# BEFORE
CREATE ROLE myrole;
CREATE TEXT SEARCH DICTIONARY my_dict2 (
    template = snowball,
    language = 'romanian',
    stopwords = 'danish'
);

COMMENT ON TEXT SEARCH DICTIONARY my_dict2 IS 'THAT';

--# CHECK
SELECT
    ARRAY[n.nspname, d.dictname] AS qualname,
    d.dictinitoption,
    (SELECT rolname FROM pg_catalog.pg_authid a WHERE a.oid = d.dictowner) AS rolname,
    (
        SELECT ARRAY[np.nspname, t.tmplname]
        FROM pg_catalog.pg_ts_template t
        JOIN pg_catalog.pg_namespace np ON np.oid = t.tmplnamespace
        WHERE d.dicttemplate = t.oid
    ) AS tmplname,
    pg_catalog.obj_description(d.oid, 'pg_ts_dict') AS objcomment
FROM pg_catalog.pg_ts_dict d
JOIN pg_catalog.pg_namespace n ON n.oid = d.dictnamespace
WHERE n.nspname !~ '^pg_'
ORDER BY 1;
