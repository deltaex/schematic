--# AFTER
CREATE TABLE public.t1 ();

CREATE POLICY pol ON public.t1
    AS RESTRICTIVE
    FOR ALL
    TO root
    USING (1 = 1);


CREATE POLICY pol2 ON public.t1
    AS RESTRICTIVE
    FOR ALL
    TO root
    USING (1 <> 1)
    WITH CHECK (1 = 1);

--# BEFORE
CREATE TABLE public.t1 ();
CREATE ROLE me;

CREATE POLICY pol2 ON public.t1
    AS RESTRICTIVE
    FOR ALL
    TO me
    USING (1 <> 1)
    WITH CHECK (0 = 1);

--# CHECK
SELECT
    array[p.polname] AS qualname,
    (SELECT array[n.nspname, c.relname] FROM pg_catalog.pg_class c JOIN pg_catalog.pg_namespace n ON n.oid = c.relnamespace WHERE c.oid = p.polrelid) AS tablename,
    p.polcmd,
    p.polpermissive,
    (SELECT array_agg(a.rolname) FROM pg_catalog.pg_authid a WHERE a.oid = any(p.polroles)) AS polroles,
    pg_catalog.pg_get_expr( p.polqual, p.oid ) AS polqual,
    pg_catalog.pg_get_expr( p.polwithcheck, p.oid ) AS polwithcheck,
    pg_catalog.obj_description(p.oid, 'pg_policy') AS objcomment
FROM pg_catalog.pg_policy p
ORDER BY 1;
