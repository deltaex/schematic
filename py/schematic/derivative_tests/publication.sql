--# AFTER

CREATE ROLE pubrole;
GRANT ALL PRIVILEGES ON SCHEMA public TO pubrole;

CREATE TABLE table1(
    a text
);
CREATE TABLE table2 (
    b text
);
CREATE TABLE table3 (
    a int,
    b int,
    active boolean
);
CREATE TABLE table4 (
    a text
);

CREATE PUBLICATION pub1 FOR TABLE table1, table2;

CREATE PUBLICATION pub2 FOR TABLE table3 WHERE (active IS TRUE);

CREATE PUBLICATION pub3 FOR TABLE table4
    WITH (publish = 'insert');

CREATE PUBLICATION pub4 FOR TABLE  table1, table2 WHERE (length(b) > 5), table3;
COMMENT ON PUBLICATION pub4 IS 'some';
ALTER PUBLICATION pub4 OWNER TO pubrole;

CREATE PUBLICATION pub5 FOR TABLE table1;
ALTER PUBLICATION pub5 SET(publish = 'insert, delete');

CREATE PUBLICATION pub6 FOR ALL TABLES;

CREATE PUBLICATION pub7 FOR TABLES IN SCHEMA public;

CREATE PUBLICATION pub8 FOR TABLE table1, table2, TABLES IN SCHEMA public;

CREATE PUBLICATION pub9 FOR TABLE table3(a);
--# BEFORE

CREATE ROLE pubrole;
GRANT ALL PRIVILEGES ON SCHEMA public TO pubrole;

CREATE TABLE table1(
    a text
);
CREATE TABLE table2 (
    b text
);
CREATE TABLE table3 (
    a int,
    b int,
    active boolean
);
CREATE TABLE table4 (
    a text
);

CREATE PUBLICATION pub9 FOR TABLE table3(b);

CREATE PUBLICATION pub4 FOR TABLE table4;
COMMENT ON PUBLICATION pub4 IS 'none';

CREATE PUBLICATION pub5 FOR TABLE table1;
--# CHECK
SELECT
    ARRAY[p.pubname] AS qualname,
    a.rolname AS owner,
    p.puballtables,
    p.pubinsert,
    p.pubupdate,
    p.pubdelete,
    p.pubtruncate,
    (
        SELECT array_agg(n.nspname ORDER BY n.nspname) FROM pg_catalog.pg_namespace n where n.oid = ANY (
            pg_temp.safe_catalog_column_multirow(
                'pg_publication_namespace', 'pnnspid', 'pnpubid = ' || p.oid, '{}')::oid[]
        )
    ) AS schemas,
    pg_temp.safe_catalog_column(
        'pg_publication', 'pubviaroot', 'pg_publication.oid = ' || p.oid, 'false') AS pubviaroot,
    tables.tables,
    pg_catalog.obj_description(p.oid, 'pg_publication') AS objcomment
FROM pg_catalog.pg_publication p
JOIN pg_catalog.pg_authid a ON p.pubowner = a.oid
LEFT JOIN LATERAL (
    SELECT
        json_agg(
            json_build_object(
                'name', n.nspname || '.' || c.relname,
                'qual', pg_catalog.pg_get_expr(pg_temp.safe_catalog_column_pg_node_tree('pg_publication_rel', 'prqual', 'oid = ' || r.oid, null), r.prrelid),
                'cols', (
                    SELECT array_agg(attname ORDER BY attname)
                    FROM pg_catalog.pg_attribute a
                    WHERE a.attrelid = r.prrelid
                    AND a.attnum = ANY(pg_temp.safe_get_int2vector('pg_publication_rel', 'prattrs', 'oid = ' || r.oid)))
        )) AS tables
    FROM pg_catalog.pg_publication_rel r
    JOIN pg_catalog.pg_class c ON c.oid = r.prrelid
    JOIN pg_catalog.pg_namespace n ON n.oid = c.relnamespace
    WHERE r.prpubid = p.oid
) AS tables ON true
ORDER BY 1;
