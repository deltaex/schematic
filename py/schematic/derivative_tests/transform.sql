# AFTER
CREATE EXTENSION plpython3u;

CREATE FUNCTION boolean_to_plpython3u (val internal) RETURNS internal
LANGUAGE internal STRICT IMMUTABLE
AS $function$boolout$function$;

CREATE FUNCTION plpython3u_to_boolean (val internal) RETURNS boolean
LANGUAGE internal STRICT IMMUTABLE
AS $function$boolout$function$;

CREATE TRANSFORM FOR boolean LANGUAGE plpython3u (
    FROM SQL WITH FUNCTION boolean_to_plpython3u(internal),
    TO SQL WITH FUNCTION plpython3u_to_boolean(internal)
);


CREATE FUNCTION boolean_to_plpgsql (val internal) RETURNS internal
LANGUAGE internal STRICT IMMUTABLE
AS $function$boolout$function$;

CREATE FUNCTION plpgsql_to_boolean (val internal) RETURNS boolean
LANGUAGE internal STRICT IMMUTABLE
AS $function$boolout$function$;

CREATE TRANSFORM FOR boolean LANGUAGE plpgsql (
    FROM SQL WITH FUNCTION boolean_to_plpgsql(internal),
    TO SQL WITH FUNCTION plpgsql_to_boolean(internal)
);
COMMENT ON TRANSFORM FOR boolean LANGUAGE plpgsql IS 'hi';

# BEFORE
CREATE FUNCTION boolean_to_plpgsql (val internal) RETURNS internal
LANGUAGE internal STRICT IMMUTABLE
AS $function$boolout$function$;

CREATE FUNCTION plpgsql_to_boolean (val internal) RETURNS boolean
LANGUAGE internal STRICT IMMUTABLE
AS $function$boolout$function$;

CREATE TRANSFORM FOR boolean LANGUAGE plpgsql (
    FROM SQL WITH FUNCTION boolean_to_plpgsql(internal),
    TO SQL WITH FUNCTION plpgsql_to_boolean(internal)
);

COMMENT ON TRANSFORM FOR boolean LANGUAGE plpgsql IS 'hey';


--# CHECK
SELECT
    array[(
        SELECT
            pg_catalog.format_type(ty.oid, ty.typtypmod)
        FROM pg_catalog.pg_type ty
        WHERE ty.oid = t.trftype
    ), lanname.lanname] AS qualname,
    (
        SELECT json_build_object(
            'name', n.nspname || '.' || p.proname,
            'args', pg_catalog.pg_get_function_identity_arguments(p.oid)
            )
        FROM pg_catalog.pg_proc p
        JOIN pg_catalog.pg_namespace n ON n.oid = p.pronamespace
        WHERE p.oid = t.trffromsql
    ) AS from_sql_func,
    (
        SELECT json_build_object(
            'name', n.nspname || '.' || p.proname,
            'args', pg_catalog.pg_get_function_identity_arguments(p.oid)
            )
        FROM pg_catalog.pg_proc p
        JOIN pg_catalog.pg_namespace n ON n.oid = p.pronamespace
        WHERE p.oid = t.trftosql
    ) AS to_sql_func,
    pg_catalog.obj_description(t.oid, 'pg_transform') AS objcomment
FROM pg_catalog.pg_transform t
JOIN LATERAL (
    SELECT
        l.lanname
    FROM pg_catalog.pg_language l
    WHERE l.oid = t.trflang AND l.lanispl
) AS lanname ON true
ORDER BY 1;
