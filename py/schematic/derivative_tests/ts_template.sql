--# AFTER

CREATE TEXT SEARCH TEMPLATE pg_catalog.my_simple (
    INIT = pg_catalog.dsimple_init,
    LEXIZE = pg_catalog.dsimple_lexize
);

CREATE TEXT SEARCH TEMPLATE pg_catalog.my_simple2 (
    LEXIZE = pg_catalog.dsimple_lexize
);

--# BEFORE

--# CHECK
SELECT
    ARRAY[n.nspname, tt.tmplname] AS qualname,
    (
        SELECT ARRAY[np.nspname, p.proname]
        FROM pg_catalog.pg_proc p
        JOIN pg_catalog.pg_namespace np ON np.oid = p.pronamespace
        WHERE p.oid = tt.tmplinit
    ) AS tmplinit,
    (
        SELECT ARRAY[np.nspname, p.proname]
        FROM pg_catalog.pg_proc p
        JOIN pg_catalog.pg_namespace np ON np.oid = p.pronamespace
        WHERE p.oid = tt.tmpllexize
    ) AS tmpllexize,
    pg_catalog.obj_description(tt.oid, 'pg_ts_template') AS objcomment
FROM pg_catalog.pg_ts_template tt
JOIN pg_catalog.pg_namespace n ON n.oid = tt.tmplnamespace
WHERE n.nspname !~ '^pg_'
ORDER BY 1;
