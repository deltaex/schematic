--# AFTER
CREATE EXTENSION intarray;
CREATE TABLE orders (
    id SERIAL PRIMARY KEY,
    customer_id INTEGER,
    order_date DATE,
    total_amount DECIMAL
);

CREATE MATERIALIZED VIEW customer_orders_mat_view
USING heap
AS
SELECT
    customer_id,
    SUM(total_amount) AS total_spent
FROM
    orders
GROUP BY
    customer_id
WITH DATA;

CREATE MATERIALIZED VIEW customer_orders_mat_view_def
USING heap
AS
SELECT
    customer_id,
    order_date
FROM
    orders;

CREATE ROLE me;

CREATE MATERIALIZED VIEW customer_orders_mat_view_2
USING heap
AS
SELECT
    customer_id,
    SUM(total_amount) AS total_spent
FROM
    orders
GROUP BY
    customer_id;

-- ALTER MATERIALIZED VIEW customer_orders_mat_view2 ALTER COLUMN total_spent SET 

ALTER MATERIALIZED VIEW customer_orders_mat_view_2 ALTER total_spent SET STORAGE EXTERNAL;
ALTER MATERIALIZED VIEW customer_orders_mat_view_2 ALTER total_spent SET STATISTICS 3;
ALTER MATERIALIZED VIEW customer_orders_mat_view_2 DEPENDS ON EXTENSION intarray;


ALTER MATERIALIZED VIEW customer_orders_mat_view_2 OWNER TO me;

CREATE INDEX ix_test_mat_view_2 ON public.customer_orders_mat_view_2 USING btree ( total_spent );
ALTER MATERIALIZED VIEW customer_orders_mat_view_2 CLUSTER ON ix_test_mat_view_2;
-- ALTER MATERIALIZED VIEW customer_orders_mat_view_2 SET WITHOUT CLUSTER;


--# BEFORE
CREATE EXTENSION intarray;

CREATE ROLE me;
CREATE TABLE orders (
    id SERIAL PRIMARY KEY,
    customer_id INTEGER,
    order_date DATE,
    total_amount DECIMAL
);

CREATE MATERIALIZED VIEW customer_orders_mat_view_def
USING heap
AS
SELECT
    customer_id,
    total_amount
FROM
    orders;


CREATE MATERIALIZED VIEW customer_orders_mat_view_2
USING heap
AS
SELECT
    customer_id,
    SUM(total_amount) AS total_spent
FROM
    orders
GROUP BY
    customer_id;

ALTER MATERIALIZED VIEW customer_orders_mat_view_2 ALTER total_spent SET STATISTICS 5;


--# CHECK
SELECT
    array[n.nspname, c.relname] AS qualname,
    c.relispopulated,
    c.reloptions,
    am.amname,
    tblspc.spcname,
    pg_catalog.pg_get_viewdef(c.oid) AS viewdef,
    pg_catalog.pg_get_userbyid(c.relowner) AS rolname,
    (SELECT array_agg(e.extname) FROM pg_extension e  WHERE e.oid = depends.refobjid) AS extension_names,
    columns.viewcolumns,
    (
        SELECT array[n.nspname, cl.relname]
        FROM pg_catalog.pg_index i
        JOIN pg_catalog.pg_class cl ON i.indexrelid = cl.oid
        JOIN pg_catalog.pg_namespace n ON n.oid = cl.relnamespace
        WHERE i.indrelid = c.oid AND i.indisclustered
    ) AS cluster_index,
    pg_catalog.obj_description(c.oid) AS objcomment
FROM pg_catalog.pg_class c
JOIN pg_catalog.pg_namespace n ON n.oid = c.relnamespace
LEFT JOIN LATERAL (
    SELECT
    json_agg(
        json_build_object(
            'attname', a.attname,
            'attstattarget', a.attstattarget,
            'attstorage', a.attstorage,
            'attoptions', a.attoptions,
            'attcompression', pg_temp.safe_catalog_column('pg_attribute', 'attcompression', 'attrelid = ' || a.attrelid, '')::char
    )) AS viewcolumns
    FROM pg_catalog.pg_attribute a
    WHERE a.attrelid = c.oid AND a.attnum >= 1
) AS columns ON true
LEFT JOIN LATERAL (
    SELECT refobjid FROM pg_catalog.pg_depend d WHERE c.oid = d.objid AND d.refclassid =
    (SELECT oid FROM pg_catalog.pg_class WHERE relname = 'pg_class')
) AS depends ON true
LEFT JOIN pg_catalog.pg_am am ON am.oid = c.relam
LEFT JOIN pg_catalog.pg_tablespace tblspc ON tblspc.oid = c.reltablespace
WHERE n.nspname != 'information_schema' AND n.nspname !~ '^pg_'
AND c.relkind = 'm'
-- AND NOT c.relhasrules
ORDER BY 1;
