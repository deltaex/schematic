'''Build a postgresql replica server.'''
import os
import re
import subprocess
import sys

from toolz import dicttoolz

from schematic import env, fs, pgcn
from schematic.build_util import (
    alter_system_set, ensure_pg_running, install_basedir_postgresql_files, mkdir, write_meta_json)


def replace_conn_data(line, **kwargs):
    '''
    Replaces values in a Postgres keyword/value connection string.

    Note: this implementation assumes that there are no spaces in the values to be replaced.
    If more parameters need to be changed in the future which may contain spaces,
    a refactor to use a more powerful parser instead of a simple regex may be needed.

    >>> replace_conn_data("primary_conninfo = 'sslcompression=0 user=root host=f16 port=1337 sslmode=prefer'", \
        user='new_user', port=12345, host='f32')
    "primary_conninfo = 'sslcompression=0 user=new_user host=f32 port=12345 sslmode=prefer'"
    >>> replace_conn_data("primary_conninfo = 'user=someuser host=localhost port=12345'", port=54321)
    "primary_conninfo = 'user=someuser host=localhost port=54321'"
    '''
    for name, value in kwargs.items():
        line = re.sub(rf'{name}\s*=\s*[^\s\']*', f'{name}={value}', line)
    return line

def new_conn_data(user, host, port):
    '''Generate a new primmary connection string.

    >>> new_conn_data('mixrank', 'f11', '56433')
    "primary_conninfo = 'user=mixrank host=f11 port=56433'"
    '''
    content = ' '.join((f'user={user}', f'host={host}', f'port={port}'))
    return 'primary_conninfo = ' + f'\'{content}\''

def update_conn_details(datadir, user, host, port):
    auto_conf = os.path.join(datadir, 'postgresql.auto.conf')
    conf_lines = fs.try_read(auto_conf, '').splitlines()
    out = ''
    conn_info_found = False
    for line in conf_lines:
        if line.strip().lower().startswith('primary_conninfo'):
            conn_info_found = True
            line = replace_conn_data(line, user=user, host=host, port=port)  # noqa: PLW2901
        out += line + '\n'
    if not conn_info_found:
        out += new_conn_data(user, host, port) + '\n'
    fs.write(auto_conf, out, mode='w')

def main():
    assert not env.get_bool('SCM_PG_UPGRADE'), 'Upgrading major Postgres versions for replicas is not supported'
    port = env.get_str('port')
    name = env.get_str('name')
    dbname = env.get_str('dbname')  # noqa: F841
    guid = env.get_str('guid')
    basedir = env.get_str('basedir')
    primary_host = env.get_str('primary_host')
    primary_port = env.get_str('primary_port')
    primary_dbname = env.get_str('primary_dbname')
    primary_user = env.get_str('primary_user')
    primary_password = env.get_str('primary_password')
    mkdir(basedir)
    write_meta_json()
    datadir = env.get_str('datadir')
    nodatadir = not os.path.exists(datadir)
    install_basedir_postgresql_files()
    if nodatadir:
        # install_basedir_postgresql_files may have created datadir, but it needs to be empty to init replication
        try:
            os.unlink(datadir)
        except FileNotFoundError:
            pass
    # .pgpass file https://www.postgresql.org/docs/12/libpq-pgpass.html
    pgpassfile = os.path.join(basedir, '.pgpass')
    try:
        with open(pgpassfile, 'w') as passfile:
            content = f'{primary_host}:{primary_port}:{primary_dbname}:{primary_user}:{primary_password}'
            passfile.write(content)
        subprocess.check_call(['chmod', '0600', pgpassfile])
        environ = dicttoolz.merge({'PGPASSFILE': pgpassfile}, os.environ)
        datadir = env.get_str('datadir')
        if os.path.exists(datadir):
            print('datadir already initialized: %s' % datadir, file=sys.stderr)
            # Update connection details to primary in case they have changed
            update_conn_details(datadir, primary_user, primary_host, primary_port)
            return
        slot = f'{name}-{guid}'.replace('-', '_').lower()
        try:
            # drop the slot if it already exists so that pg_basebackup doesn't fail creating it
            subprocess.check_call([
                'pg_receivewal', '--no-password', '--username', primary_user, '--host', primary_host,
                '--port', primary_port, '--slot', slot, '--drop-slot'],
                env=environ, stderr=subprocess.PIPE, stdout=subprocess.PIPE)
        except subprocess.CalledProcessError:
            pass
        basebackup_args = [
            os.path.join(basedir, 'bin/pg_basebackup'), '--pgdata', datadir, '--write-recovery-conf',
            '--progress', '--create-slot', '--slot', slot, '--verbose',
            '--no-password', '--username', primary_user, '--host', primary_host, '--port', primary_port]
        if env.get_bool('SCM_ISTEMP', default=True):
            basebackup_args += ['--no-sync']
        subprocess.check_call(basebackup_args, env=environ)
        install_basedir_postgresql_files()
        with open(os.path.join(datadir, 'postgresql.conf'), 'a') as conf_fp:
            conf_fp.write('\nport = %s\n' % port)
        try:
            # remove the port defined by the primary
            auto_conf = os.path.join(datadir, 'postgresql.auto.conf')
            with open(auto_conf, 'w') as conf_fp, open(auto_conf) as auto_fp:
                conf_fp.write('\n'.join([l for l in auto_fp.readlines() if not re.match(r'\s*port\s*=', l)]))
                conf_fp.write('\nport = %s\n' % port)
        except FileNotFoundError:
            pass
        ensure_pg_running(basedir, port)
        with pgcn.connect(env.get_str('pguri'), autocommit=True) as pg:
            alter_system_set(pg, 'port', port)
        install_basedir_postgresql_files()
    finally:
        try:
            os.unlink(pgpassfile)
        except OSError:
            pass

if __name__ == '__main__':
    main()
