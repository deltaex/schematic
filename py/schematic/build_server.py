'''Build a new postgresql database server.'''
import os
import subprocess

import psycopg2

from schematic import env, fs, pgcn
from schematic.build_util import (
    alter_system_set, ensure_pg_running, fmt_footer, fmt_header, install_basedir_postgresql_files,
    mkdir, parse_postgresql_drv_version, split_postgresql_version, write_meta_json)


def create_database(basedir, port, user, dbname):
    conn = psycopg2.connect(env.get_str('pguri_postgres'))
    cursor = conn.cursor()
    cursor.execute('''SELECT * FROM pg_database WHERE datname = %(dbname)s;''', vars={'dbname': dbname})
    database = cursor.fetchone()
    if not database:
        subprocess.check_call([
            os.path.join(basedir, 'bin/createdb'), '-p', port, '-U', user, dbname,
            '--encoding', 'utf8', '--locale', 'C'])

def get_server_settings():
    '''Server-level configuration settings that can't be overriden.'''
    return {
        'port': env.get_str('port')
    }

def make_settings_section():
    '''
    Wraps the server configuration in a settings section, to ensure that it's not
    removed by build_pgconf.
    '''
    guid = env.get_str('guid')
    name = env.get_str('name')
    ret = fmt_header(guid, name, comment='Non-overridable server settings', depends=None) + '\n'
    ret += '\n'.join(f'{name} = {value}' for name, value in get_server_settings().items()) + '\n'
    ret += fmt_footer(guid, name) + '\n'
    return ret

def is_initdb_flag_supported(flag):
    pgversion = split_postgresql_version(parse_postgresql_drv_version())
    if flag == '--allow-group-access' and pgversion >= [11]:
        return True
    if flag == '--no-sync' and pgversion >= [10]:
        return True
    helptext = subprocess.check_output([os.path.join(env.get_str('basedir'), 'bin/initdb'), '--help'], text=True)
    return flag in helptext

def main():
    port = env.get_str('port')
    dbname = env.get_str('dbname')
    user = env.get_str('user')
    basedir = env.get_str('basedir')
    mkdir(basedir)
    write_meta_json()
    install_basedir_postgresql_files()
    datadir = env.get_str('datadir')
    pwfile = os.path.join(basedir, 'pwfile')
    if not os.path.exists(datadir):
        fs.write(pwfile, env.get_str('password'))
        initdb_args = [
            os.path.join(basedir, 'bin/initdb'), '-D', datadir, '-U', user, '--pwfile', pwfile,
            '--encoding', 'utf8', '--locale', 'C']
        if is_initdb_flag_supported('--allow-group-access'):
            initdb_args += ['--allow-group-access']
        if env.get_bool('SCM_ISTEMP', default=True) and is_initdb_flag_supported('--no-sync'):
            initdb_args += ['--no-sync']
        if env.get_bool('SCM_VERBOSE'):
            subprocess.check_call(initdb_args, stdout=2)
        else:
            subprocess.check_call(initdb_args, stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL)
        try:
            os.unlink(pwfile)
        except FileNotFoundError:
            pass
        conf_filepath = os.path.join(datadir, 'postgresql.conf')
        fs.write(conf_filepath, '\n' + make_settings_section(), mode='a')
        # Make an aux file to signal that this is a newly created DB and so
        # build_database.py is free to restart it if some settings need it
        fs.touch(os.path.join(basedir, '.new_db'))
    ensure_pg_running(basedir, port, poll_interval=0.1)
    if not env.get_bool('SCM_PG_UPGRADE'):
        # pg_upgrade will create the database later in the upgrade process
        create_database(basedir, port, user, dbname)
        with pgcn.connect(env.get_str('pguri'), autocommit=True) as pg:
            if split_postgresql_version(parse_postgresql_drv_version()) >= [9, 4]:
                alter_system_set(pg, 'port', port)
                if env.get_bool('SCM_ISTEMP', default=True):
                    alter_system_set(pg, 'fsync', 'off')
                    pg.execute('SELECT pg_reload_conf();')
    install_basedir_postgresql_files()

if __name__ == '__main__':
    main()
