import os
import random
import re

import psycopg2

from schematic import build_util, ddlgen, env, pg_catalog, slog


def action_exec(pg_tgt, statement):
    ''' Execute but don't log the statement. '''
    pg_tgt.execute(statement)

def action_logexec(pg_tgt, statement):
    ''' Execute and log the statement. '''
    statement_text = ddlgen.as_string(statement)
    statement_oneline = build_util.onelinesql(statement_text)
    pkg = (
        f'<{env.get_str("name")}-{env.get_str("guid")}>'
        if (env and 'name' in os.environ and 'guid' in os.environ) else '')
    port = env.get_int('port')
    is_temp = env.get_bool('SCM_ISTEMP', default=False)
    logline = f':{port} {pkg} {statement_oneline}'
    (slog.debug if is_temp else slog.info)(logline)
    build_util.log_msg_upgrade_sql_text(statement_oneline)
    pg_tgt.execute(statement)

def action_dryrun(pg_tgt, statement):  # noqa: ARG001
    ''' Print the statement to STDOUT but don't execute it. '''
    print(ddlgen.as_string(statement) + ';')

def action_none(pg_tgt, statement):  # noqa: ARG001
    pass

def add_all(pg_ref, pg_tgt, action=action_logexec, validate_constraints=True, allow_commit=True):
    ''' Add all schema from reference database into target database. '''
    initialize(pg_ref)
    initialize(pg_tgt)
    rem_invalid_indexes(pg_ref, pg_tgt, action, allow_commit=allow_commit)
    add_roles(pg_ref, pg_tgt, action)
    add_namespaces(pg_ref, pg_tgt, action)
    add_extensions(pg_ref, pg_tgt, action)
    add_languages(pg_ref, pg_tgt, action)
    add_access_methods(pg_ref, pg_tgt, action)
    add_collations(pg_ref, pg_tgt, action)
    add_conversions(pg_ref, pg_tgt, action)
    add_ts_parsers(pg_ref, pg_tgt, action)
    add_ts_templates(pg_ref, pg_tgt, action)
    add_ts_dicts(pg_ref, pg_tgt, action)
    add_ts_configs(pg_ref, pg_tgt, action)
    add_databases(pg_ref, pg_tgt, action, allow_commit=allow_commit)
    add_base_types(pg_ref, pg_tgt, action)
    add_enum_types(pg_ref, pg_tgt, action)
    add_domain_types(pg_ref, pg_tgt, action, validate_constraints=validate_constraints)
    add_sequences(pg_ref, pg_tgt, action)
    add_view_stubs(pg_ref, pg_tgt, action)
    add_composite_types(pg_ref, pg_tgt, action)
    add_tables(pg_ref, pg_tgt, action)
    add_rules(pg_ref, pg_tgt, action)
    add_policies(pg_ref, pg_tgt, action)
    add_publications(pg_ref, pg_tgt, action)
    add_range_types(pg_ref, pg_tgt, action)
    add_functions(pg_ref, pg_tgt, action)
    add_operators(pg_ref, pg_tgt, action)
    add_op_families(pg_ref, pg_tgt, action)
    add_op_classes(pg_ref, pg_tgt, action)
    add_op_families_details(pg_ref, pg_tgt, action)
    add_languages(pg_ref, pg_tgt, action)
    add_aggregates(pg_ref, pg_tgt, action)
    add_composite_attributes(pg_ref, pg_tgt, action)
    add_columns(pg_ref, pg_tgt, action)
    add_casts(pg_ref, pg_tgt, action)
    add_indexes(pg_ref, pg_tgt, action, allow_commit=allow_commit)
    add_statistics(pg_ref, pg_tgt, action)
    set_sequences_owned_by(pg_ref, pg_tgt, action)
    add_views(pg_ref, pg_tgt, action)
    add_mat_views(pg_ref, pg_tgt, action)
    add_triggers(pg_ref, pg_tgt, action)
    add_table_detail(pg_ref, pg_tgt, action, allow_commit=allow_commit)
    add_evt_trg(pg_ref, pg_tgt, action)
    add_foreign_servers(pg_ref, pg_tgt, action)
    add_user_mappings(pg_ref, pg_tgt, action)
    add_foreign_data_wrappers(pg_ref, pg_tgt, action)
    add_transforms(pg_ref, pg_tgt, action)

def initialize(pg):
    pg.execute(''' SET SESSION search_path = ''; ''')
    pg.execute('''
        CREATE OR REPLACE FUNCTION pg_temp.get_sequence_last_value(namespace text, seqname text)
        RETURNS bigint LANGUAGE plpgsql STRICT AS $$
        DECLARE ret bigint;
        BEGIN
            EXECUTE format('SELECT last_value FROM %I.%I', namespace, seqname) INTO ret;
            RETURN ret;
        END; $$;

        CREATE OR REPLACE FUNCTION pg_temp.safe_catalog_column(_table_name text, _column_name text, _where_clause text, _default text)
        RETURNS text AS $$
        DECLARE
            _value text;
        BEGIN
            IF EXISTS (
                SELECT 1 FROM information_schema.columns
                WHERE (table_schema, table_name, column_name) = ('pg_catalog', _table_name, _column_name)
            ) THEN
                EXECUTE format('SELECT %I FROM pg_catalog.%I WHERE %s',  _column_name, _table_name, _where_clause) INTO _value;
            ELSE
                _value := _default;
            END IF;
            RETURN _value;
        END
        $$ LANGUAGE plpgsql;

        -- like safe_catalog_column, but returns pg_node_tree type, which can not be cast from text
        CREATE OR REPLACE FUNCTION pg_temp.safe_catalog_column_pg_node_tree(_table_name text, _column_name text, _where_clause text, _default pg_node_tree)
        RETURNS pg_node_tree AS $$
        DECLARE
            _value pg_node_tree;
        BEGIN
            IF EXISTS (
                SELECT 1 FROM information_schema.columns
                WHERE (table_schema, table_name, column_name) = ('pg_catalog', _table_name, _column_name)
            ) THEN
                EXECUTE format('SELECT %I FROM pg_catalog.%I WHERE %s',  _column_name, _table_name, _where_clause) INTO _value;
            ELSE
                _value := _default;
            END IF;
            RETURN _value;
        END
        $$ LANGUAGE plpgsql;

        CREATE OR REPLACE FUNCTION pg_temp.check_catalog_exists(_table_name text, _column_name text)
        RETURNS boolean AS $$
        BEGIN
            RETURN EXISTS (
                SELECT 1 FROM information_schema.columns
                WHERE (table_schema, table_name, column_name) = ('pg_catalog', _table_name, _column_name)
            );
        END
        $$ LANGUAGE plpgsql;

        CREATE OR REPLACE FUNCTION pg_temp.safe_catalog_column_multirow(_table_name text, _column_name text, _where_clause text, _default text) RETURNS text[] AS $$
        DECLARE _value text[];
        BEGIN
            IF EXISTS (
                SELECT 1 FROM information_schema.columns
                WHERE (table_schema, table_name, column_name) = ('pg_catalog', _table_name, _column_name)
            ) THEN
                EXECUTE format('SELECT array_agg(%I) FROM pg_catalog.%I WHERE %s',  _column_name, _table_name, _where_clause) INTO  _value;
            ELSE
                _value := _default;
            END IF;
            RETURN _value;
        END
        $$ LANGUAGE plpgsql;

        -- this function was created because postgres texts can't be cast into int2vector
        CREATE OR REPLACE FUNCTION pg_temp.safe_get_int2vector(_table_name text, _column_name text, _where_clause text)
        RETURNS int2[] AS $$
        DECLARE
            _value int2[];
        BEGIN
            IF EXISTS (
                SELECT 1 FROM information_schema.columns
                WHERE (table_schema, table_name, column_name) = ('pg_catalog', _table_name, _column_name)
            ) THEN
                EXECUTE format('SELECT ARRAY(SELECT unnest(%I) FROM pg_catalog.%I WHERE %s)',  _column_name, _table_name, _where_clause) INTO _value;
                RETURN _value;
            ELSE
                RETURN NULL;
            END IF;
        END
        $$ LANGUAGE plpgsql;

        CREATE OR REPLACE FUNCTION pg_temp.try_cast(_in text, INOUT _out ANYELEMENT)
        LANGUAGE plpgsql AS
        $$
        BEGIN
            EXECUTE format('SELECT %L::%s', $1, pg_typeof(_out))
            INTO  _out;
        EXCEPTION WHEN others THEN
            -- do nothing
        END
        $$;
    ''')
    # CREATE OR REPLACE FUNCTION pg_temp.major_version()
    # RETURNS int LANGUAGE sql AS $$
    #     SELECT CASE
    #         WHEN version() ~ 'PostgreSQL 12' THEN 12
    #         WHEN version() ~ 'PostgreSQL 13' THEN 13
    #         WHEN version() ~ 'PostgreSQL 14' THEN 14
    #         WHEN version() ~ 'PostgreSQL 15' THEN 15
    #         WHEN version() ~ 'PostgreSQL 16' THEN 16
    #         WHEN version() ~ 'PostgreSQL 17' THEN 17
    #         ELSE NULL END;
    # $$;

class ImpossibleChange(Exception):

    def __init__(self, qualname, msg, bef, aft):
        super(ImpossibleChange).__init__()
        self.qualname = qualname
        self.msg = msg
        self.bef = bef
        self.aft = aft

    def __repr__(self):
        return f'ImpossibleChange({".".join(self.qualname)!r}, {self.msg!r})'

    def __str__(self):
        return f'{".".join(self.qualname)}; {self.msg}; from: {self.bef} to: {self.aft}'

class CreateIndexFailure(Exception):

    def __init__(self, qualname, msg, bef, aft):
        super(CreateIndexFailure).__init__()
        self.qualname = qualname
        self.msg = msg
        self.bef = bef
        self.aft = aft

    def __repr__(self):
        return f'CreateIndexFailure({".".join(self.qualname)!r}, {self.msg!r})'

    def __str__(self):
        return f'{".".join(self.qualname)}; {self.msg}; from: {self.bef} to: {self.aft}'

def iter_diffs(pg_ref, pg_tgt, getter):
    refs = getter(pg_ref)
    tgts = getter(pg_tgt)
    yield from [(key, ref, tgts.get(key)) for (key, ref) in refs.items() if ref != tgts.get(key)]

def add_namespaces(pg_ref, pg_tgt, action):
    ''' Add namespaces from reference database into a target database. '''
    for qualname, ref, tgt in iter_diffs(pg_ref, pg_tgt, pg_catalog.get_namespaces):
        if ref and not tgt:
            action(pg_tgt, ddlgen.create_namespace(qualname))
        if ref.rolname not in (tgt.rolname if tgt else None, 'pg_database_owner'):
            # avoid comparing with role 'pg_database_owner' because it is a predefined role
            # and thus, everytime we create a sandbox, this is the owner of the namespace except if otherwise specified.
            # https://www.postgresql.org/docs/current/predefined-roles.html
            action(pg_tgt, ddlgen.alter_namespace_owner_to(qualname, ref.rolname))
        if ref.objcomment != (tgt.objcomment if tgt else None):
            action(pg_tgt, ddlgen.comment_generic(qualname, 'SCHEMA', ref.objcomment))

def add_sequences(pg_ref, pg_tgt, action):
    ''' Add sequences from reference database into a target database. '''
    def _attrs(obj):
        return (
            obj.typname, obj.seqstart, obj.seqincrement, obj.seqmax, obj.seqmin, obj.seqcache, obj.seqcache,
            obj.seqcycle)
    for qualname, ref, tgt in iter_diffs(pg_ref, pg_tgt, pg_catalog.get_sequences):
        # don't override seqeuence state with static value, only set restart value if sequence is unused
        # also, only restart if the ref's last_value <= tgt.seqmax
        do_restart = tgt and (ref.last_value != tgt.last_value) and (ref.last_value <= tgt.seqmax) and (
            tgt.last_value == tgt.seqstart)
        if ref and not tgt:
            action(pg_tgt, ddlgen.create_sequence(
                qualname, data_type=ref.typname,
                increment=ref.seqincrement, minvalue=ref.seqmin, maxvalue=ref.seqmax, start=ref.seqstart,
                cache=ref.seqcache, cycle=ref.seqcycle, restart=ref.last_value, unlogged=ref.relpersistence == 'u'))
        elif _attrs(ref) != _attrs(tgt) or do_restart:
            restart = ref.last_value if do_restart else None
            if ref.last_value > tgt.seqmax:
                slog.warn2('Ref last_value > target seqmax, hence this sequence will not be restarted')
            action(pg_tgt, ddlgen.alter_sequence(
                qualname, data_type=ref.typname,
                increment=ref.seqincrement, minvalue=ref.seqmin, maxvalue=ref.seqmax,
                start=ref.seqstart,
                cache=ref.seqcache, cycle=ref.seqcycle, restart=restart))
        if tgt and ref.relpersistence != tgt.relpersistence:
            action(pg_tgt, ddlgen.alter_sequence_persistence(qualname, unlogged=ref.relpersistence == 'u'))
        if ref.rolname != (tgt.rolname if tgt else None):
            action(pg_tgt, ddlgen.alter_sequence_owner_to(qualname, ref.rolname))
        if ref.objcomment != (tgt.objcomment if tgt else None):
            action(pg_tgt, ddlgen.comment_generic(qualname, 'SEQUENCE', ref.objcomment))

def set_sequences_owned_by(pg_ref, pg_tgt, action):
    ''' Associated sequences with the columns they are "OWNED BY". Separated from sequence creation so it can run
        after the columns have been added.
    '''
    for qualname, ref, tgt in iter_diffs(pg_ref, pg_tgt, pg_catalog.get_sequences):
        if ref and tgt and ref.owned_by != tgt.owned_by:
            action(pg_tgt, ddlgen.alter_sequence(
                qualname, data_type=ref.typname,
                increment=ref.seqincrement, minvalue=ref.seqmin, maxvalue=ref.seqmax, start=ref.seqstart,
                cache=ref.seqcache, cycle=ref.seqcycle, owned_by=ref.owned_by))

def add_missing_funcs(pg_ref, pg_tgt, action, oids):
    results = pg_ref.execute('''
        SELECT
            n.nspname,
            f.proname,
            pg_catalog.pg_get_functiondef(f.oid) AS funcdef,
            pg_get_function_identity_arguments(f.oid) AS funcargs
        FROM pg_proc f
        JOIN pg_namespace n ON n.oid = f.pronamespace
        WHERE f.oid = ANY(%(oids)s)
        AND n.nspname NOT IN ('pg_toast', 'pg_catalog', 'information_schema')
    ''', {'oids': oids}).fetchall()
    for x in results:
        exists = pg_tgt.execute('''
            SELECT 1
            FROM pg_catalog.pg_proc f
            JOIN pg_catalog.pg_namespace n ON n.oid = f.pronamespace
            WHERE n.nspname != 'information_schema' AND n.nspname !~ '^pg_'
            AND (n.nspname, f.proname) = (%(nspname)s, %(proname)s)
            AND pg_catalog.pg_get_function_identity_arguments(f.oid) = %(funcargs)s
            LIMIT 1;
        ''', {'nspname': x.nspname, 'proname': x.proname, 'funcargs': x.funcargs}).scalar()
        if exists:
            continue
        action(pg_tgt, ddlgen.create_function((x.nspname, x.proname), x.funcdef))

def add_tables(pg_ref, pg_tgt, action):
    ''' Add tables from reference database into a target database. '''
    def create_dependency_funcs():
        ''' Tables can depend on columns which can depend on functions (eg GENERATED ALWAYS AS STORED) in a way that the
            dependency can't be added later, so we do an extra check to make sure we have the functions available before
            creating these columns.
        '''
        oids = pg_ref.execute('''
            SELECT array_agg(d2.refobjid)
            FROM pg_catalog.pg_class c
            JOIN pg_catalog.pg_namespace n ON n.oid = c.relnamespace
            JOIN pg_catalog.pg_depend d1 ON d1.refobjid = c.oid
                AND d1.refclassid = 'pg_catalog.pg_class'::regclass
                AND d1.classid = 'pg_catalog.pg_attrdef'::regclass
            LEFT JOIN pg_catalog.pg_depend d2 ON d2.objid = d1.objid
                AND d2.refclassid = 'pg_catalog.pg_proc'::regclass
                AND d2.classid = 'pg_catalog.pg_attrdef'::regclass
            WHERE n.nspname != 'information_schema' AND n.nspname !~ '^pg_'
            AND c.relkind = 'r'
            AND d2.refobjid IS NOT NULL;
        ''').scalar()
        add_missing_funcs(pg_ref, pg_tgt, action, oids) if oids else 0
        # additionally needed for postgresql 12-14:
        oids = pg_ref.execute('''
            SELECT array_agg(d1.refobjid)
            FROM pg_catalog.pg_class c
            JOIN pg_catalog.pg_namespace n ON n.oid = c.relnamespace
            JOIN pg_catalog.pg_depend d1 ON d1.objid = c.oid
                AND d1.refclassid = 'pg_catalog.pg_proc'::regclass
                AND d1.classid = 'pg_catalog.pg_class'::regclass
            WHERE n.nspname != 'information_schema' AND n.nspname !~ '^pg_'
            AND c.relkind = 'r';
        ''').scalar()
        add_missing_funcs(pg_ref, pg_tgt, action, oids) if oids else 0
    create_dependency_funcs()
    for qualname, ref, tgt in iter_diffs(pg_ref, pg_tgt, pg_catalog.get_tables):
        if ref and not tgt:
            cols = pg_catalog.get_simple_colummns(pg_ref, qualname[1], qualname[0])
            if ref.parent_table:
                action(pg_tgt, ddlgen.create_table_pt_of(
                    qualname, ref.parent_table, cols=cols, unlogged=ref.relpersistence == 'u',
                    partdef=ref.partdef, amname=ref.amname,
                    reloptions=ref.reloptions, partition_bound_spec=ref.part_bound, spcname=ref.spcname))
            elif ref.of_type:
                action(pg_tgt, ddlgen.create_table_of_type(
                    qualname, ref.of_type, unlogged=ref.relpersistence,
                    partdef=ref.partdef, amname=ref.amname,
                    reloptions=ref.reloptions, cols=cols, spcname=ref.spcname))
            else:
                action(pg_tgt, ddlgen.create_table(
                    qualname, unlogged=ref.relpersistence == 'u', inherits=ref.inherits,
                    partdef=ref.partdef, amname=ref.amname,
                    reloptions=ref.reloptions, cols=cols, spcname=ref.spcname))
        if tgt:
            if ref.reloptions != tgt.reloptions:
                missing_opts = ddlgen.optnames(tgt.reloptions or []) - ddlgen.optnames(ref.reloptions or [])
                if missing_opts:
                    action(pg_tgt, ddlgen.alter_table_reset_attributes(qualname, ref.reloptions, tgt.reloptions))
                if ref.reloptions:
                    action(pg_tgt, ddlgen.alter_table_set_reloptions(qualname, ref.reloptions))
            if ref.relpersistence != tgt.relpersistence:
                action(pg_tgt, ddlgen.alter_table_unlogged(qualname, ref.relpersistence))
            if ref.amname != tgt.amname:
                action(pg_tgt, ddlgen.alter_table_access_method(qualname, ref.amname))
            if ref.spcname and ref.spcname != tgt.spcname:
                action(pg_tgt, ddlgen.alter_table_tablespace(qualname, ref.spcname))
            if ref.ty != tgt.ty:
                if tgt.ty.get('typname') and not ref.ty.get('typname'):
                    action(pg_tgt, ddlgen.alter_table_type(qualname, True, tgt.ty['typname']))
                elif ref.ty.get('typname'):
                    action(pg_tgt, ddlgen.alter_table_type(qualname, False, ref.ty))
            if ref.objcomment != (tgt.objcomment if tgt else None):
                action(pg_tgt, ddlgen.comment_generic(qualname, 'TABLE', ref.objcomment))

def should_minimize_access_exclusive_lock(pg_tgt, namespace, tablename):
    max_table_size = 100 * 1024 * 1024
    table_size = pg_tgt.execute('''
        SELECT pg_table_size(c.oid)
        FROM pg_class c
        JOIN pg_namespace n ON n.oid = c.relnamespace AND n.nspname = %(namespace)s
        WHERE c.relname = %(relname)s;
    ''', {'namespace': namespace, 'relname': tablename}).scalar()
    return (table_size or 0) > max_table_size

def add_table_detail(pg_ref, pg_tgt, action, allow_commit=True):
    ''' Add table details from reference database into a target database.
    Executed after tables and columns have been added.
    Additions here are those which need to be done after adding columns to tables.
    '''
    for qualname, ref, tgt in iter_diffs(pg_ref, pg_tgt, pg_catalog.get_tables):
        (namespace, objname) = qualname
        if tgt:
            if ref.partitions != tgt.partitions:
                ref_tgt_part_diff = [x for x in (ref.partitions or []) if x not in (tgt.partitions or [])]
                tgt_ref_part_diff = [x for x in (tgt.partitions or []) if x not in (ref.partitions or [])]
                autocommit = pg_tgt.conn.autocommit
                if allow_commit and not autocommit:
                    pg_tgt.commit()
                    pg_tgt.conn.set_session(autocommit=True)
                for x in tgt_ref_part_diff:
                    action(pg_tgt, ddlgen.alter_table_detach_partition(
                        qualname,
                        x['part_name'],
                        concurrent=allow_commit,
                        finalize=allow_commit))
                if allow_commit and not autocommit:
                    pg_tgt.commit()
                    pg_tgt.conn.set_session(autocommit=False)
                for x in ref_tgt_part_diff:
                    action(pg_tgt, ddlgen.alter_table_attach_partition(qualname, x['part_name'], x['bound']))
            if ref.relrowsecurity != tgt.relrowsecurity:
                action(pg_tgt, ddlgen.alter_table_row_level_security(qualname, ref.relrowsecurity))
            if ref.relforcerowsecurity != tgt.relforcerowsecurity:
                action(pg_tgt, ddlgen.alter_table_row_level_force_security(qualname, ref.relforcerowsecurity))
            if ref.relreplident != tgt.relreplident or ref.replica_index != tgt.replica_index:
                action(pg_tgt, ddlgen.alter_table_replica_id(qualname, ref.relreplident, ref.replica_index))
            if ref.inherits != tgt.inherits:
                # To be added as a child, the target table must already contain all the same columns as the parent
                # So, here, we add the columns which were not added earlier by `add_columns`, since `add_columns`
                # only adds columns local to the table
                # The problem here is that `attlocal` will always be True in this branch, even if in the reference
                # database it is not, and I do not know how to circumvent this occurence
                ref_all_cols = pg_catalog.get_table_columns(pg_ref, objname, namespace)
                tgt_nonloc_cols = pg_catalog.get_table_columns(pg_tgt, objname, namespace)
                ref_tgt_cols_diff = {x: y for x, y in (ref_all_cols.items() or {}) if x not in (tgt_nonloc_cols or {})}
                for colname, col in ref_tgt_cols_diff.items():
                    action(
                        pg_tgt,
                        ddlgen.create_column(
                            qualname=(namespace, objname, colname),
                            typname=col['typname'],
                            collname=col['collname'],
                            attnotnull=col['attnotnull'],
                            attidentity=col['attidentity'],
                            attgenerated=col['attgenerated'],
                            atthasdef=col['atthasdef'],
                            attdef=col['attdef']))
                for x in tgt.inherits or []:
                    if x not in (ref.inherits or []):
                        action(pg_tgt, ddlgen.alter_table_inherit(
                            qualname, x, True))
                for x in ref.inherits or []:
                    if x not in (tgt.inherits or []):
                        action(pg_tgt, ddlgen.alter_table_inherit(
                            qualname, x, False))
            if ref.cluster_index != tgt.cluster_index:
                if not ref.cluster_index:
                    action(pg_tgt, ddlgen.alter_table_no_cluster(qualname))
                else:
                    action(pg_tgt, ddlgen.alter_table_cluster_on(qualname, ref.cluster_index))
            if ref.constraints != tgt.constraints and not ref.parent_table:
                # When the table is large,
                # creating a new constraint will take it offline for writes while it scans the whole table
                # This can take a long time. We create the table with `NOT VALID` when it's larger then 100MiB
                notvalid = should_minimize_access_exclusive_lock(pg_tgt, namespace, objname)
                ref_constraints = {x['conname']: x for x in (ref.constraints or [])}
                tgt_constraints = {x['conname']: x for x in (tgt.constraints or [])}
                ref_tgt_const_diff = {k: v for k, v in ref_constraints.items() if k not in tgt_constraints}
                for k, v in ref_tgt_const_diff.items():
                    action(pg_tgt, ddlgen.alter_table_add_constraint(
                        qualname,
                        k,
                        v['constraint_def'],
                        v['contype'],
                        v['connoinherit'],
                        v['condeferrable'],
                        v['condeferred'],
                        convalidated=v['convalidated'],
                        index_name=v['index_name'],
                        notvalid=notvalid))
                # don't drop missing constraints. we'd like to keep consistency with missing domain constraints
                # tgt_ref_const_diff = {k: v for k, v in tgt_constraints.items() if k not in ref_constraints}
                # for k in tgt_ref_const_diff:
                #     action(pg_tgt, ddlgen.alter_table_drop_constraint(qualname, k))
                for k, v in tgt_constraints.items():
                    ref_con = ref_constraints.get(k)
                    if ref_con and (ref_con['condeferrable'] != v['condeferrable'] or\
                        ref_con['condeferred'] != v['condeferred']):
                        action(pg_tgt, ddlgen.alter_table_alter_constraint(
                            qualname, k, ref_con['condeferrable'], ref_con['condeferred']))
            ref_rules = ref.rules or {}
            tgt_rules = tgt.rules or {}
            if ref_rules != tgt_rules:
                for k, v in ref_rules.items():
                    if k not in tgt_rules or tgt_rules.get(k) != v:
                        action(pg_tgt, ddlgen.alter_table_rule(qualname, k, v))
                for k, v in tgt_rules.items():
                    if k in ref_rules and ref_rules.get(k) != v:
                        action(pg_tgt, ddlgen.alter_table_rule(qualname, k, ref_rules.get(k)))
                    elif k not in ref_rules:
                        action(pg_tgt, ddlgen.alter_table_rule(qualname, k, 'D'))
            if ref.objcomment != (tgt.objcomment if tgt else None):
                action(pg_tgt, ddlgen.comment_generic(qualname, 'TABLE', ref.objcomment))
            if ref.rolname != tgt.rolname:
                action(pg_tgt, ddlgen.alter_table_owner(qualname, ref.rolname))

def add_base_types(pg_ref, pg_tgt, action):
    ''' Add base types from reference database into a target database. '''
    def create_type_funcs(namespace, objname):
        createfuncs = pg_ref.execute('''
            WITH func_oids AS (
                SELECT unnest(array[
                    nullif(nullif(pg_temp.safe_catalog_column('pg_type', 'typsubscript', 'oid = ' || t.oid, '0'), '-'), '0')::oid,
                    t.typinput, t.typoutput, t.typreceive, t.typsend, t.typmodin, t.typmodout,
                    t.typanalyze]) AS oids
                FROM pg_type t
                JOIN pg_namespace n ON n.oid = t.typnamespace
                WHERE n.nspname = %s AND t.typname = %s
            )
            SELECT json_agg(pg_catalog.pg_get_functiondef(f.oid)) AS funcdefs
            FROM pg_proc f
            JOIN pg_namespace n ON n.oid = f.pronamespace
            WHERE f.oid = ANY((SELECT oids FROM func_oids))
            AND n.nspname NOT IN ('pg_toast', 'pg_catalog', 'information_schema')
        ''', (namespace, objname)).fetchone()
        action(pg_tgt, ddlgen.create_multiple_functions(createfuncs.funcdefs))
    def typargs(obj):
        return dict(  # pylint: disable=use-dict-literal
            input_func=obj.input_func, output_func=obj.output_func, receive_func=obj.receive_func,
            send_func=obj.send_func, modin_func=obj.modin_func, modout_func=obj.modout_func,
            analyze_func=obj.analyze_func, subscript_func=obj.subscript_func, typlen=obj.typlen, typalign=obj.typalign,
            typbyval=obj.typbyval, typstorage=obj.typstorage, typcategory=obj.typcategory,
            typispreferred=obj.typispreferred, typdefault=obj.typdefault, subscript_type=obj.subscript_type,
            typdelim=obj.typdelim, collatable=bool(obj.collname))
    for qualname, ref, tgt in iter_diffs(pg_ref, pg_tgt, pg_catalog.get_base_types):
        (namespace, objname) = qualname
        if ref and not tgt:
            # see: https://www.postgresql.org/docs/current/sql-createtype.html
            # must first do CREATE TYPE, then create the functions that support that type, then CREATE TYPE again
            # referring to the support functions.
            action(pg_tgt, ddlgen.create_base_type_step1(qualname))
            create_type_funcs(namespace, objname)
            action(pg_tgt, ddlgen.create_base_type_step2(qualname, **typargs(ref)))
        else:
            ref_args = typargs(ref)
            tgt_args = typargs(tgt)
            diffopts = {k: v for (k, v) in ref_args.items() if tgt_args[k] != v}
            if diffopts:
                create_type_funcs(namespace, objname)
                action(pg_tgt, ddlgen.alter_base_type(qualname, **diffopts))
        if ref.rolname != (tgt.rolname if tgt else None):
            action(pg_tgt, ddlgen.alter_type_owner_to(qualname, ref.rolname))
        if ref.objcomment != (tgt.objcomment if tgt else None):
            action(pg_tgt, ddlgen.comment_generic(qualname, 'TYPE', ref.objcomment))

def add_enum_types(pg_ref, pg_tgt, action):
    ''' Add enum types from reference database into a target database. '''
    for qualname, ref, tgt in iter_diffs(pg_ref, pg_tgt, pg_catalog.get_enum_types):
        if ref and not tgt:
            action(pg_tgt, ddlgen.create_enum_type(qualname, labels=ref.labels))
        else:
            for idx, label in enumerate(ref.labels):
                if label not in tgt.labels:
                    if not tgt.labels:
                        relative = to = None
                    elif idx == 0:
                        relative = 'BEFORE'
                        to = tgt.labels[0]
                    else:
                        relative = 'AFTER'
                        to = ref.labels[idx - 1]
                    action(pg_tgt, ddlgen.alter_enum_type_add_value(
                        qualname, label=label, relative=relative, to=to))
        if ref.rolname != (tgt.rolname if tgt else None):
            action(pg_tgt, ddlgen.alter_type_owner_to(qualname, ref.rolname))
        if ref.objcomment != (tgt.objcomment if tgt else None):
            action(pg_tgt, ddlgen.comment_generic(qualname, 'TYPE', ref.objcomment))

def add_range_types(pg_ref, pg_tgt, action):
    ''' Add range types from reference database to target database. '''
    for qualname, ref, tgt in iter_diffs(pg_ref, pg_tgt, pg_catalog.get_range_types):
        if ref and not tgt:
            if ref.canonical_function:
                # create the shell type
                action(pg_tgt, ddlgen.create_shell_type(qualname))
                action(pg_tgt, ddlgen.create_function(
                    ref.canonical_function['name'],
                    ref.canonical_function['def']))
            action(pg_tgt, ddlgen.create_range_type(
                qualname,
                ref.subtype,
                ref.subtype_operator_class,
                ref.collation,
                ref.canonical_function['name'] if ref.canonical_function else None,
                ref.subtype_diff_function,
                ref.multirange_type_name))
        if ref.rolname != (tgt.rolname if tgt else None):
            action(pg_tgt, ddlgen.alter_type_owner_to(qualname, ref.rolname))
        if ref.objcomment != (tgt.objcomment if tgt else None):
            action(pg_tgt, ddlgen.comment_generic(qualname, 'TYPE', ref.objcomment))

def add_composite_types(pg_ref, pg_tgt, action):
    ''' Add composite types from reference database into a target database. '''
    for qualname, ref, tgt in iter_diffs(pg_ref, pg_tgt, pg_catalog.get_composite_types):
        if ref and not tgt:
            action(pg_tgt, ddlgen.create_composite_type(qualname))
        if ref.rolname != (tgt.rolname if tgt else None):
            action(pg_tgt, ddlgen.alter_type_owner_to(qualname, ref.rolname))
        if ref.objcomment != (tgt.objcomment if tgt else None):
            action(pg_tgt, ddlgen.comment_generic(qualname, 'TYPE', ref.objcomment))

def _remove_notvalid(text):
    '''
    >>> _remove_notvalid('CHECK (VALUE > 1) NOT VALID')
    'CHECK (VALUE > 1)'
    >>> _remove_notvalid('CHECK (VALUE > 1) NOT VALID foobar')
    'CHECK (VALUE > 1) NOT VALID foobar'
    '''
    if text is None:
        return
    return re.sub(r' NOT VALID$', '', text)

def add_domain_types(pg_ref, pg_tgt, action, validate_constraints=False):
    ''' Add domain types from reference database into a target database. '''
    def constraintdict(constraints):
        return {k: (_remove_notvalid(v) if not validate_constraints else v) for (k, v) in constraints or []}
    def create_dependency_funcs():
        ''' Domain types can depend on constraints which can depend on functions. Functions are created later in this
            module, so do an extra check to be sure dependency functions are created first.
        '''
        oids = pg_ref.execute('''
            SELECT array_agg(d2.refobjid)
            FROM pg_catalog.pg_type t
            JOIN pg_catalog.pg_namespace n ON n.oid = t.typnamespace
            JOIN pg_catalog.pg_depend d1 ON d1.refobjid = t.oid
                AND d1.refclassid = 'pg_catalog.pg_type'::regclass
                AND d1.classid = 'pg_catalog.pg_constraint'::regclass
            LEFT JOIN pg_catalog.pg_depend d2 ON d2.objid = d1.objid
                AND d2.refclassid = 'pg_catalog.pg_proc'::regclass
                AND d2.classid = 'pg_catalog.pg_constraint'::regclass
            WHERE n.nspname != 'information_schema' AND n.nspname !~ '^pg_'
            AND t.typtype = 'd';
        ''').scalar()
        add_missing_funcs(pg_ref, pg_tgt, action, oids) if oids else 0
    create_dependency_funcs()
    for qualname, ref, tgt in iter_diffs(pg_ref, pg_tgt, pg_catalog.get_domain_types):
        if ref and not tgt:
            action(pg_tgt, ddlgen.create_domain_type(
                qualname, ref.domaintype, collname=ref.collname, ddefaultexp=ref.ddefaultexp, typnotnull=ref.typnotnull,
                domainconstraints=ref.domainconstraints))
        if ref.rolname != (tgt.rolname if tgt else None):
            action(pg_tgt, ddlgen.alter_domain_owner_to(qualname, ref.rolname))
        if tgt:
            tgt_constraints = constraintdict(tgt.domainconstraints)
            for conname, connexp in ref.domainconstraints or []:
                if connexp != tgt_constraints.get(conname):
                    if conname in tgt_constraints:
                        action(pg_tgt, ddlgen.alter_domain_drop_constraint(qualname, conname))
                    action(pg_tgt, ddlgen.alter_domain_add_constraint(qualname, conname, connexp))
            # don't drop missing domain constraint; it might have been added in another package we haven't gotten to yet
            # user can create a revision if they want to drop a domain constraint
            # ref_constraints = constraintdict(ref.domainconstraints)
            # for conname, connexp in tgt.domainconstraints or []:
            #     if conname not in ref_constraints:
            #         action(pg_tgt, ddlgen.alter_domain_drop_constraint(qualname, conname))
            if tgt.ddefaultexp != ref.ddefaultexp:
                action(pg_tgt, ddlgen.alter_domain_set_default(qualname, ref.ddefaultexp))
            if tgt.typnotnull != ref.typnotnull:
                action(pg_tgt, ddlgen.alter_domain_set_notnull(qualname, ref.typnotnull))
        if ref.objcomment != (tgt.objcomment if tgt else None):
            action(pg_tgt, ddlgen.comment_generic(qualname, 'DOMAIN', ref.objcomment))

def add_functions(pg_ref, pg_tgt, action):
    ''' Add functions from reference database into a target database. '''
    for i, (qualname, ref, tgt) in enumerate(iter_diffs(pg_ref, pg_tgt, pg_catalog.get_functions)):
        pg_tgt.execute('SET check_function_bodies = false;') if i == 0 else 0
        if ref.functiondef != (tgt.functiondef if tgt else None):
            try:
                action(pg_tgt, ddlgen.create_function(qualname, ref.functiondef))
                if ref.extension_names:
                    for ext in ref.extension_names:
                        action(pg_tgt, ddlgen.alter_function_ext(qualname, ref.prokind, ext))
            except (psycopg2.errors.UndefinedFunction, psycopg2.errors.UndefinedObject)  as e: # pylint: disable=no-member
                slog.warn2('Function undefined: %s', e)
        if tgt and tgt.extension_names != ref.extension_names:
            tgt_ref_ext_diff = [x for x in (tgt.extension_names or []) if x not in (ref.extension_names or [])]
            ref_tgt_ext_diff = [x for x in (ref.extension_names or []) if x not in (tgt.extension_names or [])]
            for ext in tgt_ref_ext_diff:
                action(pg_tgt, ddlgen.alter_function_ext(qualname, ref.prokind, ext, yes=False))
            for ext in ref_tgt_ext_diff:
                action(pg_tgt, ddlgen.alter_function_ext(qualname, ref.prokind, ext))
            # action(pg_tgt, ddlgen.create_function(qualname, ref.functiondef))
        if ref.rolname != (tgt.rolname if tgt else None):
            action(pg_tgt, ddlgen.alter_function_owner_to(qualname, ref.prokind, ref.rolname))
        if ref.objcomment != (tgt.objcomment if tgt else None):
            action(pg_tgt, ddlgen.comment_function(qualname, ref.prokind, ref.objcomment))

def add_composite_attributes(pg_ref, pg_tgt, action):
    ''' Add composite type fields from reference database into a target database. '''
    for qualname, ref, tgt in iter_diffs(pg_ref, pg_tgt, pg_catalog.get_composite_type_attributes):
        if ref and not tgt:
            action(pg_tgt, ddlgen.alter_composite_type_add_attribute(qualname, ref.typname, ref.collname))
        if tgt and (ref.typname, ref.collname) != (tgt.typname, tgt.collname):
            action(pg_tgt, ddlgen.alter_composite_type_alter_attribute(qualname, ref.typname, ref.collname))
        if ref.objcomment != (tgt.objcomment if tgt else None):
            action(pg_tgt, ddlgen.comment_generic(qualname, 'COLUMN', ref.objcomment))

def add_columns(pg_ref, pg_tgt, action):
    ''' Add columns from reference database into a target database. '''
    def _attrs_type(obj):
        return (obj.typname, obj.collname)
    def _attrs_attdef(obj):
        return (obj.attidentity, obj.attgenerated, obj.atthasdef, obj.attdef)
    for qualname, ref, tgt in iter_diffs(pg_ref, pg_tgt, pg_catalog.get_columns):
        if ref and not tgt:
            action(pg_tgt, ddlgen.create_column(
                qualname, ref.typname, collname=ref.collname, attnotnull=ref.attnotnull, attidentity=ref.attidentity,
                attgenerated=ref.attgenerated, atthasdef=ref.atthasdef, attdef=ref.attdef))
        elif _attrs_type(ref) != _attrs_type(tgt):
            action(pg_tgt, ddlgen.alter_column_type(qualname, ref.typname, collname=ref.collname))
        if tgt and _attrs_attdef(ref) != _attrs_attdef(tgt):
            if tgt.attgenerated == 's' and ref.attgenerated == '':
                action(pg_tgt, ddlgen.alter_column_drop_expression(
                    qualname, attidentity=ref.attidentity, attgenerated=ref.attgenerated, atthasdef=ref.atthasdef))
            if ref.attgenerated == '' and ref.attidentity == '' and ref.attdef != tgt.attdef:
                action(pg_tgt, ddlgen.alter_column_default(
                    qualname, attidentity=ref.attidentity, attgenerated=ref.attgenerated, atthasdef=ref.atthasdef,
                    attdef=ref.attdef))
            if ref.attgenerated == '' and ref.attidentity and ref.attidentity != tgt.attidentity:
                if tgt and ref.attnotnull != tgt.attnotnull:  # null constraint is precondition for identity columns
                    action(pg_tgt, ddlgen.alter_column_null(qualname, ref.attnotnull))
                action(pg_tgt, ddlgen.alter_column_generated_identity(
                    qualname, attidentity=ref.attidentity, attgenerated=ref.attgenerated, atthasdef=ref.atthasdef,
                    from_attidentity=tgt.attidentity))
            if ref.attgenerated == 's' and ref.attdef != tgt.attdef:
                raise ImpossibleChange(
                    qualname, 'Unable to change column generated stored expression', bef=tgt.attdef, aft=ref.attdef)
            if ref.attidentity == '' and tgt.attidentity:
                action(pg_tgt, ddlgen.alter_column_drop_identity(qualname))
        if tgt and ref.attnotnull != tgt.attnotnull:
            action(pg_tgt, ddlgen.alter_column_null(qualname, ref.attnotnull))
        if ref.attstattarget != (tgt.attstattarget if tgt else -1) and ref.attstattarget:
            action(pg_tgt, ddlgen.alter_column_set_statistics(qualname, ref.attstattarget))
        if not tgt or ref.attoptions != tgt.attoptions:
            missing_opts = ddlgen.optnames(tgt.attoptions if tgt else []) - ddlgen.optnames(ref.attoptions)
            if missing_opts:
                action(pg_tgt, ddlgen.alter_column_reset_attributes(qualname, ref.attoptions, tgt.attoptions))
            if ref.attoptions:
                action(pg_tgt, ddlgen.alter_column_set_attributes(qualname, ref.attoptions))
        if not tgt or ref.attstorage != tgt.attstorage:
            action(pg_tgt, ddlgen.alter_column_set_storage(qualname, ref.attstorage))
        if ref.attcompression != (tgt.attcompression if tgt else ' '):
            action(pg_tgt, ddlgen.alter_column_set_compression(qualname, ref.attcompression))
        if ref.objcomment != (tgt.objcomment if tgt else None):
            action(pg_tgt, ddlgen.comment_generic(qualname, 'COLUMN', ref.objcomment))

def rem_invalid_indexes(pg_ref, pg_tgt, action, allow_commit=True):  # noqa: ARG001
    ''' Remove temporary CONCURRENTLY indexes that failed to build. '''
    def get_invalid_indexes():
        return pg_tgt.execute('''
            SELECT array[n.nspname, c.relname] AS qualname
            FROM pg_index i
            JOIN pg_class c ON c.oid = i.indexrelid
            JOIN pg_catalog.pg_namespace n ON n.oid = c.relnamespace
            WHERE i.indisvalid IS false
            AND i.indislive IS true -- false indicates it's already being dropped
            AND c.relname ~ 'scmtmp_\\d+'
            AND NOT EXISTS (  -- check that another process isn't presently building it
                SELECT 1 FROM pg_locks l WHERE l.relation = c.oid)
            ORDER BY c.oid;
        ''').all()
    for i in get_invalid_indexes():
        action(pg_tgt, ddlgen.drop_index(i.qualname))
        pg_tgt.commit() if allow_commit and not pg_tgt.conn.autocommit else 0

def add_indexes(pg_ref, pg_tgt, action, allow_commit=True):
    ''' Add indexes from reference database into a target database. '''
    for qualname, ref, tgt in iter_diffs(pg_ref, pg_tgt, pg_catalog.get_indexes):
        (namespace, objname) = qualname
        if not tgt:
            (tablenamespace, tablename) = ref.idxtable
            concurrently = allow_commit and should_minimize_access_exclusive_lock(pg_tgt, tablenamespace, tablename)
            autocommit = pg_tgt.conn.autocommit
            if concurrently and not autocommit:
                pg_tgt.commit()
                pg_tgt.conn.set_session(autocommit=True)
            action(pg_tgt, ddlgen.create_index(qualname, ref.indexdef, concurrently=concurrently))
            if concurrently and not autocommit:
                pg_tgt.commit()
                pg_tgt.conn.set_session(autocommit=False)
        if tgt and ref.reloptions != tgt.reloptions:
            missing_opts = ddlgen.optnames(tgt.reloptions or []) - ddlgen.optnames(ref.reloptions or [])
            if missing_opts:
                action(pg_tgt, ddlgen.alter_index_reset_attributes(qualname, ref.reloptions, tgt.reloptions))
            if ref.reloptions:
                action(pg_tgt, ddlgen.alter_index_set_reloptions(qualname, ref.reloptions))
        tgt_colstats = dict((tgt.idxcolstats if tgt else None) or [])
        for atnum, stattarget in ref.idxcolstats or []:
            if stattarget != tgt_colstats.get(atnum):
                action(pg_tgt, ddlgen.alter_index_set_statistics(qualname, atnum, stattarget))
        ref_colstats = dict(ref.idxcolstats or [])
        for atnum in tgt_colstats or []:
            if atnum not in ref_colstats:
                action(pg_tgt, ddlgen.alter_index_set_statistics(qualname, atnum, -1))
        if tgt and ref.spcname and ref.spcname != tgt.spcname:
            action(pg_tgt, ddlgen.alter_index_tablespace(qualname, ref.spcname))

        if ref.extension_names != (tgt.extension_names if tgt else None):
            ref_exts = set(ref.extension_names) if ref.extension_names else set()
            tgt_exts = set(tgt.extension_names) if tgt and tgt.extension_names else set()
            for x in tgt_exts - ref_exts:
                action(pg_tgt, ddlgen.alter_index_ext(qualname, x, yes=False))
            for x in ref_exts - tgt_exts:
                action(pg_tgt, ddlgen.alter_index_ext(qualname, x))
        if ref.objcomment != (tgt.objcomment if tgt else None):
            action(pg_tgt, ddlgen.comment_generic(qualname, 'INDEX', ref.objcomment))
        if tgt:
            has_dependencies = tgt.indisprimary or tgt.indisreplident or bool(pg_tgt.execute('''
                SELECT true
                FROM pg_index i
                JOIN pg_class c ON c.oid = i.indexrelid AND c.relname = %(indexname)s
                JOIN pg_catalog.pg_namespace n ON n.oid = c.relnamespace AND n.nspname = %(namespace)s
                JOIN pg_catalog.pg_depend d ON d.refobjid = i.indexrelid AND d.deptype != 'i'
                LIMIT 1;
            ''', {'indexname': objname, 'namespace': namespace}).first())
            tgt_cols = [c for (c, _, __) in tgt.idxcols]
            ref_cols = [c for (c, _, __) in ref.idxcols]
            if has_dependencies:
                # don't attempt to replace an index if any objects depend on it
                if tgt.idxpartialexp != ref.idxpartialexp:
                    raise ImpossibleChange(
                        qualname, 'Unable to change index WHERE clause', bef=tgt.idxpartialexp, aft=ref.idxpartialexp)
                if tgt.idxexpressions != ref.idxexpressions:
                    raise ImpossibleChange(
                        qualname, 'Unable to change index expressions', bef=tgt.idxexpressions, aft=ref.idxexpressions)
                if tgt_cols != ref_cols:
                    raise ImpossibleChange(qualname, 'Unable to change index fields', bef=tgt_cols, aft=ref_cols)
            replace_idx = (
                (tgt.idxpartialexp, tgt.idxexpressions, tgt_cols) !=
                (ref.idxpartialexp, ref.idxexpressions, ref_cols))
            if replace_idx:
                # The definition of the index changed, so let's replace it in a way that minimizes downtime (when
                # allow_commit = True, otherwise we have to hold locks during index build). We do this by creating a
                # new index using CONCURRENTLY with a temp name, then we drop the old index, then we rename the new
                # index in place.
                autocommit = pg_tgt.conn.autocommit
                if allow_commit and not autocommit:
                    pg_tgt.commit()
                    pg_tgt.conn.set_session(autocommit=True)
                tempname = f'scmtmp_{random.randint(1, 2**64)}'
                action(pg_tgt, ddlgen.create_index(
                    qualname, ref.indexdef, rename=tempname, concurrently=allow_commit))
                isvalid = pg_tgt.execute('''
                    SELECT indisvalid
                    FROM pg_index i
                    JOIN pg_class c ON c.oid = i.indexrelid AND c.relname = %(indexname)s
                    JOIN pg_catalog.pg_namespace n ON n.oid = c.relnamespace AND n.nspname = %(namespace)s;
                ''', {'indexname': objname, 'namespace': namespace}).scalar()
                if not isvalid:
                    action(pg_tgt, ddlgen.drop_index((namespace, tempname)))
                    raise CreateIndexFailure(qualname, 'failed to replace index, please try again', tgt, ref)
                if allow_commit and not autocommit:
                    pg_tgt.commit()
                    pg_tgt.conn.set_session(autocommit=False)
                action(pg_tgt, ddlgen.drop_index(qualname))
                action(pg_tgt, ddlgen.rename_index((namespace, tempname), objname))
                if allow_commit and not autocommit:
                    pg_tgt.commit()

def list_starts_with(prefix, lst):
    '''
    >>> assert list_starts_with([1, 2, 3], [1, 2, 3, 4])
    >>> assert not list_starts_with([1, 2, 3], [2, 3, 4, 5])
    '''
    return prefix == lst[:len(prefix)]

def add_view_stubs(pg_ref, pg_tgt, action):
    ''' Add views stubs with empty definition. This lets other objects create dependencies on them, making it easier
        to handle cyclic dependencies or complicated dependency graphs.
    '''
    for qualname, ref, tgt in iter_diffs(pg_ref, pg_tgt, pg_catalog.get_views):
        if not tgt:
            action(pg_tgt, ddlgen.create_view(qualname, False, False, 'SELECT', ref.reloptions))

def add_views(pg_ref, pg_tgt, action):
    ''' Add views from reference database into a target database.'''
    for qualname, ref, tgt in iter_diffs(pg_ref, pg_tgt, pg_catalog.get_views):
        ref_recursive = re.search(r'(?i)with\s+recursive|recursive\s+view', ref.viewdef) is not None
        ref_temp = ref.relpersistence == 't'
        if not (tgt or ref_temp):
            action(pg_tgt, ddlgen.create_view(qualname, False, ref_recursive, ref.viewdef, ref.reloptions))
        if tgt and ref.viewdef != tgt.viewdef:
            # can replace a view in-place if the column names and types match, allowing appending new columns
            if list_starts_with(tgt.viewcolumns or [], ref.viewcolumns or []):
                action(pg_tgt, ddlgen.create_view(qualname, False, ref_recursive, ref.viewdef, ref.reloptions))
            else:
                # When we cannot simply replace, we drop + create.
                # Raise an error if there's dependent objects instead of cascading the drop
                # This is because dropping other objects would at least cause brief errors, and at worst can cause
                # unintentional loss of objects.
                # If the user insists on doing a CASCADE, they can create a revision to do this dangerous action.
                action(pg_tgt, ddlgen.drop_view(qualname, cascade=False))
                action(pg_tgt, ddlgen.create_view(qualname, False, ref_recursive, ref.viewdef, ref.reloptions))
        ref_default_cols = {v[0]: v[3] for v in ref.viewcolumns if v[2] == 'true'}
        for k ,v in ref_default_cols.items():
            action(pg_tgt, ddlgen.alter_view_column(qualname, k, True, v))
        ref_not_default_cols = [v[0] for v in ref.viewcolumns or [] if v[0] not in ref_default_cols]
        tgt_def_col_names = [v[0] for v in tgt.viewcolumns or [] if v[2] == 'true'] if tgt else []
        for k in ref_not_default_cols:
            if k in tgt_def_col_names:
                action(pg_tgt, ddlgen.alter_view_column(qualname, k, False))
        if tgt and ref.reloptions != tgt.reloptions:
            missing_opts = ddlgen.optnames(tgt.reloptions or []) - ddlgen.optnames(ref.reloptions or [])
            if missing_opts:
                action(pg_tgt, ddlgen.alter_view_reset_attributes(qualname, ref.reloptions, tgt.reloptions))
            if ref.reloptions:
                action(pg_tgt, ddlgen.alter_view_set_reloptions(qualname, ref.reloptions))
        if ref.objcomment != (tgt.objcomment if tgt else None):
            action(pg_tgt, ddlgen.comment_generic(qualname, 'VIEW', ref.objcomment))
        if tgt and ref.rolname != tgt.rolname:
            action(pg_tgt, ddlgen.alter_view_owner(qualname, ref.rolname))

def add_triggers(pg_ref, pg_tgt, action):
    '''Add triggers from reference database to target database'''
    for qualname, ref, tgt in iter_diffs(pg_ref, pg_tgt, pg_catalog.get_triggers):
        if ref and not tgt:
            action(pg_tgt, ddlgen.create_trigger(qualname, ref.triggerdef))
        if tgt:
            if ref.triggerfuncdef != tgt.triggerfuncdef:
                action(pg_tgt, ddlgen.drop_trigger(qualname))
                action(pg_tgt, ddlgen.create_trigger(qualname, ref.triggerdef))
                # dropping trigger invalidates old attributes
                tgt = pg_catalog.get_triggers(pg_tgt).get(qualname) # noqa: PLW2901
            ref_extension_names = ref.extension_names or []
            tgt_extension_names = tgt.extension_names or []
            for ext in [x for x in ref_extension_names if x not in tgt_extension_names]:
                action(pg_tgt, ddlgen.alter_trigger(qualname, True, ext))
            for ext in [x for x in tgt_extension_names if x not in ref_extension_names]:
                action(pg_tgt, ddlgen.alter_trigger(qualname, False, ext))
        if ref.tgenabled != (tgt.tgenabled if tgt else 'O'):
            action(pg_tgt, ddlgen.alter_table_trigger(qualname[:2], qualname[2], ref.tgenabled))
        if ref.objcomment != (tgt.objcomment if tgt else None):
            action(pg_tgt, ddlgen.comment_trigger(qualname, ref.objcomment))

def add_extensions(pg_ref, pg_tgt, action):
    '''
    Add extensions from reference database to target database
    NOTE: It is generally better to update extension objects using update scripts.
    Reasons stated here: https://www.postgresql.org/docs/current/extend-extensions.html
    '''
    # Comenting out the initial implementation because I think it treats member objects extensions
    # in a different light than it should be
    # https://www.postgresql.org/docs/current/sql-alterextension.html says the following about adding objects:
    # > This form adds an existing object to the extension. This is mainly useful in extension update scripts.
    # > The object will subsequently be treated as a member of the extension; notably, it can only be dropped by dropping the extension.

    # for qualname, ref, tgt in iter_diffs(pg_ref, pg_tgt, pg_catalog.get_extensions):
    #     (extname,) = qualname
    #     if ref and not tgt:
    #         action(pg_tgt, ddlgen.create_extension(extname, ref.nspname, ref.version, False))
    #     if tgt and ref.version != tgt.version:
    #         action(pg_tgt, ddlgen.alter_extension_update(extname, ref.version))
    #         tgt = pg_catalog.get_extensions(pg_tgt).get(qualname)  # upgrading invalidates old attributes
    #     if tgt and ref.members != tgt.members:
    #         ref_members = ref.members or {}
    #         tgt_members = tgt.members if tgt and tgt.members else {}
    #         for k, ref_arrays in ref_members.items():
    #             if k == 'TRANSFORM' and ref_arrays != tgt_members.get(k):
    #                 ref_transforms = ref_arrays or {}
    #                 tgt_transforms = tgt_members.get(k) or {}
    #                 for lang, types in tgt_transforms.items():
    #                     ref_lang_transforms = ref_transforms.get(lang) or []
    #                     tgt_ref_types_diff = set(types or []) - set(ref_lang_transforms)
    #                     for ty in tgt_ref_types_diff:
    #                         action(pg_tgt, ddlgen.alter_extension_drop_object(extname, k, ty, lang))
    #                 for lang, types in ref_transforms.items():
    #                     tgt_lang_transforms = tgt_transforms.get(lang) or []
    #                     ref_tgt_types_diff_ = set(types or []) - set(tgt_lang_transforms)
    #                     for ty in ref_tgt_types_diff_:
    #                         action(pg_tgt, ddlgen.alter_extension_add_object(extname, k, ty, lang))
    #                 continue
    #             tgt_arrays = tgt_members.get(k) or []
    #             if ref_arrays != tgt_arrays:
    #                 for arr in tgt_arrays or []:
    #                     if arr not in (ref_arrays or []):
    #                         if k in ('CAST',):
    #                             action(pg_tgt, ddlgen.alter_extension_add_object(extname, k, arr[0], arr[1]))
    #                         elif k in ('OPERATOR',):
    #                             action(pg_tgt, ddlgen.alter_extension_drop_object(
    #                                 extname, k, arr[:2], arr[2:4], arr[4:6]))
    #                         elif k in ('OPERATOR CLASS', 'OPERATOR FAMILY', 'AGGREGATE', 'FUNCTION', 'PROCEDURE'):
    #                             action(pg_tgt, ddlgen.alter_extension_drop_object(extname, k, arr[:2], arr[2]))
    #                         # skipping functions because they get added in `add_functions`
    #                         elif k in ('FUNCTION', 'PROCEDURE'):
    #                             continue
    #                         else:
    #                             action(pg_tgt, ddlgen.alter_extension_drop_object(extname, k, arr))
    #                 for arr in ref_arrays or []:
    #                     if arr not in tgt_arrays:
    #                         if k in ('CAST',):
    #                             action(pg_tgt, ddlgen.alter_extension_add_object(extname, k, arr[0], arr[1]))
    #                         elif k in ('OPERATOR',):
    #                             action(pg_tgt, ddlgen.alter_extension_add_object(
    #                                     extname, k, arr[:2], arr[2:4], arr[4:6]))
    #                         elif k in ('OPERATOR CLASS', 'OPERATOR FAMILY', 'AGGREGATE', 'FUNCTION', 'PROCEDURE'):
    #                             action(pg_tgt, ddlgen.alter_extension_add_object(extname, k, arr[:2], arr[2]))
    #                         # skipping functions because they get added in `add_functions`
    #                         elif k in ('FUNCTION', 'PROCEDURE'):
    #                             continue
    #                         else:
    #                             action(pg_tgt, ddlgen.alter_extension_add_object(extname, k, arr))
    #     if tgt and ref.shared_members != tgt.shared_members:
    #         ref_shared_members = ref.shared_members or {}
    #         tgt_shared_members = tgt.shared_members if tgt and tgt.shared_members else {}
    #         for k, ref_arrays in ref_shared_members.items():
    #             tgt_arrays = tgt_shared_members.get(k) or []
    #             if ref_arrays != tgt_arrays:
    #                 for arr in tgt_arrays or []:
    #                     if arr not in (ref_arrays or []):
    #                         q = arr[0]
    #                         action(pg_tgt, ddlgen.alter_extension_drop_object(extname, k, q))
    #                 for arr in ref_arrays or []:
    #                     if arr not in tgt_arrays:
    #                         q = arr[0]
    #                         action(pg_tgt, ddlgen.alter_extension_add_object(extname, k, q))
    #     if tgt and ref.nspname != tgt.nspname:
    #         if not tgt.extrelocatable:
    #             raise ImpossibleChange(
    #                 extname, 'This extension is not relocatable to another schema', bef=tgt.nspname, aft=ref.nspname)
    #         action(pg_tgt, ddlgen.alter_extension_schema(extname, ref.nspname))
    # ref_exts = pg_catalog.get_installable_extensions(pg_ref)
    # import pudb; pudb.set_trace()
    tgt_installable_exts = pg_catalog.get_installable_extensions(pg_tgt)
    ref_installed_exts = pg_catalog.get_installed_extensions(pg_ref)
    tgt_installed_exts = pg_catalog.get_installed_extensions(pg_tgt)
    for qualname, ref in ref_installed_exts.items():
        (extname,) = qualname
        tgt = tgt_installed_exts.get(qualname)
        if ref and not tgt:
            tgt_ext = tgt_installable_exts.get(qualname)
            if tgt_ext: # extension is installable in this db
                # use the defaut extension version in the db if it isn't equal to the version in the reference db
                ext_version = ref.version if tgt_ext.version == ref.version else None
                action(pg_tgt, ddlgen.create_extension(extname, ref.nspname, ext_version, False))
            else:
                slog.warn2('Extension %s is not available for installation in the Postgres installation', extname)
        if tgt and ref.version != tgt.version:
            slog.info2('We avoid changing versions because of ')
            # action(pg_tgt, ddlgen.alter_extension_update(extname, ref.version))

def parse_options(reloptions):
    '''Takes a list of `=` separated key-values and returns a dict of key:value'''
    if reloptions:
        view_options = {}
        for o in reloptions:
            key, value = o.split('=')
            view_options[key] = value
        return view_options

def add_roles(pg_ref, pg_tgt, action):
    '''Add roles/users/groups from reference database to target database'''
    for qualname, ref, tgt in iter_diffs(pg_ref, pg_tgt, pg_catalog.get_roles):
        (rolname,) = qualname
        if ref and not tgt:
            action(pg_tgt, ddlgen.create_role(rolname, ref.options))
        if tgt and ref.options != tgt.options:
            action(pg_tgt, ddlgen.alter_role(rolname, ref.options))
        if tgt and ref.rolmembers != tgt.rolmembers:
            ref_rol_members = set(ref.rolmembers or [])
            tgt_rol_members = set(tgt.rolmembers or [])
            for m in ref_rol_members - tgt_rol_members:
                action(pg_tgt, ddlgen.grant_role_membership(rolname, m))
            for m in tgt_rol_members - ref_rol_members:
                action(pg_tgt, ddlgen.revoke_role_membership(rolname, m))
        if tgt and ref.all_configs != tgt.all_configs:
            ref_all_configs = parse_options(ref.all_configs) or {}
            tgt_all_configs = parse_options(tgt.all_configs) or {}
            config_diff_tr = [k for k in tgt_all_configs if k not in ref_all_configs]
            for k, v in ref_all_configs.items():
                action(pg_tgt, ddlgen.alter_role_default_config(rolname, k, v))
            for k in config_diff_tr:
                action(pg_tgt, ddlgen.alter_role_default_config(rolname, k))
        if tgt and ref.db_configs != tgt.db_configs:
            ref_db_configs = ref.db_configs or []
            tgt_db_configs = tgt.db_configs or []
            db_configs_ref = {}
            db_configs_tgt = {}
            for red_db_config in ref_db_configs:
                for db, defaults in red_db_config.items():
                    db_configs_ref[db] = {}
                    default_values = parse_options(defaults)
                    for k, v in default_values.items():
                        db_configs_ref[db][k] = v
            for tgt_db_config in tgt_db_configs:
                for db, defaults in tgt_db_config.items():
                    db_configs_tgt[db] = []
                    default_values = parse_options(defaults)
                    for k in default_values:
                        db_configs_tgt[db].append(k)
            for db, keys in db_configs_tgt.items():
                for k in keys:
                    if not db_configs_ref.get(db) or k not in (db_configs_ref.get(db) or []):
                        action(pg_tgt, ddlgen.alter_role_default_config(rolname, k, None, db))
            for db, kv in db_configs_ref.items():
                for k, v in kv.items():
                    action(pg_tgt, ddlgen.alter_role_default_config(rolname, k, v, db))
        if ref.objcomment != (tgt.objcomment if tgt else None):
            action(pg_tgt, ddlgen.comment_generic(qualname, 'ROLE', ref.objcomment))

def add_casts(pg_ref, pg_tgt, action):
    '''Add casts from reference database to target database'''
    for _, ref, tgt in iter_diffs(pg_ref, pg_tgt, pg_catalog.get_casts):
        if tgt:
            action(pg_tgt, ddlgen.drop_cast(tgt.source_type, tgt.tgt_type))
        action(pg_tgt, ddlgen.create_cast(
            ref.source_type, ref.tgt_type, ref.castmethod, ref.castcontext, funcname=ref.funcname,
            funcargs=ref.funcargs))
        if ref.objcomment:
            action(pg_tgt, ddlgen.comment_cast(ref.source_type, ref.tgt_type, ref.objcomment))
        if ref.objcomment != (tgt.objcomment if tgt else None):
            action(pg_tgt, ddlgen.comment_cast(ref.source_type, ref.tgt_type, ref.objcomment))

def add_evt_trg(pg_ref, pg_tgt, action):
    '''Add event triggers from reference database to target database'''
    for qualname, ref, tgt in iter_diffs(pg_ref, pg_tgt, pg_catalog.get_event_trgs):
        if ref and not tgt:
            action(pg_tgt, ddlgen.create_event_trigger(qualname, ref.evtevent, tuple(ref.funcname), ref.functype))
        if ref.objcomment != (tgt.objcomment if tgt else None):
            action(pg_tgt, ddlgen.comment_generic((qualname,), 'EVENT TRIGGER', ref.objcomment))
        if tgt:
            if ref.evtenabled == 'D' and tgt.evtenabled != 'D':
                action(pg_tgt, ddlgen.alter_event_trigger_able(qualname))
            if ref.evtenabled == 'A' and tgt.evtenabled != 'A':
                action(pg_tgt, ddlgen.alter_event_trigger_able(qualname, True, 'ALWAYS'))
            if ref.evtenabled == 'R' and tgt.evtenabled != 'R':
                action(pg_tgt, ddlgen.alter_event_trigger_able(qualname, True, 'REPLICA'))
            if tgt.rolname != ref.rolname:
                action(pg_tgt, ddlgen.alter_event_trigger_owner(qualname, ref.rolname))

def add_aggregates(pg_ref, pg_tgt, action):
    '''Add aggregates from reference database to target database'''
    for qualname, ref, tgt in iter_diffs(pg_ref, pg_tgt, pg_catalog.get_aggregates):
        if ref and not tgt:
            action(pg_tgt, ddlgen.create_aggregate(
                qualname,
                ref.argtype,
                ref.aggtransfn,
                ref.aggtranstype,
                ref.proparallel,
                ref.opts))
        if tgt:
            if ref.objcomment != (tgt.objcomment if tgt else None):
                action(pg_tgt, ddlgen.comment_aggregate(qualname, ref.argtype, ref.objcomment))
            if ref.owner != tgt.owner:
                action(pg_tgt, ddlgen.alter_aggregate_owner(qualname, ref.argtype, ref.owner))

def add_databases(pg_ref, pg_tgt, action, allow_commit=True):
    '''Add databases from reference cluster to target'''
    autocommit = pg_tgt.conn.autocommit
    for qualname, ref, tgt in iter_diffs(pg_ref, pg_tgt, pg_catalog.get_dbs):
        (datname,) = qualname
        if allow_commit and not autocommit:
            pg_tgt.commit()
            pg_tgt.conn.set_session(autocommit=True)
        if pg_tgt.conn.autocommit: # these ops require autocommit
            if not tgt:
                action(pg_tgt, ddlgen.create_db(datname, ref.opts))
            else:
                opt_keys = ('ALLOW_CONNECTIONS', 'CONNECTION LIMIT', 'IS_TEMPLATE')
                opts_diff = {}
                for k in opt_keys:
                    if ref.opts.get(k) != tgt.opts.get(k):
                        opts_diff[k] = ref.opts[k]
                action(pg_tgt, ddlgen.alter_db_option(datname, opts_diff)) if opts_diff else None
                if ref.opts.get('TABLESPACE') != tgt.opts.get('TABLESPACE'):
                    action(pg_tgt, ddlgen.alter_db_tablespace(datname, ref.opts.get('TABLESPACE')))
            if allow_commit and not autocommit:
                pg_tgt.commit()
                pg_tgt.conn.set_session(autocommit=False)
        # the only operations supported here that we can do without autocommit are `ALTER OWNER` and `COMMENT ON`
        if tgt and ref.objcomment != (tgt.objcomment if tgt else None):
            action(pg_tgt, ddlgen.comment_generic(qualname, 'DATABASE', ref.objcomment))
        if tgt and ref.opts.get('OWNER') != tgt.opts.get('OWNER'):
            action(pg_tgt, ddlgen.alter_db_owner(datname, ref.opts.get('OWNER')))

def add_publications(pg_ref, pg_tgt, action):
    '''Add publications from reference database to target database'''
    def get_publish(obj):
        ret = ''
        if obj.pubinsert:
            ret += 'insert, '
        if obj.pubupdate:
            ret += 'update, '
        if obj.pubdelete:
            ret += 'delete, '
        if obj.pubtruncate:
            ret += 'truncate'
        if ret.endswith(', '):
            ret = ret[:-2]
        return ret
    for qualname, ref, tgt in iter_diffs(pg_ref, pg_tgt, pg_catalog.get_publications):
        (pubname,) = qualname
        if ref and not tgt:
            if ref.puballtables:
                action(pg_tgt, ddlgen.create_publication_all_tables(pubname))
            elif ref.tables:
                action(pg_tgt, ddlgen.create_publication_tables(
                    pubname,
                    ref.tables,
                    get_publish(ref),
                    ref.pubviaroot))
                if ref.schemas:
                    action(pg_tgt, ddlgen.alter_pub_add_schemas(pubname, ref.schemas))
            elif ref.schemas:
                action(pg_tgt, ddlgen.create_publication_schemas(pubname, ref.schemas))
        if ref.objcomment != (tgt.objcomment if tgt else None):
            action(pg_tgt, ddlgen.comment_generic((pubname,), 'PUBLICATION', ref.objcomment))
        if tgt:
            if ref.tables != tgt.tables:
                ref_tables = {x['name'] for x in ref.tables} if ref.tables else set()
                tgt_tables = {x['name'] for x in tgt.tables} if tgt.tables else set()
                ref_tgt_diff = []
                for t in ref.tables:
                    if t['name'] not in tgt_tables:
                        ref_tgt_diff.append(t)
                        continue
                    for tgt_table in tgt.tables:
                        if tgt_table['name'] == t['name'] and (
                            tgt_table['cols'] != t['cols'] or tgt_table['qual'] != t['qual']):
                            ref_tgt_diff.append(t)  # noqa: PERF401
                tgt_ref_diff = []
                for t in tgt.tables:
                    if t['name'] not in ref_tables:
                        tgt_ref_diff.append(t)
                        continue
                    for ref_table in ref.tables:
                        if ref_table['name'] == t['name'] and (
                            ref_table['cols'] != t['cols'] or ref_table['qual'] != t['qual']):
                            tgt_ref_diff.append(t)  # noqa: PERF401
                action(pg_tgt, ddlgen.alter_pub_drop_obj(pubname, tgt_ref_diff)) if tgt_ref_diff else None
                action(pg_tgt, ddlgen.alter_pub_add_obj(pubname, ref_tgt_diff)) if ref_tgt_diff else None
            if ref.schemas != tgt.schemas:
                ref_tgt_schema_diff = [x for x in ref.schemas if x not in (tgt.schemas or [])]
                tgt_ref_schema_diff = [x for x in tgt.schemas if x not in (ref.schemas or [])]
                action(pg_tgt, ddlgen.alter_pub_drop_schemas(
                    pubname, tgt_ref_schema_diff)) if tgt_ref_schema_diff else None
                action(pg_tgt, ddlgen.alter_pub_add_schemas(
                    pubname, ref_tgt_schema_diff)) if ref_tgt_schema_diff else None
            set_params = {}
            ref_publish, tgt_publish = get_publish(ref), get_publish(tgt)
            if ref_publish != tgt_publish:
                set_params['publish'] = ref_publish
            if ref.pubviaroot != tgt.pubviaroot:
                set_params['publish_via_partition_root'] = ref.pubviaroot
            action(pg_tgt, ddlgen.alter_pub_param(pubname, set_params)) if set_params else None
            if ref.owner != tgt.owner:
                action(pg_tgt, ddlgen.alter_pub_owner(pubname, ref.owner))

def add_ts_dicts(pg_ref, pg_tgt, action):
    '''Add text search dictionaries from reference database to target database'''
    for qualname, ref, tgt in iter_diffs(pg_ref, pg_tgt, pg_catalog.get_ts_dicts):
        if ref and not tgt:
            action(pg_tgt, ddlgen.create_ts_dict(qualname, ref.tmplname, ref.dictinitoption))
        if ref.objcomment != (tgt.objcomment if tgt else None):
            action(pg_tgt, ddlgen.comment_generic(qualname, 'TEXT SEARCH DICTIONARY', ref.objcomment))
        if tgt:
            if ref.dictinitoption != tgt.dictinitoption and ref.dictinitoption:
                action(pg_tgt, ddlgen.alter_ts_dict_opts(qualname, ref.dictinitoption))
            if ref.rolname != tgt.rolname:
                action(pg_tgt, ddlgen.alter_ts_owner(qualname, ref.rolname, 'DICTIONARY'))

def add_ts_parsers(pg_ref, pg_tgt, action):
    '''Add text search parsers from reference database to target database'''
    for qualname, ref, tgt in iter_diffs(pg_ref, pg_tgt, pg_catalog.get_ts_parsers):
        if ref and not tgt:
            action(pg_tgt, ddlgen.create_ts_parser(
                qualname, ref.prsstart, ref.prstoken, ref.prsend, ref.prslextype, headline=ref.prsheadline))
        if ref.objcomment != (tgt.objcomment if tgt else None):
            action(pg_tgt, ddlgen.comment_generic(qualname, 'TEXT SEARCH PARSER', ref.objcomment))

def add_ts_templates(pg_ref, pg_tgt, action):
    '''Add text search templates from reference database to target database'''
    for qualname, ref, tgt in iter_diffs(pg_ref, pg_tgt, pg_catalog.get_ts_templates):
        if ref and not tgt:
            action(pg_tgt, ddlgen.create_ts_template(qualname, ref.tmpllexize, ref.tmplinit))
        if ref.objcomment != (tgt.objcomment if tgt else None):
            action(pg_tgt, ddlgen.comment_generic(qualname, 'TEXT SEARCH TEMPLATE', ref.objcomment))

def add_ts_configs(pg_ref, pg_tgt, action):
    '''Add text search configs from reference database to target database'''
    for qualname, ref, tgt in iter_diffs(pg_ref, pg_tgt, pg_catalog.get_ts_configs):
        if ref and not tgt:
            action(pg_tgt, ddlgen.create_ts_config(qualname, ref.parser))
            for k, v in (ref.mappings.items() or {}):
                action(pg_tgt, ddlgen.alter_ts_config_mapping(qualname, 'ADD MAPPING FOR', k, v))
        if ref.objcomment != (tgt.objcomment if tgt else None):
            action(pg_tgt, ddlgen.comment_generic(qualname, 'TEXT SEARCH CONFIGURATION', ref.objcomment))
        if tgt:
            tgt_mappings = tgt.mappings or {}
            if tgt.mappings != ref.mappings:
                tgt_ref_map_diff = [x for x in tgt_mappings if x not in ref.mappings]
                if tgt_ref_map_diff:
                    action(pg_tgt, ddlgen.alter_ts_config_drop_mapping(qualname, tgt_ref_map_diff))
                for k, v in ref.mappings.items():
                    if v != tgt_mappings.get(k):
                        action(pg_tgt, ddlgen.alter_ts_config_mapping(qualname, 'ALTER MAPPING FOR', k, v))
            if ref.rolname != tgt.rolname:
                action(pg_tgt, ddlgen.alter_ts_owner(qualname, ref.rolname, 'CONFIGURATION'))

def add_operators(pg_ref, pg_tgt, action):
    '''Add operators from reference database to target database'''
    for qualname, ref, tgt in iter_diffs(pg_ref, pg_tgt, pg_catalog.get_operators):
        if ref and not tgt:
            action(pg_tgt, ddlgen.create_operator(
                qualname[:2], ref.oprcode, ref.oprcanhash, ref.oprcanmerge, left_type=ref.oprleft,
                right_type=ref.oprright, com_op=ref.oprcom, neg_op=ref.oprnegate, res_proc=ref.oprrest,
                join_proc=ref.oprjoin))
        if ref.objcomment != (tgt.objcomment if tgt else None):
            action(pg_tgt, ddlgen.comment_operator(
                qualname[:2], ref.objcomment, tgt.oprright, left_type=tgt.oprleft))
        if tgt:
            if ref.oprrest != tgt.oprrest or ref.oprjoin != tgt.oprjoin:
                action(pg_tgt, ddlgen.alter_operator_procs(
                    qualname[:2], tgt.oprright, left_type=tgt.oprleft, res_proc=ref.oprrest, join_proc=ref.oprjoin))
            if ref.rolname != tgt.rolname:
                action(pg_tgt, ddlgen.alter_op_owner(qualname[:2], ref.rolname, tgt.oprright, left_type=tgt.oprleft))

def add_languages(pg_ref, pg_tgt, action):
    '''Add languages from reference database to target database'''
    for qualname, ref, tgt in iter_diffs(pg_ref, pg_tgt, pg_catalog.get_languages):
        (langname,) = qualname
        if ref and not tgt:
            action(pg_tgt, ddlgen.create_lang(
                langname, ref.lanpltrusted, callproc=ref.callproc, inlineproc=ref.inlineproc, valproc=ref.valproc))
        if ref.objcomment != (tgt.objcomment if tgt else None):
            action(pg_tgt, ddlgen.comment_generic(qualname, 'LANGUAGE', ref.objcomment))
        if tgt and ref.rolname != tgt.rolname:
            action(pg_tgt, ddlgen.alter_lang_owner(langname, ref.rolname))

def add_collations(pg_ref, pg_tgt, action):
    '''Add collations from reference database to target database'''
    for qualname, ref, tgt in iter_diffs(pg_ref, pg_tgt, pg_catalog.get_collations):
        if ref and not tgt:
            action(pg_tgt, ddlgen.create_collation(qualname, ref.opts))
        if ref.objcomment != (tgt.objcomment if tgt else None):
            action(pg_tgt, ddlgen.comment_generic(qualname, 'COLLATION', ref.objcomment))
        if tgt and ref.opts['VERSION'] != tgt.opts['VERSION']:
            action(pg_tgt, ddlgen.alter_collation_version(qualname))
        if tgt and ref.rolname != tgt.rolname:
            action(pg_tgt, ddlgen.alter_coll_owner(qualname, ref.rolname))
        if tgt and tgt.opts != ref.opts:
            tgt_opts = dict(tgt.opts or {})
            tgt_opts.pop('VERSION')
            ref_opts = dict(ref.opts or {})
            ref_opts.pop('VERSION')
            if ref_opts != tgt_opts:
                raise ImpossibleChange(qualname, 'Unable to change option for collation', bef=tgt_opts, aft=ref_opts)

def add_conversions(pg_ref, pg_tgt, action):
    '''Add conversions from reference database to target database'''
    for qualname, ref, tgt in iter_diffs(pg_ref, pg_tgt, pg_catalog.get_conversions):
        if ref and not tgt:
            action(pg_tgt, ddlgen.create_conversion(
                qualname, ref.condefault, ref.source_encoding, ref.dest_encoding, ref.function_name))
        if ref.objcomment != (tgt.objcomment if tgt else None):
            action(pg_tgt, ddlgen.comment_generic(qualname, 'CONVERSION', ref.objcomment))
        if tgt and ref.rolname != tgt.rolname:
            action(pg_tgt, ddlgen.alter_conversion_owner(qualname, ref.rolname))

def add_mat_views(pg_ref, pg_tgt, action):
    '''Add materialized views from reference database to target database'''
    for qualname, ref, tgt in iter_diffs(pg_ref, pg_tgt, pg_catalog.get_materialized_views):
        if ref and not tgt:
            action(pg_tgt, ddlgen.create_mat_view(
                qualname, ref.viewdef, ref.viewcolumns, ref.reloptions, ref.amname, ref.spcname, ref.relispopulated))
        if tgt and ref.viewdef != tgt.viewdef:
            action(pg_tgt, ddlgen.drop_mat_view(qualname, cascade=True))
            action(pg_tgt, ddlgen.create_mat_view(
                qualname, ref.viewdef, ref.viewcolumns, ref.reloptions, ref.amname, ref.spcname, ref.relispopulated)
            )
        if tgt and ref.reloptions != tgt.reloptions:
            missing_opts = ddlgen.optnames(tgt.reloptions or []) - ddlgen.optnames(ref.reloptions or [])
            add_opts = ddlgen.optnames(ref.reloptions or []) - ddlgen.optnames(tgt.reloptions or [])
            if missing_opts:
                action(pg_tgt, ddlgen.alter_mat_view_params(qualname, False, missing_opts))
            if add_opts:
                action(pg_tgt, ddlgen.alter_mat_view_params(qualname, True, add_opts))
        if tgt:
            tgt_view_columns = {x['attname']: x for x in tgt.viewcolumns} if tgt.viewcolumns else {}
            for ref_att in ref.viewcolumns:
                ref_att_name = ref_att['attname']
                ref_opts = ref_att['attoptions']
                tgt_att = tgt_view_columns.get(ref_att_name)
                if tgt_att:
                    missing_opts = ddlgen.optnames(tgt_att['attoptions'] or []) - ddlgen.optnames(ref_opts  or [])
                    action(
                        pg_tgt, ddlgen.alter_mat_view_col_params(
                            qualname, ref_att_name, False, missing_opts)) if missing_opts else None
                    if ref_opts:
                        action(pg_tgt, ddlgen.alter_mat_view_col_params(qualname, ref_att_name, True, ref_opts))
                    if ref_att['attstattarget'] != (tgt_att['attstattarget'] if tgt else -1):
                        action(pg_tgt, ddlgen.alter_mat_view_col_set_stats(
                                qualname, ref_att_name, ref_att['attstattarget']))
                    if ref_att['attcompression'] != tgt_att['attcompression']:
                        action(pg_tgt, ddlgen.alter_mat_view_col_set_compression(
                                qualname, ref_att_name, ref_att['attcompression']))
                    if ref_att['attstorage'] != tgt_att['attstorage']:
                        action(pg_tgt, ddlgen.alter_mat_view_col_set_storage(
                                qualname, ref_att_name, ref_att['attstorage']))
        if tgt and ref.cluster_index != tgt.cluster_index:
            if ref.cluster_index:
                action(pg_tgt, ddlgen.alter_mat_view_cluster_on(qualname, ref.cluster_index))
            else:
                action(pg_tgt, ddlgen.alter_mat_view_no_cluster(qualname))
        if tgt and ref.extension_names != tgt.extension_names:
            ref_extension_names = ref.extension_names or []
            tgt_extension_names = tgt.extension_names or []
            diff_rt = [x for x in ref_extension_names if x not in tgt_extension_names]
            diff_tr = [x for x in tgt_extension_names if x not in ref_extension_names]
            for ext in diff_tr:
                action(pg_tgt, ddlgen.alter_mat_view_ext(qualname, False, ext))
            for ext in diff_rt:
                action(pg_tgt, ddlgen.alter_mat_view_ext(qualname, True, ext))
        if tgt and tgt.amname != ref.amname:
            action(pg_tgt, ddlgen.alter_mat_view_am(qualname, ref.amname))
        if tgt and tgt.spcname != ref.spcname:
            action(pg_tgt, ddlgen.alter_mat_view_tablespace(qualname, ref.spcname))
        if ref.objcomment != (tgt.objcomment if tgt else None):
            action(pg_tgt, ddlgen.comment_generic(qualname, 'MATERIALIZED VIEW', ref.objcomment))
        if tgt and ref.rolname != tgt.rolname:
            action(pg_tgt, ddlgen.alter_mat_view_owner(qualname, ref.rolname))

def add_statistics(pg_ref, pg_tgt, action):
    '''Add statistics from reference database to target database'''
    for qualname, ref, tgt in iter_diffs(pg_ref, pg_tgt, pg_catalog.get_statistics):
        if ref and not tgt:
            action(pg_tgt, ddlgen.create_statistics(
                qualname, ref.tablequal, stxkind=ref.stxkind, cols=ref.cols, stxexprs=ref.stxexprs))
        if ref.objcomment != (tgt.objcomment if tgt else None):
            action(pg_tgt, ddlgen.comment_generic(qualname, 'STATISTICS', ref.objcomment))
        if tgt:
            if ref.stxstattarget != tgt.stxstattarget:
                action(pg_tgt, ddlgen.alter_statistics_target(qualname, ref.stxstattarget))
            if tgt and ref.rolname != tgt.rolname:
                action(pg_tgt, ddlgen.alter_owner_generic(qualname, 'STATISTICS', ref.rolname))

def add_rules(pg_ref, pg_tgt, action):
    '''Add rules from reference database to target database'''
    for qualname, ref, tgt in iter_diffs(pg_ref, pg_tgt, pg_catalog.get_rules):
        if ref and not tgt:
            action(pg_tgt, ddlgen.create_rule(qualname, ref.definition))
        if ref.objcomment != (tgt.objcomment if tgt else None):
            action(pg_tgt, ddlgen.comment_rule(qualname, ref.objcomment))

def add_access_methods(pg_ref, pg_tgt, action):
    '''Add access methods from reference database to target database'''
    for qualname, ref, tgt in iter_diffs(pg_ref, pg_tgt, pg_catalog.get_access_methods):
        (amname,) = qualname
        if ref and not tgt:
            action(pg_tgt, ddlgen.create_access_method(amname, ref.amhandler, ref.amtype))
        if ref.objcomment != (tgt.objcomment if tgt else None):
            action(pg_tgt, ddlgen.comment_generic(qualname, 'ACCESS METHOD', ref.objcomment))

def add_transforms(pg_ref, pg_tgt, action):
    '''Add transforms from reference database to target database'''
    for qualname, ref, tgt in iter_diffs(pg_ref, pg_tgt, pg_catalog.get_transforms):
        if not tgt:
            action(pg_tgt, ddlgen.create_transform(
                qualname,
                ref.from_sql_func,
                ref.to_sql_func))
        if ref.objcomment != (tgt.objcomment if tgt else None):
            action(pg_tgt, ddlgen.comment_transform(qualname, ref.objcomment))

def add_op_classes(pg_ref, pg_tgt, action):
    '''Add operator classes from reference database to target database'''
    for qualname, ref, tgt in iter_diffs(pg_ref, pg_tgt, pg_catalog.get_op_classes):
        if ref and not tgt:
            action(pg_tgt, ddlgen.create_op_class(
                qualname, ref.opcdefault, ref.opcintype, ref.amname, ops=ref.ops, procs=ref.procs,
                opcfamily=ref.opcfamily, opcstorage=ref.opcstorage))
        if ref.objcomment != (tgt.objcomment if tgt else None):
            action(pg_tgt, ddlgen.comment_op(qualname, 'CLASS', ref.amname, ref.objcomment))
        if tgt and ref.rolname != tgt.rolname:
            action(pg_tgt, ddlgen.alter_opclass_owner(qualname, ref.amname, ref.rolname))

def add_op_families(pg_ref, pg_tgt, action):
    '''Add operator families from reference database to target database'''
    for qualname, ref, tgt in iter_diffs(pg_ref, pg_tgt, pg_catalog.get_op_families):
        if ref and not tgt:
            action(pg_tgt, ddlgen.create_op_family(qualname, ref.amname))

def add_op_families_details(pg_ref, pg_tgt, action):
    '''
    We add operator families before creating operator classes, because classes usually belong to families.
    This function is needed to handle the case of free-standing operators and functions which do
    not belong to classes and were not added when classes were created
    '''
    for qualname, ref, tgt in iter_diffs(pg_ref, pg_tgt, pg_catalog.get_op_families):
        if tgt:
            tgt_ops = tgt.ops if tgt and tgt.ops else {}
            ref_ops = ref.ops or {}
            tgt_procs = tgt.procs if tgt and tgt.procs else {}
            ref_procs = ref.procs or {}
            if ref_ops != tgt_ops or ref_procs != tgt_procs:
                tgt_ref_ops_diff_ops = {k: v for k, v in tgt_ops.items() if k not in ref_ops}
                ref_tgt_ops_diff_ops = {k: v for k, v in ref_ops.items() if k not in tgt_ops}
                tgt_ref_procs_diff_procs = {k: v for k, v in tgt_procs.items() if k not in ref_procs}
                ref_tgt_procs_diff_procs = {k: v for k, v in ref_procs.items() if k not in tgt_procs}
                if tgt_ref_ops_diff_ops or tgt_ref_procs_diff_procs:
                    action(pg_tgt, ddlgen.alter_opfamily_drop(
                        qualname, ref.amname, ops=tgt_ref_ops_diff_ops, procs=tgt_ref_procs_diff_procs))
                if ref_tgt_ops_diff_ops or ref_tgt_procs_diff_procs:
                    action(pg_tgt, ddlgen.alter_opfamily_add(
                        qualname, ref.amname, ops=ref_tgt_ops_diff_ops, procs=ref_tgt_procs_diff_procs))
        if ref.objcomment != (tgt.objcomment if tgt else None):
            action(pg_tgt, ddlgen.comment_op(qualname, 'FAMILY', ref.amname, ref.objcomment))
        if ref.rolname != tgt.rolname:
            action(pg_tgt, ddlgen.alter_opfamily_owner(qualname, ref.amname, ref.rolname))

def add_foreign_servers(pg_ref, pg_tgt, action):
    '''Add foreign servers from reference database to target database'''
    for qualname, ref, tgt in iter_diffs(pg_ref, pg_tgt, pg_catalog.get_foreign_servers):
        (srvname,) = qualname
        if ref and not tgt:
            action(pg_tgt, ddlgen.create_foreign_server(
                srvname, ref.fdwname, ref.srvtype, ref.srvversion, ref.srvoptions))
        if ref.objcomment != (tgt.objcomment if tgt else None):
            action(pg_tgt, ddlgen.comment_generic(qualname, 'FOREIGN SERVER', ref.objcomment))
        if tgt:
            if ref.srvversion != tgt.srvversion or ref.srvoptions != tgt.srvoptions:
                new_version = ref.srvversion if ref.srvversion != tgt.srvversion else None
                opts = {}
                ref_opts = ddlgen.server_opts(ref.srvoptions)
                tgt_opts = ddlgen.server_opts(tgt.srvoptions)
                for k, v in ref_opts.items():
                    tgt_v = tgt_opts.get(k)
                    if not tgt_v:
                        opts[f'{k}={v}'] = 'add'
                    elif v != tgt_v:
                        opts[f'{k}={v}'] = 'set'
                for k, v in tgt_opts.items():
                    if k not in ref_opts:
                        opts[f'{k}={v}'] = 'drop'
                action(pg_tgt, ddlgen.alter_server_opts(srvname, new_version=new_version, opts=opts))
            if ref.rolname != tgt.rolname:
                action(pg_tgt, ddlgen.alter_server_owner(srvname, ref.rolname))

def add_foreign_data_wrappers(pg_ref, pg_tgt, action):
    '''Add foreign data wrappers from reference database to target database'''
    for qualname, ref, tgt in iter_diffs(pg_ref, pg_tgt, pg_catalog.get_foreign_data_wrappers):
        (fdwname,) = qualname
        if not tgt:
            action(pg_tgt, ddlgen.create_fdw(fdwname, ref.fdwhandler, ref.fdwvalidator, opts=ref.fdwoptions))
        if ref.objcomment != (tgt.objcomment if tgt else None):
            action(pg_tgt, ddlgen.comment_generic(qualname, 'FOREIGN DATA WRAPPER', ref.objcomment))
        if tgt:
            if ref.fdwoptions != tgt.fdwoptions:
                opts = {}
                ref_opts = ddlgen.server_opts(ref.fdwoptions)
                tgt_opts = ddlgen.server_opts(tgt.fdwoptions)
                for k, v in ref_opts.items():
                    tgt_v = tgt_opts.get(k)
                    if not tgt_v:
                        opts[f'{k}={v}'] = 'add'
                    elif v != tgt_v:
                        opts[f'{k}={v}'] = 'set'
                for k, v in tgt_opts.items():
                    if k not in ref_opts:
                        opts[f'{k}={v}'] = 'drop'
                action(pg_tgt,
                    ddlgen.alter_fdw_opts(
                        fdwname,
                        ref.fdwhandler if ref.fdwhandler != tgt.fdwhandler else None,
                        ref.fdwvalidator if ref.fdwvalidator != tgt.fdwvalidator else None,
                        opts=opts))
            if ref.rolname != tgt.rolname:
                action(pg_tgt, ddlgen.alter_fdw_owner(fdwname, ref.rolname))

def add_user_mappings(pg_ref, pg_tgt, action):
    '''Add user mappings from reference database to target database'''
    for qualname, ref, tgt in iter_diffs(pg_ref, pg_tgt, pg_catalog.get_user_mappings):
        (umuser, umserver) = qualname
        if not tgt:
            action(pg_tgt, ddlgen.create_user_mapping(umuser, umserver, ref.umoptions))
        if tgt and ref.umoptions != tgt.umoptions:
            opts = {}
            ref_opts = ddlgen.server_opts(ref.umoptions)
            tgt_opts = ddlgen.server_opts(tgt.umoptions)
            for k, v in ref_opts.items():
                tgt_v = tgt_opts.get(k)
                if not tgt_v:
                    opts[f'{k}={v}'] = 'add'
                elif v != tgt_v:
                    opts[f'{k}={v}'] = 'set'
            for k, v in tgt_opts.items():
                if k not in ref_opts:
                    opts[f'{k}={v}'] = 'drop'
            action(pg_tgt, ddlgen.alter_user_mapping(umuser, umserver, opts))

def add_policies(pg_ref, pg_tgt, action):
    '''Add policies from reference database to target database'''
    for qualname, ref, tgt in iter_diffs(pg_ref, pg_tgt, pg_catalog.get_policies):
        (polname,) = qualname
        if not tgt:
            action(pg_tgt, ddlgen.create_policy(
                polname, ref.tablename, ref.polpermissive, ref.polcmd, polroles=ref.polroles, polqual=ref.polqual,
                polwithcheck=ref.polwithcheck))
        if ref.objcomment != (tgt.objcomment if tgt else None):
            action(pg_tgt, ddlgen.comment_policy(polname, tgt.tablename, ref.objcomment))
        if tgt and any((
                ref.polroles != tgt.polroles, ref.polqual != tgt.polqual, ref.polwithcheck != tgt.polwithcheck)):
            action(pg_tgt, ddlgen.alter_policy_detail(
                polname, ref.tablename,
                polroles=ref.polroles if ref.polroles != tgt.polroles else None,
                polqual=ref.polqual if ref.polqual != tgt.polqual else None,
                polwithcheck=ref.polwithcheck if ref.polwithcheck != tgt.polwithcheck else None))
