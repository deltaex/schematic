'''
Utility functions for ingesting data into a postgresql database.

It offers functions for update, insert, upsert (update + insert together), delete, and truncate.

It's recommended to use upsert instead of insert because it supports merging when there's an existing record.

It's recommended to use upsert instead of update because it works when there's a missing record.

The PREPARE statement is used as an optimization that saves repetitive query parsing and planning, as well as
reducing query size over the wire. https://www.postgresql.org/docs/current/sql-prepare.html
'''
# pyright: strict
import base64
import hashlib
import math
import os
import re
import select
from collections import OrderedDict, namedtuple
from functools import wraps

import psycopg
from psycopg.sql import SQL, Identifier, Literal

from schematic import env, pglsn, slog

scm_var_path = env.get_str('SCM_VAR', os.path.expanduser('~/var'))
use_prepared_statement = env.get_bool('SCM_UPSERT_PREPARED_STATEMENT', default=True)
unique_error_flag_path = os.path.join(scm_var_path, 'schematic/.pgingest-conflicting-key')
unique_error_flag_path_exists = os.path.exists(unique_error_flag_path)


def commit_tsn(pg, tsn, tts):
    ''' Update replication origin up to :tsn: and commit. '''
    pg.execute('''SELECT pg_replication_origin_xact_setup(%s::pg_lsn,
                                                          %s::timestamp with time zone);''',
               (pglsn.tsn_to_lsn(tsn), tts))
    pg.commit()

def memoize(func):
    cache = {}
    @wraps(func)
    def memoized_func(*args, **kwargs):
        key = (args, frozenset(kwargs.items()))
        if key not in cache:
            cache[key] = func(*args, **kwargs)
        return cache[key]
    return memoized_func

@memoize
def placeholders(n: int) -> str:
    return '('+','.join(['%s']*n)+')'

def better_replication_origin(pg: psycopg.Connection) -> bool:
    args = pg.execute('''
        SELECT pg_get_function_arguments(p.oid) AS function_arguments
        FROM pg_proc p
        WHERE p.proname = 'pg_replication_origin_session_setup';
    ''').fetchall()
    print(args)
    return any((arg.function_arguments in {
                   'text, integer', 'node_name text, acquired_by integer DEFAULT 0'})
                for arg in args)

def init_replication_settings(pg, replication_role, subid=None, master_pid=0):
    ''' Configure a pg session for receiving a replication stream.
        :subid: when provided, setup replication origin and return the position
    '''
    # tables with self-referential foreign keys may cause constraint violation if rows are out of dependency order
    # this defers constraint validation until the end of the transaction
    # https://www.postgresql.org/docs/current/sql-set-constraints.html
    print('init_replication_settings', (replication_role, subid, master_pid))
    pg.execute('SET CONSTRAINTS ALL DEFERRED;')
    assert replication_role in ('replica', 'origin', 'local'), f'invalid replication_role {replication_role!r}'
    if replication_role != 'origin':
        pg.execute('SET session_replication_role = %s;', (replication_role,))
    pg.execute('SET statement_timeout = \'60min\'') # limit the delay caused by deadlocks
                                                    # from triggers or subscription conflicts
    pg.execute('SET plan_cache_mode = force_generic_plan;')  # reduce work with prepared statement execution
    # disabling synchronous_commit improves performance and is safe in this context because upon a postgresql crash
    # recent writes can be lost, but we'll just replay them anyway on the next run
    pg.execute('SET SESSION synchronous_commit TO OFF;')
    if subid:
        pg.execute('SELECT coalesce(pg_replication_origin_oid(%s), pg_replication_origin_create(%s));', (subid, subid))
        if master_pid == 0:
            pg.execute('SELECT pg_replication_origin_session_setup(%s);', (subid,))
        else:
            pg.execute('SELECT pg_replication_origin_session_setup(%s, %s);', (subid, master_pid))
        return pglsn.lsn_to_tsn(pg.execute('SELECT pg_replication_origin_session_progress(false);').fetchone()[0])

def get_column_defs(pg):
    allcols = pg.execute('''
        SELECT
            n.nspname AS namespace,
            t.relname AS tablename,
            c.attname AS colname,
            c.attnotnull AS notnull,
            c.attgenerated AS generated,
            c.attstorage IN ('e', 'x') AS toastable
        FROM pg_class t
        LEFT JOIN pg_namespace n ON n.oid = t.relnamespace
        LEFT JOIN pg_attribute c ON c.attrelid = t.oid
        WHERE c.attnum > 0
        ORDER BY 1, 2, c.attnum;
    ''').fetchall()
    ret = OrderedDict()
    for col in allcols:
        key = (col.namespace, col.tablename)
        ret[key] = ret.get(key, OrderedDict())
        ret[key][col.colname] = col
    return ret

@memoize
def get_keycols(pg, namespace, tablename):
    records = pg.execute(SQL('''
        SELECT a.attname AS name
        FROM pg_index i
        JOIN pg_attribute a ON a.attrelid = i.indrelid AND a.attnum = ANY(i.indkey)
        WHERE i.indisprimary
        AND i.indrelid = (
           SELECT c.oid
           FROM pg_namespace n
           JOIN pg_class c ON c.relnamespace = n.oid
           WHERE n.nspname = {namespace}
           AND c.relname = {tablename}
           LIMIT 1)
        ORDER BY array_position(i.indkey, a.attnum);
    ''').format(namespace=Literal(namespace), tablename=Literal(tablename))).fetchall()
    return [record.name for record in records]

def get_table_sym(table):
    ''' Get a psycopg Identifier symbol given a table dict. '''
    return Identifier(table['namespace'], table['tablename'])

PreparedStatementKey = namedtuple('PreparedStatementKey', ('opname', 'namespace', 'tablename', 'cols')) # noqa: PYI024

def snip_middle(text, limit, mark='...'):
    ''' Limit text to max length by inserting a separator string in middle.
    >>> snip_middle(None, 1)
    >>> snip_middle('12345678', 5)
    '1...8'
    >>> snip_middle('12345678', 5, 'X')
    '12X78'
    >>> snip_middle('abcdefghijklmnopqrstuvwxyz', 12)
    'abcd...vwxyz'
    >>> snip_middle('abcdefg', 4)  # not enough room for mark
    'abcd'
    '''
    if not text or len(text) <= limit:
        return text
    if limit < len(mark) + 2:
        return text[:limit]
    limit -= len(mark)
    return f'{text[:math.floor(limit/2)]}{mark}{text[-math.ceil(limit/2):]}'

def gen_prep_guid(prep_key):
    return re.sub(r'[^a-zA-Z0-9]', '', base64.b32encode(hashlib.md5(repr(prep_key).encode('utf8')).digest()).decode())

def gen_prep_name(prep_key):
    ''' Generate a name to use for a prepared statement.
    >>> gen_prep_name(PreparedStatementKey('insert', 'foo', 'bar', ('id', 'name', 'created_at')))
    'insert_bar_BELRJATTJZHR5DYJGVKUIUF4AU'
    >>> gen_prep_name(PreparedStatementKey('insert', 'foo', 'bar' * 50, ('id', 'name', 'created_at')))
    'insert_barbarbarbarbarbarbarbaXXXbar_SRIDN2AWHRI4YT5JCJ7KELCV7M'
    '''
    guid = gen_prep_guid(prep_key)
    # 63 is max length in postgresql for a prepared statement name
    return snip_middle(f'{prep_key.opname}_{prep_key.tablename}_{guid}', 63, 'XXX')

class MissingTable(Exception):
    pass

def is_generated_col(tbl_cols, colname):
    coldef = tbl_cols.get(colname)
    return coldef.generated == 's' if coldef else None

def get_cols(pg, table, column_defs, rec):
    ''' Get tuple of columns names leading with primary key followed by sorted remaining columns.
        :column_defs: output of `get_column_defs(pg)`
    '''
    keycols = get_keycols(pg, table['namespace'], table['tablename'])
    rec_cols = tuple(keycols + sorted(c for c in rec if c not in keycols))
    tblid = (table['namespace'], table['tablename'])
    if tblid not in column_defs:
        namespace, tablename = tblid
        msg = f'Table {namespace}.{tablename} is not present. Please upgrade database and try again.'
        raise MissingTable(msg)
    tbl_cols = column_defs[tblid]
    return tuple(  # can't insert or update generated columns, so omit them
        c for c in rec_cols if is_generated_col(tbl_cols, c) is not True)

class WorkerExit(RuntimeError):
    pass

def send_carefully(worker, content):
    ready = []
    while not ready and worker.process.is_alive():
        _, ready, _ = select.select([], [worker.sink.fileno()], [], 1)
    if not worker.process.is_alive():
        raise WorkerExit
    worker.sink.send(content)

def insert(pg, table, new, column_defs, _prep_cache={}):  # pylint: disable=dangerous-default-value
    ''' Perform an INSERT. '''
    cols = get_cols(pg, table, column_defs, new)
    prep_key = PreparedStatementKey('insert', table['namespace'], table['tablename'], cols)
    if prep_key in _prep_cache:
        prep_name = _prep_cache[prep_key]
    else:
        prep_name = _prep_cache[prep_key] = gen_prep_name(prep_key)
        pg.execute(SQL('''
            DO $$ BEGIN
                BEGIN
                    PREPARE {prep_name} AS
                    INSERT INTO {tbl} ({cols}) OVERRIDING SYSTEM VALUE
                    VALUES ({args})
                EXCEPTION
                    WHEN duplicate_prepared_statement THEN
                END;
            END $$;
        ''').format(
            prep_name=Identifier(prep_name),
            tbl=get_table_sym(table),
            cols=SQL(', ').join([Identifier(c) for c in cols]),
            args=SQL(', ').join([SQL(f'${i}') for i, _ in enumerate(cols, start=1)]),
        ))
    pg.execute(
        SQL('EXECUTE {prep_name} ' + placeholders(len(cols))).format(prep_name=Identifier(prep_name)),
        tuple(new[c] for c in cols))

def update(pg, table, old, new, column_defs, _prep_cache={}):  # pylint: disable=dangerous-default-value
    ''' Perform an UPDATE.
        The WHERE clause is an optimization to eliminate a write when nothing has changed.
    '''
    keycols = get_keycols(pg, table['namespace'], table['tablename'])
    cols = get_cols(pg, table, column_defs, new)
    prep_key = PreparedStatementKey('update', table['namespace'], table['tablename'], cols)
    if prep_key in _prep_cache:
        prep_name = _prep_cache[prep_key]
    else:
        prep_name = _prep_cache[prep_key] = gen_prep_name(prep_key)
        pg.execute(SQL('''
            DO $$ BEGIN
                BEGIN
                    PREPARE {prep_name} AS
                    UPDATE {tbl}
                    SET ({cols}) = ({args})
                    WHERE ({pkey}) = ({pkey_args});
                EXCEPTION
                    WHEN duplicate_prepared_statement THEN
                END;
            END $$;
        ''').format(
            prep_name=Identifier(prep_name),
            tbl=get_table_sym(table),
            cols=SQL(', ').join([Identifier(c) for c in cols]),
            pkey=SQL(', ').join([Identifier(c) for c in keycols]),
            args=SQL(', ').join([SQL(f'${i}') for i, _ in enumerate(cols, start=1)]),
            pkey_args=SQL(', ').join([SQL(f'${i + len(cols)}') for i, _ in enumerate(keycols, start=1)]),
        ))
    keyrec = old or new  # primary key might change, match on old key
    keyvals = tuple(keyrec[c] for c in keycols)
    pg.execute(
        SQL('EXECUTE {prep_name} ' + placeholders(len(cols)+len(keyvals))).format(prep_name=Identifier(prep_name)),
        tuple(new[c] for c in cols) + keyvals)

def parse_unique_error_key(text):
    '''
    >>> parse_unique_error_key('')
    >>> parse_unique_error_key(None)
    >>> parse_unique_error_key('Key (foo_id, bar_id)=(1, 2) already exists.')
    (None, ('foo_id', 'bar_id'), ('1', '2'))
    >>> parse_unique_error_key('Ya existe la llave (foo_id, bar_id)=(1, 2).')
    (None, ('foo_id', 'bar_id'), ('1', '2'))
    >>> parse_unique_error_key('Key (key)=(:1:rl:28c8c88c2) already exists.')
    (None, ('key',), (':1:rl:28c8c88c2',))
    >>> parse_unique_error_key('Key (key)=(some whitespace) already exists.')
    (None, ('key',), ('some whitespace',))
    >>> parse_unique_error_key('Key (key)=(hello, world) already exists.')
    (None, ('key',), ('hello, world',))
    >>> parse_unique_error_key('Key (key, key2)=(hello, world, goodbye) already exists.')
    (None, ('key', 'key2'), ('hello', 'world, goodbye'))
    >>> parse_unique_error_key('Key (key64(slug))=(2387803335588640347) already exists.')
    ('key64', ('slug',), ('2387803335588640347',))
    >>> parse_unique_error_key('Key (key64(VARIADIC ARRAY[slug]))=(2387803335588640347) already exists.')
    ('key64', ('slug',), ('2387803335588640347',))
    >>> parse_unique_error_key('Key (key64(VARIADIC ARRAY[a::text, b]))=(-6741337538869099751) already exists')
    ('key64', ('a::text', 'b'), ('-6741337538869099751',))
    '''
    if match := re.match(r'.*\((?P<func>\w+)\(VARIADIC ARRAY\[(?P<cols>.+)\]\)\)=\((?P<vals>.+)\).*', text or ''):
        func = match.group('func')
        cols = match.group('cols').split(', ')
        vals = match.group('vals').split(', ', maxsplit=len(cols) - 1)
        return func, tuple(cols), tuple(vals)
    if match := re.match(r'.*\((?P<func>\w+)\((?P<cols>.+)\)\)=\((?P<vals>.+)\).*', text or ''):
        func = match.group('func')
        cols = match.group('cols').split(', ')
        vals = match.group('vals').split(', ', maxsplit=len(cols) - 1)
        return func, tuple(cols), tuple(vals)
    if match := re.match(r'.*\((?P<cols>.+)\)=\((?P<vals>.+)\).*', text or ''):
        cols = match.group('cols').split(', ')
        vals = match.group('vals').split(', ', maxsplit=len(cols) - 1)
        return None, tuple(cols), tuple(vals)

def upsert(pg, table, cmd, old, new, column_defs, onconflict='do update', _prep_cache={},  # pylint: disable=dangerous-default-value
           prepared=use_prepared_statement):
    ''' Perform an INSERT or UPDATE ("upsert") using PostgreSQL ON CONFLICT clause[1].
        The WHERE clause is an optimization to eliminate a write when nothing has changed.
        https://www.postgresql.org/docs/current/sql-insert.html
    '''
    assert cmd in ('insert', 'update'), f'expected insert or update for cmd, got {cmd!r}'
    if old is not None:
        # Grab unchanged TOASTed values from previous versions of the record
        new.update((k, v) for k, v in old.items() if k not in new)
    keycols = get_keycols(pg, table['namespace'], table['tablename'])
    cols = get_cols(pg, table, column_defs, new)
    prep_key = PreparedStatementKey('upsert', table['namespace'], table['tablename'], cols)
    if cmd == 'update' and gen_prep_guid(prep_key) in ('M2OIIYVO6OBBI24DPANINOZQHM', '5WF7DO3ROEX26VFLUGAPL7C5CM'):
        return  # temp stop-gap to omit invalid rows with missing toast value
    if not prepared:
        prep_name = None
    elif prep_key in _prep_cache:
        prep_name = _prep_cache[prep_key]
    else:
        prep_name = _prep_cache[prep_key] = gen_prep_name(prep_key)
        conflict_exp = '''
            ON CONFLICT ({pkey}) DO UPDATE SET
                ({upcols}) = ROW({upargs})
            WHERE ({qualcols}) IS DISTINCT FROM ({upargs});
        '''
        if not keycols:
            onconflict = 'do nothing'  # happens when primary key is a generated column
        elif len(keycols) == len(cols):
            onconflict = 'do nothing'  # no point doing an update if key includes all columns because nothing can change
        if onconflict == 'do nothing':
            conflict_exp = 'ON CONFLICT ({pkey}) DO NOTHING;'
            if not keycols:
                conflict_exp = 'ON CONFLICT DO NOTHING;'  # avoid syntax error
        pg.execute(SQL('''
            DO $$ BEGIN
                BEGIN
                    PREPARE {prep_name} AS
                    INSERT INTO {tbl} ({cols}) OVERRIDING SYSTEM VALUE
                    VALUES ({args})''' + conflict_exp + '''
                EXCEPTION
                    WHEN duplicate_prepared_statement THEN
                END;
            END $$;
            ''').format(
            prep_name=Identifier(prep_name),
            tbl=get_table_sym(table),
            cols=SQL(', ').join([Identifier(c) for c in cols]),
            upcols=SQL(', ').join([Identifier(c) for c in cols if c not in keycols]),
            qualcols=SQL(', ').join([Identifier(table['tablename'], c) for c in cols if c not in keycols]),
            pkey=SQL(', ').join([Identifier(c) for c in keycols]),
            args=SQL(', ').join([SQL(f'${cols.index(c) + 1}') for c in cols]),
            upargs=SQL(', ').join([SQL(f'${cols.index(c) + 1}') for c in cols if c not in keycols]),
        ))
    # When a non-nullable value is omitted (because it is TOASTed and hasn't changed), then this function will fail
    # the null constraint check on insert; use update instead
    tbl_cols = column_defs.get((table['namespace'], table['tablename']))
    if cmd == 'update' and any(
            k for k, c in tbl_cols.items() if k not in keycols and c.notnull and new.get(k) is None):
        return update(pg, table, old, new, column_defs)
    if old:  # if the primary key has changed, we delete the old record before upserting the new record
        try:
            oldkey = [old[c] for c in keycols]
        except KeyError:
            oldkey = None
        if oldkey and oldkey != [new[c] for c in keycols]:
            delete(pg, table, old)
    if prepared:
        pg.execute(
            SQL('EXECUTE {prep_name} '+placeholders(len(cols))).format(prep_name=Identifier(prep_name)),
            tuple(new[c] for c in cols))
    else:
        _upsert_noprep(pg, table, cols, keycols, new, onconflict)

def mitigate_conflict(pg, e):
    confkey = parse_unique_error_key(e.diag.message_detail)
    if not confkey:
        raise RuntimeError(f'Couldn\'t parse error message: \'{e.diag.message_detail}\'')
    func, cols, vals = confkey
    pg.rollback()
    def format_column(c):
        if '*' in c or '::' in c:
            return SQL(c)
        return Identifier(c)
    namespace, tablename = e.diag.schema_name, e.diag.table_name
    keycols = get_keycols(pg, namespace, tablename)
    keyexp = SQL(', ').join([format_column(c) for c in cols])
    if func:
        keyexp = SQL('%s({0})' % func).format(keyexp)
    query = SQL('''
        DELETE FROM {tbl}
        WHERE ({keyexp}) = '''+placeholders(len(vals))+'''
        RETURNING ({pkey})''').format(
        tbl=Identifier(namespace, tablename),
        keyexp=keyexp,
        pkey=SQL(', ').join([Identifier(c) for c in keycols])
    )
    pkey = tuple(*pg.execute(query, vals).fetchall())
    slog.warn2('conflicting row detected and removed: %s', keyexp)
    pg.commit()
    return (namespace, tablename, pkey)

def _upsert_noprep(pg, table, cols, keycols, new, onconflict):
    '''
    A bug report indicates that prepared statements can cause postgresql to use unbounded memory and CPU. This function
    exists as a work-around to avoid the bug in postgresql (reportedly confirmed on v12 and v15). The test case was
    reproducible on their system but hasn't been observed independently.
    '''
    conflict_exp = '''
        ON CONFLICT ({pkey}) DO UPDATE SET
            ({upcols}) = ROW({exclcols})
        WHERE ({qualcols}) IS DISTINCT FROM ({exclcols});
    '''
    if not keycols:
        onconflict = 'do nothing'  # happens when primary key is a generated column
    elif len(keycols) == len(cols):
        onconflict = 'do nothing'  # no point doing an update if key includes all columns because nothing can change
    if onconflict == 'do nothing':
        conflict_exp = 'ON CONFLICT ({pkey}) DO NOTHING;'
        if not keycols:
            conflict_exp = 'ON CONFLICT DO NOTHING;'  # avoid syntax error
    pg.execute(SQL('''
        INSERT INTO {tbl} ({cols}) OVERRIDING SYSTEM VALUE
        VALUES ''' + placeholders(len(cols)) + conflict_exp).format(
        tbl=get_table_sym(table),
        cols=SQL(', ').join([Identifier(c) for c in cols]),
        upcols=SQL(', ').join([Identifier(c) for c in cols if c not in keycols]),
        qualcols=SQL(', ').join([Identifier(table['tablename'], c) for c in cols if c not in keycols]),
        pkey=SQL(', ').join([Identifier(c) for c in keycols]),
        exclcols=SQL(', ').join([Identifier('excluded', c) for c in cols if c not in keycols]),
    ), tuple(new[c] for c in cols))

def delete(pg, table, old, _prep_cache={}, prepared=use_prepared_statement):  # pylint: disable=dangerous-default-value
    ''' Perform a DELETE. '''
    keycols = get_keycols(pg, table['namespace'], table['tablename'])
    if not prepared:
        _delete_noprep(pg, table, keycols, old)
        return
    prep_key = PreparedStatementKey('delete', table['namespace'], table['tablename'], tuple(keycols))
    if prep_key in _prep_cache:
        prep_name = _prep_cache[prep_key]
    else:
        prep_name = _prep_cache[prep_key] = gen_prep_name(prep_key)
        pg.execute(SQL('''
            DO $$ BEGIN
                BEGIN
                    PREPARE {prep_name} AS
                    DELETE FROM {tbl}
                    WHERE ({pkey}) = ({pkey_args});
                EXCEPTION
                    WHEN duplicate_prepared_statement THEN
                END;
            END $$;
        ''').format(
            prep_name=Identifier(prep_name),
            tbl=get_table_sym(table),
            pkey=SQL(', ').join([Identifier(c) for c in keycols]),
            pkey_args=SQL(', ').join([SQL(f'${i}') for i, _ in enumerate(keycols, start=1)]),
        ))
    pg.execute(
        SQL('EXECUTE {prep_name} '+placeholders(len(keycols))).format(prep_name=Identifier(prep_name)),
        tuple(old[c] for c in keycols))

def _delete_noprep(pg, table, keycols, old):
    '''
    Same issue as _upsert_noprep()
    A bug report indicates that prepared statements can cause postgresql to use unbounded memory and CPU. This function
    exists as a work-around to avoid the bug in postgresql (reportedly confirmed on v12 and v15). The test case was
    reproducible on their system but hasn't been observed independently.
    '''
    pg.execute(SQL('''
        DELETE FROM {tbl}
        WHERE ({pkey}) = '''+placeholders(len(keycols))).format(
        tbl=get_table_sym(table),
        pkey=SQL(', ').join([Identifier(c) for c in keycols]),
    ), tuple(old[c] for c in keycols))

def truncate(pg, tables, cascade=False, restartid=None):
    ''' Perform a TRUNCATE command. '''
    statement = 'TRUNCATE {tbls}'
    if cascade:
        statement += ' CASCADE'
    if restartid:
        statement += ' RESTART IDENTITY'
    pg.execute(SQL(statement).format(tbls=SQL(', ').join([get_table_sym(table) for table in tables])))
