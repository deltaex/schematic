import json
import os
import sys

from schematic import ablative, env, pgcn
from schematic.build_util import acquire_shared_builder_lock, apply_sql_file


def revision_exists(pg):
    return pg.execute('''
        SELECT EXISTS (SELECT * FROM pg_tables WHERE schemaname = 'meta' AND tablename = 'revision');
    ''').scalar()

def is_applied(pg, guid):
    return revision_exists(pg) and pg.execute('''
        SELECT EXISTS (SELECT * FROM meta.revision WHERE guid = %(guid)s);
    ''', {'guid': guid}).scalar()

def insert_revision(pg):
    pg.execute('''
        INSERT INTO meta.revision (guid, name, env, argv, dependencies)
        SELECT
            %(guid)s,
            %(name)s,
            %(env)s,
            %(argv)s,
            %(dependencies)s
    ''', {
        'env': json.dumps(dict(os.environ)),
        'argv': sys.argv,
        'guid': env.get_str('guid'),
        'name': env.get_str('name'),
        'dependencies': env.get_array('dependencies')})

def apply_imperative(pguri, autocommit):
    ''' Upgrade a database using revisions. '''
    with pgcn.connect(pguri, autocommit=autocommit) as pg:
        upgrade_sql = env.get_str('upgrade_sql')
        if pg.execute('SELECT pg_is_in_recovery();').scalar():
            print('read-only, skipping %r' % upgrade_sql, file=sys.stderr)
            return
        acquire_shared_builder_lock(pg)
        name = env.get_str('name')
        guid = env.get_str('guid')
        if is_applied(pg, guid):
            print(f'already applied: {name}-{guid}', file=sys.stderr)
            return
        apply_sql_file(pg, upgrade_sql)
        insert_revision(pg)
        pg.commit()

def apply_derivative(pguri, autocommit):
    ''' Upgrade a database by diffing; build_schema.py does most of the work but revisions are still used for
        mutations and destructive changes.
    '''
    name = env.get_str('name')
    print(f'revision derivative {name}')
    upgrade_sql: str = env.get_str('upgrade_sql')  # type: ignore[reportGeneralTypeIssues]
    with open(upgrade_sql, encoding='utf8') as fp:
        content = fp.read()
    with pgcn.connect(pguri, autocommit=autocommit) as pg:
        if pg.execute('SELECT pg_is_in_recovery();').scalar():
            print('read-only, skipping %r' % upgrade_sql, file=sys.stderr)
            return
        name = env.get_str('name')
        guid = env.get_str('guid')
        if is_applied(pg, guid):
            print(f'already applied: {name}-{guid}', file=sys.stderr)
            return
        acquire_shared_builder_lock(pg)
        pg.execute('BEGIN;')
        ablative.safe_apply_statements(pg, content)
        insert_revision(pg)
        pg.commit()

def main():
    autocommit = env.get_bool('autocommit')
    if env.get_str('SCM_UPGRADE_MODE') == 'derivative':
        if not env.get_bool('SCM_PG_UPGRADE'):
            apply_derivative(env.get_str('pguri'), autocommit)

if __name__ == '__main__':
    main()
