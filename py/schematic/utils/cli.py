import os
import re
import sys
from typing import NoReturn

from schematic import env


def head(args: list[str]) -> str | None:
    for x in args:
        return x

def lpop(args: list[str]) -> tuple[str | None, list[str]]:
    return head(args), args[1:]

def errexit(code: int, msg: str, hint: str | None = None) -> NoReturn:
    print(msg, file=sys.stderr)
    print(hint, file=sys.stderr) if hint else None
    sys.exit(code)

def helpexit(msg: str) -> NoReturn:
    print(msg)
    sys.exit(0)

def parse_args(
        argv: list[str], flags: tuple[str, ...] = (), options: tuple[str, ...] = ()
    ) -> tuple[list[str], tuple[str, ...], dict[str, str]]:
    ''' Parse arg vector, returning a tuple of (list of positional args, list of flags, dict of options).

    >>> parse_args(['foo'], flags=(), options=())
    (['foo'], (), {})
    >>> parse_args(['foo', 'bar'], flags=(), options=())
    (['foo', 'bar'], (), {})
    >>> parse_args(['foo', '-x', '1', 'bar'], flags=(), options=('-x',))
    (['foo', 'bar'], (), {'-x': '1'})
    >>> parse_args(['foo', '--yes', '--sql_where', 'bar'], flags=('--yes', '--sql_where',), options=())
    (['foo', 'bar'], ('--yes', '--sql_where'), {})
    >>> parse_args(['foo', '-abc', 'bar'], flags=('-a', '-b', '-c'), options=())
    (['foo', 'bar'], ('-a', '-b', '-c'), {})
    '''
    for opt in flags + options:
        assert (
            re.match(r'^-[a-z0-9]$', opt, re.I) or
            re.match(r'^--[a-z0-9]+[a-z0-9-_]*$', opt, re.I)), f'invalid syntax: {opt}'
    ret_pos: list[str] = []
    ret_flags: list[str] = []
    ret_options: dict[str, str] = {}
    while argv:
        arg, argv = lpop(argv)
        if arg is None:
            break
        if not arg.startswith('-'):
            ret_pos.append(arg)
        elif arg in flags:
            ret_flags.append(arg)
        elif arg in options:
            value, argv = lpop(argv)
            if value is None:
                errexit(1, 'expected value for %s' % arg)
            assert value is not None
            ret_options[arg] = value
        elif re.match(r'^-[a-z0-9]{2,36}$', arg):  # split up bundled short flags
            argv = ['-%s' % x for x in arg[1:]] + argv
        else:
            errexit(1, 'unexpected arg: %s' % arg)
    return ret_pos, tuple(ret_flags), ret_options

def get_option_str(options: dict[str, str], names: str | tuple[str, ...], *, required: bool = False) -> str | None:
    argnames: list[str] = [names] if isinstance(names, str) else list(names)
    for argname in argnames:
        val = options.get(argname) if argname.startswith('-') else env.get_str(argname)
        if val is None:
            continue
        try:
            return str(val)
        except ValueError:
            errexit(1, f'invalid value for option: {argname} {val}')
    if required:
        errexit(1, f'required option: {head(argnames)}')

def get_option_int(options: dict[str, str], names: str | tuple[str, ...], *, required: bool = False) -> int | None:
    argnames: list[str] = [names] if isinstance(names, str) else list(names)
    for argname in argnames:
        val = options.get(argname) if argname.startswith('-') else env.get_str(argname)
        if val is None:
            continue
        try:
            return int(val)
        except ValueError:
            errexit(1, f'invalid value for option: {argname} {val}')
    if required:
        errexit(1, f'required option: {head(argnames)}')

def get_option_bool(options: dict[str, str], names: str | tuple[str, ...], *, required: bool = False) -> bool | None:
    argnames: list[str] = [names] if isinstance(names, str) else list(names)
    for argname in argnames:
        val = options.get(argname) if argname.startswith('-') else env.get_str(argname)
        if val is None:
            continue
        try:
            return env.parse_bool(val)
        except KeyError:
            errexit(1, f'invalid value for option: {argname} {val}')
    if required:
        errexit(1, 'required option: %s' % head(argnames))

def get_flag(flags: tuple[str, ...], names: tuple[str, ...]) -> bool:
    for name in names:
        if name.startswith('-') and name in flags:
            return True
        if not name.startswith('-') and name in os.environ:
            return env.get_bool(name)
    return False

def get_pos_arg(pos_args: list[str], index: int, name: str) -> str | NoReturn:
    if index >= len(pos_args):
        command_name = sys.argv[1]
        errexit(1, f'''Error: Missing argument \'{name}\'.

Try running \'scm {command_name} --help\' for help.''')
    return pos_args[index]

def ask_help(argv: list[str]) -> bool:
    arg = head(argv)
    return arg in ('help', '-h', '--help', '-H', '-help')
