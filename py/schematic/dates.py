import calendar
import datetime


def datetime_to_unix(dt):
    if isinstance(dt, datetime.datetime):
        return calendar.timegm(dt.utctimetuple())
    if isinstance(dt, datetime.date):
        return calendar.timegm(dt.timetuple())
    raise ValueError(f'unexpected type: {dt!r}')

def fmt_age(timestamp):
    age = datetime.datetime.now() - datetime.datetime.fromtimestamp(timestamp)
    secs = age.total_seconds()
    if age.days:
        return '%4dd' % age.days
    if secs >= 60 * 60:
        return '%4dh' % int(secs / (60 * 60))
    if secs >= 60:
        return '%4dm' % int(secs / 60)
    return '%4ds' % int(secs)
