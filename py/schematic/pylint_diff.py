'''
Compare the pylint messages of an edited python file and its most recent comitted version.

Used for generating an error in git pre-commit hook when there's pylint regressions.
'''

import json
import os
import random
import sys

import sh

from schematic import fs


def get_lint_msgs(filename):
    out = sh.pylint(
        '--rcfile', os.path.join(os.environ['ROOT_DIR'], 'etc/pylint/default.rc'), '--output-format', 'json',
        '--exit-zero', filename)
    return {
        (msg['message-id'], msg['message'], msg['obj'], msg['symbol']): msg for msg in json.loads(out or '{}')}

def get_lint_messages_from_git_index(tmpdir, filename, ref):
    if ref == ':0':
        # use a directory name that conforms to our pylint rules (not 0 for dirname)
        refdir = os.path.join(tmpdir, 'stage')
    elif ref.lower() == 'head':
        refdir = os.path.join(tmpdir, 'head')
    else:
        # consider a more robust implementation if for some reason
        # we need to support additional refs instead of just stage and head
        msg = f'Usupported ref {ref}'
        raise Exception(msg)  # noqa: TRY002
    os.mkdir(refdir) # create a directory for the reference to avoid modifying the file name
    temp_filepath = os.path.join(refdir, os.path.basename(filename))
    with open(temp_filepath, 'wt', encoding='utf-8') as fp:
        try:
            fp.write(sh.git.show(f'{ref}:{filename}', _tty_out=False))
        except sh.ErrorReturnCode_128:  # file isn't in HEAD
            return {}
        fp.flush()  # make sure we flush so that pylint pickups the whole file
        return get_lint_msgs(temp_filepath)

def main():
    filename = sys.argv[-1]
    tmpdir_path = os.path.join('/tmp/pylint_diff/', 'x%s' % random.randint(1, 2 ** 31))  # noqa: S108
    fs.tmpdir(tmpdir_path)
    # ref :0, refers to 'stage', slot 0
    staged = get_lint_messages_from_git_index(tmpdir_path, filename, ref=':0')
    previous = get_lint_messages_from_git_index(tmpdir_path, filename, ref='HEAD') if staged else ''
    exitcode = 0
    print(filename + ' (staged copy)', file=sys.stderr)
    for key in staged:
        if key not in previous:
            msg = staged[key]
            print('%d,%d %s %s' % (msg['line'], msg['column'], msg['message-id'], msg['message']), file=sys.stderr)
            exitcode = 1
    sys.exit(exitcode)

if __name__ == '__main__':
    main()
