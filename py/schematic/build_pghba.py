'''
Translate host-based authentication rules into a section of the pg_hba.conf file.

See: https://www.postgresql.org/docs/current/auth-pg-hba-conf.html
'''
import json
import os
import re
import sys
from collections import OrderedDict
from graphlib import TopologicalSorter

from schematic import env, pgcn, slog
from schematic.build_util import (
    acquire_shared_builder_lock, append_newline, fmt_footer, fmt_header, install_buildinput_files,
    parse_footer, parse_header)

type_enum = ('host', 'hostgssenc', 'hostnogssenc', 'hostnossl', 'hostssl', 'local')
auth_enum = (
    'bsd', 'cert', 'gss', 'ident', 'ldap', 'md5', 'pam', 'password', 'peer', 'radius', 'reject', 'scram-sha-256',
    'sspi', 'trust')

def fmt_line(parts, colsizes=(13, 20, 20, 25, 14, 1)):
    out = ''
    for idx, part in enumerate(parts):
        out += ((part or '') + ' ').ljust(colsizes[idx] if len(colsizes) > idx else 30)
    return out.rstrip(' ')

def validate_reqd(rule, field, not_=False):
    if bool(rule.get(field)) is not_:
        maybe_not = 'not ' if not_ else ''
        slog.error2(f'"{field}" field is {maybe_not}required for rule {rule!r}')
        sys.exit(10)

def validate_quotes(rule, field):
    val = rule.get(field)
    if val and re.search(r'[\s,]+', val) and not re.match(r'".+"', val):
        slog.error2(f'"{field}" field has value {val!r} that must use double quotes')
        sys.exit(10)

def validate_enum(rule, field, options):
    val = rule.get(field)
    if val is not None and val not in options:
        slog.error2(f'"{field}" field expected one of {options!r}, got {val!r}')
        sys.exit(10)

def validate_notrust_remote(rule):
    ''' Validate that we're only trusting (password-less) local connections. '''
    insecure = rule.get('insecure')
    if rule.get('auth') == 'trust' and rule.get('type') in ('host', 'hostnossl'):
        addr = rule.get('addr').strip()
        if addr:
            if addr.startswith('127.') or addr == '::1/128' or addr == 'localhost':
                return
            msg = '"trust" auth method for non-local client "%s" is insecure' % addr
            if insecure:
                return slog.debug('warn: %s' % msg)
            slog.error2('error: %s' % msg)
            slog.error2('hint: use auth = "md5" instead for password authentication')
            slog.error2('hint: use insecure = true to override this error')
            sys.exit(10)

def validate_unencrypted_password(rule):
    ''' Validate that we aren't allowing plaintext passwords over the network. '''
    insecure = rule.get('insecure')
    if rule.get('auth') == 'password':
        addr = rule.get('addr').strip()
        if addr:
            msg = '"password" auth method sends plain-text passwords which can be intercepted'
            if insecure:
                return slog.debug('warn: %s' % msg)
            slog.error2('error: %s' % msg)
            slog.error2('hint: use auth = "md5" instead send hashed passwords')
            slog.error2('hint: use insecure = true to override this error')
            sys.exit(10)

def validate_rule(rule):
    for key in rule.keys() - {'type', 'dbname', 'user', 'addr', 'auth', 'opts', 'comment', 'insecure'}:
        slog.warn2(slog.yellow(f'unrecognized field {key!r} in pghba rule {rule}'))
    validate_reqd(rule, 'type')
    validate_enum(rule, 'type', type_enum)
    validate_reqd(rule, 'dbname')
    validate_quotes(rule, 'dbname')
    validate_reqd(rule, 'user')
    validate_quotes(rule, 'user')
    validate_reqd(rule, 'addr', not_=(rule['type'] == 'local'))
    validate_quotes(rule, 'addr')
    validate_reqd(rule, 'auth')
    validate_enum(rule, 'auth', auth_enum)
    validate_enum(rule, 'insecure', (None, True, False))
    validate_notrust_remote(rule)
    validate_unencrypted_password(rule)

def fmt_rule(rule):
    ''' Translate a rule object into a string line.
    >>> print(fmt_rule({'auth': 'trust', 'comment': 'test', 'dbname': 'testdb', 'type': 'local', 'user': 'root'}))
    local        testdb              root                                         trust          # test
    >>> print(fmt_rule({'addr': '0.0.0.0/0', 'auth': 'trust', 'opts': '', 'comment': 'test', 'dbname': 'testdb', 'type': 'host', 'user': 'root'}))
    host         testdb              root                0.0.0.0/0                trust          # test
    >>> print(fmt_rule({'addr': '0.0.0.0/0', 'auth': 'trust', 'opts': '', 'comment': 'test1\\ntest2', 'dbname': 'testdb', 'type': 'host', 'user': 'root'}))
    host         testdb              root                0.0.0.0/0                trust          # test1_test2
    '''
    return fmt_line((
        rule.get('type'), rule.get('dbname'), rule.get('user'), rule.get('addr'), rule.get('auth'), rule.get('opts'),
        '# %s' % re.sub(r'\n', '_', rule['comment']) if rule.get('comment') else None))

def parse_pghbaconf(content):
    ''' Parse a pg_hba.conf file into sections (one section per pghba package, plus a section for default content). '''
    sections = OrderedDict()
    buf = []
    header = None
    for line in content.split('\n'):
        if footer := parse_footer(line):
            if not header:
                raise ValueError('invalid file, got footer with no matching header on line %r' % line)
            _, guid, depends = header  # pylint: disable=unpacking-non-sequence
            if guid != footer:
                msg = f'invalid file, expected footer to have guid {guid} on line {line!r}'
                raise ValueError(msg)
            sections[guid] = {'lines': [*buf, line], 'depends': depends}
            buf = []
            header = None
            continue
        if newheader := parse_header(line):
            if header:
                raise ValueError('invalid file, got header inside another header header on line %r' % line)
            header = newheader
            buf.append(line)
            continue
        if header:
            buf.append(line)
            continue
        if None not in sections:
            sections[None] = {'lines': [], 'depends': []}
        sections[None]['lines'].append(line)
    if header:
        raise ValueError('header had no footer: %r' % header)
    return sections

def topographic_sort_sections(sections):
    ''' Topographic sort sections so that dependees come before dependencies. This allows the user to control the order
        of sections by creating a dependency on another pghba package in order to force a given package to come first
        (hence override the rules in another package). In the absense of a dependency relation between two sections,
        order by guid for deterministic output.
        Returns a list of guids.
    '''
    graph = {k: v['depends'] for k, v in sections.items() if k is not None}
    sorted_graph = dict(sorted(graph.items(), reverse=True))
    return list(TopologicalSorter(sorted_graph).static_order())[::-1]

def serialize_pghbaconf(sections):
    ''' Serialize the sections into a string. '''
    buf = []
    for guid in topographic_sort_sections(sections) + [None]:  # noqa: RUF005
        buf.extend(sections[guid]['lines'])
    return append_newline(''.join(append_newline(line) for line in buf).rstrip())

def update_config(filepath, guid, name, comment, rules):
    ''' Update config file, replacing content between start_flag and end_flag (if they exist) and returning boolean
        indicating whether any change was made.
    '''
    old_content = open(filepath, encoding='utf8').read() if os.path.exists(filepath) else ''  # pylint: disable=consider-using-with
    sections = parse_pghbaconf(old_content)
    depends = [x for x in env.get_array('transDepGuids') or [] if x.startswith('C1')]
    lines = [fmt_header(guid, name, comment, depends)] + [fmt_rule(rule) for rule in rules] + [fmt_footer(guid, name)]
    sections[guid] = {'lines': lines, 'depends': depends, 'comment': comment}
    new_content = serialize_pghbaconf(sections)
    if new_content == old_content:
        return False
    with open(filepath, 'w', encoding='utf8') as fp:
        fp.write(new_content)
    return True

def apply(pguri, basedir):
    if env.get_bool('SCM_PG_UPGRADE'):
        # If we're upgrading postgresql versions, skip as there's no database
        # created. But install needed files anyways.
        install_buildinput_files()
        return
    name = env.get_str('name')
    guid = env.get_str('guid')
    comment = env.get_str('comment')
    rules = env.get_str('rules')
    rules = json.loads(rules)
    assert isinstance(rules, list), 'expected list of rules, got %r' % rules
    [validate_rule(rule) for rule in rules]
    install_buildinput_files()
    with pgcn.connect(pguri, autocommit=True) as pg:
        acquire_shared_builder_lock(pg)
        # obtain lock to avoid consistency issues with concurrent edits
        pg.execute('SELECT pg_advisory_lock(8980025710542546871);').first()
        pghba_path = (
            pg.execute('''SELECT setting FROM pg_settings WHERE name = 'hba_file';''').scalar() or
            os.path.join(basedir, 'data/pg_hba.conf'))
        if update_config(pghba_path, guid, name, comment, rules):
            slog.debug2(f'<{name}-{guid}> -> {pghba_path}')
        if errors := pg.execute('SELECT * FROM pg_hba_file_rules WHERE error IS NOT NULL;').all():
            slog.warn2('invalid pg_hba.conf configuration in %s', pghba_path)
            for line in errors:
                slog.warn2('    line %d: %s', line.line_number, line.error)
        # Add this pghba to the log file to have build_database clean up old sections
        # from previous pghbas that have been unlinked from the dependency chain.
        dotfile = os.path.join(env.get_str('basedir'), '.applied_pghbas')
        with open(dotfile, 'a', encoding='utf8') as fp:
            fp.write(f'{guid}\n')
        pg.execute('SELECT pg_reload_conf();')

def main():
    apply(env.get_str('pguri'), env.get_str('basedir'))

if __name__ == '__main__':
    main()
