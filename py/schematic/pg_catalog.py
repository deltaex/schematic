'''Index of pg_catalog tables: https://www.postgresql.org/docs/current/catalogs.html'''
from collections import OrderedDict


def get_namespaces(pg):
    return OrderedDict([(tuple(x.qualname), x) for x in pg.execute('''
        SELECT
            array[n.nspname] AS qualname, n.nspacl,
            pg_catalog.pg_get_userbyid(n.nspowner) AS rolname,
            pg_catalog.obj_description(n.oid, 'pg_namespace') AS objcomment
        FROM pg_catalog.pg_namespace n
        WHERE n.nspname != 'information_schema' AND n.nspname !~ '^pg_'
        ORDER BY oid;
    ''').fetchall()])

def get_sequences(pg):
    return OrderedDict([(tuple(x.qualname), x) for x in pg.execute('''
        SELECT
            array[n.nspname, c.relname] AS qualname,
            s.seqstart, s.seqincrement, s.seqmax, s.seqmin, s.seqcache, s.seqcycle, c.relpersistence,
            owned_by.owned_by, pg_catalog.format_type(t.oid, t.typtypmod) AS typname,
            pg_temp.get_sequence_last_value(n.nspname, c.relname) AS last_value,
            pg_catalog.pg_get_userbyid(c.relowner) AS rolname,
            pg_catalog.obj_description(c.oid, 'pg_class') AS objcomment
        FROM pg_catalog.pg_class c
        JOIN pg_catalog.pg_namespace n ON n.oid = c.relnamespace
        JOIN pg_catalog.pg_sequence s ON s.seqrelid = c.oid
        JOIN pg_catalog.pg_type t ON t.oid = s.seqtypid
        LEFT JOIN LATERAL ( -- which column owns this sequence
            SELECT array[tbl_nsp.nspname, tbl.relname, a.attname] AS owned_by
            FROM pg_catalog.pg_depend d
            JOIN pg_catalog.pg_class seq ON seq.oid = d.objid
            JOIN pg_catalog.pg_namespace seq_nsp ON seq_nsp.oid = seq.relnamespace
            JOIN pg_catalog.pg_class tbl ON tbl.oid = d.refobjid
            JOIN pg_catalog.pg_namespace tbl_nsp ON tbl_nsp.oid = tbl.relnamespace
            JOIN pg_catalog.pg_attribute a ON a.attrelid = tbl.oid AND a.attnum = d.refobjsubid
            WHERE seq_nsp.nspname = n.nspname AND seq.relname = c.relname
            AND d.classid = 'pg_catalog.pg_class'::regclass
            AND d.refclassid = 'pg_catalog.pg_class'::regclass
        ) AS owned_by ON true
        WHERE n.nspname != 'information_schema' AND n.nspname !~ '^pg_'
        AND c.relkind = 'S'
        ORDER BY c.oid;
    ''').fetchall()])

def get_tables(pg):
    return OrderedDict([(tuple(x.qualname), x) for x in pg.execute('''
        SELECT
            array[n.nspname, c.relname] AS qualname, c.relpersistence, pg_catalog.pg_get_expr(c.relpartbound, c.oid) AS part_bound,
            inherits.inherits, partitions.partitions, constraints.constraints,
            partition_parent.parent_table, ty.ty, rules.rules,
            c.relrowsecurity, c.relforcerowsecurity, c.relreplident, c.reloptions, c.relispartition, c.relacl,
            pt.partstrat, pt.partnatts, pt.partattrs, pt.partclass, pt.partcollation,
            partcols.partcols, pg_catalog.pg_get_expr(pt.partexprs, pt.partrelid) AS partexpressions,
            am.amname, tblspc.spcname,
            pg_catalog.pg_get_partkeydef(c.oid) AS partdef,
            pg_catalog.pg_get_userbyid(c.relowner) AS rolname,
            (SELECT array[n.nspname, relty.typname] FROM pg_catalog.pg_type relty join pg_catalog.pg_namespace n ON n.oid = relty.typnamespace WHERE relty.oid = c.reloftype) AS of_type,
            (
                SELECT cl.relname
                FROM pg_catalog.pg_index i
                JOIN pg_class cl ON cl.oid = i.indexrelid
                JOIN pg_catalog.pg_namespace ns ON ns.oid = cl.relnamespace
                WHERE i.indrelid = c.oid AND i.indisreplident
            ) AS replica_index,
            (
                SELECT array[n.nspname, cl.relname]
                FROM pg_catalog.pg_index i
                JOIN pg_catalog.pg_class cl ON i.indexrelid = cl.oid
                JOIN pg_catalog.pg_namespace n ON n.oid = cl.relnamespace
                WHERE i.indrelid = c.oid AND i.indisclustered
            ) AS cluster_index,
            pg_catalog.obj_description(c.oid, 'pg_class') AS objcomment
        FROM pg_catalog.pg_class c
        JOIN pg_catalog.pg_namespace n ON n.oid = c.relnamespace
        LEFT JOIN LATERAL ( -- parent tables this table inherits from
            SELECT array_agg(
                array[pn.nspname, p.relname]
                ORDER BY p.relname, pn.nspname) AS inherits
            FROM pg_catalog.pg_inherits i
            JOIN pg_catalog.pg_class p ON i.inhparent = p.oid
            JOIN pg_catalog.pg_namespace pn ON pn.oid = p.relnamespace
            WHERE i.inhrelid = c.oid
        ) AS inherits ON true
        LEFT JOIN LATERAL ( -- this table's parent. it's `PARTITION OF` table
            SELECT array[pn.nspname, p.relname] AS parent_table
            FROM pg_catalog.pg_inherits i
            JOIN pg_catalog.pg_class p ON i.inhparent = p.oid
            JOIN pg_catalog.pg_namespace pn ON pn.oid = p.relnamespace
            WHERE i.inhrelid = c.oid AND c.relispartition is true
        ) AS partition_parent ON true
        LEFT JOIN LATERAL ( -- existing partitions of this table
            SELECT
                json_agg(json_build_object(
                    'part_name', array[pn.nspname, p.relname],
                    'bound', pg_catalog.pg_get_expr(p.relpartbound, p.oid)
                ) ORDER BY p.oid) AS partitions
            FROM pg_catalog.pg_inherits i
            JOIN pg_catalog.pg_class p ON i.inhrelid = p.oid
            JOIN pg_catalog.pg_namespace pn ON pn.oid = p.relnamespace
            WHERE i.inhparent = c.oid AND p.relispartition is true
        ) AS partitions ON true
        LEFT JOIN LATERAL ( -- constraints on this table
            SELECT
                json_agg(json_build_object(
                    'conname', con.conname,
                    'constraint_def', pg_catalog.pg_get_constraintdef(con.oid),
                    'index_name', ind.relname,
                    'indisvalid', i.indisvalid,
                    'convalidated', con.convalidated,
                    'contype', con.contype,
                    'connoinherit', con.connoinherit,
                    'condeferrable', con.condeferrable,
                    'condeferred', con.condeferred
                ) ORDER BY con.conname) AS constraints
            FROM pg_catalog.pg_constraint con
            JOIN pg_catalog.pg_namespace n ON n.oid = con.connamespace
            LEFT JOIN pg_catalog.pg_class ind ON ind.oid = con.conindid
            LEFT JOIN pg_catalog.pg_index i ON i.indexrelid = ind.oid
            WHERE con.conrelid = c.oid
        ) AS constraints ON true
        LEFT JOIN LATERAL ( -- rules associated with this table
            SELECT
                json_object_agg(r.rulename, r.ev_enabled) AS rules
            FROM pg_catalog.pg_rewrite r
            WHERE r.ev_class = c.oid
        ) AS rules ON true
        LEFT JOIN LATERAL ( -- the underlying composite type of the table
            SELECT
                json_build_object(
                    'typname', array[n.nspname, t.typname]
                ) AS ty
            FROM pg_catalog.pg_type t
            JOIN pg_catalog.pg_namespace n ON n.oid = t.typnamespace
            WHERE t.oid = c.reloftype
        ) AS ty ON true
        LEFT JOIN pg_catalog.pg_partitioned_table pt ON pt.partrelid = c.oid
        LEFT JOIN LATERAL ( -- columns in the partition key
            WITH cols AS (
                SELECT
                    unnest(pt.partattrs) AS colid,
                    unnest(pt.partclass) AS opclassid,
                    unnest(pt.partcollation) AS collationid
            )
            SELECT array_agg(array[att.attname, opclass.opcname, collat.collname] ORDER BY array_position(pt.partattrs, att.attnum)) AS partcols
            FROM cols
            LEFT JOIN pg_catalog.pg_attribute att ON att.attrelid = c.oid AND att.attnum = cols.colid
            JOIN pg_catalog.pg_opclass opclass ON opclass.oid = cols.opclassid
            LEFT JOIN pg_catalog.pg_collation collat ON collat.oid = cols.collationid
        ) AS partcols ON true
        LEFT JOIN pg_catalog.pg_am am ON am.oid = c.relam
        LEFT JOIN pg_catalog.pg_tablespace tblspc ON tblspc.oid = c.reltablespace
        WHERE n.nspname != 'information_schema' AND n.nspname !~ '^pg_'
        AND c.relkind IN ('r', 'p')
        ORDER BY c.oid;
    ''').fetchall()])

def get_simple_colummns(pg, name, nspname):
    '''
        This function gets columns that could possibly be used as partition keys
        so that we can create them along with the function
        It does not include inherited columns, so that we can preserve inheritance relationships
    '''
    return pg.execute('''
        WITH c AS (
            SELECT c.oid FROM pg_class c WHERE c.relname = %(name)s AND c.relnamespace = (
                SELECT n.oid FROM pg_namespace n WHERE n.nspname = %(nspname)s
        ))
        SELECT
            json_agg(
                json_build_object(
                    'colname', a.attname,
                    'typname', pg_catalog.format_type(t.oid, t.typtypmod),
                    'collname', collat.collname,
                    'attnotnull', a.attnotnull,
                    'attidentity', a.attidentity,
                    'attgenerated', a.attgenerated,
                    'atthasdef', a.atthasdef,
                    'attdef', pg_catalog.pg_get_expr(attrdef.adbin, attrdef.adrelid)
                ) ORDER BY a.attnum
            ) AS data
        FROM pg_catalog.pg_attribute a
        LEFT JOIN pg_catalog.pg_type t ON t.oid = a.atttypid
        LEFT JOIN pg_catalog.pg_attrdef attrdef ON attrdef.adrelid = a.attrelid AND attrdef.adnum = a.attnum
        LEFT JOIN pg_catalog.pg_collation collat ON collat.oid = a.attcollation
        WHERE a.attnum > 0 AND a.attrelid = (SELECT oid FROM c)
        AND a.attislocal
        AND (NOT a.atthasdef OR a.attgenerated = '' OR a.attidentity = '');
    ''', {'name': name, 'nspname': nspname}).fetchone().data

def get_table_columns(pg, name, nspname):
    '''
    Similar to get_simple_colummns in that it gets the columns belonging to a table.
    When we want to alter a table to inherit another table,
    we need to add the columns from the parent table before executing `ALTER TABLE INHERIT`
    The reason for this function is that we want `derivative.add_columns()` to add both local and non-local columns
    '''
    return pg.execute('''
        WITH c AS (
            SELECT c.oid FROM pg_class c WHERE c.relname = %(name)s AND c.relnamespace = (
                SELECT n.oid FROM pg_namespace n WHERE n.nspname = %(nspname)s
        ))
        SELECT
            json_object_agg(
                a.attname,
                json_build_object(
                    'colname', a.attname,
                    'typname', pg_catalog.format_type(t.oid, t.typtypmod),
                    'collname', collat.collname,
                    'attnotnull', a.attnotnull,
                    'attidentity', a.attidentity,
                    'attgenerated', a.attgenerated,
                    'atthasdef', a.atthasdef,
                    'attdef', pg_catalog.pg_get_expr(attrdef.adbin, attrdef.adrelid)
                ) ORDER BY a.attnum
            ) AS data
        FROM pg_catalog.pg_attribute a
        LEFT JOIN pg_catalog.pg_type t ON t.oid = a.atttypid
        LEFT JOIN pg_catalog.pg_attrdef attrdef ON attrdef.adrelid = a.attrelid AND attrdef.adnum = a.attnum
        LEFT JOIN pg_catalog.pg_collation collat ON collat.oid = a.attcollation
        WHERE a.attnum > 0 AND a.attrelid = (SELECT oid FROM c)
        -- AND NOT a.attislocal
        AND (NOT a.atthasdef OR a.attgenerated = '' OR a.attidentity = '');
    ''', {'name': name, 'nspname': nspname}).fetchone().data

def get_columns(pg):
    return OrderedDict([(tuple(x.qualname), x) for x in pg.execute('''
        SELECT
            array[n.nspname, c.relname, a.attname] AS qualname,
            pg_catalog.format_type(t.oid, t.typtypmod) AS typname,
            a.attstattarget, a.attstorage,
            pg_temp.safe_catalog_column('pg_attribute', 'attcompression', 'attrelid = ' || a.attrelid, '')::char AS attcompression,
            a.attnotnull, a.atthasmissing, a.attisdropped, a.attislocal,
            a.attinhcount, collat.collname, a.attacl, a.attoptions, a.attfdwoptions,
            a.attidentity, a.attgenerated, a.atthasdef,
            pg_catalog.pg_get_expr(attrdef.adbin, attrdef.adrelid) AS attdef,
            pg_catalog.col_description(c.oid, a.attnum) AS objcomment
        FROM pg_catalog.pg_class c
        JOIN pg_catalog.pg_namespace n ON n.oid = c.relnamespace
        JOIN pg_catalog.pg_attribute a ON a.attrelid = c.oid AND a.attnum > 0 AND a.attislocal
        LEFT JOIN pg_catalog.pg_type t ON t.oid = a.atttypid
        LEFT JOIN pg_catalog.pg_collation collat ON collat.oid = a.attcollation
        LEFT JOIN pg_catalog.pg_attrdef attrdef ON attrdef.adrelid = a.attrelid AND attrdef.adnum = a.attnum
        WHERE n.nspname != 'information_schema' AND n.nspname !~ '^pg_'
        AND c.relkind IN ('r', 'p', 'f')
        ORDER BY c.oid, a.attnum;
    ''').fetchall()])

def get_composite_types(pg):
    return OrderedDict([(tuple(x.qualname), x) for x in pg.execute('''
        SELECT
            array[n.nspname, c.relname] AS qualname,
            pg_catalog.pg_get_userbyid(t.typowner) AS rolname,
            pg_catalog.obj_description(t.oid, 'pg_type') AS objcomment
        FROM pg_catalog.pg_class c
        JOIN pg_catalog.pg_namespace n ON n.oid = c.relnamespace
        LEFT JOIN pg_catalog.pg_type t ON t.oid = c.reltype
        WHERE n.nspname != 'information_schema' AND n.nspname !~ '^pg_'
        AND c.relkind = 'c'
        ORDER BY c.oid;
    ''').fetchall()])

def get_composite_type_attributes(pg):
    return OrderedDict([(tuple(x.qualname), x) for x in pg.execute('''
        SELECT
            array[n.nspname, c.relname, a.attname] AS qualname,
            pg_catalog.format_type(coltype.oid, coltype.typtypmod) AS typname,
            a.attstorage, a.attisdropped, collat.collname,
            a.attacl, a.attoptions,
            pg_catalog.col_description(c.oid, a.attnum) AS objcomment
        FROM pg_catalog.pg_class c
        JOIN pg_catalog.pg_namespace n ON n.oid = c.relnamespace
        LEFT JOIN pg_catalog.pg_type t ON t.oid = c.reltype
        JOIN pg_catalog.pg_attribute a ON a.attrelid = c.oid AND a.attnum > 0
        JOIN pg_catalog.pg_type coltype ON coltype.oid = a.atttypid -- inner join to ignore dropped columns (atttypid = 0)
        LEFT JOIN pg_catalog.pg_collation collat ON collat.oid = a.attcollation
        WHERE n.nspname != 'information_schema' AND n.nspname !~ '^pg_'
        AND c.relkind = 'c'
        ORDER BY c.oid, a.attnum;
    ''').fetchall()])

def get_base_types(pg):
    return OrderedDict([(tuple(x.qualname), x) for x in pg.execute('''
        SELECT
            array[n.nspname, t.typname] AS qualname, t.typlen, t.typbyval, t.typcategory, t.typispreferred, t.typdefault, t.typisdefined,
            t.typdelim, t.typalign, t.typstorage, t.typbasetype,
            pg_catalog.pg_get_userbyid(t.typowner) AS rolname,
            (SELECT array[y.nspname, x.proname] FROM pg_catalog.pg_proc x JOIN pg_catalog.pg_namespace y ON x.pronamespace = y.oid WHERE x.oid = t.typinput) AS input_func,
            (SELECT array[y.nspname, x.proname] FROM pg_catalog.pg_proc x JOIN pg_catalog.pg_namespace y ON x.pronamespace = y.oid WHERE x.oid = t.typoutput) AS output_func,
            (SELECT array[y.nspname, x.proname] FROM pg_catalog.pg_proc x JOIN pg_catalog.pg_namespace y ON x.pronamespace = y.oid WHERE x.oid = t.typreceive) AS receive_func,
            (SELECT array[y.nspname, x.proname] FROM pg_catalog.pg_proc x JOIN pg_catalog.pg_namespace y ON x.pronamespace = y.oid WHERE x.oid = t.typsend) AS send_func,
            (SELECT array[y.nspname, x.proname] FROM pg_catalog.pg_proc x JOIN pg_catalog.pg_namespace y ON x.pronamespace = y.oid WHERE x.oid = t.typmodin) AS modin_func,
            (SELECT array[y.nspname, x.proname] FROM pg_catalog.pg_proc x JOIN pg_catalog.pg_namespace y ON x.pronamespace = y.oid WHERE x.oid = t.typmodout) AS modout_func,
            (SELECT array[y.nspname, x.proname] FROM pg_catalog.pg_proc x JOIN pg_catalog.pg_namespace y ON x.pronamespace = y.oid WHERE x.oid = t.typanalyze) AS analyze_func,
            (SELECT array[y.nspname, x.proname] FROM pg_catalog.pg_proc x JOIN pg_catalog.pg_namespace y ON x.pronamespace = y.oid WHERE x.oid = pg_temp.try_cast(nullif(nullif(pg_temp.safe_catalog_column('pg_type', 'typsubscript', 'oid = ' || t.oid, '0'), '-'), '0'), NULL::oid)) AS subscript_func,
            (SELECT array[y.nspname, pg_catalog.format_type(x.oid, x.typtypmod)] FROM pg_catalog.pg_type x JOIN pg_catalog.pg_namespace y ON x.typnamespace = y.oid WHERE x.oid = t.typelem) AS subscript_type,
            (SELECT x.collname FROM pg_collation x WHERE x.oid = t.typcollation) AS collname,
            pg_catalog.obj_description(t.oid, 'pg_type') AS objcomment
        FROM pg_catalog.pg_type t
        JOIN pg_catalog.pg_namespace n ON n.oid = t.typnamespace
        WHERE n.nspname != 'information_schema' AND n.nspname !~ '^pg_'
        AND t.typtype IN ('b')
        AND NOT EXISTS ( -- postgresql automatically creates an array type for other user types, don't try to create that base type ourselves
            SELECT 1
            FROM pg_catalog.pg_type elemtype
            WHERE elemtype.oid = t.typelem
            AND elemtype.typtype IN ('c', 'd', 'e', 'r', 'm', 'b')
            AND t.typname LIKE '_%')
        ORDER BY t.oid;
    ''').fetchall()])

def get_enum_types(pg):
    return OrderedDict([(tuple(x.qualname), x) for x in pg.execute('''
        SELECT
            array[n.nspname, t.typname] AS qualname,
            pg_catalog.pg_get_userbyid(t.typowner) AS rolname,
            (SELECT array_agg(e.enumlabel ORDER BY enumsortorder) FROM pg_catalog.pg_enum e WHERE e.enumtypid = t.oid) AS labels,
            pg_catalog.obj_description(t.oid, 'pg_type') AS objcomment
        FROM pg_catalog.pg_type t
        JOIN pg_catalog.pg_namespace n ON n.oid = t.typnamespace
        WHERE n.nspname != 'information_schema' AND n.nspname !~ '^pg_'
        AND t.typtype = 'e'
        ORDER BY t.oid;
    ''').fetchall()])

def get_range_types(pg):
    return OrderedDict([(tuple(x.qualname), x) for x in pg.execute('''
        SELECT
            array[n.nspname, t.typname] AS qualname,
            pg_catalog.pg_get_userbyid(t.typowner) AS rolname,
            (SELECT array[n.nspname, col.collname] FROM pg_catalog.pg_collation col JOIN pg_catalog.pg_namespace n ON n.oid = col.collnamespace WHERE col.oid = r.rngcollation) AS collation,
            (SELECT array[n.nspname, t.typname] FROM pg_catalog.pg_type t JOIN pg_catalog.pg_namespace n ON n.oid = t.typnamespace WHERE t.oid = r.rngsubtype) AS subtype,
            (SELECT array[n.nspname, x.typname] FROM pg_catalog.pg_type x JOIN pg_catalog.pg_namespace n ON n.oid = t.typnamespace WHERE x.oid = (pg_temp.safe_catalog_column('pg_range', 'rngmultitypid', 'rngtypid = ' || t.oid::text, NULL))::oid) AS multirange_type_name,
            (SELECT array[n.nspname, oc.opcname] FROM pg_catalog.pg_opclass oc JOIN pg_catalog.pg_namespace n ON oc.opcnamespace = n.oid WHERE oc.oid = r.rngsubopc) AS subtype_operator_class,
            (SELECT
                json_build_object(
                    'name', array[n.nspname, p.proname],
                    'def', pg_catalog.pg_get_functiondef(p.oid)
                ) FROM pg_catalog.pg_proc p JOIN pg_catalog.pg_namespace n ON p.pronamespace = n.oid WHERE p.oid = r.rngcanonical) AS canonical_function,
            (SELECT array[n.nspname, p.proname] FROM pg_catalog.pg_proc p JOIN pg_catalog.pg_namespace n ON p.pronamespace = n.oid WHERE p.oid = r.rngsubdiff) AS subtype_diff_function,
            pg_catalog.obj_description(t.oid, 'pg_type') AS objcomment
        FROM pg_catalog.pg_type t
        JOIN pg_catalog.pg_range r ON r.rngtypid = t.oid
        JOIN pg_catalog.pg_namespace n ON n.oid = t.typnamespace
        WHERE n.nspname != 'information_schema' AND n.nspname !~ '^pg_'
        AND t.typtype = 'r'
        ORDER BY t.oid;
    ''').fetchall()])

def get_domain_types(pg):
    return OrderedDict([(tuple(x.qualname), x) for x in pg.execute('''
        SELECT
            array[n.nspname, t.typname] AS qualname, t.typnotnull, t.typndims, t.typtypmod,
            pg_catalog.pg_get_expr(t.typdefaultbin, 0) AS ddefaultexp,
            -- format_type only adds up to one set of array brackets, but additionals are needed for types like text[][]
            pg_catalog.format_type(t.typbasetype, t.typtypmod) || repeat('[]', t.typndims - 1) AS domaintype,
            pg_catalog.pg_get_userbyid(t.typowner) AS rolname,
            (SELECT x.collname FROM pg_catalog.pg_collation x WHERE x.oid = t.typcollation) AS collname,
            constraints.domainconstraints,
            pg_catalog.obj_description(t.oid, 'pg_type') AS objcomment
        FROM pg_catalog.pg_type t
        JOIN pg_catalog.pg_namespace n ON n.oid = t.typnamespace
        LEFT JOIN LATERAL (
            SELECT array_agg(array[x.conname::text, pg_catalog.pg_get_constraintdef(x.oid, false)] ORDER BY x.conname) AS domainconstraints
            FROM pg_catalog.pg_constraint x
            WHERE t.oid = x.contypid
        ) AS constraints ON true
        WHERE n.nspname != 'information_schema' AND n.nspname !~ '^pg_'
        AND t.typtype = 'd'
        ORDER BY t.oid;
    ''').fetchall()])

def get_functions(pg):
    return OrderedDict([(tuple(x.qualname), x) for x in pg.execute('''
        SELECT
            array[n.nspname::text, f.proname::text, pg_catalog.pg_get_function_identity_arguments(f.oid)] AS qualname,
            f.procost, f.prorows, f.prokind, f.prosecdef, f.proleakproof, f.proisstrict,
            f.proretset, f.provolatile, f.proparallel, f.pronargs, f.pronargdefaults, f.proargmodes, f.proargnames,
            f.prosrc, f.probin, f.proconfig, f.proacl,
            (
                SELECT array_agg(e.extname ORDER BY e.extname) AS extension_names
                FROM pg_extension e
                WHERE e.oid = any(
                    SELECT d.refobjid
                    FROM pg_catalog.pg_depend d
                    WHERE d.objid = f.oid
                    AND d.classid = 'pg_catalog.pg_proc'::regclass
                    AND d.refclassid = 'pg_catalog.pg_extension'::regclass
                )
            ) AS extension_names,
            (SELECT array[y.nspname, x.proname] FROM pg_catalog.pg_proc x JOIN pg_catalog.pg_namespace y ON x.pronamespace = y.oid WHERE x.oid = f.prosupport) AS prosupport_func,
            (SELECT array[y.nspname::text, pg_catalog.format_type(x.oid, x.typtypmod)] FROM pg_catalog.pg_type x JOIN pg_catalog.pg_namespace y ON x.typnamespace = y.oid WHERE x.oid = f.prorettype) AS funcreturn_type,
            (SELECT array_agg(array[y.nspname::text, pg_catalog.format_type(x.oid, x.typtypmod)] ORDER BY y.nspname, x.typname) FROM pg_catalog.pg_type x JOIN pg_catalog.pg_namespace y ON x.typnamespace = y.oid WHERE x.oid = any(f.protrftypes)) AS functransform_types,
            pg_catalog.pg_get_userbyid(f.proowner) AS rolname,
            pg_catalog.pg_get_functiondef(f.oid) AS functiondef,
            pg_catalog.pg_get_function_arguments(f.oid) AS functionargs,
            pg_catalog.obj_description(f.oid, 'pg_proc') AS objcomment
        FROM pg_catalog.pg_proc f
        JOIN pg_catalog.pg_namespace n ON n.oid = f.pronamespace
        WHERE n.nspname != 'information_schema' AND n.nspname !~ '^pg_' AND f.prokind != 'a'
        ORDER BY f.oid;
    ''').fetchall()])

def get_indexes(pg):
    return OrderedDict([(tuple(x.qualname), x) for x in pg.execute('''
        SELECT
            array[n.nspname, c.relname] AS qualname, c.relpersistence, c.relispartition, c.relacl,
            am.amname, c.reloptions, tblspc.spcname,
            i.indnatts, i.indnkeyatts, i.indisunique, i.indisexclusion, i.indisprimary,
            pg_temp.safe_catalog_column('pg_index', 'indnullsnotdistinct', 'indexrelid = ' || i.indexrelid::text, 'false')::boolean AS indnullsnotdistinct,
            i.indimmediate, i.indisreplident, i.indoption,
            idxcolstats.*, idxcols.*, pg_catalog.pg_get_expr(i.indexprs, i.indrelid) AS idxexpressions,
            pg_catalog.pg_get_expr(i.indpred, i.indrelid) AS idxpartialexp,
            pg_get_indexdef(c.oid) AS indexdef,
            (SELECT array_agg(e.extname ORDER BY e.extname) FROM pg_extension e WHERE e.oid = d.refobjid) AS extension_names,
            (SELECT array[y.nspname, x.relname] FROM pg_class x JOIN pg_catalog.pg_namespace y ON x.relnamespace = y.oid WHERE x.oid = i.indrelid) AS idxtable,
            pg_catalog.pg_get_userbyid(c.relowner) AS rolname,
            pg_catalog.obj_description(c.oid, 'pg_class') AS objcomment
        FROM pg_catalog.pg_class c
        JOIN pg_catalog.pg_namespace n ON n.oid = c.relnamespace
        LEFT JOIN pg_index i ON i.indexrelid = c.oid
        LEFT JOIN pg_catalog.pg_am am ON am.oid = c.relam
        LEFT JOIN pg_catalog.pg_tablespace tblspc ON tblspc.oid = c.reltablespace
        LEFT JOIN pg_catalog.pg_depend d ON d.objid = c.oid AND d.classid = 'pg_catalog.pg_class'::regclass AND d.refclassid = 'pg_catalog.pg_extension'::regclass
        LEFT JOIN LATERAL (
            SELECT array_agg(array[a.attnum, a.attstattarget] ORDER BY a.attname) AS idxcolstats
            FROM pg_catalog.pg_attribute a
            WHERE a.attrelid = c.oid
            AND a.attstattarget >= 0
        ) AS idxcolstats ON true
        LEFT JOIN LATERAL ( -- columns in the index key (indnkeyatts) with trailing INCLUDE columns (indnatts)
            WITH cols AS (
                SELECT
                    unnest(i.indkey) AS colid,
                    unnest(i.indclass) AS opclassid,
                    unnest(i.indcollation) AS collationid
            )
            SELECT array_agg(array[att.attname, opclass.opcname, collat.collname] ORDER BY array_position(i.indkey, att.attnum)) AS idxcols
            FROM cols
            LEFT JOIN pg_catalog.pg_attribute att ON att.attrelid = i.indrelid AND att.attnum = cols.colid
            JOIN pg_catalog.pg_opclass opclass ON opclass.oid = cols.opclassid
            LEFT JOIN pg_catalog.pg_collation collat ON collat.oid = cols.collationid
        ) AS idxcols ON true
        WHERE n.nspname != 'information_schema' AND n.nspname !~ '^pg_' AND NOT i.indisexclusion
        AND c.relkind IN ('i', 'I')
        ORDER BY c.oid;
    ''').fetchall()])

def get_views(pg):
    return OrderedDict([(tuple(x.qualname), x) for x in pg.execute('''
        SELECT
            array[n.nspname, c.relname] AS qualname,
            c.relpersistence,
            c.reloptions,
            pg_catalog.pg_get_viewdef(c.oid) AS viewdef,
            pg_catalog.pg_get_userbyid(c.relowner) AS rolname,
            columns.viewcolumns,
            pg_catalog.obj_description(c.oid, 'pg_class') AS objcomment
        FROM pg_catalog.pg_class c
        JOIN pg_catalog.pg_namespace n ON n.oid = c.relnamespace
        LEFT JOIN LATERAL (
            SELECT
                array_agg(array[a.attname::text,
                    pg_catalog.format_type(columntype.oid, columntype.typtypmod),
                    a.atthasdef::text,
                    pg_catalog.pg_get_expr(attrdef.adbin, attrdef.adrelid)] ORDER BY a.attnum) AS viewcolumns
            FROM pg_catalog.pg_attribute a
            JOIN pg_catalog.pg_type columntype ON columntype.oid = a.atttypid
            LEFT JOIN pg_catalog.pg_attrdef attrdef ON attrdef.adrelid = a.attrelid AND attrdef.adnum = a.attnum
            WHERE a.attrelid = c.oid
        ) AS columns ON true
        WHERE n.nspname != 'information_schema' AND n.nspname !~ '^pg_'
        AND c.relkind = 'v'
        ORDER BY c.oid;
    ''').fetchall()])

def get_triggers(pg):
    return OrderedDict([(tuple(x.qualname), x) for x in pg.execute('''
        SELECT
            array[n.nspname, c.relname, t.tgname] AS qualname,
            t.tgisinternal, t.tgdeferrable, t.tginitdeferred, t.tgnargs, t.tgattr, t.tgargs, t.tgoldtable,
            t.tgnewtable, t.tgenabled,
            extension_names.extension_names,
            array[n.nspname, f.proname] as triggerfunc_qualname,
            pg_catalog.pg_get_functiondef(f.oid) AS triggerfuncdef,
            pg_catalog.pg_get_triggerdef(t.oid) AS triggerdef,
            pg_catalog.obj_description(t.oid, 'pg_trigger') AS objcomment
        FROM pg_catalog.pg_trigger t
        JOIN pg_catalog.pg_class c ON c.oid = t.tgrelid
        JOIN pg_catalog.pg_namespace n ON n.oid = c.relnamespace
        JOIN pg_catalog.pg_proc f ON f.oid = t.tgfoid
        LEFT JOIN LATERAL (
            SELECT array_agg(e.extname ORDER BY e.extname) AS extension_names
            FROM pg_catalog.pg_depend d
            JOIN pg_catalog.pg_extension e ON e.oid = d.refobjid
            WHERE t.oid = d.objid
            AND d.classid = 'pg_catalog.pg_trigger'::regclass
            AND d.refclassid = 'pg_catalog.pg_extension'::regclass
        ) AS extension_names ON true
        WHERE n.nspname != 'information_schema' AND n.nspname !~ '^pg_'
        AND not t.tgisinternal
        ORDER BY t.oid;
    ''').fetchall()])


def get_installed_extensions(pg):
    return OrderedDict([(tuple(x.qualname), x) for x in pg.execute('''
        SELECT
            array[pv.name] AS qualname,
            coalesce(pv.schema, 'public') AS nspname,
            pv.version,
            pv.installed,
            pv.superuser,
            pv.requires,
            pv.comment
        FROM pg_catalog.pg_available_extension_versions pv
        WHERE pv.installed
        ORDER BY pv.schema, pv.name;
    ''').fetchall()])

def get_installable_extensions(pg):
    return OrderedDict([(tuple(x.qualname), x) for x in pg.execute('''
        SELECT
            array[pv.name] AS qualname,
            pv.version,
            coalesce(pv.schema, 'public') AS nspname,
            pv.installed,
            pv.superuser,
            pv.requires,
            pv.comment
        FROM pg_catalog.pg_available_extension_versions pv
        ORDER BY name;
    ''').fetchall()])

def get_extensions(pg):
    return OrderedDict([(tuple(x.qualname), x) for x in pg.execute('''
        SELECT
            array[e.extname] AS qualname,
            n.nspname,
            e.extversion AS version,
            extrelocatable,
            members.members,
            shared_members.members AS shared_members
        FROM pg_catalog.pg_extension e
        LEFT JOIN pg_catalog.pg_namespace n ON n.oid = e.extnamespace
        LEFT JOIN LATERAL (
            WITH d AS (
                SELECT * FROM pg_catalog.pg_depend d WHERE (d.refclassid = 'pg_catalog.pg_extension'::regclass) AND (d.refobjid = e.oid)
            ),
            funcs AS (
                SELECT
                np.nspname,
                p.proname,
                p.prokind,
                pg_catalog.pg_get_function_identity_arguments(p.oid) AS argtype
                FROM pg_catalog.pg_proc AS p
                JOIN d ON d.classid = 'pg_catalog.pg_proc'::regclass AND p.oid = d.objid
                JOIN pg_catalog.pg_namespace np ON np.oid = p.pronamespace
            ),
            classes AS (
                SELECT
                    n.nspname,
                    c.relname,
                    c.relkind
                FROM pg_catalog.pg_class c
                JOIN d ON d.classid = 'pg_catalog.pg_class'::regclass AND c.oid = d.objid
                JOIN pg_catalog.pg_namespace np ON np.oid = c.relnamespace
            ),
            evts AS (
                SELECT
                e.evtname
                FROM pg_catalog.pg_event_trigger e
                JOIN d ON d.classid = 'pg_catalog.pg_event_trigger'::regclass AND e.oid = d.objid
            ),
            cols AS (
                SELECT
                np.nspname,
                c.collname
                FROM pg_catalog.pg_collation c
                JOIN d ON d.classid = 'pg_catalog.pg_collation'::regclass AND c.oid = d.objid
                JOIN pg_catalog.pg_namespace np ON np.oid = c.collnamespace
            ),
            convs AS (
                SELECT
                np.nspname,
                c.conname
                FROM pg_catalog.pg_conversion c
                JOIN d ON d.classid = 'pg_catalog.pg_conversion'::regclass AND c.oid = d.objid
                JOIN pg_catalog.pg_namespace np ON np.oid = c.connamespace
            ),
            types AS (
                SELECT
                    n.nspname,
                    t.typname,
                    t.typtype
                FROM pg_catalog.pg_type t
                JOIN d ON d.classid = 'pg_catalog.pg_type'::regclass AND t.oid = d.objid
                JOIN pg_catalog.pg_namespace np ON np.oid = t.typnamespace
            ),
            ops AS (
                SELECT
                    (SELECT n.nspname FROM pg_catalog.pg_namespace n WHERE n.oid = o.oprnamespace) AS nspname,
                    o.oprname,
                    (SELECT n.nspname FROM pg_catalog.pg_namespace n WHERE n.oid = lty.typnamespace) AS lnspname,
                    lty.typname AS ltypname,
                    (SELECT n.nspname FROM pg_catalog.pg_namespace n WHERE n.oid = rty.typnamespace) AS rnspname,
                    rty.typname AS rtypname
                FROM pg_catalog.pg_operator o
                JOIN d ON d.classid = (SELECT oid FROM pg_catalog.pg_class WHERE relname = 'pg_operator') AND o.oid = d.objid
                JOIN pg_catalog.pg_type lty ON o.oprleft = lty.oid
                JOIN pg_catalog.pg_type rty ON o.oprright = rty.oid
            ),
            opc AS (
                SELECT
                    n.nspname,
                    o.opcname,
                    am.amname
                FROM pg_catalog.pg_opclass o
                JOIN d ON d.classid = 'pg_catalog.pg_opclass'::regclass AND o.oid = d.objid
                JOIN pg_catalog.pg_namespace np ON np.oid = o.opcnamespace
                JOIN pg_catalog.pg_am am ON am.oid = o.opcmethod
            ),
            opf AS (
                SELECT
                    n.nspname,
                    o.opfname,
                    am.amname
                FROM pg_catalog.pg_opfamily o
                JOIN d ON d.classid = 'pg_catalog.pg_opfamily'::regclass AND o.oid = d.objid
                JOIN pg_catalog.pg_namespace np ON np.oid = o.opfnamespace
                JOIN pg_catalog.pg_am am ON am.oid = o.opfmethod
            ),
            tsc AS (
                SELECT
                    n.nspname,
                    t.cfgname
                FROM pg_catalog.pg_ts_config t
                JOIN d ON d.classid = 'pg_catalog.pg_ts_config'::regclass AND t.oid = d.objid
                JOIN pg_catalog.pg_namespace np ON np.oid = t.cfgnamespace
            ),
            tsd AS (
                SELECT
                    n.nspname,
                    t.dictname
                FROM pg_catalog.pg_ts_dict t
                JOIN d ON d.classid = 'pg_catalog.pg_ts_dict'::regclass AND t.oid = d.objid
                JOIN pg_catalog.pg_namespace np ON np.oid = t.dictnamespace
            ),
            tsp AS (
                SELECT
                    n.nspname,
                    t.prsname
                FROM pg_catalog.pg_ts_parser t
                JOIN d ON d.classid = 'pg_catalog.pg_ts_parser'::regclass AND t.oid = d.objid
                JOIN pg_catalog.pg_namespace np ON np.oid = t.prsnamespace
            ),
            tst AS (
                SELECT
                    n.nspname,
                    t.tmplname
                FROM pg_catalog.pg_ts_template t
                JOIN d ON d.classid = 'pg_catalog.pg_ts_template'::regclass AND t.oid = d.objid
                JOIN pg_catalog.pg_namespace np ON np.oid = t.tmplnamespace
            ),
            schemas AS (
                SELECT
                    n.nspname
                FROM pg_catalog.pg_namespace n
                JOIN d ON d.classid = 'pg_catalog.pg_namespace'::regclass AND n.oid = d.objid
            ),
            langs AS (
                SELECT
                    l.lanname,
                    (   SELECT array_agg(t.typname ORDER BY t.typname)
                        FROM pg_catalog.pg_transform tr
                        JOIN pg_catalog.pg_type t ON t.oid = tr.trftype
                        WHERE tr.trflang = l.oid
                    ) AS typname
                FROM pg_catalog.pg_language l
                JOIN d ON d.classid = 'pg_catalog.pg_language'::regclass AND l.oid = d.objid
            ),
            fd_wrappers AS (
                SELECT fdwname FROM pg_catalog.pg_foreign_data_wrapper fdw
                JOIN d ON d.classid = 'pg_catalog.pg_class'::regclass
                AND fdw.oid = d.objid
            ),
            casts AS (
                SELECT
                    ARRAY[ns.nspname, sty.typname] AS source_type,
                    ARRAY[nt.nspname, tty.typname] AS tgt_type
                FROM pg_catalog.pg_cast c
                JOIN d ON d.classid = 'pg_catalog.pg_cast'::regclass AND c.oid = d.objid
                JOIN pg_catalog.pg_type sty on c.castsource = sty.oid
                JOIN pg_catalog.pg_namespace ns on ns.oid = sty.typnamespace
                JOIN pg_catalog.pg_type tty on c.casttarget = tty.oid
                JOIN pg_catalog.pg_namespace nt on nt.oid = tty.typnamespace
            ),
            am AS (
                SELECT
                    a.amname
                FROM pg_catalog.pg_am a
                JOIN d ON d.classid = 'pg_catalog.pg_am'::regclass AND a.oid = d.objid
            )
            SELECT json_build_object(
                -- 'FUNCTION', ( SELECT array_agg(array[f.nspname::text, f.proname::text, f.argtype] ORDER BY f.proname, f.nspname, f.argtype) FROM funcs AS f WHERE prokind = 'f'),
                'AGGREGATE', ( SELECT array_agg(array[f.nspname::text, f.proname::text, f.argtype] ORDER BY f.proname, f.nspname, f.argtype) FROM funcs AS f WHERE prokind = 'a'),
                -- 'PROCEDURE', ( SELECT array_agg(array[f.nspname, f.proname, f.argtype] ORDER BY f.proname, f.nspname, f.argtype) FROM funcs AS f WHERE prokind = 'p'),
                'COLLATION', (SELECT array_agg(array[c.nspname, c.collname] ORDER BY c.collname, c.nspname) FROM cols c),
                'CONVERSION', (SELECT array_agg(array[c.nspname, c.conname] ORDER BY c.conname, c.nspname) FROM convs c),
                'EVENT TRIGGER', (SELECT array_agg(e.evtname ORDER BY e.evtname) FROM evts e),
                'TABLE', (SELECT array_agg(array[c.nspname, c.relname] ORDER BY c.relname, c.nspname) FROM classes c WHERE c.relkind = 'r'),
                'SEQUENCE', (SELECT array_agg(array[c.nspname, c.relname] ORDER BY c.relname, c.nspname) FROM classes c WHERE c.relkind = 'S'),
                'VIEW', (SELECT array_agg(array[c.nspname, c.relname] ORDER BY c.relname, c.nspname) FROM classes c WHERE c.relkind = 'v'),
                'MATERIALIZED VIEW', (SELECT array_agg(array[c.nspname, c.relname] ORDER BY c.relname, c.nspname) FROM classes c WHERE c.relkind = 'm'),
                'FOREIGN TABLE', (SELECT array_agg(array[c.nspname, c.relname] ORDER BY c.relname) FROM classes c WHERE c.relkind = 'f'),
                'TYPE', (SELECT array_agg(array[ty.nspname, ty.typname] ORDER BY ty.typname, ty.nspname) FROM types ty WHERE ty.typtype = 'c'),
                'DOMAIN', (SELECT array_agg(array[ty.nspname, ty.typname] ORDER BY ty.typname, ty.nspname) FROM types ty WHERE ty.typtype = 'd'),
                'OPERATOR', (SELECT array_agg(array[x.nspname, x.oprname, x.lnspname, x.ltypname, x.rnspname, x.rtypname] ORDER BY x.oprname, x.nspname) FROM ops x),
                'OPERATOR CLASS', (SELECT array_agg(array[nspname, opcname, amname] ORDER BY opc.opcname, nspname, amname) FROM opc),
                'OPERATOR FAMILY', (SELECT array_agg(array[nspname, opfname, amname] ORDER BY opf.opfname, nspname, amname) FROM opf),
                'TEXT SEARCH CONFIGURATION', (SELECT array_agg(array[nspname, cfgname] ORDER BY tsc.cfgname, nspname) FROM tsc),
                'TEXT SEARCH DICTIONARY', (SELECT array_agg(array[nspname, dictname] ORDER BY tsd.dictname, nspname) FROM tsd),
                'TEXT SEARCH PARSER', (SELECT array_agg(array[nspname, prsname] ORDER BY tsp.prsname, nspname) FROM tsp),
                'TEXT SEARCH TEMPLATE', (SELECT array_agg(array[nspname, tmplname] ORDER BY tst.tmplname, nspname) FROM tst),
                'SCHEMA', (SELECT array_agg(nspname ORDER BY nspname) FROM schemas),
                'LANGUAGE', (SELECT array_agg(lanname ORDER BY lanname) FROM langs),
                'TRANSFORM', (SELECT json_object_agg(lanname, typname ORDER BY lanname) FROM langs),
                'FOREIGN DATA WRAPPER', (SELECT array_agg(fdwname ORDER BY fdwname) FROM fd_wrappers),
                'CAST', (SELECT array_agg(array[source_type, tgt_type] ORDER BY source_type, tgt_type ) FROM casts),
                'ACCESS METHOD', (SELECT array_agg(am.amname ORDER BY am.amname) FROM am)
            ) AS members
        ) AS members ON true
        LEFT JOIN LATERAL (
            WITH sd AS (
                SELECT * FROM pg_catalog.pg_shdepend sd WHERE sd.refclassid = 'pg_catalog.pg_extension'::regclass AND sd.refobjid = e.oid
            ),
            servers AS (
                SELECT
                    fs.srvname
                FROM pg_catalog.pg_foreign_server fs
                JOIN sd ON sd.classid = 'pg_catalog.pg_foreign_server'::regclass AND fs.oid = sd.objid
            )
            SELECT json_build_object(
                'SERVER', (SELECT array_agg(srvname ORDER BY srvname) FROM servers)
            ) AS members
        ) AS shared_members ON true
        ORDER BY e.oid;
    ''').fetchall()])

def get_roles(pg):
    return OrderedDict([(tuple(x.qualname), x) for x in pg.execute('''
        SELECT
            ARRAY[r.rolname] AS qualname,
            pg_catalog.obj_description(r.oid, 'pg_authid') AS objcomment,
            json_build_object(
                'SUPERUSER', r.rolsuper,
                'PASSWORD', r.rolpassword,
                'INHERIT', r.rolinherit,
                'CREATEROLE', r.rolcreaterole,
                'CREATEDB', r.rolcreatedb,
                'LOGIN', r.rolcanlogin,
                'REPLICATION', r.rolreplication,
                'BYPASSRLS', r.rolbypassrls,
                'CONNECTION LIMIT', r.rolconnlimit,
                'VALID UNTIL', r.rolvaliduntil
            ) AS options,
            (
                SELECT array_agg(a.rolname ORDER BY a.rolname) FROM pg_catalog.pg_authid a WHERE a.oid = any(members.oids)
            ) AS rolmembers,
            default_db_configs.c AS db_configs,
            default_all_db_configs.c AS all_configs
        FROM pg_catalog.pg_authid r
        LEFT JOIN LATERAL (
            SELECT
                array_agg(json_build_object(d.datname, rs.setconfig) ORDER BY d.datname, rs.setconfig) AS c
            FROM pg_catalog.pg_db_role_setting rs
            JOIN pg_catalog.pg_database d ON d.oid = rs.setdatabase
            WHERE rs.setrole = r.oid
        ) AS default_db_configs ON true
        LEFT JOIN LATERAL (
            SELECT
                rs.setconfig AS c
            FROM pg_catalog.pg_db_role_setting rs
            WHERE rs.setrole = r.oid AND rs.setdatabase = 0
        ) AS default_all_db_configs ON true
        LEFT JOIN LATERAL (
            SELECT array_agg(m.member ORDER BY m.member) AS oids from pg_catalog.pg_auth_members m WHERE r.oid = m.roleid
        ) AS members ON true
        WHERE r.rolname !~ 'pg_.*'
        ORDER BY r.oid;
    ''').fetchall()])

def get_casts(pg):
    return OrderedDict([(tuple(x.qualname), x) for x in pg.execute('''
        SELECT
            ARRAY[CONCAT(ns.nspname, sty.typname, nt.nspname, tty.typname)] AS qualname,
            c.castcontext,
            c.castmethod,
            ARRAY[ns.nspname, sty.typname] AS source_type,
            ARRAY[nt.nspname, tty.typname] AS tgt_type,
            ARRAY[np.nspname, p.proname] AS funcname,
            pg_catalog.pg_get_function_identity_arguments(p.oid) AS funcargs,
            pg_catalog.obj_description(c.oid, 'pg_cast') AS objcomment
        FROM pg_catalog.pg_cast c
        JOIN pg_catalog.pg_type sty on c.castsource = sty.oid
        JOIN pg_catalog.pg_namespace ns on ns.oid = sty.typnamespace
        JOIN pg_catalog.pg_type tty on c.casttarget = tty.oid
        JOIN pg_catalog.pg_namespace nt on nt.oid = tty.typnamespace
        LEFT JOIN pg_catalog.pg_proc p ON p.oid = c.castfunc
        LEFT JOIN pg_catalog.pg_namespace np ON np.oid = p.pronamespace
        WHERE np.nspname !~ 'pg_.*'
        ORDER BY c.oid;
        ''').fetchall()])

def get_event_trgs(pg):
    return OrderedDict([(x.qualname, x) for x in pg.execute('''
        SELECT
            e.evtname AS qualname,
            e.evtevent,
            e.evtenabled,
            array[np.nspname, p.proname] AS funcname,
            p.prokind AS functype,
            r.rolname AS rolname,
            pg_catalog.obj_description(e.oid, 'pg_event_trigger') AS objcomment
        FROM pg_catalog.pg_event_trigger e
        JOIN pg_catalog.pg_authid r on r.oid = e.evtowner
        JOIN pg_catalog.pg_proc p on p.oid = e.evtfoid
        JOIN pg_catalog.pg_namespace np on np.oid = p.pronamespace
        WHERE np.nspname !~ 'pg_.*'
        ORDER BY e.oid;
    ''').fetchall()])

def get_aggregates(pg):
    return OrderedDict([(tuple(x.qualname), x) for x in pg.execute('''
        SELECT
            ARRAY[np.nspname, pr.proname] AS qualname,
            pg_catalog.pg_get_function_identity_arguments(pr.oid) AS argtype,
            au.rolname AS owner,
            a.aggkind,
            a.aggnumdirectargs,
            pg_catalog.obj_description(pr.oid, 'pg_proc') AS objcomment,
            pr.proparallel,
            (SELECT ARRAY[n.nspname, p.proname] FROM pg_catalog.pg_proc p JOIN pg_catalog.pg_namespace n on p.pronamespace = n.oid WHERE p.oid = a.aggtransfn) AS aggtransfn,
            (SELECT ARRAY[n.nspname, t.typname] FROM pg_catalog.pg_type t JOIN pg_catalog.pg_namespace n ON t.typnamespace = n.oid WHERE t.oid = a.aggtranstype) AS aggtranstype,
            json_build_object(
                'INITCOND', a.agginitval,
                'MINITCOND', a.aggminitval,
                'FINALFUNC_EXTRA', a.aggfinalextra,
                'MFINALFUNC_EXTRA', a.aggmfinalextra,
                'FINALFUNC_MODIFY', a.aggfinalmodify,
                'MFINALFUNC_MODIFY', a.aggmfinalmodify,
                'SSPACE', a.aggtransspace,
                'MSSPACE', a.aggmtransspace,
                'FINALFUNC', (SELECT ARRAY[n.nspname, p.proname] FROM pg_catalog.pg_proc p JOIN pg_catalog.pg_namespace n on p.pronamespace = n.oid WHERE p.oid = a.aggfinalfn),
                'COMBINEFUNC', (SELECT ARRAY[n.nspname, p.proname] FROM pg_catalog.pg_proc p JOIN pg_catalog.pg_namespace n on p.pronamespace = n.oid WHERE p.oid = a.aggcombinefn),
                'SERIALFUNC', (SELECT ARRAY[n.nspname, p.proname] FROM pg_catalog.pg_proc p JOIN pg_catalog.pg_namespace n on p.pronamespace = n.oid WHERE p.oid = a.aggserialfn),
                'DESERIALFUNC', (SELECT ARRAY[n.nspname, p.proname] FROM pg_catalog.pg_proc p JOIN pg_catalog.pg_namespace n on p.pronamespace = n.oid WHERE p.oid = a.aggdeserialfn),
                'MSFUNC', (SELECT ARRAY[n.nspname, p.proname] FROM pg_catalog.pg_proc p JOIN pg_catalog.pg_namespace n on p.pronamespace = n.oid WHERE p.oid = a.aggmtransfn),
                'MINVFUNC', (SELECT ARRAY[n.nspname, p.proname] FROM pg_catalog.pg_proc p JOIN pg_catalog.pg_namespace n on p.pronamespace = n.oid WHERE p.oid = a.aggminvtransfn),
                'MFINALFUNC', (SELECT ARRAY[n.nspname, p.proname] FROM pg_catalog.pg_proc p JOIN pg_catalog.pg_namespace n on p.pronamespace = n.oid WHERE p.oid = a.aggmfinalfn),
                'SORTOP', (SELECT ARRAY[n.nspname, o.oprname] FROM pg_catalog.pg_operator o LEFT JOIN pg_catalog.pg_namespace n ON o.oprnamespace = n.oid WHERE o.oid = a.aggsortop),
                'MSTYPE', (SELECT ARRAY[n.nspname, t.typname] FROM pg_catalog.pg_type t JOIN pg_catalog.pg_namespace n ON t.typnamespace = n.oid WHERE t.oid = a.aggmtranstype)
            ) AS opts
        FROM pg_catalog.pg_aggregate a
        JOIN pg_catalog.pg_proc pr ON a.aggfnoid = pr.oid
        JOIN pg_catalog.pg_namespace np ON pr.pronamespace = np.oid AND np.nspname !~ 'pg_.*'
        JOIN pg_catalog.pg_authid au ON pr.proowner = au.oid
        LEFT JOIN pg_catalog.pg_operator op ON op.oid = a.aggsortop
        ORDER BY pr.oid;
    ''').fetchall()])

def get_dbs(pg):
    return OrderedDict([(tuple(x.qualname), x) for x in pg.execute('''
        SELECT
            ARRAY[d.datname] AS qualname,
            pg_catalog.obj_description(d.oid, 'pg_database') AS objcomment,
            json_build_object(
                'OWNER', a.rolname,
                'ENCODING', pg_catalog.pg_encoding_to_char(d.encoding),
                'LC_COLLATE', d.datcollate,
                'LC_CTYPE', d.datctype,
                'ICU_LOCALE', pg_temp.safe_catalog_column('pg_database', 'daticulocale', 'oid = ' || d.oid, ''),
                'LOCALE_PROVIDER', pg_temp.safe_catalog_column('pg_database', 'datlocprovider', 'oid = ' || d.oid, '')::char,
                'TABLESPACE', t.spcname,
                'ALLOW_CONNECTIONS', d.datallowconn,
                'CONNECTION LIMIT', d.datconnlimit,
                'IS_TEMPLATE', d.datistemplate
            ) AS opts
        FROM pg_catalog.pg_database d
        LEFT JOIN pg_catalog.pg_authid a ON a.oid = d.datdba
        LEFT JOIN pg_catalog.pg_tablespace t ON t.oid = d.dattablespace
        where d.datname NOT IN ('postgres', 'template0', 'template1')
        ORDER BY d.oid;
    ''').fetchall()])

def get_publications(pg):
    return OrderedDict([(tuple(x.qualname), x) for x in pg.execute('''
        SELECT
            ARRAY[p.pubname] AS qualname,
            a.rolname AS owner,
            p.puballtables,
            p.pubinsert,
            p.pubupdate,
            p.pubdelete,
            p.pubtruncate,
            (
                SELECT array_agg(n.nspname ORDER BY n.nspname) FROM pg_catalog.pg_namespace n where n.oid = ANY (
                    pg_temp.safe_catalog_column_multirow(
                        'pg_publication_namespace', 'pnnspid', 'pnpubid = ' || p.oid, '{}')::oid[]
                )
            ) AS schemas,
            pg_temp.safe_catalog_column(
                'pg_publication', 'pubviaroot', 'pg_publication.oid = ' || p.oid, 'false') AS pubviaroot,
            tables.tables,
            pg_catalog.obj_description(p.oid, 'pg_publication') AS objcomment
        FROM pg_catalog.pg_publication p
        JOIN pg_catalog.pg_authid a ON p.pubowner = a.oid
        LEFT JOIN LATERAL (
            SELECT
                json_agg(
                    json_build_object(
                        'name', n.nspname || '.' || c.relname,
                        'qual', pg_catalog.pg_get_expr(pg_temp.safe_catalog_column_pg_node_tree('pg_publication_rel', 'prqual', 'oid = ' || r.oid, null), r.prrelid),
                        'cols', (
                            SELECT array_agg(attname ORDER BY attname)
                            FROM pg_catalog.pg_attribute a
                            WHERE a.attrelid = r.prrelid
                            AND a.attnum = ANY(pg_temp.safe_get_int2vector('pg_publication_rel', 'prattrs', 'oid = ' || r.oid)))
                )) AS tables
            FROM pg_catalog.pg_publication_rel r
            JOIN pg_catalog.pg_class c ON c.oid = r.prrelid
            JOIN pg_catalog.pg_namespace n ON n.oid = c.relnamespace
            WHERE r.prpubid = p.oid
        ) AS tables ON true
        ORDER BY p.oid;
    ''').fetchall()])

def get_ts_dicts(pg):
    return OrderedDict([(tuple(x.qualname), x) for x in pg.execute('''
        SELECT
            ARRAY[n.nspname, d.dictname] AS qualname,
            d.dictinitoption,
            pg_catalog.pg_get_userbyid(d.dictowner) AS rolname,
            (
                SELECT ARRAY[np.nspname, t.tmplname]
                FROM pg_catalog.pg_ts_template t
                JOIN pg_catalog.pg_namespace np ON np.oid = t.tmplnamespace
                WHERE d.dicttemplate = t.oid
            ) AS tmplname,
            pg_catalog.obj_description(d.oid, 'pg_ts_dict') AS objcomment
        FROM pg_catalog.pg_ts_dict d
        JOIN pg_catalog.pg_namespace n ON n.oid = d.dictnamespace
        WHERE n.nspname !~ '^pg_'
        ORDER BY d.oid;
    ''').fetchall()])

def get_ts_configs(pg):
    return OrderedDict([(tuple(x.qualname), x) for x in pg.execute('''
        SELECT
            ARRAY[n.nspname, tc.cfgname] AS qualname,
            pg_catalog.pg_get_userbyid(tc.cfgowner) AS rolname,
            ARRAY[np.nspname, p.prsname] as parser,
            (
                WITH x AS (
                    SELECT
                        (SELECT alias FROM pg_catalog.ts_token_type(tc.cfgparser) AS t WHERE t.tokid = m.maptokentype) AS token,
                        array_agg(ARRAY[np.nspname, d.dictname] ORDER BY m.mapseqno) AS dicts
                    FROM pg_catalog.pg_ts_config_map m
                    JOIN pg_catalog.pg_ts_dict d ON d.oid = m.mapdict
                    JOIN pg_catalog.pg_namespace np ON np.oid = d.dictnamespace
                    WHERE m.mapcfg = tc.oid
                    GROUP BY m.maptokentype
                )
                SELECT json_object_agg(x.token, x.dicts) FROM x
            ) AS mappings,
            pg_catalog.obj_description(tc.oid, 'pg_ts_config') AS objcomment
        FROM pg_catalog.pg_ts_config tc
        JOIN pg_catalog.pg_namespace n ON n.oid = tc.cfgnamespace
        JOIN pg_catalog.pg_ts_parser p ON p.oid = tc.cfgparser
        JOIN pg_catalog.pg_namespace np ON np.oid = p.prsnamespace
        WHERE n.nspname !~ '^pg_'
        ORDER BY tc.oid;
    ''').fetchall()])

def get_ts_parsers(pg):
    return OrderedDict([(tuple(x.qualname), x) for x in pg.execute('''
        SELECT
            ARRAY[n.nspname, tp.prsname] AS qualname,
            (
                SELECT ARRAY[np.nspname, p.proname]
                FROM pg_catalog.pg_proc p
                JOIN pg_catalog.pg_namespace np ON np.oid = p.pronamespace
                WHERE p.oid = tp.prsstart
            ) AS prsstart,
            (
                SELECT ARRAY[np.nspname, p.proname]
                FROM pg_catalog.pg_proc p
                JOIN pg_catalog.pg_namespace np ON np.oid = p.pronamespace
                WHERE p.oid = tp.prstoken
            ) AS prstoken,
            (
                SELECT ARRAY[np.nspname, p.proname]
                FROM pg_catalog.pg_proc p
                JOIN pg_catalog.pg_namespace np ON np.oid = p.pronamespace
                WHERE p.oid = tp.prsend
            ) AS prsend,
            (
                SELECT ARRAY[np.nspname, p.proname]
                FROM pg_catalog.pg_proc p
                JOIN pg_catalog.pg_namespace np ON np.oid = p.pronamespace
                WHERE p.oid = tp.prsheadline
            ) AS prsheadline,
            (
                SELECT ARRAY[np.nspname, p.proname]
                FROM pg_catalog.pg_proc p
                JOIN pg_catalog.pg_namespace np ON np.oid = p.pronamespace
                WHERE p.oid = tp.prslextype
            ) AS prslextype,
            pg_catalog.obj_description(tp.oid, 'pg_ts_parser') AS objcomment
        FROM pg_catalog.pg_ts_parser tp
        JOIN pg_catalog.pg_namespace n ON n.oid = tp.prsnamespace
        WHERE n.nspname !~ '^pg_'
        ORDER BY tp.oid;
    ''').fetchall()])

def get_ts_templates(pg):
    return OrderedDict([(tuple(x.qualname), x) for x in pg.execute('''
        SELECT
            ARRAY[n.nspname, tt.tmplname] AS qualname,
            (
                SELECT ARRAY[np.nspname, p.proname]
                FROM pg_catalog.pg_proc p
                JOIN pg_catalog.pg_namespace np ON np.oid = p.pronamespace
                WHERE p.oid = tt.tmplinit
            ) AS tmplinit,
            (
                SELECT ARRAY[np.nspname, p.proname]
                FROM pg_catalog.pg_proc p
                JOIN pg_catalog.pg_namespace np ON np.oid = p.pronamespace
                WHERE p.oid = tt.tmpllexize
            ) AS tmpllexize,
            pg_catalog.obj_description(tt.oid, 'pg_ts_template') AS objcomment
        FROM pg_catalog.pg_ts_template tt
        JOIN pg_catalog.pg_namespace n ON n.oid = tt.tmplnamespace
        WHERE n.nspname !~ '^pg_'
        ORDER BY tt.oid;
    ''').fetchall()])

def get_operators(pg):
    return OrderedDict([(tuple(x.qualname), x) for x in pg.execute('''
        SELECT
            ARRAY[n.nspname, o.oprname] || oprleft.oprleft || oprright.oprright AS qualname,
            pg_catalog.pg_get_userbyid(o.oprowner) AS rolname,
            o.oprkind,
            o.oprcanmerge,
            o.oprcanhash,
            oprleft.oprleft,
            oprright.oprright,
            (
                SELECT ARRAY[np.nspname, op.oprname]
                FROM pg_catalog.pg_operator op JOIN pg_catalog.pg_namespace np ON np.oid = op.oprnamespace
                WHERE op.oid = o.oprcom
            ) AS oprcom,
            (
                SELECT ARRAY[np.nspname, op.oprname]
                FROM pg_catalog.pg_operator op JOIN pg_catalog.pg_namespace np ON np.oid = op.oprnamespace
                WHERE op.oid = o.oprnegate
            ) AS oprnegate,
            (
                SELECT ARRAY[np.nspname, p.proname]
                FROM pg_catalog.pg_proc p JOIN pg_catalog.pg_namespace np ON np.oid = p.pronamespace
                WHERE p.oid = o.oprcode
            ) AS oprcode,
            (
                SELECT ARRAY[np.nspname, p.proname]
                FROM pg_catalog.pg_proc p JOIN pg_catalog.pg_namespace np ON np.oid = p.pronamespace
                WHERE p.oid = o.oprrest
            ) AS oprrest,
            (
                SELECT ARRAY[np.nspname, p.proname]
                FROM pg_catalog.pg_proc p JOIN pg_catalog.pg_namespace np ON np.oid = p.pronamespace
                WHERE p.oid = o.oprjoin
            ) AS oprjoin,
            pg_catalog.obj_description(o.oid, 'pg_operator') AS objcomment
        FROM pg_catalog.pg_operator o
        JOIN pg_catalog.pg_namespace n ON n.oid = o.oprnamespace
        LEFT JOIN LATERAL (
            SELECT ARRAY[np.nspname, t.typname] AS oprleft
            FROM pg_catalog.pg_type t JOIN pg_catalog.pg_namespace np ON np.oid = t.typnamespace
            WHERE t.oid = o.oprleft
        ) AS oprleft ON true
        LEFT JOIN LATERAL (
            SELECT ARRAY[np.nspname, t.typname] AS oprright
            FROM pg_catalog.pg_type t JOIN pg_catalog.pg_namespace np ON np.oid = t.typnamespace
            WHERE t.oid = o.oprright
        ) AS oprright ON true
        WHERE n.nspname !~ 'pg_.*'
        ORDER BY o.oid;
    ''').fetchall()])

def get_languages(pg):
    return OrderedDict([(tuple(x.qualname), x) for x in pg.execute('''
        SELECT
            ARRAY[l.lanname] AS qualname,
            pg_catalog.pg_get_userbyid(l.lanowner) AS rolname,
            l.lanpltrusted,
            (
                SELECT ARRAY[n.nspname, p.proname]
                FROM pg_catalog.pg_proc p
                JOIN pg_catalog.pg_namespace n ON n.oid = p.pronamespace
                WHERE p.oid = l.lanplcallfoid
            ) AS callproc,
            (
                SELECT ARRAY[n.nspname, p.proname]
                FROM pg_catalog.pg_proc p
                JOIN pg_catalog.pg_namespace n ON n.oid = p.pronamespace
                WHERE p.oid = l.laninline
            ) AS inlineproc,
            (
                SELECT ARRAY[n.nspname, p.proname]
                FROM pg_catalog.pg_proc p
                JOIN pg_catalog.pg_namespace n ON n.oid = p.pronamespace
                WHERE p.oid = l.lanvalidator
            ) AS valproc,
            pg_catalog.obj_description(l.oid, 'pg_language') AS objcomment
        FROM pg_catalog.pg_language l
        WHERE l.lanispl
        ORDER BY l.oid;
    ''').fetchall()])

def get_collations(pg):
    return OrderedDict([(tuple(x.qualname), x) for x in pg.execute('''
        SELECT
            ARRAY [n.nspname, c.collname] as qualname,
            json_build_object(
                'LOCALE', pg_temp.safe_catalog_column('pg_collation', 'colliculocale', 'oid = ' || c.oid, '')::text,
                'LC_COLLATE', c.collcollate,
                'LC_CTYPE', c.collctype,
                'PROVIDER', c.collprovider,
                'DETERMINISTIC', c.collisdeterministic,
                'RULES',  pg_temp.safe_catalog_column('pg_collation', 'collicurules', 'oid = ' || c.oid, '')::text,
                'VERSION', c.collversion
            ) AS opts,
            pg_catalog.pg_get_userbyid(c.collowner) AS rolname,
            pg_catalog.obj_description(c.oid, 'pg_collation') AS objcomment
        FROM pg_catalog.pg_collation c
        JOIN pg_catalog.pg_namespace n ON n.oid = c.collnamespace
        WHERE n.nspname !~ 'pg_.*'
        ORDER BY c.oid;
    ''').fetchall()])

def get_conversions(pg):
    return OrderedDict([(tuple(x.qualname), x) for x in pg.execute('''
        SELECT
            ARRAY [n.nspname, c.conname] as qualname,
            c.condefault,
            pg_catalog.pg_encoding_to_char(c.conforencoding) AS source_encoding,
            pg_catalog.pg_encoding_to_char(c.contoencoding) AS dest_encoding,
            ARRAY[np.nspname, p.proname] AS function_name,
            pg_catalog.pg_get_userbyid(c.conowner) AS rolname,
            pg_catalog.obj_description(c.oid, 'pg_conversion') AS objcomment
        FROM pg_catalog.pg_conversion c
        JOIN pg_catalog.pg_namespace n ON n.oid = c.connamespace
        LEFT JOIN pg_catalog.pg_proc p ON p.oid = c.conproc
        LEFT JOIN pg_catalog.pg_namespace np ON np.oid = p.pronamespace
        WHERE n.nspname != 'information_schema' AND n.nspname !~ '^pg_'
        ORDER BY c.oid;
    ''').fetchall()])

def get_materialized_views(pg):
    return OrderedDict([(tuple(x.qualname), x) for x in pg.execute('''
        SELECT
            array[n.nspname, c.relname] AS qualname,
            c.relispopulated,
            c.reloptions,
            am.amname,
            tblspc.spcname,
            pg_catalog.pg_get_viewdef(c.oid) AS viewdef,
            pg_catalog.pg_get_userbyid(c.relowner) AS rolname,
            (SELECT array_agg(e.extname ORDER BY e.extname) FROM pg_catalog.pg_extension e WHERE e.oid = depends.refobjid) AS extension_names,
            columns.viewcolumns,
            pg_catalog.obj_description(c.oid, 'pg_class') AS objcomment,
            (
                SELECT
                    cl.relname
                FROM pg_catalog.pg_index i
                JOIN pg_catalog.pg_class cl ON i.indexrelid = cl.oid
                JOIN pg_catalog.pg_namespace n ON n.oid = cl.relnamespace
                WHERE i.indrelid = c.oid AND i.indisclustered
            ) AS cluster_index
        FROM pg_catalog.pg_class c
        JOIN pg_catalog.pg_namespace n ON n.oid = c.relnamespace
        LEFT JOIN LATERAL (
            SELECT
            json_agg(
                json_build_object(
                    'attname', a.attname,
                    'attstattarget', a.attstattarget,
                    'attstorage', a.attstorage,
                    'attoptions', a.attoptions,
                    'attcompression', pg_temp.safe_catalog_column('pg_attribute', 'attcompression', 'attrelid = ' || a.attrelid, '')::char
            )) AS viewcolumns
            FROM pg_catalog.pg_attribute a
            WHERE a.attrelid = c.oid AND a.attnum >= 1
        ) AS columns ON true
        LEFT JOIN LATERAL (
            SELECT d.refobjid
            FROM pg_catalog.pg_depend d
            WHERE c.oid = d.objid
            AND d.classid = 'pg_catalog.pg_extension'::regclass
            AND d.refclassid = 'pg_catalog.pg_class'::regclass
        ) AS depends ON true
        LEFT JOIN pg_catalog.pg_am am ON am.oid = c.relam
        LEFT JOIN pg_catalog.pg_tablespace tblspc ON tblspc.oid = c.reltablespace
        WHERE n.nspname != 'information_schema' AND n.nspname !~ '^pg_'
        AND c.relkind = 'm'
        -- AND NOT c.relhasrules
        ORDER BY c.oid;
    ''').fetchall()])

def get_statistics(pg):
    return OrderedDict([(tuple(x.qualname), x) for x in pg.execute('''
        SELECT
            array[n.nspname, s.stxname] as qualname,
            pg_temp.safe_catalog_column('pg_statistic_ext', 'stxstattarget', 'oid = ' || s.oid, null)::int AS stxstattarget,
            s.stxkind,
            pg_catalog.pg_get_expr(pg_temp.safe_catalog_column_pg_node_tree('pg_statistic_ext', 'stxexprs', 'oid = ' || s.oid, null), s.stxrelid) AS stxexprs,
            (
                SELECT array[n.nspname, c.relname]
                FROM pg_catalog.pg_class c
                JOIN pg_catalog.pg_namespace n ON n.oid = c.relnamespace
                WHERE c.oid = s.stxrelid
            ) AS tablequal,
            (
                SELECT array_agg(a.attname ORDER BY a.attname)
                FROM pg_attribute a
                WHERE a.attrelid = s.stxrelid AND a.attnum = any(s.stxkeys)
            ) AS cols,
            pg_catalog.pg_get_userbyid(s.stxowner) AS rolname,
            pg_catalog.obj_description(s.oid, 'pg_statistic_ext') AS objcomment
        FROM pg_catalog.pg_statistic_ext s
        JOIN pg_catalog.pg_namespace n ON n.oid = s.stxnamespace
        WHERE n.nspname !~ '^pg_'
        ORDER BY s.oid;
    ''').fetchall()])

def get_rules(pg):
    return OrderedDict([(tuple(x.qualname), x) for x in pg.execute('''
        SELECT
            array[n.nspname, r.rulename, ns.nspname, c.relname] as qualname,
            pg_get_ruledef(r.oid) AS definition,
            pg_catalog.obj_description(r.oid) AS objcomment
        FROM (
            pg_rewrite r
            JOIN pg_catalog.pg_class c ON (c.oid = r.ev_class)
            LEFT JOIN pg_catalog.pg_namespace ns ON ns.oid = c.relnamespace)
        LEFT JOIN pg_catalog.pg_namespace n ON (n.oid = c.relnamespace)
        WHERE n.nspname != 'information_schema' AND n.nspname !~ '^pg_'
        AND r.rulename != '_RETURN'
        ORDER BY r.oid;
    ''').fetchall()])

def get_access_methods(pg):
    return OrderedDict([(tuple(x.qualname), x) for x in pg.execute('''
        SELECT
            array[am.amname] AS qualname,
            am.amtype,
            amhandler.amhandler,
            pg_catalog.obj_description(am.oid, 'pg_am') AS objcomment
        FROM pg_catalog.pg_am am
        JOIN LATERAL (
            SELECT array[n.nspname, p.proname] AS amhandler
            FROM pg_catalog.pg_proc p
            JOIN pg_catalog.pg_namespace n ON n.oid = p.pronamespace
            WHERE p.oid = am.amhandler and n.nspname !~ '^pg_'
        ) AS amhandler ON true
        ORDER BY am.oid;
    ''').fetchall()])

def get_system_configs(pg):
    return OrderedDict([(tuple(x.qualname), x) for x in pg.execute('''
        SELECT
            array[s.name] AS qualname,
            row_to_json(s.*) AS values
            s.context,
            s.source,
            s.setting,
            s.reset_val
         FROM pg_settings s
        WHERE s.context != ('internal')
        ORDER BY s.name;
    ''').fetchall()])

def get_transforms(pg):
    return OrderedDict([(tuple(x.qualname), x) for x in pg.execute('''
        SELECT
            array[(
                SELECT
                    pg_catalog.format_type(ty.oid, ty.typtypmod)
                FROM pg_catalog.pg_type ty
                WHERE ty.oid = t.trftype
            ), lanname.lanname] AS qualname,
            (
                SELECT json_build_object(
                    'name', n.nspname || '.' || p.proname,
                    'args', pg_catalog.pg_get_function_identity_arguments(p.oid)
                    )
                FROM pg_catalog.pg_proc p
                JOIN pg_catalog.pg_namespace n ON n.oid = p.pronamespace
                WHERE p.oid = t.trffromsql
            ) AS from_sql_func,
            (
                SELECT json_build_object(
                    'name', n.nspname || '.' || p.proname,
                    'args', pg_catalog.pg_get_function_identity_arguments(p.oid)
                    )
                FROM pg_catalog.pg_proc p
                JOIN pg_catalog.pg_namespace n ON n.oid = p.pronamespace
                WHERE p.oid = t.trftosql
            ) AS to_sql_func,
            pg_catalog.obj_description(t.oid, 'pg_transform') AS objcomment
        FROM pg_catalog.pg_transform t
        JOIN LATERAL (
            SELECT
                l.lanname
            FROM pg_catalog.pg_language l
            WHERE l.oid = t.trflang AND l.lanispl
        ) AS lanname ON true
        ORDER BY t.oid;
    ''').fetchall()])

def get_op_classes(pg):
    return OrderedDict([(tuple(x.qualname), x) for x in pg.execute('''
        SELECT
            array[n.nspname, o.opcname] as qualname,
            array[npo.nspname, ofam.opfname] AS opcfamily,
            array[npt1.nspname, t1.typname] AS opcintype,
            array[npt2.nspname, t1.typname] AS opckeytype,
            o.opcdefault,
            am.amname,
            ops.ops,
            procs.procs,
            pg_catalog.obj_description(o.oid, 'pg_opclass') AS objcomment,
            (
                SELECT array[n.nspname, t.typname]
                FROM pg_type t
                JOIN pg_catalog.pg_namespace n ON n.oid = t.typnamespace
                WHERE t.oid = o.opckeytype
            ) AS opcstorage,
            pg_catalog.pg_get_userbyid(o.opcowner) AS rolname
        FROM pg_catalog.pg_opclass o
        JOIN pg_catalog.pg_namespace n ON n.oid = o.opcnamespace
        LEFT JOIN pg_catalog.pg_am am on am.oid = o.opcmethod
        LEFT JOIN pg_catalog.pg_opfamily ofam ON ofam.oid = o.opcfamily
        LEFT JOIN pg_catalog.pg_namespace npo ON npo.oid = ofam.opfnamespace
        JOIN pg_catalog.pg_type t1 ON t1.oid = o.opcintype
        JOIN pg_catalog.pg_namespace npt1 ON npt1.oid = t1.typnamespace
        LEFT JOIN pg_catalog.pg_type t2 ON t2.oid = o.opckeytype
        LEFT JOIN pg_catalog.pg_namespace npt2 ON npt2.oid = t2.typnamespace
        LEFT JOIN LATERAL (
            SELECT array_agg(json_build_object(
                'oprname', np.nspname || '.' || op.oprname,
                'amopstrategy', a.amopstrategy,
                'lefttype', npt1.nspname || '.' || t1.typname,
                'righttype', npt2.nspname || '.' || t2.typname,
                'amoppurpose', a.amoppurpose,
                'amopsortfamily', (
                    SELECT name.nspname || '.' || ofam.opfname
                    FROM pg_catalog.pg_opfamily ofam
                    JOIN pg_catalog.pg_namespace name ON name.oid = ofam.opfnamespace
                    WHERE ofam.oid = a.amopsortfamily
                )
            ) ORDER BY np.nspname, op.oprname) AS ops
            FROM pg_catalog.pg_amop a
            JOIN pg_catalog.pg_operator op ON op.oid = a.amopopr
            JOIN pg_catalog.pg_namespace np ON op.oprnamespace = np.oid
            LEFT JOIN pg_catalog.pg_type t1 ON t1.oid = a.amoplefttype
            LEFT JOIN pg_catalog.pg_namespace npt1 ON npt1.oid = t1.typnamespace
            LEFT JOIN pg_catalog.pg_type t2 ON t2.oid = a.amoprighttype
            LEFT JOIN pg_catalog.pg_namespace npt2 ON npt2.oid = t2.typnamespace
            WHERE a.amopmethod = o.opcmethod AND a.amoplefttype = o.opcintype AND a.amopfamily = o.opcfamily
            ) AS ops ON TRUE
        LEFT JOIN LATERAL (
            SELECT array_agg(
                json_build_object(
                    'proname', array[np.nspname, f.proname],
                    'proargs', pg_catalog.pg_get_function_identity_arguments(f.oid),
                    'lefttype', npt1.nspname || '.' || t1.typname,
                    'righttype', npt2.nspname || '.' || t2.typname,
                    'amprocnum', p.amprocnum,
                    'amprocfamily', (
                        SELECT name.nspname || '.' || ofam.opfname
                        FROM pg_catalog.pg_opfamily ofam
                        JOIN pg_catalog.pg_namespace name ON name.oid = ofam.opfnamespace
                        WHERE ofam.oid = p.amprocfamily
                    )
            ) ORDER BY np.nspname, f.proname) AS procs
            FROM pg_catalog.pg_amproc p
            JOIN pg_catalog.pg_proc f ON f.oid = p.amproc
            JOIN pg_catalog.pg_namespace np ON f.pronamespace = np.oid
            LEFT JOIN pg_catalog.pg_type t1 ON t1.oid = p.amproclefttype
            LEFT JOIN pg_catalog.pg_namespace npt1 ON npt1.oid = t1.typnamespace
            LEFT JOIN pg_catalog.pg_type t2 ON t2.oid = p.amprocrighttype
            LEFT JOIN pg_catalog.pg_namespace npt2 ON npt2.oid = t2.typnamespace
            WHERE p.amproclefttype = o.opcintype AND p.amprocfamily = o.opcfamily
        ) AS procs ON TRUE
        WHERE n.nspname != 'information_schema' AND n.nspname !~ '^pg_'
        ORDER BY o.oid;
    ''').fetchall()])

def get_op_families(pg):
    return OrderedDict([(tuple(x.qualname), x) for x in pg.execute('''
        SELECT
            ARRAY [n.nspname, o.opfname] as qualname,
            am.amname,
            ops.ops,
            procs.procs,
            pg_catalog.obj_description(o.oid, 'pg_opfamily') AS objcomment,
            pg_catalog.pg_get_userbyid(o.opfowner) AS rolname
        FROM pg_catalog.pg_opfamily o
        JOIN pg_catalog.pg_namespace n ON n.oid = o.opfnamespace
        JOIN pg_catalog.pg_am am ON am.oid = o.opfmethod
        LEFT JOIN LATERAL (
            SELECT json_object_agg (
                np.nspname || '.' || op.oprname,
                json_build_object(
                    'amopstrategy', a.amopstrategy,
                    'lefttype', npt1.nspname || '.' || t1.typname,
                    'righttype', npt2.nspname || '.' || t2.typname,
                    'amoppurpose', a.amoppurpose,
                    'amopsortfamily', (
                        SELECT name.nspname || '.' || ofam.opfname
                        FROM pg_catalog.pg_opfamily ofam
                        JOIN pg_catalog.pg_namespace name ON name.oid = ofam.opfnamespace
                        WHERE ofam.oid = a.amopsortfamily
                    ))
            ORDER BY a.oid) AS ops
            FROM pg_catalog.pg_amop a
            JOIN pg_catalog.pg_operator op ON op.oid = a.amopopr
            JOIN pg_catalog.pg_namespace np ON op.oprnamespace = np.oid
            LEFT JOIN pg_catalog.pg_type t1 ON t1.oid = a.amoplefttype
            LEFT JOIN pg_catalog.pg_namespace npt1 ON npt1.oid = t1.typnamespace
            LEFT JOIN pg_catalog.pg_type t2 ON t2.oid = a.amoprighttype
            LEFT JOIN pg_catalog.pg_namespace npt2 ON npt2.oid = t2.typnamespace
            WHERE a.amopmethod = o.opfmethod AND a.amopfamily = o.oid
            ) AS ops ON TRUE
        LEFT JOIN LATERAL (
            SELECT json_object_agg (
                np.nspname || '.' || f.proname,
                json_build_object(
                    'proargs', pg_catalog.pg_get_function_identity_arguments(f.oid),
                    'lefttype', npt1.nspname || '.' || t1.typname,
                    'righttype', npt2.nspname || '.' || t2.typname,
                    'amprocnum', p.amprocnum,
                    'amprocfamily', (
                        SELECT name.nspname || '.' || ofam.opfname
                        FROM pg_catalog.pg_opfamily ofam
                        JOIN pg_catalog.pg_namespace name ON name.oid = ofam.opfnamespace
                        WHERE ofam.oid = p.amprocfamily
                    ))
            ORDER BY p.oid) AS procs
            FROM pg_catalog.pg_amproc p
                JOIN pg_catalog.pg_proc f ON f.oid = p.amproc
                JOIN pg_catalog.pg_namespace np ON f.pronamespace = np.oid
                LEFT JOIN pg_catalog.pg_type t1 ON t1.oid = p.amproclefttype
                LEFT JOIN pg_catalog.pg_namespace npt1 ON npt1.oid = t1.typnamespace
                LEFT JOIN pg_catalog.pg_type t2 ON t2.oid = p.amprocrighttype
                LEFT JOIN pg_catalog.pg_namespace npt2 ON npt2.oid = t2.typnamespace
                WHERE p.amprocfamily = o.oid
            ) AS procs ON TRUE
        WHERE n.nspname != 'information_schema' AND n.nspname !~ '^pg_'
        ORDER BY o.oid;
    ''').fetchall()])

def get_foreign_servers(pg):
    return OrderedDict([(tuple(x.qualname), x) for x in pg.execute('''
        SELECT
            ARRAY[s.srvname] AS qualname,
            s.srvtype,
            s.srvversion,
            s.srvacl,
            s.srvoptions,
            fdw.fdwname,
            pg_catalog.obj_description(s.oid, 'pg_foreign_server') AS objcomment,
            pg_catalog.pg_get_userbyid(s.srvowner) AS rolname
        FROM pg_catalog.pg_foreign_server s
        LEFT JOIN pg_catalog. pg_foreign_data_wrapper fdw ON s.srvfdw = fdw.oid
        ORDER BY s.oid;
    ''').fetchall()])

def get_foreign_data_wrappers(pg):
    return OrderedDict([(tuple(x.qualname), x) for x in pg.execute('''
        SELECT
            ARRAY[fdw.fdwname] AS qualname,
            (
                SELECT array[n.nspname, f.proname]
                FROM pg_catalog.pg_proc f
                JOIN pg_catalog.pg_namespace n ON f.pronamespace = n.oid
                WHERE f.oid = fdw.fdwhandler
            ) AS fdwhandler,
            (
                SELECT array[n.nspname, f.proname]
                FROM pg_catalog.pg_proc f
                JOIN pg_catalog.pg_namespace n ON f.pronamespace = n.oid
                WHERE f.oid = fdw.fdwvalidator
            ) AS fdwvalidator,
            fdw.fdwacl,
            fdw.fdwoptions,
            pg_catalog.obj_description(fdw.oid, 'pg_foreign_data_wrapper') AS objcomment,
            pg_catalog.pg_get_userbyid(fdw.fdwowner) AS rolname
        FROM pg_catalog.pg_foreign_data_wrapper fdw
        ORDER BY fdw.oid;
    ''').fetchall()])

def get_user_mappings(pg):
    return OrderedDict([(tuple(x.qualname), x) for x in pg.execute('''
        SELECT
            array[
                (SELECT a.rolname FROM pg_catalog.pg_authid a WHERE a.oid = u.umuser),
                (SELECT s.srvname FROM pg_catalog.pg_foreign_server s WHERE s.oid = u.umserver)
                ] AS qualname,
            u.umoptions
        FROM pg_catalog.pg_user_mapping u
        ORDER BY u.oid;
    ''').fetchall()])

# def get_foreign_tables(pg):
#     return pg.execute('''
#         SELECT
#             t.ftoptions,
#             fs.srvname AS ftserver,
#             (
#                 SELECT array[n.nspname, c.relname]
#                 FROM pg_catalog.pg_class c
#                 JOIN pg_catalog.pg_namespace n ON n.oid = c.relnamespace
#                 WHERE c.oid = t.ftrelid
#             ) AS class
#         FROM pg_catalog.pg_foreign_table t
#         LEFT JOIN pg_catalog.pg_foreign_server fs ON t.ftserver = fs.oid
#         ORDER BY t.ftrelid;
#     ''').fetchall()

def get_policies(pg):
    return OrderedDict([(tuple(x.qualname), x) for x in pg.execute('''
        SELECT
            array[p.polname] AS qualname,
            (SELECT array[n.nspname, c.relname] FROM pg_catalog.pg_class c JOIN pg_catalog.pg_namespace n ON n.oid = c.relnamespace WHERE c.oid = p.polrelid) AS tablename,
            p.polcmd,
            p.polpermissive,
            (SELECT array_agg(a.rolname ORDER BY a.rolname) FROM pg_catalog.pg_authid a WHERE a.oid = any(p.polroles)) AS polroles,
            pg_catalog.pg_get_expr( p.polqual, p.oid ) AS polqual,
            pg_catalog.pg_get_expr( p.polwithcheck, p.oid ) AS polwithcheck,
            pg_catalog.obj_description(p.oid, 'pg_policy') AS objcomment
        FROM pg_catalog.pg_policy p
        ORDER BY p.oid;
    ''').fetchall()])
