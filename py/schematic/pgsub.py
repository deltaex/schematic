'''
Utility module for replicating data from a postgresql database.
* Uses COPY for initial exports.
* Uses logical decoding for incremental changes.
* Transactionally consistent between initial export and incremental changes.
* You do not need to do any schema migration for publications and replication slots; these are created dynamically.

It's necessary that initial COPY exports use a transaction snapshot that is consistent with the state of the
replication slot. If it's ahead, we duplicate records and can cause errors for clients (eg two INSERTs for one
primary key). If it's behind, we'll neglect to send some records and the export will be incomplete. This constraint
causes much of the complexity with orchestrating these subscriptions.

When a COPY initial export is required and we _haven't_ processed any incremental changes from a replication slot, we
initialize a new replication slot and do the initial exports using the new slot's transaction snapshot.

When a COPY initial export is required and we _have_ processed incremental changes from a replication slot
(for example, when a new table is added to a subscription), we initialize a new temporary replication slot to get a
transaction snapshot (gives us an LSN paired with a snapshot, which we're unable to get any other way), process changes
from the existing replication slot until the existing and temporary slots have their LSNs synchronized, and then
initial exports using the temporary slot's transaction snapshot. This ensures that we're delivering rows in
transactionally consistent order to the client. Afterward these initial exports finish, we could process more
incremental changes, but opt not to for simplicity (instead waiting for next invocation to continue with incremental
changes).

Note that a transaction snapshot retrieved when creating a replication slot is only valid while that transaction
remains open.

Added a table to a publication should happen after a table's initial export is commited/saved, since we use the
presence of a table in a publication as the indication of whether the initial export has already been performed.

Added a table to a publication should happen in the same transaction snapshot that the COPY export uses, since we want
incremental changes to start being generated precisely from the point that the initial COPY export is generated.
'''

import datetime
import re
import select
import signal
import time
from collections import OrderedDict, namedtuple
from contextlib import contextmanager

import psycopg2
from psycopg2.extensions import ISOLATION_LEVEL_REPEATABLE_READ
from psycopg2.extras import REPLICATION_LOGICAL, LogicalReplicationConnection
from psycopg2.sql import SQL, Composed, Identifier

from schematic import pgcn, pgcopy, pglsn, pgoutput, pgtext, seqs, slog
from schematic.build_util import retry
from schematic.urls import modquery


def get_current_txn_xmax(pg):
    ''' Get the maximum integer transaction ID that has been committed.
        https://www.postgresql.org/docs/current/functions-info.html#FUNCTIONS-TXID-SNAPSHOT
    '''
    return pg.execute('SELECT txid_snapshot_xmax(txid_current_snapshot());').scalar() - 1

# SerializationFailure with error message "initial slot snapshot too large" may happen with too many concurrent txns
@retry(10, exceptions=(psycopg2.errors.SerializationFailure,), onerror=lambda *args, **kwargs: time.sleep(1)) # noqa: ARG005  # pylint: disable=no-member
def create_replication_slot(pg, slotname, temporary=False):
    ''' Create replication slot using replication protocol command CREATE_REPLICATION_SLOT.
        See: https://www.postgresql.org/docs/current/protocol-replication.html#PROTOCOL-REPLICATION-CREATE-SLOT
        Returns tuple(slotname, lsn, snapshot, output_plugin).
        Eg: ('slot_01', '0/27363C8', '00000006-00000756-1', 'pgoutput')
    '''
    slog.info('CREATE_REPLICATION_SLOT %s %s', slotname, 'TEMPORARY' if temporary else '')
    return pg.execute(SQL('CREATE_REPLICATION_SLOT {slotname} {temp} LOGICAL "pgoutput"').format(
            slotname=Identifier(slotname),
            temp=SQL(' TEMPORARY' if temporary else ''))).first()

def get_slot(pg, slotname):
    return pg.execute('''
        SELECT * FROM pg_replication_slots WHERE slot_name = %(slotname)s LIMIT 1;
    ''', {'slotname': slotname}).first()

def get_publication(pg, pubname):
    return pg.execute('''
        SELECT * FROM pg_publication WHERE pubname = %(pubname)s;
    ''', {'pubname': pubname}).first()

def create_publication(pg, pubname):
    return pg.execute(SQL('CREATE PUBLICATION {pubname};').format(pubname=Identifier(pubname)))

def alter_publication_add_table(pg, pubname, tablesub):
    return pg.execute(SQL('ALTER PUBLICATION {pubname} ADD TABLE {table};').format(
            pubname=Identifier(pubname),
            table=Identifier(tablesub.namespace, tablesub.tablename)))

def alter_publication_drop_table(pg, pubname, namespace, tablename):
    slog.info2('removing table %s.%s from %s', namespace, tablename, pubname)
    return pg.execute(SQL('ALTER PUBLICATION {pubname} DROP TABLE {table};').format(
            pubname=Identifier(pubname),
            table=Identifier(namespace, tablename)))

def get_publication_tables(pg, pubname):
    return pg.execute('''
        SELECT * FROM pg_publication_tables WHERE pubname = %(pubname)s;
    ''', {'pubname': pubname}).all()

def remove_missing_pub_tables(pg, pubname, tablesubs):
    ''' If a table is no longer specified in :tablesubs:, remove it from the postgresql publication. '''
    target = {(t.namespace, t.tablename) for t in tablesubs.values() if t.included}
    for t in get_publication_tables(pg, pubname):
        if (t.schemaname, t.tablename) not in target:
            alter_publication_drop_table(pg, pubname, t.schemaname, t.tablename)

def get_publication_new_tables(pg, pubname, tablesubs):
    ''' Get TableSub's for tables that aren't already added to the publication. '''
    pubtables = [(t.schemaname, t.tablename) for t in get_publication_tables(pg, pubname)]
    return [t for k, t in tablesubs.items() if k not in pubtables and t.included]

def getc_publication(pg, pubname):
    ''' Get or create postgresql publication '''
    publication = get_publication(pg, pubname)
    if not publication:
        create_publication(pg, pubname)
        publication = get_publication(pg, pubname)
    return publication

def get_columns(pg, namespace, tablename):
    return OrderedDict([(x.attname, x) for x in pg.execute('''
        WITH ns AS (
            SELECT n.* FROM pg_namespace n WHERE n.nspname = %(namespace)s
        ), tbl AS (
            SELECT c.* FROM pg_class c, ns WHERE (c.relnamespace, c.relname) = (ns.oid, %(tablename)s)
        ), pk AS (
            SELECT con.* FROM pg_constraint con, tbl WHERE con.conrelid = tbl.oid AND con.contype = 'p'
        )
        SELECT
            pg_attribute.*,
            exists(SELECT 1 FROM pk WHERE pg_attribute.attnum = any(pk.conkey)) AS iskey
        FROM pg_attribute, tbl
        WHERE attrelid = tbl.oid
        AND attnum > 0
        AND attisdropped IS FALSE
        ORDER BY attrelid, attnum;
    ''', {'namespace': namespace, 'tablename': tablename}).all()])

def _decode_fallback(text, *args):  # noqa: ARG001
    return text

def decode_to_dict(columns, values, cur):
    return OrderedDict([
        (col.attname, pgtext.decode_nobin(val, col.atttypid, cursor=cur, fallback=_decode_fallback))
        for (col, val) in zip(columns, values)
        if val != pgoutput.TOAST
    ])

def stream_initcopy(pg, tablesub, batch_size=1000):
    ''' Perform initial export for a TableSub using COPY. Filters and maps records.
        See postgresql's src/backend/replication/logical/tablesync.c for documentation on the official implementation
        of logical replication initial export.
        Returns generator yielding mapped and filtered OrderedDict records.
    '''
    query = SQL('SELECT {cols} FROM {table}').format(
        cols=SQL(', ').join([Identifier(c) for c in tablesub.colkeys]),
        table=Identifier(tablesub.namespace, tablesub.tablename))
    if tablesub.sql_where:
        query = Composed([query, SQL(' WHERE '), SQL(tablesub.sql_where)])
    slog.info('initial export: %s', query.as_string(pg.cur))
    yield from seqs.batches(pgcopy.read_via_fs(pg, query.as_string(pg.cur)), batch_size)

@retry(10, exceptions=(psycopg2.errors.ObjectInUse,), onerror=lambda *args: time.sleep(1))  # pylint: disable=no-member # noqa: ARG005
def start_replication(stream_pg, slotname, pubname, lsnstart):
    ''' Send START REPLICATION command to postgresql.

    TODO: support postgresql binary format for efficiency and more precise decoding
    * more efficient for encoding/decoding, more efficient on space (network, storage)
    * https://www.postgresql.org/docs/current/sql-copy.html
    * https://github.com/MagicStack/asyncpg (python3.5+) appears to support decoding, encoding
        * ./blob/e064f59e7944bf7d85fde597ed2eceeec7d8a2b6/asyncpg/protocol/codecs/base.pxd
    * https://github.com/spitz-dan-l/postgres-binary-parser
        * decodes to a pandas data frame, so it would need some significant modifications
        * in cython, so harder to edit, but more efficient result
    * https://pypi.org/project/pg-stream-copy/
        * https://github.com/rtbhouse-apps/pg-stream-copy/blob/master/pg_stream_copy/protocol.py
        * supports encoding to binary (for loading data into postgresql)
        * look straight forward to reverse engineer into a decoder
    * https://github.com/psycopg/psycopg3/pull/6#pullrequestreview-434459273
    * libpqtypes
        * a c library exposing postgresql binary formats
        * unmaintained; perhaps still useful for reference
        * https://github.com/scrive/hpqtypes/tree/master/libpqtypes
    * https://pypi.org/project/pgcopy/
        * https://github.com/altaurog/pgcopy/blob/master/pgcopy/copy.py
        * https://github.com/altaurog/cpgcopy
        * supports encoding to binary format for loading into postgresql
    '''
    pgversion = stream_pg.execute('SHOW server_version_num;').scalar()
    options = {  # see postgresql/src/backend/replication/pgoutput/pgoutput.c:parse_output_parameters
        'proto_version': '1',
        'publication_names': pubname
    }
    if pgversion >= '140001':
        options.update({
            'binary': 'false',  # binary is more efficient, requires some work to support
            'streaming': 'false',  # transaction streaming (saves buffering server side), requires some work to support
            #'two_phase': 'false',  # two phase transaction support
            'messages': 'true',  # enable logical decoding messages ("Message" type)
        })
    stream_pg.cur.start_replication(
        slot_name=slotname,
        slot_type=REPLICATION_LOGICAL,
        start_lsn=lsnstart,
        status_interval=60 * 60 * 24 * 365 * 10,  # higher than will ever happen in practice so we control feedback
        options=options)

@contextmanager
def repliconn(pguri, name='pgsub'):
    ''' Context manager for a psycopg2 replication protocol connection. '''
    pguriapp = modquery(pguri, {'application_name': name})
    pguriapp = pguriapp + '&options=-c%20wal_sender_timeout%3D0'
    with pgcn.connect(pguriapp, connection_factory=LogicalReplicationConnection) as pg:
        yield pg

CopyBatch = namedtuple('CopyBatch', ('records', 'tsn'))  # noqa: PYI024
LogicalMsg = namedtuple(  # noqa: PYI024
    'LogicalMsg',
    ('rawmsg', 'payload', 'tsn', 'lsn', 'tts', 'txn', 'relation', 'tablesub', 'new_record', 'old_record', 'relcache'))

def stream_changes(pg, tablesubs, lsnmin: int, relcache: dict, seginterval: int=None, lsnmax: int=None):  # pylint: disable=too-many-branches
    ''' Stream incremental changes using logical replication/logical decoding.

        :pg: a postgresql replication connection (created with repliconn)
        :tablesubs: OrderedDict of TableSub instances
        :lsnmin: read at least to this lsn
        :seginterval: approximate interval in seconds to size stream segments
        :lsnmax: read no further than this lsn
    '''
    assert lsnmax is None or isinstance(lsnmax, int), f'expected integer for lsnmax, got {lsnmax!r}'
    slog.info('streaming until seginterval %r lsnmax %r', seginterval or '-', lsnmax or '-')
    start = checkin = time.time()
    txn = tts = lsn = rawmsg = None
    def logstatus(i, rawmsg):
        if not rawmsg:
            return
        read_lag = datetime.datetime.utcnow() - tts.replace(tzinfo=None) if tts else '?'
        slog.info('received %d messages, lsn %s, lag %s', i, pglsn.encode_tsn(rawmsg.data_start), read_lag)
    i = 0
    intxn = False
    streaming = False
    # seginterval is None: go forever
    # intxn: keep going until we complete the transaction
    # not lsn: run until lsn advances else no progress is made (avoid retries that never make progress)
    # lsn <= (lsnmin or 0): read at least this far
    while (seginterval is None) or intxn or not lsn or (lsn <= (lsnmin or 0)) or (time.time() - start < seginterval):
        rawmsg = pg.cur.read_message()
        if not rawmsg:
            select.select([pg.cur], [], [], 300)
            continue
        lsn = rawmsg.data_start or lsn  # rawmsg.data_start is None for Relation messages
        i += 1
        if i == 1 or time.time() - checkin >= 60 * 10:
            logstatus(i, rawmsg)
            checkin = time.time()
        payload = pgoutput.decode_message(rawmsg.payload, streaming=streaming)
        new_record = old_record = relation = tablesub = None
        match type(payload):
            case pgoutput.Begin:
                txn = payload.tx_xid
                intxn = True
                tts = payload.commit_tx_ts or tts
            case pgoutput.Commit:
                intxn = False
                tts = payload.commit_tx_ts or tts
            case pgoutput.Relation:
                relation = relcache[payload.relid] = payload
            case pgoutput.Insert:
                relation = relcache[payload.relid]
                new_record = decode_to_dict(relation.columns, payload.new_tuple.record, pg.cur)
            case pgoutput.Update:
                relation = relcache[payload.relid]
                old_record = decode_to_dict(relation.columns, payload.old_tuple.record, pg.cur) if payload.old_tuple else None  # pylint: disable=line-too-long
                new_record = decode_to_dict(relation.columns, payload.new_tuple.record, pg.cur)
            case pgoutput.Delete:
                relation = relcache[payload.relid]
                old_record = decode_to_dict(relation.columns, payload.old_tuple.record, pg.cur)
        tablesub = tablesubs.get((relation.namespace, relation.tablename)) if relation else None
        new_record = stream_maprec(relation, tablesub, new_record)
        old_record = stream_maprec(relation, tablesub, old_record)
        yield LogicalMsg(
            rawmsg, payload, pglsn.encode_tsn(lsn), lsn, tts, txn, relation,
            tablesub, new_record, old_record, relcache)
        if lsnmax and (lsn or 0) >= lsnmax:
            break
    logstatus(i, rawmsg)

def stream_maprec(relation, tablesub, rec):
    if not relation or not tablesub or not rec:
        return
    if tablesub.exc_columns_set:
        for rmcol in tablesub.exc_columns_set:
            del rec[rmcol]
    if tablesub.inc_columns_set:
        for col in rec.keys() - tablesub.inc_columns_set:
            del rec[col]
    return rec

def commit_lsn(stream_pg, lsn):
    ''' Commit to primary that we've processed until this lsn, so that it may free WALs.

        Corresponds to a "Standby status update" in https://www.postgresql.org/docs/current/protocol-replication.html

        write_lsn:
            updates internal state in postgresql src/backend/replication/walsender.c:ProcessStandbyReplyMessage
            (used to prevent resending the same messages in a given session)
        flush_lsn:
            sets pg_replication_slot.confirmed_flush_lsn, it also incidentally causes psycopg2 to send a message
            that advances pg_replication_slot.restart_lsn, but not to the lsn passed, rather to some earlier lsn
            the earlier lsn is determined by the start of the oldest txn that was active at this lsn
    '''
    slog.info2(f'feedback tsn {pglsn.encode_tsn(lsn)}')
    stream_pg.cur.send_feedback(flush_lsn=lsn, write_lsn=lsn, reply=True, force=True) if lsn else None

_signal_inbox = {}

def handle_signal_sigusr1(sigid, frame):
    ''' Signal handler to receive graceful shutdown request. '''
    slog.info2('received signal SIGUSR1 (10) for graceful shutdown')
    _signal_inbox[sigid] = frame

def recv_graceful_shutdown_req():
    ''' Has user requested a graceful shutdown? '''
    return signal.SIGUSR1 in _signal_inbox

class BaseCmd:
    pass

class CopyTableBegin(BaseCmd):

    def __init__(self, pg, tablesub, snapshot, tts, txn, tsn, est_total):
        self.pg = pg
        self.tablesub = tablesub
        self.snapshot = snapshot
        self.tts = tts
        self.txn = txn
        self.tsn = tsn
        self.est_total = est_total

class CopyTableEnd(BaseCmd):
    pass

class StreamBegin(BaseCmd):

    def __init__(self, lsn, tsn):
        self.lsn = lsn
        self.tsn = tsn

class StreamEnd(BaseCmd):
    pass

class Terminate(BaseCmd):
    pass

class StreamCommit(BaseCmd):

    def __init__(self, keep_going):
        ''' A message sent to stream_commands to indicate that changes have been safely persisted, and that it is OK
            to advance the replication slot forward.

            :keep_going: whether to start stream another segment or exit
        '''
        self.keep_going = keep_going

def stream_commands(pg, slotname, pubname, tablesubs, seginterval, initcopy_batch_size=1000):  # pylint: disable=too-many-branches
    ''' A coroutine that generates command messages, initial export records, and incremental update records.

        It expects a StreamCommit message to be sent as feedback after CopyTableEnd and StreamEnd are sent.
        The receipt the StreamCommit feedback message acts as a "commit" that indicates all changes have been durably
        committed to external storage, and it's a safe checkpoint to pick up from on future invocations.

        Params:
        :pubname: A unique ID for this subscription; used for postgresql replication slot and publication names.
        :tablesubs: a list of TableSub instances;
            expected to be read from a yaml file specifying permitted columns using cons_tablesubs
            yaml file can be generated from pgyaml.write_schema_yaml
            Note! Virtual/computed column names must also be specified in here, or they will be removed.
        :seginterval: approximate interval in seconds to size stream segments. This is only a suggestion; there's
            other constraints that make this impossible to enforce precisely (eg transactional atomicity can force
            going for longer).
        :initcopy_batch_size: how many records to include in an initcopy CopyBatch record
    '''
    signal.signal(signal.SIGUSR1, handle_signal_sigusr1)
    assert re.match(r'[a-z0-9_]+', pubname), f'invalid pubname: {pubname!r}'
    add_tables = []
    with repliconn(pg.pguri, f'scm:pgsub:{pubname}') as stream_pg, \
            pgcn.connect(pg.pguri, isolation_level=ISOLATION_LEVEL_REPEATABLE_READ) as pg_init:
        getc_publication(pg, pubname)
        remove_missing_pub_tables(pg, pubname, tablesubs)
        for table in get_publication_new_tables(pg, pubname, tablesubs):
            # tables that require initcopy are added after initcopy is performed; tables that don't require
            # initcopy but are listed after tables that do are added in the right order to honor fk constraints
            if table.initcopy or add_tables:
                add_tables.append(table)
            else:
                alter_publication_add_table(pg, pubname, table)
        pg.commit()
        if add_tables:
            slog.info2('%d tables to add, next up %s', len(add_tables), add_tables[0].fqname)
        slot = get_slot(pg, slotname)
        slog.info('existing slot: %s', slot)
        copy_tts = stream_pg.execute('SELECT now();').scalar()
        # If slot already exists, we create a temporary one to get an (lsn, snapshot) pair to use as a consistent
        # state for initial COPY exports. We then replay the primary slot up to that same lsn, and perform the
        # initial export(s).
        copy_lsn = copy_snapshot = None
        if not slot or add_tables:
            new_slotname = f'{slotname}_copy' if slot else slotname
            (_, copy_lsn, copy_snapshot, _) = create_replication_slot(stream_pg, new_slotname, temporary=bool(slot))
            copy_lsn = pglsn.lsn_to_int(copy_lsn, None)
            slog.info('created slot %s lsn %s', new_slotname, pglsn.encode_tsn(copy_lsn))
            pg_init.execute('SET TRANSACTION SNAPSHOT %(snapshot)s;', {'snapshot': copy_snapshot})
        if not add_tables:
            pg_init.close()
        pg.commit()
        slot = slot or get_slot(pg, slotname)
        pg.commit()
        flush_lsn = pglsn.lsn_to_int(slot.confirmed_flush_lsn, None)
        restart_lsn = pglsn.lsn_to_int(slot.restart_lsn, None)
        lsn = flush_lsn or restart_lsn or 0
        slog.info('pub %s slot %s snapshot %s lsn %s', pubname, slotname, copy_snapshot or '-', pglsn.encode_tsn(lsn))
        assert not copy_lsn or copy_lsn >= lsn, \
            f'unexpected state, copy_lsn < slot lsn: {pglsn.encode_tsn(copy_lsn)} < {pglsn.encode_tsn(lsn)}'
        if not add_tables or lsn < copy_lsn:
            start_replication(stream_pg, slotname, pubname, lsn)
            relcache = {}
            keep_going = True
            while keep_going:
                if recv_graceful_shutdown_req():
                    break
                yield StreamBegin(lsn, pglsn.encode_tsn(lsn))
                for i, lmsg in enumerate(stream_changes(
                        stream_pg, tablesubs, lsn, relcache, lsnmax=copy_lsn, seginterval=seginterval)):
                    if pg_init.is_open and i % 10000 == 0:
                        pg_init.execute('SELECT %(i)s;', {'i': i}) # keep this connection alive to preserve the snapshot
                    if not lmsg:
                        break
                    lsn = lmsg.lsn or lsn  # lmsg.lsn can be None on Relation messages
                    yield lmsg
                response = (yield StreamEnd())
                assert isinstance(response, StreamCommit), 'expected confirmation before proceeding'
                commit_lsn(stream_pg, lsn) if lsn else None
                slog.info('advanced slot %s to lsn %s', slotname, pglsn.encode_tsn(lsn))
                keep_going = response.keep_going
                yield
                if copy_lsn and lsn >= copy_lsn:
                    break  # we've synchronized against the snapshot, time to do initcopy's
        # it should be possible to interleave initcopy and logical changes, but it requires some
        # refactoring to get a new copy_snapshot
        if not add_tables:
            pass
        elif lsn < copy_lsn:
            slog.warn('lsn %s still has not caught up to %s, unable to perform initcopy',
                      pglsn.encode_tsn(lsn), pglsn.encode_tsn(copy_lsn))
        elif lsn >= copy_lsn:
            keep_going = True
            start = time.time()
            for table_num, tablesub in enumerate(add_tables, start=1):
                if recv_graceful_shutdown_req() or not keep_going:
                    break
                if tablesub.initcopy:
                    slog.info2('initcopy table %s', tablesub.fqname)
                    txn = get_current_txn_xmax(pg_init)
                    initcopy_lsn = pglsn.encode_tsn(lsn + table_num - len(add_tables))
                    est_total = pg_init.execute('''
                        SELECT reltuples::bigint
                        FROM pg_class
                        WHERE relname = %(tablename)s
                        AND relnamespace = (SELECT ns.oid FROM pg_namespace ns WHERE ns.nspname = %(namespace)s);
                    ''', {'tablename': tablesub.tablename, 'namespace': tablesub.namespace}).scalar()
                    tablesub.all_columns = get_columns(pg, tablesub.namespace, tablesub.tablename)
                    yield CopyTableBegin(pg_init, tablesub, copy_snapshot, copy_tts, txn, initcopy_lsn, est_total)
                    for batch in stream_initcopy(pg_init, tablesub, batch_size=initcopy_batch_size):
                        yield CopyBatch(batch, initcopy_lsn)
                    response = (yield CopyTableEnd())
                    assert isinstance(response, StreamCommit), 'expected confirmation before proceeding'
                    keep_going = response.keep_going
                    if seginterval and time.time() - start > seginterval:
                        keep_going = False  # limit the amount of replication lag
                else:
                    slog.info('skip initcopy table %s', tablesub.fqname)
                slog.info2('adding table %s to %s', tablesub.fqname, pubname)
                alter_publication_add_table(pg_init, pubname, tablesub)
                pg_init.commit()
                yield
        yield Terminate()
