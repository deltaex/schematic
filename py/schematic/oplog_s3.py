import contextlib
import datetime
import itertools
import os
import re
import shutil
import tempfile
from dataclasses import dataclass
from subprocess import PIPE, Popen
from typing import IO, Iterable, Optional, Tuple
from urllib.parse import urlparse

import boto3
import bugsnag
import orjson
import sh
from botocore.exceptions import ClientError

from schematic import slog
from schematic.oplog import Operation


class S3AccessError(Exception):
    pass

@dataclass
class OplogFile:
    bucket: str
    path: Optional[str]
    name: Optional[str]
    subdir: Optional[str]
    tsn: Optional[str]
    table: Optional[str]
    size: Optional[int]
    modified: Optional[datetime.datetime]

def parse_oploguri(oploguri: str):
    '''
    >>> parse_oploguri('s3://example/foo.jsonl.gz')  # pylint: disable=line-too-long
    OplogFile(bucket='example', path='foo.jsonl.gz', name='foo.jsonl.gz', subdir='', tsn=None, table=None, size=None, modified=None)
    >>> parse_oploguri('s3://example/bar/foo.jsonl.gz')  # pylint: disable=line-too-long
    OplogFile(bucket='example', path='bar/foo.jsonl.gz', name='foo.jsonl.gz', subdir='bar', tsn=None, table=None, size=None, modified=None)
    >>> parse_oploguri('s3://example/playstore/0000A01CBADCBF28.jsonl.gz')  # pylint: disable=line-too-long
    OplogFile(bucket='example', path='playstore/0000A01CBADCBF28.jsonl.gz', name='0000A01CBADCBF28.jsonl.gz', subdir='playstore', tsn='0000A01CBADCBF28', table=None, size=None, modified=None)
    >>> parse_oploguri('s3://example/playstore/00009FF539AF5667-public.play_app.jsonl.gz')  # pylint: disable=line-too-long
    OplogFile(bucket='example', path='playstore/00009FF539AF5667-public.play_app.jsonl.gz', name='00009FF539AF5667-public.play_app.jsonl.gz', subdir='playstore', tsn='00009FF539AF5667', table='public.play_app', size=None, modified=None)
    >>> parse_oploguri('s3://example/00009FF539AF5667-public.play_app.jsonl.gz')  # pylint: disable=line-too-long
    OplogFile(bucket='example', path='00009FF539AF5667-public.play_app.jsonl.gz', name='00009FF539AF5667-public.play_app.jsonl.gz', subdir='', tsn='00009FF539AF5667', table='public.play_app', size=None, modified=None)
    >>> parse_oploguri('s3://ex-ample/sub-1/00011AE4904CEB18')  # pylint: disable=line-too-long
    OplogFile(bucket='ex-ample', path='sub-1/00011AE4904CEB18', name='00011AE4904CEB18', subdir='sub-1', tsn='00011AE4904CEB18', table=None, size=None, modified=None)
    '''
    uri = urlparse(oploguri)
    assert uri.scheme == 's3'
    path = uri.path.lstrip('/') if uri.path else uri.path
    name = os.path.basename(path) if path else None
    subdir = os.path.dirname(path) if path else None
    tsn = re.match(r'([\w\.-]+/)?(?P<tsn>[0-9A-F]{16})([\.\-]|$).*', path) if path else None
    if tsn:
        tsn = tsn.group('tsn')
    table = re.match(r'(\w+/)?[0-9A-F]{16}\-(?P<table>\w+\.\w+)\.*', path) if path else None
    if table:
        table = table.group('table')
    return OplogFile(uri.netloc, path, name, subdir, tsn, table, None, None)

def s3ls_page(bucket: str, subdir: Optional[str] = None, start: Optional[str] = None):
    ''' List s3 files, eg s3://bucket/subdir.
        :start: exclude files that are alphabetically before this path/key (relative to bucket, not subdir)
    '''
    s3 = boto3.client('s3') # pyright: ignore[reportUnknownMemberType]
    try:
        resp = s3.list_objects_v2(Bucket=bucket, StartAfter=start, Prefix=subdir or '') # pyright: ignore[reportGeneralTypeIssues]
    except ClientError as ex:
        aws_config = os.path.exists(os.path.expanduser('~/.aws/config'))
        def before_notify(notification):
            notification['extraData']['awsConfigFileExists'] = aws_config
        bugsnag.before_notify(before_notify)
        raise S3AccessError(ex)  # pylint: disable=raise-missing-from
    for k in sorted(resp.get('Contents', []), key=lambda x: x['Key']):
        olfile = parse_oploguri(os.path.join('s3://', resp['Name'], k['Key']))
        if subdir and olfile.subdir != subdir:  # multiple subscriptions can be in one bucket, don't include others
            continue
        olfile.modified = k['LastModified']
        olfile.size = k['Size']
        yield olfile

def s3ls_all(bucket: str, subdir: Optional[str] = None) -> Iterable[OplogFile]:
    ''' List all oplog files in a bucket and subdir. '''
    start = ''
    for _ in range(1, 100):
        page = list(s3ls_page(bucket, subdir=subdir or '', start=start))
        if not page:
            break
        for olfile in page:
            yield olfile
            start = olfile.path

def ls(path: str, tsn: Optional[str] = None):
    ''' List oplog files, optionally offset from a tsn.
        :path: eg s3://some-bucket/SUBDIR. Exclude files not in SUBDIR.
        :tsn: eg 00011AE4904CEB18. Exclude files that are before this TSN.
            If the TSN falls between two file names, include the previous file (treating it as a range).
    '''
    path = re.sub(r'/+$', '/', '%s/' % path)  # add trailing slash
    parsed = parse_oploguri(path)
    assert not parsed.name, f'expected no filename after subdir in {path}, got {parsed.name}, omit it or use tsn param'
    olfiles = list(s3ls_all(parsed.bucket, subdir=parsed.subdir or ''))
    for (olfile, next_olfile) in itertools.zip_longest(olfiles, olfiles[1:]):
        if tsn and next_olfile and next_olfile.tsn and tsn >= next_olfile.tsn:
            continue  # we're past this file already
        if tsn and olfile.table and olfile.tsn and tsn > olfile.tsn:
            continue  # initcopy exports all have same tsn, so we can safely skip this
        yield olfile

def stream(data: OplogFile) -> Iterable[bytes]:
    # aws s3 cp s3://bucket/path/file.jsonl.gz - | gzip -dc -
    aws_path = shutil.which('aws')
    gzip_path = shutil.which('gzip')
    assert aws_path, 'Didn\'t find aws binary'
    assert gzip_path, 'Didn\'t find gzip binary'
    assert data.bucket and data.path
    with Popen([aws_path, 's3', 'cp', f's3://{data.bucket}/{data.path}', '-'], # noqa: SIM117
               stdout=PIPE) as downloader:
        with Popen([gzip_path, '-dc', '-'],
                   stdin=downloader.stdout, stdout=PIPE) as decompressor:
            yield from decompressor.stdout

@dataclass(slots=True)
class OplogReference:
    offset: int
    length: int

def stream_json(data: OplogFile) -> Iterable[Operation]:
    for line in stream(data):
        yield orjson.loads(line)

def read_jsonl(fileobj) -> Iterable[Tuple[Operation , OplogReference]]:
    for line in fileobj:
        length = len(line)
        yield (orjson.loads(line), OplogReference(fileobj.tell() - length, length))

@contextlib.contextmanager
def open_oplog(olfile):
    tmpdir = os.environ.get('SCM_TMPDIR') or os.path.expandvars('$HOME/var/tmp')
    if olfile.subdir:
        tmpdir = os.path.join(tmpdir, olfile.subdir)
    try:
        sh.mkdir('-p', tmpdir)
    except sh.ErrorReturnCode_1:
        slog.error2(slog.red(f'failed to create temp directory: {tmpdir}'))
        slog.error2('hint: set SCM_TMPDIR env var to use a different temp directory')
        raise
    with tempfile.NamedTemporaryFile(dir=tmpdir) as opfp:
        decompressor = Popen([shutil.which('gzip'), '-dc', '-'], stdin=PIPE, stdout=opfp)
        boto3.client('s3').download_fileobj(olfile.bucket, olfile.path, decompressor.stdin)
        decompressor.stdin.close()
        decompressor.wait()
        opfp.seek(0)
        yield opfp

def extend_reference(a: OplogReference, b: OplogReference) -> None:
    assert a.offset + a.length == b.offset, 'only contiguous references may be extended'
    a.length = a.length + b.length

def stream_reference(file_pointer: IO[bytes], reference: OplogReference) -> Iterable[Operation]:
    while reference.length > 0:
        file_pointer.seek(reference.offset)
        line = next(file_pointer)
        reference.offset += len(line)
        reference.length -= len(line)
        yield line

def stream_whole_reference(
    file_pointer: IO[bytes], reference: OplogReference
) -> Iterable[Operation]:
    file_pointer.seek(reference.offset)
    remaining_length = reference.length
    while remaining_length > 0 and (line := next(file_pointer, None)):
        yield orjson.loads(line)
        remaining_length -= len(line)

def next_allowed_operation(
    file_pointer: IO[bytes], reference: OplogReference, commit_tsn: Optional[str]
) -> Optional[Tuple[Operation, OplogReference]]:
    if reference.length:
        file_pointer.seek(reference.offset)
        line = next(file_pointer)
        operation = orjson.loads(line)
        if operation['tsn'] <= commit_tsn:
            this_reference = OplogReference(reference.offset, len(line))
            reference.offset += len(line)
            reference.length -= len(line)
            return (operation, this_reference)
