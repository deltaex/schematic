import os
import sys

import psycopg2
from psycopg2.sql import SQL, Identifier, Literal

from schematic import env, pgcn, slog
from schematic.build_util import acquire_shared_builder_lock, mkdir


def create_tablespace(pg, name, location, options):
    print(f'tablespace {name} create, location {location}', file=sys.stdout)
    with_options = SQL('WITH ({options})').format(options=SQL(', ').join([
        SQL('%s = {val}' % opt).format(val=Literal(val)) for (opt, val) in options.items()]))
    try:
        return pg.execute(SQL('''
            CREATE TABLESPACE {name} LOCATION {location} {with_options};
        ''').format(
            name=Identifier(name),
            location=Literal(location),
            with_options=with_options if options else SQL(''),
        ))
    except psycopg2.errors.InsufficientPrivilege:  # pylint: disable=no-member
        slog.error2('tablespace %s creation failed; check if %s is owned by same user and group that runs postgresql',
                    name, location)
        raise

def alter_tablespace_reset(pg, name, options, existing_options):
    remopts = [opt for opt in existing_options if opt not in options]
    if remopts:
        opts_str = ', '.join(remopts)
        print(f'tablespace {name} reset {opts_str}', file=sys.stdout)
        return pg.execute(SQL('''
            ALTER TABLESPACE {name} RESET ({options});
        ''').format(
            name=Identifier(name),
            options=SQL(', ').join([SQL(opt) for opt in remopts]),
        ))

def alter_tablespace_set(pg, name, options, existing_options):
    newopts = {opt: options[opt] for opt in options if options[opt] != existing_options.get(opt)}
    if newopts:
        opts_str = ', '.join([f'{k}={v}' for k, v in newopts.items()])
        print(f'tablespace {name} set {opts_str}',
              file=sys.stdout)
        return pg.execute(SQL('''
            ALTER TABLESPACE {name} SET ({options});
        ''').format(
            name=Identifier(name),
            options=SQL(', ').join([SQL('%s = {val}' % opt).format(val=Literal(v)) for (opt, v) in newopts.items()]),
        ))

def set_tablespace_comment(pg, name, comment):
    existing_comment = pg.execute('''
        SELECT shobj_description(oid, 'pg_tablespace')
        FROM pg_catalog.pg_tablespace
        WHERE spcname = %s;
    ''', (name,)).scalar()
    if existing_comment != comment:
        print(f'tablespace {name!r} comment {comment!r}', file=sys.stdout)
        return pg.execute(SQL('COMMENT ON TABLESPACE {name} IS {comment};').format(
            name=Identifier(name), comment=Literal(comment)))

def setup_tablespace_directory(symlink, location, name):
    if not os.path.exists(location):
        print('mkdir %s' % location, file=sys.stdout)
        try:
            mkdir(location)
        except FileExistsError:
            pass
        except PermissionError:
            print(f'tablespace {name} permission error: {location}', file=sys.stderr)
            sys.exit(59)
    if not os.path.exists(symlink):
        print(f'symlink {symlink} -> {location}', file=sys.stdout)
        try:
            os.symlink(location, symlink, target_is_directory=True)
        except FileExistsError:
            pass
    symlink_dest = os.path.realpath(symlink).rstrip('/')
    if symlink_dest != location.rstrip('/'):
        print(f'symlink {symlink} points to {symlink_dest} instead of desired {location}', file=sys.stdout)
        print('this change cannot be made with a running database, please stop postgresql and do it manually',
              file=sys.stdout)
        sys.exit(28)

def setup_fake_tablespace_directory(symlink):
    if not os.path.exists(symlink):
        print('mkdir %s' % symlink, file=sys.stdout)
        try:
            mkdir(symlink)
        except FileExistsError:
            pass

def apply(pguri, basedir):
    name = env.get_str('name')
    location = env.get_str('location')
    comment = env.get_str('comment')
    symlink_dir = os.path.join(basedir, 'tablespaces')
    symlink = os.path.join(symlink_dir, name)
    if not os.path.exists(symlink_dir):
        print('mkdir %s' % symlink_dir, file=sys.stdout)
        mkdir(symlink_dir)
    if env.get_bool('SCM_INSTALL_TABLESPACES'):
        setup_tablespace_directory(symlink, location, name)
    else:  # avoids issues with permissions/missing filesystem mounts/etc for dev/testing
        setup_fake_tablespace_directory(symlink)
        location = symlink
    options = {opt: val for (opt, val) in {
        'seq_page_cost': env.get_str('seq_page_cost'),
        'random_page_cost': env.get_str('random_page_cost'),
        'effective_io_concurrency': env.get_str('effective_io_concurrency'),
        'maintenance_io_concurrency': env.get_str('maintenance_io_concurrency'),
    }.items() if val}
    if env.get_bool('SCM_PG_UPGRADE'):
        print('Upgrading major PostgreSQL versions, skipping tablespace %s' % name, file=sys.stdout)
        return
    with pgcn.connect(pguri, autocommit=True) as pg:
        if pg.execute('SELECT pg_is_in_recovery();').scalar():
            print('read-only replica, skipping tablespace %s' % name, file=sys.stdout)
            return
        acquire_shared_builder_lock(pg)
        existing = pg.execute('SELECT * FROM pg_tablespace WHERE spcname = %s;', (name,)).first()
        if existing:
            existing_options = dict(map(lambda s: s.split('='), existing.spcoptions or ''))
            alter_tablespace_reset(pg, name, options, existing_options)
            alter_tablespace_set(pg, name, options, existing_options)
        else:
            create_tablespace(pg, name, location, options)
        set_tablespace_comment(pg, name, comment)

def main():
    apply(env.get_str('pguri'), env.get_str('basedir'))

if __name__ == '__main__':
    main()
