import fcntl
import os
import re
import sys
import time

from schematic import env, fs, pgcn, slog
from schematic.build_database import add_merged_section
from schematic.build_pgconf import check_config_errors, conf_lock_id, update_config_file
from schematic.build_util import (
    acquire_shared_builder_lock, apply_sql_file, builders_shared_lock_id, install_buildinput_files,
    pg_ctl_fn, poll_pg_isready, release_shared_builder_lock)


def write_config(pg, basedir, guid):
    # Generate a config section leveraging pgconf utils and persist it to the config file
    # Also shares a lock with pgconf objects to avoid concurrent modifications
    # to the configuration file.
    pg.execute('SELECT pg_advisory_lock(%s);', (conf_lock_id,)).first()
    conf_filepath = os.path.join(basedir, 'data/postgresql.conf')
    old_conf_content = fs.try_read(conf_filepath)
    new_conf_content = update_config_file(old_conf_content)
    fs.write(conf_filepath, new_conf_content, mode='w')

    if check_config_errors(pg, conf_filepath):
        # check_config_errors already prints error messages
        sys.exit(11)

    dotfile = os.path.join(basedir, '.applied_pgconfs')
    fs.write(dotfile, f'{guid}\n', mode='a')
    pg.execute('SELECT pg_advisory_unlock(%s);', (conf_lock_id,)).first()

def get_extension_sql_name(sql_path):
    sql = fs.try_read(sql_path, default='')
    if m := re.search(r'CREATE\s+EXTENSION\s*(\s*IF\s+NOT\s+EXISTS\s*)?\s*"?(?P<name>[\w-]+)', sql, re.IGNORECASE):
        return m.group('name')
    return env.get_str('name')

def wait_until_restart(basedir, pg, lock_poll_interval=1):
    '''
    Coordinates concurrent extension objects to wait until all other packages have finished installing.
    The first extension to call this function waits for other packages and restarts,
    all subsequent extensions wait for the first one.
    '''
    lock_file = os.path.join(basedir, '.restart-intent')
    if try_acquire_lock_file(lock_file):
        # We own the restart intent and are in charge of waiting for other packages and restarting the database
        # We don't need to release the acquired lock since the file will be eventually deleted anyways.
        try:
            wait_for_builders_lock(pg)
            port = env.get_int('port')
            slog.info2('Restarting database to enable new extensions...')
            # Trigger a settings merge now in case more than one extension was added
            add_merged_section(os.path.join(basedir, 'data/postgresql.conf'))
            pg_ctl_fn(basedir, 'restart', shutdown_mode='fast', quiet=True)
            if not poll_pg_isready(basedir, port, duration=600):
                raise TimeoutError('Database restart timed out.')
        finally:
            fs.try_unlink(lock_file)
    else:
        # Someone else owns the intent, just wait for them to restart and for the lock file to dissapear
        while os.path.isfile(lock_file):
            time.sleep(lock_poll_interval)

def try_acquire_lock_file(lock_file):
    '''
    Tries to acquire a unique lock on :lock_file:, returns immediately with a boolean
    denoting whether the lock was acquired or not.

    Note that this function does *not* automatically release the lock.
    '''
    fd = os.open(lock_file, os.O_CREAT | os.O_TRUNC | os.O_WRONLY)
    try:
        fcntl.flock(fd, fcntl.LOCK_EX | fcntl.LOCK_NB)
        return True
    except BlockingIOError:
        return False

def wait_for_builders_lock(pg, poll_interval=1, min_intervals=4):
    '''
    Periodically polls the database to check if all shared builder locks have been released, to proceed
    with a restart. Only returns when at least `min_intervals` consecutive intervals have passed where
    all locks were released, to prevent unlucky cases where there's a polling in between two installations.
    '''
    ok_intervals = 0
    while ok_intervals < min_intervals:
        time.sleep(poll_interval)
        any_active_builders = pg.execute('''
            SELECT * FROM pg_locks WHERE locktype = \'advisory\' AND objid = %s LIMIT 1;
        ''', (builders_shared_lock_id,)).first()
        if any_active_builders:
            # Still at least one lock
            ok_intervals = 0
        else:
            ok_intervals += 1

###############################################################################

def main():
    name = env.get_str('name')
    guid = env.get_str('guid')
    basedir = env.get_str('basedir')

    install_buildinput_files()

    if env.get_bool('SCM_PG_UPGRADE'):
        # There's no database available in binary upgrade mode, just copy the extension files
        return

    with pgcn.connect(env.get_str('pguri'), autocommit=True) as pg:
        sql_path = env.get_str('upgrade_sql')

        acquire_shared_builder_lock(pg)
        write_config(pg, basedir, guid)

        if pg.execute('SELECT pg_is_in_recovery()').scalar():
            # Don't attempt to execute CREATE EXTENSION on binary replicas,
            # just copy over the files, apply the configuration, and exit.
            release_shared_builder_lock(pg)
            print('read-only, skipping %r' % sql_path, file=sys.stderr)
            return

        # Exit if the extension is already installed in the DB
        ext_sql_name = get_extension_sql_name(sql_path)
        if pg.execute('SELECT oid FROM pg_extension WHERE extname = %s', (ext_sql_name,)).scalar():
            slog.info2(f'<{name}-{guid}> -> Extension "{ext_sql_name}" already installed, skipping...')
            return

        # Ensure that this is a freshly created database or that we have the user's consent to restart the server
        can_restart = os.path.isfile(os.path.join(basedir, '.new_db')) or env.get_bool('SCM_ALLOW_RESTART')
        if not can_restart:
            slog.error2(slog.red(f'ERROR: adding the extension <{name}-{guid}> requires restarting the database.'))
            slog.error2('hint: you can allow an automatic restart with scm upgrade --allow-restart=yes ...')
            sys.exit(11)

        # Restart the database (or wait for someone else to restart it) before applying upgrade.sql
        release_shared_builder_lock(pg)
        wait_until_restart(basedir, pg)

    # Re-acquire the connection as it will have been lost after restarting
    with pgcn.connect(env.get_str('pguri'), autocommit=True) as pg:
        acquire_shared_builder_lock(pg)
        apply_sql_file(pg, sql_path)
        pg.commit()

if __name__ == '__main__':
    main()
