--# REV
DROP ACCESS METHOD noexist;
--# REV
DROP AGGREGATE noexist(int);
--# REV
DROP CAST (timestamp AS int[]);
--# REV
DROP COLLATION noexist;
--# REV
DROP CONVERSION noexist;
--# REV
--DROP DATABASE noexist; -- can't run this test because it can't run in a txn
--# REV
DROP DOMAIN noexist;
--# REV
DROP EVENT TRIGGER noexist;
--# REV
DROP EXTENSION noexist;
--# REV
DROP FOREIGN DATA WRAPPER noexist;
--# REV
DROP FOREIGN TABLE noexist;
--# REV
DROP FUNCTION noexist(int);
--# REV
DROP GROUP noexist;
--# REV
DROP INDEX noexist;
--# REV
DROP LANGUAGE noexist;
--# REV
DROP MATERIALIZED VIEW noexist;
--# REV
DROP OPERATOR ^^ (int, int);
--# REV
DROP OPERATOR CLASS noexist USING btree;
--# REV
DROP OPERATOR FAMILY noexist USING btree;
--# REV
DROP OWNED BY noexist;
--# REV
DROP POLICY noexist ON pg_catalog.pg_class;
DROP POLICY noexist ON noexist;
--# REV
DROP PROCEDURE noexist(int);
--# REV
DROP PUBLICATION noexist;
--# REV
DROP ROLE noexist;
--# REV
DROP ROUTINE noexist;
--# REV
DROP RULE noexist ON pg_catalog.pg_class;
DROP RULE noexist ON noexist;
--# REV
DROP SCHEMA noexist;
--# REV
DROP SEQUENCE noexist;
--# REV
DROP SERVER noexist;
--# REV
DROP STATISTICS noexist;
--# REV
DROP SUBSCRIPTION noexist;
--# REV
DROP TABLE noexist;
--# REV
--DROP TABLESPACE noexist; -- can't run in a transaction
--# REV
DROP TEXT SEARCH CONFIGURATION noexist;
--# REV
DROP TEXT SEARCH DICTIONARY noexist;
--# REV
DROP TEXT SEARCH PARSER noexist;
--# REV
DROP TEXT SEARCH TEMPLATE noexist;
--# REV
DROP TRANSFORM FOR timestamp[] LANGUAGE sql;
--# REV
DROP TRIGGER noexist ON pg_catalog.pg_class;
DROP TRIGGER noexist ON noexist;
--# REV
DROP TYPE noexist;
--# REV
DROP USER noexist;
--# REV
DROP USER MAPPING FOR noexist SERVER noexist;
DROP USER MAPPING FOR root SERVER noexist;
CREATE EXTENSION postgres_fdw;
CREATE SERVER myserver FOREIGN DATA WRAPPER postgres_fdw;
DROP USER MAPPING FOR noexist SERVER myserver;
--# REV
DROP VIEW noexist;

--# SCHEMA
SELECT 1;

--# CHECK
SELECT 1;
