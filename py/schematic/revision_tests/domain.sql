--# REV 1
ALTER DOMAIN nodomain VALIDATE CONSTRAINT noconstraint;
--# REV 2
CREATE DOMAIN ctext AS text;
ALTER DOMAIN ctext VALIDATE CONSTRAINT noconstraint;
--# REV 3
ALTER DOMAIN ctext RENAME CONSTRAINT constraint_name TO new_constraint_name;
--# REV 4
ALTER DOMAIN noexist RENAME TO new_name;
--# REV 5
ALTER DOMAIN noexist SET SCHEMA new_schema;
--# REV 6
ALTER DOMAIN noexist DROP CONSTRAINT noexist;

--# SCHEMA
CREATE DOMAIN ctext AS text;

--# CHECK
SELECT
    array[n.nspname, t.typname] AS qualname, t.typnotnull, t.typndims, t.typtypmod, t.typdefault,
    -- format_type only adds up to one set of array brackets, but additionals are needed for types like text[][]
    pg_catalog.format_type(t.typbasetype, t.typtypmod) || repeat('[]', t.typndims - 1) AS domaintype,
    (SELECT x.rolname FROM pg_catalog.pg_authid x WHERE x.oid = t.typowner) AS rolname,
    (SELECT x.collname FROM pg_catalog.pg_collation x WHERE x.oid = t.typcollation) AS collname,
    constraints.domainconstraints,
    pg_catalog.obj_description(t.oid) AS objcomment
FROM pg_catalog.pg_type t
JOIN pg_catalog.pg_namespace n ON n.oid = t.typnamespace
LEFT JOIN LATERAL (
    SELECT array_agg(array[x.conname, pg_catalog.pg_get_constraintdef(x.oid, true)] ORDER BY x.oid) AS domainconstraints
    FROM pg_catalog.pg_constraint x
    WHERE t.oid = x.contypid
) AS constraints ON true
WHERE n.nspname != 'information_schema' AND n.nspname !~ '^pg_'
AND t.typtype = 'd'
ORDER BY 1
