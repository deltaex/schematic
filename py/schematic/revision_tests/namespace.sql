--# REV 1
ALTER SCHEMA noexist RENAME TO foo;

--# SCHEMA
CREATE SCHEMA foo;

--# CHECK
SELECT n.nspname, n.nspowner, n.nspacl
FROM pg_namespace n
ORDER BY 1;
