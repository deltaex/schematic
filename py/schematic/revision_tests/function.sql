--# REV 1
ALTER FUNCTION noexist () RENAME TO new_name;
--# REV 2
ALTER FUNCTION noexist () SET SCHEMA new_name;
--# REV 3
ALTER PROCEDURE noexist () RENAME TO new_name;
--# REV 4
ALTER PROCEDURE noexist () SET SCHEMA new_name;

--# SCHEMA
CREATE DOMAIN unixtime AS integer;
CREATE OR REPLACE FUNCTION timestamp_to_unixtime(ts timestamp) RETURNS unixtime AS $$
    SELECT EXTRACT (EPOCH FROM ts)::unixtime;
$$ LANGUAGE SQL STRICT IMMUTABLE NOT LEAKPROOF;
COMMENT ON FUNCTION timestamp_to_unixtime(ts timestamp) IS 'Converts a timestap without time zone to unixtime.';

CREATE FUNCTION public.anyarray_ranges(from_array anyarray) RETURNS SETOF text[] LANGUAGE plpgsql AS $$ BEGIN
RETURN QUERY
    SELECT
        ARRAY_AGG(consolidated_values.consolidated_range) AS ranges
    FROM
        (
            SELECT
                (CASE WHEN COUNT(*) > 1 THEN
                    MIN(unconsolidated_values.array_value)::text || '-' || MAX(unconsolidated_values.array_value)::text
                ELSE
                    MIN(unconsolidated_values.array_value)::text
                END) AS consolidated_range
            FROM (
                SELECT
                    array_values.array_value,
                    ROW_NUMBER() OVER (ORDER BY array_values.array_value) - array_values.array_value AS consolidation_group
                FROM ( SELECT UNNEST(from_array) AS array_value ) AS array_values
                ORDER BY array_values.array_value) AS unconsolidated_values
            GROUP BY unconsolidated_values.consolidation_group
            ORDER BY MIN(unconsolidated_values.array_value)
        ) AS consolidated_values ;
END; $$ ROWS 1234 SET search_path = 'pg_catalog';
CREATE OR REPLACE PROCEDURE select_name(name text)
LANGUAGE plpgsql AS $$ BEGIN
    SELECT name;
END; $$;
COMMENT ON PROCEDURE select_name(name text) IS 'hello';

--# CHECK
SELECT
    array[n.nspname::text, f.proname::text, pg_catalog.pg_get_function_identity_arguments(f.oid)] AS qualname,
    f.procost, f.prorows, f.prokind, f.prosecdef, f.proleakproof, f.proisstrict,
    f.proretset, f.provolatile, f.proparallel, f.pronargs, f.pronargdefaults, f.proargmodes, f.proargnames,
    f.prosrc, f.probin, f.proconfig, f.proacl,
    (
        SELECT array_agg(e.extname ORDER BY e.extname) AS extension_names
        FROM pg_extension e
        WHERE e.oid = any(
            SELECT d.refobjid
            FROM pg_catalog.pg_depend d
            WHERE d.objid = f.oid
            AND d.refclassid = (
                SELECT x.oid
                FROM pg_catalog.pg_class x
                JOIN pg_catalog.pg_namespace y ON y.nspname = 'pg_catalog' and y.oid = x.relnamespace
                WHERE x.relname = 'pg_extension')
            AND d.classid = (
                SELECT x.oid
                FROM pg_catalog.pg_class x
                JOIN pg_catalog.pg_namespace y ON y.nspname = 'pg_catalog' and y.oid = x.relnamespace
                WHERE x.relname = 'pg_proc')
        )
    ) AS extension_names,
    (SELECT array[y.nspname, x.proname] FROM pg_catalog.pg_proc x JOIN pg_catalog.pg_namespace y ON x.pronamespace = y.oid WHERE x.oid = f.prosupport) AS prosupport_func,
    (SELECT array[y.nspname, pg_catalog.format_type(x.oid, x.typtypmod)] FROM pg_catalog.pg_type x JOIN pg_catalog.pg_namespace y ON x.typnamespace = y.oid WHERE x.oid = f.prorettype) AS funcreturn_type,
    (SELECT array_agg(array[y.nspname, pg_catalog.format_type(x.oid, x.typtypmod)] ORDER BY y.nspname, x.typname) FROM pg_catalog.pg_type x JOIN pg_catalog.pg_namespace y ON x.typnamespace = y.oid WHERE x.oid = any(f.protrftypes)) AS functransform_types,
    pg_catalog.pg_get_userbyid(f.proowner) AS rolname,
    pg_catalog.pg_get_functiondef(f.oid) AS functiondef,
    pg_catalog.pg_get_function_arguments(f.oid) AS functionargs,
    pg_catalog.obj_description(f.oid, 'pg_proc') AS objcomment
FROM pg_catalog.pg_proc f
JOIN pg_catalog.pg_namespace n ON n.oid = f.pronamespace
WHERE n.nspname != 'information_schema' AND n.nspname !~ '^pg_' AND f.prokind != 'a'
ORDER BY 1;
