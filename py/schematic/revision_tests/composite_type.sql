--# REV 1
ALTER TYPE noexist RENAME TO new_name;
--# REV 2
ALTER TYPE noexist SET SCHEMA new_schema;
--# REV 3
ALTER TYPE noexist RENAME ATTRIBUTE attribute_name TO new_attribute_name;
--# REV 4
ALTER TYPE noexist DROP ATTRIBUTE attribute_name;
--# REV 5
CREATE TYPE ct0 AS (x int, y text);
ALTER TYPE ct0 DROP ATTRIBUTE noexist;
--# REV 6
ALTER TYPE ct0 RENAME ATTRIBUTE x TO a;
--# REV 7
ALTER TYPE ct0 DROP ATTRIBUTE y;

--# SCHEMA
CREATE TYPE ct0 AS (a int, b bigint, c text collate "C");

--# CHECK type
SELECT
    array[n.nspname, c.relname] AS qualname, (SELECT x.rolname FROM pg_catalog.pg_authid x WHERE x.oid = t.typowner) AS rolname,
    pg_catalog.obj_description(t.oid) AS objcomment
FROM pg_catalog.pg_class c
JOIN pg_catalog.pg_namespace n ON n.oid = c.relnamespace
LEFT JOIN pg_catalog.pg_type t ON t.oid = c.reltype
WHERE c.relkind = 'c'
AND n.nspname NOT IN ('pg_toast', 'pg_catalog', 'information_schema')
ORDER BY 1;

--# CHECK attributes
SELECT
    array[n.nspname, c.relname, a.attname] AS qualname,
    pg_catalog.format_type(coltype.oid, coltype.typtypmod) AS typname,
    a.attstorage, a.attisdropped, collat.collname,
    a.attacl, a.attoptions,
    pg_catalog.col_description(c.oid, a.attnum) AS objcomment
FROM pg_catalog.pg_class c
JOIN pg_catalog.pg_namespace n ON n.oid = c.relnamespace
LEFT JOIN pg_catalog.pg_type t ON t.oid = c.reltype
JOIN pg_catalog.pg_attribute a ON a.attrelid = c.oid AND a.attnum > 0
JOIN pg_catalog.pg_type coltype ON coltype.oid = a.atttypid -- inner join to ignore dropped columns (atttypid = 0)
LEFT JOIN pg_catalog.pg_collation collat ON collat.oid = a.attcollation
WHERE n.nspname != 'information_schema' AND n.nspname !~ '^pg_'
AND c.relkind = 'c'
ORDER BY 1, a.attnum;
