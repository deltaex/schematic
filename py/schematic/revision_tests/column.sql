--# REV 1
ALTER TABLE public.tbl1 DROP COLUMN noexist;
--# REV 2
ALTER TABLE public.tbl1 DROP COLUMN noexist1, DROP COLUMN noexist2;
--# REV 3
ALTER TABLE public.tbl1 ADD COLUMN c int, DROP COLUMN noexist;
--# REV 4
ALTER TABLE public.noexist DROP COLUMN a;
--# REV 5
ALTER TABLE public.tbl0 RENAME noexist TO col1;
--# REV 6
CREATE TABLE public.tbl0 ( );
ALTER TABLE public.tbl0 RENAME noexist TO col1;
--# REV 7
ALTER TABLE public.tbl1 ALTER COLUMN noexist TYPE int;
--# REV 8
CREATE TABLE IF NOT EXISTS tbl1 (
    b int
);
ALTER TABLE public.tbl1 ALTER COLUMN b TYPE text USING ((b + 1)::text);


--# SCHEMA
CREATE TABLE tbl1 (
    b text
);

--# CHECK
SELECT
    array[n.nspname, c.relname, a.attname] AS qualname,
    pg_catalog.format_type(t.oid, t.typtypmod) AS typname,
    a.attstattarget, a.attstorage,
    pg_temp.safe_catalog_column('pg_attribute', 'attcompression', 'attrelid = ' || a.attrelid, '')::char AS attcompression,
    a.attnotnull, a.atthasmissing, a.attisdropped, a.attislocal,
    a.attinhcount, collat.collname, a.attacl, a.attoptions, a.attfdwoptions,
    a.attidentity, a.attgenerated, a.atthasdef,
    pg_catalog.pg_get_expr(attrdef.adbin, attrdef.adrelid) AS attdef,
    pg_catalog.col_description(c.oid, a.attnum) AS objcomment
FROM pg_catalog.pg_class c
JOIN pg_catalog.pg_namespace n ON n.oid = c.relnamespace
JOIN pg_catalog.pg_attribute a ON a.attrelid = c.oid AND a.attnum > 0
LEFT JOIN pg_catalog.pg_type t ON t.oid = a.atttypid
LEFT JOIN pg_catalog.pg_collation collat ON collat.oid = a.attcollation
LEFT JOIN pg_catalog.pg_attrdef attrdef ON attrdef.adrelid = a.attrelid AND attrdef.adnum = a.attnum
WHERE n.nspname != 'information_schema' AND n.nspname !~ '^pg_'
AND c.relkind IN ('r', 'p', 'f')
ORDER BY 1;



