--# REV
ALTER VIEW noexist RENAME TO noexist1;
--# REV
ALTER VIEW noexist SET SCHEMA pub2;
--# REV
ALTER VIEW pub1.noexist SET SCHEMA pub2;
--# REV
ALTER VIEW pub1.noexist SET SCHEMA public;
--# REV
ALTER VIEW noexist RENAME COLUMN column_name TO new_column_name;
--# REV
CREATE VIEW public.view0 AS SELECT 1 AS a;
ALTER VIEW public.view0 RENAME COLUMN column_name TO new_column_name;

--# SCHEMA
CREATE VIEW public.view0 AS SELECT 1 AS a;

--# CHECK
SELECT
    array[n.nspname, c.relname] AS qualname,
    c.relpersistence,
    c.reloptions,
    pg_catalog.pg_get_viewdef(c.oid) AS viewdef,
    pg_catalog.pg_get_userbyid(c.relowner) AS rolname,
    columns.viewcolumns,
    pg_catalog.obj_description(c.oid) AS objcomment
FROM pg_catalog.pg_class c
JOIN pg_catalog.pg_namespace n ON n.oid = c.relnamespace
LEFT JOIN LATERAL (
    SELECT
        array_agg(array[a.attname,
            pg_catalog.format_type(columntype.oid, columntype.typtypmod),
            a.atthasdef::text,
            pg_catalog.pg_get_expr(attrdef.adbin, attrdef.adrelid
        )] ORDER BY a.attnum ASC ) AS viewcolumns
    FROM pg_catalog.pg_attribute a
    JOIN pg_catalog.pg_type columntype ON columntype.oid = a.atttypid
    LEFT JOIN pg_catalog.pg_attrdef attrdef ON attrdef.adrelid = a.attrelid AND attrdef.adnum = a.attnum
    WHERE a.attrelid = c.oid
) AS columns ON true
WHERE n.nspname != 'information_schema' AND n.nspname !~ '^pg_'
AND c.relkind IN ('v', 'm')
ORDER BY 1;
