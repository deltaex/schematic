--# REV
ALTER ROLE noexist RENAME TO new_name;

--# SCHEMA
SELECT 1;

--# CHECK
SELECT
    r.rolname AS qualname,
    json_build_object(
        'SUPERUSER', r.rolsuper,
        'INHERIT', r.rolinherit,
        'CREATEROLE', r.rolcreaterole,
        'CREATEDB', r.rolcreatedb,
        'LOGIN', r.rolcanlogin,
        'REPLICATION', r.rolreplication,
        'BYPASSRLS', r.rolbypassrls,
        'CONNECTION LIMIT', r.rolconnlimit,
        'VALID UNTIL', r.rolvaliduntil
    ) AS options,
    (
        SELECT array_agg(a.rolname) FROM pg_catalog.pg_authid a WHERE a.oid = any(members.oids)
    ) AS rolmembers,
    default_db_configs.c AS db_configs,
    default_all_db_configs.c AS all_configs
FROM pg_catalog.pg_authid r
LEFT JOIN LATERAL (
    SELECT
        array_agg(json_build_object(d.datname, rs.setconfig) ORDER BY rs.setrole) AS c
    FROM pg_catalog.pg_db_role_setting rs
    JOIN pg_catalog.pg_database d ON d.oid = rs.setdatabase
    WHERE rs.setrole = r.oid
) AS default_db_configs ON true
LEFT JOIN LATERAL (
    SELECT
        rs.setconfig AS c
    FROM pg_catalog.pg_db_role_setting rs
    WHERE rs.setrole = r.oid AND rs.setdatabase = 0
) AS default_all_db_configs ON true
LEFT JOIN LATERAL (
    SELECT array_agg(member) AS oids from pg_catalog.pg_auth_members m WHERE r.oid = m.roleid
) AS members ON true
WHERE r.rolname NOT IN (
    'pg_monitor',
    'root',
    'pg_write_all_data',
    'pg_read_all_data',
    'pg_database_owner',
    'pg_checkpoint',
    'pg_read_all_stats',
    'pg_read_all_settings',
    'pg_stat_scan_tables',
    'pg_read_server_files',
    'pg_write_server_files',
    'pg_execute_server_program',
    'pg_signal_backend')
ORDER BY 1;
