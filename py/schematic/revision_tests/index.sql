--# REV 1
ALTER INDEX noexist RENAME TO new_name;

--# SCHEMA
CREATE TABLE public.cc (
    iso text NOT NULL,
    name text NOT NULL,
    area_sqkm double precision NOT NULL,
    pop integer NOT NULL,
    tld text,
    aliases text[] NOT NULL
);

ALTER TABLE ONLY public.cc ADD CONSTRAINT cc_pkey PRIMARY KEY (iso);
CREATE UNIQUE INDEX cc_cc_idx ON public.cc USING btree (name);
CREATE INDEX cc_tld_idx ON public.cc USING btree (tld) WITH ( fillfactor = 50, deduplicate_items = OFF );
COMMENT ON INDEX cc_tld_idx IS 'hi';
CREATE INDEX testidx1 ON public.cc (pop DESC NULLS LAST, iso) INCLUDE (aliases, name) WHERE tld IS NOT NULL;
CREATE INDEX testidx2 ON cc ((pop + 1));
ALTER INDEX testidx2 ALTER COLUMN 1 SET STATISTICS 6345;
CREATE INDEX testidx3 ON cc ((pop + 2)) WHERE (pop > 3);

--# CHECK
SELECT
    array[n.nspname, c.relname] AS qualname, c.relpersistence, c.relispartition, c.relacl,
    am.amname, c.reloptions, tblspc.spcname,
    i.indnatts, i.indnkeyatts, i.indisunique, i.indisexclusion, i.indisprimary,
    pg_temp.safe_catalog_column('pg_index', 'indnullsnotdistinct', 'indexrelid = ' || i.indexrelid::text, 'false')::boolean AS indnullsnotdistinct,
    i.indimmediate, i.indisreplident, i.indoption,
    idxcolstats.*, idxcols.*, pg_catalog.pg_get_expr(i.indexprs, i.indrelid) AS idxexpressions,
    pg_catalog.pg_get_expr(i.indpred, i.indrelid) AS idxpartialexp,
    pg_get_indexdef(c.oid) AS indexdef,
    (SELECT array_agg(e.extname) FROM pg_extension e WHERE e.oid = d.refobjid) AS extension_names,
    (SELECT array[y.nspname, x.relname] FROM pg_class x JOIN pg_catalog.pg_namespace y ON x.relnamespace = y.oid WHERE x.oid = i.indrelid) AS idxtable,
    pg_catalog.pg_get_userbyid(c.relowner) AS rolname,
    pg_catalog.obj_description(c.oid) AS objcomment
FROM pg_catalog.pg_class c
JOIN pg_catalog.pg_namespace n ON n.oid = c.relnamespace
LEFT JOIN pg_index i ON i.indexrelid = c.oid
LEFT JOIN pg_catalog.pg_am am ON am.oid = c.relam
LEFT JOIN pg_catalog.pg_tablespace tblspc ON tblspc.oid = c.reltablespace
LEFT JOIN pg_catalog.pg_depend d ON d.objid = c.oid AND d.classid = (
    SELECT oid FROM pg_catalog.pg_class WHERE relname = 'pg_class' LIMIT 1
)
LEFT JOIN LATERAL (
    SELECT array_agg(array[a.attnum, a.attstattarget] ORDER BY a.attnum) AS idxcolstats
    FROM pg_catalog.pg_attribute a
    WHERE a.attrelid = c.oid
    AND a.attstattarget >= 0
) AS idxcolstats ON true
LEFT JOIN LATERAL ( -- columns in the index key (indnkeyatts) with trailing INCLUDE columns (indnatts)
    WITH cols AS (
        SELECT
            unnest(i.indkey) AS colid,
            unnest(i.indclass) AS opclassid,
            unnest(i.indcollation) AS collationid
    )
    SELECT array_agg(array[att.attname, opclass.opcname, collat.collname] ORDER BY array_position(i.indkey, att.attnum)) AS idxcols
    FROM cols
    LEFT JOIN pg_catalog.pg_attribute att ON att.attrelid = i.indrelid AND att.attnum = cols.colid
    JOIN pg_catalog.pg_opclass opclass ON opclass.oid = cols.opclassid
    LEFT JOIN pg_catalog.pg_collation collat ON collat.oid = cols.collationid
) AS idxcols ON true
WHERE n.nspname != 'information_schema' AND n.nspname !~ '^pg_' AND NOT i.indisexclusion
AND c.relkind IN ('i', 'I')
ORDER BY 1;
