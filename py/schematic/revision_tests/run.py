#! /usr/bin/env python3
'''
Run tests for revisions.

$ ./py/schematic/revision_tests/run.py

There's some subtle magic behavior that makes revisions easier to use. Consider this example:

- create a schema package with table foo, column bar (with no revision, schema is created declaratively)
- run upgrade, now database X has foo.bar
- create revision to drop foo.bar
- run upgrade, now database X does not have foo.bar
- create new database Y, run upgrade, get an error dropping non-existent foo.bar because the revision runs before the
  declarative schema package has created foo.bar

To make revisions work as expected in the presence of declarative schema packages, we have to support multiple orders
of execution. Consider the case of one schema package and two revisions, then there's multiple execution orders that
are possible: (schema, rev1, rev2), (rev1, schema, rev2), (rev1, rev2, schema).

py/schematic/build_revision.py is aware of the content of sql statements it runs and the error codes postgresql returns
to make revisions do the desired thing automatically, without the user having to code their revisions defensively with
IF statements.

This module runs unit tests to check the revision logic works as expected.

The tests are defined as sql files with three types of sections: SCHEMA, REV, and CHECK. The SCHEMA section defines
the declarative schema/target state. One or more REV sections define the revisions. One or more CHECK sections
define a query that should return the same result between the different execution orders.

The intention with the CHECK section is to query postgresql catalog tables for the relevant descriptions of schema to
ensure they match.

There can be multiple statements in each section. When there is multiple statements in a CHECK section, only the result
of the last one is used. There can be multiple instances of each section type except SCHEMA. When using multiple of a
given type, give them a name by putting some text after the section label, otherwise a nondescript name will be given.

See examples for formatting. Notice that sections are delimited with `--# `. Blank lines and comments are ignored.

For CHECK queries, take inspiration from py/schematic/pg_catalog.py queries which are used to detect changes to schema.
'''

import glob
import itertools
import os
import re
import sys
import traceback
from collections import OrderedDict
from dataclasses import dataclass
from sys import stderr

import path
from clint.textui import colored
from nose.tools import nottest

from schematic import ablative, derivative, pgcn, slog
from schematic.build_util import is_empty_sql
from schematic.scm_cli_utils import _gc_basedir, _sandbox


def parse_section_head(line):
    if match := re.match(r'^--# (?P<name>[\w ]+)\s*$', line):
        return match.group('name')

def is_section_head(line):
    return line.startswith('--# ')

@nottest
def parse_testfile(filename):
    sections = OrderedDict()
    name = None
    seq = 1
    with open(filename, encoding='utf8') as fp:
        for line in fp:
            if is_section_head(line):
                name = parse_section_head(line)
                if name in sections:
                    name = f'{name} {seq}'
                    seq += 1
                sections[name] = ''
            elif name:
                sections[name] += line
    return sections

@dataclass
class TestModule:
    testfile: str
    sections: OrderedDict

    @property
    def filename(self):
        return os.path.basename(self.testfile)

@nottest
def get_test_modules():
    for testfile in glob.glob(path.Path(os.path.dirname(__file__)) / '*.sql'):
        yield TestModule(testfile=testfile, sections=parse_testfile(testfile))

def get_mod_pobjix(testmod, pobjix):
    for name, content in testmod.sections.items():
        if name.startswith(pobjix):
            yield name, content

PASS = colored.green('PASS')
FAIL = colored.red('FAIL')

def print_row_diff(sbj_result, obj_result):
    show_fields = [ # show first few fields (identifiers) or ones that are different
        k for (i, k, b, a) in zip(range(len(sbj_result)), sbj_result._fields, sbj_result, obj_result)
        if b != a or i < 2]
    def printrec(label, rec):
        print(f'''    {label} {', '.join(f'{f}={getattr(rec, f)!r}' for f in show_fields)}''', file=stderr)
    printrec('OBJ', sbj_result)
    printrec('SBJ', obj_result)

def main():
    obj_basedir = sbj_basedir = None
    passes = fails = 0
    try:
        obj_drv = _sandbox([])
        sbj_drv = _sandbox([])
        obj_basedir = obj_drv['env']['basedir']
        sbj_basedir = sbj_drv['env']['basedir']
        with pgcn.connect_dir(obj_basedir) as pg_obj, pgcn.connect_dir(sbj_basedir) as pg_sbj:
            for testmod in get_test_modules():
                if fails and '--break' in sys.argv:
                    break
                for obj_name, obj_content in get_mod_pobjix(testmod, 'SCHEMA'):
                    if fails and '--break' in sys.argv:
                        break
                    pg_obj.execute(obj_content) if not is_empty_sql(obj_content) else 0
                    for sbj_name, sbj_content in get_mod_pobjix(testmod, 'REV'):
                        try:
                            ablative.safe_apply_statements(pg_sbj, sbj_content, log=slog.debug2, log_statements=0)
                            print(f'{PASS} {testmod.filename} {obj_name} : {sbj_name}', file=stderr)
                            passes += 1
                        except Exception as ex:  # noqa: BLE001
                            print(f'{FAIL} {testmod.filename} {sbj_name} {ex} (while initializing)', file=stderr)
                            traceback.print_exc(file=stderr)
                            pg_sbj.rollback()
                            fails += 1
                            break
                    try:
                        derivative.add_all(pg_obj, pg_sbj, action=derivative.action_exec, allow_commit=False)
                    except Exception:  # noqa: BLE001
                        print(f'{FAIL} {testmod.filename} {obj_name} (while deriving)', file=stderr)
                        traceback.print_exc(file=stderr)
                        pg_sbj.rollback()
                        fails += 1
                        break
                    for chk_name, chk_content in get_mod_pobjix(testmod, 'CHECK'):
                        chkobj_results = pg_obj.execute(chk_content).fetchall()
                        chksbj_results = pg_sbj.execute(chk_content).fetchall()
                        if len(chkobj_results) != len(chksbj_results):
                            print(f'{FAIL} {testmod.filename} {obj_name} : {chk_name}', file=stderr)
                            print(f'    SBJ {len(chksbj_results)} results', file=stderr)
                            print(f'    OBJ {len(chkobj_results)} results', file=stderr)
                            fails += 1
                            continue
                        anyfail = False
                        for obj_result, sbj_result in itertools.zip_longest(chkobj_results, chksbj_results):
                            if obj_result == sbj_result:
                                passes += 1
                            else:
                                print(f'{FAIL} {testmod.filename} {obj_name} : {chk_name}', file=stderr)
                                print_row_diff(sbj_result, obj_result)
                                fails += 1
                                anyfail = True
                        if not anyfail:
                            print(f'{PASS} {testmod.filename} {obj_name} : {chk_name}', file=stderr)
                    pg_obj.rollback()
                    pg_sbj.rollback()
        print(f'{PASS} {passes} {FAIL} {fails}') if fails else print(f'{PASS} {passes}')
    finally:
        _gc_basedir(obj_basedir, force=True, stop=True) if obj_basedir else 0
        _gc_basedir(sbj_basedir, force=True, stop=True) if sbj_basedir else 0

if __name__ == '__main__':
    main()
