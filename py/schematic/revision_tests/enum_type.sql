--# REV 1
ALTER TYPE noexist RENAME TO new_name;
--# REV 2
ALTER TYPE noexist SET SCHEMA new_schema;
--# REV 3
ALTER TYPE noexist RENAME VALUE 'existing_enum_value' TO 'new_enum_value';
--# REV 4
CREATE TYPE enum0 AS ENUM ('enumx');
ALTER TYPE enum0 RENAME VALUE 'enumy' TO 'enum1';
--# REV 5
ALTER TYPE enum0 RENAME VALUE 'enumx' TO 'enum1';

--# SCHEMA
CREATE TYPE enum0 AS ENUM ('enum1', 'enum2', 'enum3');

--# CHECK
SELECT
    array[n.nspname, t.typname] AS qualname, (SELECT x.rolname FROM pg_catalog.pg_authid x WHERE x.oid = t.typowner) AS rolname,
    (SELECT array_agg(e.enumlabel ORDER BY enumsortorder) FROM pg_catalog.pg_enum e WHERE e.enumtypid = t.oid) AS labels,
    pg_catalog.obj_description(t.oid) AS objcomment
FROM pg_catalog.pg_type t
JOIN pg_catalog.pg_namespace n ON n.oid = t.typnamespace
WHERE n.nspname != 'information_schema' AND n.nspname !~ '^pg_'
AND t.typtype = 'e'
ORDER BY 1;
