--# REV 1
ALTER TABLE public.tbl0 RENAME TO tbl1;
--# REV 2
CREATE TABLE public.tbl0 ();
ALTER TABLE public.tbl0 RENAME TO tbl1;
--# REV 3
CREATE SCHEMA nsp0;
ALTER TABLE nsp0.tbl1 SET SCHEMA public;
--# REV 4
ALTER TABLE noexist RENAME CONSTRAINT foo TO bar;
--# REV 5
ALTER TABLE tbl1 RENAME CONSTRAINT foo TO bar;

--# SCHEMA
CREATE TABLE public.tbl1 ();

--# CHECK
SELECT
    array[n.nspname, c.relname] AS qualname, c.relpersistence, inherits.inherits, c.relrowsecurity, c.relforcerowsecurity,
    c.relreplident, c.reloptions, c.relispartition, c.relacl,
    pt.partstrat, pt.partnatts, pt.partattrs, pt.partclass, pt.partcollation,
    partcols.partcols, pg_catalog.pg_get_expr(pt.partexprs, pt.partrelid) AS partexpressions,
    am.amname, tblspc.spcname,
    (SELECT x.rolname FROM pg_catalog.pg_authid x WHERE x.oid = c.relowner) AS rolname,
    pg_catalog.obj_description(c.oid) AS objcomment
FROM pg_catalog.pg_class c
JOIN pg_catalog.pg_namespace n ON n.oid = c.relnamespace
LEFT JOIN LATERAL ( -- parent tables this table inherits from
    SELECT array_agg(array[pn.nspname, p.relname]) AS inherits
    FROM pg_catalog.pg_inherits i
    JOIN pg_catalog.pg_class p ON i.inhparent = p.oid
    JOIN pg_catalog.pg_namespace pn ON pn.oid = p.relnamespace
    WHERE i.inhrelid = c.oid
) AS inherits ON true
LEFT JOIN pg_catalog.pg_partitioned_table pt ON pt.partrelid = c.oid
LEFT JOIN LATERAL ( -- columns in the partition key
    WITH cols AS (
        SELECT
            unnest(pt.partattrs) AS colid,
            unnest(pt.partclass) AS opclassid,
            unnest(pt.partcollation) AS collationid
    )
    SELECT array_agg(array[att.attname, opclass.opcname, collat.collname]) AS partcols
    FROM cols
    LEFT JOIN pg_catalog.pg_attribute att ON att.attrelid = c.oid AND att.attnum = cols.colid
    JOIN pg_catalog.pg_opclass opclass ON opclass.oid = cols.opclassid
    LEFT JOIN pg_catalog.pg_collation collat ON collat.oid = cols.collationid
) AS partcols ON true
LEFT JOIN pg_catalog.pg_am am ON am.oid = c.relam
LEFT JOIN pg_catalog.pg_tablespace tblspc ON tblspc.oid = c.reltablespace
WHERE n.nspname != 'information_schema' AND n.nspname !~ '^pg_'
AND c.relkind IN ('r', 'p')
ORDER BY 1;
