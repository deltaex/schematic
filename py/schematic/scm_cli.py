#! /usr/bin/env python3
'''
Schematic is a package manager for PostgreSQL.

Use Schematic to declaratively define databases from packages of schema, configuration, and PostgreSQL extensions.

Detailed help available per command, for example: scm sandbox -h

Dev Commands:

    sandbox              Create a temporary sandbox database from 0+ input packages.
    upgrade              Create or upgrade a PostgreSQL database.
    pgconf               Create a package for a new pgconf object which sets options in a postgresql.conf file.
    pghba                Create a package for a new pghba object which generates a section in a pg_hba.conf file.
    database             Create files for a new database object.
    revision             Create files for a new revision object to migrate the specified schema.
    schema               Create files for a new schema object.
    tablespace           Create files for a new tablespace object.
    namespace            Create files for a new namespace (ie "CREATE SCHEMA") object.
    extension            Create files for a new extension object.
    gc                   Clean up objects, including deleting obsolete sandbox databases from ~/var/pg.
    gc-basedir           Irreversibly delete a database.
    psql                 Open psql shell to specified database.
    inspect              Inspect a schematic module, printing out dependencies and their metadata.
    dependencies-list    Print a list of dependencies for the given object.
    derivative-diff      Print the DDL statements that would be run with an derivative upgrade, but don't apply them.

Operational Commands:

    upgrade              Create or upgrade a PostgreSQL database.
    start                Start specified database server.
    reload               Reload specified database server.
    restart              Restart specified database server.
    status               List status of all databases in ~/var/pg.
    stop                 Stop specified database servers.
    cron                 Install a crontab task, wrapping the command in shell script with a suitable environment.
    pg-activity          Open pg_activity for a specific database.
    export-certificates  Exports the certificates of the database specified as a zip file in the current directory.
    gc-basedir           Irreversibly delete a database.
    derivative-diff      Print the DDL statements that would be run with an derivative upgrade, but don't apply them.

Replication Commands:

    oplog-ingress        Receive oplog update feed into postgresql.
    oplog-ls             List oplog files, optionally offset from a tsn.
    sync                 Use logical replication to stream changes from one postgresql database to another.
    sync-conf            Read tables from a database and generate a yaml file used by 'scm sync' for replication \
subscriptions.
    sync-table           Batch synchronize a table from one database to another.

Env Vars:
    SCM_VERBOSE          Enable verbose output to stderr.

'''

import importlib
import re
import sys

from schematic.utils import cli


def command_help():
    cli.helpexit(__doc__.strip()) # type: ignore reportAttributeAccessIssue

def main():
    command, argv = cli.lpop(sys.argv[1:])
    assert command is not None, command_help()
    assert not cli.ask_help([command]), command_help()
    command = command.replace('-', '_')
    if not re.match(r'^[a-z_]+[a-z0-9_]*$', command):
        cli.errexit(1, f'invalid command: {command}')
    try:
        command_module = importlib.import_module(f'schematic.commands.{command}')
    except ImportError:
        cli.errexit(1, f'unknown command: {command}')
    if cli.ask_help(argv):
        cli.helpexit((command_module.__doc__ or '').strip())
    command_module.main(argv)

if __name__ == '__main__':
    main()
