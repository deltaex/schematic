'''
Utility functions for postgresql DDL statements using psycopg2 api.

Statements are constucted as a list of values:
- Use S() to wrap literal sql expressions. SQL injection risk; don't use on untrusted input.
- Use I() to wrap an identifier (an object name). It will handle quoting as appropriate.
- Use L() to wrap a primitive value (string, integer, etc).
'''
import itertools
import re

from psycopg2 import sql
from psycopg2.sql import SQL as S
from psycopg2.sql import Identifier as I  # noqa: N817
from psycopg2.sql import Literal as L  # noqa: N817


def quote_identifier(s):
    if re.match(r'^[\w_\.]+$', s):
        return s
    assert '"' not in s, 'quote escaping not implemented'
    return f'"{s}"'

def as_string(o):
    ''' Implementation of sql.Composable.as_string that doesn't require a database connection. '''
    if isinstance(o, sql.Composed):
        return ''.join([as_string(x) for x in o]).strip()
    if isinstance(o, sql.Identifier):
        return '.'.join([quote_identifier(s) for s in o.strings or [o.string]])
    if isinstance(o, sql.SQL):
        return o.string
    ret = sql.ext.adapt(o._wrapped).getquoted()  # noqa: SLF001
    return (ret.decode() if isinstance(ret, bytes) else ret).strip()

def compose(*args):
    return S(' ').join(filter(None, itertools.chain(*args)))

def _sequence_opts(
        data_type=None, increment=None, minvalue=None, maxvalue=None, start=None, cache=None, cycle=None,
        owned_by=None, restart=None, alter=False):
    if data_type is not None:
        assert data_type in ('smallint', 'integer', 'bigint'), 'invalid data_type %r' % data_type
        yield S('AS %s' % data_type)
    if increment is not None:
        yield S('INCREMENT {increment}').format(increment=L(increment))
    if minvalue is not None:
        yield S('MINVALUE {minvalue}').format(minvalue=L(minvalue))
    else:
        yield S('NO MINVALUE')
    if maxvalue is not None:
        yield S('MAXVALUE {maxvalue}').format(maxvalue=L(maxvalue))
    else:
        yield S('NO MAXVALUE')
    if start is not None:
        yield S('START {start}').format(start=L(start))
    if cache is not None:
        yield S('CACHE {cache}').format(cache=L(cache))
    if cycle is not None:
        yield S('CYCLE' if cycle else 'NO CYCLE')
    if owned_by is not None:
        yield S('OWNED BY {owned_by}').format(owned_by=I(*owned_by))
    else:
        yield S('OWNED BY NONE')
    if (restart is not None and (alter or restart != start)):
        yield S('RESTART {restart}').format(restart=L(restart))

def comment_generic(qualname, objtype, objcomment):
    '''Generate COMMENT ON statement.
    >>> as_string(comment_generic(('public', 'foo_id_seq'), 'SEQUENCE', 'hi'))
    "COMMENT ON SEQUENCE public.foo_id_seq IS 'hi'"
    '''
    return compose([S('COMMENT ON'), S(objtype), I(*qualname), S('IS'), L(objcomment)])

def create_namespace(qualname):
    ''' Generate CREATE SCHEMA statement.
        https://www.postgresql.org/docs/current/sql-createschema.html
    >>> as_string(create_namespace(('foospc',)))
    'CREATE SCHEMA IF NOT EXISTS foospc'
    '''
    return compose([S('CREATE SCHEMA IF NOT EXISTS'), I(*qualname)])

def alter_namespace_owner_to(qualname, rolname):
    ''' Generate ALTER SCHEMA OWNER TO.
    >>> as_string(alter_namespace_owner_to(('public',), 'root'))
    'ALTER SCHEMA public OWNER TO root'
    '''
    return compose([S('ALTER SCHEMA'), I(*qualname), S('OWNER TO'), I(rolname)])

def create_sequence(
        qualname, data_type=None, increment=None, minvalue=None, maxvalue=None, start=None, cache=None, cycle=None,
        owned_by=None, unlogged=False, restart=None):
    ''' Generate CREATE SEQUENCE statement.
        https://www.postgresql.org/docs/current/sql-createsequence.html
    >>> as_string(create_sequence(('public', 'foo_id_seq')))
    'CREATE SEQUENCE public.foo_id_seq NO MINVALUE NO MAXVALUE OWNED BY NONE'
    >>> as_string(create_sequence(('public', 'foo id seq')))
    'CREATE SEQUENCE public."foo id seq" NO MINVALUE NO MAXVALUE OWNED BY NONE'
    >>> as_string(create_sequence(('public', 'foo_id_seq'), increment=-2, minvalue=100, maxvalue=999, start=998, \
        cache=3, cycle=True, owned_by=('public', 'bar', 'id')))  # pylint: disable=line-too-long
    'CREATE SEQUENCE public.foo_id_seq INCREMENT -2 MINVALUE 100 MAXVALUE 999 START 998 CACHE 3 CYCLE \
OWNED BY public.bar.id'
    >>> as_string(create_sequence(('public', 'foo_id_seq'), unlogged=True))
    'CREATE UNLOGGED SEQUENCE public.foo_id_seq NO MINVALUE NO MAXVALUE OWNED BY NONE'
    '''
    return compose(
        [S('CREATE'), S('UNLOGGED') if unlogged else '', S('SEQUENCE'), I(*qualname)],
        _sequence_opts(data_type=data_type, increment=increment, minvalue=minvalue, maxvalue=maxvalue, start=start,
            cache=cache, cycle=cycle, owned_by=owned_by, restart=restart))

def alter_sequence(
        qualname, data_type=None, increment=None, minvalue=None, maxvalue=None, start=None, cache=None, cycle=None,
        owned_by=None, restart=None):
    ''' Generate ALTER SEQUENCE statement.
        https://www.postgresql.org/docs/current/sql-altersequence.html
        RESTART option intentionally omitted because resetting the sequence state is not desired/intended.
    >>> as_string(alter_sequence(('public', 'foo_id_seq'), cycle=True))
    'ALTER SEQUENCE public.foo_id_seq NO MINVALUE NO MAXVALUE CYCLE OWNED BY NONE'
    >>> as_string(alter_sequence(('public', 'foo_id_seq'), increment=-4, minvalue=-6, maxvalue=66, start=6, cache=2, \
        cycle=False, owned_by=('public', 'bar', 'id')))  # pylint: disable=line-too-long
    'ALTER SEQUENCE public.foo_id_seq INCREMENT -4 MINVALUE -6 MAXVALUE 66 START 6 CACHE 2 NO CYCLE \
OWNED BY public.bar.id'
    '''
    return compose(
        [S('ALTER'), S('SEQUENCE'), I(*qualname)], _sequence_opts(
        data_type=data_type, increment=increment, minvalue=minvalue, maxvalue=maxvalue, start=start, cache=cache,
        cycle=cycle, owned_by=owned_by, restart=restart, alter=True))

def alter_sequence_persistence(qualname, unlogged=False):
    ''' Generate ALTER SEQUENCE SET { LOGGED | UNLOGGED } statement.
        https://www.postgresql.org/docs/current/sql-altersequence.html
    >>> as_string(alter_sequence_persistence(('public', 'foo_id_seq'), unlogged=True))
    'ALTER SEQUENCE public.foo_id_seq SET UNLOGGED'
    >>> as_string(alter_sequence_persistence(('public', 'foo_id_seq'), unlogged=False))
    'ALTER SEQUENCE public.foo_id_seq SET LOGGED'
    '''
    return compose([
        S('ALTER'), S('SEQUENCE'), I(*qualname), S('SET'), S('UNLOGGED' if unlogged else 'LOGGED')])

def alter_sequence_owner_to(qualname, rolname):
    ''' Generate ALTER SEQUENCE OWNER TO.
    >>> as_string(alter_sequence_owner_to(('public', 'fooseqid'), 'root'))
    'ALTER SEQUENCE public.fooseqid OWNER TO root'
    '''
    return compose([S('ALTER SEQUENCE'), I(*qualname), S('OWNER TO'), I(rolname)])

def _table_opts():
    return []

def _table_inherits(inherits):
    return (
        [S('INHERITS'), S('( {} )').format(S(', ').join([I(*x) for x in inherits]))]
        if inherits else [])

def split_partexpressions(s):
    ''' Multiple expressions can be present separated by commas. Parenthesis determines split points.
    >>> split_partexpressions('(a + b)')
    ['(a + b)']
    >>> split_partexpressions('(a + b), (a - b)')
    ['(a + b)', '(a - b)']
    >>> split_partexpressions("(((a + b) + 7) + hashtext('hello'::text))")
    ["(((a + b) + 7) + hashtext('hello'::text))"]
    >>> split_partexpressions("(((a + b) + 7) + hashtext('hello'::text)), (((a * b) + (b * a)) + 2)")
    ["(((a + b) + 7) + hashtext('hello'::text))", '(((a * b) + (b * a)) + 2)']
    '''
    ret = []
    buf = ''
    parens = 0
    for c in s:
        buf += c
        if c == '(':
            parens += 1
        elif c == ')':
            parens -= 1
        if parens == 0:
            if buf not in (' ', ','):
                ret.append(buf)
            buf = ''
    return ret

def _collate(collation):
    if not collation:
        return []
    assert re.match(r'^[a-zA-Z0-9_\.-]+$', collation), 'unexpected collation name: %r' % collation
    return [S('COLLATE "%s"' % collation)]

def _table_access_method(amname):
    return [S('USING'), I(amname)] if amname else []

def _table_reloptions(reloptions, keyword):
    assert keyword in ('WITH', 'SET')
    return [S('%s ( {} ) ' % keyword).format(S(', ').join([S(o) for o in reloptions]))] if reloptions else []

def _table_partition_of(parent_table):
    return[
        S('PARTITION OF'),
        I(*parent_table)
    ]

def _create_column_for_part(colname, attnotnull=None, attidentity='', attgenerated=None, atthasdef=None,
        attdef=None):
    return [S(colname), S('WITH OPTIONS'),
        *_column_not_null(attnotnull), *_column_default_generated(attidentity, attgenerated, atthasdef, attdef)]

def _tablespace(spcname=None):
    if not spcname:
        return []
    return [S('TABLESPACE'), S(spcname)]

def create_table_pt_of(qualname, parent_table, cols, unlogged=False,
                       partdef=None, partition_bound_spec=None,
                       amname=None, reloptions=None, spcname=None):
    ''' Generate CREATE TABLE PARTITION OF TABLE
    >>> as_string(create_table_pt_of(('public', 'foo'), ('public', 'parent'), cols = [\
        {'colname': 'id', 'typname': 'integer', 'collname': None, 'attnotnull': True, 'attidentity': '', \
        'attgenerated': '', 'atthasdef': True, 'attdef': "nextval('email_address_id_seq'::regclass)"}], \
        reloptions=['fillfactor=50', 'security_barrier=true'], \
        partition_bound_spec="FOR VALUES FROM ('2016-07-01') TO ('2016-08-01')"))
    "CREATE TABLE public.foo PARTITION OF public.parent ( id WITH OPTIONS NOT NULL \
DEFAULT nextval('email_address_id_seq'::regclass) ) \
FOR VALUES FROM ('2016-07-01') TO ('2016-08-01') WITH ( fillfactor=50, security_barrier=true )"
    '''
    ret = [S('CREATE'), S('UNLOGGED') if unlogged else '', S('TABLE'), I(*qualname), *_table_partition_of(parent_table)]
    if cols:
        ret.append(S('('))
        for col in (cols or []):
            ret.extend(_create_column_for_part(
                    col['colname'], col['attnotnull'], col['attidentity'], col['attgenerated'], col['atthasdef'],
                    col['attdef']))
            ret.append(S(','))
        ret = ret[:-1] if ret[-1] == S(',') else ret
        ret.append(S(')'))
    ret.extend(
        [S(partition_bound_spec)] +
        ([S('PARTITION BY'), S(partdef)] if partdef else []) +
        _table_access_method(amname)+
        _table_reloptions(reloptions, 'WITH')+
        _tablespace(spcname))
    return compose(ret)

def create_table_of_type(qualname, of_type, unlogged=False, partdef=None,
                         amname=None, reloptions=None, cols=None, spcname=None):
    ''' Generate CREATE TABLE OF type
    https://www.postgresql.org/docs/current/sql-createtable.html
    >>> as_string(create_table_of_type(('public', 'foo'), ('public', 'ty'), unlogged=False,\
        reloptions=['fillfactor=50', 'security_barrier=true'], cols = [{'colname': 'id', 'typname': 'integer', \
        'collname': None, 'attnotnull': True, 'attidentity': '', 'attgenerated': '', 'atthasdef': True, \
        'attdef': "nextval('email_address_id_seq'::regclass)"}], spcname='spacefoo'))
    "CREATE TABLE public.foo OF public.ty ( id WITH OPTIONS NOT NULL \
DEFAULT nextval('email_address_id_seq'::regclass) ) WITH ( fillfactor=50, security_barrier=true ) TABLESPACE spacefoo"
    '''
    ret = [
        S('CREATE'), S('UNLOGGED') if unlogged else '', S('TABLE'), I(*qualname), *_table_opts(), S('OF'), I(*of_type)]
    ret.append(S('('))
    for col in (cols or []):
        ret.extend(_create_column_for_part(
                col['colname'], col['attnotnull'], col['attidentity'], col['attgenerated'], col['atthasdef'],
                col['attdef']))
        ret.append(S(','))
    ret = ret[:-1] if ret[-1] == S(',') else ret
    ret.append(S(')'))
    ret.extend(
        ([S('PARTITION BY'), S(partdef)] if partdef else []) +
        _table_access_method(amname)+
        _table_reloptions(reloptions, 'WITH')+
        _tablespace(spcname))
    return compose(ret)

def create_table(
        qualname, unlogged=False, partdef=None, inherits=(), amname=None,
        reloptions=None, cols=None, spcname=None):
    ''' Generate CREATE TABLE statement.
        https://www.postgresql.org/docs/current/sql-createtable.html
    >>> as_string(create_table(('public', 'foo')))
    'CREATE TABLE public.foo ( )'
    >>> as_string(create_table(('public', 'foo'), unlogged=True))
    'CREATE UNLOGGED TABLE public.foo ( )'
    >>> as_string(create_table(('public', 'foo'), inherits=(('public', 'bar'), ('other', 'baz'))))
    'CREATE TABLE public.foo ( ) INHERITS ( public.bar, other.baz )'
    >>> as_string(create_table(('public', 'foo'), partdef='RANGE ( a int4_ops, b int2_ops )'))
    'CREATE TABLE public.foo ( ) PARTITION BY RANGE ( a int4_ops, b int2_ops )'
    >>> as_string(create_table(('public', 'foo'), partdef='RANGE ( name COLLATE "zu-ZA-x-icu" text_ops )'))
    'CREATE TABLE public.foo ( ) PARTITION BY RANGE ( name COLLATE "zu-ZA-x-icu" text_ops )'
    >>> as_string(create_table(('public', 'foo'),\
        partdef='RANGE ( a COLLATE "zu-ZA-x-icu" text_ops, (a::int + b) COLLATE "C.utf8" text_ops, b int_ops, \
(hashtext(a) + b) COLLATE "ucs_basic" text_ops )'))
    'CREATE TABLE public.foo ( ) PARTITION BY RANGE ( a COLLATE "zu-ZA-x-icu" text_ops, (a::int + b) \
COLLATE "C.utf8" text_ops, b int_ops, (hashtext(a) + b) COLLATE "ucs_basic" text_ops )'
    >>> as_string(create_table(('public', 'foo'), amname='heap'))
    'CREATE TABLE public.foo ( ) USING heap'
    >>> as_string(create_table(('public', 'foo'), reloptions=['fillfactor=50', 'security_barrier=true']))
    'CREATE TABLE public.foo ( ) WITH ( fillfactor=50, security_barrier=true )'
    >>> as_string(create_table(('public', 'foo'), cols = [\
        {'colname': 'id', 'typname': 'integer', 'collname': None, 'attnotnull': True, 'attidentity': '',\
        'attgenerated': '', 'atthasdef': True, 'attdef': "nextval('email_address_id_seq'::regclass)"}]))
    "CREATE TABLE public.foo ( id integer NOT NULL DEFAULT nextval('email_address_id_seq'::regclass) )"
    >>> as_string(create_table(('public', 'foo'), cols = [\
        {'colname': 'id', 'typname': 'integer', 'collname': None, 'attnotnull': True, 'attidentity': '',\
        'attgenerated': '', 'atthasdef': True, 'attdef': "nextval('email_address_id_seq'::regclass)"}]))
    "CREATE TABLE public.foo ( id integer NOT NULL DEFAULT nextval('email_address_id_seq'::regclass) )"
    '''
    ret = [S('CREATE'), S('UNLOGGED') if unlogged else '', S('TABLE'), I(*qualname), *_table_opts(), S('(')]
    if cols:
        for col in (cols or []):
            ret.extend(_create_column_inner(
                    col['colname'], col['typname'], col['collname'], col['attnotnull'], col['attidentity'],
                    col['attgenerated'], col['atthasdef'], col['attdef'])) if col else None
            ret.append(S(','))
    ret = ret[:-1] if ret[-1] == S(',') else ret
    ret.extend(
        [S(')')]+
        _table_inherits(inherits) +
        ([S('PARTITION BY'), S(partdef)] if partdef else []) +
        _table_access_method(amname)+
        _table_reloptions(reloptions, 'WITH')+
        _tablespace(spcname))
    return compose(ret)

def _alter_table(qualname):
    return [S('ALTER TABLE'), I(*qualname[:2])]

def optnames(opts):
    '''
    >>> optnames(['fillfactor=51', 'toast_tuple_target=2134'])
    {'fillfactor', 'toast_tuple_target'}
    '''
    return {x.split('=')[0] for x in opts or []}

def alter_table_set_reloptions(qualname, reloptions):
    ''' Generate ALTER TABLE SET.
        https://www.postgresql.org/docs/current/sql-altertable.html
    >>> as_string(alter_table_set_reloptions(('public', 'foo', 'id'),\
        reloptions=['fillfactor=51', 'toast_tuple_target=2134']))
    'ALTER TABLE public.foo SET ( fillfactor=51, toast_tuple_target=2134 )'
    '''
    return compose(_alter_table(qualname), _table_reloptions(reloptions, 'SET'))

def _table_reset_attributes(reloptions, from_reloptions):
    missing = optnames(from_reloptions) - optnames(reloptions)
    return [S('RESET ( {} ) ').format(S(', ').join([S(o) for o in missing]))] if missing else []

def alter_table_reset_attributes(qualname, reloptions, from_reloptions):
    ''' Generate ALTER TABLE RESET.
        https://www.postgresql.org/docs/current/sql-altertable.html
    >>> as_string(alter_table_reset_attributes(('public', 'foo', 'id'), reloptions=['fillfactor=51'],\
        from_reloptions=['toast_tuple_target=2134']))
    'ALTER TABLE public.foo RESET ( toast_tuple_target )'
    '''
    return compose(_alter_table(qualname), _table_reset_attributes(reloptions, from_reloptions))

def alter_table_unlogged(qualname, relpersistence):
    ''' Generate ALTER TABLE SET { LOGGED | UNLOGGED }.
        https://www.postgresql.org/docs/current/sql-altertable.html
    >>> as_string(alter_table_unlogged(('public', 'foo', 'id'), 'u'))
    'ALTER TABLE public.foo SET UNLOGGED'
    >>> as_string(alter_table_unlogged(('public', 'foo', 'id'), 'p'))
    'ALTER TABLE public.foo SET LOGGED'
    '''
    return compose(_alter_table(qualname), [S('SET'), S({'p': 'LOGGED', 'u': 'UNLOGGED'}[relpersistence])])

def alter_table_access_method(qualname, amname):
    ''' Generate ALTER TABLE SET { LOGGED | UNLOGGED }.
        https://www.postgresql.org/docs/current/sql-altertable.html
    >>> as_string(alter_table_access_method(('public', 'foo', 'id'), 'heap'))
    'ALTER TABLE public.foo SET ACCESS METHOD heap'
    '''
    return compose(_alter_table(qualname), [S('SET ACCESS METHOD'), I(amname)])

def alter_table_tablespace(qualname, spcname):
    ''' Generate ALTER TABLE SET { LOGGED | UNLOGGED }.
        https://www.postgresql.org/docs/current/sql-altertable.html
    >>> as_string(alter_table_tablespace(('public', 'foo', 'id'), 'yourspace'))
    'ALTER TABLE public.foo SET TABLESPACE yourspace'
    '''
    return compose(_alter_table(qualname), [S('SET TABLESPACE'), I(spcname)])

def alter_table_attach_partition(qualname, partname, bound):
    ''' Generate ALTER TABLE ATTACH PARTITION
        https://www.postgresql.org/docs/current/sql-altertable.html
    >>> as_string(alter_table_attach_partition(('public', 'foo'), 'foo_part1',\
        "FOR VALUES FROM ('2006-02-01') TO ('2006-03-01')"))
    "ALTER TABLE public.foo ATTACH PARTITION foo_part1 FOR VALUES FROM ('2006-02-01') TO ('2006-03-01')"
    '''
    return compose(_alter_table(qualname), [S('ATTACH PARTITION'), S(partname), S(bound)])

def alter_table_detach_partition(qualname, partname, concurrent=False, finalize=False):
    ''' Generate ALTER TABLE DETACH PARTITION
        https://www.postgresql.org/docs/current/sql-altertable.html
    >>> as_string(alter_table_detach_partition(('public', 'foo'), 'foo_part1', concurrent=True))
    'ALTER TABLE public.foo DETACH PARTITION foo_part1 CONCURRENTLY'
    '''
    return compose(
        _alter_table(qualname), [
            S('DETACH PARTITION'), S(partname),
            S('CONCURRENTLY') if concurrent else None,
            S('FINALIZE') if finalize else None])

def alter_table_add_constraint(
        qualname, constraint_name, constraint_def, contype, connoinherit, condeferrable, condeferred,
        convalidated=None, index_name=None, notvalid=False):
    ''' Generate ALTER TABLE ADD CONSTRAINT
        https://www.postgresql.org/docs/current/sql-altertable.html
    >>> as_string(alter_table_add_constraint(('public', 'foo'), 'public.con', 'PRIMARY KEY (id)', 'p', False, True,\
        True, index_name='index'))
    'ALTER TABLE public.foo ADD CONSTRAINT public.con PRIMARY KEY USING INDEX index DEFERRABLE INITIALLY DEFERRED'
    >>> as_string(alter_table_add_constraint(('public', 'foo'), 'public.con', 'CHECK ((VALUE >= 0))', 'c', True, False,\
        False, convalidated=False))
    'ALTER TABLE public.foo ADD CONSTRAINT public.con CHECK ((VALUE >= 0)) NO INHERIT NOT DEFERRABLE \
INITIALLY IMMEDIATE NOT VALID'
    '''
    ret = _alter_table(qualname)
    ret.extend([
        S('ADD CONSTRAINT'),
        S(constraint_name)])

    if index_name and contype in ('u', 'p'):
        ret.extend([
            S({'u': 'UNIQUE', 'p': 'PRIMARY KEY'}[contype]),
            S('USING INDEX'),
            S(index_name)])
    else:
        # 'f': 'FOREIGN KEY', 'u': 'UNIQUE', 'x': 'EXCLUDE', 'c': 'CHECK'
        # looks like constraint_def takes care of these details
        ret.append(S(constraint_def))

    ret.extend([
        S('NO INHERIT') if contype == 'c' and connoinherit else None,
        S('DEFERRABLE') if condeferrable else S('NOT DEFERRABLE'),
        S('INITIALLY DEFERRED') if condeferred else S('INITIALLY IMMEDIATE'),
    ])
    if contype in ('f', 'c') and (notvalid or not convalidated):
        ret.append(S('NOT VALID'))
    return compose(ret)

def alter_table_alter_constraint(qualname, constraint_name, condeferrable, condeferred):
    ''' Generate ALTER TABLE ALTER CONSTRAINT
        https://www.postgresql.org/docs/current/sql-altertable.html
    >>> as_string(alter_table_alter_constraint(('public', 'foo'), 'public.con', True, True))
    'ALTER TABLE public.foo ALTER CONSTRAINT public.con DEFERRABLE INITIALLY DEFERRED'
    >>> as_string(alter_table_alter_constraint(('public', 'foo'), 'public.con', False, False))
    'ALTER TABLE public.foo ALTER CONSTRAINT public.con NOT DEFERRABLE INITIALLY IMMEDIATE'
    '''
    return compose(_alter_table(qualname), [
        S('ALTER CONSTRAINT'), S(constraint_name),
        S('DEFERRABLE') if condeferrable else S('NOT DEFERRABLE'),
        S('INITIALLY DEFERRED') if condeferred else S('INITIALLY IMMEDIATE')])

def alter_table_drop_constraint(qualname, constraint_name):
    ''' Generate ALTER TABLE DROP CONSTRAINT
        https://www.postgresql.org/docs/current/sql-altertable.html
    >>> as_string(alter_table_drop_constraint(('public', 'foo'), 'public.con'))
    'ALTER TABLE public.foo DROP CONSTRAINT public.con'
    >>> as_string(alter_table_drop_constraint(('public', 'foo'), 'public.con'))
    'ALTER TABLE public.foo DROP CONSTRAINT public.con'
    '''
    return compose(_alter_table(qualname), [S('DROP CONSTRAINT'), S(constraint_name)])

def alter_table_trigger(qualname, trigger_name, tgenabled):
    ''' Generate ALTER TABLE cmd TRIGGER
        cmd is based on tgenabled.
        possible values: `ENABLE`, `DISABLE`, `ENABLE ALWAYS`, `ENABLE REPLICA`
        https://www.postgresql.org/docs/current/sql-altertable.html
    >>> as_string(alter_table_trigger(('public', 'foo'), 'trig', 'D'))
    'ALTER TABLE public.foo DISABLE TRIGGER trig'
    >>> as_string(alter_table_trigger(('public', 'foo'), 'trig', 'O'))
    'ALTER TABLE public.foo ENABLE TRIGGER trig'
    '''
    cmd = {'O': 'ENABLE', 'D': 'DISABLE', 'R': 'ENABLE REPLICA', 'A': 'ENABLE ALWAYS'}[tgenabled]
    return compose(_alter_table(qualname), [S(cmd), S('TRIGGER'), S(trigger_name)])

def alter_table_rule(qualname, rulename, ev_enabled):
    ''' Generate ALTER TABLE cmd RULE
        possible cmd values: `ENABLE`, `DISABLE`, `ENABLE ALWAYS`, `ENABLE REPLICA`
        https://www.postgresql.org/docs/current/sql-altertable.html
    >>> as_string(alter_table_rule(('public', 'foo'), 'rule', 'D'))
    'ALTER TABLE public.foo DISABLE RULE rule'
    >>> as_string(alter_table_rule(('public', 'foo'), 'rule', 'R'))
    'ALTER TABLE public.foo ENABLE REPLICA RULE rule'
    '''
    cmd = {'O': 'ENABLE', 'D': 'DISABLE', 'R': 'ENABLE REPLICA', 'A': 'ENABLE ALWAYS'}[ev_enabled]
    return compose(_alter_table(qualname), [S(cmd), S('RULE'), S(rulename)])

def alter_table_inherit(qualname, parent, no):
    ''' Generate ALTER TABLE INHERIT/ NO INHERIT parent
        https://www.postgresql.org/docs/current/sql-altertable.html
    >>> as_string(alter_table_inherit(('public', 'foo'), ('public', 'par'), True))
    'ALTER TABLE public.foo NO INHERIT public.par'
    >>> as_string(alter_table_inherit(('public', 'foo'), ('public', 'par'), False))
    'ALTER TABLE public.foo INHERIT public.par'
    '''
    cmd = 'NO INHERIT' if no else 'INHERIT'
    return compose(_alter_table(qualname), [S(cmd), I(*parent)])

def alter_table_type(qualname, no, ty=None):
    ''' Generate ALTER TABLE OF / NOT OF type
        https://www.postgresql.org/docs/current/sql-altertable.html
    >>> as_string(alter_table_type(('public', 'foo'), True))
    'ALTER TABLE public.foo NOT OF'
    >>> as_string(alter_table_type(('public', 'foo'), False, ('public', 'par')))
    'ALTER TABLE public.foo OF public.par'
    '''
    cmd = 'NOT OF' if no else 'OF'
    return compose(_alter_table(qualname), [S(cmd), I(*ty) if not no else None])

def alter_table_cluster_on(qualname, index_name):
    ''' Generate ALTER TABLE CLUSTER ON
    >>> as_string(alter_table_cluster_on(('public', 'foo'), ('public', 'idx')))
    'ALTER TABLE public.foo CLUSTER ON idx'
    '''
    return compose(_alter_table(qualname), [S('CLUSTER ON'), I(index_name[1])])

def alter_table_no_cluster(qualname):
    ''' Generate ALTER TABLE SET WITHOUT CLUSTER
    >>> as_string(alter_table_no_cluster(('public', 'foo')))
    'ALTER TABLE public.foo SET WITHOUT CLUSTER'
    '''
    return compose(_alter_table(qualname), [S('SET WITHOUT CLUSTER')])

def alter_table_row_level_security(qualname, yes):
    ''' Generate ALTER TABLE ROW LEVEL SECURITY
    >>> as_string(alter_table_row_level_security(('public', 'foo'), True))
    'ALTER TABLE public.foo ENABLE ROW LEVEL SECURITY'
    >>> as_string(alter_table_row_level_security(('public', 'foo'), False))
    'ALTER TABLE public.foo DISABLE ROW LEVEL SECURITY'
    '''
    return compose(_alter_table(qualname), [S('ENABLE') if yes else S('DISABLE'), S('ROW LEVEL SECURITY')])

def alter_table_row_level_force_security(qualname, yes):
    ''' Generate ALTER TABLE ROW LEVEL SECURITY
    >>> as_string(alter_table_row_level_force_security(('public', 'foo'), True))
    'ALTER TABLE public.foo FORCE ROW LEVEL SECURITY'
    >>> as_string(alter_table_row_level_force_security(('public', 'foo'), False))
    'ALTER TABLE public.foo NO FORCE ROW LEVEL SECURITY'
    '''
    return compose(_alter_table(qualname), [S('FORCE') if yes else S('NO FORCE'), S('ROW LEVEL SECURITY')])

def alter_table_validate_constraint(qualname, constraint_name):
    ''' Generate ALTER TABLE VALIDATE CONSTRAINT
    >>> as_string(alter_table_validate_constraint(('public', 'foo'), ('public', 'cons')))
    'ALTER TABLE public.foo VALIDATE CONSTRAINT public.cons'
    '''
    return compose(_alter_table(qualname), [S('VALIDATE CONSTRAINT'), I(*constraint_name)])

def alter_table_replica_id(qualname, relreplident, ind_name=None):
    ''' Generate ALTER TABLE REPLICA IDENTITY
    >>> as_string(alter_table_replica_id(('public', 'foo'), 'n'))
    'ALTER TABLE public.foo REPLICA IDENTITY NOTHING'
    >>> as_string(alter_table_replica_id(('public', 'foo'), 'i', ind_name='ind'))
    'ALTER TABLE public.foo REPLICA IDENTITY USING INDEX ind'
    '''
    opt = {'d': 'DEFAULT', 'n': 'NOTHING', 'f': 'FULL', 'i': 'USING INDEX'}.get(relreplident)
    return compose(_alter_table(qualname), [S('REPLICA IDENTITY'), S(opt), S(ind_name) if ind_name else None])

def alter_table_owner(qualname, rolname):
    ''' Generate ALTER TABLE OWNER
    >>> as_string(alter_table_owner(('public', 'foo'), 'new_role'))
    'ALTER TABLE public.foo OWNER TO new_role'
    '''
    return compose(_alter_table(qualname), [S('OWNER TO'), S(rolname)])

def _column_not_null(attnotnull):
    return {None: [], True: [S('NOT NULL')], False: [S('NULL')]}[attnotnull]

def _column_default_generated(attidentity, attgenerated, atthasdef, attdef):
    ''' https://www.postgresql.org/docs/current/catalog-pg-attribute.html '''
    assert attidentity in ('', 'a', 'd'), 'unexpected attidentity %r' % attidentity
    if attgenerated == 's':
        assert not attidentity, 'unexpected attidentity %r' % attidentity
        assert atthasdef, 'unexpected atthasdef %r' % atthasdef
        return [S('GENERATED ALWAYS AS ( {} ) STORED ').format(S(attdef))]
    if attidentity == 'a':
        assert not attgenerated, 'unexpected attgenerated %r' % attgenerated
        assert not atthasdef, 'unexpected atthasdef %r' % atthasdef
        return [S('GENERATED ALWAYS AS IDENTITY')]
    if attidentity == 'd':
        assert not attgenerated, 'unexpected attgenerated %r' % attgenerated
        assert not atthasdef, 'unexpected atthasdef %r' % atthasdef
        return [S('GENERATED BY DEFAULT AS IDENTITY')]
    if atthasdef:
        return [S('DEFAULT'), S(attdef)]
    return []

def _create_column_inner(colname, typname, collname=None, attnotnull=None, attidentity='',
                         attgenerated=None, atthasdef=None, attdef=None):
    return [I(colname), S(typname), *_collate(collname),
        *_column_not_null(attnotnull), *_column_default_generated(attidentity, attgenerated, atthasdef, attdef)]

def create_column(
        qualname, typname, collname=None, attnotnull=None, attidentity='', attgenerated=None, atthasdef=None,
        attdef=None):
    ''' Generate ALTER TABLE ADD COLUMN statement.
        https://www.postgresql.org/docs/current/sql-altertable.html
    >>> as_string(create_column(('public', 'foo', 'id'), 'int4'))
    'ALTER TABLE public.foo ADD COLUMN id int4'
    >>> as_string(create_column(('public', 'foo', 'id'), 'int4', collname='C.utf8'))
    'ALTER TABLE public.foo ADD COLUMN id int4 COLLATE "C.utf8"'
    >>> as_string(create_column(('public', 'foo', 'id'), 'int4', collname='C.utf8'))
    'ALTER TABLE public.foo ADD COLUMN id int4 COLLATE "C.utf8"'
    >>> as_string(create_column(('public', 'foo', 'id'), 'int4', attnotnull=True))
    'ALTER TABLE public.foo ADD COLUMN id int4 NOT NULL'
    >>> as_string(create_column(('public', 'foo', 'id'), 'int4', attnotnull=False))
    'ALTER TABLE public.foo ADD COLUMN id int4 NULL'
    >>> as_string(create_column(('public', 'foo', 'id'), 'int8', atthasdef=True, attdef='nextval(foo_seq_id) + 7',\
        attgenerated=''))
    'ALTER TABLE public.foo ADD COLUMN id int8 DEFAULT nextval(foo_seq_id) + 7'
    >>> as_string(create_column(('public', 'foo', 'id'), 'int8', atthasdef=True, attdef='7', attgenerated='s'))
    'ALTER TABLE public.foo ADD COLUMN id int8 GENERATED ALWAYS AS ( 7 ) STORED'
    >>> as_string(create_column(('public', 'foo', 'id'), 'int8', atthasdef=False, attdef='7', attgenerated='',\
        attidentity='a'))
    'ALTER TABLE public.foo ADD COLUMN id int8 GENERATED ALWAYS AS IDENTITY'
    >>> as_string(create_column(('public', 'foo', 'id'), 'int8', atthasdef=False, attdef='7', attgenerated='',\
        attidentity='d'))
    'ALTER TABLE public.foo ADD COLUMN id int8 GENERATED BY DEFAULT AS IDENTITY'
    '''
    ret = [S('ALTER TABLE'), I(*qualname[:2]), S('ADD COLUMN')]
    ret.extend(_create_column_inner(
        qualname[2], typname, collname, attnotnull, attidentity, attgenerated, atthasdef,
        attdef
    ))
    return compose(ret)

def _alter_column(qualname):
    return [S('ALTER TABLE'), I(*qualname[:2]), S('ALTER COLUMN'), I(qualname[2])]

def _alter_column_type(typname):
    return [S('TYPE'), S(typname)] if typname else []

def alter_column_type(qualname, typname, collname=None):
    ''' Generate ALTER TABLE ALTER COLUMN statement to change the column type.
        https://www.postgresql.org/docs/current/sql-altertable.html
    >>> as_string(alter_column_type(('public', 'foo', 'id'), typname='int4'))
    'ALTER TABLE public.foo ALTER COLUMN id TYPE int4 USING id::int4'
    >>> as_string(alter_column_type(('public', 'foo', 'id'), typname='text', collname='C'))
    'ALTER TABLE public.foo ALTER COLUMN id TYPE text COLLATE "C" USING id::text'
    '''
    return compose(
        _alter_column(qualname), _alter_column_type(typname), _collate(collname),
        [S('USING'), S('{}::{}').format(I(qualname[2]), S(typname))])

def alter_column_default(qualname, attidentity='', attgenerated=None, atthasdef=None, attdef=None):
    ''' Generate ALTER TABLE ALTER COLUMN {SET, DROP} DEFAULT.
        https://www.postgresql.org/docs/current/sql-altertable.html
    >>> as_string(alter_column_default(('public', 'foo', 'id'), atthasdef=True, attdef='nextval(foo_seq_id) + 7',\
        attgenerated=''))
    'ALTER TABLE public.foo ALTER COLUMN id SET DEFAULT nextval(foo_seq_id) + 7'
    >>> as_string(alter_column_default(('public', 'foo', 'id'), atthasdef=False, attdef=None))
    'ALTER TABLE public.foo ALTER COLUMN id DROP DEFAULT'
    '''
    assert not attgenerated, 'unexpected attgenerated %r' % attgenerated
    assert not attidentity, 'unexpected attidentity %r' % attidentity
    if atthasdef:
        return compose(_alter_column(qualname), [S('SET DEFAULT'), S(attdef)])
    return compose(_alter_column(qualname), [S('DROP DEFAULT')])

def alter_column_drop_expression(qualname, attidentity='', attgenerated=None, atthasdef=None):
    ''' Generate ALTER TABLE ALTER COLUMN DROP EXPRESSION.
        https://www.postgresql.org/docs/current/sql-altertable.html
    >>> as_string(alter_column_drop_expression(('public', 'foo', 'id'), attidentity='', attgenerated='',\
        atthasdef=False))
    'ALTER TABLE public.foo ALTER COLUMN id DROP EXPRESSION'
    '''
    assert attgenerated == '', 'unexpected attgenerated %r' % attgenerated
    assert attidentity == '', 'unexpected attidentity %r' % attidentity
    assert not atthasdef, 'unexpected atthasdef %r' % atthasdef
    return compose(_alter_column(qualname), [S('DROP EXPRESSION')])

def alter_column_generated_identity(qualname, attidentity='', attgenerated=None, atthasdef=None, from_attidentity=None):
    ''' Generate ALTER TABLE ALTER COLUMN {ADD | SET} GENERATED {ALWAYS | BY DEFAULT} AS IDENTITY.
        https://www.postgresql.org/docs/current/sql-altertable.html
    >>> as_string(alter_column_generated_identity(('public', 'foo', 'id'), attidentity='a', attgenerated='',\
        atthasdef=False, from_attidentity=''))
    'ALTER TABLE public.foo ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY'
    >>> as_string(alter_column_generated_identity(('public', 'foo', 'id'), attidentity='d', attgenerated='',\
        atthasdef=False, from_attidentity=''))
    'ALTER TABLE public.foo ALTER COLUMN id ADD GENERATED BY DEFAULT AS IDENTITY'
    >>> as_string(alter_column_generated_identity(('public', 'foo', 'id'), attidentity='a', attgenerated='',\
        atthasdef=False, from_attidentity='d'))
    'ALTER TABLE public.foo ALTER COLUMN id SET GENERATED ALWAYS'
    >>> as_string(alter_column_generated_identity(('public', 'foo', 'id'), attidentity='d', attgenerated='',\
        atthasdef=False, from_attidentity='a'))
    'ALTER TABLE public.foo ALTER COLUMN id SET GENERATED BY DEFAULT'
    '''
    assert attgenerated == '', 'unexpected attgenerated %r' % attgenerated
    assert not atthasdef, 'unexpected atthasdef %r' % atthasdef
    assert attidentity in ('a', 'd'), 'unexpected attidentity %r' % attidentity
    if from_attidentity:
        return compose(
            _alter_column(qualname), [S('SET GENERATED'), S({'a': 'ALWAYS', 'd': 'BY DEFAULT'}[attidentity])])
    return compose(
        _alter_column(qualname),
        [S('ADD GENERATED'), S({'a': 'ALWAYS', 'd': 'BY DEFAULT'}[attidentity]), S('AS IDENTITY')])

def alter_column_drop_identity(qualname):
    ''' Generate ALTER TABLE ALTER COLUMN DROP IDENTITY.
        https://www.postgresql.org/docs/current/sql-altertable.html
    >>> as_string(alter_column_drop_identity(('public', 'foo', 'id')))
    'ALTER TABLE public.foo ALTER COLUMN id DROP IDENTITY'
    '''
    return compose(_alter_column(qualname), [S('DROP IDENTITY')])

def alter_column_null(qualname, attnotnull):
    ''' Generate ALTER TABLE ALTER COLUMN {SET | DROP} NOT NULL.
        https://www.postgresql.org/docs/current/sql-altertable.html
    >>> as_string(alter_column_null(('public', 'foo', 'id'), attnotnull=True))
    'ALTER TABLE public.foo ALTER COLUMN id SET NOT NULL'
    >>> as_string(alter_column_null(('public', 'foo', 'id'), attnotnull=False))
    'ALTER TABLE public.foo ALTER COLUMN id DROP NOT NULL'
    '''
    return compose(_alter_column(qualname), [S('SET' if attnotnull else 'DROP'), S('NOT NULL')])

def alter_column_set_statistics(qualname, attstattarget):
    ''' Generate ALTER TABLE ALTER COLUMN SET STATISTICS.
        https://www.postgresql.org/docs/current/sql-altertable.html
    >>> as_string(alter_column_set_statistics(('public', 'foo', 'id'), attstattarget=42))
    'ALTER TABLE public.foo ALTER COLUMN id SET STATISTICS 42'
    '''
    return compose(_alter_column(qualname), [S('SET STATISTICS'), L(attstattarget)])

def _column_set_attributes(attoptions):
    return [S('SET ( {} ) ').format(S(', ').join([S(o) for o in attoptions]))] if attoptions else []

def alter_column_set_attributes(qualname, attoptions):
    ''' Generate ALTER TABLE ALTER COLUMN SET.
        https://www.postgresql.org/docs/current/sql-altertable.html
    >>> as_string(alter_column_set_attributes(('public', 'foo', 'id'),\
        attoptions=['n_distinct=1000', 'n_distinct_inherited=999']))
    'ALTER TABLE public.foo ALTER COLUMN id SET ( n_distinct=1000, n_distinct_inherited=999 )'
    '''
    return compose(_alter_column(qualname), _column_set_attributes(attoptions))

def _column_reset_attributes(attoptions, from_attoptions):
    missing = optnames(from_attoptions) - optnames(attoptions)
    return [S('RESET ( {} ) ').format(S(', ').join([S(o) for o in missing]))] if missing else []

def alter_column_reset_attributes(qualname, attoptions, from_attoptions):
    ''' Generate ALTER TABLE ALTER COLUMN RESET.
        https://www.postgresql.org/docs/current/sql-altertable.html
    >>> as_string(alter_column_reset_attributes(('public', 'foo', 'id'), attoptions=['n_distinct=1000'],\
        from_attoptions=['n_distinct_inherited=999']))
    'ALTER TABLE public.foo ALTER COLUMN id RESET ( n_distinct_inherited )'
    '''
    return compose(_alter_column(qualname), _column_reset_attributes(attoptions, from_attoptions))

storage_types = {'p': 'PLAIN', 'e': 'EXTERNAL', 'm': 'MAIN', 'x': 'EXTENDED'}

def alter_column_set_storage(qualname, attstorage):
    ''' Generate ALTER TABLE ALTER COLUMN SET STORAGE { PLAIN | EXTERNAL | EXTENDED | MAIN }.
        https://www.postgresql.org/docs/current/sql-altertable.html
    >>> as_string(alter_column_set_storage(('public', 'foo', 'id'), 'm'))
    'ALTER TABLE public.foo ALTER COLUMN id SET STORAGE MAIN'
    >>> as_string(alter_column_set_storage(('public', 'foo', 'id'), 'p'))
    'ALTER TABLE public.foo ALTER COLUMN id SET STORAGE PLAIN'
    >>> as_string(alter_column_set_storage(('public', 'foo', 'id'), 'x'))
    'ALTER TABLE public.foo ALTER COLUMN id SET STORAGE EXTENDED'
    >>> as_string(alter_column_set_storage(('public', 'foo', 'id'), 'e'))
    'ALTER TABLE public.foo ALTER COLUMN id SET STORAGE EXTERNAL'
    '''
    return compose(_alter_column(qualname), [S('SET STORAGE'), S(storage_types[attstorage])])

def alter_column_set_compression(qualname, attcompression):
    ''' Generate ALTER TABLE ALTER COLUMN SET COMPRESSION.
        https://www.postgresql.org/docs/current/sql-altertable.html
    >>> as_string(alter_column_set_compression(('public', 'foo', 'id'), ''))
    'ALTER TABLE public.foo ALTER COLUMN id SET COMPRESSION default'
    >>> as_string(alter_column_set_compression(('public', 'foo', 'id'), 'p'))
    'ALTER TABLE public.foo ALTER COLUMN id SET COMPRESSION pglz'
    >>> as_string(alter_column_set_compression(('public', 'foo', 'id'), 'l'))
    'ALTER TABLE public.foo ALTER COLUMN id SET COMPRESSION LZ4'
    '''
    return compose(_alter_column(qualname),
        [S('SET COMPRESSION'), S({'': 'default', ' ': 'default', 'p': 'pglz', 'l': 'LZ4'}[attcompression])])

def create_composite_type(qualname):
    ''' Generate CREATE TYPE AS.
    >>> as_string(create_composite_type(('public', 'foo')))
    'CREATE TYPE public.foo AS ()'
    '''
    return compose([S('CREATE TYPE'), I(*qualname), S('AS ()')])

def alter_composite_type_add_attribute(qualname, typname, collname=None):
    ''' Generate ALTER TYPE ADD ATTRIBUTE
    >>> as_string(alter_composite_type_add_attribute(('public', 'foo', 'a'), 'int4'))
    'ALTER TYPE public.foo ADD ATTRIBUTE a int4'
    >>> as_string(alter_composite_type_add_attribute(('public', 'foo', 'b'), 'text', collname='C'))
    'ALTER TYPE public.foo ADD ATTRIBUTE b text COLLATE "C"'
    '''
    return compose([
        S('ALTER TYPE'), I(*qualname[:2]), S('ADD ATTRIBUTE'), I(qualname[2]), S(typname), *_collate(collname)])

def alter_composite_type_alter_attribute(qualname, typname, collname=None):
    ''' Generate ALTER TYPE ALTER ATTRIBUTE
    >>> as_string(alter_composite_type_alter_attribute(('public', 'foo', 'a'), 'int8'))
    'ALTER TYPE public.foo ALTER ATTRIBUTE a TYPE int8'
    >>> as_string(alter_composite_type_alter_attribute(('public', 'foo', 'b'), 'text', collname='C'))
    'ALTER TYPE public.foo ALTER ATTRIBUTE b TYPE text COLLATE "C"'
    '''
    return compose([
        S('ALTER TYPE'), I(*qualname[:2]), S('ALTER ATTRIBUTE'), I(qualname[2]), S('TYPE'), S(typname),
        *_collate(collname)])

def create_shell_type(qualname):
    '''Generate CREATE type _type_name_
    >>> as_string(create_shell_type(('public', 'foo')))
    'CREATE TYPE public.foo'
    '''
    return compose([S('CREATE TYPE'), I(*qualname[:2])])
def create_base_type_step1(qualname):
    ''' Generate CREATE TYPE.
    >>> as_string(create_base_type_step1(('public', 'foo')))
    'CREATE TYPE public.foo'
    '''
    return compose([S('CREATE TYPE'), I(*qualname[:2])])

def _base_type_opt(label, opt):
    return compose([S(label), S('='), opt]) if opt is not None else None

def _base_type_options(
    input_func=None, output_func=None, receive_func=None, send_func=None, modin_func=None,
        modout_func=None, analyze_func=None, subscript_type=None, subscript_func=None, typlen=None,
        typalign=None, typbyval=None, typstorage=None, typcategory=None, typispreferred=None, typdefault=None,
        typdelim=None, collatable=None):
    return S(', ').join(filter(None, [
        _base_type_opt('INPUT', I(*input_func)) if input_func else None,
        _base_type_opt('OUTPUT', I(*output_func)) if output_func else None,
        _base_type_opt('RECEIVE', I(*receive_func)) if receive_func else None,
        _base_type_opt('SEND', I(*send_func)) if send_func else None,
        _base_type_opt('TYPMOD_IN', I(*modin_func)) if modin_func else None,
        _base_type_opt('TYPMOD_OUT', I(*modout_func)) if modout_func else None,
        _base_type_opt('ANALYZE', I(*analyze_func)) if analyze_func else None,
        _base_type_opt('SUBSCRIPT', I(*subscript_func)) if subscript_func else None,
        _base_type_opt('INTERNALLENGTH', L(typlen)) if typlen is not None else None,
        _base_type_opt('ALIGNMENT', S({'c': 'char', 's': 'int2', 'i': 'int4', 'd': 'double'}[typalign])) if typalign else None,  # pylint: disable=line-too-long
        _base_type_opt('PASSEDBYVALUE', L(typbyval)) if typbyval is not None else None,
        _base_type_opt('STORAGE', S(storage_types[typstorage])) if typstorage else None,
        _base_type_opt('CATEGORY', L(typcategory)) if typcategory else None,
        _base_type_opt('PREFERRED', L(typispreferred)) if typispreferred is not None else None,
        _base_type_opt('DEFAULT', L(typdefault)) if typdefault is not None else None,
        _base_type_opt('ELEMENT', I(*subscript_type)) if subscript_type else None,
        _base_type_opt('DELIMITER', L(typdelim)) if typdelim is not None else None,
        _base_type_opt('COLLATABLE', L(collatable)) if collatable is not None else None,
    ]))

def create_base_type_step2(
        qualname, input_func=None, output_func=None, receive_func=None, send_func=None, modin_func=None,
        modout_func=None, analyze_func=None, subscript_type=None, subscript_func=None, typlen=None,
        typalign=None, typbyval=None, typstorage=None, typcategory=None, typispreferred=None, typdefault=None,
        typdelim=None, collatable=None):
    ''' Generate CREATE TYPE ().
    >>> as_string(create_base_type_step2(('public', 'foo'), ['public', 'input_func'], ['public', 'output_func']))
    'CREATE TYPE public.foo ( INPUT = public.input_func, OUTPUT = public.output_func )'
    >>> as_string(create_base_type_step2(('public', 'foo'), ['x', 'input_func'], ['x', 'output_func'],\
        receive_func=['x', 'receive_func'], send_func=['x', 'send_func'], modin_func=['x', 'modin_func'],\
        modout_func=['x', 'modout_func'], analyze_func=['x', 'analyze_func'], subscript_type=['x', 'subscript_type'],\
        subscript_func=['x', 'subscript_func'], typlen=8, typalign='c', typbyval=True, typstorage='m',\
        typcategory='B', typispreferred=False, typdefault='t', typdelim=';', collatable=True))
    "CREATE TYPE public.foo ( INPUT = x.input_func, OUTPUT = x.output_func, RECEIVE = x.receive_func, \
SEND = x.send_func, TYPMOD_IN = x.modin_func, TYPMOD_OUT = x.modout_func, ANALYZE = x.analyze_func, \
SUBSCRIPT = x.subscript_func, INTERNALLENGTH = 8, ALIGNMENT = char, PASSEDBYVALUE = true, STORAGE = MAIN, \
CATEGORY = 'B', PREFERRED = false, DEFAULT = 't', ELEMENT = x.subscript_type, DELIMITER = ';', COLLATABLE = true )"
    '''
    return compose([
        S('CREATE TYPE'), I(*qualname[:2]), S('('), _base_type_options(input_func=input_func,
            output_func=output_func, receive_func=receive_func, send_func=send_func, modin_func=modin_func,
            modout_func=modout_func, analyze_func=analyze_func, subscript_type=subscript_type,
            subscript_func=subscript_func, typlen=typlen, typalign=typalign, typbyval=typbyval, typstorage=typstorage,
            typcategory=typcategory, typispreferred=typispreferred, typdefault=typdefault, typdelim=typdelim,
            collatable=collatable), S(')')])

def alter_base_type(
        qualname, input_func=None, output_func=None, receive_func=None, send_func=None, modin_func=None,
        modout_func=None, analyze_func=None, subscript_type=None, subscript_func=None, typlen=None,
        typalign=None, typbyval=None, typstorage=None, typcategory=None, typispreferred=None, typdefault=None,
        typdelim=None, collatable=None):
    ''' Generate ALTER TYPE SET ().
    >>> as_string(alter_base_type(('public', 'foo'), ['public', 'input_func'], ['public', 'output_func']))
    'ALTER TYPE public.foo SET ( INPUT = public.input_func, OUTPUT = public.output_func )'
    >>> as_string(alter_base_type(('public', 'foo'), ['x', 'input_func'], ['x', 'output_func'],\
        receive_func=['x', 'receive_func'], send_func=['x', 'send_func'], modin_func=['x', 'modin_func'],\
        modout_func=['x', 'modout_func'], analyze_func=['x', 'analyze_func'], subscript_type=['x', 'subscript_type'],\
        subscript_func=['x', 'subscript_func'], typlen=8, typalign='c', typbyval=True, typstorage='m',\
        typcategory='B', typispreferred=False, typdefault='t', typdelim=';', collatable=True))
    "ALTER TYPE public.foo SET ( INPUT = x.input_func, OUTPUT = x.output_func, RECEIVE = x.receive_func, \
SEND = x.send_func, TYPMOD_IN = x.modin_func, TYPMOD_OUT = x.modout_func, ANALYZE = x.analyze_func, \
SUBSCRIPT = x.subscript_func, INTERNALLENGTH = 8, ALIGNMENT = char, PASSEDBYVALUE = true, STORAGE = MAIN, \
CATEGORY = 'B', PREFERRED = false, DEFAULT = 't', ELEMENT = x.subscript_type, DELIMITER = ';', COLLATABLE = true )"
    '''
    return compose([
        S('ALTER TYPE'), I(*qualname[:2]), S('SET ('), _base_type_options(input_func=input_func,
            output_func=output_func, receive_func=receive_func, send_func=send_func, modin_func=modin_func,
            modout_func=modout_func, analyze_func=analyze_func, subscript_type=subscript_type,
            subscript_func=subscript_func, typlen=typlen, typalign=typalign, typbyval=typbyval,
            typstorage=typstorage, typcategory=typcategory, typispreferred=typispreferred, typdefault=typdefault,
            typdelim=typdelim, collatable=collatable), S(')')])

def create_range_type(qualname, subtype, subtype_operator_class=None, collation=None,
                      canonical_function=None, subtype_diff_function=None, multirange_type_name=None):
    ''' Generate CREATE TYPE AS RANGE
    https://www.postgresql.org/docs/current/sql-createtype.html
    >>> as_string(create_range_type(('public', 'r_type'), ('public', 'sub'), collation=('nsp', 'col'),\
        subtype_diff_function=('nsp', 'sub_dif')))
    'CREATE TYPE public.r_type AS RANGE ( SUBTYPE = public.sub , COLLATION = nsp.col , SUBTYPE_DIFF = nsp.sub_dif )'
    '''
    ret = [
        S('CREATE TYPE'),
        I(*qualname[:2]),
        S('AS RANGE ('),
        S('SUBTYPE ='),
        I(*subtype),
    ]
    ret.extend([
        S(','),
        S('SUBTYPE_OPCLASS ='),
        I(*subtype_operator_class),
    ]) if subtype_operator_class else None
    ret.extend([
        S(','),
        S('COLLATION ='),
        I(*collation)
    ]) if collation else None
    ret.extend([
        S(','),
        S('CANONICAL ='),
        I(*canonical_function)
    ]) if canonical_function else None
    ret.extend([
        S(','),
        S('SUBTYPE_DIFF ='),
        I(*subtype_diff_function),
    ]) if subtype_diff_function else None
    ret.extend([
        S(','),
        S('MULTIRANGE_TYPE_NAME = '),
        I(*multirange_type_name),
    ]) if multirange_type_name else None
    ret.append(S(')'))
    return compose(ret)

def create_enum_type(qualname, labels=None):
    ''' Generate CREATE TYPE AS ENUM.
    >>> as_string(create_enum_type(('public', 'foo')))
    'CREATE TYPE public.foo AS ENUM (  )'
    >>> as_string(create_enum_type(('public', 'foo'), ('a', 'b', 'c')))
    "CREATE TYPE public.foo AS ENUM ( 'a', 'b', 'c' )"
    '''
    return compose([
        S('CREATE TYPE'), I(*qualname), S('AS ENUM ('), S(', ').join([L(label) for label in labels or []]), S(')')])

def alter_enum_type_add_value(qualname, label, relative=None, to=None):
    ''' Generate ALTER TYPE ADD VALUE.
    >>> as_string(alter_enum_type_add_value(('public', 'foo'), 'x'))
    "ALTER TYPE public.foo ADD VALUE 'x'"
    >>> as_string(alter_enum_type_add_value(('public', 'foo'), 'x', relative='AFTER', to='y'))
    "ALTER TYPE public.foo ADD VALUE 'x' AFTER 'y'"
    >>> as_string(alter_enum_type_add_value(('public', 'foo'), 'x', relative='BEFORE', to='y'))
    "ALTER TYPE public.foo ADD VALUE 'x' BEFORE 'y'"
    '''
    assert not to or relative in ('BEFORE', 'AFTER')
    return compose([
        S('ALTER TYPE'), I(*qualname), S('ADD VALUE'),
        L(label), S(relative) if relative else None, L(to) if to else None,])

def alter_type_owner_to(qualname, rolname):
    ''' Generate ALTER TYPE OWNER TO.
    >>> as_string(alter_type_owner_to(('public', 'foo'), 'root'))
    'ALTER TYPE public.foo OWNER TO root'
    '''
    return compose([S('ALTER TYPE'), I(*qualname), S('OWNER TO'), I(rolname)])

def _type_default(typdefault):
    if typdefault is None:
        return []
    return [S('DEFAULT'), S(typdefault)]

def _type_constraints(domainconstraints):
    for conname, connexp in domainconstraints or []:
        yield from [S('CONSTRAINT'), I(conname), S(connexp)]

def create_domain_type(qualname, domaintype, typnotnull=None, ddefaultexp=None, collname=None, domainconstraints=None):
    ''' Generate CREATE DOMAIN.
    >>> as_string(create_domain_type(('public', 'foo'), 'int4', typnotnull=True))
    'CREATE DOMAIN public.foo AS int4 NOT NULL'
    >>> as_string(create_domain_type(('public', 'foo'), 'text', collname='C'))
    'CREATE DOMAIN public.foo AS text COLLATE "C"'
    >>> as_string(create_domain_type(('public', 'foo'), 'text', ddefaultexp="'helo'::text"))
    "CREATE DOMAIN public.foo AS text DEFAULT 'helo'::text"
    >>> as_string(create_domain_type(('public', 'foo'), 'int4[]'))
    'CREATE DOMAIN public.foo AS int4[]'
    >>> as_string(create_domain_type(('public', 'foo'), 'int4[][]'))
    'CREATE DOMAIN public.foo AS int4[][]'
    >>> as_string(create_domain_type(('public', 'foo'), 'int4', domainconstraints=[['gt1', 'CHECK (VALUE > 1)']]))
    'CREATE DOMAIN public.foo AS int4 CONSTRAINT gt1 CHECK (VALUE > 1)'
    >>> as_string(create_domain_type(('public', 'foo'), 'int4',\
        domainconstraints=[['gt1', 'CHECK (VALUE > 1)'], ['lt9', 'CHECK (VALUE < 9)']]))
    'CREATE DOMAIN public.foo AS int4 CONSTRAINT gt1 CHECK (VALUE > 1) CONSTRAINT lt9 CHECK (VALUE < 9)'
    >>> as_string(create_domain_type(('public', 'email'), 'text', domainconstraints=[['pat',\
    """CHECK (VALUE ~ '^[a-z0-9.!#$%&''*+/=?^_`{|}~-]+@[a-z0-9](?:[a-z0-9-]{0,61}[a-z0-9])?(?:"""\
    """[a-z0-9](?:[a-z0-9-]{0,61}[a-z0-9])?)*$')"""]]))
    "CREATE DOMAIN public.email AS text CONSTRAINT pat \
CHECK (VALUE ~ '^[a-z0-9.!#$%&''*+/=?^_`{|}~-]+@[a-z0-9](?:[a-z0-9-]{0,61}[a-z0-9])?(?:\
[a-z0-9](?:[a-z0-9-]{0,61}[a-z0-9])?)*$')"
    '''
    return compose([
        S('CREATE DOMAIN'), I(*qualname), S('AS'), S(domaintype), *_collate(collname), *_type_default(ddefaultexp),
        *_type_constraints(domainconstraints), *([S('NOT NULL') if typnotnull else None])])

def alter_domain_owner_to(qualname, rolname):
    ''' Generate ALTER DOMAIN OWNER TO.
    >>> as_string(alter_domain_owner_to(('public', 'foo'), 'root'))
    'ALTER DOMAIN public.foo OWNER TO root'
    '''
    return compose([S('ALTER DOMAIN'), I(*qualname), S('OWNER TO'), I(rolname)])

def alter_domain_drop_constraint(qualname, conname):
    ''' Generate ALTER DOMAIN DROP CONSTRAINT.
    >>> as_string(alter_domain_drop_constraint(('public', 'foo'), 'gt1'))
    'ALTER DOMAIN public.foo DROP CONSTRAINT gt1'
    '''
    return compose([S('ALTER DOMAIN'), I(*qualname), S('DROP CONSTRAINT'), I(conname)])

def alter_domain_add_constraint(qualname, conname, connexp):
    ''' Generate ALTER DOMAIN ADD CONSTRAINT.
    >>> as_string(alter_domain_add_constraint(('public', 'foo'), conname='gt1', connexp='CHECK (VALUE > 1)'))
    'ALTER DOMAIN public.foo ADD CONSTRAINT gt1 CHECK (VALUE > 1)'
    >>> as_string(alter_domain_add_constraint(('public', 'foo'), conname='gt1', connexp='CHECK (VALUE > 1) NOT VALID'))
    'ALTER DOMAIN public.foo ADD CONSTRAINT gt1 CHECK (VALUE > 1) NOT VALID'
    '''
    return compose([S('ALTER DOMAIN'), I(*qualname), S('ADD CONSTRAINT'), I(conname), S(connexp)])

def alter_domain_set_default(qualname, ddefaultexp):
    ''' Generate ALTER DOMAIN {SET | DROP} DEFAULT.
    >>> as_string(alter_domain_set_default(('public', 'foo'), ddefaultexp=None))
    'ALTER DOMAIN public.foo DROP DEFAULT'
    >>> as_string(alter_domain_set_default(('public', 'foo'), ddefaultexp='123'))
    'ALTER DOMAIN public.foo SET DEFAULT 123'
    '''
    return compose([S('ALTER DOMAIN'), I(*qualname)] + (
        [S('DROP DEFAULT')] if ddefaultexp is None else [S('SET DEFAULT'), S(ddefaultexp)]))

def alter_domain_set_notnull(qualname, typnotnull):
    ''' Generate ALTER DOMAIN {SET | DROP} DEFAULT.
    >>> as_string(alter_domain_set_notnull(('public', 'foo'), typnotnull=True))
    'ALTER DOMAIN public.foo SET NOT NULL'
    >>> as_string(alter_domain_set_notnull(('public', 'foo'), typnotnull=False))
    'ALTER DOMAIN public.foo DROP NOT NULL'
    '''
    return compose([S('ALTER DOMAIN'), I(*qualname), S('SET' if typnotnull else 'DROP'), S('NOT NULL')])

def create_function(qualname, functiondef): # noqa: ARG001
    ''' Generate CREATE FUNCTION.
    >>> as_string(create_function(('public', 'foo', 'ts timestamp without time zone'),\
functiondef='CREATE OR REPLACE FUNCTION public.timestamp_to_unixtime(ts timestamp without time zone)\
RETURNS unixtime LANGUAGE sql IMMUTABLE STRICT AS $function$ SELECT EXTRACT (EPOCH FROM ts)::unixtime; $function$'))
    'CREATE OR REPLACE FUNCTION public.timestamp_to_unixtime(ts timestamp without time zone)\
RETURNS unixtime LANGUAGE sql IMMUTABLE STRICT AS $function$ SELECT EXTRACT (EPOCH FROM ts)::unixtime; $function$'
    '''
    return S(functiondef)

def create_multiple_functions(functiondefs):
    ''' Generate multiple CREATE FUNCTION statements from a list of function definitions
    >>> as_string(create_multiple_functions(['CREATE FUNCTION get_constant() RETURNS INTEGER AS $$ SELECT 42; $$ LANGUAGE SQL',\
'CREATE FUNCTION add_two_integers(a INT, b INT) RETURNS INT AS $$ SELECT a + b; $$ LANGUAGE SQL',\
'CREATE FUNCTION get_current_date() RETURNS DATE AS $$ SELECT CURRENT_DATE; $$ LANGUAGE SQL']))
    'CREATE FUNCTION get_constant() RETURNS INTEGER AS $$ SELECT 42; $$ LANGUAGE SQL ; \
CREATE FUNCTION add_two_integers(a INT, b INT) RETURNS INT AS $$ SELECT a + b; $$ LANGUAGE SQL ; \
CREATE FUNCTION get_current_date() RETURNS DATE AS $$ SELECT CURRENT_DATE; $$ LANGUAGE SQL ;'
    '''
    ret = []
    for funcdef in functiondefs:
        ret.extend([S(funcdef), S(';')])
    return compose(ret)

def _function_sig(qualname):
    return [I(*qualname[:2]), S('('), S(qualname[2]), S(')')]

def _prokind_label(prokind):
    return {'f': 'FUNCTION', 'p': 'PROCEDURE'}[prokind]

def comment_function(qualname, prokind, objcomment):
    '''Generate COMMENT ON FUNCTION.
    >>> as_string(comment_function(('public', 'foofunc', 'r1 timestamp, r2 text, r3 integer, VARIADIC text[]'),\
        'f', 'hi'))
    "COMMENT ON FUNCTION public.foofunc ( r1 timestamp, r2 text, r3 integer, VARIADIC text[] ) IS 'hi'"
    >>> as_string(comment_function(('public', 'foofunc', 'r1 timestamp'), 'p', 'hi'))
    "COMMENT ON PROCEDURE public.foofunc ( r1 timestamp ) IS 'hi'"
    '''
    return compose([
        S('COMMENT ON'), S(_prokind_label(prokind)), *_function_sig(qualname), S('IS'), L(objcomment)])

def alter_function_owner_to(qualname, prokind, rolname):
    ''' Generate ALTER FUNCTION OWNER TO.
    >>> as_string(alter_function_owner_to(('public', 'foofunc', 'r1 timestamp'), 'f', 'root'))
    'ALTER FUNCTION public.foofunc ( r1 timestamp ) OWNER TO root'
    >>> as_string(alter_function_owner_to(('public', 'foofunc', 'r1 timestamp'), 'p', 'root'))
    'ALTER PROCEDURE public.foofunc ( r1 timestamp ) OWNER TO root'
    '''
    return compose([
        S('ALTER'), S(_prokind_label(prokind)), *_function_sig(qualname), S('OWNER TO'), I(rolname)])

def alter_function_ext(qualname, prokind, extname, yes=True):
    ''' Generate ALTER FUNCTION  DEPENDS ON EXTENSION
    >>> as_string(alter_function_ext(('public', 'foofunc', 'r1 timestamp'), 'f', 'func_ext'))
    'ALTER FUNCTION public.foofunc ( r1 timestamp ) DEPENDS ON EXTENSION func_ext'
    '''
    return compose([
        S('ALTER'), S(_prokind_label(prokind)), *_function_sig(qualname),
        S('NO') if not yes else None, S('DEPENDS ON EXTENSION'), S(extname)])

def _create_index_change_name(indexdef, oldname, newname):
    '''
    >>> _create_index_change_name('CREATE INDEX cc_tld_idx ON public.cc USING btree (tld);', 'cc_tld_idx', 'idx123')
    'CREATE INDEX idx123 ON public.cc USING btree (tld);'
    >>> _create_index_change_name(\
        'create UNIQUE INDEX cc_tld_idx ON public.cc USING btree (tld);', 'cc_tld_idx', 'idx123')
    'create UNIQUE INDEX idx123 ON public.cc USING btree (tld);'
    >>> _create_index_change_name(\
        'Create INDEX IF not EXISTs cc_tld_idx ON public.cc USING btree (tld);', 'cc_tld_idx', 'idx123')
    'Create INDEX IF not EXISTs idx123 ON public.cc USING btree (tld);'
    >>> _create_index_change_name(\
        'Create INDEX ConccurrENTLY "cc_tld_idx" ON public.cc USING btree (tld);', 'cc_tld_idx', 'idx123')
    'Create INDEX ConccurrENTLY "idx123" ON public.cc USING btree (tld);'
    >>> _create_index_change_name('CREATE INDEX "a on b" ON cc USING btree (tld);', 'a on b', 'idx123')
    'CREATE INDEX "idx123" ON cc USING btree (tld);'
    >>> _create_index_change_name('CREATE INDEX "INDEX" ON cc USING btree (tld);', 'INDEX', 'idx123')
    'CREATE INDEX "idx123" ON cc USING btree (tld);'
    >>> _create_index_change_name('CREATE INDEX create ON cc USING btree (tld);', 'create', 'idx123')
    'CREATE INDEX idx123 ON cc USING btree (tld);'
    '''
    if f'"{oldname}"' in indexdef:
        return re.sub(f'"{oldname}"', f'"{newname}"', indexdef, count=1)
    return re.sub(f'({oldname})\\s+ON\\s+', f'{quote_identifier(newname)} ON ', indexdef, count=1, flags=re.IGNORECASE)

def _create_index_add_concurrently(indexdef):
    '''
    >>> _create_index_add_concurrently('CREATE INDEX myidx ON public.cc USING btree (tld);')
    'CREATE INDEX CONCURRENTLY myidx ON public.cc USING btree (tld);'
    >>> _create_index_add_concurrently('CREATE INDEX CONCURRENTLY myidx ON public.cc USING btree (tld);')
    'CREATE INDEX CONCURRENTLY myidx ON public.cc USING btree (tld);'
    >>> _create_index_add_concurrently('CREATE UNIQUE INDEX CONCURRENTLY myidx ON public.cc USING btree (tld);')
    'CREATE UNIQUE INDEX CONCURRENTLY myidx ON public.cc USING btree (tld);'
    '''
    if re.match(r'\s*CREATE\s+(UNIQUE\s+)?INDEX\s+CONCURRENTLY\s+', indexdef, flags=re.IGNORECASE):
        return indexdef
    return re.sub(r'\s+INDEX\s+', ' INDEX CONCURRENTLY ', indexdef, count=1, flags=re.IGNORECASE)

def _create_index_remove_concurrently(indexdef):
    '''
    >>> _create_index_remove_concurrently('CREATE INDEX myidx ON public.cc USING btree (tld);')
    'CREATE INDEX myidx ON public.cc USING btree (tld);'
    >>> _create_index_remove_concurrently('CREATE INDEX CONCURRENTLY myidx ON public.cc USING btree (tld);')
    'CREATE INDEX myidx ON public.cc USING btree (tld);'
    >>> _create_index_remove_concurrently('CREATE UNIQUE INDEX CONCURRENTLY myidx ON public.cc USING btree (tld);')
    'CREATE UNIQUE INDEX myidx ON public.cc USING btree (tld);'
    '''
    if not re.match(r'\s*CREATE\s+(UNIQUE\s+)?INDEX\s+CONCURRENTLY\s+', indexdef, flags=re.IGNORECASE):
        return indexdef
    return re.sub(r'\s+INDEX\s+CONCURRENTLY\s+', ' INDEX ', indexdef, count=1, flags=re.IGNORECASE)

def create_index(qualname, indexdef, rename=None, concurrently=None):
    r''' Generate CREATE INDEX.
    >>> as_string(create_index(('public', 'foo'), indexdef='CREATE INDEX cc_tld_idx ON public.cc USING btree (tld);'))
    'CREATE INDEX cc_tld_idx ON public.cc USING btree (tld);'
    '''
    if rename:
        indexdef = _create_index_change_name(indexdef, qualname[-1], rename)
    if concurrently is True:
        indexdef = _create_index_add_concurrently(indexdef)
    if concurrently is False:
        indexdef = _create_index_remove_concurrently(indexdef)
    return S(indexdef)

def alter_index_ext(qualname, extname, yes=True):
    ''' Generate ALTER INDEX  DEPENDS ON EXTENSION
    >>> as_string(alter_index_ext(('public', 'ind'), 'ind_ext', yes=False))
    'ALTER INDEX public.ind NO DEPENDS ON EXTENSION ind_ext'
    '''
    return compose(
        [S('ALTER INDEX'), I(*qualname), S('NO') if not yes else None, S('DEPENDS ON EXTENSION'), S(extname)])

def alter_index_tablespace(qualname, spcname):
    ''' Generate ALTER INDEX SET TABLESPACE.
        https://www.postgresql.org/docs/current/sql-altertable.html
    >>> as_string(alter_index_tablespace(('public', 'fooidx'), 'yourspace'))
    'ALTER INDEX public.fooidx SET TABLESPACE yourspace'
    '''
    return compose([S('ALTER INDEX'), I(*qualname), S('SET TABLESPACE'), I(spcname)])

def alter_index_set_reloptions(qualname, reloptions):
    ''' Generate ALTER INDEX SET.
        https://www.postgresql.org/docs/current/sql-altertable.html
    >>> as_string(alter_index_set_reloptions(('public', 'fooidx'),\
        reloptions=['fillfactor=51', 'toast_tuple_target=2134']))
    'ALTER INDEX public.fooidx SET ( fillfactor=51, toast_tuple_target=2134 )'
    '''
    return compose([S('ALTER INDEX'), I(*qualname), *_table_reloptions(reloptions, 'SET')])

def alter_index_reset_attributes(qualname, reloptions, from_reloptions):
    ''' Generate ALTER INDEX RESET.
        https://www.postgresql.org/docs/current/sql-altertable.html
    >>> as_string(alter_index_reset_attributes(('public', 'fooidx'), reloptions=['fillfactor=51'],\
        from_reloptions=['toast_tuple_target=2134']))
    'ALTER INDEX public.fooidx RESET ( toast_tuple_target )'
    '''
    return compose([S('ALTER INDEX'), I(*qualname), *_table_reset_attributes(reloptions, from_reloptions)])

def alter_index_set_statistics(qualname, atnum, stattarget):
    ''' Generate ALTER INDEX RESET.
        https://www.postgresql.org/docs/current/sql-altertable.html
    >>> as_string(alter_index_set_statistics(('public', 'fooidx'), 1, 123))
    'ALTER INDEX public.fooidx ALTER COLUMN 1 SET STATISTICS 123'
    '''
    return compose([S('ALTER INDEX'), I(*qualname), S('ALTER COLUMN'), L(atnum), S('SET STATISTICS'), L(stattarget)])

def drop_index(qualname):
    ''' Cenerate DROP INDEX
    >>> as_string(drop_index(('public', 'fooidx')))
    'DROP INDEX IF EXISTS public.fooidx'
    '''
    return compose([S('DROP INDEX IF EXISTS'), I(*qualname)])

def rename_index(qualname, newname):
    ''' Cenerate ALTER INDEX name RENAME TO new_name.
    >>> as_string(rename_index(('public', 'fooidx'), 'baridx'))
    'ALTER INDEX public.fooidx RENAME TO baridx'
    '''
    return compose([S('ALTER INDEX'), I(*qualname), S('RENAME TO'), I(newname)])

def _compose_create_view(qualname, temp, recursive, query, reloptions=None):
    return compose([
        S('CREATE OR REPLACE'), S('TEMP') if temp else None, S('RECURSIVE') if recursive else None,
        S('VIEW'), I(*qualname),
        *_table_reloptions(reloptions, 'WITH'), S('AS'), S(query)])

def create_view(qualname, temp, recursive, query, reloptions=None):
    ''' Generate CREATE VIEW
        https://www.postgresql.org/docs/current/sql-createview.html
    >>> as_string(create_view(('v1r1', 'foo_view'), False, False, 'SELECT 1;'))
    'CREATE OR REPLACE VIEW v1r1.foo_view AS SELECT 1;'
    >>> as_string(create_view(('v1r1', 'foo_view'), True, True, 'SELECT 1;',\
        ['check_option=cascaded', 'security_barrier=true', 'security_invoker=true']))
    'CREATE OR REPLACE TEMP RECURSIVE VIEW v1r1.foo_view WITH \
( check_option=cascaded, security_barrier=true, security_invoker=true ) AS SELECT 1;'
    '''
    return _compose_create_view(qualname, temp, recursive, query, reloptions)

def drop_view(qualname, cascade=False):
    ''' Cenerate DROP VIEW
        https://www.postgresql.org/docs/current/sql-dropview.html
        NOTE: used *only* when we want to replace views
    >>> as_string(drop_view(('public', 'foo_view'), cascade=True))
    'DROP VIEW IF EXISTS public.foo_view CASCADE'
    '''
    return compose([S('DROP VIEW IF EXISTS'), I(*qualname), S('CASCADE') if cascade else S('RESTRICT')])

def alter_view_column(qualname, columnname, set_default, expr=None):
    ''' Generate ALTER VIEW column default
        https://www.postgresql.org/docs/current/sql-alterview.html
    >>> as_string(alter_view_column(('v1r1', 'foo_view'), 'id', True, '1+1'))
    'ALTER VIEW v1r1.foo_view ALTER id SET DEFAULT 1+1'
    >>> as_string(alter_view_column(('v1r1', 'foo_view'), 'id', False))
    'ALTER VIEW v1r1.foo_view ALTER id DROP DEFAULT'
    '''
    return compose([
        S('ALTER VIEW'), I(*qualname), S('ALTER'), I(columnname),
        S('SET DEFAULT') if set_default else (S('DROP DEFAULT')), S(expr) if expr else None])

def alter_view_owner(qualname, owner):
    ''' Generate ALTER VIEW OWNER TO
        https://www.postgresql.org/docs/current/sql-alterview.html
    >>> as_string(alter_view_owner(('v1r1', 'foo_view'), 'new_role'))
    'ALTER VIEW v1r1.foo_view OWNER TO new_role'
    '''
    return compose([S('ALTER VIEW'), I(*qualname), S('OWNER TO'), I(owner)])

def alter_view_set_reloptions(qualname, reloptions):
    ''' Generate ALTER VIEW SET/RESET view_option_name
        https://www.postgresql.org/docs/current/sql-alterview.html
    >>> as_string(alter_view_set_reloptions(('public', 'foo'), ['check_option=cascaded', 'security_barrier=true']))
    'ALTER VIEW public.foo SET ( check_option=cascaded, security_barrier=true )'
    '''
    return compose([S('ALTER VIEW'), I(*qualname)], _table_reloptions(reloptions, 'SET'))

def alter_view_reset_attributes(qualname, reloptions, from_reloptions):
    ''' Generate ALTER VIEW RESET.
        https://www.postgresql.org/docs/current/sql-alterview.html
    >>> as_string(alter_view_reset_attributes(('public', 'foo'), reloptions=[], from_reloptions=['check_option=local']))
    'ALTER VIEW public.foo RESET ( check_option )'
    '''
    return compose([S('ALTER VIEW'), I(*qualname)], _table_reset_attributes(reloptions, from_reloptions))

def _mat_view_columns(viewcolumns):
    return [S('('), *S(',').join([S(c['attname']) for c in viewcolumns]), S(')')]

def create_mat_view(qualname, query, viewcolumns, reloptions, method, spcname, relispopulated):
    ''' Generate CREATE MATERIALIZED VIEW
    >>> as_string(create_mat_view(('public', 'mat'), 'SELECT 1',\
        [{'attname': 'cola'}, {'attname':'colb'}],\
        ['check_option=cascaded', 'security_barrier=true', 'security_invoker=true'], 'gist', None, False))
    'CREATE MATERIALIZED VIEW IF NOT EXISTS public.mat ( cola , colb ) USING gist WITH\
 ( check_option=cascaded, security_barrier=true, security_invoker=true ) AS SELECT 1 WITH NO DATA'
    '''
    ret = [S('CREATE MATERIALIZED VIEW IF NOT EXISTS'), I(*qualname)]
    ret.extend(_mat_view_columns(viewcolumns)) if viewcolumns else None
    ret.extend([S('USING'), S(method)]) if method else None
    ret.extend(_table_reloptions(reloptions, 'WITH')) if reloptions else None
    ret.extend([S('TABLESPACE'), S(spcname)]) if spcname else None
    query = query[:-1] if query[-1] == ';' else query
    ret.extend([S('AS'), S(query)])
    ret.extend([S('WITH'), S('NO') if not relispopulated else None, S('DATA')])
    return compose(ret)

def alter_mat_view_ext(qualname, depends, extension_name):
    ''' Generate ALTER MATERIALIZED VIEW [NO] DEPENDS ON EXTENSION
    >>> as_string(alter_mat_view_ext(('public', 'mat'), True, 'test_ext'))
    'ALTER MATERIALIZED VIEW public.mat DEPENDS ON EXTENSION test_ext'
    >>> as_string(alter_mat_view_ext(('public', 'mat'), False, 'test_ext'))
    'ALTER MATERIALIZED VIEW public.mat NO DEPENDS ON EXTENSION test_ext'
    '''
    return compose([
        S('ALTER MATERIALIZED VIEW'), I(*qualname),
        S('NO') if not depends else None,
        S('DEPENDS ON EXTENSION'), I(extension_name)])

def alter_mat_view_tablespace(qualname, new_tablespace):
    ''' Generate ALTER MATERIALIZED VIEW ALL IN TABLESPACE
    >>> as_string(alter_mat_view_tablespace(('public', 'mat'), 'new_space'))
    'ALTER MATERIALIZED VIEW ALL IN TABLESPACE public.mat SET TABLESPACE new_space'
    '''
    return compose([
        S('ALTER MATERIALIZED VIEW ALL IN TABLESPACE'), I(*qualname), S('SET TABLESPACE'), S(new_tablespace)])

def alter_mat_view_owner(qualname, rolname):
    ''' Generate ALTER MATERIALIZED VIEW OWNER TO
    >>> as_string(alter_mat_view_owner(('public', 'mat'), 'myrole'))
    'ALTER MATERIALIZED VIEW public.mat OWNER TO myrole'
    '''
    return compose([S('ALTER MATERIALIZED VIEW'), I(*qualname), S('OWNER TO'), I(rolname)])

def alter_mat_view_am(qualname, new_access_method):
    ''' Generate ALTER VIEW ACCESS METHOD
    >>> as_string(alter_mat_view_am(('public', 'mat'), 'gist'))
    'ALTER MATERIALIZED VIEW public.mat SET ACCESS METHOD gist'
    '''
    return compose([S('ALTER MATERIALIZED VIEW'), I(*qualname), S('SET ACCESS METHOD'), S(new_access_method)])

def alter_mat_view_col_params(qualname, colname, do_set, opts):
    ''' Generate ALTER MATERIALIZED VIEW SET/RESET
    >>> as_string(alter_mat_view_col_params(('public', 'mat'), 'col1', False,\
        ['check_option=cascaded', 'security_barrier=true', 'security_invoker=true']))
    'ALTER MATERIALIZED VIEW public.mat ALTER COLUMN col1 RESET ( check_option, security_barrier, security_invoker )'
    >>> as_string(alter_mat_view_col_params(('public', 'mat'), 'col1', True,\
        ['check_option=cascaded', 'security_barrier=true', 'security_invoker=true']))
    'ALTER MATERIALIZED VIEW public.mat ALTER COLUMN col1 SET \
( check_option=cascaded, security_barrier=true, security_invoker=true )'
    '''
    ret = [S('ALTER MATERIALIZED VIEW'), I(*qualname), S('ALTER COLUMN'), S(colname)]
    if do_set:
        ret.extend(_table_reloptions(opts, 'SET'))
    else:
        attribute_options = [x.split('=')[0] for x in opts or []]
        ret.append(S('RESET'))
        ret.extend([S('( {} )').format(S(', ').join([S(o) for o in attribute_options]))])
    return compose(ret)

def alter_mat_view_params(qualname, do_set, opts):
    ''' Generate ALTER MATERIALIZED VIEW SET/RESET
    >>> as_string(alter_mat_view_params(('public', 'mat'), False,\
        ['check_option', 'security_barrier', 'security_invoker']))
    'ALTER MATERIALIZED VIEW public.mat RESET ( check_option, security_barrier, security_invoker )'
    >>> as_string(alter_mat_view_params(('public', 'mat'), True,\
        ['check_option=cascaded', 'security_barrier=true', 'security_invoker=true']))
    'ALTER MATERIALIZED VIEW public.mat SET ( check_option=cascaded, security_barrier=true, security_invoker=true )'
    '''
    ret = [S('ALTER MATERIALIZED VIEW'), I(*qualname)]
    if do_set:
        ret.extend(_table_reloptions(opts, 'SET'))
    else:
        ret.append(S('RESET'))
        ret.extend([S('( {} )').format(S(', ').join([S(o) for o in opts]))])
    return compose(ret)

def alter_mat_view_col_set_storage(qualname, colname, attstorage):
    ''' Generate ALTER MATERIALIZED VIEW ALTER COLUMN SET STORAGE { PLAIN | EXTERNAL | EXTENDED | MAIN }.
    >>> as_string(alter_mat_view_col_set_storage(('public', 'mat'), 'id', 'm'))
    'ALTER MATERIALIZED VIEW public.mat ALTER COLUMN id SET STORAGE MAIN'
    >>> as_string(alter_mat_view_col_set_storage(('public', 'mat'), 'id', 'p'))
    'ALTER MATERIALIZED VIEW public.mat ALTER COLUMN id SET STORAGE PLAIN'
    >>> as_string(alter_mat_view_col_set_storage(('public', 'mat'), 'id', 'x'))
    'ALTER MATERIALIZED VIEW public.mat ALTER COLUMN id SET STORAGE EXTENDED'
    >>> as_string(alter_mat_view_col_set_storage(('public', 'mat'), 'id', 'e'))
    'ALTER MATERIALIZED VIEW public.mat ALTER COLUMN id SET STORAGE EXTERNAL'
    '''
    return compose([
        S('ALTER MATERIALIZED VIEW'), I(*qualname), S('ALTER COLUMN'), S(colname),
        S('SET STORAGE'), S(storage_types[attstorage])])

def alter_mat_view_col_set_compression(qualname, colname, attcompression):
    ''' Generate ALTER MATERIALIZED VIEW ALTER COLUMN SET COMPRESSION.
    >>> as_string(alter_mat_view_col_set_compression(('public', 'mat'), 'id', ''))
    'ALTER MATERIALIZED VIEW public.mat ALTER COLUMN id SET COMPRESSION default'
    >>> as_string(alter_mat_view_col_set_compression(('public', 'mat'), 'id', 'p'))
    'ALTER MATERIALIZED VIEW public.mat ALTER COLUMN id SET COMPRESSION pglz'
    >>> as_string(alter_mat_view_col_set_compression(('public', 'mat'), 'id', 'l'))
    'ALTER MATERIALIZED VIEW public.mat ALTER COLUMN id SET COMPRESSION LZ4'
    '''
    return compose([
        S('ALTER MATERIALIZED VIEW'), I(*qualname), S('ALTER COLUMN'), S(colname),
        S('SET COMPRESSION'), S({'': 'default', ' ': 'default', 'p': 'pglz', 'l': 'LZ4'}[attcompression])])

def alter_mat_view_col_set_stats(qualname, colname, attstattarget):
    ''' Generate ALTER MATERIALIZED VIEW ALTER COLUMN SET STATISTICS.
    >>> as_string(alter_mat_view_col_set_stats(('public', 'mat'), 'id', attstattarget=42))
    'ALTER MATERIALIZED VIEW public.mat ALTER COLUMN id SET STATISTICS 42'
    '''
    return compose([
        S('ALTER MATERIALIZED VIEW'), I(*qualname),
        S('ALTER COLUMN'), S(colname), S('SET STATISTICS'), L(attstattarget)])

def alter_mat_view_cluster_on(qualname, index_name):
    ''' Generate ALTER MATERIALIZED VIEW CLUSTER ON index_name
    https://www.postgresql.org/docs/current/sql-altermaterializedview.html
    >>> as_string(alter_mat_view_cluster_on(('public', 'mat'),'idx'))
    'ALTER MATERIALIZED VIEW public.mat CLUSTER ON idx'
    '''
    return compose([S('ALTER MATERIALIZED VIEW'), I(*qualname), S('CLUSTER ON'), I(index_name)])

def alter_mat_view_no_cluster(qualname):
    ''' Generate ALTER MATERIALIZED VIEW CLUSTER ON index_name
    https://www.postgresql.org/docs/current/sql-altermaterializedview.html
    >>> as_string(alter_mat_view_no_cluster(('public', 'mat')))
    'ALTER MATERIALIZED VIEW public.mat SET WITHOUT CLUSTER'
    '''
    return compose([S('ALTER MATERIALIZED VIEW'), I(*qualname), S('SET WITHOUT CLUSTER')])

def drop_mat_view(qualname, cascade=False):
    ''' Cenerate DROP VIEW
        https://www.postgresql.org/docs/current/sql-dropmaterializedview.html
        NOTE: used *only* when we want to replace materialized views
    >>> as_string(drop_mat_view(('public', 'mat'), cascade=True))
    'DROP MATERIALIZED VIEW IF EXISTS public.mat CASCADE'
    '''
    return compose([S('DROP MATERIALIZED VIEW IF EXISTS'), I(*qualname), S('CASCADE') if cascade else S('RESTRICT')])

def create_trigger(qualname, triggerdef): # noqa: ARG001
    '''Cenerate CREATE TRIGGER
    >>> as_string(create_trigger(('public', 'foo', 'tgname'),\
        triggerdef='CREATE TRIGGER trg_update_cc AFTER INSERT ON bb FOR EACH ROW EXECUTE FUNCTION public.update_cc();'))
    'CREATE TRIGGER trg_update_cc AFTER INSERT ON bb FOR EACH ROW EXECUTE FUNCTION public.update_cc();'
    '''
    return S(triggerdef)

def drop_trigger(qualname):
    '''Generate DROP TRIGGER
    NOTE: Only use this to replace triggers
    >>> as_string(drop_trigger(('public', 'bar', 'foo_trg')))
    'DROP TRIGGER foo_trg ON public.bar'
    '''
    return compose([S('DROP TRIGGER'), I(qualname[2]), S('ON'), I(*qualname[:2])])

def alter_trigger(qualname, depends, extension_name):
    '''Generate ALTER TRIGGER [NO] DEPENDS ON EXTENSION
    >>> as_string(alter_trigger(('public', 'bb', 'trg_update_foo'), True, 'test_ext')) # pylint: disable=line-too-long
    'ALTER TRIGGER trg_update_foo ON public.bb DEPENDS ON EXTENSION test_ext'
    >>> as_string(alter_trigger(('public', 'bb', 'trg_update_foo'), False, 'test_ext')) # pylint: disable=line-too-long
    'ALTER TRIGGER trg_update_foo ON public.bb NO DEPENDS ON EXTENSION test_ext'
    '''
    return compose([
        S('ALTER TRIGGER'), I(qualname[2]), S('ON'), I(*qualname[:2]), S('NO') if not depends else None,
        S('DEPENDS ON EXTENSION'), I(extension_name)])

def _compose_role_opts(opts):
    ret = []
    if opts:
        for k,v in opts.items():
            if isinstance(v, bool):
                ret.append(S(k) if v else S({
                    'SUPERUSER': 'NOSUPERUSER',
                    'CREATEDB': 'NOCREATEDB',
                    'CREATEROLE': 'NOCREATEROLE',
                    'INHERIT': 'NOINHERIT',
                    'LOGIN': 'NOLOGIN',
                    'REPLICATION': 'NOREPLICATION',
                    'BYPASSRLS': 'NOBYPASSRLS',
                }[k]))
            elif k in ('CONNECTION LIMIT', 'VALID UNTIL'):
                ret.append(S(k)) if v else None
                ret.append(L(v)) if v else None
            elif k == 'PASSWORD':
                ret.append(S(k))
                ret.append(L(v) if v else S('NULL'))
    return ret

def create_role(qualname, opts=None):
    ''' Generate CREATE ROLE
        :param qualname: name of the role/user/group to be created
        A role subsumes the concepts of “users” and “groups”.
        A user is essentially just a role with the rolcanlogin flag set.
        Any role can have other roles as members
    >>> as_string(create_role('me'))
    'CREATE ROLE me'
    >>> as_string(create_role('me', {'SUPERUSER': True, 'CREATEDB': False}))
    'CREATE ROLE me SUPERUSER NOCREATEDB'
    '''
    return compose([S('CREATE ROLE'), I(qualname), *_compose_role_opts(opts)])

def alter_role(qualname, opts):
    ''' Generate ALTER ROLE
    >>> as_string(alter_role('me', {'SUPERUSER': True}))
    'ALTER ROLE me WITH SUPERUSER'
    >>> as_string(alter_role('me', {'CONNECTION LIMIT': 5}))
    'ALTER ROLE me WITH CONNECTION LIMIT 5'
    >>> from datetime import date
    >>> as_string(alter_role('me', {'VALID UNTIL': date(2023, 1, 1)}))
    "ALTER ROLE me WITH VALID UNTIL '2023-01-01'::date"
    >>> as_string(alter_role('me', { 'SUPERUSER': True, 'CONNECTION LIMIT': 5, 'VALID UNTIL': date(2023, 1, 1)}))
    "ALTER ROLE me WITH SUPERUSER CONNECTION LIMIT 5 VALID UNTIL '2023-01-01'::date"
    '''
    return compose([S('ALTER ROLE'), I(qualname), S('WITH'), *_compose_role_opts(opts)])

def alter_role_default_config(qualname, param, value=None, db=None):
    ''' Generate ALTER ROLE SET/RESET configuration_parameter
    >>> as_string(alter_role_default_config('me', 'datestyle', 'postgres, dmy'))
    'ALTER ROLE me SET datestyle TO postgres, dmy'
    >>> as_string(alter_role_default_config('me', 'search_path'))
    'ALTER ROLE me RESET search_path'
    '''
    return compose([
        S('ALTER ROLE'), I(qualname),
        S('IN DATABASE') if db else None, I(db) if db else None,
        S('SET') if value else S('RESET'), S(param),
        S('TO') if value else None, S(value) if value else None])

def grant_role_membership(group, member):
    ''' Generate GRANT group TO rolename
    >>> as_string(grant_role_membership('admin', 'me'))
    'GRANT admin TO me'
    '''
    return compose([S('GRANT'), I(group), S('TO'), I(member)])

def revoke_role_membership(group, member):
    ''' Generate GRANT group TO rolename
    >>> as_string(revoke_role_membership('admin', 'me'))
    'REVOKE admin FROM me'
    '''
    return compose([S('REVOKE'), I(group), S('FROM'), I(member)])

def comment_trigger(qualname, objcomment):
    '''Generate COMMENT ON TRIGGER.
    >>> as_string(comment_trigger(('public', 'bb', 'checker'), 'hi'))
    "COMMENT ON TRIGGER checker ON public.bb IS 'hi'"
    '''
    return compose([
        S('COMMENT ON TRIGGER'), I(qualname[2]), S('ON'), I(*qualname[:2]), S('IS'), L(objcomment)])

def create_extension(qualname, schema, version, cascade):
    r''' Generate CREATE EXTENSION.
    >>> as_string(create_extension('ext', 'my_schema', 2.5, True))
    'CREATE EXTENSION ext SCHEMA my_schema VERSION 2.5 CASCADE'
    '''
    return compose([
        S('CREATE EXTENSION'),
        I(qualname),
        S('SCHEMA') if schema else None,
        I(schema) if schema else None,
        S('VERSION') if version else None,
        L(version) if version else None,
        S('CASCADE') if cascade else None])

def alter_extension_update(qualname, version):
    r''' Generate ALTER EXTENSION UPDATE TO.
    >>> as_string(alter_extension_update('ext', 2.5))
    'ALTER EXTENSION ext UPDATE TO 2.5'
    '''
    return compose([S('ALTER EXTENSION'), I(qualname), S('UPDATE TO'), L(version)])

def alter_extension_schema(qualname, schema):
    r''' Generate ALTER EXTENSION SET SCHEMA.
    >>> as_string(alter_extension_schema('ext', 'public'))
    'ALTER EXTENSION ext SET SCHEMA public'
    '''
    return compose([S('ALTER EXTENSION'), I(qualname), S('SET SCHEMA'), I(schema)])

def construct_object_update(ty, *v):
    if ty in ('FUNCTION', 'AGGREGATE', 'PROCEDURE'):
        return [S(ty), I(*v[0]), S('('), S(v[1]), S(')')]
    if ty in ('TABLE', 'SEQUENCE', 'VIEW', 'MATERIALIZED VIEW', 'FOREIGN TABLE'):
        return [S(ty), I(*(v[0]))]
    if ty in ('COLLATION', 'CONVERSION', 'TYPE', 'DOMAIN'):
        return [S(ty), I(*(v[0]))]
    if ty == 'EVENT TRIGGER':
        return [S(ty), I(v[0])]
    if ty == 'OPERATOR':
        return [S(ty), I_op(v[0][0], v[0][1]), S('('), I(*v[1]), S(','), I(*v[2]), S(')')]
    if ty in ('TEXT SEARCH CONFIGURATION', 'TEXT SEARCH DICTIONARY', 'TEXT SEARCH PARSER', 'TEXT SEARCH TEMPLATE'):
        return [S(ty),  I(*(v[0]))]
    if ty in('OPERATOR CLASS', 'OPERATOR FAMILY'):
        return [S(ty),  I(*(v[0])), S('USING'),  S(v[1])]
    if ty in ('FOREIGN DATA WRAPPER', 'LANGUAGE', 'SCHEMA', 'ACCESS METHOD', 'SERVER'):
        return [S(ty), I(v[0])]
    if ty == 'CAST':
        return [S('CAST'), S('('), I(*v[0]), S('AS'), I(*v[1]), S(')')]
    if ty == 'TRANSFORM':
        return [S('TRANSFORM FOR'), S(v[0]), S('LANGUAGE'), S(v[1])]
    return []

def alter_extension_add_object(qualname, ty, *val):
    r''' Generate ALTER EXTENSION ADD OBJECT.
    >>> as_string(alter_extension_add_object('ext', 'TABLE', ('public', 'table_test')))
    'ALTER EXTENSION ext ADD TABLE public.table_test'
    >>> as_string(alter_extension_add_object('ext', 'OPERATOR', ['a', 'op_name'], ['a', 'integer'], ['a', 'numeric']))
    'ALTER EXTENSION ext ADD OPERATOR a.op_name ( a.integer , a.numeric )'
    >>> as_string(alter_extension_add_object('ext', 'COLLATION', ('public', 'coll')))
    'ALTER EXTENSION ext ADD COLLATION public.coll'
    >>> as_string(alter_extension_add_object('ext', 'TEXT SEARCH CONFIGURATION', ('public', 'txt')))
    'ALTER EXTENSION ext ADD TEXT SEARCH CONFIGURATION public.txt'
    >>> as_string(alter_extension_add_object('ext', 'SCHEMA', 'testschema'))
    'ALTER EXTENSION ext ADD SCHEMA testschema'
    '''
    return compose([S('ALTER EXTENSION'), I(qualname), S('ADD'), *construct_object_update(ty, *val)])

def alter_extension_drop_object(qualname, ty, *val):
    r''' Generate ALTER EXTENSION DROP OBJECT.
    >>> as_string(alter_extension_drop_object('ext', 'EVENT TRIGGER', 'abc_trg'))
    'ALTER EXTENSION ext DROP EVENT TRIGGER abc_trg'
    >>> as_string(alter_extension_drop_object('ext', 'ACCESS METHOD', 'gist'))
    'ALTER EXTENSION ext DROP ACCESS METHOD gist'
    >>> as_string(alter_extension_drop_object('ext', 'FUNCTION', ('public', 'fn_test'), 'integer, numeric'))
    'ALTER EXTENSION ext DROP FUNCTION public.fn_test ( integer, numeric )'
    >>> as_string(alter_extension_drop_object('ext', 'OPERATOR CLASS', ('public', 'op'), 'heap'))
    'ALTER EXTENSION ext DROP OPERATOR CLASS public.op USING heap'
    >>> as_string(alter_extension_drop_object('ext', 'FOREIGN DATA WRAPPER', 'postgres_fdw'))
    'ALTER EXTENSION ext DROP FOREIGN DATA WRAPPER postgres_fdw'
    '''
    return compose([S('ALTER EXTENSION'), I(qualname), S('DROP'), *construct_object_update(ty, *val)])

def create_cast(source_type, tgt_type, castmethod, castcontext, funcname=None, funcargs=None):
    ''' Generate CREATE CAST
    >>> as_string(create_cast(['pg_catalog', 'bigint'], ['pg_catalog', 'int4'], 'f', 'a', funcname=\
        ['pg_catalog', 'int4'], funcargs='bigint'))
    'CREATE CAST ( pg_catalog.bigint AS pg_catalog.int4 ) WITH FUNCTION pg_catalog.int4 ( bigint ) AS ASSIGNMENT'
    >>> as_string(create_cast(['pg_catalog','int2'], ['pg_catalog', 'regrole'], 'b', 'e'))
    'CREATE CAST ( pg_catalog.int2 AS pg_catalog.regrole ) WITHOUT FUNCTION'
    '''
    return compose([
        S('CREATE CAST'), S('('), I(*source_type), S('AS'), I(*tgt_type), S(')'),
        *([S('WITH FUNCTION'), I(*funcname), S('('), S(funcargs), S(')')] if castmethod == 'f' else
          [S({'b': 'WITHOUT FUNCTION', 'i': 'WITH INOUT'}[castmethod])]),
        S({'a': 'AS ASSIGNMENT', 'i': 'AS IMPLICIT', 'e': ''}[castcontext])])

def drop_cast(source_type, tgt_type):
    ''' Generate DROP CAST
    >>> as_string(drop_cast(['pg_catalog','int2'], ['pg_catalog', 'regrole']))
    'DROP CAST IF EXISTS ( pg_catalog.int2 AS pg_catalog.regrole )'
    '''
    return compose([S('DROP CAST IF EXISTS'), S('('), I(*source_type), S('AS'), I(*tgt_type), S(')')])

def comment_cast(src_type, tgt_type, objcomment):
    '''Generate COMMENT ON CAST.
    >>> as_string(comment_cast(('public', 'text'), ('public', 'integer'), 'Cast here'))
    "COMMENT ON CAST ( public.text AS public.integer ) IS 'Cast here'"
    '''
    return compose([S('COMMENT ON'), S('CAST ('), I(*src_type), S('AS'), I(*tgt_type), S(')'), S('IS'), L(objcomment)])

def create_event_trigger(qualname, evtevent, funcname, functype):
    ''' Generate CREATE EVENT TRIGGER
    >>> as_string(create_event_trigger('evt_trg', 'evt_start', ('public', 'update_fn'), 'f'))
    'CREATE EVENT TRIGGER evt_trg ON evt_start EXECUTE FUNCTION public.update_fn ()'
    '''
    return compose([
        S('CREATE EVENT TRIGGER'), I(qualname), S('ON'), I(evtevent), S('EXECUTE'),
        S({'f': 'FUNCTION', 'p': 'PROCEDURE'}[functype]), I(*funcname), S('()')])

def alter_event_trigger_able(qualname, enable=False, mode=None):
    ''' Generate ALTER EVENT TRIGGER able/disable
    >>> as_string(alter_event_trigger_able('evt_trg'))
    'ALTER EVENT TRIGGER evt_trg DISABLE'
    >>> as_string(alter_event_trigger_able('evt_trg', True, 'ALWAYS'))
    'ALTER EVENT TRIGGER evt_trg ENABLE ALWAYS'
    '''
    return compose([
        S('ALTER EVENT TRIGGER'), I(qualname), S('ENABLE') if enable else S('DISABLE'), S(mode) if mode else None])

def alter_event_trigger_owner(qualname, rolname):
    ''' Generate ALTER EVENT TRIGGER OWNER TO
    >>> as_string(alter_event_trigger_owner('evt_trg', 'myrole'))
    'ALTER EVENT TRIGGER evt_trg OWNER TO myrole'
    '''
    return compose([S('ALTER EVENT TRIGGER'), I(qualname), S('OWNER TO'), I(rolname)])

def create_aggregate(qualname, argtype, aggtransfn, aggtranstype, proparallel, opts):
    ''' Generate CREATE AGGREGATE
    >>> from collections import OrderedDict
    >>> as_string(create_aggregate(['public', 'maximum_by'], 'cmpbox', ['public', '_cmpbox_greatest'],\
        ['public', 'cmpbox'], 'u', OrderedDict({'MSSPACE': 0, ' SSPACE': 1, 'FINALFUNC_MODIFY': 'r',\
        'MFINALFUNC_MODIFY': 's', 'FINALFUNC': ['public' ,'cmpunbox']})))
    'CREATE AGGREGATE public.maximum_by ( cmpbox ) ( SFUNC = public._cmpbox_greatest , STYPE = public.cmpbox , \
PARALLEL = UNSAFE ,  SSPACE = 1 , FINALFUNC_MODIFY = READ_ONLY , MFINALFUNC_MODIFY = SHAREABLE , FINALFUNC = \
public.cmpunbox )'
    '''
    ret = [
        S('CREATE AGGREGATE'), I(*qualname), S('('), S(argtype), S(')'), S('('), S('SFUNC ='), I(*aggtransfn),
        S(','), S('STYPE ='), I(*aggtranstype), S(','), S('PARALLEL ='),
        S({'s': 'SAFE', 'r': 'RESTRICTED', 'u': 'UNSAFE', None: None, '': None}[proparallel])]
    for k, v in opts.items():
        if v:
            ret.extend([S(','), S(k), S('=')])
            if k in ('FINALFUNC_MODIFY', 'MFINALFUNC_MODIFY'):
                ret.append(S({'r': 'READ_ONLY', 's': 'SHAREABLE', 'w': 'READ_WRITE'}[v]))
            elif isinstance(v, str):
                ret.append(S(v))
            elif isinstance(v, list):
                ret.append(I(*v))
            else:
                ret.append(L(v))
    ret.append(S(')'))
    return compose(ret)

def alter_aggregate_owner(qualname, argtype, rolname):
    ''' Generate ALTER AGGREGATE OWNER TO
    >>> as_string(alter_aggregate_owner(['public', 'maximum_by'], 'cmpbox', 'myrole'))
    'ALTER AGGREGATE public.maximum_by ( cmpbox ) OWNER TO myrole'
    '''
    return compose([S('ALTER AGGREGATE'), I(*qualname), S('('), S(argtype), S(')'), S('OWNER TO'), I(rolname)])

def comment_aggregate(qualname, signature, objcomment=None):
    '''Generate COMMENT ON aggregate.
    >>> as_string(comment_aggregate(('public', 'foo_agg'), 'text, text', 'hi'))
    "COMMENT ON AGGREGATE public.foo_agg ( text, text ) IS 'hi'"
    >>> as_string(comment_aggregate(('public', 'foo_agg'), 'int'))
    'COMMENT ON AGGREGATE public.foo_agg ( int ) IS NULL'
    '''
    return compose([
        S('COMMENT ON AGGREGATE'), I(*qualname), S('('), S(signature), S(')'), S('IS'),
        L(objcomment) if objcomment else S('NULL')])

def create_db(qualname, dbopts):
    '''Generate CREATED DB
    # NOTE: we  don't need to care about strategy because all new dbs use WAL_LOG and no longer the old WAL_LOG
    >>> as_string(create_db('mydb', {'OWNER': 'myrole', 'TEMPLATE': 'UTF8', 'ALLOW_CONNECTIONS': True, \
        'CONNECTION LIMIT': 5}))
    "CREATE DATABASE mydb WITH OWNER = 'myrole' TEMPLATE = 'UTF8' ALLOW_CONNECTIONS = true CONNECTION LIMIT = 5"
    '''
    ret = [S('CREATE DATABASE'), I(qualname)]
    if dbopts:
        ret.append(S('WITH'))
        for opt, v in dbopts.items():
            if opt =='LOCALE_PROVIDER' and v == 'c':
                v = 'libc'  # https://www.postgresql.org/docs/current/sql-createdatabase.html  # noqa: PLW2901
            ret.extend([S(opt), S('='), L(v)] if v is not None else [])
    return compose(ret)

def alter_db_option(qualname, opts):
    '''Generate ALTER DB option
    NOTE: SET/RESET configuration_parameter is not implemented because I think that's handled in roles.
    >>> as_string(alter_db_option('test_db', {'ALLOW_CONNECTIONS': True, 'CONNECTION LIMIT': 6, 'IS_TEMPLATE': True}))
    'ALTER DATABASE test_db WITH ALLOW_CONNECTIONS true CONNECTION LIMIT 6 IS_TEMPLATE true'
    '''
    ret = [S('ALTER DATABASE'),
        I(qualname),
        S('WITH')]
    for opt, v in opts.items():
        ret.extend([S(opt), L(v)])
    return compose(ret)

def alter_db_owner(qualname, rolname):
    ''' Generate ALTER DB rolname
    >>> as_string(alter_db_owner('test_db', 'myrole'))
    'ALTER DATABASE test_db OWNER TO myrole'
    '''
    return compose([S('ALTER DATABASE'), I(qualname), S('OWNER TO'), I(rolname)])

def alter_db_tablespace(qualname, tablespace):
    ''' Generate ALTER DB tablespace
    >>> as_string(alter_db_tablespace('test_db', 'nvme0'))
    'ALTER DATABASE test_db SET TABLESPACE nvme0'
    '''
    return compose([S('ALTER DATABASE'), I(qualname), S('SET TABLESPACE'), I(tablespace)])


def create_publication_tables(qualname, tables, publish, publish_via_partition_root):
    '''Generate CREATE PUBLICATION FOR
    >>> as_string(create_publication_tables('pub', [{'name': 'public.t1', 'qual': 'expr', 'cols': ['col1', 'col2']},\
        { 'name': 'public.t2', 'cols': [], 'qual': ''}], 'insert, update', True))  # pylint: disable=line-too-long
    "CREATE PUBLICATION pub FOR TABLE ONLY public.t1 ( col1 , col2 ) WHERE ( expr ) , public.t2 WITH ( publish = \
'insert, update' , publish_via_partition_root = true )"
    '''
    ret = [S('CREATE PUBLICATION'), I(qualname), S('FOR TABLE ONLY')]
    for t in tables:
        ret.append(S(t['name']))
        if t.get('cols'):
            ret.append(S('('))
            ret.extend(S(',').join([S(c) for c in t['cols']]))
            ret.append(S(')'))
        if t.get('qual'):
            ret.extend([
                S('WHERE ('),
                S(t['qual']),
                S(')')
            ])
        ret.append(S(','))
    ret = ret[:-1] if ret[-1] == S(',') else ret
    ret.append(S('WITH ('))
    ret.extend([S('publish'), S('='), L(publish), S(',')])
    ret.extend([S('publish_via_partition_root'), S('='), L(publish_via_partition_root)])
    ret.append(S(')'))
    return compose(ret)

def create_publication_schemas(qualname, schemas):
    '''Generate CREATE PUBLICATION FOR TABLES IN SCHEMA
    >>> as_string(create_publication_schemas('pub', ['marketing', 'sales']))
    'CREATE PUBLICATION pub FOR TABLES IN SCHEMA marketing , sales'
    '''
    return compose([
        S('CREATE PUBLICATION'), I(qualname), S('FOR TABLES IN SCHEMA'), *S(',').join([S(s) for s in schemas])])

def create_publication_all_tables(qualname):
    '''Generate CREATE PUBLICATION FOR ALL TABLES
    >>> as_string(create_publication_all_tables('pub'))
    'CREATE PUBLICATION pub FOR ALL TABLES'
    '''
    return compose([S('CREATE PUBLICATION'), I(qualname), S('FOR ALL TABLES')])

def compose_alter_pub_obj(cmd, qualname, objs):
    ret = [S('ALTER PUBLICATION'), I(qualname), S(cmd), S('TABLE')]
    for o in objs:
        ret.append(S(o['name']))
        # postgres does not allow us to add cols in `ALTER PUBLICATION DROP`
        if o.get('cols') and cmd == 'ADD':
            ret.append(S('('))
            ret.extend(S(',').join([S(c) for c in o['cols']]))
            ret.append(S(')'))
        if o.get('qual'):
            ret.extend([
                S('WHERE ('),
                S(o['qual']),
                S(')')
            ])
        ret.append(S(','))
    ret = ret[:-1] if ret[-1] == S(',') else ret
    return compose(ret)

def alter_pub_add_obj(qualname, objs):
    '''Generate ALTER PUBLICATION ADD OBJECT
    >>> as_string(alter_pub_add_obj('pub', [{'name': 'public.t1', 'cols': [], 'qual': None},\
        {'name': 'public.t2', 'cols': None,  'qual': 'expr'}]))
    'ALTER PUBLICATION pub ADD TABLE public.t1 , public.t2 WHERE ( expr )'
    '''
    return compose_alter_pub_obj('ADD', qualname, objs)

def alter_pub_drop_obj(qualname, objs):
    '''Generate ALTER PUBLICATION DROP OBJECT
    >>> as_string(alter_pub_drop_obj('pub', [{'name': 'public.t1'}]))
    'ALTER PUBLICATION pub DROP TABLE public.t1'
    '''
    return compose_alter_pub_obj('DROP', qualname, objs)

def alter_pub_add_schemas(qualname, schemas):
    '''Generate ALTER PUBLICATION ADD schema
    >>> as_string(alter_pub_add_schemas('pub', ['schema1', 'schema2']))
    'ALTER PUBLICATION pub ADD TABLES IN SCHEMA schema1 , schema2'
    '''
    return compose([
        S('ALTER PUBLICATION'), I(qualname), S('ADD TABLES IN SCHEMA'), *S(',').join([S(s) for s in schemas])])

def alter_pub_drop_schemas(qualname, schemas):
    '''Generate ALTER PUBLICATION DROP schema'''
    '''Generate ALTER PUBLICATION DROP schema
    >>> as_string(alter_pub_drop_schemas('pub', ['schama1']))
    ALTER PUBLICATION pub DROP TABLES IN SCHEMA schema1
    '''
    return compose([
        S('ALTER PUBLICATION'), I(qualname), S('DROP TABLES IN SCHEMA'), *S(',').join([S(s) for s in schemas])])

def alter_pub_param(qualname, params):
    ''' Generate ALTER PUBLICATION SET publication_parameter
    >>> as_string(alter_pub_param('pub', {'publish': 'insert, delete', 'publish_via_partition_root': False}))
    "ALTER PUBLICATION pub SET ( publish = 'insert, delete' , publish_via_partition_root = false )"
    '''
    return compose([
        S('ALTER PUBLICATION'), I(qualname), S('SET'), S('('),
        *S(',').join([S(' ').join([S(k), S('='), L(v)]) for k, v in params.items()]), S(')')])

def alter_pub_owner(qualname, rolname):
    ''' Generate ALTER PUBLICATION OWNER TO.
    >>> as_string(alter_pub_owner('pub', 'root'))
    'ALTER PUBLICATION pub OWNER TO root'
    '''
    return compose([S('ALTER PUBLICATION'), I(qualname), S('OWNER TO'), I(rolname)])

def create_operator(
        qualname, function_name, hash, merge, left_type=None, right_type=None, com_op=None, neg_op=None, res_proc=None,
        join_proc=None):
    ''' generate CREATE OPERATOR
    >>> as_string(create_operator(('public', '>'), ('public', 'func'),  True, False, right_type=('public', 'right'),\
        res_proc=('public', 'res')))
    'CREATE OPERATOR public.> ( FUNCTION = public.func , RIGHTARG = public.right , RESTRICT = public.res , HASHES )'
    '''
    return compose([
        S('CREATE OPERATOR'), I_op(*qualname), S('('),
        S('FUNCTION ='),
        I(*function_name),
        *([S(', LEFTARG ='), I(*left_type)] if left_type else []),
        *([S(', RIGHTARG ='), I(*right_type)] if right_type else []),
        *([S(', COMMUTATOR ='), I(*com_op)] if com_op else []),
        *([S(', NEGATOR ='), I(*neg_op)] if neg_op else []),
        *([S(', RESTRICT ='), I(*res_proc)] if res_proc else []),
        *([S(' JOIN ='), I(*join_proc)] if join_proc else []),
        S(', HASHES') if hash else None,
        S(', MERGES') if merge else None,
        S(')'),
    ])

def alter_op_owner(qualname, rolname, right_type, left_type=None):
    ''' Generate ALTER OPERATOR OWNER TO.
    >>> as_string(alter_op_owner(('public', '>>>'), 'root', ('public', 'integer')))
    'ALTER OPERATOR public.>>> ( NONE , public.integer ) OWNER TO root'
    '''
    return compose([
        S('ALTER OPERATOR'), I_op(*qualname), S('('), I(*left_type) if left_type else S('NONE'),
        S(','), I(*right_type), S(')'), S('OWNER TO'), I(rolname)])

def I_op(nspname, opname):  # noqa: N802
    ''' Quote a qualified operator. The namespace gets quoted, but the operator does not.
    >>> as_string(I_op('foo', '>>>>'))
    'foo.>>>>'
    >>> as_string(I_op('foo bar', '>>>>'))
    '"foo bar".>>>>'
    '''
    return S('{}.{}').format(I(nspname), S(opname))

def alter_operator_procs(qualname, right_type, left_type=None, res_proc=None, join_proc=None):
    ''' Generate ALTER OPERATOR SET
    >>> as_string(alter_operator_procs(('public', '---'), ('public', 'integer'), res_proc=('public', 'restrict')))
    'ALTER OPERATOR public.--- ( NONE , public.integer ) SET ( RESTRICT = public.restrict , JOIN = NONE )'
    '''
    return compose([
        S('ALTER OPERATOR'), I_op(*qualname), S('('),
        I(*left_type) if left_type else S('NONE'), S(','),
        I(*right_type),S(')'),
        S('SET ('),
        S('RESTRICT ='), I(*res_proc) if res_proc else S('NONE'), S(','),
        S('JOIN ='), I(*join_proc) if join_proc else S('NONE'),
        S(')'),])

def comment_operator(qualname, objcomment, right_type, left_type=None):
    '''Generate COMMENT ON statement.
    >>> as_string(comment_operator(('public', '---'), 'hi', ('public', 'integer')))
    "COMMENT ON OPERATOR public.--- ( NONE , public.integer ) IS 'hi'"
    '''
    return compose([
        S('COMMENT ON OPERATOR'), I_op(*qualname),
        S('('), I(*left_type) if left_type else S('NONE'), S(','), I(*right_type), S(')'), S('IS'), L(objcomment)])

def create_lang(qualname, trusted, callproc=None, inlineproc=None, valproc=None):
    ''' Generate CREATE LANGUAGE
    >>> as_string(create_lang('ex_lang', True, callproc=('public', 'call'), valproc=('public', 'validate')))
    'CREATE TRUSTED LANGUAGE ex_lang HANDLER public.call VALIDATOR public.validate'
    >>> as_string(create_lang('ex_lang', False, callproc=('public', 'call')))
    'CREATE LANGUAGE ex_lang HANDLER public.call'
    '''
    return compose([
        S('CREATE'), S('TRUSTED') if trusted else None, S('LANGUAGE'), I(qualname),
        *([S('HANDLER'), I(*callproc)] if callproc else []),
        *([S('INLINE'), I(*inlineproc)] if inlineproc else []),
        *([S('VALIDATOR'), I(*valproc)] if valproc else [])])

def alter_lang_owner(qualname, rolname):
    ''' Generate ALTER LANGUAGE OWNER
    >>> as_string(alter_lang_owner('lang', 'myrole'))
    'ALTER LANGUAGE lang OWNER TO myrole'
    '''
    return compose([S('ALTER LANGUAGE'), I(qualname), S('OWNER TO'), I(rolname)])

def create_ts_dict(qualname, dicttemplate, dictinitoption):
    ''' Generate CREATE TEXT SEARCH DICTIONARY
    >>> as_string(create_ts_dict(('public', 'my_russian'), ('public', 'snowball'), \
        "language = 'russian', stopwords = 'myrussian'"))
    "CREATE TEXT SEARCH DICTIONARY public.my_russian \
( TEMPLATE = public.snowball , language = 'russian', stopwords = 'myrussian' )"
    '''
    return compose([
        S('CREATE TEXT SEARCH DICTIONARY'), I(*qualname),
        S('( TEMPLATE ='), I(*dicttemplate), *([S(','), S(dictinitoption)] if dictinitoption else []), S(')')])

def alter_ts_dict_opts(qualname, opts):
    ''' Generate ALTER TEXT SEARCH DICTIONARY option = value
    >>> as_string(alter_ts_dict_opts(('public', 'my_russian'), 'language = russian, stopwords = myrussian'))
    'ALTER TEXT SEARCH DICTIONARY public.my_russian ( language = russian, stopwords = myrussian )'
    '''
    return compose([S('ALTER TEXT SEARCH DICTIONARY'), I(*qualname), S('('), S(opts), S(')')])

def alter_ts_owner(qualname, rolname, ty):
    ''' Generate ALTER TEXT SEARCH DICTIONARY OWNER TO.
    >>> as_string(alter_ts_owner(('public', 'my_russian'), 'root', 'DICTIONARY'))
    'ALTER TEXT SEARCH DICTIONARY public.my_russian OWNER TO root'
    '''
    return compose([S('ALTER TEXT SEARCH'), S(ty), I(*qualname), S('OWNER TO'), I(rolname)])

def create_ts_template(qualname, lexize, init=None):
    ''' Generate CREATE TEXT SEARCH TEMPLATE
    >>> as_string(create_ts_template(('public', 'temp'), ('public', 'lex'), ('public', 'init')))
    'CREATE TEXT SEARCH TEMPLATE public.temp ( INIT = public.init , LEXIZE = public.lex )'
    >>> as_string(create_ts_template(('public', 'temp'), ('public', 'lex')))
    'CREATE TEXT SEARCH TEMPLATE public.temp ( LEXIZE = public.lex )'
    '''
    return compose([
        S('CREATE TEXT SEARCH TEMPLATE'), I(*qualname),
        S('('), S('INIT =') if init else None, I(*init) if init else None, S(',') if init else None,
        S('LEXIZE ='), I(*lexize), S(')')])

def create_ts_parser(qualname, start, get_token, end, lex_types, headline=None):
    ''' Generate CREATE TEXT SEARCH PARSER
    >>> as_string(create_ts_parser(('public', 'parser'), ('public', 'start'), ('public', 'get'), ('public', 'end'),\
        ('public', 'lex'), headline=('public', 'head')))
    'CREATE TEXT SEARCH PARSER public.parser ( START = public.start , GETTOKEN = public.get , END = public.end ,\
 LEXTYPES = public.lex , HEADLINE = public.head )'
    >>> as_string(create_ts_parser(('public', 'parser'), ('public', 'start'), ('public', 'get'), ('public', 'end'),\
        ('public', 'lex')))
    'CREATE TEXT SEARCH PARSER public.parser ( START = public.start , GETTOKEN = public.get , END = public.end ,\
 LEXTYPES = public.lex )'
    '''
    return compose([
        S('CREATE TEXT SEARCH PARSER'), I(*qualname), S('('),
        S('START ='), I(*start), S(','),
        S('GETTOKEN ='), I(*get_token), S(','),
        S('END ='), I(*end), S(','),
        S('LEXTYPES ='), I(*lex_types), S(',') if headline else None,
        S('HEADLINE =') if headline else None, I(*headline) if headline else None,
        S(')')])

def create_ts_config(qualname, parser):
    ''' Generate CREATE TEXT SEARCH CONFIGURATION
    >>> as_string(create_ts_config(('postgres', 'pg'), ('pg_catalog', 'parser')))
    'CREATE TEXT SEARCH CONFIGURATION postgres.pg ( PARSER = pg_catalog.parser )'
    '''
    return compose([S('CREATE TEXT SEARCH CONFIGURATION'), I(*qualname), S('('), S('PARSER ='), I(*parser), S(')')])

def alter_ts_config_mapping(qualname, cmd, token_type, dicts):
    ''' Generate ALTER TEXT SEARCH CONFIGURATION ADD MAPPING
    possible cmds: 'ADD MAPPING FOR', 'ALTER MAPPING FOR'
    >>> as_string(alter_ts_config_mapping(('public', 'simple'), 'ADD MAPPING FOR', 'ascii', [('public', 'dict1'), \
        ('public', 'dict2')]))
    'ALTER TEXT SEARCH CONFIGURATION public.simple ADD MAPPING FOR ascii WITH public.dict1 , public.dict2'
    '''
    return compose([
        S('ALTER TEXT SEARCH CONFIGURATION'), I(*qualname), S(cmd), S(token_type), S('WITH'),
        *S(',').join([I(*x) for x in dicts])])

def alter_ts_config_drop_mapping(qualname, token_types):
    ''' Generate ALTER TEXT SEARCH CONFIGURATION DROP MAPPING
    >>> as_string(alter_ts_config_drop_mapping(('public', 'simple'), ['ascii', 'ty2', 'ty3']))
    'ALTER TEXT SEARCH CONFIGURATION public.simple DROP MAPPING FOR ascii, ty2, ty3'
    '''
    return compose([
        S('ALTER TEXT SEARCH CONFIGURATION'), I(*qualname), S('DROP MAPPING FOR'),
        S(', ').join([S(x) for x in token_types])])

def alter_ts_config_replace_mapping(qualname, old_dict, new_dict, token_types=None):
    ''' Generate ALTER TEXT SEARCH CONFIGURATION ALTER MAPPING REPLACE
    >>> as_string(alter_ts_config_replace_mapping(('public', 'simple'), ('public', 'dict1'), ('public', 'dict2'), \
        token_types=('ascii', 'numhword')))
    'ALTER TEXT SEARCH CONFIGURATION public.simple ALTER MAPPING FOR ascii , numhword REPLACE \
public.dict1 WITH public.dict2'
    >>> as_string(alter_ts_config_replace_mapping(('public', 'simple'), ('public', 'dict1'), ('public', 'dict2'), \
        token_types=('ascii',)))
    'ALTER TEXT SEARCH CONFIGURATION public.simple ALTER MAPPING FOR ascii REPLACE public.dict1 WITH public.dict2'
    >>> as_string(alter_ts_config_replace_mapping(('public', 'simple'), ('public', 'dict1'), ('public', 'dict2')))
    'ALTER TEXT SEARCH CONFIGURATION public.simple ALTER MAPPING REPLACE public.dict1 WITH public.dict2'
    '''
    return compose([
        S('ALTER TEXT SEARCH CONFIGURATION'), I(*qualname),
        S('ALTER MAPPING'), S('FOR') if token_types else S('REPLACE'),
        *S(',').join([S(x) for x in token_types or []]),
        (S('REPLACE') if token_types else None),
        *[I(*old_dict), S('WITH'), I(*new_dict)]])

def _op_class_ops(ops):
    ret = []
    for op in ops:
        x = {'o': 'FOR ORDER BY', 's': 'FOR SEARCH'}[op['amoppurpose']]
        ret.extend([
            S('OPERATOR'), L(op['amopstrategy']), S(op['oprname']), S('('),
            S(op['lefttype']), S(','),
            S(op['righttype']), S(')'),
            S(x),
            S(op['amopsortfamily']) if x == 'FOR ORDER BY' else None, S(',')])
    return ret

def _op_class_procs(procs):
    ret = []
    for proc in procs:
        ret.extend([
            S('FUNCTION'), L(proc['amprocnum']), S('('),
            S(proc['lefttype']), S(','),
            S(proc['righttype']), S(')'),
            I(*proc['proname']), S('('), S(proc['proargs']), S(')'), S(',')])
    return ret[:-1]

def create_op_class(qualname, opcdefault, opcintype, amname, ops=None, procs=None, opcfamily=None, opcstorage=None):
    ''' Generate CREATE OPERATOR CLASS
    https://www.postgresql.org/docs/current/sql-createopclass.html
    >>> as_string(create_op_class(('public', 'opc'), True, ('public', '_int4'), 'gist', [{'oprname': 'public.==', \
        'amopstrategy': 2, 'lefttype': 'public.integer', 'righttype': 'public.text', 'amoppurpose': 'o', \
        'amopsortfamily': 'public.opf'}, {'oprname': 'public.++', 'amopstrategy': 1, 'lefttype': 'public.text', \
        'righttype': 'public.text', 'amoppurpose': 's', 'amopsortfamily': None}], procs=[{'proname': ['public', 'pro'],\
        'proargs': 'text,text', 'lefttype': 'public.text', 'righttype': 'public.text', 'amprocnum': 4, 'amprocfamily':\
        'public.fam'}], opcfamily=('public', 'opf')))
    'CREATE OPERATOR CLASS public.opc DEFAULT FOR TYPE public._int4 USING gist FAMILY public.opf \
AS OPERATOR 2 public.== ( public.integer , public.text ) FOR ORDER BY public.opf , OPERATOR \
1 public.++ ( public.text , public.text ) FOR SEARCH , FUNCTION 4 \
( public.text , public.text ) public.pro ( text,text )'
    '''
    ret = [
        S('CREATE OPERATOR CLASS'), I(*qualname),
        S('DEFAULT') if opcdefault else None,
        S('FOR TYPE'), I(*opcintype),
        S('USING'), I(amname),
        S('FAMILY') if opcfamily else None,
        I(*opcfamily) if opcfamily else None,
        S('AS')]
    ret.extend(_op_class_ops(ops))
    ret = ret[:-1] if not procs else ret
    ret.extend(_op_class_procs(procs))
    ret = ret[:-1] if ret[-1] == S(',') else ret
    ret.extend([S(','), S('STORAGE'), I(*opcstorage)]) if opcstorage else None
    return compose(ret)

def alter_opclass_owner(qualname, amname, rolname):
    ''' Generate ALTER OPERATOR CLASS
    >>> as_string(alter_opclass_owner(('public', 'opc'), 'gist', 'me'))
    'ALTER OPERATOR CLASS public.opc USING gist OWNER TO me'
    '''
    return compose([S('ALTER OPERATOR CLASS'), I(*qualname), S('USING'), I(amname), S('OWNER TO'), I(rolname)])

def create_op_family(qualname, amname):
    ''' Generate CREATE OPERATOR FAMILY
    >>> as_string(create_op_family(('public', 'opf'), 'acm'))
    'CREATE OPERATOR FAMILY public.opf USING acm'
    '''
    return compose([S('CREATE OPERATOR FAMILY'), I(*qualname), S('USING'), I(amname)])

def _gen_ops_add(ops):
    ret = []
    for k, op in ops.items():
        x = {'o': 'FOR ORDER BY', 's': 'FOR SEARCH'}[op['amoppurpose']]
        ret.extend([
            S('OPERATOR'), L(op['amopstrategy']),
            S(k), S('('),
            S(op['lefttype']), S(','),
            S(op['righttype']),
            S(')'),
            S(x),
            S(op['amopsortfamily']) if x == 'FOR ORDER BY' else None, S(',')])
    return ret

def _gen_ops_drop(ops):
    ret = []
    for op in ops.values():
        ret.extend([
            S('OPERATOR'), L(op['amopstrategy']), S('('),
            S(op['lefttype']), S(','),
            S(op['righttype']), S(')'), S(',')])
    return ret

def _gen_procs_add(procs):
    ret = []
    for k, proc in procs.items():
        ret.extend([
            S('FUNCTION'), L(proc['amprocnum']), S('('),
            S(proc['lefttype']), S(','),
            S(proc['righttype']), S(')'),
            S(k), S('('), S(proc['proargs']), S(')'), S(',')])
    return ret[:-1]

def _gen_procs_drop(procs):
    ret = []
    for proc in procs.values():
        ret.extend([
            S('FUNCTION'), L(proc['amprocnum']), S('('),
            S(proc['lefttype']), S(','),
            S(proc['righttype']), S(')'), S(',')])
    return ret[:-1]

def alter_opfamily_add(qualname, amname, ops=None, procs=None):
    ''' Generate ALTER OPERATOR CLASS ADD
    >>> as_string(alter_opfamily_add(('public', 'opf'), 'gist', ops={'public.==': {'amopstrategy': 2, 'lefttype': \
        'public.integer', 'righttype': 'public.text', 'amoppurpose': 'o', 'amopsortfamily': 'public.opf'}}))
    'ALTER OPERATOR FAMILY public.opf USING gist ADD OPERATOR 2 public.== ( public.integer , public.text \
) FOR ORDER BY public.opf'
    >>> as_string(alter_opfamily_add(('public', 'opf'), 'gist', ops={'public.==': {'amopstrategy': 2, 'lefttype': \
        'public.integer', 'righttype': 'public.text', 'amoppurpose': 'o', 'amopsortfamily': 'public.opf'}}))
    'ALTER OPERATOR FAMILY public.opf USING gist ADD OPERATOR 2 public.== ( \
public.integer , public.text ) FOR ORDER BY public.opf'
    '''
    ret = [S('ALTER OPERATOR FAMILY'), I(*qualname), S('USING'), I(amname), S('ADD')]
    if ops:
        ret.extend(_gen_ops_add(ops))
        ret = ret[:-1] if not procs else ret
    if procs:
        ret.extend(_gen_procs_add(procs))
    ret = ret[:-1] if ret[-1] == S(',') else ret
    return compose(ret)

def alter_opfamily_drop(qualname, amname, ops=None, procs=None):
    '''
    Generate ALTER OPERATOR CLASS ADD DROP
    https://www.postgresql.org/docs/current/sql-alteropfamily.html
    >>> as_string(alter_opfamily_drop(('public', 'opf'), 'gist', procs=\
        {'public.pro': {'proargs': 'text,text', 'lefttype': 'public.text', 'righttype': 'public.text', 'amprocnum': 4,\
        'amprocfamily': 'public.fam'}}))
    'ALTER OPERATOR FAMILY public.opf USING gist DROP FUNCTION 4 ( public.text , public.text )'
    '''
    ret = [S('ALTER OPERATOR FAMILY'), I(*qualname), S('USING'), I(amname), S('DROP')]
    if ops:
        ret.extend(_gen_ops_drop(ops))
        ret = ret[:-1] if not procs else ret
    if procs:
        ret.extend(_gen_procs_drop(procs))
    ret = ret[:-1] if ret[-1] == S(',') else ret
    return compose(ret)

def alter_opfamily_owner(qualname, amname, rolname):
    ''' Generate ALTER OPERATOR CLASS
    >>> as_string(alter_opfamily_owner(('public', 'opf'), 'gist', 'me'))
    'ALTER OPERATOR FAMILY public.opf USING gist OWNER TO me'
    '''
    return compose([S('ALTER OPERATOR FAMILY'), I(*qualname), S('USING'), I(amname), S('OWNER TO'), I(rolname)])

def comment_op(qualname, objtype, method, objcomment):
    '''Generate COMMENT ON opclass or opfamily.
    >>> as_string(comment_op(('public', 'opc'), 'CLASS', 'btree', 'hi'))
    "COMMENT ON OPERATOR CLASS public.opc USING btree IS 'hi'"
    '''
    return compose([S('COMMENT ON OPERATOR'), S(objtype), I(*qualname), S('USING'), S(method), S('IS'), L(objcomment)])

def server_opts(opts):
    ret = {}
    if opts:
        for opt in opts:
            splits = opt.split('=')
            ret[splits[0]] = splits[1]
    return ret

def _gen_opts(opts):
    ret = []
    options = server_opts(opts)
    if options:
        ret.extend([S('OPTIONS'), S('(')])
        for k, v in options.items():
            ret.extend([S(k), L(str(v)), S(',')])
        ret.pop()
        ret.append(S(')'))
    return ret

def _gen_alter_opts(opts):
    ret = []
    if opts:
        ret.extend([S('OPTIONS'), S('(')])
        for k, v in opts.items():
            splits = k.split('=')
            l, r = splits[0], splits[1]
            if v == 'set':
                ret.extend([S('SET'), S(l), L(r), S(',')])
            elif v == 'add':
                ret.extend([S('ADD'), S(l), L(r), S(',')])
            elif v == 'drop':
                ret.extend([S('DROP'), S(l), S(',')])
        ret.pop()
        ret.append(S(')'))
    return ret

def create_foreign_server(qualname, fdw_name, server_type=None, version=None, opts=None):
    ''' Generate CREATE SERVER
    >>> as_string(create_foreign_server('server', 'my_fdw', server_type=None, version='0.1', \
        opts=['host=127.0.0.1', 'port=5432', 'dbname=foreigndb']))
    "CREATE SERVER server VERSION '0.1' FOREIGN DATA WRAPPER my_fdw OPTIONS ( \
host '127.0.0.1' , port '5432' , dbname 'foreigndb' )"
    '''
    ret = [S('CREATE SERVER'), I(qualname)]
    if server_type:
        ret.extend([S('TYPE'), L(server_type)])
    if version:
        ret.extend([S('VERSION'), L(str(version))])
    ret.extend([S('FOREIGN DATA WRAPPER'), I(fdw_name), *_gen_opts(opts)])
    return compose(ret)

def alter_server_owner(qualname, rolname):
    ''' Generate ALTER SERVER OWNER
    https://www.postgresql.org/docs/current/sql-alterserver.html
    >>> as_string(alter_server_owner('serv', 'me'))
    'ALTER SERVER serv OWNER TO me'
    '''
    return compose([S('ALTER SERVER'), I(qualname), S('OWNER TO'), I(rolname)])

def alter_server_opts(qualname, new_version=None, opts=None):
    ''' Generate ALTER SERVER OPTIONS
    https://www.postgresql.org/docs/current/sql-alterserver.html
    >>> as_string(alter_server_opts('server', new_version='1.2', opts=\
        {'host=127.0.0.1': 'add', 'port=5432': 'drop', 'dbname=foreigndb': 'set'}))
    "ALTER SERVER server VERSION '1.2' OPTIONS ( ADD host '127.0.0.1' , DROP port , SET dbname 'foreigndb' )"
    '''
    return compose([
        S('ALTER SERVER'), I(qualname), S('VERSION') if new_version else None,
        L(str(new_version)) if new_version else None, *_gen_alter_opts(opts)])

def create_fdw(qualname, fdwhandler=None, fdwvalidator=None, opts=None):
    ''' Generate CREATE FOREIGN DATA WRAPPER
    https://www.postgresql.org/docs/current/sql-createforeigndatawrapper.html
    >>> as_string(create_fdw('fdw', fdwhandler=('public', 'handler'), fdwvalidator=None, opts=['foo=bar', 'fizz=buzz']))
    "CREATE FOREIGN DATA WRAPPER fdw HANDLER public.handler NO VALIDATOR OPTIONS ( foo 'bar' , fizz 'buzz' )"
    '''
    ret = [S('CREATE FOREIGN DATA WRAPPER'), I(qualname)]
    if fdwhandler:
        ret.extend([S('HANDLER'), I(*fdwhandler)])
    else:
        ret.append(S('NO HANDLER'))
    if fdwvalidator:
        ret.extend([S('VALIDATOR'), I(*fdwvalidator)])
    else:
        ret.append(S('NO VALIDATOR'))
    options = server_opts(opts)
    if options:
        ret.extend([S('OPTIONS'), S('(')])
        for k, v in options.items():
            ret.extend([S(k), L(str(v)), S(',')])
        ret = ret[:-1]
        ret.append(S(')'))
    return compose(ret)

def alter_fdw_opts(qualname, fdwhandler=None, fdwvalidator=None, opts=None):
    ''' Generate ALTER FOREIGN DATA WRAPPER
    >>> as_string(alter_fdw_opts('fdw', fdwhandler=None, fdwvalidator=('public', 'validator'), opts=\
        {'foo=bar': 'add', 'fizz=buzz': 'drop'}))
    "ALTER FOREIGN DATA WRAPPER fdw NO HANDLER VALIDATOR public.validator OPTIONS ( ADD foo 'bar' , DROP fizz )"
    '''
    ret = [S('ALTER FOREIGN DATA WRAPPER'), I(qualname)]
    if fdwhandler:
        ret.extend([S('HANDLER'), I(*fdwhandler)])
    else:
        ret.append(S('NO HANDLER'))
    if fdwvalidator:
        ret.extend([S('VALIDATOR'), I(*fdwvalidator)])
    else:
        ret.append(S('NO VALIDATOR'))
    ret.extend(_gen_alter_opts(opts))
    return compose(ret)

def alter_fdw_owner(qualname, rolname):
    ''' Generate ALTER FDW OWNER
    https://www.postgresql.org/docs/current/sql-alterserver.html
    >>> as_string(alter_fdw_owner('fdw', 'me'))
    'ALTER FOREIGN DATA WRAPPER fdw OWNER TO me'
    '''
    return compose([S('ALTER FOREIGN DATA WRAPPER'), I(qualname), S('OWNER TO'), I(rolname)])

def create_user_mapping(umuser, server, opts=None):
    ''' Generate CREATE USER MAPPING
    https://www.postgresql.org/docs/current/sql-createusermapping.html
    >>> as_string(create_user_mapping('me', 'serv', opts=['host=127.0.0.1', 'port=5432']))
    "CREATE USER MAPPING FOR me SERVER serv OPTIONS ( host '127.0.0.1' , port '5432' )"
    '''
    return compose([S('CREATE USER MAPPING FOR'), I(umuser), S('SERVER'), I(server), *_gen_opts(opts)])

def alter_user_mapping(umuser, server, opts=None):
    ''' Generate ALTER USER MAPPING
    https://www.postgresql.org/docs/current/sql-alterusermapping.html
    >>> as_string(alter_user_mapping('me', 'serv', opts={'host=127.0.0.1': 'add', 'port=5432': 'drop'}))
    "ALTER USER MAPPING FOR me SERVER serv OPTIONS ( ADD host '127.0.0.1' , DROP port )"
    '''
    return compose([S('ALTER USER MAPPING FOR'), I(umuser), S('SERVER'), I(server), *_gen_alter_opts(opts)])

def create_policy(qualname, tablename, polpermissive, polcmd, polroles=None, polqual=None, polwithcheck=None):
    ''' Generate CREATE POLICY
    https://www.postgresql.org/docs/current/sql-createpolicy.html
    >>> as_string(create_policy('pol', ('public', 't'), False, '*', polroles=['me', 'you'], polqual='SELECT 1'))
    'CREATE POLICY pol ON public.t AS RESTRICTIVE FOR ALL TO me , you USING ( SELECT 1 )'
    '''
    ret = [
        S('CREATE POLICY'), I(qualname), S('ON'), I(*tablename), S('AS'),
        S('PERMISSIVE') if polpermissive else S('RESTRICTIVE')]
    if polcmd:
        ret.extend([S('FOR'),
            S({
                'r': 'SELECT',
                'a': 'INSERT',
                'w': 'UPDATE',
                'd': 'DELETE',
                '*': 'ALL'
            }[polcmd])])
    if polroles:
        tos = S(',').join([S(role) for role in polroles])
        ret.append(S('TO'))
        ret.extend(tos)
    if polqual:
        ret.extend([S('USING ('), S(polqual), S(')')])
    if polwithcheck:
        ret.extend([S('WITH CHECK ('), S(polwithcheck), S(')')])
    return compose(ret)

def alter_policy_detail(qualname, tablename, polroles=None, polqual=None, polwithcheck=None):
    ''' Generate ALTER POLICY
    https://www.postgresql.org/docs/current/sql-alterpolicy.html
    >>> as_string(alter_policy_detail('pol', ('public', 't'), polqual='SELECT 1', polwithcheck='1 = 1'))
    'ALTER POLICY pol ON public.t USING ( SELECT 1 ) WITH CHECK ( 1 = 1 )'
    '''
    ret = [S('ALTER POLICY'), I(qualname), S('ON'), I(*tablename)]
    if polroles:
        tos = S(',').join([S(role) for role in polroles])
        ret.append(S('TO'))
        ret.extend(tos)
    if polqual:
        ret.extend([S('USING ('), S(polqual), S(')')])
    if polwithcheck:
        ret.extend([S('WITH CHECK ('), S(polwithcheck), S(')')])
    return compose(ret)

def comment_policy(qualname, tablename, objcomment):
    '''Generate COMMENT ON POLICY.
    >>> as_string(comment_policy('pol', 'SEQUENCE', 'hi'))
    "COMMENT ON POLICY pol ON SEQUENCE IS 'hi'"
    '''
    return compose([S('COMMENT ON POLICY'), I(qualname), S('ON'), I(tablename), S('IS'), L(objcomment)])

def _collation_opts(opts):
    ret = []
    for k, v in opts.items():
        if k == 'PROVIDER':
            v = {'i': 'icu', 'c': 'libc', 'd': 'libc'}[v]  # noqa: PLW2901
        if not isinstance(v, bool) and v:
            ret.extend([S(k), S('='), I(v), S(',')])
    return ret[:-1]

def create_collation(qualname, opts):
    ''' Generate CREATE COLLATION
    >>> as_string(create_collation(('public', 'coll'), opts=\
        {'LC_COLLATE': 'collate', 'PROVIDER': 'i', 'RULES': None, 'VERSION': None}))
    'CREATE COLLATION public.coll ( LC_COLLATE = collate , PROVIDER = icu )'
    '''
    return compose([S('CREATE COLLATION'), I(*qualname), S('('), *_collation_opts(opts), S(')')])

def alter_collation_version(qualname):
    ''' Generate ALTER COLLATION VERSION
    https://www.postgresql.org/docs/current/sql-altercollation.html
    >>> as_string(alter_collation_version(('public', 'coll')))
    'ALTER COLLATION public.coll REFRESH VERSION'
    '''
    return compose([S('ALTER COLLATION'), I(*qualname), S('REFRESH VERSION')])

def alter_coll_owner(qualname, rolname):
    ''' Generate ALTER COLLATION OWNER TO.
    >>> as_string(alter_coll_owner(('public', 'coll'), 'root'))
    'ALTER COLLATION public.coll OWNER TO root'
    '''
    return compose([S('ALTER COLLATION'), I(*qualname), S('OWNER TO'), I(rolname)])

def create_conversion(qualname, is_default, source_encoding, dest_encoding, function_name):
    ''' Generate CREATE conversion
    >>> as_string(create_conversion(('public', 'conv'), True, 'MULE_INTERNAL', 'KOI8R', ('pg_catalog', 'mic_to_koi8r')))
    "CREATE DEFAULT CONVERSION public.conv FOR 'MULE_INTERNAL' TO 'KOI8R' FROM pg_catalog.mic_to_koi8r"
    '''
    return compose([
        S('CREATE'), S('DEFAULT') if is_default else None, S('CONVERSION'), I(*qualname),
        S('FOR'), L(source_encoding), S('TO'), L(dest_encoding), S('FROM'), I(*function_name)])

def alter_conversion_owner(qualname, rolname):
    ''' Generate ALTER CONVERSION OWNER TO.
    >>> as_string(alter_conversion_owner(('public', 'conv'), 'root'))
    'ALTER CONVERSION public.conv OWNER TO root'
    '''
    return compose([S('ALTER CONVERSION'), I(*qualname), S('OWNER TO'), I(rolname)])

def create_statistics(qualname, tablequal, stxkind=None, cols=None, stxexprs=None):
    ''' Generate CREATE STATISTICS
    https://www.postgresql.org/docs/current/sql-createstatistics.html
    >>> as_string(create_statistics(('public', 'stat'), ('public', 't1'), stxkind='m', cols=['a', 'b']))
    'CREATE STATISTICS public.stat ( mcv ) ON a , b FROM public.t1'
    >>> as_string(create_statistics(('public', 'stat'), ('public', 't1'), stxkind='m', stxexprs=\
        "date_trunc('month', a), date_trunc('day', a)"))
    "CREATE STATISTICS public.stat ( mcv ) ON date_trunc('month', a), date_trunc('day', a) FROM public.t1"
    '''
    ret = [S('CREATE STATISTICS'), I(*qualname)]
    if stxkind:
        ret.append(S('('))
        kind_types = {'d': 'ndistinct', 'f': 'dependencies', 'm': 'mcv'}
        for x in stxkind:
            k = kind_types.get(x)
            ret.extend([S(k), S(',')]) if k else None
        ret.pop() if ret[-1] == S(',') else None
        ret.append(S(')'))
    ret.append(S('ON'))
    if cols:
        ret.extend(S(',').join([S(x) for x in cols]))
    if stxexprs:
        if cols:
            ret.append(S(','))
        ret.append(S(stxexprs))
    ret.extend([S('FROM'), I(*tablequal)])
    return compose(ret)

def alter_owner_generic(qualname, object_type, rolname):
    ''' Generate ALTER object OWNER TO
    >>> as_string(alter_owner_generic(('public', 'stat'), 'STATISTICS', 'me'))
    'ALTER STATISTICS public.stat OWNER TO me'
    >>> as_string(alter_owner_generic(('conv',), 'CONVERSION', 'me'))
    'ALTER CONVERSION conv OWNER TO me'
    '''
    return compose([S('ALTER'), S(object_type), I(*qualname), S('OWNER TO'), I(rolname)])

def alter_statistics_target(qualname, new_target):
    ''' Generate ALTER STATISTICS target
    >>> as_string(alter_statistics_target(('public', 'stat'), 5))
    'ALTER STATISTICS public.stat SET STATISTICS 5'
    '''
    return compose([S('ALTER STATISTICS'), I(*qualname), S('SET STATISTICS'), L(new_target)])

def create_rule(_qualname, ruledef):
    ''' Generate CREATE RULE
    https://www.postgresql.org/docs/current/sql-createrule.html
    >>> as_string(create_rule(('public', 'rule', 't1'), 'CREATE RULE pg_settings_u AS ON UPDATE TO public.t1'))
    'CREATE RULE pg_settings_u AS ON UPDATE TO public.t1'
    '''
    return compose([S(ruledef)])

def comment_rule(qualname, objcomment):
    '''Generate COMMENT ON RULE. '''
    return compose([S('COMMENT ON RULE'), I(qualname[1]), S('ON'), I(*qualname[2:4]), S('IS'), L(objcomment)])

def create_access_method(qualname, amhandler, amtype):
    ''' Generate CREATE ACCESS METHOD
    https://www.postgresql.org/docs/current/sql-create-access-method.html
    >>> as_string(create_access_method('heptree', ('public', 'heptree_handler'), 'i'))
    'CREATE ACCESS METHOD heptree TYPE INDEX HANDLER public.heptree_handler'
    '''
    return compose([
        S('CREATE ACCESS METHOD'), I(qualname), S('TYPE'), S({'t': 'TABLE', 'i': 'INDEX'}[amtype]), S('HANDLER'),
        I(*amhandler)])

def alter_system_set_config(qualname, value):
    ''' Generate ALTER SYSTEM
    https://www.postgresql.org/docs/current/sql-altersystem.html
    >>> as_string(alter_system_set_config('wal_level', 'replica'))
    'ALTER SYSTEM SET wal_level = replica'
    '''
    return compose([S('ALTER SYSTEM SET'), I(qualname), S('='), S(value)])

def alter_system_reset_config(qualname):
    ''' Generate ALTER SYSTEM
    https://www.postgresql.org/docs/current/sql-altersystem.html
    >>> as_string(alter_system_reset_config('wal_level'))
    'ALTER SYSTEM RESET wal_level'
    '''
    return compose([S('ALTER SYSTEM RESET'), I(qualname)])

def reload_conf():
    ''' Generate reload_conf()
    >>> as_string(reload_conf())
    'SELECT pg_reload_conf()'
    '''
    return compose([S('SELECT pg_reload_conf()')])

def create_transform(qualname, from_sql_func, to_sql_func):
    ''' Generate CREATE TRANSFORM
    https://www.postgresql.org/docs/current/sql-createtransform.html
    >>> as_string(create_transform(('hstore', 'plpython3u'), {'name': 'public.hstore_to_plpython', 'args': 'internal'},\
        {'name': 'public.plpython_to_hstore', 'args': 'internal'}))
    'CREATE TRANSFORM FOR hstore LANGUAGE plpython3u ( \
FROM SQL WITH FUNCTION public.hstore_to_plpython ( internal ) , \
TO SQL WITH FUNCTION public.plpython_to_hstore ( internal ) )'
    '''
    return compose([
        S('CREATE TRANSFORM FOR'), I(qualname[0]), S('LANGUAGE'), I(qualname[1]), S('('),
        S('FROM SQL WITH FUNCTION'), S(from_sql_func['name']), S('('), S(from_sql_func['args']), S(')'), S(','),
        S('TO SQL WITH FUNCTION'), S(to_sql_func['name']), S('('), S(to_sql_func['args']), S(')'), S(')')])

def comment_transform(qualname, objcomment):
    '''Generate COMMENT ON TRANSFORM.
    >>> as_string(comment_transform(('hstore', 'plpython3u'), 'hi'))
    "COMMENT ON TRANSFORM FOR hstore LANGUAGE plpython3u IS 'hi'"
    '''
    return compose([
        S('COMMENT ON TRANSFORM FOR'), I(qualname[0]), S('LANGUAGE'), I(qualname[1]), S('IS'), L(objcomment)])
