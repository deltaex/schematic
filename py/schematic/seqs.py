import itertools


def cmp(x, y):
    return (x > y) - (x < y)

def groups(iterable, key=None):
    return itertools.groupby(sorted(iterable, key=key), key)

def batches(generator, batch_size):
    gen = iter(generator)
    return iter(lambda: list(itertools.islice(gen, batch_size)), [])
