import os
import sys

from schematic import env

bugsnag_key = env.get_str('BUGSNAG')
if bugsnag_key:
    import bugsnag

    from schematic import slog
    project_root = env.get_str('ROOT_DIR', os.getcwd())
    scm_version = slog.get_version()
    bugsnag.configure(
        api_key=bugsnag_key, project_root=project_root, app_version=scm_version,
        release_stage=env.get_str('ENV', 'prod'))

    def before_notify(notification):
        notification.meta_data['extraData']['argv'] = sys.argv
        notification.meta_data['extraData']['SCM_VAR'] = env.get_str('SCM_VAR')
        notification.meta_data['extraData']['ROOT_DIR'] = env.get_str('ROOT_DIR')
        exc = notification.exception
        if not (isinstance(exc, OSError) and exc.errno == 24):  # avoid begging error "too many open files"
            notification.meta_data['extraData']['SCM_VERSION'] = scm_version
        slog.critical('unhandled exception: %r, %s, argv: %r', exc, exc, sys.argv, exc_info=True)

    bugsnag.before_notify(before_notify)
if 'AUTO_PUDB' in os.environ:
    import autodebug  # noqa: F401
    os.environ['AUTO_DEBUG'] = 'pudb'

def prompt_yesno(prompt, default=False):
    fmt = '%s (y/N) ' if not default else '%s (Y/n) '
    sys.stdout.write(fmt % prompt)
    sys.stdout.flush()
    answer = sys.stdin.readline().strip().lower()
    return {'y': True, 'n': False}.get(answer, default)
