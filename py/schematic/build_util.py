'''Utilities to support python nix builder scripts.'''

import datetime
import os
import re
import string  # pylint: disable=deprecated-module
import subprocess
import sys
import time

import psutil
import psycopg2

from schematic import env, fs, prompt_yesno, slog

re_pgctl_log = re.compile(r'(?P<time>\d+-\d+-\d+ \d+:\d+:\d+\.\d+).*?(?P<level>\w+):\s*(?P<msg>.*)')

# ID for a shared lock held by concurrent builder scripts, which is polled by dependencies that
# require a restart and must wait for everything else to be done installing.
builders_shared_lock_id = '3948431731'

class ParseError(ValueError):
    pass

class LineIterator:
    r'''
    An iterator over the trimmed, non-empty lines of a file.
    It allows peeking at the next line without consuming it,
    and implements the Iterator protocol for convenience.

    >>> iter = LineIterator(' 1 \n\n2\n   \n\n3  \n4')
    >>> iter.next()
    '1'
    >>> iter.next()
    '2'
    >>> iter.peek()
    '3'
    >>> iter.next()
    '3'
    >>> iter.next()
    '4'
    >>> iter.peek()
    >>> iter.next()

    >>> iter2 = LineIterator(' 1 \n\n2\n   \n\n3  \n4 \n5\n\n\n')
    >>> for line in iter2: print(line)
    1
    2
    3
    4
    5
    '''

    def __init__(self, raw_content):
        self._i = 0
        self._lines = list(filter(
            lambda line: len(line) > 0,
            (line.strip() for line in raw_content.splitlines())
        ))

    def next(self):
        '''Consumes and returns the next line, or `None` if all lines have been consumed.'''
        if self._i >= len(self._lines):
            return None
        self._i += 1
        return self._lines[self._i - 1]

    def peek(self):
        '''Returns the next line *without consuming it*, or `None` if all lines have been consumed.'''
        return self._lines[self._i] if self._i < len(self._lines) else None

    def __iter__(self):
        return self

    def __next__(self):
        if (line := self.next()) is None:
            raise StopIteration
        return line

def mkdir(path):
    try:
        os.makedirs(path)
    except OSError as ex:
        if ex.errno not in (17,):
            raise

def build_setup():
    ''' Call nix initializer which propogates transitive dependencies and initializes env vars. '''
    bash = os.path.join(env.get_str('bash'), 'bin/bash') if env.get_str('bash') else 'bash'
    with subprocess.Popen([bash, '-c', 'source $stdenv/setup && env'], stdout=subprocess.PIPE) as proc:
        for line in proc.stdout:
            (key, _, value) = line.decode('utf8').partition('=')
            os.environ[key] = value
        proc.communicate()

def retry(attempts, exceptions=None, onerror=None):
    '''Decorator to retry a function on error.'''
    import functools  # pylint: disable=import-outside-toplevel

    def outer(func):
        @functools.wraps(func)
        def inner(*args, **kwargs):
            for attempt in range(attempts):
                try:
                    return func(*args, **kwargs)
                except exceptions:
                    if onerror is not None:
                        onerror(*args, **kwargs)
                    if attempt >= attempts - 1:
                        raise
        return inner
    return outer

def acquire_shared_builder_lock(pgs):
    pgs.execute('SELECT pg_advisory_lock_shared(%s);', (builders_shared_lock_id,)).first()

def release_shared_builder_lock(pgs):
    pgs.execute('SELECT pg_advisory_unlock_shared(%s);', (builders_shared_lock_id,)).first()

def is_port_in_use(port):
    '''Check if a port is in use.'''
    out = subprocess.run(['lsof', '-iTCP', '-sTCP:LISTEN', '-nP'], capture_output=True, text=True, check=False)
    return re.search(rf':{port}\b', out.stdout) is not None

def pg_is_ready(basedir, port):
    prog = os.path.join(basedir, 'bin/pg_isready')
    if not os.path.exists(basedir):
        return
    if not os.path.exists(prog):
        return is_port_in_use(port)  # pg_isready was added in postgresql 9.3
    cmd = [prog, '-d', 'postgres', '-p', str(port), '-h', 'localhost']
    try:
        subprocess.check_call(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        return True
    except subprocess.CalledProcessError:
        return False

def poll_pg_isready(basedir, port, duration=10, interval=1):
    ''' Poll for a total of :poll_duration: seconds with interval :poll_interval: until postgresql is ready to
        accept connections. Return boolean indicating success.
    '''
    start = time.time()
    while time.time() - start < duration:
        if pg_is_ready(basedir, port):
            return True
        time.sleep(interval)
    return False

def pg_ctl_fn(basedir: str, mode: str, shutdown_mode: str ='fast', quiet: bool=False, wal_senders: (str | None)=None):
    '''
    Sends a command to a database server via the `pg_ctl` binary.
    :basedir: base directory of the database
    :mode: any mode supported by `pg_ctl` (https://www.postgresql.org/docs/current/app-pg-ctl.html)
    :shutdown_mode: either `smart`, `fast` or `immediate` (only applies when `mode` is `stop` or `restart`)
    :quiet: whether to suppress the resulting output
    :wal_senders: how to handle WAL sender processes on stop or restart
        - `graceful` waits for all replication clients to catch up
        - `fast` terminates replication without waiting
        - `None` selects dynamically based on :shutdown_mode:
    '''
    pg_ctl = os.path.join(basedir, 'bin/pg_ctl')
    if not os.path.exists(pg_ctl):
        print('error: bin/pg_ctl not found in %s' % (basedir), file=sys.stderr)
        if os.path.exists(os.path.join(basedir, 'default.nix')):
            print('Perhaps you meant to pass a path from %s' % env.get_str('SCM_PG', '~/var/pg'), file=sys.stderr)
        sys.exit(19)

    data_dir = os.path.join(basedir, 'data')
    pg_ctl_log = os.path.join(data_dir, 'pg_ctl.log')
    pidfile = os.path.join(data_dir, 'postmaster.pid')
    pid = get_pid(pidfile)

    # Prevent an error by pg_ctl when trying to start a database that has an existing pidfile.
    if pid and mode == 'start':
        slog.warn2(f'Database seems to be already running with PID {pid}')
        slog.warn2(f'hint: if not running, remove obsolete pidfile ({pidfile}) and try again.')
        return

    # Use a very generous timeout to allow pg_ctl to do the pidfile polling for us.
    timeout = 60 * 60 * 24  # 1 day in seconds
    cmd = [pg_ctl, mode, '-D', data_dir, '-l', pg_ctl_log, '--wait', '-t', str(timeout)]
    if mode in ['stop', 'restart']:
        stop_wal_senders(shutdown_mode, wal_senders, basedir)
        cmd.extend(['-m', shutdown_mode])

    success = True
    utcnow = datetime.datetime.utcnow()
    try:
        if quiet:
            subprocess.check_call(cmd, stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL)
        else:
            subprocess.check_call(cmd, stdout=2)
    except subprocess.CalledProcessError:
        success = False

    if not success and mode in ('start', 'restart', 'reload'):
        # Wait a couple of seconds for the error info to be flushed to the log
        time.sleep(2)
        log_errors = read_pgctl_log_errors(pg_ctl_log, utcnow)
        slog.error2(slog.red(f'ERROR: database {mode} failed'))
        if log_errors:
            slog.error2('pg_ctl context:\n%s', '\n'.join(log_errors))
        slog.error2('Check the log for additional details: %s', pg_ctl_log)
        sys.exit(20)

def ensure_pg_running(basedir, port, poll_duration=60 * 60 * 24, poll_interval=1):
    ''' Start postgresql if it's not already running. Poll for a total of :poll_duration: seconds with interval
        :poll_interval: until it's up and responding.
    '''
    if not pg_is_ready(basedir, port):
        pg_ctl_fn(basedir, 'start', quiet=(not env.get_bool('SCM_VERBOSE')))
    poll_pg_isready(basedir, port, duration=poll_duration, interval=poll_interval)

def get_pid(pidfile):
    if data := fs.try_read(pidfile):
        return int(data.splitlines()[0].strip())
    return None

def stop_wal_senders(shutdown_mode, wal_senders_mode, base_dir):
    if wal_senders_mode == 'fast' or (shutdown_mode != 'smart' and wal_senders_mode is None):
        pidfile = os.path.join(base_dir, 'data/postmaster.pid')
        for child in psutil.Process(get_pid(pidfile)).children(recursive=True):
            try:
                if 'walsender' in ''.join(child.cmdline()):
                    child.kill() if shutdown_mode == 'immediate' else child.terminate()
            except psutil.NoSuchProcess:  # Process already dead
                continue

def read_pgctl_log_errors(log_path, utc_since):
    def match_line(line):
        if m := re_pgctl_log.match(line):
            log_time = datetime.datetime.fromisoformat(m.group('time'))
            level = m.group('level').lower()
            if log_time >= utc_since and level in ('error', 'fatal', 'panic', 'hint'):
                return '\t' + m.group('msg')
        return None
    lines = fs.try_read(log_path, '').splitlines()
    return list(filter(None, map(match_line, lines)))

def install_basedir_files(basedir, *derivs):
    any_changes = False
    for deriv in filter(None, derivs):
        if not os.path.exists(deriv):
            continue
        if deriv.endswith('-basefiles'):
            path = deriv
        elif os.path.exists(os.path.join(deriv, 'basefiles')):
            path = os.path.join(deriv, 'basefiles')
        else:
            continue
        print(f'installing {path!r} -> {basedir!r}', file=sys.stderr)
        # TODO: more sophisticated policy about copying/overwriting files into database
        maybe_change = subprocess.check_output(['rsync', '--itemize-changes', '-rlc', '%s/' % path, basedir])
        any_changes = any_changes or maybe_change
    if any_changes:
        # TODO: dirty hack to let pg_upgrade advance. Keep in mind that the server  # pylint: disable=fixme
        # key file can be specified via the config parameter ss_key_file, so a more
        # general solution is needed
        server_key_file = os.path.join(basedir, 'data/server.key')
        if os.path.isfile(server_key_file) and (os.stat(server_key_file).st_mode & 0o777) > 0o400:
            os.chmod(server_key_file, 0o400)
        if not env.get_bool('SCM_PG_UPGRADE'):
            # We shouldn't start the database if we're in the middle of a binary upgrade
            pg_ctl_fn(basedir, 'reload', quiet=True)

def install_buildinput_files():
    install_basedir_files(
        env.get_str('basedir'), env.get_str('out'), env.get_str('basefiles'), *env.get_array('buildInputs'))

def onelinesql(text):
    return re.sub(r'\s+', ' ', text)

def log_msg_upgrade_sql_text(text, prefix='applying'):
    # truncate: unfortunately shutil.get_terminal_size doesn't get the terminal width inside the env that nix sets up
    import shutil  # pylint: disable=import-outside-toplevel
    width = shutil.get_terminal_size().columns or 152
    line = ' '.join(filter(None, [
        prefix,
        f'<{env.get_str("name")}-{env.get_str("guid")}>' if env.get_str('guid') else '',
        onelinesql(text)]))
    print(line[:width], file=sys.stderr)

def log_msg_upgrade_sql(upgrade_sql, prefix='applying'):
    log_msg_upgrade_sql_text(fs.try_read(upgrade_sql, default=''), prefix=prefix)

def is_empty_sql(text):
    if not text.strip():
        return True
    empty_line = lambda l: not l or not l.strip() or l.lstrip().startswith('--')
    if all(empty_line(l) for l in text.splitlines()):
        return True
    return False

def apply_sql_text(pg, text, attempts=3, retry_patience=30):
    ''' Execute sql statement.
        :attempts: retry attempts for some errors related to concurrency/locks
        :retry_patience: only retry if this budget (in seconds) is not exceeded to avoid retrying expensive operations
    '''
    assert attempts >= 1, 'invalid argument attempts = %r' % attempts
    autocommit = env.get_bool('autocommit')
    if autocommit:
        attempts = 1  # these aren't safe to retry, better to have manual intevention
    if is_empty_sql(text):
        return
    start = time.time()
    for attempt in range(1, attempts + 1):
        if attempt > 0 or autocommit:  # autocommit revisions are frequent cause of race conditions, space them out
            import random  # pylint: disable=import-outside-toplevel
            time.sleep(random.random())  # helps mitigate livelock race conditions
        try:
            return pg.execute(text)
        except (psycopg2.errors.InternalError, psycopg2.errors.DeadlockDetected) as ex:  # pylint: disable=no-member
            if retry_patience and time.time() - start >= retry_patience:
                raise
            if attempt >= attempts:
                raise
            if isinstance(ex, psycopg2.errors.InternalError) and 'tuple concurrently updated' in (ex.pgerror or ''):  # pylint: disable=no-member
                pg.rollback() if not autocommit else 0
                continue  # happens when removing a postgresql catalog record that was concurrently updated
            if isinstance(ex, psycopg2.errors.DeadlockDetected):  # pylint: disable=no-member
                pg.rollback() if not autocommit else 0
                continue  # can happen with CREATE INDEX CONCURRENTLY
            raise

def apply_sql_file(pg, upgrade_sql, attempts=3, retry_patience=30):
    ''' Execute contents of a sql file.
        :attempts: retry attempts for some errors related to concurrency/locks
        :retry_patience: only retry if this budget (in seconds) is not exceeded to avoid retrying expensive operations
    '''
    log_msg_upgrade_sql(upgrade_sql, prefix='applying')
    return apply_sql_text(
        pg, open(upgrade_sql, encoding='utf8').read(), attempts=attempts, retry_patience=retry_patience)

def write_meta_json():
    import json  # pylint: disable=import-outside-toplevel

    from schematic import dates  # pylint: disable=import-outside-toplevel
    with open(os.path.join(env.get_str('basedir'), 'meta.json'), 'w') as metajson:
        metajson.write(json.dumps({
            'guid': env.get_str('guid'),
            'basedir': env.get_str('basedir'),
            'created': dates.datetime_to_unix(datetime.datetime.utcnow()),
            'user': env.get_str('user'),
            'name': env.get_str('name'),
            'dbname': env.get_str('dbname'),
            'port': int(env.get_str('port')),
            'istemp': env.get_bool('SCM_ISTEMP'),
        }))

def install_basedir_postgresql_files():
    basedir = env.get_str('basedir')
    pgdir = env.get_str('postgresql')
    if server_needs_major_upgrade(basedir, pgdir, permission='yes'):
        # Avoid overwriting binaries if there's a newer major or beta version available
        return
    for subdir in 'bin include lib share'.split():
        subprocess.check_call(['rsync', '-arc', os.path.join(pgdir, subdir), basedir])
        subprocess.check_call(['chmod', '-R', '+w', os.path.join(basedir, subdir)])

def parse_postgresql_drv_version(drv=None):
    '''
    >>> parse_postgresql_drv_version('/nix/store/xm8p6bxn1zh444sgbd3zi63sgmhd6fm0-postgresql-9.5.25.drv')
    '9.5.25'
    >>> parse_postgresql_drv_version('/nix/store/5af87d97gx6kjakxshrglvnmjl2y76nm-postgresql-16.2')
    '16.2'
    >>> parse_postgresql_drv_version('/nix/store/a7p09mmhffrkpwnjli96nc618rlf6xn2-postgresql-14beta1')
    '14beta1'
    '''
    drv = drv or env.get_str('postgresql')
    if match := re.search(r'postgresql-(?P<version>\d+(\.\d+)?(\.\d+)?(alpha\d*)?(beta\d*)?(rc\d*)?)', drv):
        return match.group('version')

def split_postgresql_version(version):
    '''
    >>> split_postgresql_version(None)
    []
    >>> split_postgresql_version('16.2')
    [16, 2]
    >>> split_postgresql_version('9.5.25')
    [9, 5, 25]
    >>> split_postgresql_version('14beta1')
    [14, 'beta', 1]
    '''
    if not version:
        return []
    ret = []
    digitbuf = ''
    letterbuf = ''
    for c in version + '.':
        if c in string.digits:
            if letterbuf:
                ret.append(letterbuf)
                letterbuf = ''
            digitbuf += c
        elif c in string.ascii_letters:
            if digitbuf:
                ret.append(int(digitbuf))
                digitbuf = ''
            letterbuf += c
        elif c == '.':
            if digitbuf:
                ret.append(int(digitbuf))
                digitbuf = ''
            elif letterbuf:
                ret.append(letterbuf)
                letterbuf = ''
        else:
            raise ValueError(f'unexpected char {c} in version string {version}')
    return ret

def alter_system_set(pg, key, value):
    pg.execute(f'ALTER SYSTEM SET {key} = {value};')

def show_pg_param(pg, key):
    return pg.execute('SHOW %s;' % key).scalar()

def server_needs_major_upgrade(curr_pg_dir, target_pg_dir, permission='no'):
    '''Determine if the server needs to be upgraded.'''
    if permission not in ('yes', 'no', 'ask'):
        raise ValueError("permission must be either 'yes', 'no' or 'ask'")  # pylint: disable=invalid-string-quote
    if not os.path.isdir(os.path.join(curr_pg_dir, 'bin')):
        # <curr_pg_dir>/bin doesn't exist, probably first installation
        return False
    # 'SHOW server_version_num' can be used to retrieve the numerical version
    # for appropriate comparisons. Although, it doesn't seem available without
    # the server running, so let's fallback to pg_config --version and parse
    # the version string to avoid starting the database for retrieving just a
    # single value.
    #
    # See: https://pgpedia.info/s/server_version_num.html to reliably compare versions
    curr_version = pg_config_version(curr_pg_dir)
    target_version = pg_config_version(target_pg_dir)
    return upgrade_decision(curr_version, target_version, permission)

def pg_config_version(basedir):
    '''Return PostgreSQL version number using pg_config utility. Eg "15.1".'''
    return subprocess.check_output([os.path.join(basedir, 'bin/pg_config'), '--version'], text=True).strip().split()[1]

def upgrade_decision(cur, tgt, permission='yes'):
    '''
    >>> assert not upgrade_decision('14.1', '14.1')
    >>> assert upgrade_decision('14.1', '15.1')
    >>> assert not upgrade_decision('15.0', '14.5')
    >>> assert upgrade_decision('15beta1', '15beta2')
    >>> assert not upgrade_decision('15beta2', '15beta1')
    >>> assert upgrade_decision('15beta2', '16beta1')
    >>> assert not upgrade_decision('16beta2', '15beta1')
    >>> assert upgrade_decision('15beta1', '16.0')
    '''
    if cur == tgt:
        return False
    beta_in_curr = 'beta' in cur
    beta_in_target = 'beta' in tgt
    curr_major, curr_minor = map(int, cur.split('beta' if beta_in_curr else '.'))
    target_major, target_minor = map(int, tgt.split('beta' if beta_in_target else '.'))
    if curr_major > target_major:
        return False
    if curr_major == target_major:
        if ((not beta_in_curr and not beta_in_target)  # No major upgrade involved
               or (not beta_in_curr and beta_in_target)):  # Avoid regressing to a beta version
            return False
        if beta_in_curr and beta_in_target:
            if curr_minor >= target_minor:
                return False  # Avoid regressing to an older beta version
    oldest_supported_major = 11
    if curr_major <= oldest_supported_major:
        slog.info2('Postgres upgrade from version %s not supported' % cur)
        return False
    if target_major <= oldest_supported_major:
        slog.info2('Postgres upgrade to version %s not supported' % tgt)
        return False
    if (permission == 'no'
        or permission not in ('yes', 'ask')
        or (permission == 'ask' and not prompt_yesno(
                f'Upgrade Postgres from version {cur} to version {tgt}?', default=False))):
        return False
    return True

def fmt_header(guid, name, comment, depends):
    ''' Format a section header.
    >>> print(fmt_header('C1MKM4UFIN2B4S3X', 'pghba01', 'hello', ['C103QA2MJ7CI7BOK']))
    # <pghba01-C1MKM4UFIN2B4S3X> depends (C103QA2MJ7CI7BOK)
    # hello
    >>> print(fmt_header('C1MKM4UFIN2B4S3X', 'pghba01', 'world', []))
    # <pghba01-C1MKM4UFIN2B4S3X>
    # world
    >>> print(fmt_header('C1MKM4UFIN2B4S3X', 'pghba01', None, ['C103QA2MJ7CI7BOK']))
    # <pghba01-C1MKM4UFIN2B4S3X> depends (C103QA2MJ7CI7BOK)
    >>> print(fmt_header('C1MKM4UFIN2B4S3X', 'pghba01', '', []))
    # <pghba01-C1MKM4UFIN2B4S3X>
    >>> print(fmt_header('C1MKM4UFIN2B4S3X', 'pghba01', 'foo \\n bar', []))
    # <pghba01-C1MKM4UFIN2B4S3X>
    # foo _ bar
    '''
    ret = f'# <{name}-{guid}>'
    if depends:
        ret += ' depends (%s)' % ' '.join(sorted(depends))
    if comment:
        ret += '\n# %s' % re.sub(r'\n', '_', comment)
    return ret

def fmt_footer(guid, name):
    ''' Format a section footer.
    >>> print(fmt_footer('C1MKM4UFIN2B4S3X', 'pghba01'))
    # </pghba01-C1MKM4UFIN2B4S3X>
    '''
    return f'# </{name}-{guid}>'

def append_newline(text):
    return '%s\n' % text if not text.endswith('\n') else text

def parse_header(line):
    ''' Parse a section header into its name, guid, and its depencencies' guids.
    >>> parse_header('# <pghba02-C1SMJSALSU559R9H>')
    ('pghba02', 'C1SMJSALSU559R9H', ())
    >>> parse_header('# <pghba02-C1SMJSALSU559R9H> depends (C1MKM4UFIN2B4S3X C103QA2MJ7CI7BOK)')
    ('pghba02', 'C1SMJSALSU559R9H', ('C1MKM4UFIN2B4S3X', 'C103QA2MJ7CI7BOK'))
    >>> parse_header('# <pghba00-C103QA2MJ7CI7BOK> depends (C103QA2MJ7CI7BOK)')
    ('pghba00', 'C103QA2MJ7CI7BOK', ('C103QA2MJ7CI7BOK',))
    >>> parse_header('# <lan-pghba-C0ILCOBY3I22I7D6>')
    ('lan-pghba', 'C0ILCOBY3I22I7D6', ())
    >>> parse_header('# <lan_pghba-F2ILCOBY3I22I7D6>')
    ('lan_pghba', 'F2ILCOBY3I22I7D6', ())
    '''
    reg = re.compile(r'^# <(?P<name>[\w_-]+)\-(?P<guid>[A-Z0-9]+)>( depends \((?P<deps>[A-Z0-9 ]+)\))?$')
    if match := reg.match(line.strip()):
        deps = match.group('deps')
        return (match.group('name'), match.group('guid'), tuple(deps.split(' ')) if deps else ())

def parse_footer(line):
    ''' Parse a section footer into its guid.
    >>> parse_footer('# </pghba02-C1SMJSALSU559R9H>')
    'C1SMJSALSU559R9H'
    >>> parse_footer('# </lan-pghba_2-C0ILCOBY3I22I7D6>')
    'C0ILCOBY3I22I7D6'
    '''
    if match := re.match(r'^# </[\w_-]+\-(?P<guid>[A-Z0-9]+)>$', line.strip()):
        return match.group('guid')

def cleanup_old_sections(conf_filepath, guids_filepath):
    '''
    Removes configuration sections from `conf_filepath` generated by packages
    whose GUIDs don't appear listed in `guids_filepath`.

    This is done to clean up autogenerated sections by packages that have been
    removed from a database's dependencies.
    '''
    guids_raw = fs.try_read(guids_filepath, default='')
    guids = {line.strip() for line in guids_raw.splitlines()}

    with open(conf_filepath, 'r', encoding='utf8') as fp:
        old_lines = fp.readlines()
    new_lines = []

    adding = True
    for line in old_lines:
        if adding:
            new_lines.append(line)

        if header := parse_header(line):
            _, guid, _ = header
            adding = guid in guids
            if not adding:
                new_lines.pop()
        elif parse_footer(line):
            adding = True

    fs.write(conf_filepath, ''.join(new_lines))
    fs.try_unlink(guids_filepath)
