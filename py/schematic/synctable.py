import multiprocessing
import time

from psycopg2.sql import SQL, Composed, Identifier

from schematic import pgcn, pgcopy, pgingest, pgsub, seqs, slog


def stream_initcopy(pg, namespace, tablename, sql_where, include_cols, batch_size=1000):
    query = SQL('SELECT {cols} FROM {table}').format(
        cols=SQL(', ').join([Identifier(c) for c in include_cols]),
        table=Identifier(namespace, tablename))
    if sql_where:
        query = Composed([query, SQL(' WHERE '), SQL(sql_where)])
    slog.info('initial export: %s', query.as_string(pg.cur))
    yield from seqs.batches(pgcopy.read_via_fs(pg, query.as_string(pg.cur)), batch_size)

def synctable_proc(pguri, replication_role, column_defs, include_cols, table, queue, is_done, onconflict):
    decodecols = include_cols.values()
    with pgcn.connect(pguri) as pg:
        pgingest.init_replication_settings(pg, replication_role)
        while not is_done.is_set() or not queue.empty():
            try:
                batch = queue.get(timeout=.1)
            except multiprocessing.queues.Empty:  # type: ignore[reportGeneralTypeIssues]
                continue
            if not batch:
                continue
            pgingest.upsert_batch(pg, table, [
                pgsub.decode_to_dict(decodecols, pgcopy.decode_record(x), pg.cur) for x in batch],
                column_defs, onconflict)
            pg.commit()

def get_key_cols(pg, namespace, tablename):
    return [x.column_name for x in pg.execute('''
        SELECT kcu.column_name
        FROM information_schema.table_constraints AS tc
        JOIN information_schema.key_column_usage AS kcu
            ON tc.constraint_name = kcu.constraint_name AND tc.table_schema = kcu.table_schema
        WHERE
            tc.constraint_type = 'PRIMARY KEY'
            AND tc.table_schema = %s
            AND tc.table_name = %s;
    ''', (namespace, tablename)).fetchall()]

def ingress_initcopy(
        pg, namespace, tablename, stream, column_defs, include_cols, replication_role, onconflict,
        parallelism=16, status_interval: int=5 * 60):
    ops = 0
    lastlog = None
    table = {
        'namespace': namespace, 'tablename': tablename,
        'key': get_key_cols(pg, namespace, tablename)}
    pg.commit()
    is_done = multiprocessing.Event()
    queue = multiprocessing.Queue()
    procs = []
    for i in range(parallelism):
        p = multiprocessing.Process(
            name=f'scm synctable {i}/{parallelism} {namespace}.{tablename}',
            target=synctable_proc,
            args=(pg.pguri, replication_role, column_defs, include_cols, table, queue, is_done, onconflict))
        p.start()
        procs.append(p)
    def logstatus():
        slog.info2(f'initcopy {namespace}.{tablename} {ops:10d} inserts')
    logstatus()
    for copybatch in stream:
        queue.put(copybatch)
        while queue.qsize() >= parallelism:
            time.sleep(.1)
        ops += len(copybatch)
        now = time.time()
        if not lastlog or now - lastlog >= status_interval:
            logstatus()
            lastlog = now
    is_done.set()
    for p in procs:
        p.join()
        assert p.exitcode == 0, 'got non-zero exitcode from synctable_proc: %r' % p.exitcode
        p.close()
    logstatus()

def sync(pg_src, pg_dst, tablename, sql_where, parallelism, replication_role='replica', onconflict='do update'):
    assert onconflict in ('do update', 'do nothing'), 'unexpected value for onconflict: %r' % onconflict
    lock = pg_dst.execute('SELECT pg_try_advisory_lock(-41639395, hashtext(%s));', (tablename,)).scalar()
    if not lock:
        slog.info2(f'there is another running process syncing table {tablename}')
        raise SystemExit(107)
    pg_dst.commit()  # prevent holding idle txn
    namespace, tablename = (tablename if '.' in tablename else f'public.{tablename}').split('.')
    column_defs = pgingest.get_column_defs(pg_dst)
    include_cols = pgsub.get_columns(pg_src, namespace, tablename)
    if (namespace, tablename) not in column_defs:
        slog.info2(f'table {namespace}.{tablename} does not exist on target')
        raise SystemExit(45)
    for rm_col in include_cols.keys() - column_defs[(namespace, tablename)].keys():
        del include_cols[rm_col]  # exclude columns not in the destination
    stream = stream_initcopy(pg_src, namespace, tablename, sql_where, include_cols)
    ingress_initcopy(
        pg_dst, namespace, tablename, stream, column_defs, include_cols, replication_role, onconflict,
        parallelism=parallelism)
