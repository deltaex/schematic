import json
import os
import re
import sys

from schematic import env, fs, pgcn, settings_util, slog
from schematic.build_pghba import topographic_sort_sections
from schematic.build_util import (
    LineIterator, ParseError, acquire_shared_builder_lock, fmt_footer, fmt_header,
    install_buildinput_files, parse_footer, parse_header)

re_conf_line = re.compile(r'^\s*(?P<name>[\w\.]+)\s*=?\s*(?P<value>.*?)(?P<comment>(?<!\\)#.*)?$')
re_number_or_amount = re.compile(r'^-?(\d+)?(\.\d+)?([kmgt]?b|[um]?s|min|h|d)?$', re.IGNORECASE)
conf_lock_id = '188214120735504968'

def conf_section(name, guid, comment, depends, settings):
    return {'name': name, 'guid': guid, 'comment': comment, 'depends': depends, 'settings': settings}

def conf_setting(name, value):
    return {'name': name, 'value': value}

###############################################################################
# Validation functions

def check_config_errors(pg, conf_filepath):
    '''
    Queries the database for configuration errors and displays them in stderr if they exist.
    Returns a boolean indicating whether any errors were found.
    '''
    if not (errors := get_config_errors(pg)):
        return False

    path = conf_filepath or 'generated postgresql.conf'
    slog.error2(slog.red(f'ERROR: invalid configuration in {path}'))
    lines = fs.try_read(conf_filepath, '').splitlines()

    for err in errors:
        if err.sourceline:
            slog.error2(f'  {lines[err.sourceline - 1]}')
        slog.error2(f'%s {err.error}', '  ^' if err.sourceline else '')
    return True

def get_config_errors(pg):
    '''
    Queries the database for configuration errors, filtering out those
    that are due to a setting that has changed and needs a restart.
    '''
    return pg.execute('''
        SELECT * FROM pg_file_settings pgfs
            WHERE pgfs.error IS NOT NULL
            AND pgfs.error NOT LIKE '%cannot be changed without restarting%'
            AND NOT EXISTS (
                SELECT * FROM pg_settings pgs WHERE pgfs.name = pgs.name AND pgs.pending_restart
            )
    ''').all()

###############################################################################
# Serializing functions

def update_config_file(old_content):
    '''
    Updates the database's `postgresql.conf` file with the `scm.pgconf` object being processed.

    Returns the contents for the new configuration file.
    '''
    new_section = read_env_section()
    settings_util.validate_section(new_section)

    default_content, sections = parse_postgresql_conf(old_content)
    sections[new_section['guid']] = new_section

    sorted_guids = topographic_sort_sections(sections)
    sorted_guids.reverse()  # Dependees first and dependants last

    new_content = default_content
    for guid in sorted_guids:
        new_content += fmt_section(sections[guid])

    return new_content

def fmt_section(sect):
    ret = fmt_header(sect['guid'], sect['name'], sect['comment'], sect['depends']) + '\n'
    for setting in sect['settings']:
        ret += fmt_setting(setting) + '\n'
    ret += fmt_footer(sect['guid'], sect['name']) + '\n\n'
    return ret

def fmt_setting(setting):
    quoted_val = quote_value(setting['value'])
    return f'{setting["name"]} = {quoted_val}'

def quote_value(value):
    '''
    Adds single quotes around string values (i.e. everything non-numerical or time/space amount.)
    as long as they're not already quoted.

    >>> quote_value('hello')
    "'hello'"
    >>> quote_value("'hello'")
    "'hello'"
    >>> quote_value('13.37')
    '13.37'
    >>> quote_value('550MB')
    '550MB'
    >>> quote_value('120ms')
    '120ms'
    >>> quote_value('a123')
    "'a123'"
    >>> quote_value('/some/path')
    "'/some/path'"
    >>> quote_value(123)
    123
    '''
    if not isinstance(value, str):
        return value
    if value.startswith('\'') or re_number_or_amount.match(value):
        return value
    return f'\'{value}\''

###############################################################################
# Parsing functions

def parse_postgresql_conf(raw_content):
    '''
    Parses the content of a postgresql.conf file.

    Returns a tuple with the default content before the custom setting sections,
    and a dict `{guid: section_data}` with the settings created by pgconfs packages.
    '''
    lines_iter = LineIterator(raw_content)
    sections = {}
    default_content = ''

    while line := lines_iter.next():
        header_data = parse_header(line)
        if header_data is None:
            default_content += line + '\n'
            continue

        name, guid, deps = header_data
        sections[guid] = parse_section(lines_iter, name, guid, deps)

    return default_content, sections

def read_env_section():
    '''
    Creates a new settings section from the current `scm.pgconf` object,
    whose properties are read via environment variables.
    '''
    name = env.get_str('name')
    guid = env.get_str('guid')
    comment = env.get_str('comment')
    depends = [x for x in (env.get_array('transDepGuids') or []) if x.startswith('C0')]
    settings = parse_nix_settings(env.get_str('settings', '{}'))
    settings = [conf_setting(name, value) for name, value in settings.items()]
    return conf_section(name, guid, comment, depends, settings)

def parse_nix_settings(raw_settings):
    error_msg = ('The "settings" attribute of a pgconf package should be an attribute set '
                 'of the form { setting_name = value; }')
    try:
        data = json.loads(raw_settings)
    except json.decoder.JSONDecodeError:
        raise ParseError(error_msg)  # pylint: disable=raise-missing-from

    if not isinstance(data, dict):
        raise ParseError(error_msg)

    # Handle settings names with dots in them, which are parsed by nix as sub-sets
    return dict(flatten_setting(k, v) for k, v in data.items())

def flatten_setting(name, value):
    '''
    >>> flatten_setting('some_key', 'some_value')
    ('some_key', 'some_value')
    >>> flatten_setting('some_key', {'prop': 'value'})
    ('some_key.prop', 'value')
    >>> flatten_setting('some_key', {'prop': {'prop2': 'value'}})
    ('some_key.prop.prop2', 'value')
    '''
    k = name
    v = value
    while isinstance(v, dict):
        a, b = next(iter(v.items()))
        k = f'{k}.{a}'
        v = b
    return (k, v)

def parse_section(lines_iter, name, guid, deps):
    '''Parses an auto-generated config section into a conf_section dict.'''
    comment = None
    settings = []
    # The header has been consumed, check if the next line is the section's comment and consume it if so
    next_line = lines_iter.peek()
    if next_line.startswith('#') and not next_line.startswith('# </'):
        comment = lines_iter.next()[1:].strip()
    # Parse the section's body
    while line := lines_iter.next():
        # Is this the section's end?
        if footer_guid := parse_footer(line):
            if footer_guid != guid:
                raise parse_error(f'Mismatched section end: got {footer_guid} but expected {guid}')
            return conf_section(name, guid, comment, deps, settings)
        # Otherwise, it must be a setting
        if not (setting := parse_setting(line)):
            raise parse_error(f'Invalid setting line inside config section: "{line}"')
        settings.append(setting)
    # If the whole file was consumed without finding a matching footer, panic
    raise parse_error(f'Reached end-of-file without finding a section end for {guid}')

def parse_setting(line):
    '''
    Parses a postgresql.conf setting line into a conf_setting dict.

    >>> parse_setting('setting1  =  some_value123 # a comment')
    {'name': 'setting1', 'value': 'some_value123'}
    >>> parse_setting(' max_memory  500GB  ')
    {'name': 'max_memory', 'value': '500GB'}
    >>> parse_setting("complicated = 'val; ''escaped'' data'    # some comments...")
    {'name': 'complicated', 'value': "'val; ''escaped'' data'"}
    >>> parse_setting("archive_mode = 'off'")
    {'name': 'archive_mode', 'value': "'off'"}
    >>> parse_setting("setting = 'string with \\# comment character'")
    {'name': 'setting', 'value': "'string with # comment character'"}
    '''
    if m := re_conf_line.match(line):
        name = m.group('name')
        value = m.group('value').replace('\\#', '#').strip()
        return conf_setting(name, value)

def parse_error(msg):
    msg = 'Error parsing postgresql.conf:\n' + msg
    return ParseError(msg)

###############################################################################

def main():
    if env.get_bool('SCM_PG_UPGRADE'):
        # If we're upgrading postgresql versions, skip as there's no database
        # created. But install needed files anyways.
        install_buildinput_files()
        return

    with pgcn.connect(env.get_str('pguri'), autocommit=True) as pg:
        acquire_shared_builder_lock(pg)
        # obtain lock to avoid consistency issues with concurrent edits
        pg.execute('SELECT pg_advisory_lock(%s);', (conf_lock_id,)).first()

        install_buildinput_files()
        conf_filepath = os.path.join(env.get_str('basedir'), 'data/postgresql.conf')
        guid = env.get_str('guid')

        # Remove the problematic tmp config from a previous run
        tmp_filepath = conf_filepath + '.tmp'
        fs.try_unlink(tmp_filepath)

        old_content = fs.try_read(conf_filepath, default='')
        new_content = update_config_file(old_content)

        if old_content != new_content:
            name = env.get_str('name')
            slog.info2(f'<{name}-{guid}> -> {conf_filepath}')
            fs.write(conf_filepath, new_content)

        if check_config_errors(pg, conf_filepath):
            if not env.get_bool('SCM_ISTEMP'):
                slog.warn2('Restoring previous configuration to leave the file in a valid state.\n'
                           'hint: The problematic config file is kept here so you can inspect it;'
                           ' it will be removed on the next build:\n%s', tmp_filepath)
                fs.write(conf_filepath, old_content)
                fs.write(tmp_filepath, new_content)
            sys.exit(11)

        # Add this pgconf to the log file to have build_database clean up old sections
        # from previous pgconfs that have been unlinked from the dependency chain.
        dotfile = os.path.join(env.get_str('basedir'), '.applied_pgconfs')
        fs.write(dotfile, f'{guid}\n', mode='a')

        pg.execute('SELECT pg_reload_conf();')

if __name__ == '__main__':
    main()
