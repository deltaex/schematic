import os


def get_str(key: str, default: str | None = None) -> str | None:
    return os.environ[key].rstrip('\n') if key in os.environ else default

def get_int(key: str, default: int | None = None) -> int | None:
    try:
        return int(os.environ[key].rstrip('\n'))
    except (TypeError, KeyError):
        return default

def parse_bool(val: str | None) -> bool:
    return {'1': True, '': False, '0': False, 'True': True, 'False': False,
            'true': True, 'false': False, 'yes': True, 'no': False, None: False}[val]

def get_bool(key: str, default: bool = False) -> bool:  # noqa: FBT001
    if (val := get_str(key)) is not None:
        return parse_bool(val)
    return default

def unquote(text: str | None) -> str | None:
    '''
    >>> unquote('abc')
    'abc'
    >>> unquote('"abc"')
    'abc'
    >>> unquote('"abc')
    '"abc'
    '''
    return text[1:-1] if text and text[0] == text[-1] == '"' else text

def parse_bash_array(text: str | None) -> list[str] | None:
    '''
    >>> parse_bash_array(None)
    >>> parse_bash_array('')
    []
    >>> parse_bash_array('a b')
    ['a', 'b']
    >>> parse_bash_array('a b "c d"')
    ['a', 'b', 'c d']
    >>> parse_bash_array('"a b" c "d e" f')
    ['a b', 'c', 'd e', 'f']
    >>> parse_bash_array('  a  b  ')
    ['a', 'b']
    '''
    import re  # pylint: disable=import-outside-toplevel
    if text is None:
        return
    if not text:
        return []
    pattern = r'"([^"\\]*(?:\\.[^"\\]*)*)"|\S+'
    return list(filter(None, [unquote(match.group(1) or match.group(0)) for match in re.finditer(pattern, text)]))

def get_array(key: str) -> list[str] | None:
    return parse_bash_array(get_str(key)) or []
