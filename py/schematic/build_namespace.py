import sys

from psycopg2.sql import SQL, Identifier, Literal

from schematic import env, pgcn
from schematic.build_util import acquire_shared_builder_lock


def create_namespace(pg, name):
    print(f'namespace {name!r} create', file=sys.stdout)
    return pg.execute(SQL('''
        CREATE SCHEMA IF NOT EXISTS {name};
    ''').format(name=Identifier(name)))

def set_namespace_comment(pg, name, comment):
    existing_comment = pg.execute('''
        SELECT shobj_description(oid, 'pg_namespace')
        FROM pg_catalog.pg_namespace
        WHERE nspname = %s;
    ''', (name,)).scalar()
    if existing_comment != comment:
        print(f'namespace {name!r} comment {comment!r}', file=sys.stdout)
        return pg.execute(SQL('COMMENT ON SCHEMA {name} IS {comment};').format(
            name=Identifier(name), comment=Literal(comment)))

def get_database_search_path(pg, database_oid):
    settings = pg.execute('SELECT * FROM pg_db_role_setting WHERE setdatabase = %s;', (database_oid,)).first()
    search_path = [x for x in settings.setconfig if x.startswith('search_path=')] if settings else []
    return search_path[0].partition('=')[2] if search_path else '"$user", public'

def _clean_namespace(ns):
    '''
    >>> _clean_namespace(None)
    >>> _clean_namespace('')
    >>> _clean_namespace('public')
    'public'
    >>> _clean_namespace('hello operator')
    'hello operator'
    >>> _clean_namespace('"hello operator"')
    'hello operator'
    >>> _clean_namespace('hello "operator"')
    'hello "operator"'
    >>> _clean_namespace('"hello ""operator"", goo bye"')
    'hello "operator", goo bye'
    '''
    ns = (ns or '').strip()
    if not ns:
        return
    if ns[0] == '"':
        assert len(ns) > 1, 'invalid namespace %s' % ns
        assert ns[-1] == '"', 'invalid namespace %s' % ns
        ns = ns[1:-1]
    return ns.strip().replace('""', '"')

def split_search_path(text):
    ''' Split a search_path string into an array of namespaces.

        Namespaces can contain whitespace, commas, and quote characters (which are escaped with a quote character).
        If namespaces contain whitespace, commas, or quote characters, they must be wrapped in quotes in the search_path
        string. The quotes must be the first and last character.

    >>> split_search_path('')
    []
    >>> split_search_path(' ')
    []
    >>> split_search_path('ns1 ')
    ['ns1']
    >>> split_search_path('ns1,ns2 ')
    ['ns1', 'ns2']
    >>> split_search_path('ns1, "ns2, ns3"')
    ['ns1', 'ns2, ns3']
    >>> split_search_path('ns1,ns2, ns3 ')
    ['ns1', 'ns2', 'ns3']
    >>> split_search_path('ns1,"ns2, ""ns3"", ns4 " ')
    ['ns1', 'ns2, "ns3", ns4']
    '''
    ret = []
    buf = ''
    quoted = False
    for c in text or '':
        if c == '"':
            quoted = not quoted
            buf += c
        elif c == ',' and not quoted:
            ret.append(buf)
            buf = ''
        else:
            buf += c
    ret.append(buf)
    ret = [_clean_namespace(ns) for ns in ret]
    return [ns for ns in ret if ns]

def format_search_path(nslist):
    ''' Take a list of namespaces and return a formatted search_path.

    >>> format_search_path([])
    ''
    >>> format_search_path(None)
    ''
    >>> format_search_path(['a'])
    'a'
    >>> format_search_path(['a', 'b'])
    'a, b'
    >>> format_search_path(['$user', 'public'])
    '"$user", public'
    >>> format_search_path(['a', 'b', 'c d'])
    'a, b, "c d"'
    >>> format_search_path(['a', 'b', 'c,d'])
    'a, b, "c,d"'
    >>> format_search_path(['a', 'b', 'c "d" e'])
    'a, b, "c ""d"" e"'
    '''
    ret = []
    for ns in nslist or []:
        if '"' in ns or ' ' in ns or ',' in ns or '$' in ns:
            ns = '"%s"' % ns.replace('"', '""')  # noqa: PLW2901
        ret.append(ns)
    return ', '.join(ret)

def set_database_search_path(pg, database, search_path):
    print(f'database {database!r} search_path = {search_path}', file=sys.stdout)
    return pg.execute(SQL('''
        ALTER DATABASE {database} SET search_path = {search_path};
    ''').format(database=Identifier(database), search_path=SQL(search_path)))

def dedup(items):
    ''' Yield from :items:, omitting duplicate elements. '''
    seen = set()
    for item in items:
        if item in seen:
            continue
        seen.add(item)
        yield item

def ordered_search_path(namespaces, add_namespace):
    ''' Add :add_namespace: to :namespaces:, at the front, but behind the default "$user" and "public" (if present).

        We want "$user" and "public" in front so that "CREATE TABLE foo" without a namespace specified will place "foo"
        where the user would expect on a default postgresql install, instead of on some third party namespace.

        Remove any existing occurances of :add_namespace and put in front of search_path. This works because if a
        namespace depends on another, the dependee will be evaluated last, thus added in front of the dependency. If
        the dependency is invalidated, it will also invalidate the dependee, so the dependee will still be in front.

    >>> ordered_search_path([], 'ns1')
    ['ns1']
    >>> ordered_search_path(['ns1'], 'ns1')
    ['ns1']
    >>> ordered_search_path(['$user', 'public'], 'ns1')
    ['$user', 'public', 'ns1']
    >>> ordered_search_path(['$user', 'public', 'ns2'], 'ns1')
    ['$user', 'public', 'ns1', 'ns2']
    '''
    namespaces = [add_namespace, *namespaces]
    if 'public' in namespaces:
        namespaces = ['public', *namespaces]
    if '$user' in namespaces:
        namespaces = ['$user', *namespaces]
    return list(dedup(namespaces))

def prepend_database_search_path(pg, name):
    # obtain lock to avoid consistency issues with concurrent edits
    pg.execute('SELECT pg_advisory_lock(9090660030157094277);').first()
    database = pg.execute('SELECT * FROM pg_database WHERE datname = current_database();').first()
    namespaces = split_search_path(get_database_search_path(pg, database.oid))
    set_database_search_path(pg, database.datname, format_search_path(ordered_search_path(namespaces, name)))

def apply(pguri):
    if env.get_bool('SCM_PG_UPGRADE'):
        # In an upgrade process no database has been created up to this point, so let's exit
        return
    name = env.get_str('name')
    comment = env.get_str('comment')
    with pgcn.connect(pguri) as pg:
        if pg.execute('SELECT pg_is_in_recovery();').scalar():
            print('read-only replica, skipping namespace %s' % name, file=sys.stdout)
            return
        acquire_shared_builder_lock(pg)
        existing = pg.execute('SELECT * FROM pg_namespace WHERE nspname = %s;', (name,)).first()
        if not existing:
            create_namespace(pg, name)
        set_namespace_comment(pg, name, comment)
        prepend_database_search_path(pg, name)
        pg.commit()

def main():
    apply(env.get_str('pguri'))

if __name__ == '__main__':
    main()
