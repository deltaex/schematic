'''Utils for validating postgresql settings.'''
import re
import sys

import psutil

from schematic import env, slog
from schematic.build_server import get_server_settings
from schematic.build_util import pg_config_version

re_memory = re.compile(r'(?P<amount>\d+(\.\d+)?)\s*(?P<unit>[KMGT]?B)')

# Settings that can't be set via pgconf because they're managed by other components of schematic
forbidden_settings = {
    'search_path',
}

# Settings that should be merged instead of replaced, and the merge function
# used to merge new values into the existing ones.
settings_to_merge = {
    'shared_preload_libraries': lambda prev, new: f'{prev},{new}',
    'local_preload_libraries': lambda prev, new: f'{prev},{new}',
    'session_preload_libraries': lambda prev, new: f'{prev},{new}',
    'dynamic_library_path': lambda prev, new: f'{prev}:{new}',
}

# A collection of settings that were added or removed in different versions of Postgres.
# Useful for warning users if they try to include a setting that isn't available
# in the current version of their database and preventing the corresponding errors.
# Please keep the format as `setting_name: (introduced_in, deprecated_in)`
settings_by_version = {
    'max_sync_workers_per_subscription': (10, None),
    'wal_keep_segments': (None, 13),
    'wal_keep_size': (13, None),
    'logical_decoding_work_mem': (13, None),
    'maintenance_io_concurrency': (13, None),
    'max_slot_wal_keep_size': (13, None),
    'hash_mem_multiplier': (13, None),
    'wal_receiver_create_temp_slot': (14, None),
    'ssl_dh_params_file': (14, None),
    'log_transaction_sample_rate': (14, None),
    'vacuum_cleanup_index_scale_factor': (14, None),
    'allow_in_place_tablespaces': (15, None),
    'autovacuum_vacuum_insert_threshold': (15, None),
    'log_parameter_max_length': (15, None),
    'recovery_target_timeline': (15, None),
    'debug_parallel_query': (16, None),
    'enable_presorted_aggregate': (16, None),
    'vacuum_buffer_usage_limit': (16, None),
    'createrole_self_grant': (16, None),
    'gss_accept_delegation': (16, None),
    'scram_iterations': (16, None),
    'reserved_connections': (16, None),
    'debug_logical_replication_streaming': (16, None),
    'max_parallel_apply_workers_per_subscription': (16, None),
    'vacuum_defer_cleanup_age': (None, 16),
    'promote_trigger_file': (12, 16),
    'force_parallel_mode': (14, 16),
    'old_snapshot_threshold': (9, 17),
    'allow_alter_system': (17, None),
    'enable_group_by_reordering': (17, None),
    'serializable_buffers': (17, None),
    'synchronized_standby_slots': (17, None),
    'summarize_wal': (17, None),
    'subtransaction_buffers': (17, None),
    'transaction_timeout': (17, None),
    'transaction_buffers': (17, None),
    'wal_summary_keep_time': (17, None),
    'event_triggers': (17, None),
    'io_combine_limit': (17, None),
    'multixact_member_buffers': (17, None),
    'multixact_offset_buffers': (17, None),
    'notify_buffers': (17, None),
    'db_user_namespace': (None, 17),
    'default_with_oids': (None, 12),
    'operator_precedence_warning': (None, 14),
    'stats_temp_directory': (None, 15)
}

def merge_settings(file_contents):
    from schematic.build_pgconf import \
        parse_setting  # Prevent circular import | pylint: disable=import-outside-toplevel
    '''
    Reads the contents of a postgresql.conf and merges the values of the settings
    specified in `settings_to_merge`, returning a dict with the merged values
    for those settings.
    '''
    merged = {}

    for line in file_contents.splitlines():
        if (sett := parse_setting(line)) and sett['name'] in settings_to_merge:
            name = sett['name']
            val = sett['value'].strip('\'')
            if name not in merged:
                merged[name] = val
            else:
                merge_func = settings_to_merge[name]
                merged[name] = merge_func(merged[name], val)

    return merged

def validate_section(section):
    '''Validates a whole pgconf settings section, and formats a helpful message on errors.'''
    error = False
    # Pre-compute some things before validation
    current_ver = pg_config_version(env.get_str('basedir'))
    current_pg_major = get_major_version(current_ver)

    for setting in section['settings']:
        try:
            validate_server_override(setting)
            validate_shared_buffers(setting, section)
            validate_setting_version(setting, section, current_pg_major)
        except ValueError as ex:
            # Show a nice error message before exiting
            slog.error2(slog.red(f'ERROR: invalid setting in <{section["name"]}-{section["guid"]}>:'))
            slog.error2(f'    {setting["name"]} = {setting["value"]}\n'
                        f'    ^ {ex}')
            error = True
        if error:
            # Exit here to avoid chaining stack traces, for better error readability
            sys.exit(11)

def validate_server_override(setting):
    '''Make sure that the default server settings can't be overriden'''
    name = setting['name']
    if name in forbidden_settings | {k.lower() for k in get_server_settings()}:
        msg = f'Setting "{name}" is privileged and cannot be overriden.'
        if name == 'port':
            msg += ('\nPorts can only be set in the server\'s definition,'
                    '\nyou can change it in srv/<your-server>/default.nix')
        if name == 'search_path':
            msg += '\nThis setting is automatically managed by schematic.'
        if 'ssl' in name:
            msg += ('\nSSL is enabled by default using standardized cert/key paths,'
                    '\nalthough it\'s not mandatory to use SSL to connect.')
        raise ValueError(msg)

def validate_shared_buffers(setting, ctx):
    '''
    Protect against `shared_buffers` settings that are larger than available RAM
    by capping it to a % of it. Receives both the setting, and the section in which
    it appears as context, to look for `shared_buffers_max_pct`.
    '''
    name = setting['name']
    if name != 'shared_buffers':
        return
    value = setting['value']
    if (m := re_memory.search(value)) is None:
        raise ValueError(f'Invalid memory amount: "{value}"\nTip: it should be of the form (number)(KB/MB/GB/TB) '
                          '--- case sensitive!')
    amount = m.group('amount')
    unit = m.group('unit')
    units = {'b': 1, 'kb': 2**10, 'mb': 2**20, 'gb': 2**30, 'tb': 2**40}

    requested_memory = int(float(amount) * units[unit.lower()])
    system_memory = psutil.virtual_memory().total
    max_pct = pop_max_pct_buffers(ctx['settings'])
    cap = int(system_memory * max_pct / 100.0)

    if requested_memory > cap:
        cap_mb = int(cap / units['mb'])
        setting['value'] = f'{cap_mb}MB'
        slog.warn2(
            f'WARNING: Requested memory for `shared_buffers` ({requested_memory / units["gb"]:.2f} GB) is larger '
            f'than {max_pct}%% of total usable memory ({cap / units["gb"]:.2f} GB). It will be capped to that amount.\n'
            '    hint: A different ceiling can be set (eg `shared_buffers_max_pct = "33.3%%";`) '
            'in the pgconf package where shared_buffers is defined.')

def validate_setting_version(setting, ctx, current_major):
    name = setting['name']
    if data := settings_by_version.get(name):
        min_v, max_v = data[0] or 0, data[1] or 99999
        can_apply = eval(f'{min_v} <= {current_major} < {max_v}')  # noqa: PGH001,S307 # pylint: disable=eval-used
        if not can_apply:
            slog.warn2(f'WARNING: setting "{name}" requires postgresql {version_error_msg(data)}'
                       f' but this server is running on version {current_major}. '
                        'It will be ignored to prevent a configuration error.')
            pop_setting(name, ctx['settings'])

def version_error_msg(data):
    match data:
        case (a, None):
            return f'>= {a}'
        case (None, b):
            return f'< {b}'
        case (a, b):
            return f'>= {a} and < {b}'

def pop_setting(name, settings):
    '''
    Finds and returns a setting by name (case-insensitive), removing it from the settings list.

    If the setting can't be found, returns None.
    '''
    try:
        ix = next(i for i, setting in enumerate(settings) if setting['name'] == name.lower())
    except StopIteration:
        return
    return settings.pop(ix)

def pop_max_pct_buffers(settings, default=25.0):
    '''
    If present, parses and returns the value of `shared_buffers_max_pct` from the settings list,
    and then removes it from the list since it's a non-standard config setting.

    If not present, returns the default value.
    '''
    if setting := pop_setting('shared_buffers_max_pct', settings):
        val = setting['value'].replace('%', '')
        return float(val)
    return default

def get_major_version(ver):
    '''
    >>> get_major_version('16')
    '16'
    >>> get_major_version('15.2')
    '15'
    >>> get_major_version('9.3.4')
    '9'
    >>> get_major_version('15beta1')
    '15'
    >>> get_major_version('14.6beta8')
    '14'
    >>> get_major_version('17rc2')
    '17'
    '''
    major = ver.split('.')[0]
    return re.sub(r'(beta|rc)\d+', '', major, flags=re.IGNORECASE)
