from typing import List, Literal, Optional, TypeAlias, TypedDict, TypeGuard

JSON: TypeAlias = dict[str, 'JSON'] | list['JSON'] | str | int | float | bool | None

class Table(TypedDict):
    namespace: str
    tablename: str
    key: List[str]

class Insert(TypedDict):
    op: Literal['insert']
    tsn: str
    tts: str
    txn: int
    table: Table
    new: dict[str, JSON]

class Update(TypedDict):
    op: Literal['update']
    tsn: str
    tts: str
    txn: int
    table: Table
    new: dict[str, JSON]
    old: Optional[dict[str, JSON]]

class Delete(TypedDict):
    op: Literal['delete']
    tsn: str
    tts: str
    txn: int
    table: Table
    old: dict[str, JSON]

class Truncate(TypedDict):
    op: Literal['truncate']
    tsn: str
    tts: str
    txn: int
    tables: List[Table]
    cascade: bool
    restartid: Optional[int]

Operation: TypeAlias = Insert | Update | Delete | Truncate

def is_new_data(operation: Operation) -> TypeGuard[Insert | Update]:
    return operation['op'] == 'insert' or operation['op'] == 'update'
