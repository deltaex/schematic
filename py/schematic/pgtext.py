'''
Codec for postgresql text format and python types.

Useful for decoding values from COPY ("text" format) and pgoutput logical decoding,

Be careful with custom types that aren't standard, since they'll end up with different OIDs in different databases.
'''
import psycopg2
import psycopg2._psycopg
import psycopg2.extensions
from clint.textui import colored, puts

from schematic import pglsn

type_pg_lsn = psycopg2.extensions.new_type((3220,), 'PG_LSN', pglsn.lsn_to_int)

def uuid_to_str(uuid, cursor):  # noqa: ARG001
    ''' Decode UUID type.
    >>> uuid_to_str('3cd38be8-f2d5-c520-c9d0-0f0330f7f9a6', None)
    '3cd38be8f2d5c520c9d00f0330f7f9a6'
    '''
    return uuid.replace('-', '') if uuid else None

type_uuid = psycopg2.extensions.new_type((2950,), 'UUID', uuid_to_str)

# NB: many overlap, place in order of preference when oids in x.values conflict
# pylint: disable=c-extension-no-member,protected-access
# ruff: noqa: SLF001
psycopg2_types = [
    type_pg_lsn,
    type_uuid,
    psycopg2.extensions.BOOLEAN,
    psycopg2.extensions.BOOLEANARRAY,
    psycopg2.extensions.INTEGER,
    psycopg2.extensions.INTEGERARRAY,
    psycopg2.extensions.LONGINTEGERARRAY,
    psycopg2.extensions.LONGINTEGER,
    psycopg2.extensions.DECIMAL,
    psycopg2.extensions.DECIMALARRAY,
    psycopg2.extensions.FLOAT,
    psycopg2.extensions.FLOATARRAY,
    psycopg2.NUMBER,
    psycopg2.BINARY,
    psycopg2.extensions.BINARYARRAY,
    psycopg2.extensions.DATE,
    psycopg2.extensions.DATEARRAY,
    psycopg2.DATETIME,
    psycopg2.extensions.DATETIMEARRAY,
    psycopg2.ROWID,
    psycopg2.extensions.ROWIDARRAY,
    psycopg2.extensions.UNICODE,
    psycopg2.extensions.UNICODEARRAY,
    psycopg2.STRING,
    psycopg2.extensions.STRINGARRAY,
    psycopg2.extensions.INTERVAL,
    psycopg2.extensions.INTERVALARRAY,
    # Omit json decoders because we want to transport json values verbatum, avoiding suble differences from re-encoding.
    # Also because decoding from json to dict is automatic, but encoding from dict to json isn't, causing exceptions.
    # Ultimately, we just treat json as text.
    #psycopg2.extensions.JSON, psycopg2.extensions.JSONARRAY, psycopg2.extensions.JSONB, psycopg2.extensions.JSONBARRAY,
    psycopg2.extensions.PYDATE,
    psycopg2.extensions.PYDATEARRAY,
    psycopg2.extensions.PYDATETIME,
    psycopg2.extensions.PYDATETIMEARRAY,
    psycopg2.extensions.PYDATETIMETZ,
    psycopg2.extensions.PYDATETIMETZARRAY,
    psycopg2.extensions.PYINTERVAL,
    psycopg2.extensions.PYINTERVALARRAY,
    psycopg2.extensions.PYTIME,
    psycopg2.extensions.PYTIMEARRAY,
    psycopg2.extensions.TIME,
    psycopg2.extensions.TIMEARRAY,
    psycopg2._psycopg.CIDRARRAY,
    psycopg2._psycopg.DATETIMETZ,
    psycopg2._psycopg.DATETIMETZARRAY,
    psycopg2._psycopg.INETARRAY,
    psycopg2._psycopg.MACADDRARRAY,
    psycopg2._psycopg.UNKNOWN,
]

oid_types = {}
for t in psycopg2_types:
    for oid in t.values:
        if oid not in oid_types:
            oid_types[oid] = t

def print_type_defs(module):
    ''' Use heuristic to discover type definitions in psycopg2 modules. '''
    print('\n'.join([x for x in dir(module) if x == x.upper() and hasattr(getattr(module, x), 'values')]))


class UnknownType(ValueError):

    def __init__(self, text, oid):
        super().__init__(text)
        self.text = text
        self.oid = oid

def unknown_type(text, oid, cursor=None): # noqa: ARG001
    raise UnknownType(text, oid)

def decode(text, oid, cursor=None, fallback=unknown_type):
    ''' Decode from a postgresql text value to an appropriate python type.
        :oid: the postgresql type id, see catalog table pg_type.atttypid
        :cursor: The psycopg2 cursor object. Says it's required but seems to work fine without it.
        :fallback: a function to decode the value when the type is not known

        Future improvement: decode custom types to their python equivalents.
        https://www.postgresql.org/docs/current/sql-createdomain.html
        https://www.postgresql.org/docs/current/sql-createtype.html
        https://www.postgresql.org/docs/current/catalog-pg-type.html

    >>> decode('2020-10-21 23:41:49.115957', 1114)
    datetime.datetime(2020, 10, 21, 23, 41, 49, 115957)

    # >>> decode('8925/38C3A4C0', 3220)  # causes segfault without cursor param
    # 150792959141056
    # >>> decode('3cd38be8-f2d5-c520-c9d0-0f0330f7f9a6', 2950)  # causes segfault without cursor param
    # '3cd38be8f2d5c520c9d00f0330f7f9a6'
    '''
    if oid in oid_types:
        return oid_types[oid](text, cursor)
    return fallback(text, oid, cursor)

def decode_nobin(text, oid, cursor=None, fallback=unknown_type):
    r''' Like decode function, but decodes bytea and bytea[] as hex format strings[1] instead of raw bytes. This is
        useful for values that will be encoded in json, since json doesn't allow raw bytes (it insists on utf8).

        [1] https://www.postgresql.org/docs/current/datatype-binary.html#id-1.5.7.12.9

    # >>> decode_nobin('\\x528eae77b7aab320609b714bf631721bd87302f6', 17)  # causes segfault without cursor param
    # '\\x528eae77b7aab320609b714bf631721bd87302f6'
    # >>> decode_nobin('{"\\\\x528eae77b7aab320609b714bf631721bd87302f6"}', 1001) # causes segfault without cursor param
    # ['\\x528eae77b7aab320609b714bf631721bd87302f6']
    '''
    if oid == 17:
        return psycopg2.STRING(text, cursor)
    if oid == 1001:
        return psycopg2.extensions.STRINGARRAY(text, cursor)
    return decode(text, oid, cursor=cursor, fallback=fallback)

def test_decode_all(text=None):
    ''' Decode from a postgresql text value using all decoders. For testing/debugging. '''
    is_error = False
    for t in psycopg2_types:
        try:
            v = colored.green(repr(t(text, None)))
        except Exception as ex:  # noqa: BLE001
            is_error = True
            v = colored.red(repr(ex))
        puts(f'{t.name.ljust(20)}\t{v}')
    assert is_error is False
