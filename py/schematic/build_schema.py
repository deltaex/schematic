import sys

from schematic import env, pgcn
from schematic.build_util import (
    acquire_shared_builder_lock, apply_sql_file, install_buildinput_files)


def apply_declarative(autocommit):
    ''' Upgrade a declarative sandbox database from declarative definitions.'''
    install_buildinput_files()
    upgrade_sql = env.get_str('upgrade_sql')
    with pgcn.connect(env.get_str('pguri'), autocommit=autocommit) as pg:
        if pg.execute('SELECT pg_is_in_recovery();').scalar():
            print('read-only, skipping %r' % upgrade_sql, file=sys.stderr)
            return
        acquire_shared_builder_lock(pg)
        apply_sql_file(pg, upgrade_sql)
        pg.commit()

def main():
    autocommit = env.get_bool('autocommit')
    if env.get_str('SCM_UPGRADE_MODE') == 'declarative':
        if not env.get_bool('SCM_PG_UPGRADE'):
            apply_declarative(autocommit)

if __name__ == '__main__':
    main()
