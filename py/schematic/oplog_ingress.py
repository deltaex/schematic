#! /usr/bin/env python3
import fcntl
import itertools
import multiprocessing
import os
import re
import select
import time
from collections import deque
from dataclasses import dataclass
from multiprocessing import Lock, Process
from multiprocessing.connection import Connection
from typing import Any, Deque, Dict, Optional, Tuple

import bugsnag
import orjson
import psycopg

from schematic import oplog_s3, pgcn, pgingest3, seqs, slog
from schematic.oplog import Operation, is_new_data
from schematic.oplog_index import Transaction, indexed_oplog, pop_concurrent
from schematic.oplog_s3 import OplogReference, extend_reference, stream_whole_reference


@dataclass(slots=True)
class Worker:
    sink: Connection
    process: Process
    lock: Any

@dataclass(slots=True)
class Job:
    data: Transaction
    worker: Worker

def logstatus(i: int, txn: Optional[int], tsn: Optional[str], tts: Optional[str]):
    slog.info2(f'cursor {i:10d} ops, txn {txn}, tsn {tsn}, tts {tts}')

def handle_op(pg: psycopg.Connection, op: Operation, column_defs, upsert_cache, delete_cache):
    if op['op'] == 'insert':  # use upsert instead of insert in case there's existing data  # noqa: SIM114
        pgingest3.upsert(pg, op['table'], op['op'], op.get('old'), op['new'], column_defs, _prep_cache=upsert_cache)
    elif op['op'] == 'update':  # use upsert for idempotence, especially with filtered subscriptions
        pgingest3.upsert(pg, op['table'], op['op'], op.get('old'), op['new'], column_defs, _prep_cache=upsert_cache)
    elif op['op'] == 'delete':
        pgingest3.delete(pg, op['table'], op['old'], _prep_cache=delete_cache)
    elif op['op'] == 'truncate':
        pgingest3.truncate(pg, op['tables'], cascade=op['cascade'], restartid=op['restartid'])

def blacklisted(pg: psycopg.Connection, op: Operation, blacklist) -> bool:
    if is_new_data(op):
        namespace, tablename = op['table']['namespace'], op['table']['tablename']
        keycols = pgingest3.get_keycols(pg, namespace, tablename)
        entry = (namespace, tablename, tuple(op['new'][key] for key in keycols))
        return entry in blacklist
    return False

def parallel_stream_worker(pguri, replication_role, subid, oplog_file, column_defs, pipe, lock, master_pid):
    assert os.get_blocking(pipe.fileno()), 'pipes must be blocking'
    with pgcn.connect3(pguri) as pg, \
            open(oplog_file, 'rb') as oplog_file_pointer:
        pgingest3.init_replication_settings(pg, replication_role, subid=subid, master_pid=master_pid)
        pg.commit()
        tsn = tts = None
        committing, redo, blacklist = False, None, set()
        running: bool = True
        while running:
            try:
                upsert_cache, delete_cache = dict(), dict()
                if redo:
                    for previous_op in stream_whole_reference(oplog_file_pointer, redo):
                        if blacklisted(pg, previous_op, blacklist):
                            continue
                        handle_op(pg, previous_op, column_defs, upsert_cache, delete_cache)
                if committing:
                    pgingest3.commit_tsn(pg, tsn, tts)
                    committing, redo, blacklist = False, None, set()
                    lock.release()
                with pg.pipeline():
                    while True:
                        match data := pipe.recv():
                            case None:
                                committing = True
                                pgingest3.commit_tsn(pg, tsn, tts)
                                committing, redo, blacklist = False, None, set()
                                lock.release()
                            case (query, (offset, length)):
                                op = orjson.loads(query)
                                reference = OplogReference(offset, length)
                                if redo is None:
                                    redo = reference
                                else:
                                    extend_reference(redo, reference)
                                tsn, tts = op['tsn'], op['tts']
                                handle_op(pg, op, column_defs, upsert_cache, delete_cache)
                            case b'':
                                running = False
                                break
                            case _:
                                raise NotImplementedError(f"Not implemented: {data}")
                running = False
            except psycopg.errors.UniqueViolation as e:
                # rolling back will delete the cache
                bad_record = pgingest3.mitigate_conflict(pg, e) # rolls back and deletes conflict
                slog.info2(f'blacklisting {bad_record}')
                blacklist.add(bad_record)

def ingress_stream(pg, olfile, start_tsn, column_defs, status_interval: int=5 * 60):
    ''' Download oplog file and concurrently process ops as writes into the database. '''
    txn: Optional[int] = None
    tsn: Optional[str] = None
    tts: Optional[str] = None
    lastlog: Optional[float] = None
    upsert_cache, delete_cache = {}, {}
    stream = itertools.dropwhile(lambda x: start_tsn and x['tsn'] <= start_tsn, oplog_s3.stream_json(olfile))
    blacklist = set()
    ops = []
    i = 0
    running = True
    while running:
        try:
            for previous_op in ops:
                if previous_op is not None:
                    if blacklisted(pg, previous_op, blacklist):
                        continue
                    handle_op(pg, previous_op, column_defs, upsert_cache, delete_cache)
                    pg.commit()
                else:
                    pgingest3.commit_tsn(pg, tsn, tts)
                    ops = list()
                    blacklist = set()
            for i, op in enumerate(stream, start=1):
                if txn is not None and txn != op['txn']:  # first op of new txn, infer commit
                    ops.append(None) # register we're about to commit
                    # because of pipeline mode, exceptions from previous operations may be delayed to commit time
                    # use op['tsn'] instead of tsn to restart at this op, not prev
                    pgingest3.commit_tsn(pg, op['tsn'], tts)
                    ops = list() # success!
                ops.append(op)
                txn, tsn, tts = op['txn'], op['tsn'], op['tts']
                handle_op(pg, op, column_defs, upsert_cache, delete_cache)
                now = time.time()
                if not lastlog or now - lastlog >= status_interval:
                    logstatus(i, txn, tsn, tts)
                    lastlog = now
            running = False
        except psycopg.errors.UniqueViolation as e:
                # rolling back will delete the cache
                upsert_cache.clear()
                delete_cache.clear()
                bad_record = pgingest3.mitigate_conflict(pg, e) # rolls back and deletes conflict
                slog.info2(f'blacklisting {bad_record}')
                blacklist.add(bad_record)
    logstatus(i, txn, tsn, tts)
    pgingest3.commit_tsn(pg, tsn, tts)

def get_max_pipe_size():
    with open('/proc/sys/fs/pipe-max-size') as fp:
        return int(fp.read())

def draft_worker(
    workers: Dict[int, Job], xid: int, data: Transaction, oplog_file: str, idle_workers: Deque[Worker],
    fd_map: Dict[int, int], pguri, replication_role: str, subid: str,
    column_defs, master_pid
) -> None:
    assert xid not in workers, f'tried to assign two workers to transaction {xid}'
    if idle_workers:
        worker = idle_workers.popleft()
    else:
        source, sink = multiprocessing.Pipe(False)
        lock = Lock()
        p: Process = multiprocessing.Process(
            target=parallel_stream_worker, args=(pguri, replication_role, subid, oplog_file, column_defs, source, lock, master_pid))
        p.start()
        # have bigger pipes without limiting the amount of pipes we can have open
        fcntl.fcntl(sink.fileno(), fcntl.F_SETPIPE_SZ, get_max_pipe_size()//16)
        worker = Worker(sink, p, lock)
        worker.lock.acquire() # will be released later to signal commit
    workers[xid] = Job(data, worker)
    fd_map[worker.sink.fileno()] = xid

def acquire_carefully(worker: Worker):
    running, done = True, True
    while running:
        alive = worker.process.is_alive()
        done = worker.lock.acquire(timeout=0.1)
        running = alive and not done
    if not done:
        raise pgingest3.WorkerExit

def cleanup_directory(directory_path):
    files = os.listdir(directory_path)
    for file in files:
        file_path = os.path.join(directory_path, file)
        if os.path.isfile(file_path):
            os.remove(file_path)

def parallel_ingress_stream(pg, pguri, replication_role, subid, olfile,
                            start_tsn, column_defs, max_workers=100, status_interval: int=5 * 60):
    master_pid = pg.execute('SELECT pg_backend_pid();').fetchone()[0]
    with oplog_s3.open_oplog(olfile) as oplog_file:
        def already_applied(x: Tuple[Operation, OplogReference]) -> bool:
            return start_tsn and x[0]['tsn'] <= start_tsn
        stream = itertools.dropwhile(already_applied, oplog_s3.read_jsonl(oplog_file))
        idle_workers = deque()
        workers: Dict[int, Job] = {}
        waiting: Dict[int, Transaction] = {}
        fd_map = {}
        file_descriptors = []
        i = 0
        tsn, tts, lastlog = None, None, None
        try:
            ids_by_commit, transactions_by_start = indexed_oplog(stream)
            file_descriptors = list()
            for leading in ids_by_commit:
                #print('starting')
                assert len(workers) < max_workers
                now = time.time()
                if not lastlog or now - lastlog >= status_interval:
                    logstatus(i, None, tsn, tts)
                    lastlog = now
                #print('popping newcomers')
                while newcomer := pop_concurrent(leading.commit_tsn, transactions_by_start):
                    waiting[newcomer.identifier] = newcomer
                # now `waiting` contains all transactions that start before the leading transaction
                # commits, in particular it contains the leading transaction
                if leading.identifier not in workers:
                    #print('ensuring leading identifier is present')
                    data = waiting.pop(leading.identifier)
                    draft_worker(workers, leading.identifier, data, oplog_file.name, idle_workers, fd_map,
                                 pguri, replication_role, subid, column_defs, master_pid)
                # get more transactions to apply in start_tsn order
                #print('drafting side transactions')
                to_draft = list(itertools.islice(waiting.keys(), max_workers - len(workers)))
                for identifier in to_draft:
                    data = waiting.pop(identifier)
                    draft_worker(workers, identifier, data, oplog_file.name, idle_workers, fd_map,
                                 pguri, replication_role, subid, column_defs, master_pid)
                main_worker = workers[leading.identifier]
                #print('preparing to send transaction')
                main_fd = main_worker.worker.sink.fileno()
                file_descriptors = [main_fd]
                file_descriptors.extend(job.worker.sink.fileno() for job in workers.values() if job.worker.sink.fileno() != main_fd)
                while main_worker.data.operations.length:
                    #print('preparing to send transaction')
                    _, ready, _ = select.select([], file_descriptors, [])
                    for fd in ready: # fair queueing avoids starvation.
                        xid = fd_map[fd]
                        job = workers[xid]
                        # in theory since we're working with file segments it would be
                        # possible to work with non-blocking pipes and os.sendfile
                        # it would require rewriting lots of code though, so I'm postponing this
                        # optimization
                        data = oplog_s3.next_allowed_operation(oplog_file.file, job.data.operations,
                                                               main_worker.data.commit_tsn)
                        if data:
                            if xid == main_worker.data.identifier:
                                tsn = data[0]['tsn']
                                tts = data[0]['tts']
                            i += 1
                            pgingest3.send_carefully(job.worker,
                                                     (orjson.dumps(data[0]), (data[1].offset, data[1].length)))
                        else: # avoids loops in some cases
                            # Has quadratic complexity, but the amount of concurrent
                            # transactions is limited by max_workers
                            file_descriptors.remove(fd)
                pgingest3.send_carefully(main_worker.worker, None)
                idle_workers.append(main_worker.worker)
                del workers[leading.identifier]
                acquire_carefully(main_worker.worker)
            assert len(workers) == 0
            pgingest3.commit_tsn(pg, tsn, tts)
        except pgingest3.WorkerExit:
            for w in workers.values():
                try:
                    pgingest3.send_carefully(w.worker, b'')
                except pgingest3.WorkerExit:
                    pass
            for w in idle_workers:
                try:
                    pgingest3.send_carefully(w, b'')
                except pgingest3.WorkerExit:
                    pass
            for w in workers.values():
                w.worker.process.join()
            for w in idle_workers:
                w.process.join()
            raise
        for w in idle_workers:
            try:
                pgingest3.send_carefully(w, b'')
            except pgingest3.WorkerExit:
                pass
        for w in idle_workers:
            w.process.join()
    tmpdir = os.environ.get('SCM_TMPDIR') or os.path.expandvars('$HOME/var/tmp')
    cleanup_directory(tmpdir)

def initcopy_worker(pguri, replication_role, column_defs, queue, is_done):
    table = None
    with pgcn.connect3(pguri) as pg:
        pgingest3.init_replication_settings(pg, replication_role)
        pg.commit()
        upsert_cache = dict()
        while not is_done.is_set() or not queue.empty():
            try:
                batch = queue.get(timeout=.1)
            except multiprocessing.queues.Empty:
                continue
            if not batch:
                continue
            batch = [orjson.loads(l) for l in batch]
            table = table or batch[0]['table']
            done = False
            while not done:
                try:
                    for op in batch:
                        assert op['op'] == 'insert', 'expected insert, got %r' % op['op']
                        assert (op['table']['namespace'], op['table']['tablename']) == (
                            table['namespace'], table['tablename']), f'table mismatch {table!r} vs {op["table"]!r}'
                        pgingest3.upsert(pg, op['table'], op['op'],
                                         op.get('old'), op['new'], column_defs, _prep_cache=upsert_cache)
                    done = True
                except psycopg.errors.UniqueViolation as e:
                    upsert_cache = dict()
                    pgingest3.mitigate_conflict(pg, e) # rolls back and deletes conflict
            pg.commit()

def ingress_initcopy(
        pg, pguri, olfile, column_defs, replication_role, batch_size=1000, parallelism=50, status_interval: int=5 * 60):
    ''' Import an initcopy file using batching, higher performance than ingress_stream. '''
    txn = tsn = tts = op = lastlog = None
    ops = 0
    is_done = multiprocessing.Event()
    queue = multiprocessing.Queue()
    procs = []
    for i in range(parallelism):
        p = multiprocessing.Process(
            name=f'scm initcopy {i}/{parallelism} {olfile.name}',
            target=initcopy_worker, args=(pguri, replication_role, column_defs, queue, is_done))
        p.start()
        procs.append(p)
    for batch in seqs.batches(oplog_s3.stream(olfile), batch_size):
        op = orjson.loads(batch[0])
        queue.put(batch)
        while queue.qsize() >= parallelism:
            time.sleep(.1)
        ops += len(batch)
        txn, tsn, tts = op['txn'], op['tsn'], op['tts']
        now = time.time()
        if not lastlog or now - lastlog >= status_interval:
            logstatus(ops, txn, tsn, tts)
            lastlog = now
    is_done.set()
    for p in procs:
        p.join()
        assert p.exitcode == 0, 'got non-zero exitcode from initcopy_worker: %r' % p.exitcode
        p.close()
    logstatus(ops, txn, tsn, tts)
    # This logical message is partially a work-around for a bug that prevents the logical replication origin from being
    # updated in pgingest3.commit_tsn. The bug occurs because this main session isn't doing any writes of its own,
    # and pg_replication_origin_xact_setup doesn't take effect if no writes have been performed in the same txn.
    pg.execute('''SELECT pg_logical_emit_message(true, 'fujladyoihys-initcopy-finished', %s);''', (olfile.table,))
    pgingest3.commit_tsn(pg, tsn, tts)

def is_initcopy_file(olfile):
    ''' Determines if it's an initcopy file, used to determine whether batching can be used. '''
    return bool(olfile.table)

def ingress(s3uri, pguri, tsn=None, replication_role='replica', time_limit=None, force_sequential=False, max_workers=100):
    ''' Receive oplog update feed into postgresql.
        :s3uri: eg s3://some-bucket/some-prefix
        :pguri: eg postgresql://user:pass@localhost:port/dbname?application_name=oplog-ingress
        :tsn: skip to this TSN; useful for debugging; do not use for production databases or risk corrupt data!
        :replication_role: specifies PostgreSQL's session_replication_role setting[1];
            use 'replica' to disable triggers including foreign key constraint validation (requires superuser);
            use 'origin' to enable triggers and foreign key constraint validation
        :time_limit: stop processing new files after running this many seconds

        [1] https://www.postgresql.org/docs/current/runtime-config-client.html#RUNTIME-CONFIG-CLIENT-STATEMENT
    '''
    assert s3uri, 's3uri is required'
    subid = re.match(r's3://[\w-]+/\w+/', s3uri)
    assert subid, 'Unable to parse subscription id from s3uri. Check correctness of provided uri'
    subid = subid.group(0)
    start = time.time()
    with pgcn.connect3(pguri) as pg:
        lock = pg.execute('SELECT pg_try_advisory_lock(-1699317507, hashtext(%s));', (subid,)).fetchone()[0]
        if not lock:
            slog.info2(f'there is another running process consuming the subscription {subid}')
            raise SystemExit(1)
        start_tsn = pgingest3.init_replication_settings(pg, replication_role, subid=subid)
        parallel = (not force_sequential) and pgingest3.better_replication_origin(pg)
        pg.commit()
        start_tsn = tsn or start_tsn
        column_defs = pgingest3.get_column_defs(pg)
        # TODO: use the same function for sequential or parallel oplog-ingress
        # max_workers = 1 if not parallel else max_workers
        for olfile in oplog_s3.ls(s3uri, tsn=start_tsn):
            slog.info2(f'oplog file {olfile.size:12d} bytes\t {olfile.modified} \t {olfile.path}')
            bugsnag.leave_breadcrumb(f'olfile {olfile.bucket}/{olfile.path}')
            if is_initcopy_file(olfile):
                ingress_initcopy(pg, pguri, olfile, column_defs, replication_role)
            elif parallel:
                parallel_ingress_stream(pg, pguri, replication_role, subid, olfile, start_tsn, column_defs, max_workers=max_workers)
            else:
                ingress_stream(pg, olfile, start_tsn, column_defs)
            if time_limit and (time.time() - start) > time_limit:
                slog.info2('hit time limit, breaking')
                break
