import atexit
import errno
import glob
import os
import pathlib
import shutil

from schematic import slog


def try_unlink(filename):
    '''Tries to remove `filename`; returns a boolean representing whether it succeeded.'''
    try:
        os.unlink(filename) if filename else None
        return True
    except (OSError, IOError):
        return False

def try_read(filename, default=None, encoding='utf8'):
    '''
    Tries to read the contents of `filename`. If it doesn't exist or isn't readable,
    `default` is returned instead.
    '''
    if not filename or not os.path.exists(filename):
        return default

    try:
        with open(filename, 'r', encoding=encoding) as fp:
            return fp.read()
    except (OSError, IOError):
        return default

def read(filename, encoding='utf8'):
    with open(filename, 'r', encoding=encoding) as fp:
        return fp.read()

def write(filename, content, mode='w', encoding='utf8'):
    ''' Shorthand for writing `content` into `filename`. '''
    with open(filename, mode, encoding=encoding) as fp:
        fp.write(content)

def touch(filename):
    pathlib.Path(filename).touch()

def fsync_dir(filepath):
    ''' fsync the parent directory to ensure create or unlink operations on :filepath: are durable. '''
    parentdir = os.path.dirname(filepath)
    parentdir_fd = os.open(parentdir, os.O_DIRECTORY)
    os.fsync(parentdir_fd)
    os.close(parentdir_fd)

def mkdirs(directory, fsync=True):
    ''' Ensure directory exists, creating recursively if necessary, and issuing fsync for crash-safety. '''
    def dirs():
        x = directory
        while x not in '/':
            yield x
            x = os.path.dirname(x)
    for subdir in list(dirs())[::-1]:
        if not os.path.exists(subdir):
            try:
                os.mkdir(subdir)
            except OSError as exception:
                if exception.errno != errno.EEXIST:
                    raise
            if fsync:
                fsync_dir(subdir)  # we fsync the parent directory to ensure the new directory's entry doesn't get lost

def tmpdir(path, delete=True, verbose=2):
    ''' Create directory @path, deleting it automatically upon process exit. '''
    mkdirs(path, fsync=False)
    lock_path = os.path.join(path, '%d.lock' % os.getpid())
    with open(lock_path, 'w', encoding='utf8') as lockfile:
        lockfile.write('lock to prevent removing this directory until finished')
    def cleanup():
        try:
            os.remove(lock_path)
        except OSError:
            pass
        if not delete:
            return
        locks = glob.glob(os.path.join(path, '*.lock'))
        if locks:
            if verbose >= 2:
                slog.info2('leaving %r because of remaining locks: %r', path, locks) if locks else 0
        else:
            try:
                shutil.rmtree(path)
            except OSError as ex:
                if ex.errno != errno.ENOENT:
                    raise
            if verbose >= 1 and os.path.exists(path):
                slog.warn2('tmpdir failed to remove path: %r', path)
    atexit.register(cleanup)
    return path
