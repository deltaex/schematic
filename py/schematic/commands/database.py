'''
scm database [name]

Create files for a new database object.
    name: Logical name of database (tip: suffix primaries with 0, replicas with 1, 2, etc).
    dbname: Physical name of database (passed to PostgreSQL createdb).

Options:
    -d / --dbname
        Define postgresql dbname (if different from name).

Examples:
    scm database 'cool_db0'
        create files for a primary database object
    scm database 'cool_db1'
        create files for a replica database object
    scm database 'cool_db0' --dbname 'really_cool_db0'
        use a different name for the physical name of the database

'''
import os
import sys

from schematic import env, guids
from schematic.scm_cli_utils import get_free_port, mkdir, validate_name
from schematic.utils import cli


def database(name: str, dbname: str):
    '''
    Create files for a new database object.

    name: Logical name of database (tip: suffix primaries with 0, replicas with 1, 2, etc).

    dbname: Physical name of database (passed to PostgreSQL createdb).
    '''
    validate_name(name)
    dbname = dbname or name
    validate_name(dbname)
    database_guid = guids.get_database_guid()
    dirname = f'{name}-{database_guid}'
    root_dir: str = env.get_str('ROOT_DIR') # type: ignore reportAssignmentType
    database_path = os.path.abspath(os.path.join(root_dir, 'srv', dirname))
    server_guid = guids.get_server_guid()
    mkdir(database_path)
    outpath = os.path.join(database_path, 'default.nix')
    if os.path.exists(outpath):
        print('error, file exists: %s' % outpath, file=sys.stderr)
        sys.exit(1)
    params = {
        'database_guid': database_guid,
        'server_guid': server_guid,
        'name': name,
        'dbname': dbname,
        'port': get_free_port(),
        'user': env.get_str('SCM_USER', 'root'),
        'pass': env.get_str('SCM_PASS', guids.get_password()),
    }
    with open(outpath, 'w') as out:
        out.write('''
stdargs @ { scm, pkgs, ... }:

scm.database rec {
    guid = "%(database_guid)s";
    name = "%(name)s";
    server = scm.server rec {
        postgresql = pkgs.postgresql_17;
        guid = "%(server_guid)s";
        name = "%(name)s";
        dbname = "%(dbname)s";
        port = "%(port)d";
        user = "%(user)s";
        password = "%(pass)s";
    };
    dependencies = [

    ];
}\n'''.lstrip() % params)
    print('created %s' % outpath)

def main(argv):
    (pos_args, _, options) = cli.parse_args(argv, options=('-d', '--dbname'))
    name: str = cli.get_pos_arg(pos_args, 0, 'name')
    dbname: str = cli.get_option_str(options, names=('-d', '--dbname')) or name
    database(name, dbname)
