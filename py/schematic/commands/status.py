'''
scm status [path] [path] ...

List status of all databases in ~/var/pg.

Options:
    -v / --verbose
        enable verbose output to stderr

Env Vars:
    SCM_VERBOSE
        enables --verbose

Examples:
    scm status
        list the status of all databases in ~/var/pg
    scm status ~/other_basedir
        list the status of all databases in ~/other_basedir
    scm status ~/other_basedir1 ~/other_basedir2 ~/other_basedir3
        list status of all the databases in multiple directories

'''
import json
import os
import sys

from clint.textui import colored

from schematic import dates, env, fs
from schematic.scm_cli_utils import get_pid, get_postgres_process, resolve_basedirs, unexpand_user
from schematic.utils import cli


def status(basedirs: list[str] | None, verbose: bool):
    '''List status of all databases in ~/var/pg.'''
    for basedir in resolve_basedirs(basedirs):
        pidfile = os.path.join(basedir, 'data/postmaster.pid')
        pid = get_pid(pidfile)
        metajson = os.path.join(basedir, 'meta.json')
        try:
            metadata = json.loads(fs.read(metajson))
        except (json.JSONDecodeError, KeyError, TypeError):
            print('invalid %s' % unexpand_user(metajson), file=sys.stderr) if verbose else None
            continue
        except FileNotFoundError:
            print('missing %s' % unexpand_user(metajson), file=sys.stderr) if verbose else None
            continue
        proc = get_postgres_process(pid, pidfile_to_remove=pidfile)
        out = ''
        out += ('pid %7d' % pid) if pid else '           '
        if proc:
            out += f' {colored.green("up")} {dates.fmt_age(proc.create_time())}'
        else:
            dirstat = os.stat(basedir)
            out += f' {colored.red("dn")} {dates.fmt_age(dirstat.st_mtime)}'
        out += ' port %5d' % metadata['port']
        out += ' %s' % unexpand_user(basedir)
        print(out)

def main(argv):
    (pos_args, flags, _) = cli.parse_args(argv, flags=('-v', '--verbose'))
    basedirs: list[str] | None = pos_args if len(pos_args) > 0 else None
    verbose: bool = env.get_str('SCM_VERBOSE') == '1' or cli.get_flag(flags, ('-v', '--verbose'))
    status(basedirs, verbose)
