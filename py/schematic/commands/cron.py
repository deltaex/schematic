'''
scm cron [name] [schedule] [command]

Install a crontab task, wrapping the command in shell script with a suitable environment.


Examples:
    scm cron 'task_name' '5 4 * * *' 'echo "Hello world"'

'''
import os
import subprocess
import sys

from schematic import env
from schematic.scm_cli_utils import mkdir, scm_var_path, validate_name
from schematic.utils import cli


def escape_shell_cmd(cmd):
    r''' Escape shell :cmd: so that it can be wrapped in double quotes.

    >>> escape_shell_cmd('echo hi')
    'echo hi'
    >>> escape_shell_cmd("echo 'hi'")
    "echo 'hi'"
    >>> escape_shell_cmd('echo "hi"')
    'echo \\"hi\\"'
    '''
    return cmd.replace('"', '\\"')

def update_crontab(text, scriptpath, schedule):
    r''' Update or add a crontab line to a crontab file.
    >>> update_crontab('', 'example.sh', '* * * * *')
    '* * * * * example.sh # added by schematic\n'
    >>> update_crontab('* * * * * example.sh # added by schematic\n', 'example.sh', '* * * * *')
    '* * * * * example.sh # added by schematic\n'
    >>> update_crontab('* * * * * example2.sh # added by schematic\n', 'example.sh', '* * * * *')
    '* * * * * example2.sh # added by schematic\n* * * * * example.sh # added by schematic\n'
    >>> update_crontab('* * * * * example.sh\n', 'example.sh', '* * * * *')
    '* * * * * example.sh\n* * * * * example.sh # added by schematic\n'
    '''
    comment = '# added by schematic'
    ret = ''
    matched = False
    cron_banner = True   # used to supress the first few commented lines to prevent repeated cron banner
    newcronline = f'{schedule} {scriptpath} {comment}\n'

    if text:
        for line in text.rstrip('\n').split('\n'):
            if line.lstrip(' ').startswith('#'):
                if not cron_banner:
                    ret += '%s\n' % line
                continue
            # reached a line that isn't a comment so we assume the banner is over and subsequent comments must be kept
            cron_banner = False
            # the comment serves as an additional heuristic to prevent false positive matches
            if scriptpath in line and comment in line:
                matched = True
                ret += newcronline
                continue
            ret += '%s\n' % line
    if not matched:
        ret += newcronline
    return ret

def cron(name: str, schedule: str, command: str):
    ''' Install a crontab task, wrapping the command in shell script with a suitable environment. '''
    validate_name(name)
    crondir = os.path.join(scm_var_path, 'cron')
    mkdir(crondir)
    cronpath = os.path.join(crondir, name)
    root_dir = env.get_str('ROOT_DIR')
    with open(cronpath, 'w') as fp:
        fp.write('#! /usr/bin/env bash\n')
        fp.write('PATH=~/.nix-profile/bin:"$PATH"\n')
        fp.write('export PATH\n')
        fp.write('cd "%s"\n' % root_dir)
        fp.write('nix-shell -I ~/.nix-defexpr/channels --command "%s"\n' % escape_shell_cmd(command))
    print('script path %s' % cronpath, file=sys.stderr)
    os.chmod(cronpath, 0o775)  # ensure executable bit is set # noqa: S103
    try:
        crontext = subprocess.check_output(['crontab', '-l'], text=True, stderr=subprocess.PIPE)
    except subprocess.CalledProcessError as ex:
        if ex.returncode == 1:  # empty crontab
            crontext = ''
        else:
            raise
    newcrontext = update_crontab(crontext, cronpath, schedule)
    pipe = subprocess.PIPE
    with subprocess.Popen(['crontab', '-'], stdout=pipe, stdin=pipe, stderr=pipe, text=True) as crontabproc:
        (stdout, stderr) = crontabproc.communicate(input=newcrontext)
        if crontabproc.returncode != 0:
            print('error updating crontab!', file=sys.stderr)
            print(stderr, file=sys.stderr) if stderr.strip() else None
            print(stdout, file=sys.stdout) if stdout.strip() else None
            print('crontab text:\n%s' % newcrontext, file=sys.stderr)
            print('hint: try installing the cron line manually using crontab -e', file=sys.stderr)

def main(argv):
    (pos_args, _, _) = cli.parse_args(argv)
    name: str = cli.get_pos_arg(pos_args, 0, 'name')
    schedule: str = cli.get_pos_arg(pos_args, 1, 'schedule')
    command: str = cli.get_pos_arg(pos_args, 2, 'command')
    cron(name, schedule, command)
