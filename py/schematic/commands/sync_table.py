'''
scm sync-table [pguri] [pguri] [tablename]

Batch synchronize a table from one database to another.

Options:
    --sql_where
        SQL expression to filter on source table.
    --parallelism
        Number of parallel processes to use.
        default: 16
    --replication-role {'replica', 'origin', 'local'}
        Sets PostgreSQL session_replication_role setting.
        default: 'replica'
    --onconflict {'do nothing', 'do update'}
        Opt to skip an update for existing records. Useful for ad-hoc synchronization to backfill
         missing records, but not touch existing records.
        default: 'do update'

Examples:
    scm sync-table 'postgresql://user1:pass@localhost:port/dbname1' 'postgresql://user2:pass@localhost:port/dbname2' 'tablename'

'''
import sys

import psycopg2

from schematic import pgcn, slog
from schematic.utils import cli


def sync_table(
    src: str, dst: str, tablename: str, sql_where: str | None, parallelism: int,
    replication_role: str, onconflict: str
):
    ''' Batch synchronize a table from one database to another. '''
    try:
        from schematic import synctable  # pylint: disable=import-outside-toplevel
        with pgcn.connect(src) as pg_src, pgcn.connect(dst) as pg_dst:
            synctable.sync(pg_src, pg_dst, tablename, sql_where, parallelism, replication_role, onconflict)
    except (psycopg2.errors.AdminShutdown, psycopg2.OperationalError) as exc:  # pylint: disable=no-member
        slog.error2('error: %s, %s, %r', exc, exc, exc.args)
        raise SystemExit(1) from exc
    except psycopg2.errors.InsufficientPrivilege:  # pylint: disable=no-member
        slog.error2(f'replication role {replication_role} requires superuser')
        slog.error2((  # noqa: UP034
            'hint: replication role "replica" is faster because triggers and foreign key constraints are '
            'skipped, but replication role "origin" can be used without superuser access.'))
        sys.exit(2)

def main(argv):
    (pos_args, _, options) = cli.parse_args(argv,
        options=('--sql_where', '--parallelism', '--replication-role', '--onconflict')
    )
    src: str = cli.get_pos_arg(pos_args, 0, 'src')
    dst: str = cli.get_pos_arg(pos_args, 1, 'dst')
    tablename: str = cli.get_pos_arg(pos_args, 2, 'tablename')
    sql_where: str | None = cli.get_option_str(options, names=('--sql_where',))
    parallelism: int = cli.get_option_int(options, names=('--parallelism',)) or 16
    replication_role: str = cli.get_option_str(options, names=('--replication-role',)) or 'replica'
    onconflict: str = cli.get_option_str(options, names=('--onconflict',)) or 'do update'
    sync_table(src, dst, tablename, sql_where, parallelism, replication_role, onconflict)
