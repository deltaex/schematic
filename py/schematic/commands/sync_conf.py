'''
scm sync-conf [pguri]

Read tables from a database and generate a yaml file used by 'scm sync' for replication subscriptions.

Flags:
    -c / --columns
        Include column list (optional; useful for excluding columns)

Options:
    -o / --out
        Path to output file
    -n / --namespaces
        Comma-separated list of namespaces to include

Examples:
    scm sync-conf 'postgresql://user1:pass@localhost:port/dbname1'

'''
import sys

import psycopg2

from schematic import pgcn, pgyaml, slog
from schematic.utils import cli


def sync_conf(pguri: str, out: str, namespaces: str, columns: bool):
    ''' 'Read tables from a database and generate a yaml file used by 'scm sync' for replication subscriptions.'
        :pguri: eg postgresql://user:pass@localhost:port/dbname

        Format of output file is a list of records with fields:
        "namespace": table namespace (aka "schema")
        "tablename": name of table
        "initcopy": whether to perform an initial COPY export.
        "included": pass false to exclude this table from the subscription
        "inc_columns": a list of column names to include
            non-empty list will cause unnamed columns to be omitted
            an empty list will include all columns (including ones created during processing), except exc_columns
        "exc_columns": a list of column names to omit
        "sql_where": a text string to be used as a WHERE clause to filter the table for initial copy
            this is only for efficiency optimizations for initial COPY exports, it does not apply to incremental changes
    '''
    try:
        with pgcn.connect(pguri) as pg:
            pgyaml.write_schema_yaml(pg, namespaces=namespaces.split(','), out=out, inc_columns=columns)
    except (psycopg2.errors.AdminShutdown, psycopg2.OperationalError) as exc:  # pylint: disable=no-member
        slog.error2('error: %s, %s, %r', exc, exc, exc.args)
        sys.exit(1)

def main(argv):
    (pos_args, flags, options) = cli.parse_args(
        argv, flags=('-c', '--columns'),
        options=('-o', '--out', '-n', '--namespaces')
    )
    pguri: str = cli.get_pos_arg(pos_args, 0, 'pguri')
    out: str = cli.get_option_str(options, names=('-o', '--out')) or '/dev/stdout'
    namespaces: str = cli.get_option_str(options, names=('-n', '--namespaces')) or 'public'
    columns: bool = cli.get_flag(flags, names=('-c', '--columns'))
    sync_conf(pguri, out, namespaces, columns)
