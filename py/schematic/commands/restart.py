'''
scm restart [path] [path] ...

Restart specified database server. This allows changing the postgres command-line options.

Flags:
    -q / --quiet
        Quiet mode. Updates only the log file. No output is shown

Options:
    -m / --shutdown-mode {'smart', 'fast', 'immediate'}
        specifies the shutdown mode.
        default: 'fast'
    -w / --wal-senders {'graceful', 'fast'}
        Pass 'graceful' to wait for all replication clients to catch up before exiting.
         If any replication clients are lagging, this might take a long time.
         Pass 'fast' to terminate the replication processes instead of waiting for them.
         'fast' is the default when --shutdown-mode=fast and 'graceful' is the default when --shutdown-mode=smart

Examples:
    scm restart ~/var/pg/example_database-D0J6PLZEYV46LZA8
        restarting a specific database
    scm restart '<example_database-D0J6PLZEYV46LZA8>'
        another way to reference a database
    scm restart ~/var/pg/example_database-D0J6PLZEYV46LZA8 ~/var/pg/other_database-N6W6KLZUCV46LVZY
        restarting multiple databases at once

'''
from schematic.build_util import stop_wal_senders
from schematic.scm_cli_utils import get_basedir, pg_ctl_fn, resolve_basedirs
from schematic.utils import cli


def restart(basedirs: list[str], shutdown_mode: str, wal_senders: str, quiet: bool):
    '''Restart specified database server. This allows changing the postgres command-line options.'''
    for bdir in resolve_basedirs(basedirs):
        stop_wal_senders(shutdown_mode, wal_senders, bdir)
        pg_ctl_fn(get_basedir(bdir), 'restart', shutdown_mode=shutdown_mode, quiet=quiet, wal_senders=wal_senders)

def main(argv):
    (pos_args, flags, options) = cli.parse_args(argv,
        flags=('-q', '--quiet'), options=('-m','--shutdown-mode', '-q', '--quiet'))
    basedirs  = pos_args
    shutdown_mode = cli.get_option_str(options, names=('-m', '--shutdown-mode')) or 'fast'
    wal_senders = cli.get_option_str(options, names=('-w', '--wal-senders')) or \
      ('graceful' if shutdown_mode == 'smart' else 'fast')
    quiet = cli.get_flag(flags, names=('-q', '--quiet'))
    restart(basedirs, shutdown_mode, wal_senders, quiet)
