'''
scm dependencies-list [path]

print a list of dependencies for the given object.

Examples:
    scm dependencies-list 'pkg/logic.if-S02OJ7RPEKLQ97I2/'
        print a list of dependencies of an package
    scm sandbox '<logic.if-S02OJ7RPEKLQ97I2>'
        another way to reference an object

'''

from schematic.scm_cli_utils import _inspect, drvobj_to_nixref
from schematic.utils import cli


def _dependencies_list(path):
    data = _inspect(path)
    deps = set()
    for obj in data['dependencies'].values():
        env = obj['env']
        if not env.get('guid'):
            continue
        dep = drvobj_to_nixref(obj)
        deps.add(dep) if dep else None
    return deps

def dependencies_list(path: str):
    ''' Print a list of dependencies for the given object. '''
    for dep in sorted(_dependencies_list(path), reverse=True):
        print(dep)

def main(argv):
    (pos_args, _, _) = cli.parse_args(argv)
    path: str = cli.get_pos_arg(pos_args, 0, 'path')
    dependencies_list(path)
