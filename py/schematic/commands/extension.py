'''
scm extension [name]

Create files for a new extension object.

Examples:
    scm extension 'foo'
        create an extension named foo

'''
import os
import sys

from schematic import env, fs, guids
from schematic.build_util import mkdir
from schematic.scm_cli_utils import validate_name
from schematic.utils import cli


def create_extension(name):
    validate_name(name)
    guid = guids.get_extension_guid()
    dirname = f'{name}-{guid}'
    ext_path = os.path.abspath(
        os.path.join(env.get_str('ROOT_DIR'), # type: ignore reportAttributeAccessIssue
        'pkg',
        dirname
    ))
    mkdir(ext_path)
    nixout = os.path.join(ext_path, 'default.nix')

    if os.path.exists(nixout):
        print('error, file exists: %s' % nixout, file=sys.stderr)
        sys.exit(1)

    fs.write(nixout, f'''
stdargs @ {{ scm, pkgs, ... }}:

scm.extension {{
    guid = "{guid}";
    name = "{name}";
    upgrade_sql = ./upgrade.sql;
    dependencies = [ ];
    settings = {{
        shared_preload_libraries = "{name}";
    }};
    preBuildInputs = {{ pkgs, ... }}: [
        (pkgs.callPackage ./extension.nix stdargs)
    ];
}}\n'''.lstrip())

    sqlout = os.path.join(ext_path, 'upgrade.sql')

    if os.path.exists(sqlout):
        print('error, file exists: %s' % sqlout, file=sys.stderr)
        sys.exit(1)

    fs.write(sqlout, f'CREATE EXTENSION {name.replace("-", "_")};\n')

    extout = os.path.join(ext_path, 'extension.nix')
    if os.path.exists(extout):
        print('error, file exists: %s' % extout, file=sys.stderr)
        sys.exit(1)

    fs.write(extout, f'''
{{ pkgs, stdenv, postgresql, readline, openssl, zlib, ... }}:

stdenv.mkDerivation rec {{
    name = "{name}";
    src = pkgs.fetchurl {{
        url = "...";
        sha256 = "...";
    }};
    buildInputs = [
        postgresql
    ];
    installPhase = ''
        targetdir=$out/basefiles
        mkdir -p "$targetdir/data"
        install -D {name}.so -t $targetdir/lib/
        install -D {{*.sql,*.control}} -t $targetdir/share/postgresql/extension/
    '';
}}
\n'''.lstrip())

    print('created "<%s>"' % dirname)
    print('    %s' % os.path.relpath(nixout))
    print('    %s' % os.path.relpath(sqlout))
    print('    %s' % os.path.relpath(extout))
    print('hint: Review auto-generated `extension.nix` and `upgrade.sql` to make sure they are correct.')
    return '<%s>' % dirname

def extension(name: str):
    '''Create files for a new extension object.'''
    create_extension(name)

def main(argv):
    (pos_args, _, _) = cli.parse_args(argv)
    name: str = cli.get_pos_arg(pos_args, 0, 'name')
    extension(name)
