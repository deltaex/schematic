'''
scm sync [pguri] [pguri]

Use logical replication to stream changes from one postgresql database to another.
 Replicates a subset of tables and columns.
 Expects to be run again after it exits. To schedule it on cron, use the `scm cron` command.
 Send signal SIGUSR1 (10) to request a graceful shutdown at the next convenient point.

Options:
    --yaml
        Path to YAML file defining tables to replicate. Use "scm sync-conf" to generate.'
    --replication-role {'replica', 'origin', 'local'}
        Sets PostgreSQL session_replication_role setting.
        default: 'replica'
    --time-limit
        Stop processing updates after running this many seconds.
        default: 0
    --slotname
        Name of the PostgreSQL replication slot (created on src server).
    --pubname
        Name of the PostgreSQL publication/replication slot (created on src server). Defaults to slotname.
    --subname
        Name of the PostgreSQL subscription (created at dst server). Defaults to slotname.
    --onconflict {'do nothing', 'do update'}
        Opt to skip an update for existing records. Useful for ad-hoc synchronization to backfill missing records, but not touch existing records.
        default: 'do update'

Examples:
    scm sync 'postgresql://user1:pass@localhost:port/dbname2' 'postgresql://user2:pass@localhost:port/dbname2'

'''
import sys

import psycopg2

from schematic import pgcn, pgsync, pgyaml, slog
from schematic.utils import cli


def _sync(src, dst, yaml, replication_role, slotname, pubname, subname, time_limit, onconflict):
    tablesubs = pgyaml.cons_tablesubs(yaml)
    with pgcn.connect(src) as pg_src, pgcn.connect(dst) as pg_dst:
        pgsync.sync(
            pg_src, pg_dst, slotname, pubname, subname, tablesubs, replication_role=replication_role,
            time_limit=time_limit, onconflict=onconflict)

def sync(src: str, dst: str, yaml, replication_role: str, slotname: str, pubname: str | None, subname: str | None,
    time_limit: int, onconflict: str):
    ''' Use logical replication to stream changes from one postgresql database to another.
        Replicates a subset of tables and columns.
        Expects to be run again after it exits. To schedule it on cron, use the `scm cron` command.
        Send signal SIGUSR1 (10) to request a graceful shutdown at the next convenient point.

        :src: eg postgresql://user:pass@localhost:port/dbname
        :dst: eg postgresql://user:pass@localhost:port/dbname
    '''
    try:
        _sync(src, dst, yaml, replication_role, slotname, pubname or slotname, subname or slotname, time_limit,
              onconflict)
    except (psycopg2.errors.AdminShutdown, psycopg2.OperationalError) as exc:  # pylint: disable=no-member
        slog.error2('error: %s, %s, %r', exc, exc, exc.args)
        raise SystemExit(1) from exc
    except psycopg2.errors.InsufficientPrivilege:  # pylint: disable=no-member
        slog.error2(f'replication role {replication_role} requires superuser')
        slog.error2((  # noqa: UP034
            'hint: replication role "replica" is faster because triggers and foreign key constraints are '
            'skipped, but replication role "origin" can be used without superuser access.'))
        sys.exit(2)

def main(argv):
    (pos_args, _, options) = cli.parse_args(argv,
        options=('--yaml', '--replication-role', '--time-limit', '--slotname', '--pubname', '--subname', '--onconflict')
    )
    src: str = cli.get_pos_arg(pos_args, 0, 'src')
    dst: str = cli.get_pos_arg(pos_args, 1, 'dst')
    yaml: str = cli.get_option_str(options, names=('--yaml',), required=True) # type: ignore reportAssignmentType
    replication_role: str = cli.get_option_str(options, names=('--replication-role',)) or 'replica'
    time_limit: int = cli.get_option_int(options, names=('--time-limit',)) or 0
    slotname: str = cli.get_option_str(options, names=('--slotname',), required=True) # type: ignore reportAssignmentType
    pubname: str | None = cli.get_option_str(options, names=('--pubname',),)
    subname: str | None = cli.get_option_str(options, names=('--subname',),)
    onconflict: str = cli.get_option_str(options, names=('--onconflict',)) or 'do update'

    sync(src, dst, yaml, replication_role, slotname, pubname, subname, time_limit, onconflict)
