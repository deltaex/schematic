'''
scm namespace [name]

Create files for a new namespace (ie "CREATE SCHEMA") object.
 The terms "schema" and "namespace" refer to different concepts in Schematic. Schematic uses the
 term "schema" to refer to a package that contains sql statements, and "namespace" for the qualified prefix that
 schema objects are nested under. PostgreSQL also uses the term "namespace" to refer to the prefix that objects
 are nested under, but externally it often uses the term "schema" to mean the same thing (eg "CREATE SCHEMA").

It is recommended to create a schematic namespace object instead of creating a schema package with a
 "CREATE SCHEMA" command in it. By doing so, adding a dependency on a namespace package will automatically
 add it to the search_path so that objects can be referred to without the namespace prefix. When one namespace
 depends on another namespace (directly or transitively), it will place them in the correct order in search_path
 (the dependee will occur first, so that it occludes the dependency).

Namespaces objects don't require revisions like schemas do. They are idempotent.

You can not change the name field, if you do, it will create a new namespace using the new name.

Options:
    -c / --comment
        comment to describe what the namespace is for

Examples:
    scm namespace 'foo'
        create files for a namespace called foo
    scm namespace 'foo' --comment 'For foo stuff'
        create a namespace with a comment

'''
import os
import sys

from schematic import env, guids
from schematic.build_util import mkdir
from schematic.scm_cli_utils import escape_double_quote, validate_name
from schematic.utils import cli


def create_namespace(name, comment):
    validate_name(name)
    guid = guids.get_namespace_guid()
    dirname = f'{name}-{guid}'
    namespace_path = os.path.abspath(
        os.path.join(env.get_str('ROOT_DIR'), 'pkg', dirname) # type: ignore reportAttributeAccessIssue
    )
    mkdir(namespace_path)
    nixout = os.path.join(namespace_path, 'default.nix')
    if os.path.exists(nixout):
        print(f'error, file exists: {nixout}', file=sys.stderr)
        sys.exit(1)
    with open(nixout, 'w') as out:
        escaped_comment = escape_double_quote(comment or '')
        out.write(f'''
stdargs @ {{ scm, ... }}:

scm.namespace {{
    guid = "{guid}";
    name = "{name}";
    comment = "{escaped_comment}";
}}\n'''.lstrip())
    print(f'created "<{dirname}>"')
    print(f'    {os.path.relpath(nixout)}')
    return f'<{dirname}>'

def namespace(name: str, comment: str | None):
    ''' Create files for a new namespace (ie "CREATE SCHEMA") object.

        The terms "schema" and "namespace" refer to different concepts in Schematic. Schematic uses the
        term "schema" to refer to a package that contains sql statements, and "namespace" for the qualified prefix that
        schema objects are nested under. PostgreSQL also uses the term "namespace" to refer to the prefix that objects
        are nested under, but externally it often uses the term "schema" to mean the same thing (eg "CREATE SCHEMA").

        It is recommended to create a schematic namespace object instead of creating a schema package with a
        "CREATE SCHEMA" command in it. By doing so, adding a dependency on a namespace package will automatically
        add it to the search_path so that objects can be referred to without the namespace prefix. When one namespace
        depends on another namespace (directly or transitively), it will place them in the correct order in search_path
        (the dependee will occur first, so that it occludes the dependency).

        Namespaces objects don't require revisions like schemas do. They are idempotent.

        You can not change the name field, if you do, it will create a new namespace using the new name.
    '''
    create_namespace(name, comment)

def main(argv):
    (pos_args, _, options) = cli.parse_args(argv, options=('-c', '--comment'))
    name: str = cli.get_pos_arg(pos_args, 0, 'name')
    comment: str | None = cli.get_option_str(options, names=('-c', '--comment'))
    namespace(name, comment)
