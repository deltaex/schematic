'''
scm gc-basedir [path] [path] ...

Irreversibly delete a database.

Flags:
    -s / --stop
        stop database if it's running (otherwise leave it be).
    -f / --force
        remove a non-sandbox database (prompting for safety).

Examples:
    scm gc-basedir ~/var/pg/example_database-D0J6PLZEYV46LZA8
        permanently deleting a specific database
    scm gc-basedir '<example_database-D0J6PLZEYV46LZA8>'
        another way to reference a database
    scm gc-basedir ~/var/pg/example_database-D0J6PLZEYV46LZA8 --stop
        permanently deleting a running database
    scm gc-basedir ~/var/pg/example_database-D0J6PLZEYV46LZA8 --force
        deleting a non-sandbox database
    scm gc-basedir ~/var/pg/example_database-D0J6PLZEYV46LZA8 --force --stop
        deleting a running, non-sandbox database

'''
import json
import sys

from schematic.scm_cli_utils import _gc_basedir
from schematic.utils import cli


def gc_basedir(basedirs: list[str], stop: bool, force: bool):
    '''Irreversibly delete a database.'''
    for basedir in basedirs:
        try:
            _gc_basedir(basedir, stop=stop, force=force, verbose=True, raise_exc=True)
        except FileNotFoundError:
            sys.exit(2)
        except (json.JSONDecodeError, KeyError):
            sys.exit(3)
        except RuntimeError:
            sys.exit(4)

def main(argv):
    (pos_args, flags, _) = cli.parse_args(argv, flags=('-s', '--stop', '-f', '--force'))
    basedirs = pos_args
    stop = cli.get_flag(flags, names=('-s', '--stop'))
    force = cli.get_flag(flags, names=('-f', '--force'))
    gc_basedir(basedirs, stop, force)
