'''
scm upgrade [path]

Create or upgrade a PostgreSQL database.

    A new database will be created declaratively. An existing database will be upgraded with an intelligent diff
    against a temporary declarative database.

Flags:
    -v / --verbose
        enable verbose output to stderr
    -n / --no-psql
        don't automatically open psql to the new database

Options:
    --tablespaces {0,1}
        install symlinks for tablespaces; defaults off (0) to avoid errors related to missing directories or permissions
    --pg-upgrade {'yes', 'no', 'ask'}
        whether to automatically upgrade the database (yes), ask for permission
         (ask) or skip the upgrade step (no) when the server's definition file
         default.nix specifies a major PostgreSQL version higher than the
         currently installed.
         default: 'no'
    --pg-upgrade-hardlink {0, 1}
        whether to use hardlinks instead of copying files to the new Postgres cluster.
        default: 0
    --pg-upgrade-slots-no-wait {0, 1}
        whether to wait for replication slots to catch up.
         CAUTION: this could lead to data loss for replication consumers.
         default: 0
    --allow-restart {'yes', 'no'}
        whether to automatically restart the database's server if any applied changes require a restart
         default: 'no'

Env Vars:
    SCM_INSTALL_TABLESPACES
        enables --tablespaces
    SCM_VERBOSE
        enables --verbose

Examples:
    scm upgrade srv/example_db-D0J6PLZEYV46LZA8
        upgrade an existing database
    scm upgrade '<example_db-D0J6PLZEYV46LZA8>'
        another way to reference a database

'''
# ruff: noqa: FBT001
import os
import sys

from schematic import env
from schematic.scm_cli_utils import (
    EvalException, _derivative_nix, _gc_basedir, eval_module, get_pid, get_postgres_process,
    maybe_open_psql, resolve_basedir, validate_nix_module)
from schematic.utils import cli


def upgrade(
        path: str, pg_upgrade_hardlink: bool, pg_upgrade_slots_no_wait: bool, allow_restart: str,
        pg_upgrade: str, tablespaces: str, no_psql: bool, verbose: bool=False):
    ''' Create or upgrade a PostgreSQL database.

        A new database will be created declaratively. An existing database will be upgraded with an intelligent diff
        against a temporary declarative database.
    '''
    nix_module = validate_nix_module(path)
    ref_basedir = None
    if tgt_basedir := resolve_basedir(nix_module):
        pidfile = os.path.join(tgt_basedir, 'data/postmaster.pid')
        get_postgres_process(get_pid(pidfile), pidfile)  # remove pidfile if it's invalid
    # When creating database for first time, do a declarative build; after that a derivative upgrade.
    # A declarative upgrade is faster and less surprising behavior to the user, and exactly matches what they see in a
    #  sandbox database, whereas derivative upgrades run revisions (not relevant for a new databse), optimize for
    #  uptime, avoid destructive changes.
    upgrade_mode = 'derivative' if tgt_basedir else 'declarative'
    env = {
        'SCM_INSTALL_TABLESPACES': str(tablespaces),
        'SCM_UPGRADE_MODE': upgrade_mode,
        'SCM_ALLOW_RESTART': allow_restart}
    if upgrade_mode == 'derivative':
        try:
            refobj = _derivative_nix(nix_module, verbose=False)
        except EvalException as ex:
            print('error: failed to create declarative database', file=sys.stderr)
            print(f'process {ex.proc.name} failed with code {ex.exitcode}', file=sys.stderr)
            if ex.main_obj and ex.main_obj.get('env') and ex.main_obj['env'].get('basedir'):
                _gc_basedir(ex.main_obj['env']['basedir'], stop=True, force=True, verbose=False)
            sys.exit(1)
        if not refobj or 'env' not in refobj or 'basedir' not in refobj['env']:
            sys.exit(2)
        env['SCM_DERIVATIVE_REF_BASEDIR'] = ref_basedir = refobj['env']['basedir']
    try:
        upgobj = eval_module(
            path, verbose=verbose, environ=env,
            hardlink=pg_upgrade_hardlink,
            slots_no_wait=pg_upgrade_slots_no_wait,
            pg_upgrade=pg_upgrade)
    finally:
        _gc_basedir(ref_basedir, stop=True, verbose=False)
    maybe_open_psql(upgobj['env']['basedir'], no_psql)

def main(argv):
    (pos_args, flags, options) = cli.parse_args(
        argv,
        flags=('-v', '--verbose', '-n', '--no-psql'),
        options=('--tablespaces', '--pg-upgrade', '--pg-upgrade-hardlink', '--pg-upgrade-slots-no-wait',
        '--allow-restart'),
    )
    path: str = cli.get_pos_arg(pos_args, 0, 'path')
    tablespaces: str = env.get_str('SCM_INSTALL_TABLESPACES') or cli.get_option_str(options, '--tablespaces') or '1'
    pg_upgrade_hardlink = cli.get_option_str(options, '--pg-upgrade-hardlink') == '1'
    pg_upgrade_slots_no_wait: bool = cli.get_option_str(options, '--pg-upgrade-slots-no-wait') == '1'
    allow_restart: str = cli.get_option_str(options, '--allow-restart') or 'no'
    pg_upgrade: str = cli.get_option_str(options, '--pg-upgrade') or 'no'
    no_psql: bool = cli.get_flag(flags, ('-n', '--no-psql'))
    verbose: bool = env.get_str('SCM_VERBOSE') == '1' or cli.get_flag(flags, ('-v', '--verbose'))

    upgrade(path, pg_upgrade_hardlink, pg_upgrade_slots_no_wait, allow_restart, pg_upgrade, tablespaces,\
        no_psql, verbose)
