'''
scm derivative-diff [path] [pguri]

Print the DDL statements that would be run with an derivative upgrade, but don't apply them.

Flags:
    -v / --verbose
        Enable verbose output to stderr

Examples:
    scm derivative-diff ~/var/pg/toy_db-S1FK55PDKN6REATR 'postgresql://user2:pass@localhost:port/dbname2' 'tablename'

'''
import sys

from schematic import derivative, pgcn
from schematic.scm_cli_utils import EvalException, _derivative_nix, _gc_basedir, validate_nix_module
from schematic.utils import cli


def derivative_diff(path: str, pguri: str, verbose: bool):
    ''' Print the DDL statements that would be run with an derivative upgrade, but don't apply them.'''
    nix_module = validate_nix_module(path)
    try:
        refobj = _derivative_nix(nix_module, verbose=verbose)
    except EvalException as ex:
        print('error: failed to create declarative database', file=sys.stderr)
        print(f'process {ex.proc.name} failed with code {ex.exitcode}', file=sys.stderr)
        if ex.main_obj and ex.main_obj.get('env') and ex.main_obj['env'].get('basedir'):
            _gc_basedir(ex.main_obj['env']['basedir'], stop=True, force=True, verbose=False)
        sys.exit(1)
    if not refobj or 'env' not in refobj or 'basedir' not in refobj['env']:
        sys.exit(2)
    ref_pguri = refobj['env']['pguri']
    try:
        with pgcn.connect(pguri) as pg_tgt, pgcn.connect(ref_pguri) as pg_ref:
            derivative.add_all(pg_ref, pg_tgt, action=derivative.action_dryrun, validate_constraints=False)
    finally:
        _gc_basedir(refobj['env']['basedir'], stop=True, force=True, verbose=False)

def main(argv):
    (pos_args, flags, _) = cli.parse_args(argv, flags=('-v', '--verbose'))
    path: str = cli.get_pos_arg(pos_args, 0, 'path')
    pguri: str = cli.get_pos_arg(pos_args, 1, 'pguri')
    verbose: bool = cli.get_flag(flags, names=('-v', '--verbose'))
    derivative_diff(path, pguri, verbose)
