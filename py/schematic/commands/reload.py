'''
scm reload [path] [path] ...

Reload specified database server.
 This allows changing of configuration-file options that do not require a complete restart to take effect.

Flags:
    -q / --quiet
        Quiet mode. Updates only the log file. No output is shown

Examples:
    scm reload ~/var/pg/example_database-D0J6PLZEYV46LZA8
        reloading a specific database
    scm reload '<example_database-D0J6PLZEYV46LZA8>'
        another way to reference a database
    scm reload ~/var/pg/example_database-D0J6PLZEYV46LZA8 ~/var/pg/other_database-N6W6KLZUCV46LVZY
        reloading multiple databases at once

'''
from schematic.scm_cli_utils import get_basedir, pg_ctl_fn, resolve_basedirs
from schematic.utils import cli


def reload(basedirs: list[str], quiet: bool):
    '''
    Reload specified database server.
     This allows changing of configuration-file options that do not require a complete restart to take effect.
    '''
    for bdir in resolve_basedirs(basedirs):
        pg_ctl_fn(get_basedir(bdir), 'reload', quiet=quiet)

def main(argv):
    (pos_args, flags, _) = cli.parse_args(argv, flags=('-q', '--quiet'))
    basedirs = pos_args
    quiet = cli.get_flag(flags, names=('-q', '--quiet'))
    reload(basedirs, quiet)
