'''
scm inspect [path]

Inspect a schematic module, printing out dependencies and their metadata.

Flags:
    -a / --all
        include all depenedencies, including pure software

Examples:
    scm inspect pkg/postgis-E06AX6C50XJAUCVV
        print out the dependencies of a schematic module
    scm inspect '<postgis-E06AX6C50XJAUCVV>'
        another way to reference a package

'''
import json
import os

from schematic.scm_cli_utils import (
    get_relative_nix_mod, instantiate_module, parse_drv, validate_nix_module)
from schematic.utils import cli


def _inspect(path, include_all=False):
    nix_module = validate_nix_module(path)
    environ = {
        'SCM_SANDBOX_MODULES': os.path.abspath(nix_module),
    }
    main_drv = instantiate_module(get_relative_nix_mod('lib/sandbox.nix'), environ=environ)
    all_drvs = parse_drv(main_drv, recursive=True)
    if not include_all:
        all_drvs = {drv: obj for drv, obj in all_drvs.items() if obj['env'].get('scm_type')}
    return {'meta': {'drv_path': main_drv, 'drv_obj': all_drvs[main_drv]}, 'dependencies': all_drvs}

def inspect(path: str, all: bool):
    '''Inspect a schematic module, printing out dependencies and their metadata.'''
    data = _inspect(path, include_all=all)
    print(json.dumps(data, indent=4))

def main(argv):
    (pos_args, flags, _) = cli.parse_args(argv, flags=('-a', '--all'))
    path = cli.get_pos_arg(pos_args, 0, 'path')
    all_ = cli.get_flag(flags, ('-a', '--all'))
    inspect(path, all_)
