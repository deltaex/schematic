'''
scm sandbox [package] [package] ...

Create a temporary sandbox database from 0+ input packages.

Flags:
    -v / --verbose
        enable verbose output to stderr
    -n / --no-psql
        don't automatically open psql to the new database
    -k / --kill
        destroy the database immediately after building (useful for testing)

Options:
    -V / --version version
        specify a PostgreSQL version to use
    --tablespaces {0,1}
        install symlinks for tablespaces; defaults off (0) to avoid errors related to missing directories or permissions
        default: 0

Env Vars:
    SCM_VERBOSE
        enables --verbose
    SCM_INSTALL_TABLESPACES
        enables --tablespaces

Examples:
    scm sandbox
        create an empty sandbox database
    scm sandbox -V '17'
        create an empty sandbox database using PostgreSQL 17
    scm sandbox -V '16.1'
        create an empty sandbox database using PostgreSQL 16.1
    scm sandbox pkg/logic.if-S02OJ7RPEKLQ97I2/
        create an empty sandbox database with a package installed
    scm sandbox '<logic.if-S02OJ7RPEKLQ97I2>'
        another way to reference a package
    scm sandbox '<logic.if-S02OJ7RPEKLQ97I2>' '<hstore-S0UQM5WIU4BB05A8>'
        mutiple packages accepted

'''
import json
import os
import re
import sys

from schematic import env, prompt_yesno
from schematic.scm_cli_utils import (
    EvalException, _gc_basedir, _sandbox, maybe_open_psql, pretty_basedir, unexpand_user)
from schematic.utils import cli


def is_supported_postgresql_version(version: str) -> bool:
    '''
    >>> assert is_supported_postgresql_version('16.2')
    >>> assert not is_supported_postgresql_version('16.99')
    >>> assert is_supported_postgresql_version('16')
    >>> assert is_supported_postgresql_version('9.6.9')
    >>> assert is_supported_postgresql_version('9.6')
    '''
    if not re.match(r'^\d+(\.\d+)?(\.\d+)?(alpha\d*)?(beta\d*)?(rc\d*)?$', version):
        return False
    scm_path: str = env.get_str('SCM_PATH') # type: ignore[reportGeneralTypeIssues]
    with open(os.path.join(scm_path, 'lib/postgresql/postgresql-releases.json')) as fp:
        releases = json.load(fp)
    versions = {r['version'] for r in releases}
    if version in versions:
        return True
    versions |= {'.'.join(v.split('.')[:-1]) for v in versions}
    if version in versions:
        return True
    return False

def sandbox(
        paths: list[str], version: str | None, *, tablespaces: int, no_psql: bool, kill: bool, verbose: bool = False):
    '''Create a temporary sandbox database from a package.'''
    if version and not is_supported_postgresql_version(version):
        print(f'error: unsupported postgresql version: {version}', file=sys.stderr)
        sys.exit(17)
    try:
        drvobj = _sandbox(paths, verbose=verbose, scm_install_tablespaces=tablespaces, version=version)
    except EvalException as ex:
        print('error: failed to create sandbox database', file=sys.stderr)
        print(f'process {ex.proc.name} failed with code {ex.exitcode}', file=sys.stderr)
        if ex.main_obj and ex.main_obj.get('env') and ex.main_obj['env'].get('basedir'):
            _gc_basedir(ex.main_obj['env']['basedir'], stop=True, force=True, verbose=verbose)
        sys.exit(1)
    if not drvobj or 'env' not in drvobj or 'basedir' not in drvobj['env']:
        sys.exit(2)
    basedir = drvobj['env']['basedir']
    maybe_open_psql(basedir, no_psql)
    if kill:
        _gc_basedir(basedir, stop=True, verbose=verbose)
        sys.exit(0)
    if not no_psql:
        consent = prompt_yesno('remove sandbox database %s?' % unexpand_user(basedir), default=True)
        if not consent:
            sys.exit(0)
        _gc_basedir(basedir, stop=True, verbose=verbose)
    else:
        print('remove database: scm gc-basedir -s %s' % pretty_basedir(basedir), file=sys.stderr)

def main(argv):
    (pos_args, flags, options) = cli.parse_args(
        argv, ('-v', '--verbose', '-n', '--no-psql', '-k', '--kill'), ('-V', '--version', '--tablespaces'))
    paths = pos_args
    tablespaces: int = env.get_int('SCM_INSTALL_TABLESPACES') or cli.get_option_int(options, ('--tablespaces',)) or 0
    no_psql: bool = cli.get_flag(flags, ('-n', '--no-psql'))
    kill: bool = cli.get_flag(flags, ('-k', '--kill'))
    version: str | None = cli.get_option_str(options, ('--version', '-V'))
    verbose: bool = env.get_str('SCM_VERBOSE') == '1' or cli.get_flag(flags, ('-v', '--verbose'))
    sandbox(list(paths), version, tablespaces=tablespaces, no_psql=no_psql, kill=kill, verbose=verbose)
