'''
scm export-certificates [path]

Exports the certificates of the database specified as a zip file in the current directory.

Examples:
    scm export-certificates ~/var/pg/toy_db-S1FK55PDKN6REATR

'''
import os
import zipfile

from schematic import env, fs
from schematic.utils import cli

export_certificates_readme = fs.read(
    os.path.join(env.get_str('ROOT_DIR'), 'etc/export-certificates-README.md') # type: ignore reportAttributeAccessIssue
)

def export_certificates(db_dir):
    '''Exports the certificates of the database specified as a zip file in the current directory.'''
    db_name = os.path.basename(db_dir)
    zip_name = f'{db_name}-certificates'
    zip_path = os.path.join(os.getcwd(), f'{zip_name}.zip')

    with zipfile.ZipFile(zip_path, 'w', zipfile.ZIP_DEFLATED) as zipf:
        zipf.writestr(f'{zip_name}/README.md', export_certificates_readme)
        ssl_dir = os.path.join(db_dir, 'data/ssl')
        server_crt_path = os.path.join(ssl_dir, 'server.crt')
        clients_dir = os.path.join(ssl_dir, 'clients')
        clients = [file.split('.crt')[0] for file in os.listdir(clients_dir) if file.endswith('.crt')]
        for client in clients:
            client_crt_path = os.path.join(clients_dir, f'{client}.crt')
            client_key_path = os.path.join(clients_dir, f'{client}.key')
            zipf.write(server_crt_path, f'{zip_name}/{client}/root.crt')
            zipf.write(client_crt_path, f'{zip_name}/{client}/postgresql.crt')
            zipf.write(client_key_path, f'{zip_name}/{client}/postgresql.key')
    print(f'Exported certificates to \'{zip_name}.zip\'')

def main(argv):
    (pos_args, _, _) = cli.parse_args(argv)
    db_dir: str = cli.get_pos_arg(pos_args, 0, 'db_dir')
    export_certificates(db_dir)
