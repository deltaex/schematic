'''
scm factorize [path]

Create one new schema per table present in @schema, removing tables and related objects from schema's upgrade.sql.
Removes the table definitions and related objects from the schema's declarative definition, creates a new schema
 with these definitions, and adds a dependency to the new schema.


This "factorize" utility is a proof of concept, pre-alpha.
WARNING: mutates source schema's upgrade.sql, ensure that it's backed up.

Options:
    -t / --table
        regexp search for server to attach with -a, or the name of a new server

Flags:
    -r / --revision
        create initial revisions for a new schemas
    --keep-failed
        keep changes even when a schema fails to build

Examples:
    scm factorize pkg/foo_schema-S06B7CF0KH4X04JB

'''
import json
import os
import shutil
import subprocess
import sys

import pglast

from schematic import fs, pguri, sqlparse
from schematic.scm_cli_utils import (
    _gc_basedir, _inspect, _sandbox, create_revision, create_schema, drvobj_to_nixref, file_search,
    get_immediate_deps, get_nix_module, get_nix_module_dir, update_schema_deps)
from schematic.utils import cli


def safe_get(data, *keys):
    '''
    Access @*keys in nested @data dictionary and return `None` instead of `KeyError`.

    >>> safe_get(None, 'x')
    >>> safe_get({}, 'x')
    >>> safe_get({'x': 1}, 'x')
    1
    >>> safe_get({'x': {'y': 2}}, 'x', 'y')
    2
    >>> safe_get({'x': {'y': ''}}, 'x', 'y')
    ''
    >>> safe_get({'x': [{'y': 'z'}]}, 'x', 0, 'y')
    'z'
    >>> safe_get({'x': [{'y': 'z'}]}, 'x', '0', 'y')
    >>> safe_get(['a'], 1)
    '''
    val = data
    for key in keys:
        if val is None:
            return
        try:
            val = val[key]
        except (TypeError, IndexError, KeyError):
            val = None
    return val

def _parse_fk_ref_table(statement):
    '''
    >>> _parse_fk_ref_table('ALTER TABLE ONLY public.f34 ADD CONSTRAINT sdf FOREIGN KEY (f34256) REFERENCES \
public.fj349_r435h(id) NOT VALID;')
    'public.fj349_r435h'
    >>> _parse_fk_ref_table('ALTER TABLE ONLY f34 ADD CONSTRAINT SDF FOREIGN KEY (g456) REFERENCES foo.gh456_ert(id) \
NOT VALID;')
    'foo.gh456_ert'
    '''
    p = json.loads(pglast.parser.parse_sql_json(statement))
    ref = safe_get(p, 'stmts', 0, 'stmt', 'AlterTableStmt', 'cmds', 0, 'AlterTableCmd', 'def', 'Constraint', 'pktable')
    if ref:
        if 'schemaname' in ref:
            schemaname, relname = ref['schemaname'], ref['relname']
            return f'{schemaname}.{relname}'
        return ref['relname']

def get_fk_reference_module(fk_statement, nix_paths=os.environ.get('NIX_PATH')):
    tableref = _parse_fk_ref_table(fk_statement)
    if not tableref:
        return
    files = file_search(
        r'CREATE\s+.*TABLE\s+%s\W' % tableref,
        nix_paths.split(':'), # type: ignore reportAttributeAcccessIssue
        'sql', case_sensitive=False
    )
    for file_match in files:
        mod_name = os.path.dirname(file_match)
        mod_metadata = _inspect(mod_name)
        if mod_metadata and mod_metadata['meta']['drv_obj']['env'].get('scm_type') == 'schema':
            return mod_name

def get_pg_dump(pguri, tablename):
    cmd = [
        'pg_dump', '--no-owner', '--schema-only', '--no-privileges',
        '-d', str(pguri),
        '-t', tablename,
    ]
    return subprocess.check_output(cmd, text=True)

def _factorize_log_statement(prefix, statement):
    repl = statement.replace('\n', '')
    print((f'{prefix}: {repl}')[:120] + '', file=sys.stderr)

def factorize(schema: str, table: str | None, revision: bool, keep_failed: bool):
    '''
    Create one new schema per table present in @schema, removing tables and related objects from schema's upgrade.sql.

    WARNING: mutates source schema's upgrade.sql, ensure that it's backed up.

    Removes the table definitions and related objects from the schema's declarative definition, creates a new schema
    with these definitions, and adds a dependency to the new schema.

    This "factorize" utility is a proof of concept, pre-alpha.

    TODO: would save a bunch of manual work if it automatically determined dependencies between the new schemas. This
    would most likely be done by inspecting the catalog tables of the sandbox database created for the parent schema.
    '''
    schema_dir: str = get_nix_module_dir(schema) # type: ignore reportAssignmentType
    upgrade_sql = os.path.join(schema_dir, 'upgrade.sql')
    schema_metadata = _inspect(schema)
    schema_deps = list(filter(
        None, [drvobj_to_nixref(d) for d in get_immediate_deps(schema_metadata, scm_types=('schema',))]))
    tablenames = [table] if table else list(sqlparse.parse_create_table_names(fs.try_read(upgrade_sql, default='')))
    try:
        drv = _sandbox([schema], verbose=False)
    except subprocess.CalledProcessError:
        print('%s: schema build failed' % schema, file=sys.stderr)
        sys.exit(0)
    sandbox_uri = pguri.pguri_from_basedir(drv['env']['basedir'])
    for tablename in tablenames:
        if not tablename.strip():
            continue
        statement_matches = []
        failed_match = did_funcs = False
        src_text: str = fs.try_read(upgrade_sql, default='') # type: ignore reportAssignmentType
        for ddl_statement in sqlparse.parse_statements(get_pg_dump(sandbox_uri, tablename)):
            stmt_txt = ddl_statement.text
            if sqlparse.is_create_trigger(stmt_txt):
                '''
                Two reasons for skipping triggers:
                1. They depend on functions which aren't included in the pg_dump output for a given table (this could
                be worked around).
                2. There's a judgement call for which schema trigger might belong, so skipping leaves it to a human.
                '''
                continue
            if not did_funcs and (sqlparse.is_create_index(stmt_txt) or sqlparse.is_add_constraint(stmt_txt)):
                # constraints and indices may depend on functions, let's add the functions first
                for func_statement in sqlparse.find_table_funcs(src_text, tablename):
                    statement_matches.append(func_statement)
                    _factorize_log_statement('MATCHED', func_statement)
                did_funcs = True
            match_statement = sqlparse.find_statement_match(src_text, stmt_txt)
            if not match_statement:
                failed_match = True
                _factorize_log_statement('UNMATCHED', stmt_txt)
                continue
            _factorize_log_statement('MATCHED', match_statement)
            statement_matches.append(match_statement)
        if failed_match and not keep_failed:
            print('skipping %s because of failed statement matches' % tablename, file=sys.stderr)
            continue
        if statement_matches:
            newschema = create_schema(sqlparse.schemaname(tablename))
            newschema_deps = []
            newschema_dir: str = get_nix_module_dir(newschema) # type: ignore reportAssignmentType
            with open(os.path.join(newschema_dir, 'upgrade.sql'), 'a') as new_upgrade_sql:
                for sttmnt in statement_matches:
                    new_upgrade_sql.write(sttmnt)
                    if not sttmnt.endswith('\n'):
                        new_upgrade_sql.write('\n')
                    src_text = src_text.replace(sttmnt, '')
                    fk_ref_module = get_fk_reference_module(sttmnt)
                    if fk_ref_module:
                        if get_nix_module(fk_ref_module) != get_nix_module(schema):
                            _factorize_log_statement('INFERRED', f'{newschema} depends on {fk_ref_module}')
                            newschema_deps.append(fk_ref_module)
            if newschema_deps:
                update_schema_deps(get_nix_module(newschema), newschema_deps)
            # build new schema
            new_basedir = None
            try:
                drv = _sandbox([newschema], verbose=False)
                new_basedir = drv['env']['basedir']
            except subprocess.CalledProcessError:
                print('%s: schema build failed' % newschema, file=sys.stderr)
            create_revision(newschema, verbose=True) if new_basedir and revision else None
            if new_basedir or keep_failed:
                # update default.nix to add a dependency on the new schema
                schema_deps.append(get_nix_module(newschema)) # type: ignore reportAttributeAcccessIssue
                update_schema_deps(get_nix_module(schema), schema_deps)
                # update upgrade.sql to remove the extracted statements
                with open(upgrade_sql, 'w') as fp:
                    fp.write(src_text)
            shutil.rmtree(newschema_dir) if not new_basedir and not keep_failed else None
            _gc_basedir(new_basedir, stop=True) if new_basedir else None

def main(argv):
    (pos_args, flags, options) = cli.parse_args(argv, flags=('-r', '--revision', '--keep-failed'), options=('-t', '--table'))
    schema: str = cli.get_pos_arg(pos_args, 0, 'schema')
    table: str | None = cli.get_option_str(options, names=('-t', '--table'))
    revision: bool = cli.get_flag(flags, names=('-r', '--revision'))
    keep_failed: bool = cli.get_flag(flags, names=('--keep-failed',))
    factorize(schema, table, revision, keep_failed)
