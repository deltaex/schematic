'''
scm tablespace [name] [path]

Create files for a new tablespace object.

    Use this instead of a schema because tablespaces require setup on the filesystem.
    Tablespace objects don't require revisions like schemas do. They are idempotent.
    You can change the following fields in the default.nix and the changes will be performed automatically:
    - comment
    - seq_page_cost
    - random_page_cost
    - effective_io_concurrency
    - maintenance_io_concurrency

    You can not change the name field. If you do, it will create a new tablespace using the new name.
    If you change the location, it will cause a build failure because this change cannot be made against a running
    server. To resolve this, stop postgresql, make the change manually, and start postgresql. Then the build will
    succeed.

Options:
    -c / --comment
        comment to describe what the tablespace is for

Examples:
    scm tablespace 'example_tablespace' .
        Create a tablespace in the current directory

'''
import os
import sys

from schematic import env, guids
from schematic.build_util import mkdir
from schematic.scm_cli_utils import escape_double_quote, validate_name
from schematic.utils import cli


def create_tablespace(name: str, location: str, comment: str | None):
    validate_name(name)
    guid = guids.get_tablespace_guid()
    dirname = f'{name}-{guid}'
    tablespace_path = os.path.abspath(
        os.path.join(env.get_str('ROOT_DIR'),'pkg', dirname) # type: ignore reportAttributeAccessIssue
    )
    mkdir(tablespace_path)
    nixout = os.path.join(tablespace_path, 'default.nix')
    if os.path.exists(nixout):
        print('error, file exists: %s' % nixout, file=sys.stderr)
        sys.exit(1)
    with open(nixout, 'w') as out:
        out.write('''
stdargs @ { scm, ... }:

scm.tablespace {
    guid = "%s";
    name = "%s";
    location = "%s";
    comment = "%s";
    seq_page_cost = null;
    random_page_cost = null;
    effective_io_concurrency = null;
    maintenance_io_concurrency = null;
}\n'''.lstrip() % (guid, name, escape_double_quote(location), escape_double_quote(comment or '')))
    print('created "<%s>"' % dirname)
    print('    %s' % os.path.relpath(nixout))
    return '<%s>' % dirname

def tablespace(name: str, location: str, comment: str | None):
    ''' Create files for a new tablespace object.

        Use this instead of a schema because tablespaces require setup on the filesystem.
        Tablespace objects don't require revisions like schemas do. They are idempotent.
        You can change the following fields in the default.nix and the changes will be performed automatically:
        - comment
        - seq_page_cost
        - random_page_cost
        - effective_io_concurrency
        - maintenance_io_concurrency

        You can not change the name field. If you do, it will create a new tablespace using the new name.
        If you change the location, it will cause a build failure because this change cannot be made against a running
        server. To resolve this, stop postgresql, make the change manually, and start postgresql. Then the build will
        succeed.
    '''
    create_tablespace(name, location, comment)

def main(argv):
    (pos_args, _, options) = cli.parse_args(argv, options=('-c', '--comment'))
    name: str = cli.get_pos_arg(pos_args, 0, 'name')
    location: str = cli.get_pos_arg(pos_args, 1, 'location')
    comment: str | None = cli.get_option_str(options, names=('-c', '--comment'))
    tablespace(name, location, comment)
