'''
scm schema [name]

Create files for a new schema object.

Examples:
    scm schema 'foo'
        create a schema package called foo

'''
from schematic.scm_cli_utils import create_schema
from schematic.utils import cli


def schema(name):
    '''Create files for a new schema object.'''
    create_schema(name)

def main(argv):
    name = cli.get_pos_arg(argv, 0, 'name')
    create_schema(name)
