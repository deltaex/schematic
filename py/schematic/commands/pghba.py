'''
scm pghba [name]

Create a package for a new pghba object which generates a section in a pg_hba.conf file.

    "HBA" stands for "host-based authentication". The pg_hba.conf file has a list of rules for validating client
    authentication requests. See: https://www.postgresql.org/docs/current/auth-pg-hba-conf.html

    Instead of maintaining a pg_hba.conf file directly, use these pghba packages to generate it. This enables
    composability by splitting the configuration into smaller logical packages that can be combined together for
    a given database instead of overwriting eachother. It automatically sends a reload signal to postgresql when the
    configuration changes, and performs additional validation checks before doing so.

    pghba objects don't require revisions; they are idempotent.

Options:
    -c / --comment
        comment to describe what the namespace is for

Examples:
    scm pghba 'foo_config'
        create pghba object called foo_config
    scm pghba 'foo_config' --comment 'configures foo'
        create pghba object with a comment

'''
import os
import sys

from schematic import env, guids
from schematic.build_util import mkdir
from schematic.scm_cli_utils import escape_double_quote, validate_name
from schematic.utils import cli


def create_pghba(name, comment):
    validate_name(name)
    guid = guids.get_pghba_guid()
    dirname = f'{name}-{guid}'
    pghba_path = os.path.abspath(
        os.path.join(env.get_str('ROOT_DIR'), 'pkg', dirname) # type: ignore reportAttributeAccessIssue
    )
    mkdir(pghba_path)
    nixout = os.path.join(pghba_path, 'default.nix')
    if os.path.exists(nixout):
        print(f'error, file exists: {nixout}', file=sys.stderr)
        sys.exit(1)
    with open(nixout, 'w') as out:
        out.write('''
stdargs @ { scm, ... }:

scm.pghba {
    guid = "%s";
    name = "%s";
    comment = "%s";
    rules = [
        {
            type = "host";
            dbname = "all";
            user = "all";
            addr = "127.0.0.1/32, ::1/128, ...";
            auth = "trust, reject, scram-sha-256, md5, ...";
            opts = "";
            comment = "...";
        }
    ];
    dependencies = [];
}\n'''.lstrip() % (guid, name, escape_double_quote(comment or '')))
    print('created "<%s>"' % dirname)
    print('    %s' % os.path.relpath(nixout))
    print('    docs: https://www.postgresql.org/docs/current/auth-pg-hba-conf.html')
    print('    hint: for multiple rules, separate by whitespace (not comma)')
    return '<%s>' % dirname

def pghba(name, comment):
    ''' Create a package for a new pghba object which generates a section in a pg_hba.conf file.

        "HBA" stands for "host-based authentication". The pg_hba.conf file has a list of rules for validating client
        authentication requests. See: https://www.postgresql.org/docs/current/auth-pg-hba-conf.html

        Instead of maintaining a pg_hba.conf file directly, use these pghba packages to generate it. This enables
        composability by splitting the configuration into smaller logical packages that can be combined together for
        a given database instead of overwriting eachother. It automatically sends a reload signal to postgresql when the
        configuration changes, and performs additional validation checks before doing so.

        pghba objects don't require revisions; they are idempotent.
    '''
    create_pghba(name, comment)

def main(argv):
    (pos_args, _, options) = cli.parse_args(argv, options=('-c', '--comment'))
    name: str = cli.get_pos_arg(pos_args, 0, 'name')
    comment: str | None = cli.get_option_str(options, names=('-c', '--comment'))
    pghba(name, comment)
