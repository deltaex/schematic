'''
scm oplog-ls [s3uri]

List oplog files, optionally offset from a tsn.

Options:
    -t / --tsn
        Exclude files that are before this TSN. If the TSN falls between two file names,
        include the previous file (treating it as a range). E.g. 00011AE4904CEB18.

Examples:
    scm oplog-ls 's3://some-bucket/SUBDIR/'

'''
from schematic import oplog_s3, pglsn
from schematic.utils import cli


def ol_ls(s3uri: str, tsn):
    ''' List oplog files, optionally offset from a tsn.
        :s3uri: eg s3://some-bucket/SUBDIR/. Lists all files in SUBDIR.
        :tsn: eg 00011AE4904CEB18. Exclude files that are before this TSN.
            If the TSN falls between two file names, include the previous file (treating it as a range).
    '''
    if tsn:
        tsn = pglsn.lsn_to_tsn(tsn) if pglsn.is_lsn(tsn) else tsn
    for olf in oplog_s3.ls(s3uri, tsn=tsn):
        print('%12.d bytes    %s    %s' % (olf.size, olf.modified, olf.path))

def main(argv):
    (pos_args, _, options) = cli.parse_args(argv, options=('-t', '--tsn'))
    s3uri = cli.get_pos_arg(pos_args, 0, 's3uri')
    tsn: str | None = cli.get_option_str(options, names=('-t', '--tsn'))
    ol_ls(s3uri, tsn)
