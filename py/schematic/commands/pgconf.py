'''
scm pgconf [name]

    Create a package for a new pgconf object which sets options in a postgresql.conf file.

    Instead of handling postgresql.conf files directly, use these pgconf packages to automatically generate them. This
    allows us to validate settings, share configuration between databases, and compose multiple configuration packages
    together into one database.

    If two pgconf packages are added to one database with conflicting settings, the resolution will be deterministic (
    it won't vary over time or between databases). The conflict is resolved by sorting the package names
    alphanumerically. To force a package to take priority over the over, add the other pgconf package as a dependency.

    Some settings require a postgresql restart to take effect. Output to stderr will indicate when this is needed but
    it won't be done automatically to avoid service disruption.

    Settings that aren't supported for this postrgresql version are automatically omitted. This prevents postgresql from
    refusing to start when an unrecognized setting is specified, makes transitioning between versions less error-prone,
    and enables using the same pgconf packages between multiple databases of different versions.

    If the shared_buffers setting is specified, it should be set to a value intended for production, which is presumably
    higher than your dev environment can support. If it's set too high, postgresql will refuse to start. A safety
    mechanism is present that will cap the shared_buffers setting as a fraction of your total memory so that you can
    launch an instance of the database locally for testing. When this happens, a log message is output letting you know
    that your shared_buffers setting was not respected. By default the fraction is 25% of total memory. If you want to
    override this, define the setting shared_buffers_max_pct, eg: `shared_buffers_max_pct = 33.33;`.

    pgconf objects don't require revisions; they are idempotent.

Options:
    -c / --comment
        comment to describe what this postgresql config is for

Examples:
    scm 'foo'
        create files for a namespace called foo
    scm namespace 'foo' --comment 'For foo stuff'
        create a namespace with a comment

'''
import os
import sys

from schematic import env, guids
from schematic.build_util import mkdir
from schematic.scm_cli_utils import escape_double_quote, validate_name
from schematic.utils import cli


def create_pgconf(name, comment):
    validate_name(name)
    guid = guids.get_pgconf_guid()
    dirname = f'{name}-{guid}'
    pgconf_path = os.path.abspath(
        os.path.join(env.get_str('ROOT_DIR'), 'pkg', dirname) # type: ignore reportAttributeAccessIssue
    )
    mkdir(pgconf_path)
    nixout = os.path.join(pgconf_path, 'default.nix')
    if os.path.exists(nixout):
        print(f'error, file exists: {nixout}', file=sys.stderr)
        sys.exit(1)
    with open(nixout, 'w', encoding='utf8') as out:
        out.write('''
stdargs @ { scm, ... }:

scm.pgconf {
    guid = "%s";
    name = "%s";
    comment = "%s";
    settings = {
        # setting_name = "value";
        # other_setting = "other_value";
    };

    dependencies = [];
}\n'''.lstrip() % (guid, name, escape_double_quote(comment or '')))
    print('created "<%s>"' % dirname)
    print('    %s' % os.path.relpath(nixout))
    print('    docs: https://www.postgresql.org/docs/current/runtime-config.html')
    print('    hint: if you need to include a # in a string value, escape it with \\#')
    return '<%s>' % dirname



def pgconf(name: str, comment: str | None):
    '''
    Create a package for a new pgconf object which sets options in a postgresql.conf file.

    Instead of handling postgresql.conf files directly, use these pgconf packages to automatically generate them. This
    allows us to validate settings, share configuration between databases, and compose multiple configuration packages
    together into one database.

    If two pgconf packages are added to one database with conflicting settings, the resolution will be deterministic (
    it won't vary over time or between databases). The conflict is resolved by sorting the package names
    alphanumerically. To force a package to take priority over the over, add the other pgconf package as a dependency.

    Some settings require a postgresql restart to take effect. Output to stderr will indicate when this is needed but
    it won't be done automatically to avoid service disruption.

    Settings that aren't supported for this postrgresql version are automatically omitted. This prevents postgresql from
    refusing to start when an unrecognized setting is specified, makes transitioning between versions less error-prone,
    and enables using the same pgconf packages between multiple databases of different versions.

    If the shared_buffers setting is specified, it should be set to a value intended for production, which is presumably
    higher than your dev environment can support. If it's set too high, postgresql will refuse to start. A safety
    mechanism is present that will cap the shared_buffers setting as a fraction of your total memory so that you can
    launch an instance of the database locally for testing. When this happens, a log message is output letting you know
    that your shared_buffers setting was not respected. By default the fraction is 25% of total memory. If you want to
    override this, define the setting shared_buffers_max_pct, eg: `shared_buffers_max_pct = 33.33;`.

    pgconf objects don't require revisions; they are idempotent.
    '''
    create_pgconf(name, comment)

def main(argv):
    (pos_args, _, options) = cli.parse_args(argv, options=('-c', '--comment'))
    name = cli.get_pos_arg(pos_args, 0, 'name')
    comment = cli.get_option_str(options, names=('-c', '--comment'))
    pgconf(name, comment)
