'''
scm start [path] [path] ...

Start specified database server.

Flags:
    -q / --quiet
        Quiet mode. Updates only the log file. No output is shown

Examples:
    scm start ~/var/pg/example_database-D0J6PLZEYV46LZA8
        starting a specific database
    scm start '<example_database-D0J6PLZEYV46LZA8>'
        another way to reference a database
    scm start ~/var/pg/example_database-D0J6PLZEYV46LZA8 ~/var/pg/other_database-N6W6KLZUCV46LVZY
        starting multiple databases at once

'''
from schematic.scm_cli_utils import get_basedir, pg_ctl_fn, resolve_basedirs
from schematic.utils import cli


def start(basedirs: list[str], quiet: bool):
    '''Start specified database server.'''
    for bdir in resolve_basedirs(basedirs):
        pg_ctl_fn(get_basedir(bdir), 'start', quiet=quiet)

def main(argv):
    (pos_args, flags, _) = cli.parse_args(argv, flags=('-q', '--quiet'))
    basedirs = pos_args
    quiet = cli.get_flag(flags, names=('-q', '--quiet'))
    start(basedirs, quiet)
