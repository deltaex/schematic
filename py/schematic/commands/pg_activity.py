'''
scm pg-activity

Open pg_activity for a specific database.

Examples:
    scm pg-activity

'''
import subprocess

from schematic.pguri import pguri_from_basedir
from schematic.scm_cli_utils import get_basedir
from schematic.utils import cli


def pg_activity(basedir):
    '''Open pg_activity for a specific database.'''
    basedir = get_basedir(basedir)
    pguri = pguri_from_basedir(basedir)

    command = ['pg_activity', str(pguri)]
    subprocess.run(command, check=False)

def main(argv):
    (pos_args, _, _) = cli.parse_args(argv)
    basedir: str = cli.get_pos_arg(pos_args, 0, 'basedir')
    pg_activity(basedir)
