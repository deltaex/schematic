'''
scm rename [path] [path]

Rename object (pkg, rev, srv, ...) from @source to @dest and update references.
 Updates source files destructively!

Examples:
   scm rename pkg/foo_schema-S06B7CF0KH4X04JB pkg/bar_schema-S06B7CF0KH4X04JB
        rename an object from foo_schema to bar_schema

'''
import os
import re
import sys

from schematic import fs
from schematic.scm_cli_utils import file_search, path_relative
from schematic.utils import cli


def is_local_scm_obj(path):
    ''' Is :path: a schematic object it the current working directory? '''
    if not path:
        return False
    path = path_relative(path, '.')
    objtypes = ['pkg', 'srv', 'rev']
    return any(path.startswith('./%s/' % objtype) for objtype in objtypes)

def nix_path_list(nix_path=os.environ.get('NIX_PATH')):
    ret = []
    for path in nix_path.split(':'): # type: ignore reportAttributeAccessIssue
        if '=' in path:
            _, _, path = path.partition('=')  # noqa: PLW2901

        if os.path.isabs(path):
            ret.append(path)
    return ret

def replace_path_name(source_name, dest_name, content):
    '''
    >>> source_name = '2020-03-17-kighluinbimfoobi-account'
    >>> dest_name = 'account-2020-03-17-kighluinbimfoobi'
    >>> replace_path_name(source_name, dest_name, 'dependencies = [ <2020-03-17-kighluinbimfoobi-account> ]; ')
    'dependencies = [ <account-2020-03-17-kighluinbimfoobi> ]; '
    >>> replace_path_name(source_name, dest_name, 'dependencies = [ <rev/2020-03-17-kighluinbimfoobi-account> ]; ')
    'dependencies = [ <rev/account-2020-03-17-kighluinbimfoobi> ]; '
    >>> replace_path_name(source_name, dest_name, 'dependencies = [ ./rev/2020-03-17-kighluinbimfoobi-account ]; ')
    'dependencies = [ ./rev/account-2020-03-17-kighluinbimfoobi ]; '
    >>> replace_path_name(source_name, dest_name, 'name = "2020-03-17-kighluinbimfoobi-account"; ')
    'name = "account-2020-03-17-kighluinbimfoobi"; '
    >>> replace_path_name(source_name, dest_name, 'name = "2020-03-17-kighluinbimfoobi-account"; ')
    'name = "account-2020-03-17-kighluinbimfoobi"; '
    '''
    content = re.sub(r'\<%s\>' % source_name, '<%s>' % dest_name, content)
    content = re.sub(r'\<((.+/))%s\>' % source_name, lambda m: f'<{m.group(1)}{dest_name}>', content)
    content = re.sub(r'"%s"' % source_name, '"%s"' % dest_name, content)
    content = re.sub(r'/%s(\s)' % source_name, '/' + dest_name + r'\1', content)
    return content  # noqa: RET504

def rename(source: str, dest: str):
    ''' Rename object (pkg, rev, srv, ...) from @source to @dest and update references.
        Updates source files destructively!
    '''
    if not is_local_scm_obj(source):
        print('source is not a schematic object: %s' % source, file=sys.stderr)
        sys.exit(5)
    if not is_local_scm_obj(dest):
        print('dest is not a schematic object: %s' % dest, file=sys.stderr)
        sys.exit(5)
    assert os.path.isdir(source), 'source must be a directory: %s' % source
    source = path_relative(source, '.')
    dest = path_relative(dest, '.')
    source_name = os.path.basename(source)
    dest_name = os.path.basename(dest)
    os.rename(source, dest)
    for filename in file_search(r'\W%s\W' % source_name, nix_path_list(), 'nix'):
        oldcontent = content = fs.try_read(filename, default='')
        content = replace_path_name(source_name, dest_name, content)
        if oldcontent == content:
            continue
        try:
            with open(filename, 'w') as fp:
                fp.write(content)
            print('updated %s' % filename, file=sys.stderr)
        except PermissionError as ex:
            if ex.errno == 13:
                print('readonly %s' % filename, file=sys.stderr)
                continue
            raise

def main(argv):
    (pos_args, _, _) = cli.parse_args(argv)
    source = cli.get_pos_arg(pos_args, 0, 'source')
    dest = cli.get_pos_arg(pos_args, 1, 'dest')
    rename(source, dest)
