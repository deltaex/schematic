'''
scm revision [path]

Create files for a new revision object to migrate the specified schema.

Flags:
    -v / --verbose
        enables verbose output to stderr
    -r / --recursive
        also create revisions for dependency schemas without any.

Env Vars:
    SCM_VERBOSE
        enables --verbose

Examples:
    scm revision pkg/foo_schema-S06B7CF0KH4X04JB
        create revision object to migrate a schema
    scm revision '<foo_schema-S06B7CF0KH4X04JB>'
        another way to reference a schema
    scm revision pkg/foo_schema-S06B7CF0KH4X04JB --recursive
        also create revisions for dependency schemas without any

'''

from schematic import env
from schematic.scm_cli_utils import (
    _inspect, create_revision, drvobj_to_nixref, get_immediate_deps, get_nix_module_dir)
from schematic.utils import cli


def get_transitive_schemas(inspect_metadata, cursor=None):
    ''' Yield dependent schemas, depth-firsth. '''
    cursor = cursor or inspect_metadata['meta']['drv_path']
    cursor_obj = inspect_metadata['dependencies'][cursor]
    for sub_drv in cursor_obj['inputDrvs']:
        if sub_drv not in inspect_metadata['dependencies']:
            continue  # pure software dependency
        sub_obj = inspect_metadata['dependencies'][sub_drv]
        if sub_obj['env']['scm_type'] == 'schema':
            yield from get_transitive_schemas(inspect_metadata, sub_drv)
            yield sub_obj

def get_transitive_schemas_unique(inspect_metadata, cursor=None):
    ''' Yield dependent schemas, depth-firsth, de-duplicated. '''
    seen = set()
    for dep in get_transitive_schemas(inspect_metadata, cursor=cursor):
        nixref = drvobj_to_nixref(dep)
        if nixref and nixref not in seen:
            seen.add(nixref)
            yield dep

def revision(schema_path: str, verbose: bool, recursive: bool):
    '''Create files for a new revision object to migrate the specified schema.'''
    if recursive:
        schema_metadata = _inspect(schema_path)
        for dep in get_transitive_schemas_unique(schema_metadata):
            if any(get_immediate_deps(schema_metadata, cursor=dep['drv_path'], scm_types=('revision',))):
                continue
            subpath = get_nix_module_dir(drvobj_to_nixref(dep))
            if not subpath or subpath.startswith('/nix/store'):
                continue  # don't attempt to create revisions for immutable 3rd party dependencies in nix store
            create_revision(subpath, verbose=verbose)
    create_revision(schema_path, verbose=verbose)

def main(argv):
    (pos_args, flags, _) = cli.parse_args(argv, flags=('-v', '--verbose', '-r', '--recursive'))
    schema_path: str = cli.get_pos_arg(pos_args, 0, 'schema_path')
    verbose: bool = env.get_str('SCM_VERBOSE') == '1' or cli.get_flag(flags, names=('-v', '--verbose'))
    recursive: bool = cli.get_flag(flags, names=('-r', '--recursive'))
    revision(schema_path, verbose, recursive)
