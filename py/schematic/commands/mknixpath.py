'''
scm mknixpath

Read SCM_REPOS env var, iterate sub-directories and output a value for NIX_PATH.

Examples:
    # outupt a value for NIX_PATH
    scm mknixpath

'''
import os
import sys

from schematic import env


def _unique_list(thelist):
    suppress = set()
    for item in thelist:
        if item in suppress:
            continue
        suppress.add(item)
        yield item

def mknixpath():
    ''' Read SCM_REPOS env var, iterate sub-directories and output a value for NIX_PATH. '''
    scm_repos = env.get_str('SCM_REPOS', '')
    assert scm_repos != None, 'could not access environment variable: "SCM_REPOS"'
    nix_paths = []
    for scm_repo in scm_repos.split(':'):
        if not scm_repo:
            continue
        if not os.path.exists(scm_repo):
            print('repo does not exist: %s' % scm_repo, file=sys.stderr)
            continue
        scm_repo = os.path.realpath(scm_repo) # noqa: PLW2901
        if not os.path.isdir(scm_repo):
            print('repo is not a directory: %s' % scm_repo, file=sys.stderr)
            continue
        for subpath in ('srv', 'pkg', 'rev'):
            abspath = os.path.join(scm_repo, subpath)
            if os.path.isdir(abspath):
                nix_paths.append(abspath)
    nix_path = env.get_str('NIX_PATH')
    if nix_path:
        nix_paths.extend(nix_path.split(':'))
    print(':'.join(list(_unique_list(nix_paths))))

def main(argv):                # noqa: ARG001
    mknixpath()
