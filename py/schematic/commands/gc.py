'''
scm gc

Clean up objects, including deleting obsolete sandbox databases from ~/var/pg.
 Database must be stopped and specify istemp = true in meta.json.

Flags:
    -v / --verbose
        enable verbose output to stderr

Env Vars:
    SCM_VERBOSE
        enable --verbose

Examples:
    scm gc
        clean up objects

'''
import glob
import os
import stat
import subprocess
import sys

from schematic import env
from schematic.scm_cli_utils import _gc_basedir, has_pidfile, ls_basedirs, unexpand_user
from schematic.utils import cli


def ls_tmuxservers(scm_tmux=os.environ['SCM_TMUX']):
    return glob.glob(os.path.join(scm_tmux, '*'))

def tmux_server_is_dead(socket_path):
    try:
        return not subprocess.check_output(['lsof', socket_path]).strip()
    except subprocess.CalledProcessError:
        return True

def remove_tmux_socket(socket_path):
    if tmux_server_is_dead(socket_path):
        print('removing socket %s' % unexpand_user(socket_path), file=sys.stderr)
        try:
            subprocess.check_call(['rm', socket_path])
        except subprocess.CalledProcessError:
            pass

def is_socket(path):
    s = os.stat(path)
    return stat.S_ISSOCK(s.st_mode)

def _gc(verbose):
    for (basedir, _) in ls_basedirs():
        if not has_pidfile(basedir):  # don't gc a running database
            _gc_basedir(basedir, verbose=verbose)
    for socket_path in ls_tmuxservers():
        if not is_socket(socket_path):
            continue
        remove_tmux_socket(socket_path)

def gc(verbose):
    '''
    Clean up objects, including deleting obsolete sandbox databases from ~/var/pg.

    Database must be stopped and specify istemp = true in meta.json.
    '''
    _gc(verbose)

def main(argv: list[str]):
    (_, flags, _) = cli.parse_args(argv, flags=('-v', '--verbose'))
    verbose = env.get_str('SCM_VERBOSE') == '1' or cli.get_flag(flags, names=('-v', '--verbose'))
    gc(verbose)
