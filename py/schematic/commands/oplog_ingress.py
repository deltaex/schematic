'''
scm oplog-ingress [s3uri] [pguri]

Receive oplog update feed into postgresql.

Flags:
    --force-sequential
        Use sequential mode even if parallel is available

Options:
    --tsn
        Skip to this TSN. Useful for debugging. Do not use for production databases as it will create
         inconsistenciesthat are impractical to fix.
    --replication-role {'replica', 'origin', 'local'}
        Sets PostgreSQL session_replication_role setting.
        default: 'replica'
    --time-limit
        Stop processing new files after running this many seconds.
        default: 0
    --max-workers
        Maximum number of parallel ingestion workers. A lower number saves memory.
         A high number may make the subscription faster.
        default: 100

Examples:
    scm oplog-ingress 's3://some-bucket/some-prefix' 'postgresql://user:pass@localhost:port/dbname?application_name=oplog-ingress'

'''
import sys

import psycopg2

from schematic import oplog_ingress, pglsn, slog
from schematic.utils import cli


def ol_ingress(s3uri: str, pguri: str, force_sequential: bool, tsn: str | None, replication_role: str, time_limit: int, max_workers: int):
    ''' Receive oplog update feed into postgresql.
        :s3uri: eg s3://some-bucket/some-prefix
        :pguri: eg postgresql://user:pass@localhost:port/dbname?application_name=oplog-ingress
    '''
    try:
        if tsn:
            tsn = pglsn.lsn_to_tsn(tsn) if pglsn.is_lsn(tsn) else tsn
        oplog_ingress.ingress(s3uri, pguri, tsn=tsn, replication_role=replication_role,
                              time_limit=time_limit, force_sequential=force_sequential,
                              max_workers=max_workers)
    except (psycopg2.errors.AdminShutdown, psycopg2.OperationalError) as exc:  # pylint: disable=no-member
        print('error: %s' % type(exc).__name__, file=sys.stderr)
        for arg in exc.args:
            print('%s' % arg, file=sys.stderr)
        sys.exit(1)
    except psycopg2.errors.InsufficientPrivilege:  # pylint: disable=no-member
        slog.error2(f'replication role {replication_role} requires superuser')
        slog.error2((  # noqa: UP034
            'hint: replication role "replica" is faster because triggers and foreign key constraints are '
            'skipped, but replication role "origin" can be used without superuser access.'))
        sys.exit(2)

def main(argv):
    (pos_args, flags, options) = cli.parse_args(
        argv, flags=('--force-sequential',),
        options=('--tsn', '--replication-role', '--time-limit', '--max-workers')
    )
    s3uri: str = cli.get_pos_arg(pos_args, 0, 's3uri')
    pguri: str = cli.get_pos_arg(pos_args, 1, 'pguri')
    force_sequential: bool = cli.get_flag(flags, names=('--force-sequential',))
    tsn: str | None = cli.get_option_str(options, names=('--tsn'))
    replication_role: str = cli.get_option_str(options, names=('--replication-role')) or 'replica'
    time_limit: int = cli.get_option_int(options, names=('--time-limit')) or 0
    max_workers: int = cli.get_option_int(options, names=('--max-workers')) or 100

    ol_ingress(s3uri, pguri, force_sequential, tsn, replication_role, time_limit, max_workers)
