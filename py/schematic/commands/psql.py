'''
scm psql [path]

Open psql shell to specified database.

Examples:
    scm upgrade srv/example_db-D0J6PLZEYV46LZA8
        open a  psql shell to an existing database
    scm upgrade '<example_db-D0J6PLZEYV46LZA8>'
        another way to reference a database

'''
from schematic.scm_cli_utils import get_basedir, psql_from_basedir
from schematic.utils import cli


def psql(basedir: str):
    '''Open psql shell to specified database.'''
    basedir = get_basedir(basedir)
    psql_from_basedir(basedir)

def main(argv):
    basedir = cli.get_pos_arg(argv, 0, 'basedir')
    psql(basedir)
