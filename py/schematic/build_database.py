import os
import random
import re
import subprocess
import sys

from schematic import derivative, env, fs, pgcn, settings_util, slog
from schematic.build_pgconf import fmt_setting
from schematic.build_pghba import fmt_rule
from schematic.build_util import (
    append_newline, cleanup_old_sections, ensure_pg_running, fmt_footer, fmt_header,
    install_buildinput_files, parse_postgresql_drv_version, pg_ctl_fn, split_postgresql_version)
from schematic.pguri import pguri_from_basedir

key_valid_days = 36500
ssl_folder = 'ssl'
clients_folder = 'clients'
server_key = 'server.key'
server_crt = 'server.crt'
clients_crt = 'clients.crt'


def add_merged_section(conf_filepath):
    sect_name, guid = 'settings-automerge', '0000000000000000'
    conf_content = fs.try_read(conf_filepath)
    merged_settings = settings_util.merge_settings(conf_content)

    section = append_newline(fmt_header(guid, sect_name, comment=None, depends=None))
    for name, value in merged_settings.items():
        section += append_newline(fmt_setting({'name': name, 'value': value}))
    section += append_newline(fmt_footer(guid, sect_name))

    fs.write(conf_filepath, section, mode='a')

def apply_derivative():
    if env.get_str('SCM_UPGRADE_MODE') != 'derivative':
        return
    autocommit = env.get_bool('autocommit')
    pguri = str(pguri_from_basedir(env.get_str('SCM_DERIVATIVE_REF_BASEDIR')))
    with pgcn.connect(pguri, autocommit=autocommit) as pg_ref, \
            pgcn.connect(env.get_str('pguri'), autocommit=autocommit) as pg_tgt:
        if pg_tgt.execute('SELECT pg_is_in_recovery();').scalar():
            print('read-only, skipping schema upgrades', file=sys.stderr)
            return
        derivative.add_all(pg_ref, pg_tgt, action=derivative.action_logexec, validate_constraints=False)
        pg_tgt.commit()

def generate_certificate(basedir, cname, key_type, key_folder =''):
    datadir = os.path.join(basedir, 'data')
    ssldir = os.path.join(datadir, ssl_folder)
    keydir = os.path.join(ssldir, key_folder)
    if os.path.exists(ssldir):
        subprocess.check_call(['chmod', '+rwx', ssl_folder], cwd=datadir)
    if not os.path.exists(keydir):
        fs.mkdirs(keydir, fsync=False)
    if not os.path.exists(os.path.join(keydir, '%s.crt' % key_type)):
        subprocess.check_call([
            'openssl', 'req', '-new', '-x509', '-days', str(key_valid_days), '-nodes', '-text',
            '-keyout', '%s.key' % key_type, '-out', '%s.crt' % key_type, '-subj', '/CN=%s/' % cname], cwd=keydir,
            stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL)


def group_certificates(basedir, rolnames):
    ssldir = os.path.join(basedir, 'data', ssl_folder)
    base_clientcrt_path = os.path.join(ssldir, clients_crt)
    root_content = ''
    if os.path.exists(base_clientcrt_path):
        with open(base_clientcrt_path, 'r', encoding='utf8') as fs_root:
            fs_root.seek(0)
            root_content = fs_root.read()
    for role in rolnames:
        search_string = 'Issuer:\\s*CN\\s*=\\s*%s\n' % role
        pattern = re.compile(search_string)
        if not re.search(pattern, root_content):
            generate_certificate(basedir, role, role, clients_folder)
            clientcrt_file = os.path.join(ssldir, clients_folder, '%s.crt' % role)
            if os.path.exists(base_clientcrt_path):
                if not os.access(base_clientcrt_path, os.W_OK):
                    subprocess.check_call(['chmod', '+rwx', base_clientcrt_path])
            with open(clientcrt_file, 'rb') as f_clientcrt, open(base_clientcrt_path, 'ab') as f_base_clientcrt:
                clientcrt_content = f_clientcrt.read()
                f_base_clientcrt.write(clientcrt_content)

def add_ssl_settings(basedir, conf_filepath):
    datadir = os.path.join(basedir, 'data')
    ssl_serverkey = os.path.join(ssl_folder, server_key)
    ssl_servercrt = os.path.join(ssl_folder, server_crt)
    ssl_clientscrt = os.path.join(ssl_folder, clients_crt)

    sect_name, guid = 'settings-ssl', '0000000000000000'
    settings = append_newline(fmt_header(guid, sect_name, comment=None, depends=None))
    if os.path.exists(os.path.join(datadir, ssl_serverkey)):
        settings += append_newline(fmt_setting({'name': 'ssl', 'value': 'on'}))
        settings += append_newline(fmt_setting({'name': 'ssl_key_file', 'value': ssl_serverkey}))
    if os.path.exists(os.path.join(datadir, ssl_servercrt)):
        settings += append_newline(fmt_setting({'name': 'ssl_cert_file', 'value': ssl_servercrt}))
    if os.path.exists(os.path.join(datadir, ssl_clientscrt)):
        settings += append_newline(fmt_setting({'name': 'ssl_ca_file', 'value': ssl_clientscrt}))
    settings += append_newline(fmt_footer(guid, sect_name))
    fs.write(conf_filepath, settings, mode='a')

def pghba_ssl_conf(basedir):
    filepath =  os.path.join(basedir, 'data/pg_hba.conf')
    hostsslip4 = fmt_rule({'type': 'hostssl', 'dbname': 'all', 'user': 'all',
                           'addr': '0.0.0.0/0', 'auth': 'md5', 'opts': 'clientcert=verify-full'})
    hostsslip6 = fmt_rule({'type': 'hostssl', 'dbname': 'all', 'user': 'all',
                           'addr': '::/0', 'auth': 'md5', 'opts': 'clientcert=verify-full'})
    with open(filepath, 'r+', encoding='utf8') as fp:
        if hostsslip4 not in fp.read():
            fp.seek(0, 2)
            fp.write(hostsslip4 + '\n')
            fp.write(hostsslip6 + '\n')


def has_ssl_in_conf(basedir):
    datadir = os.path.join(basedir, 'data')
    files = os.listdir(datadir)
    conf_files = [file for file in files if file.endswith('.conf')]
    # ssl = on, ssl=on, ssl = 'on'
    ssl_pattern = re.compile(r'ssl\s*=\s*(?:\'|\")?(on|off)(?:\'|\")?', re.IGNORECASE)
    # #ssl = on, #ssl = 'off'
    cmnt_ssl = re.compile(r'#\s*ssl\s*=\s*(?:\'|\")?(on|off)(?:\'|\")?', re.IGNORECASE)
    for file_name in conf_files:
        file_path = os.path.join(datadir, file_name)
        with open(file_path, 'r') as file:
            data = file.read()
            if ssl_pattern.search(data) and not cmnt_ssl.search(data):
                return True
    return False


def generate_cname(length=12):
    chars = 'abcdefghijklmnopqrstuvwxyz0123456789'
    return ''.join(random.choice(chars) for _ in range(length))


def main():
    install_buildinput_files()
    if env.get_bool('SCM_PG_UPGRADE'):
        return
    apply_derivative()
    # Remove orphan config sections from removed pgconfs and pghbas
    basedir = env.get_str('basedir')
    cleanup = [('.applied_pgconfs', 'data/postgresql.conf'),
               ('.applied_pghbas', 'data/pg_hba.conf')]
    for dotfile, conffile in cleanup:
        guids_path = os.path.join(basedir, dotfile)
        conf_path = os.path.join(basedir, conffile)
        cleanup_old_sections(conf_path, guids_path)

    conf_filepath = os.path.join(basedir, 'data/postgresql.conf')
    # Handle postgresql.conf settings that need merging by adding a generated settings section at
    # the bottom with the merged values. This section is wiped out every time by `cleanup_old_sections`
    # so it's always generated from scratch with the latest values.
    add_merged_section(conf_filepath)

    if not has_ssl_in_conf(basedir):
        generate_certificate(basedir, generate_cname(), 'server', '')
        with pgcn.connect(env.get_str('pguri'), autocommit=True) as pg:
            rolnames = pg.execute('SELECT rolname FROM pg_roles WHERE rolcanlogin IS true;').all()
            group_certificates(basedir, rolnames)
        subprocess.check_call(['chmod', 'go-rwx', 'server.key'], cwd=os.path.join(basedir, 'data', ssl_folder))
        add_ssl_settings(basedir, conf_filepath)
        pghba_ssl_conf(basedir)
        pg_ctl_fn(basedir, 'reload', quiet=True)

    # Do any new settings require a restart?
    is_new_database = fs.try_unlink(os.path.join(basedir, '.new_db'))
    can_restart = is_new_database or env.get_bool('SCM_ALLOW_RESTART')
    pgversion = split_postgresql_version(parse_postgresql_drv_version())
    if pgversion < [9, 5]:
        need_restart = False
    else:
        with pgcn.connect(env.get_str('pguri'), autocommit=True) as pg:
            need_restart = pg.execute('SELECT name FROM pg_settings WHERE pending_restart').all()

    if need_restart:
        if can_restart:
            slog.info2('Restarting database to apply pending setting changes...')
            pg_ctl_fn(basedir, 'restart', quiet=True)
            ensure_pg_running(basedir, env.get_str('port'), poll_interval=0.1)
        else:
            slog.warn2('\n=================================================================================\n'
                       'WARNING: the following settings changed but need a server restart to be applied:\n'
                       '  ' + ', '.join(x.name for x in need_restart) + '\n'
                       '=================================================================================\n')


if __name__ == '__main__':
    main()
