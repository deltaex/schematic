Schematic is a package manager for PostgreSQL. Use Schematic to declaratively define databases from packages of schema, configuration, and PostgreSQL extensions. 

## Declarative Schema

Schema packages only need to define the target state and migrations are automatically inferred. There's no need to define schema migrations. When performing an upgrade, Schematic figures out the steps needed to get to the desired state by diffing the schema of the target database against the schema defined in code. This isn't hard to do in simple cases, but it's complex to do with 100% feature coverage. Schematic's upgrades are feature complete, including all the niche features of PostgreSQL you've never heard of.

If this feature sounds too good to be true, please try it and see for yourself. Once you start using it, you'll quickly take it for granted and never want to write a migration again. This doesn't just make feature development faster, but it also makes code reviews faster. It eliminates a whole class of code that requires careful peer review. This feature is designed by and for the most cynical, most skeptical, most careful, most pessimistic caretakers of production databases.

## Safe Upgrades

Database upgrades have a well-earned reputation for being risky. It's easy to cause downtime and quality of service issues on production databases. One path towards safety is avoiding migrations, restricting who can write them, doing careful code review, and scheduling maintenance windows. Another path is better tooling. Schematic is designed for safe upgrades of mission-critical production databases. PostgreSQL provides excellent tools for doing live upgrades, but everyone on your team has to be an expert to use them, not be in a hurry, and have to care enough to write migrations defensively. Schematic implements tactics for safe upgrades automatically, lowing the barrier for contributions from new team members without fear of breaking your database.

Some schema changes are too risky to be performed automatically. Schematic will never drop tables or columns automatically. You have to write an explicit revision to do destructive changes. Schematic will not rename tables, columns, or other objects automatically because there's no way to perform these changes without breaking a running application. You can write explicit revisions to do renames. (For production databases, renames can be accomplished without downtime by creating a new table or column, asynchronously copying data to the new version, then dropping the old version once the application no longer depends on it.)

Schematic upgrades are safe enough to be run during the workday, multiple times a day, in rapid development environments that release often.

## Distributed Workflow

Anyone on the team can create a sandbox database in seconds. It's easy to edit the database in your editor and create a new database as an experiment, test your application against it, then throw it away. Unlike workflows that number migrations sequentially, Schematic supports distributed branching workflows without fear of merge conflicts. The distributed workflow is a quality of life improvement, similar to upgrading from SVN to git. When a workflow that's time-consuming and error-prone becomes fast and easy, it changes the way you work. Schematic lowers the barrier to rapid prototyping and experiments. It's easy to test your changes, which means changes get tested more often.

You can structure your database as a set of small packages that are composed together to build a complete database. If you target multiple PostgreSQL databases (eg product, feature, tenant, production environment, geography, legal jurisdiction), this lets you customize them and also share code between them, like tables, functions, configuration, and access control rules. You can factor out a subset of your packages into a separate repository to be shared with other teams, with customers, with vendors, or as open source.

Schematic plays nicely with other tools and workflows for changing schema. If you have a tool that changes the production schema, this won't cause errors at the next upgrade due to unexpected schema. Schematic will notice some changes have happened already and skip them. If there's extra schema you don't want to be managed by Schematic, that's fine too, Schematic will ignore it.

## Maturity

Schematic has been in development since 2017 and used in mission-critical production deployments since 2020. It's used by dozens of companies as part of their data infrastructure to manage databases behind their core products. There's at least thousands of known packages in private repositories. It runs the same PostgreSQL you already know and trust. You're welcome to use it in production, but at this stage it's expected that there will still be occasional breaking changes to the package format. It's recommended to let a maintainer know you're using it so that you can get a notifications if there's going to be a change to the package format (two such changes have happened so far; each was accomplished with a one-liner shell script). The maintainers also appreciate feedback, bug reports, and feature requests.

## Features

Highlights:
* Automatic schema migrations.
* Rapid experimentation with sandbox databases.
* PostgreSQL native extensions.
* Configuration management (postgresql.conf, pg_hba.conf, ...).
* A communal library of packages (extensions, schema, data, code).
* Distributed workflow. Works well with branches.
* Idempotent and reproducible wherever possible.
* Programming language agnostic. No assumption is made of what your primary language is.
* Support for PostgreSQL 12, 13, 14, 15, 16, including all point releases. Limited support for PostgreSQL 8.3 - 11.9 (minimal testing). Support for patching PostgreSQL or using your own fork.

## Implementation

Packages and revisions are defined in the [Nix programming language](https://nixos.org/), and can depend on any of the 60,000+ Nix software packages. Building on top of Nix enables Schematic to support for all PostgreSQL versions and all PostgreSQL extensions, natively on all Linux distributions and MacOS. Using Nix also adds discipline around reproducibility, reliability, and deterministic builds. You don't need any familiarity with Nix to use Schematic. To contribute to Schematic, some familiarity with Nix is beneficial but often not required.

Schematic is primarily implemented in Python. Rust integration is planned for some features that benefit from CPU and memory efficiency. Schematic's design supports implementing packages in any language, but Python is preferred unless there is a constraint forcing the use of another language.

## Installation

Linux and MacOS are supported natively. Windows via WSL.

Clone this git repo onto your computer.

Nix is the only dependency that needs to be installed on your system. Install it by running the shell script `./nix-install.sh` or [following these instructions](https://nixos.org/download.html).

Run `nix develop` to enter the development shell. This is not a container, but a regular bash shell that has environmental variables set up appropriately for development. Please be patient for the first invocation to install dependencies into /nix/store. Subsequent invocations will be fast.

Packages will require other dependencies, but they will only be installed into /nix/store and will not interfere with any existing software on your system. It's safe to install on a development machine or server without risk of overwriting or changing anything.

Installation inside of containers is not advised because Nix expects to run as an unprivileged user instead of as root. It's possible to set up using a Nix-compatible base container, but this isn't necessary if the intention is reproducibility or isolation from system dependencies because these benefits are already provided by Nix.

## Walkthrough

Follow the installation steps first.

Create a database package:
```
scm database example_database
```

This creates a file that defines a new database.

To build a sandbox/test copy of this database:
```
scm sandbox srv/example_database*
```

This will build the new database, including PostgreSQL itself if needed, any dependent packages, and drop you into a psql shell. You'll see there's no schema in there. Exit psql (ctrl-d) so we can define a new schema object:

```
scm schema example_country
```

Note the GUID that was created, you'll use it later. Schematic uses GUIDs because it reduces ambiguity when working with large package repositories, reduces false positive matches when searching for references to packages, and prevents naming conflicts in distributed workflows.

Open the SQL file it generated and enter this:
```
CREATE TABLE example_country (
    id text NOT NULL,
    name text NOT NULL,
    continent text NOT NULL,
    currency_code text,
    currency_name text,
    tld text,
    phone text,
    aliases text[] NOT NULL
);
ALTER TABLE example_country ADD CONSTRAINT country_pkey PRIMARY KEY (id);
CREATE UNIQUE INDEX example_country_name_ix ON example_country USING btree (name);
ALTER TABLE example_country ADD CONSTRAINT example_country_id_upper CHECK (id = upper(id));
ALTER TABLE example_country ADD CONSTRAINT example_country_continent_upper CHECK (continent = upper(continent));

INSERT INTO example_country (id, name, continent, currency_code, currency_name, tld, phone, aliases)
VALUES ('US', 'United States', 'NA', 'USD', 'Dollar', '.us', '1', array['United States of America', 'United States', 'USA', 'US']);
```

We can create a sandbox database with this schema with the command:

```
scm sandbox pkg/example_country-GUID
```

Replace GUID with the correct value. Hit the tab key to complete the GUID instead of typing it manually. Hit enter to run the command.

This will build a database, run the SQL, and open a psql shell for us to experiment with. When you exit the psql shell it will remove this temporary database automatically (with an option to leave it running).

Let's make the database depend on this schema. Open `srv/example_database-GUID/default.nix` and add the line `<example_country-GUID>` to the `dependencies` list (replacing `GUID` with the value generated earlier, leave out the trailing slash). List items are separated by whitespace (no commas); the convention is new lines for readability. The angle brackets are a special notation for a file reference that will look for the file by that name by searching the `NIX_PATH` environmental variable, similar to how your shell finds a program to run by searching for its name in the `PATH` environmental variable. You can also use a relative or absolute file reference, but the angle bracket notation is recommended because it's more flexible with refactoring.

Now you can run the sandbox command on the database and get this schema populated:

```
scm sandbox srv/example_database-GUID
```

That built a new sandbox database and installed the schema package it depends on. Go ahead and exit this psql shell to remove the temporary database. Now let's simulate creating a production database with this definition. Run this command to create the database:

```
scm upgrade srv/example_database-GUID
```

Type `\d example_country` in psql and hit enter. Notice that the table is installed. Exit the psql shell (databases created with `scm upgrade` will not be removed automatically). Now let's simulate a schema change. Open `pkg/example_country-GUID/upgrade.sql` in your editor and add `created_at timestamp NOT NULL DEFAULT now()` to the list of columns. Save your changes. Now run the upgrade command again:

```
scm upgrade srv/example_database-GUID
```

Type `\d example_country` in psql and hit enter. Notice that your change is automatically reflected in the database without having to write a schema migration. This is a simple example of an automatic schema migration, but you can experiment with more complex schema to convince yourself it's capable of handling the complicated cases as well.

You can tidy up when you're done experimenting. First find the running database:

```
scm status
```

Then stop the database:

```
scm stop ~/var/pg/example_database-GUID
```

Then delete it (this is equivalent to `rm -rf` but with a prompt for safety):

```
scm gc-basedir --force ~/var/pg/example_database-GUID
```

## Content Directories

There's three content directories: `srv`, `pkg`, `rev`.

Each of these will contain subdirectories for objects of the given type, and those will contain a `default.nix` file that defines the object. The `default.nix` is implied in filesystem paths, you can reference just the parent directory to be more succinct (eg `pkg/country-S0Y2F1PPW4X10VTW/` instead of `pkg/country-S0Y2F1PPW4X10VTW/default.nix`).

To be even more succinct, you can use a nix notation to reference the object without the relative path by wrapping the object name in angle brackets, eg `<pg_repack-HAHOMTIBILACVICS>` instead of `pkg/pg_repack-HAHOMTIBILACVICS`. These references work like references to programs in your `PATH` environmental variable. Instead of searching `PATH`, they search `NIX_PATH` for the matching object. The unique ID in the names of objects ensures the right object is being matched without risk of ambiguity/conflict. Referencing objects this way is recommended instead of relative or absolute filesystem paths because 1) it makes it easy to move and refactor code, 2) it enables references to third party packages that might be stored in different places relative to your packages, and 3) it enables overriding third party content by using the same object name in your local packages.

### `./srv/`

Short for "server" (or "service" if you prefer to think of a server as an operating system, containing definitions for database servers).

Includes definitions for postgresql databases (ie a database "cluster" in postgresql terminology, a single server instance).

### `./pkg/`

Short for "package", logical components that are composed into databases.

Includes schema definitions. Most often each table and its immediate dependents like indexes and constraints will be bundled into a schema, but a schema can bundle extensions, configuration, utility functions, etc.

### `./rev/`

Short for "revision", stateful/imperative changes made to packages/schemas/databases. Used primarily for destructive or unsafe changes that Schematic won't generate automatically out of caution.

## Create a database package

1. Use `scm database` to create a database (recommended: lowercase ascii without symbols). See `scm database --help`. Suppose it's called `bar-D0Q9HU8QL40ZSWFM`.

2. Review `srv/bar-D0Q9HU8QL40ZSWFM/default.nix`. Consider editing `name`, `dbname`, `port`, `user`, `password`. You'll add schema packages to the `dependencies` list to install a package into this database.

## Create a schema package

1. Use `scm schema` command to create a new schema. See `scm schema --help`. Suppose it's called `foo-S0DDCOO472RBML4B` ("foo" is the name, and the second part is a generated unique ID).

2. Update `pkg/foo-S0DDCOO472RBML4B/upgrade.sql` to reflect the desired/declarative state.

3. Update `pkg/foo-S0DDCOO472RBML4B/default.nix` to add dependencies on other schemas. Dependencies are required for references to other objects via foreign keys, function calls, etc. If it's unclear whether something should be a dependency or not, assume not, and add it later to fix a build error.

4. Run `scm sandbox pkg/foo-S0DDCOO472RBML4B` to test building the schema.

5. Add a reference to your schema package in the `dependencies` list of the databases you want to install it in.

## Additional package types

Use `scm pghba` to create a PostgreSQL Host-Based Authentication package, which defines rules based on which IP addresses and users can authenticate to the database. This generates a `pg_hba.conf` files that gets installed into the database.

Use `scm namespace` to create a namespace package. A namespaces is the qualified name used to group schema objects together, also commonly called a "schema". Namespaces can be created without this package type, but this package type is beneficial because it sets the `search_path` variable automatically, so that unqualified names can be used for convenience, and so that the order of precedence between namespaces is set appropriately (ones you create that depend on others will be higher precedence).

Use `scm tablespace` to create a tablespace package. Tablespaces are used for splitting the PostgreSQL data up across multiple filesystems. You can create tablespaces without this package type, but that will tightly-couple the schema to the server's filesystem layout, which won't work on development machines or other servers that are configured differently.

Use `scm extension` to create an extension package.

## License

MIT license, see LICENSE.md.
