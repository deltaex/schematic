stdargs @ { scm, pkgs, ... }:

scm.database rec {
    guid = "D0J6PLZEYV46LZA8";
    name = "world";
    server = scm.server rec {
        postgresql = pkgs.postgresql_17;
        guid = "S1LGOZ8QOP2JFGAY";
        name = "world";
        dbname = "world";
        port = "57073";
        user = "root";
        password = "pass";
    };
    dependencies = [
        <language_code-S07PMSDRZWJGSHMZ>
        <country-S0Y2F1PPW4X10VTW>
        <pg_embedding-E0YIZTJX1B958CN3>
        <pg_libphonenumber-E0JRPXXTWTEVGQXS>
        <pg_squeeze-E03UZHGLY2U7Y8MV>
        <pg_vector-E0VDJYD83D69XEU4>
        <pglogical-E03W12MZHGPHIMXJ>
        <mysql_fdw-E02J0TIE3U46BAQ4>
        <pg_repack-E0U844YU7D8AI2MI>
        <replication_setup_parameter-C01O0LWYXQ9VR29J>
        # <hashtypes-E06Y4F541JSADNHH>  # broken in postgresql 16
    ];
}
