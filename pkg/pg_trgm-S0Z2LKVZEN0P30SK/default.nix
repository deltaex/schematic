stdargs @ { scm, pkgs, ... }:

scm.schema {
    guid = "S0Z2LKVZEN0P30SK";
    name = "pg_trgm";
    upgrade_sql = ./upgrade.sql;
    dependencies = [
    ];
}
