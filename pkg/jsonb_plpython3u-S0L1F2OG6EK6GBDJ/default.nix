stdargs @ { scm, pkgs, ... }:

scm.schema {
    guid = "S0L1F2OG6EK6GBDJ";
    name = "jsonb_plpython3u";
    upgrade_sql = ./upgrade.sql;
    dependencies = [
        <plpython3u-S0R1R88DHUFSDZQE>
    ];
}
