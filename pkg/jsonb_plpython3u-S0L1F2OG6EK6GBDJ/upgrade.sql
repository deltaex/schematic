CREATE EXTENSION IF NOT EXISTS jsonb_plpython3u WITH SCHEMA public;
COMMENT ON EXTENSION jsonb_plpython3u IS 'transform between jsonb and plpython3u';

