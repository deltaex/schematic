CREATE EXTENSION IF NOT EXISTS pgrowlocks WITH SCHEMA public;
COMMENT ON EXTENSION pgrowlocks IS 'show row-level locking information';

