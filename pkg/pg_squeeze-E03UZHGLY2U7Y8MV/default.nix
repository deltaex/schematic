stdargs @ { scm, pkgs, ... }:

scm.extension {
    guid = "E03UZHGLY2U7Y8MV";
    name = "pg_squeeze";
    upgrade_sql = ./upgrade.sql;
    dependencies = [ ];
    settings = {
        shared_preload_libraries = "pg_squeeze";
    };
    preBuildInputs = { pkgs, ... }: [
        (pkgs.callPackage ./extension.nix stdargs)
    ];
}
