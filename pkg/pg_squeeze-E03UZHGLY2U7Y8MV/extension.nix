{ pkgs, stdenv, postgresql, readline, openssl, zlib, ... }:

stdenv.mkDerivation rec {
    name = "pg_squeeze-1.7.0";
    src = pkgs.fetchurl {
        url = "https://github.com/cybertec-postgresql/pg_squeeze/archive/refs/tags/REL1_7_0.tar.gz";
        sha256 = "sha256-9FpUWouumcdIXH3h8VXKUPZeh+GoauydhTEwYl0KpyU=";
    };
    buildInputs = [
        postgresql
    ];
    installPhase = ''
        targetdir=$out/basefiles
        mkdir -p "$targetdir/data"
        install -D pg_squeeze.so -t $targetdir/lib/
        install -D {*.sql,*.control} -t $targetdir/share/postgresql/extension/
    '';
}
