stdargs @ { scm, pkgs, ... }:

scm.extension {
    guid = "E0VDJYD83D69XEU4";
    name = "pg_vector";
    upgrade_sql = ./upgrade.sql;
    dependencies = [ ];
    settings = {
        shared_preload_libraries = "vector";
    };
    preBuildInputs = { pkgs, ... }: [
        (pkgs.callPackage ./extension.nix stdargs)
    ];
}
