{ pkgs, stdenv, postgresql, ... }:

stdenv.mkDerivation rec {
    name = "vector-${version}";
    version = "0.4.4";
    src = pkgs.fetchurl {
        url = "https://github.com/ankane/pgvector/archive/refs/tags/v${version}.tar.gz";
        sha256 = "sha256-HLcKY/iSjjlkdHlsIqIL6fcoWooBMAneuBUkRbYbcuY=";
    };
    installPhase = ''
        targetdir=$out/basefiles
        install -D -t $targetdir/lib/ vector.so
        install -D -t $targetdir/share/postgresql/extension/ sql/vector-*.sql
        install -D -t $targetdir/share/postgresql/extension/ vector.control
    '';
    buildInputs = [
        postgresql
    ];
}
