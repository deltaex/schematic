stdargs @ { scm, pkgs, ... }:

scm.extension {
    guid = "E0U844YU7D8AI2MI";
    name = "pg_repack";
    upgrade_sql = ./upgrade.sql;
    dependencies = [ ];
    settings = {
        shared_preload_libraries = "pg_repack";
    };
    preBuildInputs = { pkgs, ... }: [
        (pkgs.callPackage ./extension.nix stdargs)
    ];
}
