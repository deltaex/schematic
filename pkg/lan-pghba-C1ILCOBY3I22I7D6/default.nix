stdargs @ { scm, ... }:

scm.pghba {
    guid = "C1ILCOBY3I22I7D6";
    name = "lan-pghba";
    comment = "Require password auth on local/private networks.";
    rules = [
        {
            type = "host";
            dbname = "all";
            user = "all";
            addr = "10.0.0.0/8";
            auth = "md5";
            opts = "";
            comment = "ipv4 local/private network https://datatracker.ietf.org/doc/html/rfc1918";
        }
        {
            type = "host";
            dbname = "all";
            user = "all";
            addr = "172.16.0.0/12";
            auth = "md5";
            opts = "";
            comment = "ipv4 local/private network https://datatracker.ietf.org/doc/html/rfc1918";
        }
        {
            type = "host";
            dbname = "all";
            user = "all";
            addr = "192.0.0.0/24";
            auth = "md5";
            opts = "";
            comment = "ipv4 local/private network https://datatracker.ietf.org/doc/html/rfc6890";
        }
        {
            type = "host";
            dbname = "all";
            user = "all";
            addr = "192.168.0.0/16";
            auth = "md5";
            opts = "";
            comment = "ipv4 local/private network https://datatracker.ietf.org/doc/html/rfc1918";
        }
        {
            type = "host";
            dbname = "all";
            user = "all";
            addr = "198.18.0.0/15";
            auth = "md5";
            opts = "";
            comment = "ipv4 local/private network for benchmarking https://datatracker.ietf.org/doc/html/rfc6890";
        }
    ];
}
