stdargs @ { scm, pkgs, ... }:

scm.schema {
    guid = "S02OJ7RPEKLQ97I2";
    name = "logic.if";
    upgrade_sql = ./upgrade.sql;
    dependencies = [
        <logic-N0BUZPS77QZ95D7B>
    ];
}
