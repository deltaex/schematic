CREATE OR REPLACE FUNCTION logic.if(pred bool, val_true anyelement, val_false anyelement) RETURNS anyelement
LANGUAGE sql CALLED ON NULL INPUT IMMUTABLE PARALLEL SAFE LEAKPROOF AS $function$
    SELECT CASE WHEN pred THEN val_true ELSE val_false END;
$function$;

COMMENT ON FUNCTION logic.if(pred bool, val_true anyelement, val_false anyelement) IS 'IF function that returns the second argument if first arguments is TRUE else returns the third argument ';

CREATE OR REPLACE FUNCTION logic.if(pred bool, val_true text, val_false text) RETURNS text
LANGUAGE sql CALLED ON NULL INPUT IMMUTABLE PARALLEL SAFE LEAKPROOF AS $function$
    SELECT CASE WHEN pred THEN val_true ELSE val_false END;
$function$;

COMMENT ON FUNCTION logic.if(pred bool, val_true text, val_false text) IS 'IF function that returns the second argument if first arguments is TRUE else returns the third argument ';

CREATE OR REPLACE FUNCTION logic.if(pred bool, val_true anyelement, val_false anyelement, val_null anyelement) RETURNS anyelement
LANGUAGE sql CALLED ON NULL INPUT IMMUTABLE PARALLEL SAFE LEAKPROOF
AS $function$
    SELECT CASE WHEN pred is NULL THEN val_null WHEN pred THEN val_true ELSE val_false END;
$function$;

COMMENT ON FUNCTION logic.if(pred bool, val_true anyelement, val_false anyelement, val_null anyelement) IS 'Overloaded version of IF function that returns the fourth argument if first argument is NULL';

CREATE OR REPLACE FUNCTION logic.if(pred bool, val_true text, val_false text, val_null text) RETURNS text
LANGUAGE sql CALLED ON NULL INPUT IMMUTABLE PARALLEL SAFE LEAKPROOF
AS $function$
    SELECT CASE WHEN pred is NULL THEN val_null WHEN pred THEN val_true ELSE val_false END;
$function$;

COMMENT ON FUNCTION logic.if(pred bool, val_true text, val_false text, val_null text) IS 'Overloaded version of IF function that returns the fourth argument if first argument is NULL';
