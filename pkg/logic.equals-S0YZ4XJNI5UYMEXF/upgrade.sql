CREATE OR REPLACE FUNCTION logic.eq(var1 anyelement, var2 anyelement) RETURNS bool
LANGUAGE sql CALLED ON NULL INPUT IMMUTABLE PARALLEL SAFE LEAKPROOF AS $function$
    SELECT $1 IS NOT DISTINCT FROM $2;
$function$;

COMMENT ON FUNCTION logic.eq(var1 anyelement, var2 anyelement) IS 'Shorter version of IS NOT DISTINCT FROM operator.';

CREATE OPERATOR logic.== (
    LEFTARG = anyelement,
    RIGHTARG = anyelement,
    FUNCTION = logic.eq,
    COMMUTATOR = OPERATOR(logic.==),
    NEGATOR = OPERATOR(logic.!==)
);

CREATE OR REPLACE FUNCTION logic.eq(var1 text, var2 text) RETURNS bool
LANGUAGE sql CALLED ON NULL INPUT IMMUTABLE PARALLEL SAFE LEAKPROOF AS $function$
    SELECT $1 IS NOT DISTINCT FROM $2;
$function$;

COMMENT ON FUNCTION logic.eq(var1 text, var2 text) IS 'Shorter version of IS NOT DISTINCT FROM operator.';

CREATE OPERATOR logic.== (
    LEFTARG = text,
    RIGHTARG = text,
    FUNCTION = logic.eq,
    COMMUTATOR = OPERATOR(logic.==),
    NEGATOR = OPERATOR(logic.!==)
);

CREATE OR REPLACE FUNCTION logic.not_eq(var1 anyelement, var2 anyelement) RETURNS bool
LANGUAGE sql CALLED ON NULL INPUT IMMUTABLE PARALLEL SAFE LEAKPROOF AS $function$
    SELECT $1 IS DISTINCT FROM $2;
$function$;

COMMENT ON FUNCTION logic.not_eq(var1 anyelement, var2 anyelement) IS 'Shorter version of IS DISTINCT FROM operator.';

CREATE OPERATOR logic.!== (
    LEFTARG = anyelement,
    RIGHTARG = anyelement,
    FUNCTION = logic.not_eq,
    COMMUTATOR = OPERATOR(logic.!==),
    NEGATOR = OPERATOR(logic.==)
);

CREATE OR REPLACE FUNCTION logic.not_eq(var1 text, var2 text) RETURNS bool
LANGUAGE sql CALLED ON NULL INPUT IMMUTABLE PARALLEL SAFE LEAKPROOF AS $function$
    SELECT $1 IS DISTINCT FROM $2;
$function$;

COMMENT ON FUNCTION logic.not_eq(var1 text, var2 text) IS 'Shorter version of IS DISTINCT FROM operator.';

CREATE OPERATOR logic.!== (
    LEFTARG = text,
    RIGHTARG = text,
    FUNCTION = logic.not_eq,
    COMMUTATOR = OPERATOR(logic.!==),
    NEGATOR = OPERATOR(logic.==)
);

/* Demo
WITH v(x) AS (VALUES (null), ('3'))
SELECT a.x AS a, b.x AS b, eq(a.x, b.x), a.x == b.x as double_equals, a.x = b.x AS equals, not_eq(a.x, b.x), a.x != b.x as not_equals, a.x !== b.x as double_not_equals
FROM v AS a, v AS b;
*/
