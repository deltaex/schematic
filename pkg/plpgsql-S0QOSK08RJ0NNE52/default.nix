stdargs @ { scm, pkgs, ... }:

scm.schema {
    guid = "S0QOSK08RJ0NNE52";
    name = "plpgsql";
    upgrade_sql = ./upgrade.sql;
    dependencies = [
    ];
}
