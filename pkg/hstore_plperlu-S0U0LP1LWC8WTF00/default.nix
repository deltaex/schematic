stdargs @ { scm, pkgs, ... }:

scm.schema {
    guid = "S0U0LP1LWC8WTF00";
    name = "hstore_plperlu";
    upgrade_sql = ./upgrade.sql;
    dependencies = [
        <hstore-S0UQM5WIU4BB05A8>
        <plperlu-S03YAUY4TV3G1VW8>
    ];
}
