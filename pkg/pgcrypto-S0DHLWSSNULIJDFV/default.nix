stdargs @ { scm, pkgs, ... }:

scm.schema {
    guid = "S0DHLWSSNULIJDFV";
    name = "pgcrypto";
    upgrade_sql = ./upgrade.sql;
    dependencies = [
    ];
}
