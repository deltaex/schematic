stdargs @ { scm, pkgs, ... }:

scm.schema {
    guid = "S0CBBR09GZJ962NC";
    name = "btree_gin";
    upgrade_sql = ./upgrade.sql;
    dependencies = [
    ];
}
