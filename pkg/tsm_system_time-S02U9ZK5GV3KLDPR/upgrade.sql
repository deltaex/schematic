CREATE EXTENSION IF NOT EXISTS tsm_system_time WITH SCHEMA public;
COMMENT ON EXTENSION tsm_system_time IS 'TABLESAMPLE method which accepts time in milliseconds as a limit';

