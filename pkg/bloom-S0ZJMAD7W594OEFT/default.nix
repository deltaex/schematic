stdargs @ { scm, pkgs, ... }:

scm.schema {
    guid = "S0ZJMAD7W594OEFT";
    name = "bloom";
    upgrade_sql = ./upgrade.sql;
    dependencies = [
    ];
}
