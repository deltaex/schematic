stdargs @ { scm, pkgs, ... }:

scm.schema {
    guid = "S0WIDZNU5H2HME29";
    name = "jsonb_plperlu";
    upgrade_sql = ./upgrade.sql;
    dependencies = [
        <plperlu-S03YAUY4TV3G1VW8>
    ];
}
