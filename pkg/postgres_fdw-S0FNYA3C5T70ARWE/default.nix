stdargs @ { scm, pkgs, ... }:

scm.schema {
    guid = "S0FNYA3C5T70ARWE";
    name = "postgres_fdw";
    upgrade_sql = ./upgrade.sql;
    dependencies = [
    ];
}
