stdargs @ { scm, pkgs, ... }:

scm.schema {
    guid = "S09R5DELIBBPAKU6";
    name = "scm-stdlib";
    upgrade_sql = ./upgrade.sql;
    dependencies = [
        <logic.equals-S0YZ4XJNI5UYMEXF>
        <logic.if-S02OJ7RPEKLQ97I2>
        <logic.xor-S0QGPOETT4MKYRR3>
    ];
}
