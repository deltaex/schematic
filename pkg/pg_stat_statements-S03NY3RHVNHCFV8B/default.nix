stdargs @ { scm, pkgs, ... }:

scm.schema {
    guid = "S03NY3RHVNHCFV8B";
    name = "pg_stat_statements";
    upgrade_sql = ./upgrade.sql;
    dependencies = [
    ];
}
