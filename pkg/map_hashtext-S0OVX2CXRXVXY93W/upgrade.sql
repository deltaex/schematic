CREATE FUNCTION public.map_hashtext(text[]) RETURNS integer[]
    LANGUAGE sql IMMUTABLE
    AS $_$
            SELECT ARRAY_AGG(hashtext(x)) FROM UNNEST($1) x;
        $_$;

