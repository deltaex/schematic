stdargs @ { scm, pkgs, ... }:

scm.schema {
    guid = "S08ETCMLHXEI0NQR";
    name = "tsm_system_rows";
    upgrade_sql = ./upgrade.sql;
    dependencies = [
    ];
}
