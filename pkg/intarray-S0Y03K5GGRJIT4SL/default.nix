stdargs @ { scm, pkgs, ... }:

scm.schema {
    guid = "S0Y03K5GGRJIT4SL";
    name = "intarray";
    upgrade_sql = ./upgrade.sql;
    dependencies = [
    ];
}
