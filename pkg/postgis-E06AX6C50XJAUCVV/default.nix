stdargs @ { scm, pkgs, ... }:

scm.extension {
    guid = "E06AX6C50XJAUCVV";
    name = "postgis";
    upgrade_sql = ./upgrade.sql;
    dependencies = [ ];
    settings = {
        shared_preload_libraries = "postgis";
    };
    preBuildInputs = { pkgs, ... }: [
        (pkgs.callPackage ./extension.nix stdargs)
    ];
}
