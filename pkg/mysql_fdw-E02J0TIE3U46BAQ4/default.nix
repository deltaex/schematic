stdargs @ { scm, pkgs, ... }:

scm.extension rec {
    guid = "E02J0TIE3U46BAQ4";
    name = "mysql_fdw";
    upgrade_sql = ./upgrade.sql;
    dependencies = [ ];
    settings = {
        shared_preload_libraries = "mysql_fdw";
    };
    preBuildInputs = { pkgs, ... }: [
        (pkgs.callPackage ./extension.nix (stdargs // {inherit name guid;}) )
    ];
}
