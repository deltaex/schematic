{ pkgs, stdenv, postgresql, mysql80, name, guid, fetchurl, ... }:
let
  version = "2.9.2";
  tarPkgName = let
    v_name = builtins.replaceStrings ["."] ["_"] version;
  in "REL-${v_name}.tar.gz";
in
stdenv.mkDerivation {
  name = "${name}-${guid}";

  src = fetchurl {
    url = "https://github.com/EnterpriseDB/mysql_fdw/archive/refs/tags/${tarPkgName}";
    sha256 = "sha256-2uVvyCgB1o2X587PSN+t6ca6fK3DQ5Inws8ovLFC6CA=";
  };

  buildInputs = [ postgresql mysql80 ];

  makeFlags = [ "USE_PGXS=1" ];

  # set the RPATH to the mysql lib directory to be able to find the libmysqlclient library
  # according to man dlopen(3):
  #
  #  > (ELF only) If the executable file for the calling program contains a DT_RPATH tag, and
  #  > does not contain a DT_RUNPATH tag, then the directories listed in the DT_RPATH tag are searched.
  #
  # That's why this method works.
  postBuild = ''
    patchelf --set-rpath "${ mysql80 }/lib" mysql_fdw.so
  '';

  # to avoid shrinking RPATH, otherwise it will detect that the rpath has a value that
  # is not needed by the shared object, and it will remove it.
  dontPatchELF = true;

  installPhase = ''
    targetdir=$out/basefiles
    install -D -t $targetdir/lib/ mysql_fdw.so
    install -D -t $targetdir/share/postgresql/extension {mysql_fdw-*.sql,mysql_fdw.control}
  '';

  inherit version;
}
