stdargs @ { scm, pkgs, ... }:

scm.schema {
    guid = "S057OSTALE3Q06VC";
    name = "dict_int";
    upgrade_sql = ./upgrade.sql;
    dependencies = [
    ];
}
