{ pkgs, stdenv, postgresql, ... }:

stdenv.mkDerivation {
    name = "rum";
    src = pkgs.fetchFromGitHub {
        owner = "postgrespro";
        repo = "rum";
        rev = "cb1edffc57736cd2a4455f8d0feab0d69928da25";
        sha256 = "sha256-K9rBXmq2OC5YQYypbPOQB24TzVakd6k4UEYN7TqgkI8=";
    };
    USE_PGXS=1;
    buildInputs = [
        postgresql
    ];
    installPhase = ''
        targetdir=$out/basefiles
        mkdir -p "$targetdir/data"
        install -D rum.so -t $targetdir/lib/
        install -D {*.sql,*.control} -t $targetdir/share/postgresql/extension/
    '';
}

