stdargs @ { scm, pkgs, ... }:

scm.extension {
    guid = "E0T24ZDSE6N3H69R";
    name = "rum";
    upgrade_sql = ./upgrade.sql;
    dependencies = [ ];
    settings = {
        shared_preload_libraries = "rum";
    };
    preBuildInputs = { pkgs, ... }: [
        (pkgs.callPackage ./extension.nix stdargs)
    ];
}
