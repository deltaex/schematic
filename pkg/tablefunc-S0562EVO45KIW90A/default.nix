stdargs @ { scm, pkgs, ... }:

scm.schema {
    guid = "S0562EVO45KIW90A";
    name = "tablefunc";
    upgrade_sql = ./upgrade.sql;
    dependencies = [
    ];
}
