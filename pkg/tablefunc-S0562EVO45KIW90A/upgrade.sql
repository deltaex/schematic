CREATE EXTENSION IF NOT EXISTS tablefunc WITH SCHEMA public;
COMMENT ON EXTENSION tablefunc IS 'functions that manipulate whole tables, including crosstab';

