stdargs @ { scm, pkgs, ... }:

scm.schema {
    guid = "S0S5DJK51IMWH7CT";
    name = "pageinspect";
    upgrade_sql = ./upgrade.sql;
    dependencies = [
    ];
}
