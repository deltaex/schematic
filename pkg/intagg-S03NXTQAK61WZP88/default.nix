stdargs @ { scm, pkgs, ... }:

scm.schema {
    guid = "S03NXTQAK61WZP88";
    name = "intagg";
    upgrade_sql = ./upgrade.sql;
    dependencies = [
    ];
}
