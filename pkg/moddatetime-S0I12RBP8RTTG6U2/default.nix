stdargs @ { scm, pkgs, ... }:

scm.schema {
    guid = "S0I12RBP8RTTG6U2";
    name = "moddatetime";
    upgrade_sql = ./upgrade.sql;
    dependencies = [
    ];
}
