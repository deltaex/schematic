CREATE OR REPLACE FUNCTION immutable_random(text) RETURNS double precision
    LANGUAGE sql IMMUTABLE
    AS $$SELECT (0.5 - (hashtext($1) / 4294967294.0))::double precision;$$;
CREATE OR REPLACE FUNCTION rand.immutable_random(anyelement) RETURNS double precision
    LANGUAGE sql IMMUTABLE
    AS $$SELECT (0.5 - (hashtext($1 :: text) / 4294967294.0))::double precision;$$;
CREATE OR REPLACE FUNCTION rand.immutable_random(integer) RETURNS double precision
    LANGUAGE sql IMMUTABLE
    AS $$SELECT (0.5 - (hashtext($1 :: text) / 4294967294.0))::double precision;$$;
CREATE OR REPLACE FUNCTION rand.immutable_random(bigint) RETURNS double precision
    LANGUAGE sql IMMUTABLE
    AS $$SELECT (0.5 - (hashtext($1 :: text) / 4294967294.0))::double precision;$$;
CREATE OR REPLACE FUNCTION immutable_random(json) RETURNS double precision
    LANGUAGE sql IMMUTABLE
    AS $$SELECT (0.5 - (hashtext(($1) :: TEXT) / 4294967294.0))::double precision;$$;

CREATE OR REPLACE FUNCTION rand.hashkey_real(text) RETURNS real
    LANGUAGE sql IMMUTABLE
    AS $$SELECT (0.5 - (hashtext(($1) :: TEXT) / 4294967294.0))::real;$$;

CREATE OR REPLACE FUNCTION rand.hashkey_real(anyelement) RETURNS real
    LANGUAGE sql IMMUTABLE
    AS $$SELECT (0.5 - (hashtext($1 :: text) / 4294967294.0))::real;$$;
