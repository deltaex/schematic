stdargs @ { scm, pkgs, ... }:

scm.schema {
    guid = "S0K14KSM4Q46TQLF";
    name = "immutable_random";
    upgrade_sql = ./upgrade.sql;
    dependencies = [
        <rand-N0OJXDNWCR5M3D7F>
    ];
}
