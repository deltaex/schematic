stdargs @ { scm, pkgs, ... }:

scm.schema {
    guid = "S0HC0TV7EV56JD1N";
    name = "ltree_plpython3u";
    upgrade_sql = ./upgrade.sql;
    dependencies = [
        <ltree-S0KFFEHKYFLOSZZX>
        <plpython3u-S0R1R88DHUFSDZQE>
    ];
}
