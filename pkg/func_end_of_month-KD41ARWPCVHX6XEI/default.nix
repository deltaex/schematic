stdargs @ { scm, pkgs, ... }:

scm.schema {
    guid = "KD41ARWPCVHX6XEI";
    name = "func_end_of_month";
    upgrade_sql = ./upgrade.sql;
    dependencies = [
    ];
}
