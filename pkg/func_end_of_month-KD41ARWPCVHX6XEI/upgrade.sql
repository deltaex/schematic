CREATE FUNCTION end_of_month(date)
RETURNS date AS
$$
    SELECT (date_trunc('month', $1) + interval '1 month' - interval '1 day')::date;
$$ LANGUAGE 'sql'
IMMUTABLE STRICT;

COMMENT ON FUNCTION end_of_month(date) IS 'Rounds up a date to the last day of its month.';
