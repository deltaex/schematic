stdargs @ { scm, pkgs, ... }:
# https://github.com/aleksabl/hashtypes/
# supports up to postgresql 15, build error in postgresql 16
scm.extension {
    guid = "E06Y4F541JSADNHH";
    name = "hashtypes";
    upgrade_sql = ./upgrade.sql;
    dependencies = [ ];
    settings = {
        shared_preload_libraries = "hashtypes";
    };
    preBuildInputs = { pkgs, ... }: [
        (pkgs.callPackage ./extension.nix stdargs)
    ];
}
