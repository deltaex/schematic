stdargs @ { scm, ... }:

scm.extension {
    guid = "E0YIZTJX1B958CN3";
    name = "pg_embedding";
    upgrade_sql = ./upgrade.sql;
    dependencies = [ ];
    settings = {
        shared_preload_libraries = "embedding";
    };
    preBuildInputs = { pkgs, ... }: [
        (pkgs.callPackage ./extension.nix stdargs)
    ];
}
