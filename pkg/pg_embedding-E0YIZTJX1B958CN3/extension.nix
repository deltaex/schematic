{ pkgs, stdenv, postgresql, ... }:

stdenv.mkDerivation rec {
    name = "embedding-${version}";
    version = "0.2.0";
    src = pkgs.fetchFromGitHub {
        owner = "neondatabase";
        repo = "pg_embedding";
        rev = "710f44d6cb3868af9574d7f76258dd48b6834d69";
        sha256 = "sha256-Mb64nmjBt0tI9GTqU5ndzRgsZX2VmUSvNH86KEYxf0s=";
    };
    installPhase = ''
        targetdir=$out/basefiles
        install -D embedding.so -t $targetdir/lib/
        install -D embedding.control -t $targetdir/share/postgresql/extension/
        install -D embedding--${version}.sql -t $targetdir/share/postgresql/extension/
    '';

    buildInputs = [
        postgresql
    ];
}
