stdargs @ { scm, pkgs, ... }:

scm.namespace {
    guid = "N0FLM2YWXLMJE5BC";
    name = "meta";
    comment = "Schematic tables and other system objects.";
}
