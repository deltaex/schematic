stdargs @ { scm, pkgs, ... }:

scm.schema {
    guid = "AUG443VW9XAC089L";
    name = "language";
    upgrade_sql = ./upgrade.sql;
    dependencies = [
    ];
}
