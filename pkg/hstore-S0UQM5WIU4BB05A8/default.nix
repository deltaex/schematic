stdargs @ { scm, pkgs, ... }:

scm.schema {
    guid = "S0UQM5WIU4BB05A8";
    name = "hstore";
    upgrade_sql = ./upgrade.sql;
    dependencies = [
    ];
}
