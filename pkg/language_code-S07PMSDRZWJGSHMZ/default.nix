stdargs @ { scm, pkgs, ... }:

scm.schema {
    guid = "S07PMSDRZWJGSHMZ";
    name = "language_code";
    upgrade_sql = ./upgrade.sql;
    dependencies = [
    ];
}
