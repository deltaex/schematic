-- NEEDED FOR THE MIGRATION FROM OLD PATCHES
DELETE FROM pg_proc WHERE proname = 'pg_replication_origin_session_setup' 
AND prolang = 12
AND proargtypes = '25 23'::oidvector;

UPDATE pg_proc
SET prosrc = 'pg_replication_origin_session_setup'
WHERE proname = 'pg_replication_origin_session_setup'
AND prolang = 12
AND prosrc = 'pg_replication_origin_session_setup_nopid';

-- CREATE OR REPLACE FUNCTION
--   pg_replication_origin_session_setup(text)
-- RETURNS void
-- LANGUAGE internal 
-- STRICT VOLATILE
-- AS 'pg_replication_origin_session_setup';

CREATE OR REPLACE FUNCTION
  pg_replication_origin_session_setup(node_name text, acquired_by integer DEFAULT 0)
RETURNS void
AS 'replication_setup_parameter', 'pg_replication_origin_session_setup'
LANGUAGE C STRICT VOLATILE;