#include "postgres.h"
#include "fmgr.h"
#include "replication/slot.h"
#include "replication/origin.h"
#include "utils/builtins.h"
#include "utils/errcodes.h"
#include "utils/elog.h"

#ifdef PG_MODULE_MAGIC
PG_MODULE_MAGIC;
#endif

// Note: The functions need to be updated as their original versions are updated by postgresql   

PG_FUNCTION_INFO_V1(pg_replication_origin_session_setup);

// https://github.com/postgres/postgres/blob/861086493faa9ca5468cc50dd20975ee6c042ffe/src/backend/replication/logical/origin.c#L189
// IN src/backend/replication/logical/origin.c 
static void
replorigin_check_prerequisites(bool check_slots, bool recoveryOK)
{
	if (check_slots && max_replication_slots == 0)
		ereport(ERROR,
				(errcode(ERRCODE_OBJECT_NOT_IN_PREREQUISITE_STATE),
				 errmsg("cannot query or manipulate replication origin when \"max_replication_slots\" is 0")));

	if (!recoveryOK && RecoveryInProgress())
		ereport(ERROR,
				(errcode(ERRCODE_READ_ONLY_SQL_TRANSACTION),
				 errmsg("cannot manipulate replication origins during recovery")));
}
//

// MODIFIED FROM src/backend/replication/logical/origin.c 
Datum
pg_replication_origin_session_setup(PG_FUNCTION_ARGS)
{
	char	   *name;
	RepOriginId origin;
	int         pid;
	
	replorigin_check_prerequisites(true, false);

	name = text_to_cstring((text *) DatumGetPointer(PG_GETARG_DATUM(0)));
	origin = replorigin_by_name(name, false);
	pid = PG_GETARG_INT32(1);
	replorigin_session_setup(origin, pid);
	
	replorigin_session_origin = origin;

	pfree(name);

	PG_RETURN_VOID();
}
//