{ pkgs, stdenv, postgresql, ... }:

stdenv.mkDerivation rec {
      name = "replication_setup_parameter";
      version = "1.0";
      src = ./replication_setup_parameter; 
      buildInputs = [ postgresql ];
      installPhase = ''
        targetdir=$out/basefiles
        install -D replication_setup_parameter.so -t $targetdir/lib/
        install -D replication_setup_parameter.control -t $targetdir/share/postgresql/extension/
        install -D replication_setup_parameter*.sql -t $targetdir/share/postgresql/extension/
      '';
}