stdargs @ { scm, pkgs, ... }:

scm.extension {
    guid = "C01O0LWYXQ9VR29J";
    name = "replication_setup_parameter";
    upgrade_sql = ./upgrade.sql;
    dependencies = [ ];
    settings = {
        shared_preload_libraries = "replication_setup_parameter";
    };
    preBuildInputs = { pkgs, ... }: [
        (pkgs.callPackage ./extension.nix stdargs)
    ];
}
