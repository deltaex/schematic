stdargs @ { scm, pkgs, ... }:

scm.schema {
    guid = "S00EHS0N00T4JOHI";
    name = "pg_buffercache";
    upgrade_sql = ./upgrade.sql;
    dependencies = [
    ];
}
