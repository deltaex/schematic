stdargs @ { scm, pkgs, ... }:

scm.schema {
    guid = "S08IMOU9QBZXCWOH";
    name = "sslinfo";
    upgrade_sql = ./upgrade.sql;
    dependencies = [
    ];
}
