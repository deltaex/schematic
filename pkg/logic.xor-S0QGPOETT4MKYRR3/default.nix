stdargs @ { scm, ... }:

scm.schema {
    guid = "S0QGPOETT4MKYRR3";
    name = "logic.xor";
    upgrade_sql = ./upgrade.sql;
    dependencies = [
        <logic-N0BUZPS77QZ95D7B>
    ];
}
