stdargs @ { scm, pkgs, ... }:

scm.schema {
    guid = "S03PG8N8AKZ0964V";
    name = "smallint_bit";
    upgrade_sql = ./upgrade.sql;
    dependencies = [
    ];
}
