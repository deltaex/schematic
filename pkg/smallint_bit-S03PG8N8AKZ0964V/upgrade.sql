CREATE OR REPLACE FUNCTION public.smallint_to_bit(smallint) RETURNS bit
    LANGUAGE sql IMMUTABLE
    AS $_$
            SELECT ($1::smallint)::int::bit(16);
        $_$;

CREATE CAST (smallint AS bit) WITH FUNCTION public.smallint_to_bit(smallint) AS IMPLICIT;

CREATE OR REPLACE FUNCTION public.bit_to_smallint(bit) RETURNS smallint
    LANGUAGE sql IMMUTABLE
    AS $_$
            SELECT ((($1::bit(16))::bit(32)::int) >> 16)::smallint;
        $_$;

CREATE CAST (bit AS smallint) WITH FUNCTION public.bit_to_smallint(bit) AS IMPLICIT;

