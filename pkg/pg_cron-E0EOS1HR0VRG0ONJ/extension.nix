{ pkgs, stdenv, postgresql, readline, openssl, zlib, ...  }:

stdenv.mkDerivation rec {
  pname = "pg_cron";
  version = "1.6.3";

  buildInputs = [ postgresql ];

  src = pkgs.fetchFromGitHub {
    owner  = "citusdata";
    repo   = pname;
    rev    = "v${version}";
    hash   = "sha256-AI1JbfQ1xcOMo6xUt/7fM3h27RarRo5rUq4hpYQ9YQw=";
  };

  installPhase = ''
    targetdir=$out/basefiles
    mkdir -p "$targetdir/data"
    install -D *.so -t $targetdir/lib
    install -D {*.sql,*.control} -t $targetdir/share/postgresql/extension
  '';

  meta = with pkgs.lib; {
    description = "Run Cron jobs through PostgreSQL";
    homepage    = "https://github.com/citusdata/pg_cron";
    changelog   = "https://github.com/citusdata/pg_cron/raw/v${version}/CHANGELOG.md";
    maintainers = with maintainers; [ thoughtpolice ];
    platforms   = postgresql.meta.platforms;
    license     = licenses.postgresql;
  };
}
