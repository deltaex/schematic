stdargs @ { scm, pkgs, ... }:

scm.extension {
    guid = "E0EOS1HR0VRG0ONJ";
    name = "pg_cron";
    upgrade_sql = ./upgrade.sql;
    dependencies = [
    ];
    settings = {
    };
    preBuildInputs = { pkgs, ... }: [
        (pkgs.callPackage ./extension.nix stdargs)
    ];
}
