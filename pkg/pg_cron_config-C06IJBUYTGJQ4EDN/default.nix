{ database_name ? "sandbox" }:
stdargs @ { scm, ... }:

scm.pgconf {
    guid = "C06IJBUYTGJQ4EDN";
    name = "pg_cron_config";
    comment = "";
    settings = {
        "cron.database_name" = database_name;
        "shared_preload_libraries" = "pg_cron";
    };
    dependencies = [
      <pg_cron-E0EOS1HR0VRG0ONJ>
    ];
}
