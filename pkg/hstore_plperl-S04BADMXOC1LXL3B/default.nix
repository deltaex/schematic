stdargs @ { scm, pkgs, ... }:

scm.schema {
    guid = "S04BADMXOC1LXL3B";
    name = "hstore_plperl";
    upgrade_sql = ./upgrade.sql;
    dependencies = [
        <hstore-S0UQM5WIU4BB05A8>
        <plperl-S01EZ4XS4N20U1VZ>
    ];
}
