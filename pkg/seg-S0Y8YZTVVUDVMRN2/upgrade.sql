CREATE EXTENSION IF NOT EXISTS seg WITH SCHEMA public;
COMMENT ON EXTENSION seg IS 'data type for representing line segments or floating-point intervals';

