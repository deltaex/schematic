stdargs @ { scm, pkgs, ... }:

scm.schema {
    guid = "S0EZN7URREJIMESM";
    name = "dblink";
    upgrade_sql = ./upgrade.sql;
    dependencies = [
    ];
}
