stdargs @ { scm, pkgs, ... }:

scm.schema {
    guid = "S0OL4W2OIB9KMQ5K";
    name = "pgstattuple";
    upgrade_sql = ./upgrade.sql;
    dependencies = [
    ];
}
