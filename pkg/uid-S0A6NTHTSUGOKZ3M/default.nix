stdargs @ { scm, pkgs, ... }:

scm.schema {
    guid = "S0A6NTHTSUGOKZ3M";
    name = "uid";
    upgrade_sql = ./upgrade.sql;
    dependencies = [
        <smallint_bit-S03PG8N8AKZ0964V>
    ];
}
