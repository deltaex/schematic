CREATE TYPE public.uid AS (
    type smallint,
    id integer
);

CREATE OR REPLACE FUNCTION public.bigint_to_uid(bigint) RETURNS public.uid
    LANGUAGE sql IMMUTABLE
    AS $_$
            SELECT ($1::bit(64)::bit(32)::int::smallint, ($1::bit(64) << 32)::bit(32)::int)::public.uid;
        $_$;

CREATE CAST (bigint AS public.uid) WITH FUNCTION public.bigint_to_uid(bigint) AS IMPLICIT;

CREATE FUNCTION public.uid_to_bigint(public.uid) RETURNS bigint
    LANGUAGE sql IMMUTABLE
    AS $_$
            SELECT ($1.type::int::bit(32) || $1.id::bit(32))::bit(64)::bigint;
        $_$;

CREATE CAST (public.uid AS bigint) WITH FUNCTION public.uid_to_bigint(public.uid) AS IMPLICIT;

CREATE FUNCTION public.uid_type(uid bigint) RETURNS smallint
    LANGUAGE sql IMMUTABLE
    AS $$
            SELECT (uid::public.uid).type;
        $$;

CREATE FUNCTION public.uid_id(uid bigint) RETURNS integer
    LANGUAGE sql IMMUTABLE
    AS $$
            SELECT (uid::public.uid).id;
        $$;

CREATE FUNCTION public.uid_chk(uid bigint, type integer, id integer) RETURNS boolean
    LANGUAGE sql IMMUTABLE
    AS $$
            SELECT (uid_type(uid) = (type::smallint)) = (uid_id(uid) = id);
        $$;
