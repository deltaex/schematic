stdargs @ { scm, pkgs, ... }:

scm.schema {
    guid = "S0XHMI6K209W9XOW";
    name = "jsonb_plperl";
    upgrade_sql = ./upgrade.sql;
    dependencies = [
        <plperl-S01EZ4XS4N20U1VZ>
    ];
}
