stdargs @ { scm, pkgs, ... }:

scm.extension {
    guid = "E03W12MZHGPHIMXJ";
    name = "pglogical";
    upgrade_sql = ./upgrade.sql;
    dependencies = [ ];
    settings = {
        shared_preload_libraries = "pglogical";
    };
    preBuildInputs = { pkgs, ... }: [
        (pkgs.callPackage ./extension.nix stdargs)
    ];
}
