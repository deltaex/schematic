stdargs @ { scm, pkgs, ... }:

scm.schema {
    guid = "S0PFGNEK5Z58PRUE";
    name = "pg_prewarm";
    upgrade_sql = ./upgrade.sql;
    dependencies = [
    ];
}
