stdargs @ { scm, pkgs, ... }:

scm.extension {
    guid = "E0JRPXXTWTEVGQXS";
    name = "pg_libphonenumber";
    upgrade_sql = ./upgrade.sql;
    dependencies = [ ];
    settings = {
        shared_preload_libraries = "pg_libphonenumber";
    };
    preBuildInputs = { pkgs, ... }: [
        (pkgs.callPackage ./extension.nix stdargs)
    ];
}
