{ pkgs, stdenv, postgresql, libphonenumber, protobuf3_20, ... }:

stdenv.mkDerivation rec {
    name = "pg_libphonenumber-FURWIAHEDBAYRIJO";
    version = "alpha";
    src = pkgs.fetchgit {
        url = "https://github.com/blm768/pg-libphonenumber.git";
        rev = "753e2fa4be452620455a099aeda917648f2da70a";
        sha256 = "sha256-0ip8Tz6JbSWsfsEnETpF3FySWTq55Y8B+hfg+EbiXgc=";
    };
    buildInputs = [
        postgresql
        (libphonenumber.override {
            protobuf=protobuf3_20;
        })
        protobuf3_20
    ];
    installPhase = ''
        targetdir=$out/basefiles
        install -D pg_libphonenumber.so -t $targetdir/lib/
        install -D pg_libphonenumber.control -t $targetdir/share/postgresql/extension/
        install -D sql/pg_libphonenumber*.sql -t $targetdir/share/postgresql/extension/
    '';
    patches = if postgresql.version >= "16" then [ ./patches/include-varatt.patch ] else [];
}
