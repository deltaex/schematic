stdargs @ { scm, pkgs, ... }:

scm.schema {
    guid = "S0YQVIT96AMJO3NS";
    name = "adminpack";
    upgrade_sql = ./upgrade.sql;
    dependencies = [
    ];
}
