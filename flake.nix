{
  description = "Schematic's flake module.";
  nixConfig = {
    extra-substituters = [
      "s3://scm-nixcache-3i43dyr22myk"
    ];
    extra-trusted-public-keys = [
      "scm-nixcache-3i43dyr22myk:SwfiyE54Ckk7h0Dx+LbF+JIMXgdWOjrja6YeM6g8oiw="
    ];
  };
  inputs = {
    flake-utils.url = "github:numtide/flake-utils";
    nixpkgs_latest.url = "github:NixOS/nixpkgs/7b4e618602b6c415dc119dcdd1ae5d174117e8b2";
    nixpkgs.url = "github:NixOS/nixpkgs/057f9aecfb71c4437d2b27d3323df7f93c010b7e";
  };

  outputs = { self, nixpkgs, nixpkgs_latest, flake-utils }:
    flake-utils.lib.eachDefaultSystem (system:
      let
        pkgs = import nixpkgs {
          inherit system;
          overlays = [(import ./lib/nixpkgs-overlay.nix)];
        };
        pkgs_latest = import nixpkgs_latest {
          inherit system;
        };
      in
      rec {
        devShells = {
          default = devShells.dev;
          dev = import ./shell/default.nix {
            inherit pkgs pkgs_latest;
            SCM_PATH = ".";
            repos = [];
          };
        };
        lib = import ./lib;
      }
    );
}
