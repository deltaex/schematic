#! /usr/bin/env bash

HISTLOGPATH="$HOME/.history/$(hostname)/$(date -u +%Y/%m)"
HISTLOGFILE="$HISTLOGPATH/$(date -u +%dd.%Hh.%Mm.%Ss)_bash-$$"
mkdir -p "$HISTLOGPATH"
export SCM_VAR=${SCM_VAR:-"$HOME/var"}
mkdir -p "$SCM_VAR"
export SCM_TMPDIR=${SCM_TMPDIR:-"$SCM_VAR/tmp"}
mkdir -p "$SCM_TMPDIR"
export SCM_PG=${SCM_PG:-"$SCM_VAR/pg"}
mkdir -p "$SCM_PG"
export SCM_TMUX=${SCM_TMUX:-"$SCM_VAR/tmux"}
mkdir -p "$SCM_TMUX"
export TMUX_TMPDIR="$SCM_TMUX"
export SCM_PATH="$(realpath "$SCM_PATH")"
schematic_env_file=.schematic.env
if [ -n "$shellHook" ]; then
    # we just built a new nix-env, cache it for later use
    export ROOT_DIR="$PWD"
    unset shellHook
    export ENV_FILE="$ROOT_DIR/$schematic_env_file"
    # atomically move the cache to reduce chance of corruption
    tmpfile="${ENV_FILE}.$$"
    export -p > "$tmpfile" && mv "$tmpfile" "$ENV_FILE"
    [ -d "$SCM_PATH/.git" ] && "$SCM_PATH/.githooks/autohook.sh" install 2>/dev/null || true  # install git hooks
else
    # we are entering a nix-env from a plain bash env or an old nix-env that should be refreshed
    [ -f $schematic_env_file ] && source $schematic_env_file && export ENV_FILE="$schematic_env_file" && cd "$ROOT_DIR"
    unset latest_env
fi
chmod +x "$SCM_PATH/shell/.bashrc" "$SCM_PATH/shell/.bashrc-local" "$SCM_PATH/shell/nixbash" "$SCM_PATH/shell/.bash_shopt" "$SCM_PATH/shell/.bash_aliases" "$SCM_PATH/.githooks/autohook.sh"  2>/dev/null || true
export PATH="$SCM_PATH/shell:$PATH"
export PYTHONPATH="$SCM_PATH/py"
export PYTHONPYCACHEPREFIX=""$SCM_VAR/.pycache""
export SCM_REPOS="$SCM_REPOS:$SCM_PATH"
export NIX_PATH="$(scm mknixpath)"
if [ -z "$TEST_RUNNER" ]; then
    export PS1='\[\033[01;91m\](schematic)\[\033[00m\] \[\033[01;32m\]\u@\h\[\033[00m\]:\[\033[01;34m\]\w\[\033[00m\]\$ '
else
    export PS1='\$ '
fi
unset TZ
export SHELL="$SCM_PATH/shell/nixbash"
export HISTCONTROL=ignoredups:ignorespace
export HISTFILESIZE=1000000000                  # max lines in bash history file
export HISTSIZE=1000000000                      # max lines in bash history per session
export EDITOR=vim
[ -f /etc/bash_completion ] && ! shopt -oq posix && source /etc/bash_completion
eval "$(/usr/bin/lesspipe 2>/dev/null)" # enable less to view non-textual files
eval "$(/usr/bin/dircolors -b 2>/dev/null)" # enable color support
[ -f "$SCM_PATH/shell/.bash_aliases" ] && source "$SCM_PATH/shell/.bash_aliases"
[ -f "$SCM_PATH/shell/.bash_shopt" ] && source "$SCM_PATH/shell/.bash_shopt"
export FZF_COMPLETION_TRIGGER=""  # use fzf by default for completions; if you want standard bash completions set FZF_COMPLETION_TRIGGER="**" in bin/.bashrc-dev
export FZF_DEFAULT_OPTS="--layout=reverse --info=hidden --border=none"
__fzf_basedir=$(dirname $(dirname $(which fzf)))
source "$__fzf_basedir/share/fzf/completion.bash"  # enable fzf shell completions
source "$__fzf_basedir/share/fzf/key-bindings.bash"  # enable fzf key bindings for ^r (search history), ^t (complete file), alt-c (cd)
for completion_file in $(find "$SCM_PATH/shell/.bash_completions" -mindepth 1 -maxdepth 1 -type f); do
    source "$completion_file"
done
export TERM=screen-256color
export PG_COLOR=auto
export PSQL_HISTORY="${HISTLOGFILE}-psql"
export IPYTHON_HISTORY="${HISTLOGFILE}-ipython"
export IPYTHONDIR=$ROOT_DIR/.ipython/
promptFunc(){
    history -a  # flush to history file
    # audit log of all bash commands
    echo "$(date +%Y-%m-%d--%H-%M-%S) $PWD $(history 1)" >> "$HISTLOGFILE"
}
PROMPT_COMMAND=promptFunc
# source any local customizations
[ -f "$SCM_PATH/shell/.bashrc-local" ] && source "$SCM_PATH/shell/.bashrc-local"
[ -d .git ] && git config --local include.path "$SCM_PATH/.gitconfig"