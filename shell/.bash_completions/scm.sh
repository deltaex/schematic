#!/usr/bin/env bash

_list_scm_packages() {
    local cur="${COMP_WORDS[COMP_CWORD]}"
    if [[ "$cur" =~ ^([\"\']?[a-zA-Z0-9_]) ]]; then  # complete with relative paths
        find . -type d 2>/dev/null | \
            grep -P '[\w_]+\-[A-Z0-9]+$' | \
            awk -F/ -v OFS='/' '{if (NF > 2) { $1=""; print "\"" substr($0, 2) "\"" } else if (NF == 2) {print "\""$2"\""}}' | \
            sort -r
    else  # complete with "<foo>" syntax
        echo "$NIX_PATH" | \
            tr ':' '\n' | \
            xargs -I {} find {} -type d 2>/dev/null | \
            grep -P '[\w_]+\-[A-Z0-9]+$' | \
            awk -F/ '{print "\"<" $NF ">\""}' | \
            sort -r
    fi
}

_fzf_complete_scm_packages() {
    _fzf_complete -- "$@" < <(
        _list_scm_packages
    )
}

_fzf_complete_scm_srv() {
    _fzf_complete --select-1 -- "$@" < <(
        _list_scm_packages | grep "\-D0"
    )
}

_fzf_complete_scm_revisables() {
    _fzf_complete -- "$@" < <(
        _list_scm_packages | grep -P "\-(D0|S0)"
    )
}

_fzf_complete_scm_databases() {
    # list schematic databases
    _fzf_complete --select-1 -- "$@" < <(
        SCM_DIR="${SCM_VAR:-$HOME/var}/pg/" && find "$SCM_DIR" -mindepth 1 -maxdepth 1 -type d | sed "s|^$HOME|~|"
    )
}

_fzf_complete_scm() {
    if [[ "$COMP_CWORD" = 1 ]]; then  # 1st term gets completed with scm commands
        _fzf_complete --select-1 -- "$@" < <(
            find "$SCM_PATH/py/schematic/commands/" -type f | sed 's/.*\///;s/\.py$//'
        )
        return 0
    fi
    case "${COMP_WORDS[1]}" in
        upgrade)
            if [[ "$COMP_CWORD" = 2 ]]; then
                _fzf_complete_scm_srv "$@"
            fi
            return 0
            ;;
        fast-forward)
            if [[ "$COMP_CWORD" = 2 ]]; then
                _fzf_complete_scm_srv "$@"
            elif [[ "$COMP_CWORD" = 3 ]]; then
                _fzf_complete_scm_packages "$@"
            fi
            return 0
            ;;
        revision)
            _fzf_complete_scm_revisables "$@"
            return 0
            ;;
        sandbox|inspect|dependencies-list|lint-dependencies|rename)
            _fzf_complete_scm_packages "$@"
            return 0
            ;;
        status|stop|start|reload|restart|psql|gc-basedir|pg-activity|export_certificates)
            _fzf_complete_scm_databases "$@"
            return 0
            ;;
        *)
            return 1
            ;;
    esac
}

complete -F _fzf_complete_scm scm
