#!/usr/bin/env bash

_fzf_complete_git_branches() {
    _fzf_complete -- "$@" < <(
        git for-each-ref --format='%(refname:short)' refs/heads refs/remotes | \
            sed -Ee 's/\w+\///' | \
            grep -P "^\d{4}" | \
            sort -r | \
            uniq
    )
}

_fzf_complete_git_remotes() {
    _fzf_complete --select-1 -- "$@" < <(
        git remote
    )
}

_fzf_complete_git_stashes() {
    _fzf_complete -- "$@" < <(
        git stash list | awk -F': ' '{print $1}'
    )
}

_fzf_complete_git_add() {
    _fzf_complete --select-1 -- "$@" < <(
        git status --porcelain=v2 | awk '
        /^1 [AM]. N\.\.\./ || /^1 \.M N\.\.\./ || /^1 \.D N\.\.\./ || /^\?/ {
            print $NF; # The 9th field is the file path for modified, added, deleted, and untracked files
        }
        '
    )
}

_fzf_complete_git_reset() {
    _fzf_complete --select-1 -- "$@" < <(
        git status --porcelain=v2 | awk '
        /^1 [MAD]\. N\.\.\./ {
            print $9; # The 9th field is the file path for staged modifications, additions, and deletions
        }
        '
    )
}

_fzf_complete_git_restore() {
    _fzf_complete --select-1 -- "$@" < <(
        git status --porcelain=v2 | awk '
        /^1 [MAD]\. N\.\.\./ || /^1 \.[MD] N\.\.\./ {
            print $9; # The 9th field is the file path for modifications or deletions, whether staged or unstaged
        }
        '
    )
}

_fzf_complete_git() {
    if [[ "$COMP_CWORD" = 1 ]]; then  # 1st term gets completed with git commands
        _fzf_complete --select-1 -- "$@" < <(
            git --help -a | grep -E '^\s+' | awk '{print $1}'
        )
        return 0
    fi
    case "${COMP_WORDS[1]}" in
        checkout|switch|merge|rebase|cherry-pick|branch)
            _fzf_complete_git_branches "$@"
            return 0
            ;;
        fetch|push|pull)
            _fzf_complete_git_remotes "$@"
            return 0
            ;;
        add)
            _fzf_complete_git_add "$@"
            return 0
            ;;
        reset)
            _fzf_complete_git_reset "$@"
            return 0
            ;;
        restore)
            _fzf_complete_git_restore "$@"
            return 0
            ;;
        rm|diff)
            _fzf_path_completion "$@"
            return 0
            ;;
        stash)
            if [[ "$COMP_CWORD" = 2 ]]; then
                _fzf_complete -- "$@" < <(
                    echo -e "list\nshow\ndrop\npop\napply\nbranch\npush\nsave\nclear\ncreate\nstore"
                )
                return 0
            elif [[ "$COMP_CWORD" = 3 ]]; then
                _fzf_complete_git_stashes "$@"
                return 0
            fi
            return 1
            ;;
        *)
            return 1
            ;;
    esac
}

complete -F _fzf_complete_git git
