alias l='ls -CF'
alias la='ls -A'
alias ll='ls -alF'
alias ls='ls --color=auto'
alias sort='LC_ALL=C sort'
alias git='PAGER=less LESS="-RiMSx4 -FX" git'
alias grp='LC_ALL=C grep -P --color=always'
alias man='PAGER=less LESS="-RiMSx4" man'
alias isort="isort --settings-path $ROOT_DIR/etc/.isort.cfg"
alias nose="nosetests --config $ROOT_DIR/etc/nose/nose.cfg"
alias lint='pylint --rcfile=$ROOT_DIR/etc/pylint/default.rc'
alias randname='python $ROOT_DIR/py/schematic/guids.py'
alias pyright='pyright --project "$ROOT_DIR/etc/pyrightconfig.json"'

function check-python(){
    echo "# ruff:" ; ruff "${@}"
    echo "# pyright:" ; pyright "${@}"
    echo "# pylint:" ; lint "${@}"
    echo "# nose:" ; nose "${@}"
}

function code-search(){
    # use ag to search this repo
    # first argument is regexp pattern
    # optional additional arguments are directories to search (by default, they will search directories in relevant code repos)
    (
        echo "${@:2}"
        if [[ $# -le 1 ]]; then
            if [[ "$PWD" = "$ROOT_DIR" ]]; then  # _cs_prefix makes ag show relative paths when in ROOT_DIR, otherwise abs paths
                _cs_prefix="."
            else
                _cs_prefix="$ROOT_DIR"
            fi
            echo "$_cs_prefix"
            unset _cs_prefix
        fi
    ) | xargs ag --hidden --nonumbers --color --color-match "3;1;30;34" --color-path "1;32" --pager="less -RS" --case-sensitive "$1"
}

[ -f ~/.bash_aliases ] && source ~/.bash_aliases
