{
  pkgs,
  SCM_PATH,
  repos,
  verbose ? false,
  version ? null,
  pkgs_latest ? (import (builtins.fetchTarball {
      url = "https://github.com/NixOS/nixpkgs/archive/7b4e618602b6c415dc119dcdd1ae5d174117e8b2.tar.gz";
      sha256 = "sha256:00pkayw24cz3b3zmxzxqf360plkgnanz99npmkzbc98li472p5hv";
  }) { overlays = []; } ) }:
pkgs.mkShell rec {
    name = "schematic";
    inherit SCM_PATH;
    SCM_VERBOSE = if verbose then "1" else "0";
    SCM_VERSION = version;
    SCM_REPOS = builtins.concatStringsSep ":" repos;
    shellHook = ''
        source ${SCM_PATH}/shell/.bashrc
    '';
    buildInputs = (with pkgs; [
        (pkgs.callPackage ../lib/python3 {})
        (pkgs.callPackage ../lib/pg_activity {})
        awscli2
        bashInteractive
        broot
        curl
        fzf
        git
        glibcLocales
        gnugrep
        hostname
        htop
        jq
        less
        lsof
        man
        nix-prefetch-scripts
        postgresql_16
        s3cmd
        shellcheck
        silver-searcher
        tig
        tmux
        tree
        pkgs_latest.ruff
        which
        openssl
        pyright
    ] ++ (if pkgs.stdenv.isLinux then [
        # pgadmin  # build error with postgresql14
        pg_top
    ] else []));
}
