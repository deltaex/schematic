args @ {
    repos ? [],
    nixpkgs_overlays ? [],
    verbose ? false,
    version ? null
}:
let
    pkgs = (import ./lib/nixpkgs.nix { overlays = nixpkgs_overlays; });
in rec {
    inherit pkgs;
    SCM_PATH = toString ./.; # avoid copying to the /nix/store
    shell = (import ./shell {
        inherit pkgs SCM_PATH verbose version repos;
    } );
}
